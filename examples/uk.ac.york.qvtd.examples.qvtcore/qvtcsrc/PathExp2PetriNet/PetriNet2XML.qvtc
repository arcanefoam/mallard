import petriNetMM : 'PetriNet.ecore'::PetriNet;
import xmlMM : 'XML.ecore'::XML;
import petri2xmlMM : 'PetriNet2XML.ecore'::PetriNet2XML;

transformation petri2xml {
    petri imports petriNetMM;
    xml imports xmlMM;
    imports PetriNet2XML;
}

/**
 * Rule 'Main'
 * This rule generates the "pnml" root tag from the input PetriNet element.
 * This tag has an "xmlns" attribute and a "net" element as child element.
 * The "net" tag has an "id", a "type" and a "name" attributes, and the
 * following children elements:
 * a "place" element for each Place of the input PetriNet model
 * a "transition" element for each Transition of the input PetriNet model
 * an "arc" element for each Arc of the input PetriNet model.
 */
map Main in petri2xml {
    check petri(pn:PetriNet) {}
    enforce xml() {
        realize root:Root,
        realize xmlns:Attribute,
        realize id:Attribute,
        realize type:Attribute,
        realize net:Element,
        realize name:Element,
        realize text:Element,
        realize val:Text |
        root.name := 'pnml';
        root.children := Sequence{xmlns, net};
        xmlns.name := 'xmlns';
        xmlns.value :='http://www.example.org/pnpl';
        net.name := 'net';
        net.children := Sequence{id, type, name};
        id.name := 'id';
        id.value := 'n1';
        type.name := 'type';
        type.value := 'http://www.example.org/pnpl/PTNet';
        name.name := 'name';
        name.children := Sequence{text};
        text.name := 'text';
        text.children := Sequence{val};

    }
    where() {
        realize pn2root:PetriNet2Root |
        pn2root.petriNet := pn;
        pn2root.root := root;
        pn2root.xmlns := xmlns;
        pn2root.id := id;
        pn2root.type := type;
        pn2root.net := net;
        pn2root.name := name;
        pn2root.text := text;
        pn2root.val := val;
    }
    map {
        where () {
            val.value := pn.name;
        }
    }
}

map Place in petri2xml {
    check petri(pn:PetriNet) {
        pn_s : Place |
        pn.places->includes(pn_s);
    }
    enforce xml(net:Element) {
        realize xml_place : Element,
        realize id : Attribute,
        realize name : Element,
        realize text : Element,
        realize val : Text |
        xml_place.name := 'place';
        xml_place.children := Sequence{id, name};
        id.name := 'id';
        name.name := 'name';
        name.children := Sequence{text};
        text.name := 'text';
        text.children := Sequence{val};
        net.children := net.children->including(xml_place);
    }
    where(pn2root:PetriNet2Root |
            pn2root.petriNet = pn;
            pn2root.net = net; ) {
        realize p2e:Place2Element |
        p2e.place := pn_s;
        p2e.xml_place := xml_place;
        p2e.id := id;
        p2e.name := name;
        p2e.text := text;
        p2e.val := val;

    }
    map {
        where () {
            id.value := pn.places->indexOf(pn_s).toString();
            val.value := pn_s.name;
        }
    }
}

map Transition in petri2xml {
    check petri (pn:PetriNet) {
        pn_t : Transition |
        pn.transitions->includes(pn_t);
    }
    enforce xml(net:Element) {
        realize xml_trans : Element,
        realize trans_id : Attribute |
        xml_trans.name := 'transition';
        xml_trans.children := Sequence{trans_id};
        trans_id.name := 'id';
        net.children := net.children->including(xml_trans);
    }
    where (pn2root:PetriNet2Root |
            pn2root.petriNet = pn;
            pn2root.net = net;) {
        realize t2e:Transition2Element |
        t2e.xml_trans := xml_trans;
        t2e.trans_id := trans_id;
    }
    map {
        where() {
            trans_id.value := pn.places->size().toString() +
                      pn.transitions->indexOf(pn_t).toString();
        }
    }
}

map PlaceToTransArc in petri2xml {
    check petri(pn:PetriNet) {
        pn_a:PlaceToTransArc |
        pn.arcs->includes(pn_a);
    }
    enforce xml(net:Element) {
        realize xml_arc:Element,
        realize id:Attribute,
        realize source:Attribute,
        realize target:Attribute |
        xml_arc.name := 'arc';
        xml_arc.children := Sequence{id, source, target};
        id.name := 'id';
        net.children := net.children->including(xml_arc);
    }
    where(pn2root:PetriNet2Root |
            pn2root.petriNet = pn;
            pn2root.net = net;) {
        realize p2a:Place2TransArc |
        p2a.pn_a := pn_a;
        p2a.xml_arc := xml_arc;
        p2a.id := id;
        p2a.source := source;
        p2a.target := target;
    }
    map {
        where() {
            id.value := pn.places->size().toString() +
                      pn.transitions->size().toString() +
                      pn.arcs->indexOf(pn_a).toString();
            source.name := 'source';
            source.value := pn.places->indexOf(pn_a.source).toString();
            target.name := 'target';
            target.value := pn.places->size().toString() +
                      pn.transitions->indexOf(pn_a.target).toString();
        }
    }
}



map TransToPlaceArc in petri2xml {
    check petri(pn:PetriNet) {
        pn_a:TransToPlaceArc |
        pn.arcs->includes(pn_a);
    }
    enforce xml(net:Element) {
        realize xml_arc:Element,
        realize id:Attribute,
        realize source:Attribute,
        realize target:Attribute |
        xml_arc.name := 'arc';
        xml_arc.children := Sequence{id, source, target};
        id.name := 'id';
        net.children := net.children->including(xml_arc);
    }
    where(pn2root:PetriNet2Root |
            pn2root.petriNet = pn;
            pn2root.net = net;) {
       realize t2p:Trans2PlaceArc |
       t2p.xml_arc := xml_arc;
       t2p.id := id;
       t2p.source := source;
       t2p.target := target;
    }
    map {
        where() {
            id.value := pn.places->size().toString() +
                pn.transitions->size().toString() +
                pn.arcs->indexOf(pn_a).toString();
            source.name := 'source';
            source.value := pn.places->size().toString() +
                pn.transitions->indexOf(pn_a.source).toString();
            target.name := 'target';
            target.value := (pn.places->indexOf(pn_a.target)).toString();
        }
    }
}


