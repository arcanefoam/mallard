/*******************************************************************************
 * Copyright (c) 2013, 2015 The University of York, Willink Transformations and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - initial API and implementation
 ******************************************************************************/
package uk.ac.york.qvtd.compiler.mtc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.Model;
import org.eclipse.ocl.pivot.Package;
import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.qvtd.compiler.internal.utilities.ClassRelationships;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.eclipse.qvtd.pivot.qvtcorebase.CoreDomain;
import org.eclipse.qvtd.pivot.qvtcorebase.CorePattern;
import org.eclipse.qvtd.pivot.qvtimperative.Mapping;
import org.eclipse.qvtd.pivot.qvtimperative.QVTimperativeFactory;

import uk.ac.york.qvtd.compiler.Configuration;
import uk.ac.york.qvtd.compiler.utilities.QvtMtcExecutionException;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyEdge.EdgeType;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.inter.impl.ClassDatumImpl;
import uk.ac.york.qvtd.dependencies.inter.impl.DependencyGraphImpl;
import uk.ac.york.qvtd.dependencies.inter.impl.MappingActionImpl;
import uk.ac.york.qvtd.dependencies.inter.impl.ParameterEdgeImpl;
import uk.ac.york.qvtd.dependencies.inter.impl.RelationEdge;
import uk.ac.york.qvtd.dependencies.util.MappingImplExt;

/**
 * The Class QVTiToDependencyGraph.
 */
public class QVTiToDependencyGraph {

    /** The DependencyGraph. */
    final private DependencyGraph dg;

    /** The class datum trace. */
    final private Map<TypedModel, List<ClassDatum>> classDatumTrace = new HashMap<TypedModel, List<ClassDatum>>();

    /** The var typed model. */
    final private Map<Variable, TypedModel> varTypedModel = new HashMap<Variable, TypedModel>();

    /** The config. */
    final private Configuration config;

    /** The used types. */
    final private Set<Type> usedTypes = new HashSet<Type>();

    /**  The execution mode. */
    final private boolean append;

    final private @NonNull EnvironmentFactory environmentFactory;

    private ClassRelationships clasRels;

    /**
     * Instantiates a new QV ti to dependency graph.
     *
     * @param config the config
     */
    public QVTiToDependencyGraph(Configuration config, @NonNull EnvironmentFactory environmentFactory) {
        this(config, environmentFactory, false);
    }

    /**
     * Instantiates a new QVTi to DependencyGraph.
     *
     * @param config the config
     * @param append the append
     */
    public QVTiToDependencyGraph(Configuration config, @NonNull EnvironmentFactory environmentFactory, boolean append) {
        super();
        this.config = config;
        this.environmentFactory = environmentFactory;
        this.append = append;
        clasRels = new ClassRelationships(environmentFactory);
        dg = new DependencyGraphImpl(clasRels);
    }

    /**
     * Creates a dependency graph based on the producer-consumer relations between the mappings.
     * <p>
     * Dependency edges from the root to class datums should only exist if:
     * <ol>
     * <li>The type does not participate in any containment relation, i.e. it can be at the root of the model
     * <li>The type participates in one containment relation, the relation has an opposite, and the opposite has lower
     * 	  bound 0, i.e. elements of the type can be at the root of the model
     * <li>The type participates in one containment relation, the relation has an opposite, and the opposite has lower
     * 	  bound 1, but the container type is not consumed by any rule, i.e. another rule can not invoke this one by
     * 	  exploiting the relationship. TODO If the rule that consumes the container type is guarded, then perhaps
     *    not all the contained types will be transformed.
     * <li>The type participates in two ore more containment relations, emf does not allow multiple containments to have
     * 	  an opposite, i.e. elements of the type can be at the root of the model
     * <li>The type participates in one required containment relation but any producer-consumer is multi-consumer and the
     *    producer does not produce as many elements as consumed, i.e. starvation can happen from a nested call
     * </ol>
     * @param pModel the model
     * @return the dependency graph
     * @throws QvtMtcExecutionException If there is a problem creating the graph
     */
    public DependencyGraph createDependencyGraph(Resource pModel) throws QvtMtcExecutionException {

        Mapping mr = QVTimperativeFactory.eINSTANCE.createMapping();
        mr.setName("root");
        MappingAction root = new MappingActionImpl(mr);
        dg.addVertex(root);
        dg.setRootVertex(root);
        cacheUsedTypes(pModel);
        createMappingToTypeRelations(pModel);
        // For the root edges, we need access to the metamodels.
        List<Model> mms = getCandidateMetamodels(append);
        // Get all the properties from the metamodels
        List<Property> mmProps = mms.stream()
                .map(e -> e.eResource())
                .flatMap(this::getContentsStream)
                .filter(Property.class::isInstance)
                .map(Property.class::cast)
                .collect(Collectors.toList());
        List<ClassDatum> cds = getStartupClassDatums();
        for (ClassDatum cd : cds) {
            // Find all composite properties which type is the type of the ClassDatum (or one of its super types)
            @Nullable Class clzz = cd.getType().isClass();
            assert clzz != null;
            Set<Class> superClss = clasRels.getAllSuperClasses(clzz);
            List<Property> uses = getTypeUses(mmProps, cd, superClss);
            boolean addEdge = false;
            if (uses.isEmpty() || (uses.size() > 1)) {		// By Ecore constraints any type used in more than two containments can not have a required opposite (lower bound = 1)
                addEdge = true;
            } else if (uses.size() == 1) {		// If only one reference, we need an edge if the opposite lower bound is 0, i.e. isRequired
                Property p = uses.get(0);
                if ((p.getOpposite() != null)) {
                    Property oppProp = p.getOpposite();
                    if (!oppProp.isIsRequired()) {
                        addEdge = true;
                    }
                } else {		// If no opposite, containment is not unique, we need the edge
                    addEdge = true;
                }
            }
            if (addEdge) {
                DependencyEdge e = new ParameterEdgeImpl(root, cd, new HashSet<Variable>(), EdgeType.PRODUCER);
                dg.addEdge(root, cd, e);
            }
        }
        // Find all ClassDatums without incoming edges, they all should be from required containments. Add edges from the
        // Container to them.
        List<ClassDatum> childCds = dg.vertexSet().stream()
                .filter(v -> (v instanceof ClassDatum) && (dg.inDegreeOf(v) == 0))
                .map(ClassDatum.class::cast)
                .collect(Collectors.toList());
        for (ClassDatum cd : childCds) {
            // This type needs to be produces by its container
            // Find the property
            // TODO Cache this?
            @Nullable Class clzz = cd.getType().isClass();
            assert clzz != null;
//            Set<Class> superClss = clasRels.getAllSuperClasses(clzz);
//            List<Property> uses = getTypeUses(mmProps, cd, superClss);
//            assert uses.size() == 1;
//            Property p = uses.get(0);
//            assert p.getOpposite() != null;
//            Property oppProp = p.getOpposite();
            // Find the class datum for the container
            @NonNull
            Set<@NonNull Class> containers = clasRels.getContainerClasses(clzz);
            @Nullable ClassDatum ocd = cds.stream()
                .filter(ccd -> ccd.getDomain().equals(cd.getDomain())
                        && containers.contains(ccd.getType()))
                .findFirst()
                .orElse(null);
            // Childcds has also middle/right elements that dont have a parent in cds.
            if (ocd != null) {
	            DependencyEdge e = dg.getEdge(ocd, cd);
	            if (e == null) {
	                e = new RelationEdge(ocd, cd, EdgeType.CONTAINER);
	                dg.addEdge(ocd, cd, e);
	            }
            }
        }
        // Find all paths in the dependency graph
        dg.initialize();
        return dg;
    }

    private void cacheUsedTypes(Resource pModel) {
        // Precompute the set of types used in the transformation
        Iterator<EObject> it = pModel.getAllContents();
        while (it.hasNext()){
            EObject e = it.next();
            if (e instanceof Variable)
                usedTypes.add(((Variable) e).getType());

        }
    }

    /**
     * Add nodes for all mappings and types and add edges to represent input and output relations.
     * <p>
     * @see {@link QVTiToDependencyGraph#mappingToMappingAction(Mapping)}
     * @param pModel
     */
    private void createMappingToTypeRelations(Resource pModel) {
        Iterator<EObject> it;
        it = pModel.getAllContents();
        while (it.hasNext()){
            EObject e = it.next();
            if (e instanceof Mapping)
                mappingToMappingAction((Mapping) e);
        }
    }

    /**
     * Gets the candidate models' metamodels from the configuration.
     * @param append If true, gets all the metamodels, if false, only gets the input ones.
     * @param config the configuration
     * @return a list of all metamodel (Element -> modelImpl)
     * @throws QvtMtcExecutionException If metamodel information is missing in the configuraiton model or if a metamodel
     * 	can not be loaded to a resource
     */
    private List<Model> getCandidateMetamodels(boolean append) throws QvtMtcExecutionException {

        List<Model> result = new ArrayList<Model>();
        Set<TypedModel> allDirs = config.getInputDomains();
        if (append) {
            allDirs.add(config.getMiddleDomain());
            allDirs.add(config.getOutputDomain());
        }
        for(TypedModel dir : allDirs) {
            for (Package p : dir.getUsedPackage()) {
                // The package should already be loaded by the QVTc parser
                result.add((Model) p.eContainer());
            }

        }
        return result;
    }

    private Stream<EObject> getContentsStream(Resource r) {
        Iterable<EObject> iterable = () -> r.getAllContents();
        return StreamSupport.stream(iterable.spliterator(), false);
    }

//    protected void loadEcoreFile(String ecoreFileName, EPackage ePackage) {
//        URI fileURI = baseURI.appendSegment(ecoreFileName);
//        ResourceSet rSet = getResourceSet();
//        rSet.getPackageRegistry().put(fileURI.toString(), ePackage);
//    }

    /**
     * We only want ClassDatums in the domains of interest, this depends on the append flag (if append we are also
     * interested in the trace and output models)
     * <p>
     * @return The list of ClassDatums that are available at startup
     */
    private List<ClassDatum> getStartupClassDatums() {
        Set<TypedModel> initialDomainNames = config.getInputDomains();
        if (append) {
            initialDomainNames.add(config.getMiddleDomain()); 			// In a QVTp model, the middle has a name
            initialDomainNames.add(config.getOutputDomain());
        }
        List<ClassDatum> cds = classDatumTrace.values().stream()
                .flatMap(listContainer -> listContainer.stream())
                .filter(cd -> initialDomainNames.contains(cd.getDomain()))
                .collect(Collectors.toList());
        return cds;
    }

    /**
     * Gets the typed model.
     *
     * @param var the var
     * @return the typed model
     */
    private TypedModel getTypedModel(Variable var) {
        TypedModel tm = varTypedModel.get(var);
        if (tm != null)
            return tm;
        CorePattern cp = (CorePattern) var.eContainer();
        assert cp.getArea() instanceof CoreDomain;
        CoreDomain cd = (CoreDomain) cp.getArea();
        tm = cd.getTypedModel();
        varTypedModel.put(var, tm);
        return tm;
    }

    /**
     * Get the list of properties in which the Class Datum type is used.
     * @param mmProps
     * @param cd
     * @param superClss
     * @return
     */
    private List<Property> getTypeUses(List<Property> mmProps, ClassDatum cd, Set<Class> superClss) {
        List<Property> uses = mmProps.stream()
                .filter(p -> {
                    Type t = p.getType().flattenedType();
                    return p.isIsComposite() && (t.equals(cd.getType()) || superClss.contains(t));
                })
                .collect(Collectors.toList());
        return uses;
    }

    /**
     * Checks if is from middle.
     *
     * @param var the var
     * @return true, if is from middle
     */
    private boolean isFromMiddle(Variable var) {
        TypedModel tm = getTypedModel(var);
        return config.isMiddle(tm);
    }

    /**
     * Checks if is used.
     *
     * @param sc the sc
     * @return true, if is used
     */
    private boolean isUsed(Class sc) {
        return usedTypes.contains(sc);
    }

    /**
     * Add a mapping action node for the mapping and Class datum
     * nodes for its input/output types. Connect nodes with edges.
     *
     * @param m the m
     * @return the mapping action
     */
    private MappingAction mappingToMappingAction(Mapping m) {
        MappingAction ma = new MappingActionImpl(m);
        dg.addVertex(ma);
        MappingImplExt mext = new MappingImplExt(m);
        for (Variable iv : mext.getInputVariables()) {
            if (dg.getMiddleDomain() == null) {
                if (isFromMiddle(iv))
                    dg.setMiddleDomain(getTypedModel(iv));
            }
            ClassDatum cd = typeToClassDatum(iv.getType(), getTypedModel(iv));
            ParameterEdgeImpl e = (ParameterEdgeImpl) dg.getEdge(cd, ma);
            if (e == null) {
                e = new ParameterEdgeImpl(cd, ma, new HashSet<Variable>(), EdgeType.CONSUMER);
                dg.addEdge(cd, ma, e);
            } // TODO else { find if it must be distinct}
            e.addVariable(iv);
        }
        for (Variable ov : mext.getOutputVariables()) {
            ClassDatum cd = typeToClassDatum(ov.getType(), getTypedModel(ov));
            ParameterEdgeImpl e = (ParameterEdgeImpl) dg.getEdge(ma, cd);
            if (e == null) {
                e = new ParameterEdgeImpl(ma, cd, new HashSet<Variable>(), EdgeType.PRODUCER);
                dg.addEdge(ma, cd, e);
            }// TODO else { find if it must be distinct}
            e.addVariable(ov);
        }
        return ma;
    }

    /**
     * Add missing types form the hierarchy and connect them.
     *
     * @param classDatum the class datum
     */
    private void typeHierarchy(ClassDatum classDatum) {
        Type ct = classDatum.getType();
        TypedModel domain = classDatum.getDomain();
        //for (Class sc : ((Class)ct).getSuperClasses()) {
        for (Class sc : clasRels.getAllSuperClasses(((Class)ct))) {
            // Only add super types that are involved in the transformation
            if (isUsed(sc)) {
                Type sct = (Type)sc;
                ClassDatum scDatum = null;
                if (classDatumTrace.containsKey(domain)) {
                    for (ClassDatum cd : classDatumTrace.get(domain)) {
                        if (cd.getType().equals(sct)) {
                            scDatum = cd;
                            break;
                        }
                    }
                    if (scDatum == null) {
                        scDatum = new ClassDatumImpl(sct, domain);
                        classDatumTrace.get(domain).add(scDatum);
                        dg.addVertex(scDatum);
                    }
                } else {
                    List<ClassDatum> cds = new ArrayList<ClassDatum>();
                    scDatum = new ClassDatumImpl(sct, domain);
                    cds.add(scDatum);
                    classDatumTrace.put(domain, cds);
                    dg.addVertex(scDatum);
                }
                DependencyEdge e = dg.getEdge(classDatum, scDatum);
                if (e == null) {
                    e = new RelationEdge(classDatum, scDatum, EdgeType.INHERITANCE);
                    dg.addEdge(classDatum, scDatum, e);
                }
            }
        }

    }

    /**
     * Type to class datum.
     *
     * @param t the t
     * @param domain the domain
     * @return the class datum
     */
    private ClassDatum typeToClassDatum(Type t, TypedModel domain) {
        ClassDatum result = null;
        if (classDatumTrace.containsKey(domain)) {
            for (ClassDatum cd : classDatumTrace.get(domain)) {
                if (cd.getType().equals(t)) {
                    result = cd;
                    break;
                }
            }
            if (result == null) {
                result = new ClassDatumImpl(t, domain);
                classDatumTrace.get(domain).add(result);
                dg.addVertex(result);
            }
        } else {
            List<ClassDatum> cds = new ArrayList<ClassDatum>();
            result = new ClassDatumImpl(t, domain);
            cds.add(result);
            classDatumTrace.put(domain, cds);
            dg.addVertex(result);
        }
        typeHierarchy(result);
        return result;
    }

}
