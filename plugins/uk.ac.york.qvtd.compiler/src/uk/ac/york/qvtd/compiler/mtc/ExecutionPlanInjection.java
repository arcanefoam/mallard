/*******************************************************************************
 * Copyright (c) 2013, 2015 The University of York, Willink Transformations and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - initial API and implementation
 ******************************************************************************/
package uk.ac.york.qvtd.compiler.mtc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.CollectionType;
import org.eclipse.ocl.pivot.Iteration;
import org.eclipse.ocl.pivot.IteratorExp;
import org.eclipse.ocl.pivot.NavigationCallExp;
import org.eclipse.ocl.pivot.OCLExpression;
import org.eclipse.ocl.pivot.Package;
import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.StandardLibrary;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.ocl.pivot.VariableExp;
import org.eclipse.ocl.pivot.internal.complete.StandardLibraryInternal;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.ocl.pivot.utilities.NameUtil;
import org.eclipse.ocl.pivot.utilities.PivotUtil;
import org.eclipse.qvtd.pivot.qvtbase.Rule;
import org.eclipse.qvtd.pivot.qvtbase.Transformation;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.eclipse.qvtd.pivot.qvtbase.utilities.QVTbaseUtil;
import org.eclipse.qvtd.pivot.qvtcore.utilities.QVTcoreHelper;
import org.eclipse.qvtd.pivot.qvtcorebase.QVTcoreBaseFactory;
import org.eclipse.qvtd.pivot.qvtimperative.ImperativeModel;
import org.eclipse.qvtd.pivot.qvtimperative.Mapping;
import org.eclipse.qvtd.pivot.qvtimperative.MappingCall;
import org.eclipse.qvtd.pivot.qvtimperative.MappingCallBinding;
import org.eclipse.qvtd.pivot.qvtimperative.MappingLoop;
import org.eclipse.qvtd.pivot.qvtimperative.MappingSequence;
import org.eclipse.qvtd.pivot.qvtimperative.MappingStatement;
import org.eclipse.qvtd.pivot.qvtimperative.QVTimperativeFactory;
import org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor;

import uk.ac.york.qvtd.compiler.Binding;
import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.bindings.DerivedVariable;
import uk.ac.york.qvtd.compiler.bindings.NavigationBinding;
import uk.ac.york.qvtd.compiler.executionplan.AllInstLoop;
import uk.ac.york.qvtd.compiler.executionplan.CallActionInvocation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.compiler.executionplan.LoopInvocation;
import uk.ac.york.qvtd.compiler.executionplan.LoopOperation;
import uk.ac.york.qvtd.compiler.executionplan.PropNavLoop;


/**
 * The Class ExecutionPlanInjection injects the Executin plan into a QVTi model to add the reguired mapping calls,
 * bindings and local variables.
 */
public class ExecutionPlanInjection extends AbstractExecutionPlanInjection implements QVTimperativeVisitor<Object> {

    /** The copier. */
    private final EcoreUtil.Copier copier;

    /** The environment factory. */
    private final EnvironmentFactory environmentFactory;

    private QVTcoreHelper helper;


    /**
     * Instantiates a new execution plan injection
     *
     * @param executionPlan the context
     * @param environmentFactory the environment factory
     */
    public ExecutionPlanInjection(ExecutionPlanGraph executionPlan, @NonNull EnvironmentFactory environmentFactory) {
        super(executionPlan);
        assert executionPlan.vertexSet().stream()
            .filter(v -> v instanceof CallActionOperation)
            .filter(v -> !v.isRoot())
            .allMatch(caop -> executionPlan.inDegreeOf(caop) > 0);
        this.environmentFactory = environmentFactory;
        copier = new EcoreUtil.Copier();
        helper = new QVTcoreHelper(environmentFactory);
    }

    /**
     * Adds the mapping statements.
     *
     * @param mapping the mapping
     * @param op the op
     */
    public void addMappingStatements(Mapping mapping, Operation op) {

        // Add invocations
        Set<Invocation> targetOps = context.outgoingEdgesOf(op);
        if (!targetOps.isEmpty())
            mapping.setMappingStatement(craeteMappingStatement(targetOps));
    }


    /**
     * @return the copier
     */
    public EcoreUtil.Copier getCopier() {
        return copier;
    }

    /**
     * Mapping statement for invocation.
     *
     * @param inv the inv
     * @return the mapping statement
     */
    public MappingStatement mappingStatementForInvocation(Invocation inv) {
        MappingStatement mst;
        if (inv instanceof CallActionInvocation) {
            mst = createMappingCall((CallActionInvocation) inv);
        } else {
            mst = createLoopCall((LoopInvocation) inv);
        }
        return mst;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitImperativeModel(org.eclipse.qvtd.pivot.qvtimperative.ImperativeModel)
     */
    @Override
    @Nullable
    public Object visitImperativeModel(@NonNull ImperativeModel mIn) {
        ImperativeModel qvtiModel = (ImperativeModel) copier.copy(mIn);		// Copy all of the QVTp model
        copier.copyReferences();
        qvtiModel.setExternalURI(mIn.getExternalURI().replace(".qvtp.qvti", ".qvti"));
        // Now that all has been copied, inject the local variables, loops and
        // calls
        Package modelP = mIn.getOwnedPackages().stream().filter(p -> p.getName().equals("")).limit(1).collect(Collectors.toList()).get(0);
        assert modelP.getOwnedClasses().get(0) instanceof Transformation;
        Transformation tIn = (Transformation) modelP.getOwnedClasses().get(0);
        tIn.accept(this);
        Transformation tOut = (Transformation) copier.get(tIn);
        TreeIterator<EObject> tIt = tOut.eAllContents();
        List<Mapping> invokedMappings = new ArrayList<Mapping>();
        while (tIt.hasNext()) {
            EObject eo = tIt.next();
            if (eo instanceof MappingCall) {
                invokedMappings.add(((MappingCall) eo).getReferredMapping());
            }
        }
        @NonNull
        List<Rule> rulesOut = new ArrayList<Rule>(tOut.getRule());
        rulesOut.removeAll(invokedMappings);
        assert rulesOut.size() == 1;		// Only root should remain
        assert rulesOut.get(0).getName().equals("__root__");
        return qvtiModel;

    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitMapping(org.eclipse.qvtd.pivot.qvtimperative.Mapping)
     */
    @Override
    @Nullable
    public Object visitMapping(@NonNull Mapping object) {
        //System.out.println("Inejection variables, loops and calls in " + object.getName());
        // Find the equivalent mapping in the plan
        for (Operation op : context.vertexSet()) {
            if (op instanceof CallActionOperation) {
                if (((CallActionOperation) op).getAction().getMapping().equals(object)) {
                    //System.out.println("Match!!");
                    Mapping iMapping = (Mapping) copier.get(object);
                    addLocalVariables(iMapping, op);
                    addMappingStatements(iMapping, op);
                }
            }
        }
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtbase.util.QVTbaseVisitor#visitTransformation(org.eclipse.qvtd.pivot.qvtbase.Transformation)
     */
    @Override
    @Nullable
    public Object visitTransformation(@NonNull Transformation object) {
        // Create the root mapping
        Mapping root = QVTimperativeFactory.eINSTANCE.createMapping();
        root.setName("__root__");
        root.setBottomPattern(QVTimperativeFactory.eINSTANCE.createImperativeBottomPattern());
        root.setGuardPattern(QVTcoreBaseFactory.eINSTANCE.createGuardPattern());
        addMappingStatements(root, context.getRoot());
        for (Rule m : object.getRule()) {
            m.accept(this);
        }
        ((Transformation) copier.get(object)).getRule().add(root);
        return null;
    }

    /**
     * Adds the local variables.
     *
     * @param mapping the mapping
     * @param op the op
     */
    private void addLocalVariables(Mapping mapping, Operation op) {
        // Copy local variables
        for (DerivedVariable dv : ((CallActionOperation) op).getLocalVariables()) {
            Variable localVar = dv.getVariable();
            // Use the source variable and navigation info to initialise the variable
            // TODO the source variable might be another derived variable
            Variable iVar = (Variable) copier.get(dv.getSourveVariable());
            assert iVar != null;
            //VariableExp asSource = PivotUtil.createVariableExp(iVar);
            //PropertyCallExp initExp = PivotUtil.createPropertyCallExp(asSource, dv.getNavigation());
            //List<Property> properties = dv.getNavigation();
            NavigationCallExp initExp = createNavigationExp(iVar, dv.getNavigation());
            //for (int i = 0; i < properties.size())
            //PropertyCallExp initExp = PivotUtil.createNavigationCallExp(asSource, asProperty)  (asSource, );
            localVar.setOwnedInit(initExp);
            mapping.getBottomPattern().getVariable().add(localVar);
        }
    }

    /**
     * Craete mapping statement.
     *
     * @param targetOps the target ops
     * @return the mapping statement
     */
    private MappingStatement craeteMappingStatement(Set<Invocation> targetOps) {

        MappingStatement mst = null;
        if (targetOps.size() > 1) {
            mst = QVTimperativeFactory.eINSTANCE.createMappingSequence();
            ((MappingSequence)mst).getMappingStatements().addAll(craeteMappingStatements(targetOps));
        } else {
            Iterator<Invocation> opsIt = targetOps.iterator();
            if (opsIt.hasNext()) {
                Invocation next = opsIt.next();
                mst = mappingStatementForInvocation(next);
            }
        }
        return mst;
    }

    /**
     * Craete mapping statements.
     *
     * @param targetOps the target ops
     * @return the list
     */
    private List<MappingStatement> craeteMappingStatements(Set<Invocation> targetOps) {

        List<MappingStatement> stms = new ArrayList<MappingStatement>();
        Iterator<Invocation> opsIt = targetOps.iterator();
        while (opsIt.hasNext()) {
            Invocation next = opsIt.next();
            MappingStatement mst = mappingStatementForInvocation(next);
            stms.add(mst);
        }
        return stms;
    }

    /**
     * Creates the binding exp.
     *
     * @param b the b
     * @return the OCL expression
     */
    private OCLExpression createBindingExp(Binding b) {

        OCLExpression exp;
        if (b instanceof NavigationBinding) {
            Variable sourceVariable = b.getSourceVariable();
            if (copier.containsKey(sourceVariable)) {
                sourceVariable = (Variable) copier.get(sourceVariable);
            }
            exp = createNavigationExp(sourceVariable, ((NavigationBinding) b).getNavigation());
        } else {
            Variable sourceVariable = b.getSourceVariable();
            if (copier.containsKey(sourceVariable)) {
                Variable copy = (Variable) copier.get(sourceVariable);
                assert copy != null;
                exp = PivotUtil.createVariableExp(copy);
            } else {
                exp = PivotUtil.createVariableExp(sourceVariable);
            }
        }
        return exp;
    }

    /**
     * Creates the bindings.
     *
     * @param invocation the invocation
     * @return the list
     */
    private List<MappingCallBinding> createBindings(CallActionInvocation invocation) {

        List<MappingCallBinding> result = new ArrayList<MappingCallBinding>();
        for (Binding b : invocation.getBindings().values()) {
            MappingCallBinding mb = QVTimperativeFactory.eINSTANCE.createMappingCallBinding();
            mb.setBoundVariable((Variable) copier.get(b.getBoundVariable()));
            mb.setValue(createBindingExp(b));
            result.add(mb);
        }
        return result;
    }

    /**
     * Creates the loop call.
     *
     * @param inv the inv
     * @return the mapping loop
     */
    private MappingLoop createLoopCall(LoopInvocation inv) {
        MappingLoop loop = QVTimperativeFactory.eINSTANCE.createMappingLoop();
        LoopOperation loopOp = ((LoopOperation)inv.getTarget());
        loop.getOwnedIterators().add(loopOp.getLoopVariable());
        if (loopOp instanceof AllInstLoop) {
            StandardLibraryInternal standardLibrary = (StandardLibraryInternal)environmentFactory.getStandardLibrary();
            TypedModel typedModel = (TypedModel) copier.get(((AllInstLoop) loopOp).getModelType().getDomain());
            assert typedModel != null;
            Variable contextVariable = QVTbaseUtil.getContextVariable(standardLibrary, typedModel);
            VariableExp modelExp = PivotUtil.createVariableExp(contextVariable);
            Type modelType = ((AllInstLoop) loopOp).getModelType().getType();
            assert modelType != null;
            OCLExpression exp  = helper.createOperationCallExp(modelExp, "objectsOfKind", helper.createTypeExp(modelType));
            loop.setOwnedSource(exp);
        } else if (loopOp instanceof PropNavLoop) {
            PropNavLoop propLoop = (PropNavLoop) loopOp;
            Variable sourceVariable = propLoop.getSourceVariable();
            if (copier.containsKey(sourceVariable)) {
                sourceVariable = ((Variable) copier.get(sourceVariable));
            }
            assert sourceVariable != null;
            @NonNull OCLExpression exp = createNavigationExp(sourceVariable, propLoop.getNavigation());
            Property loopProperty = propLoop.getNavigation().get(propLoop.getNavigation().size()-1);
            Type actualType = loopProperty.getType();
            if (actualType instanceof CollectionType) {
                actualType = ((CollectionType)actualType).getElementType();
            }
            Type selectByKindType = propLoop.getLoopVariable().getType();
            if (!selectByKindType.equals(actualType)) {
                //exp = wrapInSelectByKind(exp, propLoop.getLoopVariable().getType());
                exp = helper.createOperationCallExp(exp, "selectByKind", helper.createTypeExp(selectByKindType));
            }
            loop.setOwnedSource(exp);
            // Do we need a filter in the previous loop? If the source variable is an iterator and any of the properties
            // is of a type that has sub types, we need a filter in the parent loop
            // FIXME we can only fix this issue for the first navigation, if it happens further down, then we would
            // need to split the navigation
            if (propLoop.getNavigation().size() > 1) {
                Property pn = propLoop.getNavigation().get(0);
                if (pn.getType() instanceof Class) {
                    Class pType = (Class) pn.getType();
                    // We assume that if it is abstract, it has subtypes
                    if (pType.isIsAbstract()) {		// We can only fix one
                        Property nextP =  propLoop.getNavigation().get(1);
                        if (!nextP.getOwningClass().equals(pType)) {
                            if (sourceVariable.eContainer() instanceof MappingLoop) {
                                MappingLoop parentLoop = (MappingLoop) sourceVariable.eContainer();
                                OCLExpression ownedSource = parentLoop.getOwnedSource();
                                Type collectionType = ownedSource.getType();
                                assert collectionType != null;
                                Type elementType = ((CollectionType)collectionType).getElementType();
                                assert elementType != null;
                                @NonNull Variable asIterator = PivotUtil.createVariable("i", elementType, true, null);
                                OCLExpression propertyCallExp = helper.createNavigationCallExp(helper.createVariableExp(asIterator), pn);
                                OCLExpression equalsExp = helper.createOperationCallExp(propertyCallExp, "=", helper.createTypeExp(nextP.getOwningClass()));
                                @NonNull IteratorExp iteratorExp = helper.createIteratorExp(ownedSource, getSelectIteration(), Collections.singletonList(asIterator), equalsExp);
                                //EcoreUtil.delete(ownedSource);
                                parentLoop.setOwnedSource(iteratorExp);
                            }
                        }
                    }
                }
            }
        }
        loop.setOwnedBody(craeteMappingStatement(context.outgoingEdgesOf(inv.getTarget())));
        return loop;
    }

    private @NonNull Iteration getSelectIteration() {
        org.eclipse.ocl.pivot.Class collectionType = ((StandardLibraryInternal)environmentFactory.getStandardLibrary()).getSetType();
         org.eclipse.ocl.pivot.@Nullable Operation selectIteration = NameUtil.getNameable(collectionType.getOwnedOperations(), "select");
        assert selectIteration != null;
        return (Iteration) selectIteration;
    }


    /**
     * Creates the mapping call.
     *
     * @param inv the inv
     * @return the mapping statement
     */
    private MappingStatement createMappingCall(CallActionInvocation inv) {
        MappingCall call = QVTimperativeFactory.eINSTANCE.createMappingCall();
        call.getBinding().addAll(invocationBindings(inv));
        CallActionOperation targetOp = ((CallActionOperation) inv.getTarget());
        Mapping targetMapping = targetOp.getAction().getMapping();
        call.setReferredMapping((Mapping) copier.get(targetMapping));
        call.getBinding().addAll(createBindings(inv));
        return call;
    }

    /**
     * Creates the navigation exp.
     *
     * @param sourceVariable the source variable
     * @param navigation the navigation
     * @return the property call exp
     */
    private @NonNull NavigationCallExp createNavigationExp(@NonNull Variable sourceVariable, List<Property> navigation) {
        // There is a implicit navigation, it must be safe. All other navigations should cause exceptions.
        if (navigation.stream().anyMatch(p -> p.isIsImplicit())) {
            return createNavigationExp(sourceVariable, navigation, true);
        }
        else {
            return createNavigationExp(sourceVariable, navigation, false);
        }
    }

    /**
     * Creates the navigation exp, which is safe. This should be used ONLY for mapping loops
     *
     * @param sourceVariable the source variable
     * @param navigation the navigation
     * @return the property call exp
     */
    private @NonNull NavigationCallExp createNavigationExp(@NonNull Variable sourceVariable, List<Property> navigation, boolean makeSafe) {

        @NonNull VariableExp sourceExp = PivotUtil.createVariableExp(sourceVariable);
        @NonNull OCLExpression asChild = PivotUtil.createNavigationCallExp(sourceExp, navigation.get(0));
        for (int i = 1; i < navigation.size(); i++) {
            Property property = navigation.get(i);
            // If the asChild type is different from the Property owner, we need a cast!
            Class owningClass = property.getOwningClass();
            Type owningType = (Type) owningClass;
            if (!asChild.getType().equals(owningClass)) {
                assert owningType != null;
                asChild = helper.createOperationCallExp(asChild, "oclAsType", helper.createTypeExp(owningType));
            }
            asChild = PivotUtil.createNavigationCallExp(asChild, property);
            // If the property is 0..1 make it safe
            if (!property.isIsRequired()) {
                ((NavigationCallExp) asChild).setIsSafe(true);
            }
            else {
                ((NavigationCallExp) asChild).setIsSafe(true);
            }
        }
        ((NavigationCallExp) asChild).setIsSafe(true);
        return (NavigationCallExp) asChild;
    }



    /**
     * Gets the ocl element all instances op.
     *
     * @return the ocl element all instances op
     */
    private org.eclipse.ocl.pivot.Operation getOclElementAllInstancesOp() {
        StandardLibrary sl = environmentFactory.getStandardLibrary();
        Class el = sl.getOclElementType();
        for(org.eclipse.ocl.pivot.Operation op : el.getOwnedOperations()) {
            if (op.getName().equals("allInstances")) {
                return op;
            }
            // TODO If the operation is not found we need to use the complete model?
        }
        return null;
    }


    /**
     * Invocation bindings.
     *
     * @param inv the inv
     * @return the list
     */
    private List<MappingCallBinding> invocationBindings(CallActionInvocation inv) {

        List<MappingCallBinding> bindings = new ArrayList<MappingCallBinding>();
        return bindings;
    }

}
