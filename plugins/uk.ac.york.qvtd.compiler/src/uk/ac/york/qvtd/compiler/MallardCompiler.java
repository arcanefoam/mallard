/*******************************************************************************
 * Copyright (c) 2013, 2015 The University of York, Willink Transformations and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - initial API and implementation
 ******************************************************************************/
package uk.ac.york.qvtd.compiler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.ocl.pivot.Element;
import org.eclipse.ocl.pivot.internal.utilities.EnvironmentFactoryInternal;
import org.eclipse.ocl.pivot.util.Visitable;
import org.eclipse.qvtd.compiler.internal.utilities.ClassRelationships;
import org.eclipse.qvtd.pivot.qvtbase.Transformation;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.eclipse.qvtd.pivot.qvtcore.CoreModel;
import org.eclipse.qvtd.pivot.qvtcorebase.analysis.DomainUsage;
import org.eclipse.qvtd.pivot.qvtimperative.ImperativeModel;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiEnvironmentFactory;
import org.eclipse.qvtd.pivot.qvtimperative.utilities.QVTimperativeDomainUsageAnalysis;
import org.eclipse.qvtd.pivot.qvtimperative.utilities.QVTimperativeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.Configuration.Mode;
import uk.ac.york.qvtd.compiler.aco.epp.AcoMaster;
import uk.ac.york.qvtd.compiler.executionplan.ExecutionPlan;
import uk.ac.york.qvtd.compiler.exploration.CompleteExploration;
import uk.ac.york.qvtd.compiler.mtc.ExecutionPlanInjection;
import uk.ac.york.qvtd.compiler.mtc.QVTcToQVTu;
import uk.ac.york.qvtd.compiler.mtc.QVTiToDependencyGraph;
import uk.ac.york.qvtd.compiler.mtc.QVTmToQVTip;
import uk.ac.york.qvtd.compiler.mtc.QVTuToQVTm;
import uk.ac.york.qvtd.compiler.slopy.SloppyPlanner;
import uk.ac.york.qvtd.compiler.utilities.MallardMtcUtil;
import uk.ac.york.qvtd.compiler.utilities.QvtMtcExecutionException;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;

/**
 * The Class MallardCompiler.
 */
public class MallardCompiler {

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(MallardCompiler.class.getName());

    /** The QVTc execution configuration. */
    private Configuration config;

    /** The environment factory. */
    private @NonNull QVTiEnvironmentFactory environmentFactory;

    /** The qvti AS models. */
    private List<Resource> qvtiAsModels = new ArrayList<Resource>();

    /** The qvtc AS. */
    private Resource qvtcAsModel;

    /** The qvti uri. */
    private URI qvtiUri;

    /** The qvtm AS. */
    private Resource qvtmAsModel;

    /** The qvtm uri. */
    private URI qvtmUri;

    /** The qvtp AS. */
    private Resource qvtpAsModel;

    /** The qvtp uri. */
    private URI qvtpUri;

    /** The qvtu AS. */
    private Resource qvtuAsModel;

    /** The qvtu uri. */
    private URI qvtuUri;

    /** The saving options. */
    private Map<?, ?> savingOptions;

    /** THe QVTi configurations
     * Each QVTi resource has different TypedModel instances, thus the configuratio has to be saved for each one.
     */
    private Map<Resource, Configuration> qvtiConfigurations = new HashMap<Resource, Configuration>();

    /** The dg. */
    private DependencyGraph dg;

    /** The best plans. */
    private Set<ExecutionPlanGraph> bestPlans;

    /** The qvti costs. */
    private Map<Resource, Double> qvtiCosts = new HashMap<Resource, Double>();


    /**
     * Instantiates a new mallard broker.
     *
     * @param qvtcasUri the qvtcas uri
     * @param genAstUri the gen ast uri
     * @param environmentFactory the environment factory
     * @param savingOptions the saving options
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public MallardCompiler(URI qvtcasUri, URI genAstUri, @NonNull QVTiEnvironmentFactory environmentFactory,
            @NonNull Map<?, ?> savingOptions) throws QvtMtcExecutionException {

        //this.qvtcasUri = qvtcasUri;
        this.environmentFactory = environmentFactory;
        this.savingOptions = savingOptions;
        qvtcAsModel = MallardMtcUtil.loadASModelFromUri(qvtcasUri, environmentFactory);
        // The intermediate files to the bin, the final qvtias to the gen folder
        String currentUri = qvtcasUri.trimFileExtension().toString();
        String outputUri = MallardMtcUtil.changeTargetToBinFolder(currentUri);
        URI tempModelsBaseUri = URI.createURI(outputUri).trimFileExtension();
        qvtuUri = tempModelsBaseUri.appendFileExtension("qvtu.qvtcas");
        qvtmUri = tempModelsBaseUri.appendFileExtension("qvtm.qvtcas");
        qvtpUri = tempModelsBaseUri.appendFileExtension("qvtp.qvtias");
        currentUri = qvtcasUri.trimFileExtension().lastSegment();
        qvtiUri = genAstUri.appendSegment(currentUri).appendFileExtension("qvtias");
    }

    /**
     * Configure. URIs can be null if we are only running the MTC
     *
     * @param qvtdAsModel the qvtd as model
     * @param mode the mode
     * @param direction the direction
     * @return the configuration
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public static Configuration createConfiguration(@NonNull Resource qvtdAsModel, @NonNull Mode mode,
            String direction) throws QvtMtcExecutionException {

        Transformation transformation = MallardMtcUtil.getTransformation(qvtdAsModel);
        Set<@NonNull TypedModel> inputs = new HashSet<@NonNull TypedModel>();
        TypedModel output = null;
        TypedModel middle = null;

        for (TypedModel typedModel : transformation.getModelParameter()) {
            @Nullable String modelName = typedModel.getName();
            if (direction.equals(modelName)) {
                output = typedModel;
            }
            else if ((modelName == null) || (QVTimperativeUtil.MIDDLE_DOMAIN_NAME.equals(modelName))) {
                middle = typedModel;
            }
            else {
                inputs.add(typedModel);
            }
        }
        assert output != null;
        assert middle != null;
        assert !inputs.isEmpty();
        return new Configuration(mode, inputs, output, middle);
    }

    /**
     * Compile one of the best solutions, selected randomly.
     *
     * @param verbose the verbose
     * @throws QvtMtcExecutionException If there is an exception at any point in
     * 	the MTC execution.
     */
    public void compile(boolean verbose) throws QvtMtcExecutionException {

        logger.info("Running CE on " + qvtiUri.trimFileExtension().lastSegment());
        compileC2P();
        CompleteExploration ce = completeExploration(verbose);
        ExecutionPlanGraph plan = ce.getBestSolution();
        Resource iAS = injectExecutionPlan(qvtpAsModel, plan);
        String idUri = iAS.getURI().trimFileExtension().toString() + "_" + plan.getCost() + "_0";
        iAS.setURI(URI.createURI(idUri).appendFileExtension("qvtias"));
        try {
            iAS.save(savingOptions);
        } catch (IOException e2) {
            throw new QvtMtcExecutionException(e2.getMessage());
        }
        qvtiAsModels.add(iAS);
        bestPlans = new HashSet<ExecutionPlanGraph>(1);
        bestPlans.add(plan);
    }

    /**
     * Compile the best ACO slution.
     *
     * @param verbose the verbose
     * @throws QvtMtcExecutionException If there is an exception at any point in
     * 	the MTC execution.
     */
    public void compileAco(boolean verbose) throws QvtMtcExecutionException {

        logger.info("Running ACO on " + qvtiUri.trimFileExtension().lastSegment());
        compileC2P();
        Map<Element, DomainUsage> analysis = craeteDomainAnalysis();
        Configuration qvtpConfig = qvtiConfigurations.get(qvtpAsModel);
        AcoMaster aco = new AcoMaster(getDg(), verbose, analysis, environmentFactory);//, qvtpConfig);
        aco.solve(true);
        ExecutionPlanGraph plan = aco.getFinalPlan();
        Resource iAS = injectExecutionPlan(qvtpAsModel, plan);
        try {
            iAS.save(savingOptions);
        } catch (IOException e2) {
            throw new QvtMtcExecutionException(e2.getMessage());
        }
        qvtiAsModels.add(iAS);
    }



    /**
     * Compile the best ACO solution random samples.
     *
     * @param verbose the verbose
     * @throws QvtMtcExecutionException If there is an exception at any point in
     * 	the MTC execution.
     */
    public void compileAllAco(boolean verbose) throws QvtMtcExecutionException {

        logger.info("Running ACO on " + qvtiUri.trimFileExtension().lastSegment());
        compileC2P();
        Map<Element, DomainUsage> analysis = craeteDomainAnalysis();
        Configuration qvtpConfig = qvtiConfigurations.get(qvtpAsModel);
        @NonNull
        TypedModel outputDomain = qvtpConfig.getOutputDomain();
        AcoMaster aco = new AcoMaster(getDg(), verbose, analysis, environmentFactory);//, qvtpConfig);
        aco.solve(true);
        int id = 0;
        Iterator<ExecutionPlan> it = aco.getSamplePlans().iterator();
        while (it.hasNext()) {
            ExecutionPlanGraph plan  = it.next();
            Resource iAS = injectExecutionPlan(qvtpAsModel, plan);
            try {
                // We need a different uri for each
                String idUri = iAS.getURI().trimFileExtension().toString() + "_" + plan.getCost() + "_" + id++;
                iAS.setURI(URI.createURI(idUri).appendFileExtension("qvtias"));
                iAS.save(savingOptions);
                qvtiAsModels.add(iAS);
            } catch (IOException e2) {
                throw new QvtMtcExecutionException(e2.getMessage());
            }
        }
    }

    /**
     * Compile the Sloppy solution.
     *
     * @param verbose the verbose
     * @throws QvtMtcExecutionException If there is an exception at any point in
     * 	the MTC execution.
     */
    public void compileBallot(boolean verbose) throws QvtMtcExecutionException {

        logger.info("Running Sloppy on " + qvtiUri.trimFileExtension().lastSegment());
        compileC2P();
        Map<Element, DomainUsage> analysis = craeteDomainAnalysis();
        SloppyPlanner planner = new SloppyPlanner(getDg(), analysis, verbose, environmentFactory);
        planner.createSloppyPlan();
        ExecutionPlanGraph plan = planner.getBestSolution();
        Resource iAS = injectExecutionPlan(qvtpAsModel, plan);
        try {
            // We need a different uri for the sloppy
            String idUri = iAS.getURI().trimFileExtension().toString() + "_Sloppy";
            iAS.setURI(URI.createURI(idUri).appendFileExtension("qvtias"));
            iAS.save(savingOptions);
        } catch (IOException e2) {
            throw new QvtMtcExecutionException(e2.getMessage());
        }
        qvtiAsModels.add(iAS);
    }

    /**
     * Execute all.
     *
     * @param verbose the verbose
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public void compileAllSolutions(boolean verbose) throws QvtMtcExecutionException {

        logger.info("Running CE on " + qvtiUri.trimFileExtension().lastSegment());
        compileC2P();
        CompleteExploration ce = completeExploration(verbose);
        Iterator<ExecutionPlanGraph> it = ce.getFinalPlans().iterator();
        int id = 0;
        while (it.hasNext()) {
            ExecutionPlanGraph plan  = it.next();
            Resource iAS = injectExecutionPlan(qvtpAsModel, plan);
            try {
                // We need a different uri for each
                String idUri = iAS.getURI().trimFileExtension().toString() + "_" + plan.getCost() + "_" + id++;
                iAS.setURI(URI.createURI(idUri).appendFileExtension("qvtias"));
                iAS.save(savingOptions);
                qvtiAsModels.add(iAS);
            } catch (IOException e2) {
                throw new QvtMtcExecutionException(e2.getMessage());
            }
        }
        bestPlans = ce.getFinalPlans();
    }


    /**
     * Execute best.
     *
     * @param verbose the verbose
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public void compileBestSolutions(boolean verbose) throws QvtMtcExecutionException {

        compileC2P();
        CompleteExploration ce = completeExploration(verbose);
        int id = 0;
        bestPlans = ce.getBestSolutions();
        for(ExecutionPlanGraph plan : bestPlans) {
            Resource iAS = injectExecutionPlan(qvtpAsModel, plan);
            try {
                // We need a different uri for each
                String idUri = iAS.getURI().trimFileExtension().toString() + "_" + id;
                iAS.setURI(URI.createURI(idUri).appendFileExtension("qvtias"));
                iAS.save(savingOptions);
                qvtiAsModels.add(iAS);
                id++;
            } catch (IOException e2) {
                throw new QvtMtcExecutionException(e2.getMessage());
            }
        }
    }


    /**
     * Craete domain analysis.
     *
     * @return the map
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public Map<Element, DomainUsage> craeteDomainAnalysis() throws QvtMtcExecutionException {
        QVTimperativeDomainUsageAnalysis domainAnalysis = new QVTimperativeDomainUsageAnalysis((EnvironmentFactoryInternal) this.getEnvironmentFactory());
        Transformation asTransformation;
        try {
            asTransformation = MallardMtcUtil.getTransformation(qvtpAsModel);
        } catch (Exception e1) {
            throw new QvtMtcExecutionException(e1.getMessage());
        }
        Map<Element, DomainUsage> analysis = domainAnalysis.analyzeTransformation(asTransformation);
        return analysis;
    }


    /**
     * Dispose models.
     */
    public void disposeModels() {
        if (qvtcAsModel != null) {
            qvtcAsModel.unload();
        }
        if (qvtuAsModel != null) {
            qvtmAsModel.unload();
        }
        if (qvtuAsModel != null) {
            qvtuAsModel.unload();
        }
        if (qvtmAsModel != null) {
            qvtmAsModel.unload();
        }
        if (qvtpAsModel != null) {
            qvtpAsModel.unload();
        }
        for (Resource m : qvtiAsModels) {
            m.unload();
        }

    }


    /**
     * Gets the configuration.
     *
     * @param qvtias the qvtias
     * @return the configuration
     */
    public Configuration getConfiguration(@NonNull Resource qvtias) {
        return qvtiConfigurations.get(qvtias);
    }

    /**
     * Gets the cost.
     *
     * @param qvtias the qvtias
     * @return the cost
     */
    public double getCost(Resource qvtias) {
        return qvtiCosts.get(qvtias);
    }


    /**
     * Gets the dg.
     *
     * @return the dg
     */
    public DependencyGraph getDg() {
        return dg;
    }

    /**
     * Gets the environment factory.
     *
     * @return the environmentFactory
     */
    public @NonNull QVTiEnvironmentFactory getEnvironmentFactory() {
        return environmentFactory;
    }

    /**
     * Gets the final plans.
     *
     * @return the final plans
     */
    public Set<ExecutionPlanGraph> getFinalPlans() {
        return bestPlans;
    }

    /**
     * Gets the qvtc AS model.
     *
     * @return the qvtc AS model
     */
    public Resource getQvtcAsModel() {
        return qvtcAsModel;
    }

    /**
     * Get the one qvti AS model.
     *
     * @return the qvti as model
     */
    public Resource getQvtiAsModel() {

        return qvtiAsModels.get(0);
    }

    /**
     * Gets the qvti AS models.
     *
     * @return the qvti AS models
     */
    public List<Resource> getQvtiAsModels() {
        return qvtiAsModels;
    }

    /**
     * Gets the qvtm AS model.
     *
     * @return the qvtm AS model
     */
    public Resource getQvtmAsModel() {
        return qvtmAsModel;
    }



    /**
     * Gets the qvtp AS model.
     *
     * @return the qvtp AS model
     */
    public Resource getQvtpAsModel() {
        return qvtpAsModel;
    }


    /**
     * Gets the qvtu AS model.
     *
     * @return the qvtu AS model
     */
    public Resource getQvtuAsModel() {
        return qvtuAsModel;
    }

    /**
     * Initial configuration. Create the initial configuration using the QVTc AS model.
     *
     * @param mode the mode
     * @param direction the direction
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public void initialConfiguration(@NonNull Mode mode, String direction) throws QvtMtcExecutionException {
        config = createConfiguration(qvtcAsModel, mode, direction);
    }

    /**
     * C AS 2 u AS.
     *
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private void cAS2uAS() throws QvtMtcExecutionException {
        qvtuAsModel = createModelFromUri(qvtuUri);
        Copier copier = new Copier();
        for (EObject e : qvtcAsModel.getContents()) {
            CoreModel newE = (CoreModel) copier.copy(e);
            copier.copyReferences();
            newE.setExternalURI(((CoreModel) e).getExternalURI().replace(".qvtc", ".qvtu.qvtc"));
            newE.setName(((CoreModel) e).getName().replace(".qvtc", ".qvtu"));
            qvtuAsModel.getContents().add(newE);
        }
        // Update config before execution
        updateConfig(copier);
        QVTcToQVTu ctou = new QVTcToQVTu(getEnvironmentFactory(), config);
        for (EObject e : qvtuAsModel.getContents()) {
            ctou.execute((CoreModel) e);
        }
        try {
            qvtuAsModel.save(savingOptions);
        } catch (IOException e2) {
            throw new QvtMtcExecutionException(e2.getMessage());
        }
        logger.info("QVTcToQVTu Done!");
    }

    /**
     * Execute the MTC till qvtp. Then it can be decided if a single or multiple qvti solution is wanted
     *
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private void compileC2P() throws QvtMtcExecutionException {

        cAS2uAS();
        uAS2mAS();
        mAS2pAS();
    }

    /**
     * Complete exploration.
     *
     * @param verbose the verbose
     * @return the complete exploration
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private CompleteExploration completeExploration(boolean verbose) throws QvtMtcExecutionException {
        Map<Element, DomainUsage> analysis = craeteDomainAnalysis();
        CompleteExploration ce = new CompleteExploration(getDg(), analysis, verbose, environmentFactory);
        ce.createAllPlans();
        logger.info("Exploration Done!");
        return ce;
    }


    /**
     * Creates a resource from the given URI using the model manager from the environment factory.
     *
     * @param uri the uri
     * @return the resource
     */
    private Resource createModelFromUri(URI uri) {

        //URI uri = URI.createURI(path, false);
        ResourceSet rSet = getEnvironmentFactory().getMetamodelManager().getASResourceSet();
        Resource modelImpl = rSet.createResource(uri);
        return modelImpl;
    }


    /**
     * Inject execution plan.
     *
     * @param pModel the model
     * @param plan the plan
     * @return the resource
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private Resource injectExecutionPlan(Resource pModel, ExecutionPlanGraph plan) throws QvtMtcExecutionException {
        Resource iModel = createModelFromUri(qvtiUri);
        ExecutionPlanInjection inj = new ExecutionPlanInjection(plan, getEnvironmentFactory());
        for (EObject eo : pModel.getContents()) {
            ImperativeModel m = (ImperativeModel) ((Visitable) eo).accept(inj);
            iModel.getContents().add(m);
        }
        // Save the configuration
        Configuration thisConfig = new Configuration(config);
        updateConfig(thisConfig, inj.getCopier());
        qvtiConfigurations.put(iModel, thisConfig);
        qvtiCosts.put(iModel, plan.getCost());
        return iModel;
    }

    /**
     * QVTm to QVTp.
     *
     * @return the QVTp model
     * @throws QvtMtcExecutionException If there is a problem loading the models or
     * 	executing the ETL script.
     */
    private void mAS2pAS() throws QvtMtcExecutionException {

        qvtpAsModel = createModelFromUri(qvtpUri);
        QVTmToQVTip tx = new QVTmToQVTip(config, getEnvironmentFactory());
        tx.transform(qvtmAsModel, qvtpAsModel);
        updateConfig(tx.getTypedModelMapper());
        getEnvironmentFactory().getMetamodelManager().getASResourceSet().getResources().remove(qvtmAsModel);
        try {
            qvtpAsModel.save(savingOptions);
        } catch (IOException e2) {
            throw new QvtMtcExecutionException(e2.getMessage());
        }
        // Save the configuration
        Configuration thisConfig = new Configuration(config);
        qvtiConfigurations.put(qvtpAsModel, thisConfig);
        // And the cost
        qvtiCosts.put(getQvtpAsModel(), 0.0);
        dg = qvtpToDependencyGraph(qvtpAsModel);
        getDg().createMappingDerivations();
        logger.info("QVTmToQVTp Done!");
    }


    /**
     * QVTu to QVTm.
     *
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private void uAS2mAS() throws QvtMtcExecutionException {
        qvtmAsModel = createModelFromUri(qvtmUri);
        Copier copier = new Copier();
        for (EObject e : qvtuAsModel.getContents()) {
            CoreModel newE = (CoreModel) copier.copy(e);
            copier.copyReferences();
            newE.setExternalURI(((CoreModel) e).getExternalURI().replace(".qvtu.qvtc", ".qvtm.qvtc"));
            newE.setName(((CoreModel) e).getName().replace(".qvtu", ".qvtm"));
            qvtmAsModel.getContents().add(newE);
        }
        // Update config before execution
        updateConfig(copier);
        QVTuToQVTm utom = new QVTuToQVTm(getEnvironmentFactory());
        for (EObject e : qvtmAsModel.getContents()) {
            utom.execute((CoreModel) e);
        }
        try {
            qvtmAsModel.save(savingOptions);
        } catch (IOException e2) {
            throw new QvtMtcExecutionException(e2.getMessage());
        }
        logger.info("QVTuToQVTm Done!");
    }


    /**
     * After each step in the MTC, we need to make sure the configuration points to the TypedModels in next model in the
     * chain. The reason for this is that the TypedModels are copied from model to model.
     *
     * @param config the config
     * @param copier the copier
     */
    private void updateConfig(Configuration config, Map<? extends EObject, ? extends EObject> copier) {

        Set<@NonNull TypedModel> newInputDomains = new HashSet<TypedModel>();
        for (TypedModel tm : config.getInputDomains()) {
            EObject newTm = copier.get(tm);
            assert newTm instanceof TypedModel;
            newInputDomains.add((@NonNull TypedModel) newTm);
        }
        config.setInputDomains(newInputDomains);
        config.setMiddleDomain((@NonNull TypedModel) copier.get(config.getMiddleDomain()));
        config.setOutputDomain((@NonNull TypedModel) copier.get(config.getOutputDomain()));
    }

    /**
     * Helper wrapper.
     *
     * @param copier the copier
     * @see #updateConfig(Configuration, Map)
     */
    private void updateConfig(Map<? extends EObject, ? extends EObject> copier) {
        updateConfig(config, copier);
    }

    /**
     * QVTp to dependency graph.
     *
     * @param pModel the model
     * @return the dependency graph
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    protected DependencyGraph qvtpToDependencyGraph(Resource pModel) throws QvtMtcExecutionException {

        QVTiToDependencyGraph tx = new QVTiToDependencyGraph(config, getEnvironmentFactory());
        return tx.createDependencyGraph(pModel);
    }

}
