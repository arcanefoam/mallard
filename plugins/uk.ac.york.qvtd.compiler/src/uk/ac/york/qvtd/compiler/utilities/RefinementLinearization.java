/*******************************************************************************
 * Copyright (c) 2013, 2015 The University of York and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - initial API and implementation
 ******************************************************************************/
package uk.ac.york.qvtd.compiler.utilities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.qvtd.pivot.qvtcore.Mapping;

/**
 * The Class RefinementLinearization.
 * Based on https://github.com/ruediste/c3java
 */
public class RefinementLinearization {

	 HashMap<Mapping, List<Mapping>> results = new HashMap<Mapping, List<Mapping>>();

    /**
     * Thrown when its not possible to linearize all specifications.
     */
    public class QVTcC3Exception extends Error {

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 1L;
        private List<List<Mapping>> sequences;

        /**
         * Instantiates a new QV tc C 3 exception.
         * @param sequences
         *
         * @param partialResult the partial result
         * @param remainingInputs the remaining inputs
         */
        protected QVTcC3Exception(List<List<Mapping>> sequences) {
            super("inconsistent refinement");
            this.sequences = sequences;
        }


    }

    private List<Mapping> merge(List<List<Mapping>> sequences) {
        List<List<Mapping>> partialresult = new ArrayList<List<Mapping>>();
        for (List<Mapping> seq : sequences) {
            partialresult.add(new ArrayList<Mapping>(seq));
        }
        List<Mapping> result = new ArrayList<>();
        for(;;) {
            // Remove empty sequences
            partialresult = partialresult.stream().filter(seq ->!seq.isEmpty()).collect(Collectors.toList());
            if (partialresult.isEmpty()) {
                return result;
            }

            // Find the first clean head
            Mapping head = null;
            for (List<Mapping> seq :partialresult) {
                Mapping found = seq.get(0);
                 // If this is not a bad head (ie. not in any other sequence)...
                if (!partialresult.stream().anyMatch(s -> s.subList(1, s.size()).contains(found))) {
                    head = found;
                    break;
                }
            }
            if (head == null) {
                throw new QVTcC3Exception(partialresult);
            }
            result.add(head);
            Iterator<List<Mapping>> it = partialresult.iterator();
            while (it.hasNext()) {
                List<Mapping> seq = it.next();
                if (seq.get(0).equals(head)) {
                    seq.remove(0);
                }
            }
        }
    }

    public Map<Mapping, List<Mapping>> build_graph(Mapping m) {
        HashMap<Mapping, List<Mapping>> graph = new HashMap<Mapping, List<Mapping>>();
        add_to_graph(m, graph);
        return graph;
    }

    private void add_to_graph(Mapping m, HashMap<Mapping, List<Mapping>> graph) {
        if (!graph.containsKey(m)) {
            graph.put(m, m.getSpecification());
            for (Mapping x : graph.get(m)) {
                add_to_graph(x, graph);
            }
        }
    }

    public Map<Mapping, List<Mapping>> mapping_graph(Mapping m) {
        return build_graph(m);
    }


    public Map<Mapping, List<Mapping>> linearize(Map<Mapping, List<Mapping>> graph) {
        return linearize(graph, true);
    }

    public Map<Mapping, List<Mapping>> linearize(Map<Mapping, List<Mapping>> graph, boolean order) {
       
        Map<Mapping, List<Mapping>> g = graph.entrySet().stream()
                    .sorted(comparingByValueSize())
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            Map.Entry::getValue,
                            (u, v) -> {
                                throw new IllegalStateException(String.format("Duplicate key %s", u));
                            },
                            LinkedHashMap::new));

        for(Mapping head : g.keySet()) {
            linearize(head, g, order, results);
        }
        return results;

    }

    private List<Mapping> linearize(Mapping head, Map<Mapping, List<Mapping>> g, boolean order,
            HashMap<Mapping, List<Mapping>> results) {

        if (results.containsKey(head)) {
            return results.get(head);
        }
        List<List<Mapping>> sequences = new ArrayList<List<Mapping>>();
        sequences.add(new ArrayList<Mapping>(Arrays.asList(head)));
        for (Mapping x : g.get(head)) {
            sequences.add(linearize(x, g, order, results));
        }
        if(order) {
            sequences.add(g.get(head));
        }
        List<Mapping> res = merge(sequences);
        results.put(head, res);
        return res;
    }

    /**
     * Returns a comparator that compares {@link Map.Entry} in according to value size.
     *
     * @param <K> the type of the map keys
     * @param <V> the {@link Comparable} type of the map values
     * @return a comparator that compares {@link Map.Entry} in natural order on value.
     * @see Comparable
     * @since 1.8
     */
    private <K, T, V extends Comparable<? super List<T>>> Comparator<Map.Entry<K,List<T>>> comparingByValueSize() {
        return (Comparator<Map.Entry<K, List<T>>> & Serializable)
            (c1, c2) -> Integer.compare(c1.getValue().size(), c2.getValue().size());
    }

    /**
     * The class linearizations are cached in a static map. This cache can lead
     * to classloader leaks. By calling this class, the cache is flushed.
     */
    public void clearCache() {
        results.clear();
    }



    //==================== C3 Specification Linearization =====================//
//
//    /**
//       * The Class LinearizationKey.
//       */
//      private static class LinearizationKey {
//
//          /** The type. */
//        public Mapping type;
//
//        /**
//         * Instantiates a new linearization key.
//         *
//         * @param type the type
//         */
//        public LinearizationKey(Mapping type) {
//            this.type = type;
//        }
//
//        /* (non-Javadoc)
//         * @see java.lang.Object#hashCode()
//         */
//        @Override
//        public int hashCode() {
//            final int prime = 31;
//            int result = 1;
//            result = prime * result + ((type == null)
//                ? 0
//                : type.hashCode());
//            return result;
//        }
//
//        /* (non-Javadoc)
//         * @see java.lang.Object#equals(java.lang.Object)
//         */
//        @Override
//        public boolean equals(Object obj) {
//            if (this == obj)
//                return true;
//            if (obj == null)
//                return false;
//            if (getClass() != obj.getClass())
//                return false;
//            LinearizationKey other = (LinearizationKey) obj;
//            if (type == null) {
//                if (other.type != null)
//                    return false;
//            } else if (!type.equals(other.type))
//                return false;
//            return true;
//        }
//
//    }
//
//    /** The linearizations. */
//    private Map<LinearizationKey, Iterable<Mapping>> linearizations = java.util.Collections
//            .synchronizedMap(new WeakHashMap<LinearizationKey, Iterable<Mapping>>());
//
//    /** The c 3 root. */
//    private Deque<Mapping> c3Root = new LinkedList<>();
//

//
//    /**
//     * Merge lists.
//     *
//     * @param partialResult the partial result
//     * @param remainingInputs the remaining inputs
//     * @return the iterable
//     * @throws QVTcC3Exception the QV tc C 3 exception
//     */
//    @SuppressWarnings("null")
//	private Iterable<Mapping> mergeLists(List<@NonNull Mapping> partialResult,
//            List<@NonNull List<@NonNull Mapping>> remainingInputs/*, final DirectSuperclassesInspector directParentClassesReader*/)
//            throws QVTcC3Exception {
//
//        if (all(remainingInputs, equalTo(Collections.<@NonNull Mapping> emptyList()))) {
//            return partialResult;
//        }
//
//        Optional<Mapping> nextOption = Optional.absent();
//        for (Mapping c : Lists.reverse(partialResult)) {
//            nextOption = Iterables.tryFind(c.getSpecification(),
//                    isCandidate(remainingInputs));
//            if (nextOption.isPresent())
//                break;
//        }
//
//        if (nextOption.isPresent()) {
//            List<List<Mapping>> newRemainingInputs = Lists.newArrayList();
//            Mapping next = nextOption.get();
//            for (List<Mapping> input : remainingInputs) {
//                newRemainingInputs.add(input.indexOf(next) == 0 ? input.subList(1, input.size()) : input);
//            }
//
//            return mergeLists(newArrayList(concat(partialResult, singletonList(next))), newRemainingInputs);
//        } else {
//            throw new QVTcC3Exception(partialResult, remainingInputs);
//        }
//    }
//
//    /**
//     * To be a candidate for the next place in the linearization, you must be
//     * the head of at least one list, and in the tail of none of the lists.
//     *
//     * @param <X> the generic type
//     * @param remainingInputs            the lists we are looking for position in.
//     * @return true if the class is a candidate for next.
//     */
//    private <X> Predicate<X> isCandidate(final Iterable<List<X>> remainingInputs) {
//        return new Predicate<X>() {
//
//            Predicate<List<X>> headIs(final X c) {
//                return new Predicate<List<X>>() {
//                    @Override
//                    public boolean apply(List<X> input) {
//                        return !input.isEmpty() && c.equals(input.get(0));
//                    }
//                };
//            }
//
//            Predicate<List<X>> tailContains(final X c) {
//                return new Predicate<List<X>>() {
//                    @Override
//                    public boolean apply(List<X> input) {
//                        return input.indexOf(c) > 0;
//                    }
//                };
//            }
//
//            @Override
//            public boolean apply(final X c) {
//                return any(remainingInputs, headIs(c)) && all(remainingInputs, not(tailContains(c)));
//            }
//        };
//    }
//
//    /**
//     * Compute class linearization.
//     *
//     * @param m the c
//     * @return the iterable
//     * @throws QVTcC3Exception the QV tc C 3 exception
//     */
//    private Iterable<Mapping> computeMappingLinearization(@NonNull Mapping m) throws QVTcC3Exception {
//
//        List<Mapping> mDirectSpecifications = new ArrayList<Mapping>(m.getSpecification());
//        //mDirectSpecifications.removeAll(c3Root);		// Avoid  loops
//        Function<Mapping, List<Mapping>> mplList = new Function<Mapping, List<Mapping>>() {
//            @Override
//            public List<Mapping> apply(Mapping c) {
//                return newArrayList(c.getSpecification());
//            }
//        };
//
//        List<@NonNull Mapping> singletonM = Collections.<@NonNull Mapping> singletonList(m);
//        List<@NonNull List<Mapping>> superContainers = Lists.transform(mDirectSpecifications, mplList);
//        return mergeLists(singletonM,
//                newArrayList(concat(superContainers, singletonList(mDirectSpecifications))));
//    }
//
//    /**
//     * Return the linearization of m. The returned iterable will finish in m, preceded by the specifications of m in
//     * revrese linearization order.
//     *
//     * @param m the Mapping
//     * @return the specification linearization for the mapping
//     * @throws QVTcC3Exception if there is an issue with the refinement hierarchy
//     */
//    public Collection<Mapping> allSpecificationsReverse(Mapping m) throws QVTcC3Exception {
//        ArrayList<Mapping> result = new ArrayList<>();
//        allSpecifications(m).forEach(result::add);
//        Collections.reverse(result);
//        return result;
//    }
//
//    /**
//     * Return the linearization of c. The returned iterable will start with m, followed by the specifications of m in
//     * linearization order.
//     *
//     * @param m the Mapping
//     * @return the specification linearization for the mapping
//     * @throws QVTcC3Exception if there is an issue with the refinement hierarchy
//     */
//    public Iterable<Mapping> allSpecifications(Mapping m)
//            throws QVTcC3Exception {
//
//        c3Root.push(m);
//        LinearizationKey key = new LinearizationKey(m);
//        Iterable<Mapping> linearization = linearizations.get(key);
//        if (linearization == null) {
//            linearization = computeMappingLinearization(m);
//            linearizations.put(key, linearization);
//        }
//        c3Root.pop();
//        return linearization;
//    }
//


}
