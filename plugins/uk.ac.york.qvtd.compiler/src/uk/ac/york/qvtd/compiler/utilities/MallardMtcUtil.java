/*******************************************************************************
 * Copyright (c) 2013, 2015 The University of York, Willink Transformations and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - initial API and implementation
 ******************************************************************************/
package uk.ac.york.qvtd.compiler.utilities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.ocl.pivot.CallExp;
import org.eclipse.ocl.pivot.CollectionLiteralExp;
import org.eclipse.ocl.pivot.CollectionType;
import org.eclipse.ocl.pivot.Element;
import org.eclipse.ocl.pivot.IfExp;
import org.eclipse.ocl.pivot.IteratorExp;
import org.eclipse.ocl.pivot.LiteralExp;
import org.eclipse.ocl.pivot.Model;
import org.eclipse.ocl.pivot.NullLiteralExp;
import org.eclipse.ocl.pivot.OCLExpression;
import org.eclipse.ocl.pivot.Operation;
import org.eclipse.ocl.pivot.OperationCallExp;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.ocl.pivot.VariableExp;
import org.eclipse.ocl.pivot.ids.OperationId;
import org.eclipse.ocl.pivot.internal.complete.StandardLibraryInternal;
import org.eclipse.ocl.pivot.internal.manager.PivotMetamodelManager;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.ocl.pivot.utilities.NameUtil;
import org.eclipse.ocl.pivot.utilities.ParserException;
import org.eclipse.ocl.pivot.utilities.PivotUtil;
import org.eclipse.ocl.xtext.base.utilities.BaseCSResource;
import org.eclipse.qvtd.pivot.qvtbase.BaseModel;
import org.eclipse.qvtd.pivot.qvtbase.Domain;
import org.eclipse.qvtd.pivot.qvtbase.Predicate;
import org.eclipse.qvtd.pivot.qvtbase.Transformation;
import org.eclipse.qvtd.pivot.qvtcore.Mapping;
import org.eclipse.qvtd.pivot.qvtcorebase.Area;
import org.eclipse.qvtd.pivot.qvtcorebase.Assignment;
import org.eclipse.qvtd.pivot.qvtcorebase.BottomPattern;
import org.eclipse.qvtd.pivot.qvtcorebase.CoreDomain;
import org.eclipse.qvtd.pivot.qvtcorebase.CorePattern;
import org.eclipse.qvtd.pivot.qvtcorebase.PropertyAssignment;
import org.eclipse.qvtd.pivot.qvtcorebase.RealizedVariable;
import org.eclipse.qvtd.pivot.qvtcorebase.VariableAssignment;

import uk.ac.york.qvtd.compiler.Configuration;


/**
 * The MtcUtil Class.
 */
public class MallardMtcUtil {


    public static @NonNull Operation getEqualsOperation(@NonNull EnvironmentFactory environmentFactory) {
        StandardLibraryInternal standardLibrary = (StandardLibraryInternal)environmentFactory.getStandardLibrary();
        org.eclipse.ocl.pivot.Class oclAnyType = standardLibrary.getOclAnyType();
        Operation operation1 = NameUtil.getNameable(oclAnyType.getOwnedOperations(), "=");
        assert operation1 != null;
        OperationId oclAnyEqualsId = operation1.getOperationId();
        return environmentFactory.getIdResolver().getOperation(oclAnyEqualsId);
    }

    /**
     * Create an ocl expression from an assignment than can be used as the
     * condition expression of a predicate.
     *
     * @param aIn the a in
     * @param environmentFactory the environment factory
     * @return the operation call exp
     */
    public static OperationCallExp assignmentToOclExp(@NonNull Assignment aIn, @NonNull EnvironmentFactory environmentFactory) {
        Operation equalOp = getEqualsOperation(environmentFactory);
        OCLExpression sourceExp;
        if (aIn instanceof PropertyAssignment) {
            @NonNull OCLExpression propSource = EcoreUtil.copy(((PropertyAssignment) aIn).getSlotExpression());
            sourceExp = PivotUtil.createNavigationCallExp(propSource, ((PropertyAssignment) aIn).getTargetProperty());
        } else { // aIn instanceof VariableAssignment
            sourceExp = PivotUtil.createVariableExp(((VariableAssignment) aIn).getTargetVariable());
        }
        return PivotUtil.createOperationCallExp(sourceExp, equalOp, EcoreUtil.copy(aIn.getValue()));
    }

    /**
     * Find all expressions of the given expression type nested in the OCL
     * expression.
     *
     * @param <T> the generic type
     * @param exp the exp
     * @param type the type
     * @return the sets the
     */
    public static <T> Set<T> findExpsOfType(OCLExpression exp, java.lang.Class<T> type) {
        Set<T> expressions = new HashSet<T>();
        if (type.isInstance(exp)) {
            expressions.add(type.cast(exp));
        }
        TreeIterator<EObject> it = exp.eAllContents();
        while (it.hasNext()) {
            EObject object = it.next();
            if (type.isInstance(object)) {
                expressions.add(type.cast(object));
            }
        }
        return expressions;
    }

    /**
     * Find all operation call expressions nested in the assigment.
     *
     * @param a the a
     * @return the sets the
     */
    public static Set<OperationCallExp> findOpCallExps(Assignment a) {
        Set<OperationCallExp> exps = MallardMtcUtil.findExpsOfType(a.getValue(), OperationCallExp.class);
        if (a instanceof PropertyAssignment) {
            exps.addAll(MallardMtcUtil.findExpsOfType(((PropertyAssignment) a).getSlotExpression(), OperationCallExp.class));
        }
        return exps;
    }

    /**
     * Find all operation call expressions nested in the predicate.
     *
     * @param p the p
     * @return the sets the
     */
    public static Set<OperationCallExp> findOpCallExps(Predicate p) {
        Set<OperationCallExp> exps = MallardMtcUtil.findExpsOfType(p.getConditionExpression(), OperationCallExp.class);
        return exps;
    }

    /**
     * Find referenced variables in the assignment.
     *
     * @param ass the assignment
     * @return the sets the
     */
    public static Set<Variable> findReferencedVariables(Assignment ass) {

        Set<Variable> vars = findReferencedVariables(ass.getValue());
        if (ass instanceof PropertyAssignment) {
            vars.addAll(findReferencedVariables(((PropertyAssignment) ass).getSlotExpression()));
        } else if (ass instanceof VariableAssignment) {
            vars.add(((VariableAssignment) ass).getTargetVariable());
        }
        return vars;
    }

    /**
     * Find referenced variables in the OCL expression.
     *
     * @param exp the exp
     * @return the sets the
     */
    public static Set<Variable> findReferencedVariables(OCLExpression exp) {

        return findReferencedVariables(exp, null);
    }


    /**
     * Find referenced variables in the OCL expression, excluding the given
     * variables.
     *
     * @param exp the exp
     * @param exclude the excluded variables
     * @return the sets the
     */
    public static Set<Variable> findReferencedVariables(OCLExpression exp, List<Variable> exclude) {
        Set<Variable> vars = new HashSet<Variable>();
        for (VariableExp varexp : MallardMtcUtil.findVariableExps(exp)) {
            if (!(varexp.getReferredVariable().eContainer() instanceof IteratorExp)) {
                vars.add((Variable) varexp.getReferredVariable());
            }
        }
        return vars;
    }

    /**
     * Find variable expressions in the Assignment.
     *
     * @param ass the ass
     * @return the sets the
     */
    public static Set<VariableExp> findVariableExps(Assignment ass) {

        Set<VariableExp> exps = MallardMtcUtil.findVariableExps(ass.getValue());
        if (ass instanceof PropertyAssignment) {
            exps.addAll(MallardMtcUtil.findVariableExps(((PropertyAssignment) ass).getSlotExpression()));
        }
        return exps;
    }

    /**
     * Find all variable expressions nested in the OCL expression.
     *
     * @param exp the exp
     * @return the sets the
     */
    public static Set<VariableExp> findVariableExps(OCLExpression exp) {
        Set<VariableExp> exps = findExpsOfType(exp, VariableExp.class);
        return exps;
    }

    /**
     * Find variable expressions in the Predicate.
     *
     * @param pred the pred
     * @return the sets the
     */
    public static Set<VariableExp> findVariableExps(Predicate pred) {

        return findVariableExps(pred.getConditionExpression());
    }

    /**
     * Fix references to variables in the Area
     *
     * @param source the source
     * @param refinedVars the refined vars
     */
    public static void fixReferences(Area source, Map<Variable, Variable> refinedVars) {
        for (Variable gv : source.getGuardPattern().getVariable()) {
            if (gv.getOwnedInit() != null)
                fixReferences(gv.getOwnedInit(), refinedVars);
        }
        for (Variable bv : source.getBottomPattern().getVariable()) {
            if (bv.getOwnedInit() != null)
                fixReferences(bv.getOwnedInit(), refinedVars);
        }
        for (RealizedVariable brv : source.getBottomPattern().getRealizedVariable()) {
            if (brv.getOwnedInit() != null)
                fixReferences(brv.getOwnedInit(), refinedVars);
        }
        // Copy all predicates and assignments, might generate duplicates?
        for (Predicate p : source.getGuardPattern().getPredicate()) {
            fixReferences(p, refinedVars);
        }
        for (Predicate p : source.getBottomPattern().getPredicate()) {
            fixReferences(p, refinedVars);
        }
        for (Assignment a : source.getBottomPattern().getAssignment()) {
            fixReferences(a, refinedVars);
        }

    }

    /**
     * Fix references in the assignments
     *
     * @param a the a
     * @param refinedVars the refined vars
     */
    public static void fixReferences(Assignment a, Map<Variable, Variable> refinedVars) {

        fixReferences(a.getValue(), refinedVars);
        if (a instanceof PropertyAssignment) {
            fixReferences(((PropertyAssignment) a).getSlotExpression(), refinedVars);
        } else if (a instanceof VariableAssignment) {
            if (refinedVars.containsKey(((VariableAssignment) a).getTargetVariable())) {
                ((VariableAssignment) a).setTargetVariable(refinedVars.get(((VariableAssignment) a).getTargetVariable()));
            }
        }

    }

    /**
     * Fix references in the OCL expression
     *
     * @param exp the exp
     * @param refinedVars the refined vars
     */
    public static void fixReferences(OCLExpression exp, Map<Variable, Variable> refinedVars) {
        for (VariableExp vExp : MallardMtcUtil.findVariableExps(exp)) {
            fixVariableReferences(vExp, refinedVars);
            assert vExp.getReferredVariable() != null;
        }

    }

    /**
     * Fix references in the predicate
     *
     * @param p the p
     * @param refinedVars the refined vars
     */
    public static void fixReferences(Predicate p, Map<Variable, Variable> refinedVars) {
        fixReferences(p.getConditionExpression(), refinedVars);

    }

    /**
     * Fix variable references.
     *
     * @param varexp the varexp
     * @param refinedVars the refined vars
     */
    public static void fixVariableReferences(VariableExp varexp, Map<Variable, Variable> refinedVars) {

        Variable varIn = (Variable) varexp.getReferredVariable();
        if (varIn.eContainer() instanceof CorePattern) {
            if (refinedVars.containsKey(varIn))
                varexp.setReferredVariable(refinedVars.get(varIn));
        }
    }

    /**
     * Gets the all property assignments in the Area
     *
     * @param a the a
     * @return the all property assignments
     */
    public static List<Predicate> getAllPredicates(Area a) {
        List<Predicate> result = new ArrayList<Predicate>();
        result.addAll(a.getGuardPattern().getPredicate());
        result.addAll(a.getBottomPattern().getPredicate());
        return result;
    }

    /**
     * Get all property assignments in the Mapping.
     *
     * @param m the m
     * @return the all property assignments
     */
    public static List<Predicate> getAllPredicates(Mapping m) {

        List<Predicate> result = new ArrayList<Predicate>();
        for (Domain d : m.getDomain()) {
            result.addAll(getAllPredicates((CoreDomain) d));
        }
        result.addAll(getAllPredicates((Area) m));
        return result;
    }

    /**
     * Gets the all property assignments in the Area
     *
     * @param a the a
     * @return the all property assignments
     */
    public static List<PropertyAssignment> getAllPropertyAssignments(Area a) {
        List<PropertyAssignment> result = new ArrayList<PropertyAssignment>();
        result.addAll(a.getBottomPattern().getAssignment()
                .stream()
                .filter(assg -> assg instanceof PropertyAssignment)
                .map(assg -> (PropertyAssignment) assg)
                .collect(Collectors.toList()));
        return result;
    }

    /**
     * Get all property assignments in the mapping.
     *
     * @param m the m
     * @return the all property assignments
     */
    public static List<PropertyAssignment> getAllPropertyAssignments(Mapping m) {

        List<PropertyAssignment> result = new ArrayList<PropertyAssignment>();
        for (Domain d : m.getDomain()) {
            result.addAll(getAllPropertyAssignments((CoreDomain) d));
        }
        result.addAll(getAllPropertyAssignments((Area) m));
        return result;
    }

    /**
     * Gets the all variable assignments.
     *
     * @param a the a
     * @return the all variable assignments
     */
    public static List<VariableAssignment> getAllVariableAssignments(Area a) {
        List<VariableAssignment> result = new ArrayList<VariableAssignment>();
        result.addAll(a.getBottomPattern().getAssignment()
                .stream()
                .filter(assg -> assg instanceof VariableAssignment)
                .map(assg -> (VariableAssignment) assg)
                .collect(Collectors.toList()));
        return result;
    }

    /**
     * Gets the all variable assignments.
     *
     * @param m the m
     * @return the all variable assignments
     */
    public static List<VariableAssignment> getAllVariableAssignments(Mapping m) {
        List<VariableAssignment> result = new ArrayList<VariableAssignment>();
        for (Domain d : m.getDomain()) {
            result.addAll(getAllVariableAssignments((CoreDomain) d));
        }
        result.addAll(getAllVariableAssignments((Area) m));
        return result;
    }

    /**
     * Gets the area.
     *
     * @param exp the exp
     * @return the area
     */
    public static Area getArea(OCLExpression exp) {
        if (exp instanceof VariableExp) {
            Variable expV = (Variable) ((VariableExp)exp).getReferredVariable();
            return getArea(expV);
        } else if (exp instanceof CallExp) {
            return getArea(((CallExp) exp).getOwnedSource());
        } else if (exp instanceof IfExp) {
            return getArea(((IfExp) exp).getOwnedCondition());
        } else if (exp instanceof LiteralExp) {
            return null;
        }
        return null;
    }

    /**
     * Gets the area.
     *
     * @param expV the exp v
     * @return the area
     */
    public static Area getArea(Variable expV) {
        if (expV.getType() != null) {
            CorePattern cp = oppositePattern(expV);
            if (cp!= null)
                return cp.getArea();
        }
        return null;
    }

    /**
     * Checks if is check mode.
     *
     * @param config the config
     * @return true, if is check mode
     */
    public static boolean isCheckMode(Configuration config) {
        return config.isCheckMode();
    }

    public static boolean isEnforceMode(Configuration config) {
        return config.isEnforceMode();
    }

    /**
     * Checks if is from input domain.
     *
     * @param exp the exp
     * @param config the config
     * @return true, if is from input domain
     */
    public static boolean isFromInputDomain(OCLExpression exp, Configuration config) {
        return isInputDomain(getArea(exp), config);
    }

    /**
     * Checks if is from middle domain.
     *
     * @param exp the exp
     * @param config the config
     * @return true, if is from middle domain
     */
    public static boolean isFromMiddleDomain(OCLExpression exp, Configuration config) {
        return isMiddleDomain(getArea(exp), config);
    }

    /**
     * Checks if is from middle domain.
     *
     * @param exp the exp
     * @param config the config
     * @return true, if is from middle domain
     */
    public static boolean isFromOutputDomain(OCLExpression exp, Configuration config) {
        return isOutputDomain(getArea(exp), config);
    }


    /**
     * Checks if is input domain.
     *
     * @param area the area
     * @param config the config
     * @return true, if is input domain
     */
    public static boolean isInputDomain(Area area, Configuration config) {

        return (area instanceof CoreDomain) &&
                config.isInput(((CoreDomain) area).getTypedModel());
    }

    /**
     * Is a local variable to M .
     *
     * @param a the a
     * @param config the config
     * @return true, if is localto m
     */
    public static boolean isLocaltoM(PropertyAssignment a, Configuration config) {
        if (isFromMiddleDomain(a.getValue(), config) &&
                (a.getValue() instanceof VariableExp)) {
            Variable var = (Variable) ((VariableExp)a.getValue()).getReferredVariable();
            CorePattern cp = oppositePattern(var);		// Make sure the variable is in a bottom pattern
            // and that the slot expression is in the middle
            boolean mid = isFromMiddleDomain(a.getSlotExpression(), config);
            return mid && (cp instanceof BottomPattern);
        }
        return false;

    }

    /**
     * Checks if is middle assignment.
     *
     * @param aIn the assignment
     * @return true, if is middle assignment
     */
    public static boolean isMiddleAssignment(Assignment aIn) {
        boolean use = true;
        BottomPattern cp = aIn.getBottomPattern();
        if (cp.getArea() instanceof Mapping) {
            for (Variable var : findReferencedVariables(aIn)) {
                Area area = getArea(var);
                if (area instanceof CoreDomain) {
                    use = false;
                    break;
                }
            }

        } else {
            use = false;
        }
        return use;
    }

    /**
     * Checks if is middle domain.
     *
     * @param area the area
     * @return true, if is middle domain
     */
    public static boolean isMiddleDomain(Area area, Configuration config) {

        if (area instanceof CoreDomain) {
            return config.isMiddle(((Domain) area).getTypedModel());
        }
        else if (area instanceof Mapping) {
            return true;
        }
        return false;
    }

    /**
     * Is a Middle to Left assignment.
     *
     * @param a the a
     * @param config the config
     * @return true, if is mto l
     */
    public static boolean isMtoL(PropertyAssignment a, Configuration config) {
        return isFromInputDomain(a.getSlotExpression(), config) &&
                findReferencedVariables(a.getValue()).stream()
                    .anyMatch(v -> isMiddleDomain(getArea(v), config));

    }

    /**
     * Checks if is middle to left.
     *
     * @param a the a
     * @param config the config
     * @return true, if is mto l
     */
    public static boolean isMtoL(VariableAssignment a, Configuration config) {
        return isInputDomain(getArea(a.getTargetVariable()), config) &&
                findReferencedVariables(a.getValue()).stream()
                    .anyMatch(v -> isMiddleDomain(getArea(v), config));

    }

    /**
     * Checks if is middle to middle.
     *
     * @param a the a
     * @param config the config
     * @return true, if is mto m
     */
    public static boolean isMtoM(VariableAssignment a, Configuration config) {
        return isMiddleDomain(getArea(a.getTargetVariable()), config) &&
                findReferencedVariables(a.getValue()).stream()
                    .anyMatch(v -> isMiddleDomain(getArea(v), config) &&
                            (oppositePattern(v) instanceof BottomPattern));
    }

    /**
     * Checks if is output domain.
     * @param config the config
     * @param domain the domain
     *
     * @return true, if is output domain
     */
    public static boolean isOutputDomain(Area area, Configuration config) {
        return (area instanceof CoreDomain) && config.isOutput(((Domain) area).getTypedModel());
    }

    /**
     * Is a Right to Middle assignment.
     *
     * @param a the a
     * @param config the config
     * @return true, if is rto m
     */
    public static boolean isRtoM(PropertyAssignment a, Configuration config) {
        boolean result = isFromMiddleDomain(a.getSlotExpression(), config);
        if (!result) {
            return false;
        }
        if (a.getValue() instanceof VariableExp) {
            return false;
        }
        if (a.getValue() instanceof NullLiteralExp) {
            return false;
        }
        if (a.getValue() instanceof CollectionLiteralExp) {
            return false;
        }
        return findReferencedVariables(a.getValue()).stream()
                .allMatch(v -> isOutputDomain(getArea(v), config));

    }

    /**
     * Checks if is rto m.
     *
     * @param a the a
     * @param config the config
     * @return true, if is rto m
     */
    public static boolean isRtoM(VariableAssignment a, Configuration config) {
        return isMiddleDomain(getArea(a.getTargetVariable()), config) &&
                !(a.getValue() instanceof VariableExp) &&
                // TODO variable assignments to null? !(a.getValue() instanceof NullLiteralExp) &&
                findReferencedVariables(a.getValue()).stream()
                    .allMatch(v -> isOutputDomain(getArea(v), config));
    }

    /**
     * Load model from uri.
     *
     * @param uri the uri
     * @param environmentFactory
     * @return the resource
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public static @Nullable Resource loadASModelFromUri(URI uri, EnvironmentFactory environmentFactory) throws QvtMtcExecutionException {

        //Injector injector = new QVTcoreStandaloneSetup().createInjectorAndDoEMFRegistration();
        //XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
        ResourceSet rSet = environmentFactory.getMetamodelManager().getASResourceSet();
        Resource asModel = rSet.getResource(uri, true);
        //EcoreUtil.resolveAll(csModel);
        if (asModel instanceof BaseCSResource) {
            asModel = ((BaseCSResource)asModel).getASResource();
        }
        EcoreUtil.resolveAll(asModel);
        return asModel;
    }

    public static Model loadMetamodelFromUri(URI uri, EnvironmentFactory environmentFactory) throws QvtMtcExecutionException {

        ResourceSet rSet = environmentFactory.getMetamodelManager().getASResourceSet();
        // The metamodel should already have been loaded when the qvtcas was loaded. Can we find it?
        Model modelImpl = null;
        try {
            @Nullable
            Element e = ((PivotMetamodelManager)environmentFactory.getMetamodelManager()).loadResource(uri, "", rSet);
            if (e != null) {
                assert e instanceof Model;
                modelImpl = (Model) e;
            }
        } catch (ParserException e) {
            throw new QvtMtcExecutionException(e.getMessage());
        }
        return modelImpl;
    }

    /**
     * Load model from uri.
     *
     * @param uri the uri
     * @param environmentFactory
     * @return the resource
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public static Resource createModelFromUri(URI uri, EnvironmentFactory environmentFactory) throws QvtMtcExecutionException {

        ResourceSet rSet = environmentFactory.getMetamodelManager().getASResourceSet();
        Resource modelImpl = rSet.createResource(uri);
        return modelImpl;
    }

    /**
     * Opposite pattern.
     *
     * @param expV the exp v
     * @return the core pattern
     */
    public static CorePattern oppositePattern(Variable expV) {
        if (expV.eContainer() instanceof CorePattern)
            return (CorePattern) expV.eContainer();
        return null;
    }

    /**
     * Searches for known src folders: /src/, /*src/, and replace them for /bin/
     * so all generated models/files are saved on the bin folder. The src folder
     * is assumed to be the first one, i.e. nested src folders will be unaffected
     * @param resourcePath
     * @return
     */
    public static String changeTargetToBinFolder(String resourcePath) {

        String result;
        Pattern p = Pattern.compile("/\\w*src/");
        Matcher m = p.matcher(resourcePath);
        result = m.replaceFirst("/bin/");
        return result;
    }

    /**
     * Gets the QVTc-i transformation from the given resource. It will return the first
     * transformation object in the models.
     *
     * @param resource the resource
     * @return The transformation
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public static @NonNull Transformation getTransformation(Resource resource) throws QvtMtcExecutionException {
        for (EObject eContent : resource.getContents()) {
            if (eContent instanceof BaseModel) {
                for (org.eclipse.ocl.pivot.Package aPackage : ((BaseModel)eContent).getOwnedPackages()) {
                    for (org.eclipse.ocl.pivot.Class aClass : aPackage.getOwnedClasses()) {
                        if (aClass instanceof Transformation) {
                            return (Transformation) aClass;
                        }
                    }
                }
            }
        }
        throw new QvtMtcExecutionException("The QVTd model does not have a Transformation element.");
    }

}
