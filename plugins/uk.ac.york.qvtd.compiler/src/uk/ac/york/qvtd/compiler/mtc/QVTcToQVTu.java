/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.mtc;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.PivotFactory;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.qvtd.pivot.qvtbase.Domain;
import org.eclipse.qvtd.pivot.qvtbase.Predicate;
import org.eclipse.qvtd.pivot.qvtbase.QVTbaseFactory;
import org.eclipse.qvtd.pivot.qvtbase.Rule;
import org.eclipse.qvtd.pivot.qvtbase.Transformation;
import org.eclipse.qvtd.pivot.qvtcore.CoreModel;
import org.eclipse.qvtd.pivot.qvtcore.Mapping;
import org.eclipse.qvtd.pivot.qvtcorebase.Area;
import org.eclipse.qvtd.pivot.qvtcorebase.BottomPattern;
import org.eclipse.qvtd.pivot.qvtcorebase.CorePattern;
import org.eclipse.qvtd.pivot.qvtcorebase.PropertyAssignment;
import org.eclipse.qvtd.pivot.qvtcorebase.RealizedVariable;
import org.eclipse.qvtd.pivot.qvtcorebase.VariableAssignment;

import uk.ac.york.qvtd.compiler.Configuration;
import uk.ac.york.qvtd.compiler.utilities.MallardMtcUtil;

/**
 * The Class QVTcToQVTu.
 */
public class QVTcToQVTu {

    /** The environment factory. */
    private final @NonNull EnvironmentFactory environmentFactory;

    /** The config. */
    private Configuration config;

    /** The refined vars. */
    Map<Variable, Variable> refinedVars = new HashMap<Variable, Variable>();


    /**
     * Instantiates a new QV tc to qv tu.
     *
     * @param environmentFactory the environment factory
     * @param config the config
     */
    public QVTcToQVTu(@NonNull EnvironmentFactory environmentFactory, Configuration config) {
        super();
        this.environmentFactory = environmentFactory;
        this.config = config;
    }

    /**
     * Execute.
     *
     * @param model the model
     */
    public void execute(CoreModel model) {

        for (org.eclipse.ocl.pivot.Package p : model.getOwnedPackages()) {
            for (Class c : p.getOwnedClasses()) {
                if (c instanceof Transformation) {
                    direct((Transformation) c);
                }
            }
        }
    }

    /**
     * Change realized to variable.
     *
     * @param m the m
     */
    private void changeRealizedToVariable(Mapping m) {

        for (Domain d : m.getDomain()) {
            if (MallardMtcUtil.isInputDomain((Area) d, config)) {
                for (RealizedVariable rv : ((Area) d).getBottomPattern().getRealizedVariable()) {
                    //References to rv must be fixed
                    Variable v = PivotFactory.eINSTANCE.createVariable();
                    v.setName(rv.getName());
                    v.setType(rv.getType());
                    ((Area) d).getBottomPattern().getVariable().add(v);
                    refinedVars.put(rv, v);
                }
            }
        }
        for (Mapping lm : m.getLocal()) {
            changeRealizedToVariable(lm);
        }
    }

    /**
     * Direct.
     *
     * @param m the m
     */
    private void direct(Mapping m) {
        // Delete Assignments
        for (PropertyAssignment a : MallardMtcUtil.getAllPropertyAssignments(m)) {
            if (MallardMtcUtil.isMtoL(a, config) ||
                    MallardMtcUtil.isRtoM(a, config) ||
                    MallardMtcUtil.isLocaltoM(a, config)) {
                EcoreUtil.delete(a, true);
            } else if (MallardMtcUtil.isFromInputDomain(a.getSlotExpression(), config) &&
                    MallardMtcUtil.findReferencedVariables(a).stream()
                    .allMatch(v -> MallardMtcUtil.isInputDomain(MallardMtcUtil.getArea(v), config))) {
                // Assignments to Predicates, only if not default
                if (!a.isIsDefault()) {
                    Predicate pOUt = QVTbaseFactory.eINSTANCE.createPredicate();
                    pOUt.setConditionExpression(MallardMtcUtil.assignmentToOclExp(a, environmentFactory));
                    CorePattern cp = (CorePattern) a.eContainer();
                    cp.getPredicate().add(pOUt);
                }
                EcoreUtil.delete(a, true);
            } else if(MallardMtcUtil.isCheckMode(config) &&
                    a.isIsDefault() &&
                    MallardMtcUtil.isOutputDomain(a.getBottomPattern().getArea(), config)
                    ) {
                // Default assignments
                //a.setIsDefault(false);
            }
        }
        for (VariableAssignment a : MallardMtcUtil.getAllVariableAssignments(m)) {
            if (MallardMtcUtil.isRtoM(a, config) || 	// MallardMtcUtil.isMtoL(a, config) keep local L variables
                    MallardMtcUtil.isMtoM(a, config)) {
                EcoreUtil.delete(a, true);
            }
        }
        for (Predicate p : MallardMtcUtil.getAllPredicates(m)) {
            if (p.getPattern() instanceof BottomPattern &&
                    MallardMtcUtil.findReferencedVariables(p.getConditionExpression())
                        .stream()
                        .allMatch(v -> MallardMtcUtil.isOutputDomain(MallardMtcUtil.getArea(v), config))) {
                EcoreUtil.delete(p, true);
            }
        }
        for (Domain d : m.getDomain()) {
            if (MallardMtcUtil.isInputDomain((Area) d, config)) {
                d.setIsEnforceable(false);
                d.setIsCheckable(true);
            } else {
                if (MallardMtcUtil.isCheckMode(config)) {
                    d.setIsEnforceable(false);
                } else if (MallardMtcUtil.isEnforceMode(config)) {
                    d.setIsCheckable(false);
                }
            }
        }
        for (Mapping lm : m.getLocal()) {
            direct(lm);
        }
    }

    /**
     * Using the desired direction:
     * 	Remove assignments in the unwanted direction
     *  Change out-to-middle assignments to predicates
     *  Change input realized variables to variables.
     *
     * @param t the t
     */
    private void direct(Transformation t) {
        // TODO Auto-generated method stub
        for (Rule r : t.getRule()) {
            migrateVariables((Mapping) r);
            direct((Mapping) r);
        }
        // Delete rv from model
        for (Variable rv : refinedVars.keySet()) {
            EcoreUtil.delete(rv, true);
        }
    }

    /**
     * Fix realized variable references.
     *
     * @param m the m
     */
    private void fixRealizedVariableReferences(Mapping m) {
        for (Domain d : m.getDomain()) {
            MallardMtcUtil.fixReferences((Area) d, refinedVars);
        }
        MallardMtcUtil.fixReferences(m, refinedVars);
        for (Mapping lm : m.getLocal()) {
            fixRealizedVariableReferences(lm);
        }
        // Extending mappings can also have references that need fixing
//		for (Mapping rm : m.getRefinement()) {
//			fixRealizedVariableReferences(rm);
//		}
    }

    /**
     * Migrate variables.
     *
     * @param m the m
     */
    private void migrateVariables(Mapping m) {

        changeRealizedToVariable(m);
        fixRealizedVariableReferences(m);
    }

}
