/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;

/**
 * Configuration captures the configuration to be imposed by a QVTc transformation and provides configuration related
 * utilities.
 */
public class Configuration {

    /**
     * The Enum Mode.
     */
    public enum Mode {

        /** The check. */
        CHECK,

        /** The enforce. */
        ENFORCE
    }

    /** The mode. */
    private final @NonNull Mode mode;

//    /** The inputs. */
//    private final @NonNull List<@NonNull CandidateModel> inputModels;		// FIXME Names rather than TypedModel until c/u TypedModel ambiguity eliminated
//
//    /** The output. */
//    private final @NonNull CandidateModel outputModel;
//
//    /** The middle. */
//    private final @NonNull CandidateModel middleModel;

    private @NonNull Set<@NonNull TypedModel> inputDomains;

    private @NonNull TypedModel outputDomain;

    private @NonNull TypedModel middleDomain;

    private Map<TypedModel, List<URI>> inputModels = new HashMap<TypedModel, List<URI>>();;

    private URI outputModel;

    private URI middleModel;


    /**
     * Instantiates a new configuration.
     *
     * @param mode the mode
     * @param inputs the input domains
     * @param output the output domain
     * @param middle the middle domain
     */
    public Configuration(@NonNull Mode mode, @NonNull Set<@NonNull TypedModel> inputs,
            @NonNull TypedModel output,
            @NonNull TypedModel middle) {
        this.mode = mode;
        this.inputDomains = inputs;
        this.outputDomain = output;
        this.middleDomain = middle;
    }



    /**
     * @return the inputDomains
     */
    public Set<TypedModel> getInputDomains() {
        return inputDomains;
    }



    /**
     * @param inputDomains the inputDomains to set
     */
    public void setInputDomains(@NonNull Set<@NonNull TypedModel> inputDomains) {
        this.inputDomains = inputDomains;
    }



    /**
     * @return the outputDomain
     */
    public @NonNull TypedModel getOutputDomain() {
        return outputDomain;
    }



    /**
     * @param outputDomain the outputDomain to set
     */
    public void setOutputDomain(@NonNull TypedModel outputDomain) {
        this.outputDomain = outputDomain;
    }



    /**
     * @return the middleDomain
     */
    public @NonNull TypedModel getMiddleDomain() {
        return middleDomain;
    }



    /**
     * @param middleDomain the middleDomain to set
     */
    public void setMiddleDomain(@NonNull TypedModel middleDomain) {
        this.middleDomain = middleDomain;
    }



    /**
     * Copy constructor.
     *
     * @param other the other
     */
    public Configuration(@NonNull Configuration other) {
        mode = other.mode;
        inputDomains = new HashSet<@NonNull TypedModel>(other.inputDomains.size());
        for (@NonNull TypedModel cm : other.inputDomains) {
            inputDomains.add(cm);
        }
        outputDomain = other.outputDomain;
        middleDomain = other.middleDomain;
    }

    /**
     * Checks if is check mode.
     *
     * @return true, if is check mode
     */
    public boolean isCheckMode() {
        return mode == Mode.CHECK;
    }

    /**
     * Checks if is enforce mode.
     *
     * @return true, if is enforce mode
     */
    public boolean isEnforceMode() {
        return mode == Mode.ENFORCE;
    }

    /**
     * Checks if is input.
     *
     * @param direction the direction
     * @return true, if is input
     */
    public boolean isInput(TypedModel direction) {
        return inputDomains.contains(direction);
    }

    /**
     * Checks if is output.
     *
     * @param direction the direction
     * @return true, if is output
     */
    public boolean isOutput(TypedModel direction) {
        return outputDomain.equals(direction);
    }

    /**
     * Checks if is middle.
     *
     * @param direction the direction
     * @return true, if is middle
     */
    public boolean isMiddle(TypedModel direction) {
        return middleDomain.equals(direction);
    }

    public void addInputModel(String domainName, URI model) {

        @Nullable TypedModel domain = inputDomains.stream().filter(tm -> tm.getName().equals(domainName)).findFirst().orElse(null);
        if (domain != null) {
            List<URI> modelsForDomain = inputModels.get(domain);
            if (modelsForDomain == null) {
                modelsForDomain = new ArrayList<URI>();
                inputModels.put(domain, modelsForDomain);
            }
            modelsForDomain.add(model);
        }
    }

    public void addMiddleModel(URI model) {
        middleModel = model;
    }

    public void addOutputModel(URI model) {
        outputModel = model;
    }

    /**
     * Gets the input models.
     *
     * @return the inputModels
     */
    public Map<TypedModel, List<URI>> getInputModels() {
        return inputModels;
    }

    /**
     * Gets the output model.
     *
     * @return the outputModel
     */
    public URI getOutputModel() {
        return outputModel;
    }

    /**
     * Gets the middle model.
     *
     * @return the middleModel
     */
    public URI getMiddleModel() {
        return middleModel;
    }

}
