/*******************************************************************************
 * Copyright (c) 2013, 2015 The University of York, Willink Transformations and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - initial API and implementation
 ******************************************************************************/
package uk.ac.york.qvtd.compiler.mtc;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.ocl.pivot.util.AbstractExtendingVisitor;
import org.eclipse.ocl.pivot.util.Visitable;
import org.eclipse.qvtd.pivot.qvtbase.BaseModel;
import org.eclipse.qvtd.pivot.qvtbase.Domain;
import org.eclipse.qvtd.pivot.qvtbase.Function;
import org.eclipse.qvtd.pivot.qvtbase.FunctionParameter;
import org.eclipse.qvtd.pivot.qvtbase.Pattern;
import org.eclipse.qvtd.pivot.qvtbase.Predicate;
import org.eclipse.qvtd.pivot.qvtbase.Rule;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.eclipse.qvtd.pivot.qvtcorebase.Assignment;
import org.eclipse.qvtd.pivot.qvtcorebase.BottomPattern;
import org.eclipse.qvtd.pivot.qvtcorebase.CoreDomain;
import org.eclipse.qvtd.pivot.qvtcorebase.CorePattern;
import org.eclipse.qvtd.pivot.qvtcorebase.EnforcementOperation;
import org.eclipse.qvtd.pivot.qvtcorebase.GuardPattern;
import org.eclipse.qvtd.pivot.qvtcorebase.NavigationAssignment;
import org.eclipse.qvtd.pivot.qvtcorebase.OppositePropertyAssignment;
import org.eclipse.qvtd.pivot.qvtcorebase.PropertyAssignment;
import org.eclipse.qvtd.pivot.qvtcorebase.RealizedVariable;
import org.eclipse.qvtd.pivot.qvtcorebase.VariableAssignment;
import org.eclipse.qvtd.pivot.qvtimperative.ConnectionAssignment;
import org.eclipse.qvtd.pivot.qvtimperative.ConnectionStatement;
import org.eclipse.qvtd.pivot.qvtimperative.ConnectionVariable;
import org.eclipse.qvtd.pivot.qvtimperative.ImperativeBottomPattern;
import org.eclipse.qvtd.pivot.qvtimperative.ImperativeDomain;
import org.eclipse.qvtd.pivot.qvtimperative.MappingCall;
import org.eclipse.qvtd.pivot.qvtimperative.MappingCallBinding;
import org.eclipse.qvtd.pivot.qvtimperative.MappingLoop;
import org.eclipse.qvtd.pivot.qvtimperative.MappingSequence;
import org.eclipse.qvtd.pivot.qvtimperative.MappingStatement;
import org.eclipse.qvtd.pivot.qvtimperative.VariablePredicate;
import org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;

/**
 * The AbstractExecutionPlanInjection is a minimal implementation.
 */
public abstract class AbstractExecutionPlanInjection extends AbstractExtendingVisitor<Object, ExecutionPlanGraph>
    implements QVTimperativeVisitor<Object>{

    /**
     * Instantiates a new abstract execution plan injection.
     *
     * @param executionPlan the context
     */
    protected AbstractExecutionPlanInjection(ExecutionPlanGraph executionPlan) {
        super(executionPlan);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtcorebase.util.QVTcoreBaseVisitor#visitAssignment(org.eclipse.qvtd.pivot.qvtcorebase.Assignment)
     */
    @Override
    @Nullable
    public Object visitAssignment(@NonNull Assignment object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtbase.util.QVTbaseVisitor#visitBaseModel(org.eclipse.qvtd.pivot.qvtbase.BaseModel)
     */
    @Override
    @Nullable
    public Object visitBaseModel(@NonNull BaseModel object) {
        return visiting(object);

    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtcorebase.util.QVTcoreBaseVisitor#visitBottomPattern(org.eclipse.qvtd.pivot.qvtcorebase.BottomPattern)
     */
    @Override
    @Nullable
    public Object visitBottomPattern(@NonNull BottomPattern object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitConnectionAssignment(org.eclipse.qvtd.pivot.qvtimperative.ConnectionAssignment)
     */
    @Override
    public @Nullable Object visitConnectionAssignment(@NonNull ConnectionAssignment object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitConnectionStatement(org.eclipse.qvtd.pivot.qvtimperative.ConnectionStatement)
     */
    @Override
    public Object visitConnectionStatement(@NonNull ConnectionStatement object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitConnectionVariable(org.eclipse.qvtd.pivot.qvtimperative.ConnectionVariable)
     */
    @Override
    public Object visitConnectionVariable(@NonNull ConnectionVariable object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtcorebase.util.QVTcoreBaseVisitor#visitCoreDomain(org.eclipse.qvtd.pivot.qvtcorebase.CoreDomain)
     */
    @Override
    @Nullable
    public Object visitCoreDomain(@NonNull CoreDomain object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtcorebase.util.QVTcoreBaseVisitor#visitCorePattern(org.eclipse.qvtd.pivot.qvtcorebase.CorePattern)
     */
    @Override
    @Nullable
    public Object visitCorePattern(@NonNull CorePattern object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtbase.util.QVTbaseVisitor#visitDomain(org.eclipse.qvtd.pivot.qvtbase.Domain)
     */
    @Override
    @Nullable
    public Object visitDomain(@NonNull Domain object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtcorebase.util.QVTcoreBaseVisitor#visitEnforcementOperation(org.eclipse.qvtd.pivot.qvtcorebase.EnforcementOperation)
     */
    @Override
    @Nullable
    public Object visitEnforcementOperation(@NonNull EnforcementOperation object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtbase.util.QVTbaseVisitor#visitFunction(org.eclipse.qvtd.pivot.qvtbase.Function)
     */
    @Override
    @Nullable
    public Object visitFunction(@NonNull Function object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtbase.util.QVTbaseVisitor#visitFunctionParameter(org.eclipse.qvtd.pivot.qvtbase.FunctionParameter)
     */
    @Override
    @Nullable
    public Object visitFunctionParameter(@NonNull FunctionParameter object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtcorebase.util.QVTcoreBaseVisitor#visitGuardPattern(org.eclipse.qvtd.pivot.qvtcorebase.GuardPattern)
     */
    @Override
    @Nullable
    public Object visitGuardPattern(@NonNull GuardPattern object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitImperativeBottomPattern(org.eclipse.qvtd.pivot.qvtimperative.ImperativeBottomPattern)
     */
    @Override
    @Nullable
    public Object visitImperativeBottomPattern(@NonNull ImperativeBottomPattern object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitImperativeDomain(org.eclipse.qvtd.pivot.qvtimperative.ImperativeDomain)
     */
    @Override
    public @Nullable Object visitImperativeDomain(@NonNull ImperativeDomain object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ocl.pivot.util.Visitor#visiting(org.eclipse.ocl.pivot.util.Visitable)
     */
    @Override
    @Nullable
    public Object visiting(@NonNull Visitable visitable) {
        throw new IllegalArgumentException("Unsupported " + visitable.eClass().getName() + " for " + getClass().getSimpleName());
    }


    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitMappingCall(org.eclipse.qvtd.pivot.qvtimperative.MappingCall)
     */
    @Override
    @Nullable
    public Object visitMappingCall(@NonNull MappingCall object) {
        return visiting(object);
    }


    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitMappingCallBinding(org.eclipse.qvtd.pivot.qvtimperative.MappingCallBinding)
     */
    @Override
    @Nullable
    public Object visitMappingCallBinding(@NonNull MappingCallBinding object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitMappingLoop(org.eclipse.qvtd.pivot.qvtimperative.MappingLoop)
     */
    @Override
    @Nullable
    public Object visitMappingLoop(@NonNull MappingLoop object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitMappingSequence(org.eclipse.qvtd.pivot.qvtimperative.MappingSequence)
     */
    @Override
    @Nullable
    public Object visitMappingSequence(@NonNull MappingSequence object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitMappingStatement(org.eclipse.qvtd.pivot.qvtimperative.MappingStatement)
     */
    @Override
    @Nullable
    public Object visitMappingStatement(@NonNull MappingStatement object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtcorebase.util.QVTcoreBaseVisitor#visitNavigationAssignment(org.eclipse.qvtd.pivot.qvtcorebase.NavigationAssignment)
     */
    @Override
    public Object visitNavigationAssignment(@NonNull NavigationAssignment object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtcorebase.util.QVTcoreBaseVisitor#visitOppositePropertyAssignment(org.eclipse.qvtd.pivot.qvtcorebase.OppositePropertyAssignment)
     */
    @Override
    public Object visitOppositePropertyAssignment(@NonNull OppositePropertyAssignment object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtbase.util.QVTbaseVisitor#visitPattern(org.eclipse.qvtd.pivot.qvtbase.Pattern)
     */
    @Override
    @Nullable
    public Object visitPattern(@NonNull Pattern object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtbase.util.QVTbaseVisitor#visitPredicate(org.eclipse.qvtd.pivot.qvtbase.Predicate)
     */
    @Override
    @Nullable
    public Object visitPredicate(@NonNull Predicate object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtcorebase.util.QVTcoreBaseVisitor#visitPropertyAssignment(org.eclipse.qvtd.pivot.qvtcorebase.PropertyAssignment)
     */
    @Override
    @Nullable
    public Object visitPropertyAssignment(@NonNull PropertyAssignment object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtcorebase.util.QVTcoreBaseVisitor#visitRealizedVariable(org.eclipse.qvtd.pivot.qvtcorebase.RealizedVariable)
     */
    @Override
    @Nullable
    public Object visitRealizedVariable(@NonNull RealizedVariable object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtbase.util.QVTbaseVisitor#visitRule(org.eclipse.qvtd.pivot.qvtbase.Rule)
     */
    @Override
    @Nullable
    public Object visitRule(@NonNull Rule object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtbase.util.QVTbaseVisitor#visitTypedModel(org.eclipse.qvtd.pivot.qvtbase.TypedModel)
     */
    @Override
    @Nullable
    public Object visitTypedModel(@NonNull TypedModel object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtcorebase.util.QVTcoreBaseVisitor#visitVariableAssignment(org.eclipse.qvtd.pivot.qvtcorebase.VariableAssignment)
     */
    @Override
    @Nullable
    public Object visitVariableAssignment(@NonNull VariableAssignment object) {
        return visiting(object);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.util.QVTimperativeVisitor#visitVariablePredicate(org.eclipse.qvtd.pivot.qvtimperative.VariablePredicate)
     */
    @Override
    @Nullable
    public Object visitVariablePredicate(@NonNull VariablePredicate object) {
        return visiting(object);
    }


}
