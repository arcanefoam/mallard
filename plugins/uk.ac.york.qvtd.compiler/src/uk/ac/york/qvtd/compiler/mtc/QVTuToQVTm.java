/*******************************************************************************
 * Copyright (c) 2015 University of York and Others
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.mtc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.OperationCallExp;
import org.eclipse.ocl.pivot.PivotFactory;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.ocl.pivot.internal.utilities.PivotUtilInternal;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.qvtd.compiler.internal.utilities.ClassRelationships;
import org.eclipse.qvtd.pivot.qvtbase.Domain;
import org.eclipse.qvtd.pivot.qvtbase.Predicate;
import org.eclipse.qvtd.pivot.qvtbase.Rule;
import org.eclipse.qvtd.pivot.qvtbase.Transformation;
import org.eclipse.qvtd.pivot.qvtcore.CoreModel;
import org.eclipse.qvtd.pivot.qvtcore.Mapping;
import org.eclipse.qvtd.pivot.qvtcorebase.Area;
import org.eclipse.qvtd.pivot.qvtcorebase.Assignment;
import org.eclipse.qvtd.pivot.qvtcorebase.BottomPattern;
import org.eclipse.qvtd.pivot.qvtcorebase.CoreDomain;
import org.eclipse.qvtd.pivot.qvtcorebase.CorePattern;
import org.eclipse.qvtd.pivot.qvtcorebase.GuardPattern;
import org.eclipse.qvtd.pivot.qvtcorebase.QVTcoreBaseFactory;
import org.eclipse.qvtd.pivot.qvtcorebase.RealizedVariable;
import org.eclipse.qvtd.pivot.qvtcorebase.VariableAssignment;

import uk.ac.york.qvtd.compiler.utilities.MallardMtcUtil;
import uk.ac.york.qvtd.compiler.utilities.RefinementLinearization;

// TODO: Auto-generated Javadoc
/**
 * The Class QVTuToQVTm.
 * This class prunes the model to flatten the mappings and merge refinements
 */
public class QVTuToQVTm {

    /** The refined vars. */
    private Map<Variable, Variable> refinedVars = new HashMap<Variable, Variable>();

    /** The environment factory. */
    private @NonNull EnvironmentFactory environmentFactory;

    /**  The Class relationships helper. */
    private ClassRelationships clasRels;


    /** The map refs. */
    private RefinementLinearization mapRefs;

    /**
     * Instantiates a new QV tu to qv tm.
     *
     * @param environmentFactory the environment factory
     */
    public QVTuToQVTm(@NonNull EnvironmentFactory environmentFactory) {
        super();
        this.environmentFactory = environmentFactory;
        this.clasRels = new ClassRelationships(environmentFactory);
        this.mapRefs = new RefinementLinearization();
    }

    /**
     * Can be flattened.
     *
     * @param local the local
     * @param area the area
     * @return true, if successful
     */
    public boolean canBeFlattened(Mapping local, Area area) {
        for (Predicate p : area.getGuardPattern().getPredicate()) {
            for (Variable vexp : MallardMtcUtil.findReferencedVariables(p.getConditionExpression())) {
                Mapping parent = local.getContext();
                Area varDom = MallardMtcUtil.getArea(vexp);
                if (varDom instanceof CoreDomain) {
                    CoreDomain varCDom = (CoreDomain) varDom;
                    for (Domain pd : parent.getDomain()) {
                        if (!pd.getTypedModel().equals(varCDom.getTypedModel())) {
                            for (Assignment assig : ((Area) pd).getBottomPattern().getAssignment()) {
                                if (assig instanceof VariableAssignment) {
                                    if (((VariableAssignment) assig).getTargetVariable().equals(vexp)) {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                    for (Assignment assig : parent.getBottomPattern().getAssignment()) {
                        if (assig instanceof VariableAssignment) {
                            if (((VariableAssignment) assig).getTargetVariable().equals(vexp)) {
                                return false;
                            }
                        }
                    }
                }
                else {
                    for (Domain pd : parent.getDomain()) {
                        for (Assignment assig : ((Area) pd).getBottomPattern().getAssignment()) {
                            if (assig instanceof VariableAssignment) {
                                if (((VariableAssignment) assig).getTargetVariable().equals(vexp)) {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * Execute.
     *
     * @param model the model
     */
    public void execute(CoreModel model) {

        for (org.eclipse.ocl.pivot.Package p : model.getOwnedPackages()) {
            for (Class c : p.getOwnedClasses()) {
                if (c instanceof Transformation) {
                    normalize((Transformation) c);
                }
            }
        }

    }

    /**
     * Tests if there is any variable referenced from a predicate that is assigned in a domain other that the
     * variable's one.
     *
     * Search for assignments should be done in the closure of parents!
     *
     * @param local the local
     * @return true, if successful
     */
    private boolean canBeFlattened(Mapping local) {
        for (Domain nd : local.getDomain()) {
            if (!canBeFlattened(local, (Area) nd)) {
                return false;
            }
        }
        return canBeFlattened(local, local);
    }



    /**
     * Find and use assignment.
     *
     * @param var the var
     * @param area the area
     * @return true, if successful
     */
    private boolean findAndUseAssignment(Variable var, Area area) {
        for (Assignment a : area.getBottomPattern().getAssignment()) {
            if (a instanceof VariableAssignment) {
                if (((VariableAssignment) a).getTargetVariable().equals(var)) {
                    OperationCallExp exp = MallardMtcUtil.assignmentToOclExp(a, environmentFactory);
                    var.setOwnedInit(exp.getOwnedArguments().get(0));
                    EcoreUtil.delete(a, true);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Move everything from the nested mappings to the top one. The mapping can only be flattened if the the
     * variables in the guard patterns are not assigned in another domain. E.g., if a variable in L is assigned
     * in the bottom pattern of M, and it is used in the guard of the nested mapping, we can not flatten it.
     *
     * TODO We should probably start bottom up, i.e. deeper nestings first, if a contained mapping can't be
     * flattened, then the parent can't either!
     * @param m the m
     */
    private void flaten(Mapping m) {
        ArrayList<EObject> temp = new ArrayList<EObject>(m.getLocal());
        Iterator<EObject> it = temp.iterator();
        while (it.hasNext()) {
            Mapping local = (Mapping) it.next();
            if (canBeFlattened(local)) {
                flaten(local);
                for (Domain nd : local.getDomain()) {
                    moveToRootDomain(m, (CoreDomain) nd);
                }
                moveToOtherArea(local, m);
                EcoreUtil.delete(local);
                // After all has been flatten all predicates, assignments and inits must
                // have their references fixed.
                for (Domain d : m.getDomain()) {
                    MallardMtcUtil.fixReferences((Area) d, refinedVars);
                }
                MallardMtcUtil.fixReferences((Area) m, refinedVars);
            }
            else {
                // Fixed the references to the unmoved one
                // After all has been flatten all predicates, assignments and inits must
                // have their references fixed.
                for (Domain d : local.getDomain()) {
                    MallardMtcUtil.fixReferences((Area) d, refinedVars);
                }
                MallardMtcUtil.fixReferences((Area) local, refinedVars);

            }

        }
    }

    /**
     * Gets the all refinements.
     *
     * @param m the m
     * @return the all refinements
     */
    private Iterable<Mapping> getAllSpecifications(Mapping m) {
//        Set<Mapping> refined = new HashSet<Mapping>(m.getSpecification());
//        for (Mapping rm : m.getSpecification()) {
//            refined.addAll(getAllSpecifications(rm));
//        }
//        return refined;
    	Map<Mapping, List<Mapping>> graph = mapRefs.mapping_graph(m);
    	Map<Mapping, List<Mapping>> lin = mapRefs.linearize(graph);
        return lin.get(m);
    }

    /**
     * Gets the root domain.
     *
     * @param m the m
     * @param nd the nd
     * @return the root domain
     */
    private CoreDomain getRootDomain(Mapping m, Domain nd) {
        for (Domain d : m.getDomain()) {
            if (d.getTypedModel().getName().equals(nd.getTypedModel().getName()))
                return (CoreDomain) d;
        }
        return null;
    }

    /**
     * Checks for variables in the Area.
     *
     * @param a the area
     * @return true, if successful
     */
    private boolean hasVariables(Area a) {
        return !(a.getGuardPattern().getVariable().isEmpty() ||
                a.getBottomPattern().getVariable().isEmpty() ||
                a.getBottomPattern().getRealizedVariable().isEmpty());
    }

    /**
     * Test if the mapping is not needed. A mapping is not needed if it is refined, and if
     * TODO Verify this!!
     * @param m the mapping
     * @return true, if is not needed
     */
    private boolean isNotNeeded(Mapping m) {
        if (m.getRefinement().isEmpty())
            return false;
        for (Mapping refining : m.getRefinement()) {
            for (Domain rd : refining.getDomain()) {
                if (hasVariables((Area) rd))
                    return false;
            }
            if (hasVariables((Area) m))
                return false;
        }
        return true;
    }

    /**
     * Merge areas.
     *
     * @param target the target area
     * @param source the source area
     */
    private void mergeAreas(Area target, Area source) {
        for (Variable gv : source.getGuardPattern().getVariable()) {
            mergeVariable(gv, target.getGuardPattern());
        }
        for (Variable bv : source.getBottomPattern().getVariable()) {
            mergeVariable(bv, target.getGuardPattern());
        }
        for (RealizedVariable brv : source.getBottomPattern().getRealizedVariable()) {
            mergeRealizedVariable(brv, target.getBottomPattern());
        }
        // Copy all predicates and assignments, might generate duplicates?
        for (Predicate p : source.getGuardPattern().getPredicate()) {
            Predicate pOut = EcoreUtil.copy(p);
            target.getGuardPattern().getPredicate().add(pOut);
        }
        for (Predicate p : source.getBottomPattern().getPredicate()) {
            Predicate pOut = EcoreUtil.copy(p);
            target.getGuardPattern().getPredicate().add(pOut);
        }
        for (Assignment as : source.getBottomPattern().getAssignment()) {
            Assignment aOut = EcoreUtil.copy(as);
            target.getBottomPattern().getAssignment().add(aOut);
        }

    }

    /**
     * Find the matching domain or create one and them merge them.
     *
     * @param refining the refining domain
     * @param rd the rd
     */
    private void mergeDomain(Mapping refining, Domain rd) {
        CoreDomain cd = null;
        for (Domain d : refining.getDomain()) {
            if (d.getTypedModel().equals(rd.getTypedModel())) {
                cd = (CoreDomain) d;
            }
        }
        if (cd == null) {
            // We need a new domain
            cd = QVTcoreBaseFactory.eINSTANCE.createCoreDomain();
            cd.setTypedModel(rd.getTypedModel());
            cd.setName(rd.getName());
            cd.setIsCheckable(rd.isIsCheckable());
            cd.setIsEnforceable(rd.isIsEnforceable());
            GuardPattern dgp = QVTcoreBaseFactory.eINSTANCE.createGuardPattern();
            cd.setGuardPattern(dgp);
            BottomPattern dbp = QVTcoreBaseFactory.eINSTANCE.createBottomPattern();
            cd.setBottomPattern(dbp);
            refining.getDomain().add(cd);
        }
        mergeAreas((Area)cd, (Area)rd);
    }

    /**
     * Merging requires that variables share the name. Variables with the
     * same name should have types in the same hierarchy. The refining variable
     * is assumed to have the less abstract type.
     *
     * @param existing the existing
     * @param target the target
     */
    private void mergeRealizedVariable(RealizedVariable existing, BottomPattern target) {
        // Find a var with the same name
        RealizedVariable match = null;
        for (RealizedVariable v : target.getRealizedVariable()) {
            if (v.getName().equals(existing.getName())) {
                match = v;
                break;
            }
        }
        if (match != null) {
            Class existingClass = (Class) existing.getType();
            Class matchClass = (Class)match.getType();
//            if (!(clasRels.getAllSuperClasses(matchClass).contains(rvClass) || matchClass.equals(rvClass))) {
//                throw new IllegalArgumentException("Cant match var.");
//            }
            //assert clasRels.getAllSuperClasses(matchClass).contains(rvClass) || matchClass.equals(rvClass);
            if (clasRels.getAllSuperClasses(matchClass).contains(existingClass)) {
                refinedVars.put(existing, match);
            } else if (clasRels.getAllSuperClasses(existingClass).contains(matchClass)) {
                refinedVars.put(match, existing);
            } else if(matchClass.equals(existingClass)) {
                refinedVars.put(existing, match);
            } else {
                throw new IllegalArgumentException("Cant match var.");
            }
        } else {
            // We need to copy the variable
            match = QVTcoreBaseFactory.eINSTANCE.createRealizedVariable();
            match.setName(existing.getName());
            match.setIsImplicit(existing.isIsImplicit());
            if (match.getOwnedInit() != null) {
                match.setOwnedInit(EcoreUtil.copy(existing.getOwnedInit()));
            }
            match.setType(existing.getType());
            target.getRealizedVariable().add(match);

        }
        // References to existing must be fixed
        refinedVars.put(existing, match);
    }

    /**
     * Merge all the domains and patterns from the refined mappings into the
     * refining one.
     * Refined mappings are merged in "complexity" order, applying mappings
     * that refine the more number of mappings (or the less abstract). This
     * follows the idea that more abstract mappings will also use the more
     * abstract types for their variables and hence the merging of variables
     * by keeping the less abstract one works.
     *
     * @param m the m
     */
    private void mergeRefinements(Mapping m) {
        // Recursively get all the mappings refined, so we only refine them once
        Iterable<Mapping> refined = getAllSpecifications(m);
//        List<Mapping> refinedOrdered = new ArrayList<Mapping>(refined);
//        Collections.sort(refinedOrdered, new RefinementComparator());
        //if (m.getDomain().isEmpty()) {
            // If the mapping has no domains, we need to merge them the other way?
            // Maybe we need to catch an exception because it seems to be example dependent
            //Collections.reverse(refinedOrdered);
        //}
//        try {
        Iterator<Mapping> it = refined.iterator();
        it.next();	// The first one is always m
            while(it.hasNext()) {
                Mapping r = it.next();
                for (Domain rd : r.getDomain()) {
                    mergeDomain(m, rd);
                }
                mergeAreas((Area)m, (Area)r);
            }
//        } catch (IllegalArgumentException ex) {
//            Collections.reverse(refinedOrdered);
//            m.getDomain().clear();// Start over
//            m.getBottomPattern().getVariable().clear();
//            m.getBottomPattern().getRealizedVariable().clear();
//            m.getGuardPattern().getVariable().clear();
//            for (Mapping r : refinedOrdered) {
//                for (Domain rd : r.getDomain()) {
//                    mergeDomain(m, rd);
//                }
//                mergeAreas((Area)m, (Area)r);
//            }
//
//        }
        // After all has been merged all predicates, assignments and inits must
        // have their references fixed.
        for (Domain d : m.getDomain()) {
            MallardMtcUtil.fixReferences((Area) d, refinedVars);
        }
        MallardMtcUtil.fixReferences((Area) m, refinedVars);

    }

    /**
     * Merging requires that variables share the name. Variables with the
     * same name should have types in the same hierarchy. The refining variable
     * is assumed to have the less abstract type.
     *
     * @param existing the existing
     * @param target the target
     */
    private void mergeVariable(Variable existing, GuardPattern target) {
        // Find a var with the same name
        Variable match = null;
        for (Variable v : target.getVariable()) {
            if (v.getName().equals(existing.getName())) {
                match = v;
                break;
            }
        }
        if (match != null) {
            Class vClass = (Class) existing.getType();
            Class matchClass = (Class)match.getType();
            if (clasRels.getAllSuperClasses(matchClass).contains(vClass)) {
                refinedVars.put(existing, match);
            } else if (clasRels.getAllSuperClasses(vClass).contains(matchClass)) {
                refinedVars.put(match, existing);
            } else if(matchClass.equals(vClass)) {
                refinedVars.put(existing, match);
            } else {
                throw new IllegalArgumentException("Cant match var.");
            }
        } else {
            // We need to copy the variable
            match = PivotFactory.eINSTANCE.createVariable();
            match.setName(existing.getName());
            match.setIsImplicit(existing.isIsImplicit());
            if (match.getOwnedInit() != null) {
                match.setOwnedInit(EcoreUtil.copy(existing.getOwnedInit()));
            }
            match.setType(existing.getType());
            target.getVariable().add(match);
        }
        // REferences to rv must be fixed
        refinedVars.put(existing, match);

    }

    /**
     * Move bottom pattern.
     *
     * @param source the source
     * @param target the target
     */
    private void moveBottomPattern(BottomPattern source, BottomPattern target) {
        moveCorePattern(source, target);
        @NonNull ArrayList<EObject> temp = new ArrayList<EObject>(source.getRealizedVariable());
        Iterator<EObject> it = temp.iterator();
        while (it.hasNext()) {
            EObject n = it.next();
            assert n != null;
            PivotUtilInternal.resetContainer(n);
            target.getRealizedVariable().add((RealizedVariable) n);
        }
        temp = new ArrayList<EObject>(source.getAssignment());
        it = temp.iterator();
        while (it.hasNext()) {
            EObject n = it.next();
            assert n != null;
            PivotUtilInternal.resetContainer(n);
            target.getAssignment().add((Assignment) n);
        }
    }

    /**
     * Move core pattern.
     *
     * @param source the source
     * @param target the target
     */
    private void moveCorePattern(CorePattern source, CorePattern target) {
        ArrayList<EObject> temp = new ArrayList<EObject>(source.getVariable());
        Iterator<EObject> it = temp.iterator();
        while (it.hasNext()) {
            Variable n = (Variable) it.next();
            assert n != null;
            PivotUtilInternal.resetContainer(n);
            target.getVariable().add(n);
        }
        temp = new ArrayList<EObject>(source.getPredicate());
        it = temp.iterator();
        while (it.hasNext()) {
            EObject p = it.next();
            assert p != null;
            PivotUtilInternal.resetContainer(p);
            target.getPredicate().add((Predicate) p);
        }
    }

    /**
     * Move to other area.
     *
     * @param source the source
     * @param target the target
     */
    private void moveToOtherArea(Area source, Area target) {
        moveCorePattern(source.getGuardPattern(), target.getGuardPattern());
        moveBottomPattern(source.getBottomPattern(), target.getBottomPattern());

    }

    /**
     * Move to root domain.
     *
     * @param m the m
     * @param nd the nd
     */
    private void moveToRootDomain(Mapping m, CoreDomain nd) {
        CoreDomain root_domain = getRootDomain(m, nd);
        assert root_domain != null;
        moveToOtherArea(nd, root_domain);
    }

    /**
     * Normalize.
     *
     * @param t the t
     */
    private void normalize(Transformation t) {

        // Flatten nested mappings
        for (Rule r : t.getRule()) {
            flaten((Mapping) r);
        }
        // Delete all not needed refined mappings
        Set<Mapping> forDeletion = new HashSet<Mapping>();
        for (Rule r : t.getRule()) {
            if (isNotNeeded((Mapping) r)) {
                forDeletion.add((Mapping) r);
            }

        }
        // Merge refinements, we only merge mappings that are not further refined
        List<Mapping> refining = new ArrayList<Mapping>();
        for (Rule r : t.getRule()) {
            Mapping m = (Mapping) r;
            if (!m.getSpecification().isEmpty() && m.getRefinement().isEmpty())
                refining.add(m);
        }
        // Sort them by # of refinings and if they are refined
        Collections.sort(refining, new RefinementComparator());
        for (Mapping m : refining) {
            mergeRefinements(m);
        }
        for (Mapping m : forDeletion) {
            EcoreUtil.delete(m, true);
        }
        // Transform variable assignments to init expressions if possible
        for (Rule r : t.getRule()) {
            for (Domain d : r.getDomain()) {
                reduceVarAssignments((Area) d);
            }
            reduceVarAssignments((Area) r);
        }

    }

    /**
     * Find any variable assignments, and if possible change them to variable
     * initialisers.
     *
     * @param area the area
     */
    private void reduceVarAssignments(Area area) {
        List<Variable> move = new ArrayList<Variable>();
        for (Variable gv : area.getGuardPattern().getVariable()) {
            if (findAndUseAssignment(gv, area)) {
                // If initialised, move it to the bottom
                move.add(gv);
            }
        }
        for (Variable v : move) {
            PivotUtilInternal.resetContainer(v);
            area.getBottomPattern().getVariable().add(v);
        }
        for (Variable bv : area.getBottomPattern().getVariable()) {
            findAndUseAssignment(bv, area);
        }

    }

    /**
     * The Class RefinementComparator.
     */
    private final class RefinementComparator implements Comparator<Mapping> {

        /* (non-Javadoc)
         * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
         */
        @Override
        public int compare(Mapping m1, Mapping m2) {
            final int BEFORE = -1;
            final int EQUAL = 0;
            final int AFTER = 1;

            if (m1 == m2) return EQUAL;

            if (m1.getSpecification().size() < m2.getSpecification().size()) return AFTER;
            if (m1.getSpecification().size() > m2.getSpecification().size()) return BEFORE;

            if (m1.getSpecification().contains(m2)) return BEFORE;
            if (m2.getSpecification().contains(m1)) return AFTER;

            return EQUAL;
        }
    }

}
