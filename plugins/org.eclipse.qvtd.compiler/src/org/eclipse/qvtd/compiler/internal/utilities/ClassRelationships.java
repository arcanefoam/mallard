/*******************************************************************************
 * Copyright (c) 2016 Willink Transformations and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     E.D.Willink - initial API and implementation
 *******************************************************************************/
package org.eclipse.qvtd.compiler.internal.utilities;

import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.all;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singletonList;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.CollectionType;
import org.eclipse.ocl.pivot.Model;
import org.eclipse.ocl.pivot.Package;
import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.TypedElement;
import org.eclipse.ocl.pivot.internal.manager.MetamodelManagerInternal;
import org.eclipse.ocl.pivot.utilities.ClassUtil;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.ocl.pivot.utilities.MetamodelManager;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;


/**
 *
 * The C3 Linearization is based on https://github.com/ruediste/c3java
 * @author asbh500
 * @author Horacio Hoyos
 *
 * @param <C>
 */
public class ClassRelationships {
    private static final @NonNull Set<@NonNull Class> EMPTY_CLASS_SET = Collections.<@NonNull Class>emptySet();

    public static class ContainerClass {

        private @NonNull Class containerClass;

        private @NonNull Property containmentProperty;

        public ContainerClass(@NonNull Class containerClass, @NonNull Property containmentProperty) {
            this.containerClass = containerClass;
            this.containmentProperty = containmentProperty;
        }

        public @NonNull Class getContainerClass() {
            return containerClass;
        }

        public @NonNull Property getContainmentProperty() {
            return containmentProperty;
        }

        @Override
        public @NonNull String toString() {

            return containerClass.getName() + " - " + containmentProperty.getName();
        }
    }

    private @NonNull Map<@NonNull Class, Set<@NonNull Class>> class2superClasses = new HashMap<@NonNull Class, Set<@NonNull Class>>();

    private @NonNull Map<@NonNull Class, Set<@NonNull Class>> class2directSubClasses = new HashMap<@NonNull Class, Set<@NonNull Class>>();

    private @NonNull Map<@NonNull Class, Set<@NonNull Class>> class2allSubClasses = new HashMap<@NonNull Class, Set<@NonNull Class>>();

    private @NonNull Map<@NonNull Class, Set<@NonNull Class>> class2containerClasses = new HashMap<@NonNull Class, Set<@NonNull Class>>();

    private @NonNull Map<@NonNull Class, Set<@NonNull ContainerClass>> class2detailedContainerClasses = new HashMap<@NonNull Class, Set<@NonNull ContainerClass>>();

    protected @NonNull MetamodelManager mManager;

    private Deque<@NonNull Package> packageToProcess = new LinkedList<@NonNull Package>();

    private Set<@NonNull Package> processedPackage = new HashSet<@NonNull Package>();

    public ClassRelationships(@NonNull EnvironmentFactory ocl) {
        mManager = ocl.getMetamodelManager();
        ((MetamodelManagerInternal)mManager).getASmetamodel();
        initializeMaps(ocl.getMetamodelManager().getASResourceSet());
    }

    private void initializeMaps(@NonNull ResourceSet resourceSet) {

        for (Resource resource : resourceSet.getResources()) {
            for (@SuppressWarnings("null") @NonNull Package aPackage : getInvolvedPackages(resource)) {
                Package pPackage = mManager.getPrimaryPackage(aPackage);
                if (!packageToProcess.contains(pPackage)) {
                    packageToProcess.add(pPackage);;
                }
            }
        }

        while (!packageToProcess.isEmpty()) {
            Package aPackage = packageToProcess.pop();
            computeClass2SuperClasses(aPackage);
        }

        for (Class type : class2superClasses.keySet()) {
            computeClass2SubClasses(type);
        }

        for (Class type : class2superClasses.keySet()) { // subtypes need to be previously computed
            computeClass2ContainerClasses(type);
        }
    }


    private void computeClass2SuperClasses(@NonNull Package p) {

        if (processedPackage.contains(p)) {
            return;
        }
        processedPackage.add(p);
        for (Class aClass : ClassUtil.nullFree(p.getOwnedClasses())) {
            computeClass2SuperClasses(aClass);
        }
        for (Package nestedPackage : ClassUtil.nullFree(p.getOwnedPackages())) {
            computeClass2SuperClasses(nestedPackage);
        }
    }

    /**
     * Tranisitively compute
     * @param aClass
     * @return a set with all the all classess
     */
    private @NonNull Set<@NonNull Class> computeClass2SuperClasses(@NonNull Class aClass) {

        //aClass = mManager.getPrimaryClass(aClass);
        Set<@NonNull Class> superRels = class2superClasses.get(aClass);
        if (superRels != null) {
            return superRels;
        } else {
            superRels = new LinkedHashSet<@NonNull Class>();
            class2superClasses.put(aClass, superRels);
        }

        // Super class inheritance might be shortcut
        for (@NonNull Class superClass : ClassUtil.nullFree(aClass.getSuperClasses())) {
            //superClass = mManager.getPrimaryClass(superClass);
            superRels.add(superClass);
            superRels.addAll(computeClass2SuperClasses(superClass));
        }

        Package classPackage = ClassUtil.nonNullState(aClass.getOwningPackage());
        Package pPackage = mManager.getPrimaryPackage(classPackage);
        if (!processedPackage.contains(pPackage) &&
            ! packageToProcess.contains(pPackage)) {
            packageToProcess.add(pPackage);
        }
        return superRels;
    }

    private void computeClass2SubClasses(Class aClass) {
        Set<@NonNull Class> superClasses = class2superClasses.get(aClass);
        if (superClasses != null) {
            for (Class superClass : superClasses) {
                if (aClass.getSuperClasses().contains(superClass)) {
                    Set<Class> directSubClasses = class2directSubClasses.get(superClass);
                    if (directSubClasses == null) {
                        directSubClasses = new LinkedHashSet<Class>();
                        class2directSubClasses.put(superClass, directSubClasses);
                    }
                    directSubClasses.add(aClass);
                }

                Set<Class> allSubClasses = class2allSubClasses.get(superClass);
                if (allSubClasses == null) {
                    allSubClasses = new LinkedHashSet<Class>();
                    class2allSubClasses.put(superClass, allSubClasses);
                }
                allSubClasses.add(aClass);
            }
        }
    }

    private void computeClass2ContainerClasses(@NonNull Class aClass) {

        for (Property property : aClass.getOwnedProperties()) {
            Type propType = getType(property);
            if (property.isIsComposite() && propType instanceof Class) {
                Class isClass = propType.isClass();
                assert isClass != null;
                addContainerClassForTypeAndSubtypes(aClass, property, isClass);
            }
        }
    }

    public @NonNull EnvironmentFactory getEnvironmentFactory() {
        return mManager.getEnvironmentFactory();
    }

    private List<Package> getInvolvedPackages(Resource resource) {

        List<Model> oclRoots = new ArrayList<Model>();
        for (EObject root : resource.getContents()) {
            if (root instanceof Model) {
                oclRoots.add((Model)root);
            }
        }

        List<Package> result = new ArrayList<Package>();
        for (Model root : oclRoots) {
            for (Package pckg : root.getOwnedPackages()) {
                result.add(pckg);
            }
        }
        return result;
    }

    protected Type getType(TypedElement typedElement) {
        Type type = typedElement.getType();
        if (type instanceof CollectionType) {
            type = ((CollectionType) type).getElementType();
        }
        return type;
    }

    private void addContainerClassForTypeAndSubtypes(@NonNull Class containerClass, @NonNull Property containmentProperty, @NonNull Class type) {

        //type = mManager.getPrimaryClass(type);
        Set<@NonNull ContainerClass> detailedContainerClasses = class2detailedContainerClasses.get(type);
        Set<@NonNull Class> containerClasses = class2containerClasses.get(type);
        if (detailedContainerClasses == null) {
            detailedContainerClasses = new LinkedHashSet<@NonNull ContainerClass>();
            class2detailedContainerClasses.put(type, detailedContainerClasses);
        }
        if (containerClasses == null) {
            containerClasses = new LinkedHashSet<@NonNull Class>();
            class2containerClasses.put(type, containerClasses);
        }

        detailedContainerClasses.add(new ContainerClass(containerClass, containmentProperty));
        containerClasses.add(containerClass);

        for (Class subType : getDirectSubClasses(type)) {
            addContainerClassForTypeAndSubtypes(containerClass, containmentProperty, subType);
        }
    }
//	public boolean typeIsSupertypeOf(Class t1, Class t2) {
//		Type primaryT1 = mManager.getPrimaryClass(t1);
//		Type primaryT2 = mManager.getPrimaryClass(t2);
//		return class2superClasses.get(primaryT1).contains(primaryT2);
//	}


    public @NonNull Set<@NonNull Class> getAllSuperClasses(@NonNull Class type) {
        Type primaryType = mManager.getPrimaryClass(type);
        Set<@NonNull Class> allSuperClasses = class2superClasses.get(primaryType);
        return allSuperClasses == null ? EMPTY_CLASS_SET
                : ClassUtil.nonNullState(Collections.<@NonNull Class>unmodifiableSet(allSuperClasses));
    }

    public @NonNull Set<@NonNull Class> getAllSubClasses(@NonNull Class type) {
        Type primaryType = mManager.getPrimaryClass(type);
        Set<@NonNull Class> allSubClasses = class2allSubClasses.get(primaryType);
        return allSubClasses == null ? EMPTY_CLASS_SET
                : ClassUtil.nonNullState(Collections.<@NonNull Class>unmodifiableSet(allSubClasses));
    }

    public @NonNull Set<@NonNull Class> getDirectSubClasses(@NonNull Class type) {
        Type primaryType = mManager.getPrimaryClass(type);
        Set<@NonNull Class> directSubClasses = class2directSubClasses.get(primaryType);
        return directSubClasses == null ? EMPTY_CLASS_SET
                : ClassUtil.nonNullState(Collections.<@NonNull Class>unmodifiableSet(directSubClasses));
    }

    public @NonNull Set<@NonNull Class> getContainerClasses(@NonNull Class type) {
        Type primaryType = mManager.getPrimaryClass(type);
        Set<@NonNull Class> containerClasses = class2containerClasses.get(primaryType);
        return containerClasses == null ? EMPTY_CLASS_SET
                : ClassUtil.nonNullState(Collections.<@NonNull Class>unmodifiableSet(containerClasses));
    }

    public @NonNull Set<@NonNull ContainerClass> getDetailedContainerClasses(@NonNull Class type) {
        Type primaryType = mManager.getPrimaryClass(type);
        Set<@NonNull ContainerClass> containerClasses = class2detailedContainerClasses.get(primaryType);
        return ClassUtil.nonNullState(containerClasses == null ? Collections.<@NonNull ContainerClass>emptySet()
                : Collections.<@NonNull ContainerClass>unmodifiableSet(containerClasses));
    }

    //==================== C3 Containment Linearization =====================//

    private static class LinearizationKey {
//        public DirectSuperclassesInspector directParentClassesReader;
        public Class type;

        public LinearizationKey(/*DirectSuperclassesInspector directParentClassesReader,*/ Class type) {
//            this.directParentClassesReader = directParentClassesReader;
            this.type = type;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((type == null)
                ? 0
                : type.hashCode());
            return result;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            LinearizationKey other = (LinearizationKey) obj;
            if (type == null) {
                if (other.type != null)
                    return false;
            } else if (!type.equals(other.type))
                return false;
            return true;
        }

    }

    private Map<LinearizationKey, Iterable<Class>> linearizations = java.util.Collections
            .synchronizedMap(new WeakHashMap<LinearizationKey, Iterable<Class>>());

    private Deque<Class> c3Root = new LinkedList<>();

    /**
     * Thrown when its not possible to linearize all superclasses.
     */
    public class JavaC3Exception extends Error {
        private static final long serialVersionUID = 1L;
        private final Iterable<@NonNull Class> partialResult;
        private final Iterable<List<Class>> remainingInputs;
//        private final DirectSuperclassesInspector dsc;

        protected JavaC3Exception(/*DirectSuperclassesInspector dsc,*/ Iterable<@NonNull Class> partialResult,
                Iterable<List<Class>> remainingInputs) {
            super("inconsistent precedence");
//            this.dsc = dsc;
            this.partialResult = partialResult;
            this.remainingInputs = remainingInputs;
        }

        /**
         * Gets the value of partialResult This is really for expert use only.
         * Its the value of partialResult at the point the linearization failed.
         *
         * @return the value of partialResult
         */
        public Iterable<Class> getPartialResult() {
            return this.partialResult;
        }

        /**
         * Gets the value of remainingInputs This is really for expert use only.
         * Its the value of remaining inputs at the point the linearization
         * failed.
         *
         * @return the value of remainingInputs
         */
        public Iterable<List<Class>> getRemainingInputs() {
            return this.remainingInputs;
        }

        @Override
        public String toString() {
            List<String> superclasses = Lists.newArrayListWithCapacity(Iterables.size(partialResult));
            for (Class c : partialResult) {
                superclasses.add(MessageFormat.format("    {0}: {1}", c, getContainerClasses(c)));
            }
            return MessageFormat.format("inconsistent precendence:\nsuperclasses:\n {0}\nremaining:\n   {1}",
                    Joiner.on("\n").join(superclasses, "\n"), remainingInputs);
        }
    }

    private Iterable<Class> mergeLists(List<@NonNull Class> partialResult,
            List<@NonNull List<@NonNull Class>> remainingInputs/*, final DirectSuperclassesInspector directParentClassesReader*/)
            throws JavaC3Exception {

        if (all(remainingInputs, equalTo(Collections.<@NonNull Class> emptyList()))) {
            return partialResult;
        }

        Optional<Class> nextOption = Optional.absent();
        for (Class c : Lists.reverse(partialResult)) {
            nextOption = Iterables.tryFind(getContainerClasses(c),
                    isCandidate(remainingInputs));
            if (nextOption.isPresent())
                break;
        }

        if (nextOption.isPresent()) {
            List<List<Class>> newRemainingInputs = Lists.newArrayList();
            Class next = nextOption.get();
            for (List<Class> input : remainingInputs) {
                newRemainingInputs.add(input.indexOf(next) == 0 ? input.subList(1, input.size()) : input);
            }

            return mergeLists(newArrayList(concat(partialResult, singletonList(next))), newRemainingInputs/*,
                    directParentClassesReader*/);
        } else {
            throw new JavaC3Exception(/*directParentClassesReader,*/ partialResult, remainingInputs);
        }
    }

    /**
     * To be a candidate for the next place in the linearization, you must be
     * the head of at least one list, and in the tail of none of the lists.
     *
     * @param remainingInputs
     *            the lists we are looking for position in.
     * @return true if the class is a candidate for next.
     */
    private <X> Predicate<X> isCandidate(final Iterable<List<X>> remainingInputs) {
        return new Predicate<X>() {

            Predicate<List<X>> headIs(final X c) {
                return new Predicate<List<X>>() {
                    @Override
                    public boolean apply(List<X> input) {
                        return !input.isEmpty() && c.equals(input.get(0));
                    }
                };
            }

            Predicate<List<X>> tailContains(final X c) {
                return new Predicate<List<X>>() {
                    @Override
                    public boolean apply(List<X> input) {
                        return input.indexOf(c) > 0;
                    }
                };
            }

            @Override
            public boolean apply(final X c) {
                return any(remainingInputs, headIs(c)) && all(remainingInputs, not(tailContains(c)));
            }
        };
    }

    private Iterable<Class> computeClassLinearization(@NonNull Class c/*, final DirectSuperclassesInspector dsc*/)
            throws JavaC3Exception {

        List<Class> cDirectContainers = new ArrayList<Class>(getContainerClasses(c));
        cDirectContainers.removeAll(c3Root);		// Avoid  loops
        Function<Class, List<Class>> cplList = new Function<Class, List<Class>>() {
            @Override
            public List<Class> apply(Class c) {
                return newArrayList(allContainerClasses(c/*, dsc*/));
            }
        };

        List<@NonNull Class> singletonC = Collections.<@NonNull Class> singletonList(c);
        List<@NonNull List<Class>> superContainers = Lists.transform(cDirectContainers, cplList);
        return mergeLists(singletonC,
                newArrayList(concat(superContainers, singletonList(cDirectContainers)))/*,
                dsc*/);
    }

//    /**
//     * Return the containment linearization of c, using the
//     * {@link DefaultDirectSuperclassesInspector}. The returned iterable will
//     * start with c, followed by the superclasses of c in linearization order.
//     */
//    public Iterable<Class> allSuperclasses(Class c) throws JavaC3Exception {
//        return allSuperclasses(c/*, DefaultDirectSuperclassesInspector.INSTANCE*/);
//    }

    /**
     * Return the linearization of c, using the
     * {@link DefaultDirectSuperclassesInspector}. The returned iterable will
     * start with {@link Object}, followed by the superclasses of c in
     * linearization order and end with c.
     */
    public Collection<Class> allContainerClassesReverse(Class c) throws JavaC3Exception {
        ArrayList<Class> result = new ArrayList<>();
        allContainerClasses(c/*, DefaultDirectSuperclassesInspector.INSTANCE*/).forEach(result::add);
        Collections.reverse(result);
        return result;
    }

    /**
     * Return the linearization of c. The returned iterable will start with c,
     * followed by the superclasses of c in linearization order.
     */
    public Iterable<Class> allContainerClasses(Class c/*, DirectSuperclassesInspector directParentClassesReader*/)
            throws JavaC3Exception {

        c3Root.push(c);
        LinearizationKey key = new LinearizationKey(/*directParentClassesReader,*/ c);
        Iterable<Class> linearization = linearizations.get(key);
        if (linearization == null) {
            linearization = computeClassLinearization(c/*, directParentClassesReader*/);
            linearizations.put(key, linearization);
        }
        c3Root.pop();
        return linearization;
    }

    /**
     * The class linearizations are cached in a static map. This cache can lead
     * to classloader leaks. By calling this class, the cache is flushed.
     */
    public void clearCache() {
        linearizations.clear();
    }

}
