Tests in this Folder
====================

HSV1
----
**HSV2HSLRoot**
a.  One HSVNode, no childs
b.  Two HSVNode, no childs

HSV2
----
**HSV2HSLRoot** + **HSV2HSLRecursion**
a.  Two HSVNodes, one contains the other
a.  Three HSVNodes, three level containment
b.  Four HSVNodes, contained in pairs
