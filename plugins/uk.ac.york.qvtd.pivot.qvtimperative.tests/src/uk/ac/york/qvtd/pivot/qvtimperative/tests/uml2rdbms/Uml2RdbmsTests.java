/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.pivot.qvtimperative.tests.uml2rdbms;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.utilities.QvtMtcExecutionException;
import uk.ac.york.qvtd.pivot.qvtimperative.tests.MallardQvtiTests;
import uk.ac.york.qvtd.pivot.qvtimperative.tests.ModelIndex;


/**
 * Unit tests for the Uml2Rdbms transformation. TEsts with _MP suffix, test the same rules as their suffixless
 * counter part, but have multiple packages at the root.
 *
 * @author Horacio Hoyos
 *
 */
@RunWith(Parameterized.class)
public class Uml2RdbmsTests extends MallardQvtiTests {

    public static Logger logger = LoggerFactory.getLogger(Uml2RdbmsTests.class.getName());

    public Uml2RdbmsTests(String qvtiasUri) {
        super(qvtiasUri, "plugins/uk.ac.york.qvtd.pivot.qvtimperative.tests/src/uk/ac/york/qvtd/pivot/qvtimperative/tests/uml2rdbms");
    }

    @Parameters(name = "{index}: qvtias={0}")
    public static Iterable<Object[]> data() {
        ArrayList<Object[]> result = new ArrayList<Object[]>();
        String qvtiasListPath = "/ACO/Uml2Rdbms.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
        qvtiasListPath = "/SLOPPY/Uml2Rdbms.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
        return result;

    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private String[] getModelNames(String modelCase) throws SecurityException, IOException {

        return getModelNames(modelCase, "UML", "RDBMS");
    }

    @Test
    public void testPackageToSchema() throws Exception {

        String[] models = getModelNames("01a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_MP() throws Exception {

        String[] models = getModelNames("01b");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable() throws Exception {

        String[] models = getModelNames("02a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_MP() throws Exception {

        String[] models = getModelNames("02b");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_associationToForeignKey() throws Exception {

        String[] models = getModelNames("03a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_associationToForeignKey_MP() throws Exception {

        String[] models = getModelNames("03b");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_associationToForeignKey_XP_MP() throws Exception {

        String[] models = getModelNames("03c");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_associationToForeignKey_DC_inheritance() throws Exception {

        String[] models = getModelNames("03d");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_associationToForeignKey_SC_inheritance() throws Exception {

        String[] models = getModelNames("03e");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_associationToForeignKey_DC_SC_inheritance() throws Exception {

        String[] models = getModelNames("03f");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }


    @Test
    public void testPackageToSchema_and_classToTable_and_classPrimitiveAttributes() throws Exception {

        String[] models = getModelNames("04a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_classPrimitiveAttributes_MP() throws Exception {

        String[] models = getModelNames("04b");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_classPrimitiveAttributes_Inheritance() throws Exception {

        String[] models = getModelNames("04c");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_classPrimitiveAttributes_Inheritance_XP() throws Exception {

        String[] models = getModelNames("04d");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_classComplexAttributes() throws Exception {

        String[] models = getModelNames("05a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_classComplexAttributes_Inheritance() throws Exception {

        String[] models = getModelNames("05b");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testPackageToSchema_and_classToTable_and_classComplexAttributes_Inheritance_XP() throws Exception {

        String[] models = getModelNames("05c");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }



    private void runAndCompare(String inputModel, String oracleModel, String outputModel, String traceSegment)
            throws QvtMtcExecutionException, IOException, InterruptedException {

        Map<String, String> inputModels = new HashMap<String, String>();
        inputModels.put("uml", inputModel);
        runAndCompare(oracleModel, outputModel, traceSegment, inputModels, "rdbms");
    }


}
