/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.pivot.qvtimperative.tests;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * The Class SchemaLocationFixer.
 */
public class SchemaLocationFixer {

    /**
      * Csv to list.
      *
      * @param csvLine the csv line
      * @return the string[]
      */
     // Utility which converts CSV to ArrayList using Split Operation
     public static String[] csvToList(String csvLine) {
         String[] values = new String[]{};
         if (csvLine != null) {
             values = csvLine.split("\\s*,\\s*");
         }
         return values;
     }

    /**
     * Fix files for folder.
     *
     * @param folder the folder
     * @param schemaMap the schema map
     * @param docBuilder the doc builder
     * @param transformer the transformer
     * @throws SAXException the SAX exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws TransformerException the transformer exception
     */
    public static void fixFilesForFolder(final File folder, Map<String, String> schemaMap, DocumentBuilder docBuilder, Transformer transformer) throws SAXException, IOException, TransformerException {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                fixFilesForFolder(fileEntry, schemaMap, docBuilder, transformer);
            } else {
                String filepath = fileEntry.getAbsolutePath();
                String ext = filepath.replaceAll("^.*\\.(.*)$", "$1");
                if (ext.equals("xmi")) {
                    Document doc = docBuilder.parse (fileEntry);
                    Node head = doc.getFirstChild();
                    NamedNodeMap earthAttributes = head.getAttributes();
                    Node sl = earthAttributes.getNamedItemNS("http://www.w3.org/2001/XMLSchema-instance", "schemaLocation");
                    if (sl == null) {
                        System.out.println("Fixing " + filepath);
                        // Find the metamodel uri
                        Node mmNode = null;
                        for (int i = 0; i < earthAttributes.getLength(); i++)
                        {
                            Node node = earthAttributes.item(i);
                            if (node.getNodeType() == Node.ATTRIBUTE_NODE)
                            {
                                String name = node.getNodeName();
                                if ("http://www.w3.org/2000/xmlns/".equals(node.getNamespaceURI())) {
                                    mmNode = node;
                                    break;
                                }
                            }
                        }
                        //String value = getMetamodelLocation(mmNode.getNodeValue());
                        String value = schemaMap.get(mmNode.getNodeValue());
                        if (value == null) {
                            System.out.println("Coudl not find a schema location for uri  " + mmNode.getNodeValue());
                        }
                        ((Element)head).setAttributeNS("http://www.w3.org/2000/xmlns/","xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
                        ((Element)head).setAttributeNS("http://www.w3.org/2001/XMLSchema-instance","xsi:schemaLocation", value);
                        DOMSource source = new DOMSource(doc);
                        StreamResult result = new StreamResult(new File(filepath));
                        transformer.transform(source, result);
                    }
                }
            }
        }
    }

     /**
     * arg[0] The path to the folder where resources are being fixed. A SchemaLocation.csv file must exist in the
     * folder. This file mus contain uri,location pairs. The location should be relative to the folder.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {

        final File folder = new File(args[0]);
        Map<String, String> schemaMap = createSchemaMapping(args[0] + "\\SchemaLocation.csv");
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        docFactory.setNamespaceAware(true);
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        fixFilesForFolder(folder, schemaMap, docBuilder, transformer);
        System.out.println("Done");
    }

    /**
     * Creates the schema mapping.
     *
     * @param schemaMapFile the schema map file
     * @return the map
     */
    private static Map<String, String> createSchemaMapping(String schemaMapFile) {
        BufferedReader buffer = null;
        Map<String, String> result = new HashMap<String, String>();
        try {
            String csvLine;
            buffer = new BufferedReader(new FileReader(schemaMapFile));
            while ((csvLine = buffer.readLine()) != null) {
                String[] values = csvToList(csvLine);
                String uri = values[0];
                String location = uri + " " + values[1];
                result.put(uri, location);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (buffer != null) buffer.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }
}
