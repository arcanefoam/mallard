Tests in this Folder
====================

UML1
----
**packageToSchema**
a.  One Package
b.  Two Packages

UML2
----
**packageToSchema** + **classToTable**
a.  One Package, two Classes, one persistent one not
b.  Same as previous, but in two packages. Alternate persistent classes

UML3
----
**packageToSchema** + **classToTable** + associationToForeignKey
a.  One Package, three classes. C1 has an association to C2 and another to C3.
    C2 has an association to C3. C2 is not persistent, hence we only expect
    C1's table to have a FK to C3's table.
b.  Same as previous, but in two packages. Alternate associations
c.  Same as previous, but in two packages. Associations between packages
d.  Three classes. C1 has an association to C2. C3 inherits from C2
e.  Three classes. C1 has an association to C3. C2 inherits from C1
f.  Four classes. C1 has an association to C3. C2 inherits from C1 and
    C4 inherits from C3. 


UML4
----
**packageToSchema** + **classToTable** + **classPrimitiveAttributes**
a.  One package, three primitive types, three classes with a mix of attributes
b.  Same as previous, but in two packages. Just three attributes per class.
c.  One package, three primitive types, two classes with a mix of attributes, C2
    inherits from C1.
d.  Two Packages, class in P1 uses types from P2 and viceversa    

*Note* classComplexAttributes and complexAttributeComplexAttributes do not
result in visible changes in the output table. The must be tested with
complexAttributePrimitiveAttributes

UML5
-----
**packageToSchema** + **classToTable** + **classComplexAttributes** + **complexAttributePrimitiveAttributes**
a.  One package, two classes. C1 has two attributes of type C2, and C2 has a primitive attribute
b.  Same as above but we introduce C3, which inherits from C1.
c.  Two packages, four classes. P1C1 has an attribute of type P2C1, and P2C1 has a primitive attribute
    P1C1 has an attribute of type P2C2 which in turn has an attribute of type P1C2. P1C2 has two primitive attributes.
    
