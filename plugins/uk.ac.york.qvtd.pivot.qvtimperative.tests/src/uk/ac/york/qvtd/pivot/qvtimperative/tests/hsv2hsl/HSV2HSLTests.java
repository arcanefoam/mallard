/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.pivot.qvtimperative.tests.hsv2hsl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import uk.ac.york.qvtd.compiler.utilities.QvtMtcExecutionException;
import uk.ac.york.qvtd.pivot.qvtimperative.tests.MallardQvtiTests;
import uk.ac.york.qvtd.pivot.qvtimperative.tests.ModelIndex;


/**
 * Unit tests for the Families2Persons transformation.
 *
 * @author Horacio Hoyos
 *
 */
@RunWith(Parameterized.class)
public class Hsv2HslTests extends MallardQvtiTests {

    //private static Logger logger = LoggerFactory.getLogger(Families2PersonsTests.class.getName());

    public Hsv2HslTests(String qvtiasUri) {
        super(qvtiasUri, "plugins/uk.ac.york.qvtd.pivot.qvtimperative.tests/src/uk/ac/york/qvtd/pivot/qvtimperative/tests/hsv2hsl");
    }

    @Parameters(name = "{index}: qvtias={0}")
    public static Iterable<Object[]> data() {
        ArrayList<Object[]> result = new ArrayList<Object[]>();
        String qvtiasListPath = "/COMPLETE/HSV2HSL.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
        qvtiasListPath = "/ACO/HSV2HSL.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
        qvtiasListPath = "/SLOPPY/HSV2HSL.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
        return result;
    }
    
    private String[] getModelNames(String modelCase) throws SecurityException, IOException {

        return getModelNames(modelCase, "HSV", "HSL");
    }

    @Test
    public void testHSV_Root() throws Exception {

        String[] models = getModelNames("01a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    private void runAndCompare(String inputModel, String oracleModel, String outputModel, String traceSegment)
            throws QvtMtcExecutionException, IOException, InterruptedException {
        Map<String, String> inputModels = new HashMap<String, String>();
        inputModels.put("hsv", inputModel);
        runAndCompare(oracleModel, outputModel, traceSegment, inputModels, "hsl");
    }

}
