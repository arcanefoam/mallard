/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.pivot.qvtimperative.tests;
// TODO: Auto-generated Javadoc

/**
 * The Enum ModelIndex.
 */
public enum ModelIndex {

        /** The input model index. */
        INPUT_MODEL_INDEX,

        /** The oracle model index. */
        ORACLE_MODEL_INDEX,

        /** The output model index. */
        OUTPUT_MODEL_INDEX,

        /** The trace model index. */
        TRACE_MODEL_INDEX;
    }