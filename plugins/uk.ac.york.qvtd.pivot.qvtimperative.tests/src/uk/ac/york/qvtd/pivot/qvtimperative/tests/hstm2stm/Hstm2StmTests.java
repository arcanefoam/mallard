/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.pivot.qvtimperative.tests.hstm2stm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.utilities.QvtMtcExecutionException;
import uk.ac.york.qvtd.pivot.qvtimperative.tests.MallardQvtiTests;
import uk.ac.york.qvtd.pivot.qvtimperative.tests.ModelIndex;


/**
 * Unit tests for the Uml2Rdbms transformation. TEsts with _MP suffix, test the same rules as their suffixless
 * counter part, but have multiple packages at the root.
 *
 * @author Horacio Hoyos
 *
 */
@RunWith(Parameterized.class)
public class Hstm2StmTests extends MallardQvtiTests {

    public static Logger logger = LoggerFactory.getLogger(Hstm2StmTests.class.getName());

    public Hstm2StmTests(String qvtiasUri) {
        super(qvtiasUri, "plugins/uk.ac.york.qvtd.pivot.qvtimperative.tests/src/uk/ac/york/qvtd/pivot/qvtimperative/tests/hstm2stm");
    }

    @Parameters(name = "{index}: qvtias={0}")
    public static Iterable<Object[]> data() {
        ArrayList<Object[]> result = new ArrayList<Object[]>();
        String qvtiasListPath = "/COMPLETE/Hstm2Stm.param";
//        for (String path : getAstsFromFolder(qvtiasListPath)) {
//            result.add(new String[]{path});
//        }
        qvtiasListPath = "/ACO/Hstm2Stm.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
//        qvtiasListPath = "/SLOPPY/Hstm2Stm.param";
//        for (String path : getAstsFromFolder(qvtiasListPath)) {
//            result.add(new String[]{path});
//        }
        return result;

    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    private String[] getModelNames(String modelCase) throws SecurityException, IOException {

        return getModelNames(modelCase, "Hstm", "Stm");
    }

    @Test
    public void testLStateToState_Single() throws Exception {

        String[] models = getModelNames("01a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testLStateToState_Multiple() throws Exception {

        String[] models = getModelNames("01b");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testLStateToState_and_CStateToState_CTransition() throws Exception {

        String[] models = getModelNames("02a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testLStateToState_and_CStateToState_LeafTransition() throws Exception {

        String[] models = getModelNames("02b");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testLStateToState_and_CStateToState_LeafTransitions() throws Exception {

        String[] models = getModelNames("02c");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

//    @Test
//    public void testLStateToState_and_CStateToState_Samek() throws Exception {
//
//        String[] models = getModelNames("02d");
//        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
//                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
//                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
//                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
//    }

    private void runAndCompare(String inputModel, String oracleModel, String outputModel, String traceSegment)
            throws QvtMtcExecutionException, IOException, InterruptedException {

        Map<String, String> inputModels = new HashMap<String, String>();
        inputModels.put("hstm", inputModel);
        super.runAndCompare(oracleModel, outputModel, traceSegment, inputModels, "stm");
    }


}
