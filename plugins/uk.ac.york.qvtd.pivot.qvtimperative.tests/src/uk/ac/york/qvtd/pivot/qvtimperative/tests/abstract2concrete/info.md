Tests in this Folder
====================

Families1
----
**MemberToMale**
a.  One member, father of a Family
b.  One member, son of a Family

Families2
----
**MemberToFemale**
a.  One member, mother of a Family
b.  One member, daughter of a Family

Families3
----
**MemberToMale**
a.  Two members, one father to family F1, one son to F1 and father to F2

Families4
----
**MemberToFemale**
a.  Two members, one mother to family F1, one daughter to F1 and mother to F2

Families5
----
**MemberToMale** + **MemberToFemale**
a.  Three generations of families, mixing two branches