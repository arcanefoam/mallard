/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.pivot.qvtimperative.tests.abstract2concrete;

import java.util.ArrayList;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import uk.ac.york.qvtd.pivot.qvtimperative.tests.MallardQvtiTests;

@RunWith(Parameterized.class)
public class Abstract2ConcrereTests extends MallardQvtiTests {

	public Abstract2ConcrereTests(String qvtiasUri) {
		super(qvtiasUri, "plugins/uk.ac.york.qvtd.pivot.qvtimperative.tests/src/uk/ac/york/qvtd/pivot/qvtimperative/tests/abstract2concrete");
	}
	
	@Parameters(name = "{index}: qvtias={0}")
    public static Iterable<Object[]> data() {
        ArrayList<Object[]> result = new ArrayList<Object[]>();
        String qvtiasListPath = "/COMPLETE/Abstract2Concrete.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
        qvtiasListPath = "/ACO/Abstract2Concrete.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
        qvtiasListPath = "/SLOPPY/Abstract2Concrete.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
        return result;
    }
    
    
}
