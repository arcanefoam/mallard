/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.pivot.qvtimperative.tests.families2persons;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import uk.ac.york.qvtd.compiler.utilities.QvtMtcExecutionException;
import uk.ac.york.qvtd.pivot.qvtimperative.tests.MallardQvtiTests;
import uk.ac.york.qvtd.pivot.qvtimperative.tests.ModelIndex;


/**
 * Unit tests for the Families2Persons transformation.
 *
 * @author Horacio Hoyos
 *
 */
@RunWith(Parameterized.class)
public class Families2PersonsTests extends MallardQvtiTests {

    //private static Logger logger = LoggerFactory.getLogger(Families2PersonsTests.class.getName());

    public Families2PersonsTests(String qvtiasUri) {
        super(qvtiasUri, "plugins/uk.ac.york.qvtd.pivot.qvtimperative.tests/src/uk/ac/york/qvtd/pivot/qvtimperative/tests/families2persons");
    }

    @Parameters(name = "{index}: qvtias={0}")
    public static Iterable<Object[]> data() {
        ArrayList<Object[]> result = new ArrayList<Object[]>();
        String qvtiasListPath = "/COMPLETE/Families2Persons.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
        qvtiasListPath = "/SLOPPY/Families2Persons.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
        qvtiasListPath = "/ACO/Families2Persons.param";
        for (String path : getAstsFromFolder(qvtiasListPath)) {
            result.add(new String[]{path});
        }
        return result;

    }

    private String[] getModelNames(String modelCase) throws SecurityException, IOException {

        return getModelNames(modelCase, "Families", "Persons");
    }

    @Test
    public void testMemberToMale_Father() throws Exception {

        String[] models = getModelNames("01a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testMemberToMale_Son() throws Exception {

        String[] models = getModelNames("01b");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testMemberToFemale_Mother() throws Exception {

        String[] models = getModelNames("02a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testMemberToFemale_Daughter() throws Exception {

        String[] models = getModelNames("02b");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testMemberToMale_Two_Generations() throws Exception {

        String[] models = getModelNames("03a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testMemberToFemale_Two_Generations() throws Exception {

        String[] models = getModelNames("04a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    @Test
    public void testMemberToMale_and_MemberToFemale_Three_Generations() throws Exception {

        String[] models = getModelNames("05a");
        runAndCompare(models[ModelIndex.INPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.ORACLE_MODEL_INDEX.ordinal()],
                models[ModelIndex.OUTPUT_MODEL_INDEX.ordinal()],
                models[ModelIndex.TRACE_MODEL_INDEX.ordinal()]);
    }

    private void runAndCompare(String inputModel, String oracleModel, String outputModel, String traceSegment)
            throws QvtMtcExecutionException, IOException, InterruptedException {
        Map<String, String> inputModels = new HashMap<String, String>();
        inputModels.put("family", inputModel);
        runAndCompare(oracleModel, outputModel, traceSegment, inputModels, "person");
    }

}
