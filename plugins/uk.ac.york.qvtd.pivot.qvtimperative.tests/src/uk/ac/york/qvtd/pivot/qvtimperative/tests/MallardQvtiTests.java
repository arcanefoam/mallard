/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.pivot.qvtimperative.tests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.AttributeChange;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.diff.DiffBuilder;
import org.eclipse.emf.compare.diff.IDiffEngine;
import org.eclipse.emf.compare.diff.IDiffProcessor;
import org.eclipse.emf.compare.match.DefaultComparisonFactory;
import org.eclipse.emf.compare.match.DefaultEqualityHelperFactory;
import org.eclipse.emf.compare.match.DefaultMatchEngine;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IEqualityHelperFactory;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.EqualityHelper;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.model.OCLstdlib;
import org.eclipse.ocl.xtext.base.services.BaseLinkingService;
import org.eclipse.qvtd.pivot.qvtcore.QVTcorePivotStandaloneSetup;
import org.eclipse.qvtd.pivot.qvtimperative.QVTimperativePivotStandaloneSetup;
import org.eclipse.qvtd.pivot.qvtimperative.utilities.QVTimperative;
import org.eclipse.qvtd.xtext.qvtbase.tests.LoadTestCase;
import org.eclipse.qvtd.xtext.qvtbase.tests.utilities.TestsXMLUtil;
import org.eclipse.qvtd.xtext.qvtcore.QVTcoreStandaloneSetup;
import org.junit.After;
import org.junit.Before;
import org.slf4j.LoggerFactory;

import com.google.common.cache.LoadingCache;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.RollingPolicy;
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy;
import ch.qos.logback.core.util.FileSize;
import uk.ac.york.qvtd.benchmark.MallardDiffEngine;
import uk.ac.york.qvtd.compiler.Configuration;
import uk.ac.york.qvtd.compiler.Configuration.Mode;
import uk.ac.york.qvtd.compiler.MallardCompiler;
import uk.ac.york.qvtd.compiler.tests.MallardMtcTests;
import uk.ac.york.qvtd.compiler.utilities.MallardMtcUtil;
import uk.ac.york.qvtd.compiler.utilities.QvtMtcExecutionException;
import uk.ac.york.qvtd.pivot.qvtimperative.evaluation.MallardEngine;
import uk.ac.york.qvtd.pivot.qvtimperative.utilities.MallardEqualityHelper;

/**
 * The Class MallardQvtiTests.
 */
public class MallardQvtiTests extends LoadTestCase {


    public static Logger logger = (Logger) LoggerFactory.getLogger(MallardQvtiTests.class);

    private @NonNull QVTimperative myQVT;
    private String outputModelAbsolutePath;
    private Resource oracleModel;
    private Resource outputModel;
    private String qvtiasUri;
    private String samplesPath;

    /**
     * @param qvtiasUri
     * @param samplesPath TODO
     */
    public MallardQvtiTests(String qvtiasUri, String samplesPath) {
        super();
        this.qvtiasUri = qvtiasUri;
        this.samplesPath = samplesPath;
    }

    private static List<String> getQvtiasts(InputStream is) throws IOException, CoreException {

        List<String> records = new ArrayList<String>();
        BufferedReader file = new BufferedReader(new InputStreamReader(is));
        String record;
        while ((record = file.readLine()) != null) {
            //String fields[] = record.split(",");
            records.add(record);
        }
        file.close();
        return records;
    }

    public static List<String> getAstsFromFolder(String qvtiasListPath) {

        URL r = MallardMtcTests.class.getResource(qvtiasListPath);
        InputStream is = null;
        try {
            is = r.openStream();
        } catch (IOException e) {
            // No QVTi files for this test
            e.printStackTrace();
            return Collections.emptyList();
        }
        List<String> completeTest = new ArrayList<String>();
        if (is != null) {
            try {
                completeTest.addAll(getQvtiasts(is));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (CoreException e) {
                e.printStackTrace();
            }
        }
        return completeTest;
    }


    public void execute(String qvtias, String direction, Map<String, String> inputSegments,
          String outputSegment, String oracleSegment, String traceSegment, String samplesPath, Mode mode)
          throws QvtMtcExecutionException, IOException, InterruptedException {

        // The paths include the eclipse project parent folder (e.g. tests) for running
        // standalone, we need to remove it
        String[] x = samplesPath.split("/");
        samplesPath = String.join("/", Arrays.copyOfRange(x, 1, x.length));

        URI qvtiasUri = URI.createURI(qvtias);
        URI modelsUri = URI.createPlatformResourceURI(samplesPath, true);
        URI oracleURI = modelsUri.appendSegment(oracleSegment);
        String genPath = MallardMtcUtil.changeTargetToBinFolder(modelsUri.toString());
        URI genModelsUri = URI.createURI(genPath, true);
        // The generated models go to the output
        URI outputModelURI = genModelsUri.appendSegment(outputSegment);
        URI middleModelURI = genModelsUri.appendSegment(traceSegment);
        // The MallardEngine is responsible for loading the models, creating the configuration
        // and execution of the qvtias
        @NonNull Map<Object, Object> savingOptions = TestsXMLUtil.defaultSavingOptions;
        savingOptions.put(XMIResource.OPTION_SCHEMA_LOCATION, true);
        Resource qvtiAsModel = MallardMtcUtil.loadASModelFromUri(qvtiasUri, myQVT.getEnvironmentFactory());
        Configuration config = MallardCompiler.createConfiguration(qvtiAsModel, mode, direction);
        for (Entry<String, String> entry : inputSegments.entrySet()) {
            URI inputModelURI = modelsUri.appendSegment(entry.getValue());
            config.addInputModel(entry.getKey(), inputModelURI);
        }
        config.addMiddleModel(middleModelURI);
        config.addOutputModel(outputModelURI);
        boolean success = false ;
        MallardEngine engine = new MallardEngine();
        success = engine.execute(qvtiAsModel, config, myQVT.getEnvironmentFactory(), true, savingOptions);
        assertTrue(success);
        outputModel = myQVT.getEnvironmentFactory().getResourceSet().getResource(outputModelURI, true);
        oracleModel = myQVT.getEnvironmentFactory().getResourceSet().getResource(oracleURI, true);
  }


    /**
     * @return the oracleModel
     */
    protected Resource getOracleModel() {
        return oracleModel;
    }

    /**
     * @return the outputModel
     */
    protected Resource getOutputModel() {
        return outputModel;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.xtext.qvtbase.tests.LoadTestCase#setUp()
     */
    @Before
    public void setUp() throws Exception {

        BaseLinkingService.DEBUG_RETRY.setState(true);
        super.setUp();
        OCLstdlib.install();
        QVTcoreStandaloneSetup.doSetup();
        QVTcorePivotStandaloneSetup.doSetup();
        QVTimperativePivotStandaloneSetup.doSetup();
        myQVT = createQVT();
        assert myQVT != null;

    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.xtext.qvtbase.tests.LoadTestCase#tearDown()
     */
    @After
    public void tearDown() throws Exception {

        myQVT.getEnvironmentFactory().getResourceSet().getResources().remove(oracleModel);
        myQVT.getEnvironmentFactory().getResourceSet().getResources().remove(outputModel);
        myQVT.dispose();
        super.tearDown();
    }



    protected @NonNull QVTimperative createQVT() {
        return QVTimperative.newInstance(getProjectMap(), null);
    }


    /**
     * Configure the logger so each test gets a separate log file.
     *
     * @param qvtiasName the qvtias name
     * @param testModel the test model
     * @throws SecurityException the security exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void configureLogger(String qvtiasName, String testModel) throws SecurityException, IOException {

        // Remove all handlers (i.e. previous executions)
        //Get Root logger
        Logger root = (Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        //FIXME This should be a parameter
        root.setLevel(Level.INFO);
        RollingFileAppender<ILoggingEvent> fileAppender = (RollingFileAppender<ILoggingEvent>) root.getAppender("FILE");
        RollingFileAppender<ILoggingEvent> executionAppender = (RollingFileAppender<ILoggingEvent>) root.getAppender("QVT");
        if (fileAppender != null) {
            RollingPolicy policy = fileAppender.getRollingPolicy();
            policy.stop();
            fileAppender.stop();
        }
        if (executionAppender != null) {
            RollingPolicy policy = executionAppender.getRollingPolicy();
            policy.stop();
            executionAppender.stop();
        }
        root.detachAppender("FILE");
        root.detachAppender("QVT");
        // Log to the MallardTests/qvti_execution/ folder, use one log per test
        StringBuilder logFileBuilder = new StringBuilder();
        logFileBuilder.append(System.getenv("HOME"));
        logFileBuilder.append("/MallardTests/qvti_execution/");
        logFileBuilder.append(qvtiasName);
        logFileBuilder.append("_");
        logFileBuilder.append(testModel);
        // General file appender
        fileAppender = createFileAppender(logFileBuilder, "FILE");
        root.addAppender(fileAppender);
        // Execution file appender
        logFileBuilder.append("_QVTiExecution");
        executionAppender = createFileAppender(logFileBuilder, "QVT");
        root.addAppender(executionAppender);
    }

        /**
     * Creates the file appender.
     *
     * @param logFileBuilder the log file builder
     * @param name the name
     * @return the rolling file appender
     */
    private RollingFileAppender<ILoggingEvent> createFileAppender(StringBuilder logFileBuilder, String name) {
        RollingFileAppender<ILoggingEvent> fileAppender;
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        PatternLayoutEncoder ple = new PatternLayoutEncoder();
        ple.setPattern("%d{HH:mm:ss.SSS} [%thread] %-5level %logger{5} - %msg%n");
        ple.setContext(lc);
        ple.start();
        String fnp = logFileBuilder.toString() + ".%i.log";
        FixedWindowRollingPolicy rollPolicy = new FixedWindowRollingPolicy();
        rollPolicy.setMinIndex(1);
        rollPolicy.setMaxIndex(3);
        rollPolicy.setFileNamePattern(fnp);
        SizeBasedTriggeringPolicy<ILoggingEvent> trigPolicy = new SizeBasedTriggeringPolicy<ILoggingEvent>();
        trigPolicy.setMaxFileSize("5MB");
        trigPolicy.start();
        fileAppender = new RollingFileAppender<ILoggingEvent>();
        fileAppender.setName(name);
        String fileName = logFileBuilder.toString() + ".log";
        fileAppender.setFile(fileName);
        fileAppender.setEncoder(ple);
        fileAppender.setContext(lc);
        fileAppender.setRollingPolicy(rollPolicy);
        rollPolicy.setParent(fileAppender);
        fileAppender.setTriggeringPolicy(trigPolicy);
        fileAppender.start();
        return fileAppender;
    }


    protected String[] getModelNames(String modelCase, String input, String output) throws SecurityException, IOException {

        String[] x = qvtiasUri.split("/");
        String qvtiasName = x[x.length-1];
        configureLogger(qvtiasName, modelCase);
        String name = URI.createFileURI(qvtiasUri).trimFileExtension().lastSegment();
        String inputModel = input + "_" + modelCase + ".xmi";
        String oracleModel = output + "_" + modelCase + "_oracle.xmi";
        String outputModel = output + "_" + modelCase + "_" + name + ".xmi";
        String traceModel = input + "2" + output + "_" + modelCase + "_trace_" + name + ".xmi";
        return new String[]{inputModel, oracleModel, outputModel, traceModel};
    }


    /**
     * Compare the generated model against the oracle
     * @param xmi1
     * @param xmi2
     * @return
     */
    protected Comparison compare(Resource outputModel, Resource oracleModel) {

        // Configure EMF Compare
        IEObjectMatcher matcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.NEVER);

        IEqualityHelperFactory helperFactory = new DefaultEqualityHelperFactory() {
            @Override
            public org.eclipse.emf.compare.utils.IEqualityHelper createEqualityHelper() {
                final LoadingCache<EObject, URI> cache = EqualityHelper.createDefaultCache(getCacheBuilder());
                return new MallardEqualityHelper(4, cache);
            }
        };
        //IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
        IComparisonFactory comparisonFactory = new DefaultComparisonFactory(helperFactory);
        IDiffProcessor diffProcessor = new DiffBuilder();
        IDiffEngine diffEngine = new MallardDiffEngine(diffProcessor);
        IMatchEngine.Factory matchEngineFactory = new MatchEngineFactoryImpl(matcher, comparisonFactory);
        matchEngineFactory.setRanking(20);
        IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
        matchEngineRegistry.add(matchEngineFactory);
        EMFCompare comparator = EMFCompare.builder().setDiffEngine(diffEngine).setMatchEngineFactoryRegistry(matchEngineRegistry).build();
        // Compare the two models
        IComparisonScope scope = new DefaultComparisonScope(outputModel, oracleModel, null);
        return comparator.compare(scope);
    }


    protected void runAndCompare(String oracleModel, String outputModel, String traceSegment, Map<String, String> inputModels, String direction)
            throws QvtMtcExecutionException, IOException, InterruptedException {
        execute(qvtiasUri, direction, inputModels, outputModel, oracleModel ,
                traceSegment, samplesPath, Mode.ENFORCE);
        Comparison result = compare(getOutputModel(), getOracleModel());
        if (!result.getDifferences().isEmpty()) {
            for (Diff dif : result.getDifferences()) {
                //System.out.println(dif);
                // add switch case on the dif kind to adjust message?
                //StringBuilder sb = new StringBuilder();
                //sb.append("Generated model is missing element ");
//                //sb.append(dif.get)
//                Objects.toStringHelper(this)
//				.add("reference", getReference().getEContainingClass().getName() + "." + getReference().getName())
//				.add("value", EObjectUtil.getLabel(getValue()))
//				.add("parentMatch", getMatch().toString())
//				.add("match of value", getMatch().getComparison().getMatch(getValue()))).toString();
                if (dif instanceof ReferenceChange) {
                    //System.out.println(((ReferenceChange) dif).getReference());
                    String feature = MallardDiffEngine.getLabel(((ReferenceChange)dif).getValue());
                    String owner = MallardDiffEngine.getLabel(((ReferenceChange)dif).getValue().eContainer());
                    String reason = "";
                    switch (dif.getKind()) {
                        case DELETE:
                            reason = "is missing from";
                            break;
                        case ADD:
                            reason = "is additional in";
                            break;
                        case CHANGE:
                            reason = "has changed in";
                            break;
                        case MOVE:
                            reason = "has moved in";
                            break;
                    }
                    logger.error(String.format("Structural feature %s, in %s, %s the output model", feature, owner, reason));
                }
                else if (dif instanceof AttributeChange) {
                    String value = (((AttributeChange)dif).getValue()).toString();
                    String attribute = MallardDiffEngine.getLabel(((AttributeChange)dif).getAttribute());
                    String owner = MallardDiffEngine.getLabel(dif.getMatch().getRight());
                    String reason = "";
                    switch (dif.getKind()) {
                        case DELETE:
                            reason = "was transformed incorrectly";
                            break;
                        case ADD:
                            reason = "has diffrent value";
                            break;
                        case CHANGE:
                            reason = "has diffrent value";
                            break;
                        case MOVE:
                            reason = "has moved in";
                            break;
                    }
                    logger.error(String.format("Attribute %s = %s, in %s, %s.", attribute, value, owner, reason));
                }
                else {
                    logger.error("We need messages for the other cases");

                }
            }
            assertTrue("Differences from the Oracle model found ", false);
        }
    }
}
