Tests in this Folder
====================

Hstm 1
------
**LStateToState**
a.  One Leaf State
b.  Two Leaf States

Hstm 2
------
**LStateToState** + **CStateToState**
a.  Two Container States, each with a Leaf State, a transition between the
    containers
b.  Two Container States, each with a Leaf State, a transition between the
    c1 and l2
c.  Two Container States, each with a Leaf State, a transition between the
    c1 and l2, and another between c2 and l1
d.  Samek Hierarchical State Machine