package uk.ac.york.qvtd.dependencies.inter;

import org.eclipse.ocl.pivot.Type;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;

/**
 * The Interface ClassDatumVertex represents a Type in the 
 * dependency graph. Types are associated to specific domains.
 */
public interface ClassDatum extends DependencyVertex {

	/**
	 * Gets the domain.
	 *
	 * @return the domain
	 */
	TypedModel getDomain();
	
	/**
	 * Sets the domain.
	 *
	 * @param domain the new domain
	 */
	void setDomain(TypedModel domain);
	
	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	Type getType();
	
	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	void setType(Type type);

}