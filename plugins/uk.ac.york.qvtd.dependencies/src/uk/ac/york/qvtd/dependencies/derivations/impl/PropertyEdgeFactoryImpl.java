/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.derivations.impl;

import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.qvtd.pivot.qvtimperative.Mapping;

import uk.ac.york.qvtd.dependencies.derivations.DerivationGraph;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdgeFactory;
import uk.ac.york.qvtd.dependencies.util.MappingImplExt;

/**
 * The Class PropertyEdgeFactoryImpl.
 */
public class PropertyEdgeFactoryImpl implements PropertyEdgeFactory<Variable, PropertyEdge> {

    /** The mapping. */
    Mapping mapping;

    /**
     * Instantiates a new property edge factory impl.
     *
     * @param mapping the mapping
     */
    public PropertyEdgeFactoryImpl(Mapping mapping) {
        super();
        this.mapping = mapping;
    }

    @Override
    public PropertyEdge createEdge(Variable sourceVertex, Variable targetVertex) {
        Property p = findDerivationProperty(mapping, sourceVertex, targetVertex);
        PropertyEdge pe = null;
        if (p != null) {
            long cost = getCost(p);
            pe = new PropertyEdgeImpl(p, cost);
            pe.setFactual(true);
        }
        return pe;
    }

    /**
     * Gets the cost.
     *
     * @param p the p
     * @return the cost
     */
    private long getCost(Property p) {

        long mult = p.isIsImplicit() ? DerivationGraph.IMPLICIT_NAVIGATION_MULT : 1;
        if (p.isIsMany()) {
            return mult*DerivationGraph.MANY_MULTIPLICITY_COST;
        } else {
            return mult*DerivationGraph.ONE_MULTIPLICITY_COST;
        }
    }

    @Override
    public PropertyEdge createOppositeEdge(Variable sourceVertex, Variable targetVertex) {
        Property p = findDerivationProperty(mapping, sourceVertex, targetVertex);
        PropertyEdge pe = null;
        if (p != null && p.getOpposite() != null) {
            Property op = p.getOpposite();
            long cost = getCost(op);
            pe = new PropertyEdgeImpl(op, cost);
            pe.setFactual(false);
        }
        return pe;
    }

    /**
     * Find derivation property.
     *
     * @param mapping the mapping
     * @param sourceVertex the source vertex
     * @param targetVertex the target vertex
     * @return the property
     */
    // TODO Remove the MappingImplExt if the code gets proted to QVT
    public Property findDerivationProperty(Mapping mapping, Variable sourceVertex, Variable targetVertex) {
        MappingImplExt mext = new MappingImplExt(mapping);
        return mext.findRealtingProperty(sourceVertex, targetVertex);
    }



}