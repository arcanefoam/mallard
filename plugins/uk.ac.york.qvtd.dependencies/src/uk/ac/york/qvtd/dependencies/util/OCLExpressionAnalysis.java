package uk.ac.york.qvtd.dependencies.util;

import org.eclipse.ocl.pivot.OCLExpression;
import org.eclipse.ocl.pivot.OperationCallExp;
import org.eclipse.ocl.pivot.PropertyCallExp;
import org.eclipse.ocl.pivot.VariableExp;

public class OCLExpressionAnalysis {

	/**
	 * Determine if an OCL Expression is a simple attribute navigation, i.e. is
	 * of the form: varA.attribute = varB
	 * <p>
	 * An OCL Expression is a simple attribute navigation if:
	 * <ul>
	 * <li>The OCLExpression is an OperationCallExp
	 * <li>The Operation must be a OclAny "=", operation
	 * <li>The Expression should only have one argument
	 * <li>The argument must be a VariableExp
	 * <li>The source must be a PropertyCallExp
	 * <li>The source's source must be a VariableExp
	 * </ul>
	 *
	 * @param exp
	 * @return True is the expression is a simple attribute navigation, False otherwise
	 * TODO The OCLStdLib is tricky, the "=" operation should be checked better
	 */
	public static boolean oclExpIsSimpleAttributeNavigation(OCLExpression exp) {

		if (exp instanceof OperationCallExp) {
			OperationCallExp ocexp = (OperationCallExp) exp;
			if (ocexp.getReferredOperation().getName().equals("=")) {
				if ((ocexp.getOwnedArguments().size() == 1) &&
						(ocexp.getOwnedArguments().get(0) instanceof VariableExp) &&
						(ocexp.getOwnedSource() instanceof PropertyCallExp)) {
					PropertyCallExp ocexpSource = (PropertyCallExp) ocexp.getOwnedSource();
					return ocexpSource.getOwnedSource() instanceof VariableExp;
				}
			}
		}
	    return false;
	}

}
