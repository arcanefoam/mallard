package uk.ac.york.qvtd.dependencies.util;

import java.util.HashMap;
import java.util.Map;

import org.jgrapht.Graph;
import org.jgrapht.ext.ComponentAttributeProvider;

import uk.ac.york.qvtd.dependencies.util.SolarizedHelper.SolarizedPalette;

public class  GraphAttributes<V, E> implements ComponentAttributeProvider<Graph<V, E>> {

        private static final String GRAPG_BGCOLOR_ATTR_NAME = "bgcolor";
        private final SolarizedPalette GRAPH_BG_COLOR;

        public GraphAttributes(boolean dark) {
            GRAPH_BG_COLOR = SolarizedHelper.getBackgroundTone(dark);

        }

        @Override
        public Map<String, String> getComponentAttributes(Graph<V, E> component) {

            Map<String, String> attr = new HashMap<>();
            attr.put(GRAPG_BGCOLOR_ATTR_NAME, GRAPH_BG_COLOR.getHexValue());
            return attr;
        }

    }