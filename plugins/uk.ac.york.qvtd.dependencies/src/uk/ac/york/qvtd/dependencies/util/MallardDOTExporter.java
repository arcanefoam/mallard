package uk.ac.york.qvtd.dependencies.util;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.Map;

import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.ext.ComponentAttributeProvider;
import org.jgrapht.ext.EdgeNameProvider;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.ext.VertexNameProvider;

public class MallardDOTExporter<V, E> {


    private ComponentAttributeProvider<Graph<V, E>> graphAttributeProvider;
    private VertexNameProvider<V> vertexIDProvider;
    private VertexNameProvider<V> vertexLabelProvider;
    private EdgeNameProvider<E> edgeLabelProvider;
    private ComponentAttributeProvider<V> vertexAttributeProvider;
    private ComponentAttributeProvider<E> edgeAttributeProvider;

    public MallardDOTExporter() {
        this(null, new IntegerNameProvider<V>(), null, null);
    }

    public MallardDOTExporter(ComponentAttributeProvider<Graph<V, E>> graphAttributeProvider,
            VertexNameProvider<V> vertexIDProvider,
            VertexNameProvider<V> vertexLabelProvider,
            EdgeNameProvider<E> edgeLabelProvider,
            ComponentAttributeProvider<V> vertexAttributeProvider,
            ComponentAttributeProvider<E> edgeAttributeProvider) {
        this.setGraphAttributeProvider(graphAttributeProvider);
        this.vertexIDProvider = vertexIDProvider;
        this.setVertexLabelProvider(vertexLabelProvider);
        this.setEdgeLabelProvider(edgeLabelProvider);
        this.setVertexAttributeProvider(vertexAttributeProvider);
        this.setEdgeAttributeProvider(edgeAttributeProvider);
    }

    public MallardDOTExporter(ComponentAttributeProvider<Graph<V, E>> graphAttributeProvider,
            VertexNameProvider<V> vertexIDProvider,
            VertexNameProvider<V> vertexLabelProvider,
            EdgeNameProvider<E> edgeLabelProvider) {
        this(graphAttributeProvider,
                vertexIDProvider,
                vertexLabelProvider,
                edgeLabelProvider,
                null,
                null);
    }

    public void export(Writer writer, Graph<V, E> g)
    {
        PrintWriter out = new PrintWriter(writer);
        String indent = "  ";
        String connector;
        Map<String, String> attributes = null;
        if (g instanceof DirectedGraph<?, ?>) {
            out.println("digraph G {");
            if (getGraphAttributeProvider() != null) {
                attributes = getGraphAttributeProvider().getComponentAttributes(g);
            }
            renderAttributes(out, null, attributes, false);
            connector = " -> ";
        } else {
            out.println("graph G {");
            connector = " -- ";
        }

        for (V v : g.vertexSet()) {
            out.print(indent + getVertexID(v));

            String labelName = null;
            if (getVertexLabelProvider() != null) {
                labelName = getVertexLabelProvider().getVertexName(v);
            }

            if (getVertexAttributeProvider() != null) {
                attributes = getVertexAttributeProvider().getComponentAttributes(v);
            }
            renderAttributes(out, labelName, attributes);

            out.println(";");
        }

        for (E e : g.edgeSet()) {
            String source = getVertexID(g.getEdgeSource(e));
            String target = getVertexID(g.getEdgeTarget(e));

            out.print(indent + source + connector + target);

            String labelName = null;
            if (getEdgeLabelProvider() != null) {
                labelName = getEdgeLabelProvider().getEdgeName(e);
            }
            if (getEdgeAttributeProvider() != null) {
                attributes = getEdgeAttributeProvider().getComponentAttributes(e);
            }
            renderAttributes(out, labelName, attributes);

            out.println(";");
        }

        out.println("}");

        out.flush();
    }

    protected void renderAttributes(
            PrintWriter out,
            String labelName,
            Map<String, String> attributes) {
        renderAttributes(out, labelName, attributes, true);

    }

    protected void renderAttributes(
            PrintWriter out,
            String labelName,
            Map<String, String> attributes,
            boolean isList)
        {
            if ((labelName == null) && (attributes == null)) {
                return;
            }
            if (isList)
                out.print(" [ ");
            if ((labelName == null) && (attributes != null)) {
                labelName = attributes.get("label");
            }
            if (labelName != null) {
                // For HTML support, need to check if the label starts and ends with "<" and ">", if so we asume HTML and dont add quotes
                if (labelName.charAt(0) == '<' && labelName.charAt(labelName.length()-1) == '>') {
                    out.print("label=" + labelName + " ");
                }
                else {
                    out.print("label=\"" + labelName + "\" ");
                }
            }
            if (attributes != null) {
                for (Map.Entry<String, String> entry : attributes.entrySet()) {
                    String name = entry.getKey();
                    if (name.equals("label")) {
                        // already handled by special case above
                        continue;
                    }
                    if (isList)
                        out.print(name + "=\"" + entry.getValue() + "\" ");
                    else
                        out.println(name + "=\"" + entry.getValue() + "\" ");
                }
            }
            if (isList)
                out.print("]");
        }

    protected String getVertexID(V v)
    {
        // TODO jvs 28-Jun-2008:  possible optimizations here are
        // (a) only validate once per vertex
        // (b) compile regex patterns

        // use the associated id provider for an ID of the given vertex
        String idCandidate = vertexIDProvider.getVertexName(v);

        // now test that this is a valid ID
        boolean isAlphaDig = idCandidate.matches("[a-zA-Z]+([\\w_]*)?");
        boolean isDoubleQuoted = idCandidate.matches("\".*\"");
        boolean isDotNumber =
            idCandidate.matches("[-]?([.][0-9]+|[0-9]+([.][0-9]*)?)");
        boolean isHTML = idCandidate.matches("<.*>");

        if (isAlphaDig || isDotNumber || isDoubleQuoted || isHTML) {
            return idCandidate;
        }

        throw new RuntimeException(
            "Generated id '" + idCandidate + "'for vertex '" + v
            + "' is not valid with respect to the .dot language");
    }

    public ComponentAttributeProvider<Graph<V, E>> getGraphAttributeProvider() {
        return graphAttributeProvider;
    }

    public void setGraphAttributeProvider(ComponentAttributeProvider<Graph<V, E>> graphAttributeProvider) {
        this.graphAttributeProvider = graphAttributeProvider;
    }

    public VertexNameProvider<V> getVertexLabelProvider() {
        return vertexLabelProvider;
    }

    public void setVertexLabelProvider(VertexNameProvider<V> vertexLabelProvider) {
        this.vertexLabelProvider = vertexLabelProvider;
    }

    public ComponentAttributeProvider<V> getVertexAttributeProvider() {
        return vertexAttributeProvider;
    }

    public void setVertexAttributeProvider(ComponentAttributeProvider<V> vertexAttributeProvider) {
        this.vertexAttributeProvider = vertexAttributeProvider;
    }

	public EdgeNameProvider<E> getEdgeLabelProvider() {
		return edgeLabelProvider;
	}

	public void setEdgeLabelProvider(EdgeNameProvider<E> edgeLabelProvider) {
		this.edgeLabelProvider = edgeLabelProvider;
	}

	public ComponentAttributeProvider<E> getEdgeAttributeProvider() {
		return edgeAttributeProvider;
	}

	public void setEdgeAttributeProvider(ComponentAttributeProvider<E> edgeAttributeProvider) {
		this.edgeAttributeProvider = edgeAttributeProvider;
	}



}
