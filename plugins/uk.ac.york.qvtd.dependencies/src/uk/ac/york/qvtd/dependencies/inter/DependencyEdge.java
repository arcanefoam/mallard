/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.inter;

/**
 * The Interface DependencyEdge used to represent edges between dependency
 * vertices.
 *
 * Edges in the dependency can represent
 * <ul>
 * <li>Producer/Consumer relations between types and mappings
 * <li>Read/Touch relations between properties and mappings
 * <li>Containment relations between types
 * <li>Inheritance relations between types
 * <p>
 */
public interface DependencyEdge {

    /**
     * Gets the source.
     *
     * @return the source
     */
    DependencyVertex getSource();

    /**
     * Gets the target.
     *
     * @return the target
     */
    DependencyVertex getTarget();

    /**
     * Get the type of this Edge
     * @return The type.
     */
    EdgeType getType();

    /**
     * The allowed type of Edges in the dependency graph
     * @author Horacio Hoyos
     *
     */
    enum EdgeType{
        CONSUMER,
        CONTAINER,
        INHERITANCE,
        PRODUCER
    }


}