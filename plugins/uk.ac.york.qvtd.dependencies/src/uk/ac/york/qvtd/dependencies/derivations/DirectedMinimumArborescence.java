/*
 *
 */
package uk.ac.york.qvtd.dependencies.derivations;

import java.util.Set;

import org.jgrapht.graph.DirectedWeightedSubgraph;

/**
 * Allows to derive <a href=https://en.wikipedia.org/wiki/Arborescence_(graph_theory)>
 * minimum arborescence</a> from given directed connected graph.
 *
 * @param <V> vertex concept type
 * @param <E> edge concept type
 */
public interface DirectedMinimumArborescence<V, E> {

    /**
     * Returns edges set constituting the minimum spanning tree/forest starting at the given
     * root vertex.
     *
     * @return minimum spanning-tree edges set
     */
    public DirectedWeightedSubgraph<V, E> getMinimumSpanningTree(V root);

}
