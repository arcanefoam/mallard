package uk.ac.york.qvtd.dependencies.inter.impl;

import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;

public abstract class AbstractInterEdge implements DependencyEdge {

    private final EdgeType type;
    protected DependencyVertex source;
    protected DependencyVertex target;

    public AbstractInterEdge(DependencyVertex source, DependencyVertex target, EdgeType type) {
        super();
        this.source = source;
        this.target = target;
        this.type = type;
    }


    @Override
    public DependencyVertex getSource() {
        return source;
    }

    @Override
    public DependencyVertex getTarget() {
        return target;
    }


    @Override
    public EdgeType getType() {
        return type;
    }

}