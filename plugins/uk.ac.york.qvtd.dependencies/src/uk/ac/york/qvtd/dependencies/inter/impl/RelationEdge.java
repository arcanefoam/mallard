package uk.ac.york.qvtd.dependencies.inter.impl;

import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;

public class RelationEdge extends AbstractInterEdge implements DependencyEdge {


    public RelationEdge(DependencyVertex source, DependencyVertex target, EdgeType type) {
        super(source, target, type);
    }

    @Override
    public String toString() {
        return source.toString() + "~" + target.toString();
    }


}
