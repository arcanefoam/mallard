package uk.ac.york.qvtd.dependencies.util;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.jgrapht.DirectedGraph;
import org.jgrapht.event.ConnectedComponentTraversalEvent;
import org.jgrapht.event.EdgeTraversalEvent;
import org.jgrapht.event.VertexTraversalEvent;
import org.jgrapht.traverse.AbstractGraphIterator;

/**
 * An qvtc iterator, is a depth-first like iterator for a dependency graphs or execution plans. The iterator supports
 * loops and multiple visits to the same node/path.
 * @author Horacio Hoyos
 *
 */
public abstract class QvtcIterator<V, E> extends AbstractGraphIterator<V, E> {

    /** The Constant CCS_AFTER_COMPONENT. */
    private static final int CCS_AFTER_COMPONENT = 3;
    /** The Constant CCS_BEFORE_COMPONENT. */
    private static final int CCS_BEFORE_COMPONENT = 1;
    /** The Constant CCS_WITHIN_COMPONENT. */
    private static final int CCS_WITHIN_COMPONENT = 2;
    /** The finished event. */
    private final ConnectedComponentTraversalEvent ccFinishedEvent = new ConnectedComponentTraversalEvent(
                    this,
                    ConnectedComponentTraversalEvent.CONNECTED_COMPONENT_FINISHED);
    /** The started event. */
    private final ConnectedComponentTraversalEvent ccStartedEvent = new ConnectedComponentTraversalEvent(
                this,
                ConnectedComponentTraversalEvent.CONNECTED_COMPONENT_STARTED);

    /** The graph. */
    protected final DirectedGraph<V, E> graph;
    /** The reusable edge event. */
    protected FlyweightEdgeEvent<E> reusableEdgeEvent;
    /** The reusable vertex event. */
    protected FlyweightVertexEvent<V> reusableVertexEvent;
    /**
     * Stores the vertices that have been seen during iteration and (optionally)
     * some additional traversal info regarding each vertex.
     */
    //private Map<V, States> seen = new HashMap<V, States>();
    private Map<V, Deque<States>> seen = new HashMap<V, Deque<States>>();	// Each paths adds a new state
    /** The sentinel. */
    protected final V SENTINEL;
    /** The stack of vertices to visit next. */
    private Deque<V> stack = new ArrayDeque<V>();
    /** The start vertex. */
    protected V startVertex;
    /** The connected component state. */
    private int state = CCS_BEFORE_COMPONENT;
    /** The vertex iterator. */
    protected Iterator<V> vertexIterator = null;
    private E finalEdge;

    public QvtcIterator(DirectedGraph<V, E> graph, V Sentinel) {
        super();
        this.graph = graph;
        this.SENTINEL = Sentinel;
    }

    /**
     * Gets the graph.
     *
     * @return the graph being traversed
     */
    public DirectedGraph<V, E> getGraph() {
        return graph;
    }

    /**
     * Retrieves the LIFO stack of vertices which have been encountered but not
     * yet visited (WHITE). This stack also contains <em>sentinel</em> entries
     * representing vertices which have been visited but are still GRAY. A
     * sentinel entry is a sequence (Operation, SENTINEL), whereas a non-sentinel entry
     * is just (Operation).
     *
     * @return stack
     */
    public Deque<V> getStack() {
        return stack;
    }

    /**
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public boolean hasNext() {
        if (startVertex != null) {
            encounterStartVertex();
        }
        if (isConnectedComponentExhausted()) {
            if (state == CCS_WITHIN_COMPONENT) {
                state = CCS_AFTER_COMPONENT;
                if (nListeners != 0) {
                    fireConnectedComponentFinished(ccFinishedEvent);
                }
            }
            if (isCrossComponentTraversal()) {
                // Pick the next head of a connected component,
                while (vertexIterator.hasNext()) {
                    V v = vertexIterator.next();
                    if (!isSeenVertex(v)) {
                        if (graph.inDegreeOf(v) == 0 && graph.outDegreeOf(v) > 0) {
                            encounterVertex(v);
                            state = CCS_BEFORE_COMPONENT;
                            return true;
                        }
                    }
                }
                return false;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }



    /**
     * @see java.util.Iterator#next()
     */
    @Override
    public V next() {
            if (startVertex != null) {
                encounterStartVertex();
            }
            if (state == CCS_BEFORE_COMPONENT) {
                state = CCS_WITHIN_COMPONENT;
                if (nListeners != 0) {
                    fireConnectedComponentStarted(ccStartedEvent);
                }
            }
            V nextVertex = provideNextVertex();
            if (nextVertex == null) {
                throw new NoSuchElementException();
            }
            if (nListeners != 0) {
                fireVertexTraversed(createVertexTraversalEvent(nextVertex));
            }
            // Only add children if we are not in a loop.
    //        VisitState state = getSeenData(nextVertex);
            States state = getSeenData(nextVertex);
            if (!state.equals(States.RED)) {
                addChildrenOf(nextVertex);
            }
            return nextVertex;
        }

    /**
     * Define an egde to be the last visited by the iterator
     * @param invocationEdge
     */
    public void setFinalEdge(E finalEdge) {
        this.finalEdge = finalEdge;
    }

    /**
     * Adds the children of the specified vertex to the queue. EdgeTraversalEvents will
     * be fired first for all the outgoing edges of the vertex. If a vertex has been visited
     * before, it delegates to {@link #encounterVertexAgain(Operation, Invocation)} to decide
     * if it should be visited again or not. This allows to manage loops in the graph.
     *
     * @param vertex the vertex
     */
    private void addChildrenOf(V vertex) {
        ArrayList<E> outgoing = new ArrayList<E>(graph.outgoingEdgesOf(vertex));
        Iterator<E> outIt = outgoing.iterator();
        int index = -1;
        //for (Invocation edge : outgoing) {
        while (outIt.hasNext()) {
            E edge = outIt.next();
            if (nListeners != 0) {
                fireEdgeTraversed(createEdgeTraversalEvent(edge));
            }
//            if (finalEdge != null) {// @Nullable finalEdge
//            	V thisSource = graph.getEdgeSource(edge);
//            	V thisTarget = graph.getEdgeTarget(edge);
//            	V thisSource = graph.getEdgeSource(edge);
//            	V thisTarget = graph.getEdgeTarget(edge);
//                if (((CallActionOperation)edge.getSource()).getAction().equals(finalEdge.getSource()) &&
//                        ((CallActionOperation)edge.getTarget()).getAction().equals(finalEdge.getTarget())) {
//                    index = outgoing.indexOf(edge);
//                    // We also need to remove everything on the stack to the right, i.e. all white nodes
//                    stack.removeIf(op -> States.WHITE.equals(getSeenData(op)));
//                    break;
//                }
//            }
        }
        // Remove all the edges after the final
        if (index != -1 && (index+1 < outgoing.size())) {
            outgoing.subList(index+1, outgoing.size()).clear();
        }
        // Since vertices are visited by popping from the stack, we add the children
        // in inverse order.
        Collections.reverse(outgoing);
        for (E edge : outgoing) {
            V child = graph.getEdgeTarget(edge);
            if (isSeenVertex(child)) {
                encounterVertexAgain(child);
            } else {
                encounterVertex(child);
            }
        }
    }

    /**
     * Creates an edge traversal event for the given edge.
     *
     * @param edge the edge
     * @return the edge traversal event
     */
    private EdgeTraversalEvent<E> createEdgeTraversalEvent(E edge) {
        if (isReuseEvents()) {
            reusableEdgeEvent.setEdge(edge);

            return reusableEdgeEvent;
        } else {
            return new EdgeTraversalEvent<E>(this, edge);
        }
    }

    /**
     * Creates a vertex traversal event for the given vertex
     *
     * @param vertex the vertex
     * @return the vertex traversal event
     */
    private VertexTraversalEvent<V> createVertexTraversalEvent(V vertex) {
        if (isReuseEvents()) {
            reusableVertexEvent.setVertex(vertex);

            return reusableVertexEvent;
        } else {
            return new VertexTraversalEvent<V>(this, vertex);
        }
    }

    /**
     * Encounter start vertex.
     */
    private void encounterStartVertex() {
        encounterVertex(startVertex);
        startVertex = null;
    }

    /**
     * Record finish.
     */
    private void recordFinish() {
            V op = stack.removeLast();
            finishVertex(op);
            // MOdify the state after visiting the vertex, as we need to know the state before
            // finishing
            if (getSeenData(op).equals(States.RED)) {
                // We are finished with this loop
                popSeenData(op);
            }
            else {
                // We are only done if we are not in a loop
                putSeenData(op, States.BLACK);

            }
        }


    /**
     * Encounter vertex.
     *
     * @param vertex the vertex
     * @param edge the edge
     */
    private void encounterVertex(V vertex) {
            putSeenData(vertex, States.WHITE);
            stack.addLast(vertex);
        }

    /**
     * There are two cases, either the operation is being invoked from a new
     * branch or the operation is being invoked from a loop.
     * If the state is WHITE or BLACK, it is a new branch, change the state to new and
     * add it to the stack.
     * If the state is GRAY, change the state to loop and add it to the stack
     * If the state is RED, don't change the color, but add the vertex to the stack
     * because we need to validate one loop iteration.
     *
     * @param vertex the vertex
     * @param siblings number if siblings
     * @param edge the edge
     */
    private void encounterVertexAgain(V vertex) {
        States state = getSeenData(vertex);
        if (state.equals(States.GRAY)) {
            pushSeenData(vertex);
            stack.addLast(vertex);
        }
        else if (state.equals(States.RED)) {
            // Need one red per loop
        	pushSeenData(vertex);
            stack.addLast(vertex);
        }
        else { //if (state.equals(States.WHITE) || state.equals(States.BLACK)) {
            // If the state is white or black, it is a new branch.
            putSeenData(vertex, States.GRAY);
            stack.addLast(vertex);
        }
    }



    /**
     * Called when a vertex has been finished (meaning is dependent on traversal
     * represented by subclass).
     *
     * @param vertex vertex which has been finished
     */
    private void finishVertex(V vertex) {
        if (nListeners != 0) {
            fireVertexFinished(createVertexTraversalEvent(vertex));
        }
    }

    /**
     * Access the data stored for a seen vertex.
     *
     * @param vertex a vertex which has already been seen.
     *
     * @return data associated with the seen vertex or <code>null</code> if no
     * data was associated with the vertex. A <code>null</code> return can also
     * indicate that the vertex was explicitly associated with <code>
     * null</code>.
     */
    protected States getSeenData(V vertex) {
        return seen.get(vertex).peekFirst();
    }

    /**
     * Return true if the given operation is being visited as part of a loop.
     * @param currentRule
     * @return
     */
    public boolean isLoop(V currentRule) {
    //        VisitState state = getSeenData(currentRule);
            States state = getSeenData(currentRule);
            return state.equals(States.RED);
        }

    /**
     * Checks if is connected component exhausted.
     *
     * @return true, if is connected component exhausted
     */
    private boolean isConnectedComponentExhausted() {
        for (;;) {
            if (stack.isEmpty()) {
                return true;
            }
            if (stack.getLast() != SENTINEL) {
                return false;
            }
            // Found a sentinel, we have finished with the tree under the vertex
            stack.removeLast();
            // This will pop corresponding vertex to be recorded as finished.
            recordFinish();
        }
    }

    /**
     * Determines whether a vertex has been seen yet by this traversal.
     *
     * @param vertex vertex in question
     *
     * @return <tt>true</tt> if vertex has already been seen
     */
    private boolean isSeenVertex(V vertex) {
        return seen.containsKey(vertex);
    }

    /**
     * Provide next vertex.
     *
     * @return the operation
     */
    private V provideNextVertex() {
        V next;
        for (;;) {
            V o = stack.removeLast();
            if (o == SENTINEL) {
                // This is a finish-time sentinel we previously pushed.
                recordFinish();
                // Now carry on with another pop until we find a non-sentinel
            } else {
                // Got a real vertex to start working on
                next = o;
                break;
            }
        }
        // Mark the vertex as visited, only if white or black (i.e. first visit or revist)
        States state = getSeenData(next);
        if (state.equals(States.WHITE) || state.equals(States.BLACK)) {
            // If the state is white or black, it is a new branch.
            putSeenData(next, States.GRAY);
        }
        // Put it back in the queue
        stack.addLast(next);
        // Push a sentinel for the Operation onto the stack so that we'll know
        // when we're done with it.
        stack.addLast(SENTINEL);
        return next;
    }

    /**
     * Stores iterator-dependent data for a vertex that has been seen.
     *
     * @param vertex a vertex which has been seen.
     * @param data data to be associated with the seen vertex.
     *
     * @return previous value associated with specified vertex or <code>
     * null</code> if no data was associated with the vertex. A <code>
     * null</code> return can also indicate that the vertex was explicitly
     * associated with <code>null</code>.
     */
    private States putSeenData(V vertex, States data) {
        Deque<States> queue = seen.get(vertex);
        if (queue == null) {
            queue= new ArrayDeque<States>();
            seen.put(vertex, queue);
        }
        States old = queue.pollFirst();
        queue.push(data);
        return old;
    }

    private void popSeenData(V vertex) {
        seen.get(vertex).pop();
    }

    /**
     * We start a loop
     * @param vertex
     */
    private void pushSeenData(V vertex) {
        seen.get(vertex).push(States.RED);
    }

	/**
     * Defines the possible states of a vertex during an iteration over the
     * graph and provides helper methods to query and handle this state(s).
     */
    protected enum States {

        /** Before being visited. */
        WHITE,
        /** Recursively visiting its descendants. */
        GRAY,
        /** All of its descendants have been visited. */
        BLACK,
        /** Vertex has been visited from a loop. */
        RED,
        /** Loop from the vertex has finished. */
        GRAY_RED;
    }

    /**
     * A reusable edge event.
     *
     * @author Barak Naveh
     * @param <VV> the generic type
     * @param <localE> the generic type
     * @since Aug 11, 2003
     */
    public static class FlyweightEdgeEvent<localE>
        extends EdgeTraversalEvent<localE>
    {

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 4051327833765000755L;

        /**
         * Instantiates a new flyweight edge event.
         *
         * @param eventSource the event source
         * @param edge the edge
         * @see EdgeTraversalEvent#EdgeTraversalEvent(Object, Edge)
         */
        public FlyweightEdgeEvent(Object eventSource, localE edge)
        {
            super(eventSource, edge);
        }

        /**
         * Sets the edge of this event.
         *
         * @param edge the edge to be set.
         */
        protected void setEdge(localE edge)
        {
            this.edge = edge;
        }
    }

    /**
     * A reusable vertex event.
     *
     * @author Barak Naveh
     * @param <VV> the generic type
     * @since Aug 11, 2003
     */
    public static class FlyweightVertexEvent<VV>
        extends VertexTraversalEvent<VV>
    {

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 3834024753848399924L;

        /**
         * Instantiates a new flyweight vertex event.
         *
         * @param eventSource the event source
         * @param vertex the vertex
         * @see VertexTraversalEvent#VertexTraversalEvent(Object, Object)
         */
        public FlyweightVertexEvent(Object eventSource, VV vertex)
        {
            super(eventSource, vertex);
        }

        /**
         * Sets the vertex of this event.
         *
         * @param vertex the vertex to be set.
         */
        protected void setVertex(VV vertex)
        {
            this.vertex = vertex;
        }
    }

    /**
     * The Interface SimpleContainer.
     *
     * @param <T> the generic type
     */
    protected static interface SimpleContainer<T>
    {
        /**
         * Adds the specified object to this container.
         *
         * @param o the object to be added.
         */
        public void add(T o);

        /**
         * Tests if this container is empty.
         *
         * @return <code>true</code> if empty, otherwise <code>false</code>.
         */
        public boolean isEmpty();

        /**
         * Remove an object from this container and return it.
         *
         * @return the object removed from this container.
         */
        public T remove();
    }

}