/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.inter.impl;

import java.util.List;
import java.util.Set;

import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.util.Pair;

/**
 * This class provides caches and utilities for using producer-consumer relations.
 * Wrapper around a {@link Partnership} so we can provide meaningful method names
 * @author hhoyos
 *
 */
public class ProducerConsumer {

    /** The delegate. */
    private Partnership delegate;

    /**
     * Instantiates a new producer consumer.
     *
     * @param producer the producer
     */
    public ProducerConsumer(MappingAction producer) {
        super();
        delegate = new Partnership(producer);
    }

    /**
     * Adds the consumer.
     *
     * @param consumer the consumer
     * @param classDatum the class datum
     * @param instances the instances
     */
    public void addConsumer(MappingAction consumer, ClassDatum classDatum, int instances) {
        delegate.addRelation(consumer, classDatum, instances);
    }

    /**
     * Gets the direct consumers.
     *
     * @return the direct consumers
     */
    public Set<MappingAction> getDirectConsumers() {
        return delegate.getDirectPartners();
    }

    /**
     * Gets the indirect consumers.
     *
     * @return the indirect consumers
     */
    public Set<MappingAction> getIndirectConsumers() {
        return delegate.getIndirectPartners();
    }

    /**
     * Gets the all consumers.
     *
     * @return the all consumers
     */
    public Set<MappingAction> getAllConsumers() {
        return delegate.getAllPartners();
    }

    /**
     * Gets the direct consumers.
     *
     * @param classDatum the class datum
     * @return the direct consumers
     */
    public Set<MappingAction> getDirectConsumers(ClassDatum classDatum) {
        return delegate.getDirectPartners(classDatum);
    }

    /**
     * Gets the indirect consumers.
     *
     * @param classDatum the class datum
     * @return the indirect consumers
     */
    public Set<MappingAction> getIndirectConsumers(ClassDatum classDatum) {
        return delegate.getIndirectPartners(classDatum);
    }

    /**
     * Gets the all consumers.
     *
     * @param classDatum the class datum
     * @return the all consumers
     */
    public Set<MappingAction> getAllConsumers(ClassDatum classDatum) {
        return delegate.getAllPartners(classDatum);
    }

    /**
     * Gets the all produced types.
     *
     * @return the all produced types
     */
    public Set<ClassDatum> getProducedTypes() {
        return delegate.getRelationTypes();
    }

    /**
     * Return a set of <ClassDatum, MappingAction> that represent all the consumer relations.
     *
     * @return the partners
     */
    public  Set<Pair<ClassDatum, MappingAction>> getConsumerRelations() {
        return delegate.getPartnerRelations();
    }

    public List<ClassDatum> getAllProducedTypes() {
        return delegate.getAllRelationTypes();
    }

}