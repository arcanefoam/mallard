/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.inter.impl;

import org.eclipse.ocl.pivot.Type;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;

import uk.ac.york.qvtd.dependencies.inter.ClassDatum;

/**
 * The Class ClassDatumImpl.
 */
public class ClassDatumImpl implements ClassDatum {

	/** The type. */
	private Type type;

	/** The domain. */
	private TypedModel domain;

	/**
	 * Instantiates a new class datum impl.
	 */
	public ClassDatumImpl() {
		super();
	}

	/**
	 * Instantiates a new class datum impl.
	 *
	 * @param type the type
	 * @param domain the domain
	 */
	public ClassDatumImpl(Type type, TypedModel domain) {
		super();
		this.type = type;
		this.domain = domain;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public TypedModel getDomain() {
		return domain;
	}

	public void setDomain(TypedModel domain) {
		this.domain = domain;
	}

	@Override
	public String toString() {
		return domain.getName() + ":" + type.getName();
	}
	@Override
	public String getLabel() {
		return toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((domain == null) ? 0 : domain.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassDatumImpl other = (ClassDatumImpl) obj;
		if (domain == null) {
			if (other.domain != null)
				return false;
		} else if (!domain.equals(other.domain))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

}