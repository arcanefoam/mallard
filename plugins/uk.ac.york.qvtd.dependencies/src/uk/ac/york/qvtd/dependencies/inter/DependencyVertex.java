package uk.ac.york.qvtd.dependencies.inter;

public interface DependencyVertex {

	String getLabel();

}