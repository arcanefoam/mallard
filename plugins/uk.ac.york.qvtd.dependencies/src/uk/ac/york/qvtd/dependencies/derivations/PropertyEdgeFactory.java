package uk.ac.york.qvtd.dependencies.derivations;

import org.jgrapht.EdgeFactory;

public interface PropertyEdgeFactory<V, E> extends EdgeFactory<V, E> {


	/**
     * Creates a new edge whose endpoints are the specified source and target
     * vertices, iif there is an Edge between the target and source, and
     * if that edge's propery has an opposite.
     *
     * @param sourceVertex the source vertex.
     * @param targetVertex the target vertex.
     *
     * @return a new edge whose endpoints are the specified source and target
     * vertices.
     */
    public E createOppositeEdge(V sourceVertex, V targetVertex);

}
