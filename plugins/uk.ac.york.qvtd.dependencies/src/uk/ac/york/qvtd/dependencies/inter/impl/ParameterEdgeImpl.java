/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.inter.impl;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.ocl.pivot.Variable;

import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;
import uk.ac.york.qvtd.dependencies.inter.ParameterEdge;

// TODO: Auto-generated Javadoc
/**
 * The Class ParameterEdgeImpl.
 */
public class ParameterEdgeImpl extends AbstractInterEdge implements ParameterEdge {


    /** The variable. */
    private final Set<Variable> variables;
    private boolean isDistinct;

    /**
     * Instantiates a new parameter edge impl.
     *
     * @param source the source
     * @param target the target
     * @param variables the variables
     * @param type the type
     */
    public ParameterEdgeImpl(DependencyVertex source, DependencyVertex target, Set<Variable> variables,
            EdgeType type) {
        super(source, target, type);
        this.variables = variables;
        this.isDistinct = false;

    }

    @Override
    public void addVariable(Variable var) {
        variables.add(var);
    }


    public String getLabel() {
        return variables.stream()
                .map(Variable::getName)
                .collect(Collectors.joining("~"));
    }


    @Override
    public Set<Variable> getVariables() {
        return Collections.unmodifiableSet(variables);
    }

    @Override
    public boolean isDistinct() {
        return isDistinct;
    }

    @Override
    public boolean isMultiple() {
        return variables.size() > 1;
    }

    @Override
    public String toString() {
        return source.toString() + " --{" + getLabel()  + "}-> " + target.toString();
    }

    @Override
    public void setDistinct(boolean isDistinct) {
        this.isDistinct = isDistinct;
    }

}
