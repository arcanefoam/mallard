/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.derivations.impl;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.qvtd.compiler.internal.utilities.ClassRelationships;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.eclipse.qvtd.pivot.qvtcorebase.CoreDomain;
import org.eclipse.qvtd.pivot.qvtcorebase.CorePattern;
import org.jgrapht.EdgeFactory;
import org.jgrapht.SimoesGraphPath;
import org.jgrapht.WeightedGraph;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.alg.KosarajuStrongConnectivityInspector;
import org.jgrapht.alg.SimoesAllPaths;
import org.jgrapht.ext.ComponentAttributeProvider;
import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.ext.StringNameProvider;
import org.jgrapht.graph.DirectedWeightedPseudograph;
import org.jgrapht.graph.DirectedWeightedSubgraph;
import org.jgrapht.graph.Subgraph;

import uk.ac.york.qvtd.dependencies.derivations.DerivationGraph;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;
import uk.ac.york.qvtd.dependencies.inter.impl.DependencyGraphImpl;
import uk.ac.york.qvtd.dependencies.util.GraphAttributes;
import uk.ac.york.qvtd.dependencies.util.MallardDOTExporter;
import uk.ac.york.qvtd.dependencies.util.Pair;
import uk.ac.york.qvtd.dependencies.util.SolarizedHelper;
import uk.ac.york.qvtd.dependencies.util.SolarizedHelper.SolarizedPalette;

/**
 * The Class MappingDerivationsImpl.
 */
public class MappingDerivationsImpl extends DirectedWeightedPseudograph<Variable, PropertyEdge> implements DerivationGraph {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6110703381809726147L;

    /** The minimum directed spanning tree, for derivation decisions */
    private Map<Variable, DirectedWeightedSubgraph<Variable, PropertyEdge>> mdsts =
            new HashMap<Variable, DirectedWeightedSubgraph<Variable,PropertyEdge>>();

    private final ConnectivityInspector<Variable, PropertyEdge> inspector;

    private Map<Set<Variable>, Double> bestCostForSet;

    private Map<Set<Variable>, Variable> primaryVariable;

    private Map<Variable, TypedModel> typeModels = new HashMap<Variable, TypedModel>();

    private Boolean fromRoot;
    //private Map<Set<Variable>, Variable> derivationPaths;


    /**
     * Instantiates a new mapping derivations impl.
     *
     * @param ef the ef
     */
    public MappingDerivationsImpl(EdgeFactory<Variable, PropertyEdge> ef) {
        super(ef);
        inspector = new ConnectivityInspector<>(this);
    }

    @Override
    public void findMinimumSpanningTrees() {
        bestCostForSet = new HashMap<Set<Variable>, Double>();
        primaryVariable = new HashMap<Set<Variable>, Variable>();
        //derivationPaths = new HashMap<Set<Variable>, Variable>();
        // Find all paths
        ConnectivityInspector<Variable, PropertyEdge> inspect = new ConnectivityInspector<Variable, PropertyEdge>(this);
        if (edgeSet().isEmpty()) {
            Set<Variable> single_set = inspect.connectedSets().get(0);
            bestCostForSet.put(single_set, 0.0);
            primaryVariable.put(single_set, vertexSet().iterator().next());
        }
        else {
            SimoesAllPaths<Variable, PropertyEdge> paths = new SimoesAllPaths<Variable, PropertyEdge>(this);
            paths.findAllPaths();
            // For each disconnected set, find the best path
            for (Set<Variable> sg : inspect.connectedSets()) {
                double best = Double.MAX_VALUE;
                if (sg.size() == 1) {		// No derivations
                    bestCostForSet.put(sg, 0.0);
                    primaryVariable.put(sg, sg.iterator().next());
                }
                else {
                    for (Variable v : sg) {
                        // Paths without loops
                        List<SimoesGraphPath<Variable, PropertyEdge>> vpaths = paths.getPathsFrom(v).stream()
                                .filter(p -> !p.hasLoop()).collect(Collectors.toList());
                        // Are all other variables reached?
                        Set<Variable> rest = new HashSet<Variable>(sg);
                        rest.remove(v);
                        Set<Variable> targetVars = vpaths.stream().map(p -> p.getEndVertex()).collect(Collectors.toSet());
                        rest.removeAll(targetVars);
                        if (rest.isEmpty()) {
                            List<SimoesGraphPath<Variable, PropertyEdge>> bestPaths = new ArrayList<SimoesGraphPath<Variable,PropertyEdge>>();
                            // For each target var we need to find the best path
                            // FIXME use Dkjistra
                            for (Variable v2 : targetVars) {
                                SimoesGraphPath<Variable, PropertyEdge> v2_best = vpaths.stream()
                                    .filter(p -> p.getEndVertex().equals(v2))
                                    .sorted((p1, p2) -> Double.compare(p1.getWeight(), p2.getWeight()))
                                    .findFirst().get();
                                bestPaths.add(v2_best);
                            }
                            best = bestCostForSet.getOrDefault(sg, Double.MAX_VALUE);
                            //double cost = vpaths.stream().mapToDouble(p -> p.getWeight()).sum();
                            Set<PropertyEdge> tree = bestPaths.stream()
                                    .flatMap(p -> p.getEdgeList().stream())
                                    .collect(Collectors.toSet());
                            double cost = tree
                                    .stream()
                                    .mapToDouble(p -> p.getWeight())
                                    .sum();
                            if (cost < best) {
                                bestCostForSet.put(sg, cost);
                                primaryVariable.put(sg, v);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @return the bestCostForSet
     */
    public Map<Set<Variable>, Double> getBestCostForSet() {
        return bestCostForSet;
    }

    /**
     * @return the primaryVariable
     */
    public Map<Set<Variable>, Variable> getPrimaryVariable() {
        return primaryVariable;
    }

    /**
     * @return the mapping has to be invoked from the root fromRoot
     */
    public boolean isFromRoot() {
        if (fromRoot != null) {
            return fromRoot;
        }

        return fromRoot;
    }


    private void exportGraph(WeightedGraph<Variable, PropertyEdge> g){
        PrintWriter writer = new PrintWriter(System.out);
        IntegerNameProvider<Variable> p1=new IntegerNameProvider<Variable>();
        StringNameProvider<Variable> p2=new StringNameProvider<Variable>();
        ComponentAttributeProvider<PropertyEdge> p4 =
           new ComponentAttributeProvider<PropertyEdge>() {
                public Map<String, String> getComponentAttributes(PropertyEdge e) {
                    Map<String, String> map =new LinkedHashMap<String, String>();
                    map.put("weight", Double.toString(g.getEdgeWeight(e)));
                    return map;
                }
           };
        DOTExporter<Variable, PropertyEdge> export=new DOTExporter<Variable, PropertyEdge>(p1, p2, null, null, p4);
        export.export(writer, g);
    }

    // This returns all edges outgoing from M
    private boolean isMtoLEdge(PropertyEdge pe) {
        assert getEdgeSource(pe).eContainer() instanceof CorePattern;
        CorePattern cp = (CorePattern) getEdgeSource(pe).eContainer();
        assert cp.getArea() instanceof CoreDomain;
        CoreDomain cd = (CoreDomain) cp.getArea();
        if (cd.getTypedModel().getName() == "middle") {
            cp = (CorePattern) getEdgeTarget(pe).eContainer();
            assert cp.getArea() instanceof CoreDomain;
            cd = (CoreDomain) cp.getArea();
            if (cd.getTypedModel().getName() != "middle") {	// REmove middle to middle
                return true;
            }
        }
        return false;
    }

    @Override
    public double getEdgeWeight(PropertyEdge e) {
        return e.getWeight();

    }

//    @Override
//    public MinimumSpanningTree<Variable, PropertyEdge> getMinimumSpanningTree() {
//        return minimumSpanningTree;
//    }


    /**
     * Checks if is primary.
     *
     * @param variable the variable
     * @return true, if is primary
     */
    public boolean isPrimary(Variable variable) {
        boolean isPrimary = true;
        for (PropertyEdge pe : incomingEdgesOf(variable)) {
            isPrimary &= !pe.isFactual();		// If any of the input edges is factual, it is not a primary
            if (!isPrimary) break;
        }
        return isPrimary;
    }

    @Override
    public void setEdgeWeight(PropertyEdge e, double weight) {
        e.setWeight(weight);
    }

    public void setIsFromRoot(DependencyGraphImpl dependencyGraphImpl, ClassRelationships classrel) {
        KosarajuStrongConnectivityInspector<Variable, PropertyEdge> inspector =
                new KosarajuStrongConnectivityInspector<>(this);
          // Get all the M to L edges of each subgraph
          List<Variable> mVars = new ArrayList<Variable>();
          for (Set<Variable> dset : inspector.stronglyConnectedSets()) {
              Subgraph<Variable, PropertyEdge, MappingDerivationsImpl> sg = new Subgraph<>(this, dset);
              if (!sg.edgeSet().isEmpty()) {
                  List<PropertyEdge> mtolEdges = sg.edgeSet().stream()
                      .filter(pe -> pe.isFactual() && isMtoLEdge(pe)).collect(Collectors.toList());
                  if (mtolEdges.isEmpty()) {
                      fromRoot = false;
                      return;		// If there are no MtoL edges in a subgraph, we can invoke
                  }
                  mVars.addAll(mtolEdges.stream().map(pe -> getEdgeTarget(pe)).collect(Collectors.toList()));
              }
          }
          List<Pair<Type, TypedModel>> mTypes = mVars.stream()
                  .map(v -> new Pair<Type, TypedModel>(v.getType(), typeModels.get(v)))
                  .collect(Collectors.toList());
          // If there is only one type per type model, invocation can be from anywhere
          Map<TypedModel, List<Pair<Type, TypedModel>>> mTypesByModel = mTypes.stream().collect(Collectors.groupingBy(p -> p.second));
          if (mTypesByModel.entrySet().stream()
                  .allMatch(en -> en.getValue().size()== 1)){
              fromRoot = false;
              return;
          }
          // If all vars in the same domain have the same containment type, we need a root invocation, because
          // we can not proof that another variable in the mapping is the container of the variables
          // We need to find the common supper type
          Set<Pair<Type, TypedModel>> cTypes = new HashSet<>();
          for(Pair<Type, TypedModel> p : mTypes) {
              // Only test if multiple types per domain
              if (mTypesByModel.get(p.second).size() > 1) {
                  Iterable<Class> containerClasses = classrel.allContainerClassesReverse((Class) p.first);
                  Class topContainer = containerClasses.iterator().next(); // It always has at least vType (p.first)
                  //if (!topContainer.equals(p.first)) {
                      cTypes.add(new Pair<Type, TypedModel>(topContainer, p.second));
                  //}
              }
          }
          Map<TypedModel, List<Pair<Type, TypedModel>>> cTypesByModel = cTypes.stream().collect(Collectors.groupingBy(p -> p.second));
          // There must be one different container per variable
          if (cTypesByModel.entrySet().stream()
                  .allMatch(en -> en.getValue().size() == mTypesByModel.get(en.getKey()).size())){
              fromRoot = false;
          }
          else {
              fromRoot = true;
          }
    }

    @Override
    public boolean pathExists(Variable sourceVariable, Variable targetVariable) {
        return inspector.pathExists(sourceVariable, targetVariable);
    }

    // We want to keep track of TypeModles
    public Object addVertex(Variable v, DependencyVertex source) {
        assert source instanceof ClassDatum;
        typeModels.put(v, ((ClassDatum)source).getDomain());
        return addVertex(v);
    }




    public String toDOT() {
        MallardDOTExporter<Variable, PropertyEdge> ex = new MallardDOTExporter<Variable, PropertyEdge>(
                new GraphAttributes<Variable, PropertyEdge>(false),
                new IntegerNameProvider<Variable>(),
                new StringNameProvider<Variable>(),
                null,
                new VariableAttributes(false),
                new PropertyEdgeAttributes(false));
        StringWriter stringWriter = new StringWriter();
        ex.export(stringWriter, this);
        return stringWriter.toString();
    }

    private class PropertyEdgeAttributes implements ComponentAttributeProvider<PropertyEdge> {

        private boolean dark;

        public PropertyEdgeAttributes(boolean dark) {
            super();
            this.dark = dark;
        }

        @Override
        public Map<String, String> getComponentAttributes(PropertyEdge component) {
            Map<String, String> attr = new HashMap<String, String>();
            SolarizedPalette color;
            if (component.getProperty() != null) {
                if (component.getProperty().isIsImplicit()) {
                    color = SolarizedPalette.RED;
                }
                else {
                    if (component.getProperty().isIsMany()) {
                        color = SolarizedPalette.VIOLET;
                    }
                    else {
                        color = SolarizedPalette.GREEN;
                    }
                }
            }
            else {
                color = SolarizedPalette.ORANGE;
            }
            attr.put("color", color.getHexValue());
            return attr;
        }

    }

    private class VariableAttributes implements ComponentAttributeProvider<Variable> {
        private boolean dark;
        public VariableAttributes(boolean dark) {
            super();
            this.dark = dark;
        }

        @Override
        public Map<String, String> getComponentAttributes(Variable component) {
            Map<String, String> attr = new HashMap<String, String>();
            SolarizedPalette color = SolarizedPalette.MAGENTA;
            attr.put("color", color.getHexValue());
            attr.put("fontcolor", SolarizedHelper.getPrimaryContentTone(dark).getHexValue());
            return attr;
        }

    }



}
