/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.inter.impl;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jgrapht.Graph;
import org.jgrapht.Graphs;

import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.DependencyPath;
import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;

public class DependencyPathImpl implements DependencyPath {

    private DependencyGraph graph;

    private List<DependencyEdge> edgeList;
    private List<DependencyVertex> vertexList;
    private List<DependencyEdge> unmodifiableEdgeList = null;

    private DependencyVertex startVertex;

    private DependencyVertex endVertex;

    private double weight;

    private boolean hasLoop = false;

    private DependencyVertex loopVertex;
    private DependencyVertex entryVertex;
    private DependencyVertex exitVertex;

    public DependencyPathImpl(DependencyGraph graph) {
        this.graph = graph;
        this.edgeList = new ArrayList<DependencyEdge>();
    }

    /**
     * Instantiates a new dependency path impl.
     *
     * @param graph the graph
     * @param e the starting edge
     */
    public DependencyPathImpl(DependencyGraph graph, DependencyEdge e) {
        this.graph = graph;
        this.edgeList = new ArrayList<DependencyEdge>();
        edgeList.add(e);
        startVertex = e.getSource();
        weight = edgeList.size();
        endVertex = e.getTarget();
    }

    public DependencyPathImpl(DependencyGraph graph, List<DependencyEdge> edgeList) {
        super();
        this.graph = graph;
        this.edgeList = new ArrayList<DependencyEdge>(edgeList);
        startVertex = edgeList.get(0).getSource();
        weight = edgeList.size();
        endVertex = edgeList.get((int) (weight-1)).getTarget();
    }

    @Override
    public void addEdge(DependencyEdge e) {
        if (startVertex == null && endVertex == null) {
            startVertex = e.getSource();
            endVertex = e.getTarget();
        } else {
            if (!e.getSource().equals(endVertex)) {
                throw new InvalidParameterException("The added edge must extend the path. " + e.getSource() + " does not match "
                        + endVertex);
            }
        }
        edgeList.add(e);
        endVertex = e.getTarget();
        weight = edgeList.size();
    }

    @Override
    public void addEdgeFirst(DependencyEdge e) {
        if (startVertex == null && endVertex == null) {
            startVertex = e.getSource();
            endVertex = e.getTarget();
        } else {
            if (!e.getTarget().equals(startVertex)) {
                throw new InvalidParameterException("The added edge must extend the path. " + e.getSource() + " does not match "
                        + endVertex);
            }
        }
        edgeList.add(0, e);
        startVertex = e.getSource();
        weight = edgeList.size();

    }

    @Override
    public boolean containsVertex(DependencyVertex vertex) {
        return getVertexList().contains(vertex);
    }


    @Override
    public List<DependencyEdge> getEdgeList() {
        if (unmodifiableEdgeList == null) {
            unmodifiableEdgeList = Collections.unmodifiableList(edgeList);
        }
        return unmodifiableEdgeList;
    }

    @Override
    public DependencyVertex getEndVertex() {
        return endVertex;
    }

    @Override
    public Graph<DependencyVertex, DependencyEdge> getGraph() {
        return graph;
    }

    /* (non-Javadoc)
     * @see org.jgrapht.GraphPath#getLength()
     */
    @Override
    public int getLength() {
        return getVertexList().size();
    }

    public DependencyVertex getLoopVertex() {
        return loopVertex;
    }

    @Override
    public DependencyVertex getStartVertex() {
        return startVertex;
    }

    /* (non-Javadoc)
     * @see org.jgrapht.GraphPath#getVertexList()
     */
    @Override
    public List<DependencyVertex> getVertexList() {
        if (vertexList == null) {
            if(edgeList.isEmpty()) {
                vertexList = Collections.emptyList();
            }
            else {
                vertexList = new ArrayList<>();
                DependencyVertex v = this.getStartVertex();
                vertexList.add(v);
                for (DependencyEdge e : edgeList) {
                    vertexList.add(e.getTarget());
                }
            }
        }
        return vertexList;
    }



    @Override
    public double getWeight() {
        return weight;
    }

    @Override
    public boolean hasLoop() {
        return hasLoop;
    }

    public void setLoopVertex(DependencyVertex loopVertex) {
        hasLoop = true;
        this.loopVertex = loopVertex;
    }

    // override Object
    @Override public String toString()
    {
        return edgeList.toString() + (hasLoop ? "*" : "");
    }

}
