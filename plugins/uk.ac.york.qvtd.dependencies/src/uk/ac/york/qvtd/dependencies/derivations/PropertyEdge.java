/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.derivations;

import org.eclipse.ocl.pivot.Property;


/**
 * The Interface PropertyEdge.
 */
public interface PropertyEdge {

    /**
     * Returns true if the property was explicitly assigned in the predicates,
     * false if it was added using a genuine property's opposite.
     *
     * @return true, if is factual
     */
    boolean isFactual();

    /**
     * Sets the factual.
     *
     * @param factual the new factual
     */
    void setFactual(boolean factual);

    /**
     * Gets the cost.
     *
     * @return the cost
     */
    long getCost();

    /**
     * Sets the cost.
     *
     * @param cost the new cost
     */
    void setCost(long cost);

    /**
     * Gets the property.
     *
     * @return the property
     */
    Property getProperty();

    /**
     * Sets the property.
     *
     * @param property the new property
     */
    void setProperty(Property property);

    double getWeight();

    void setWeight(double weight);

}
