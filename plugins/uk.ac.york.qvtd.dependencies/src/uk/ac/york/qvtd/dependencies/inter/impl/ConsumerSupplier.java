/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.inter.impl;

import java.util.List;
import java.util.Set;

import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;

/**
 * This class provides caches and utilities for using consumer-supplier relations.
 * Wrapper around a {@link Partnership} so we can provide meaningful method names.
 * @author Horacio Hoyos
 *
 */
public class ConsumerSupplier {

    /** The delegate. */
    private Partnership delegate;

    /**
     * Instantiates a new consumer producer.
     *
     * @param consumer the consumer
     */
    public ConsumerSupplier(MappingAction consumer) {
        super();
        delegate = new Partnership(consumer);
    }

    /**
     * Adds the supplier.
     *
     * @param supplier the supplier
     * @param classDatum the class datum
     * @param instances the instances
     */
    public void addSupplier(MappingAction supplier, ClassDatum classDatum, int instances) {
        delegate.addRelation(supplier, classDatum, instances);
    }

    /**
     * Gets the direct suppliers.
     *
     * @return the direct suppliers
     */
    public Set<MappingAction> getDirectSuppliers() {
        return delegate.getDirectPartners();
    }

    /**
     * Gets the indirect suppliers.
     *
     * @return the indirect suppliers
     */
    public Set<MappingAction> getIndirectSuppliers() {
        return delegate.getIndirectPartners();
    }

    /**
     * Gets the all suppliers.
     *
     * @return the all suppliers
     */
    public Set<MappingAction> getAllSuppliers() {
        return delegate.getAllPartners();
    }

    /**
     * Gets the direct suppliers.
     *
     * @param classDatum the class datum
     * @return the direct suppliers
     */
    public Set<MappingAction> getDirectSuppliers(ClassDatum classDatum) {
        return delegate.getDirectPartners(classDatum);
    }

    /**
     * Gets the indirect suppliers.
     *
     * @param classDatum the class datum
     * @return the indirect suppliers
     */
    public Set<MappingAction> getIndirectSuppliers(ClassDatum classDatum) {
        return delegate.getIndirectPartners(classDatum);
    }

    /**
     * Gets the all suppliers.
     *
     * @param classDatum the class datum
     * @return the all suppliers
     */
    public Set<MappingAction> getAllSuppliers(ClassDatum classDatum) {
        return delegate.getAllPartners(classDatum);
    }

    /**
     * Gets the all consumed types.
     *
     * @return the all consumed types
     */
    public Set<ClassDatum> getConsumedTypes() {
        return delegate.getRelationTypes();
    }

    public List<ClassDatum> getAllConsumedTypes() {
        return delegate.getAllRelationTypes();
    }

    public Set<ClassDatum> getDirectTypesBySuplier(MappingAction lastOp) {
        return delegate.getDirectRelationsWith(lastOp);
    }

    public Set<ClassDatum> getIndirectTypesBySuplier(MappingAction lastOp) {
        return delegate.getIndirectRelationsWith(lastOp);
    }

}