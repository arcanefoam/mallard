package uk.ac.york.qvtd.dependencies.derivations.impl;

import org.eclipse.ocl.pivot.Property;

import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;

public class PropertyEdgeImpl implements PropertyEdge {

    private long cost;
    private Property property;
    private boolean factual;


    public PropertyEdgeImpl(Property property, long cost) {
        super();
        this.cost = cost;
        this.property = property;
    }

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    @Override
    public Property getProperty() {

        return property;
    }

    @Override
    public void setProperty(Property property) {

        this.property = property;
    }

    @Override
    public boolean isFactual() {
        return factual;
    }

    @Override
    public void setFactual(boolean factual) {
        this.factual = factual;
    }

    @Override
    public double getWeight() {
        return cost;
    }

    @Override
    public void setWeight(double weight) {
        cost = (long) weight;
    }

    @Override
    public String toString() {
        if (property != null) {
            return property.getType().toString() + "." + property.getName();
        }
        else {
            return "dummy";
        }
    }

}
