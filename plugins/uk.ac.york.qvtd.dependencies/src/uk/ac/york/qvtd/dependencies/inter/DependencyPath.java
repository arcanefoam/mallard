/*******************************************************************************
 * Copyright (c) 2017 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.inter;

import org.jgrapht.GraphPath;

/**
 * A dependency graph can be constructed incrementally, i.e. adding one edge at a time.
 * Dependency paths also provide information about any loops in the path.
 *
 * @author Goblin
 */
public interface DependencyPath extends GraphPath<DependencyVertex, DependencyEdge> {

    /**
     * Allows incremental construction of the path. The edge must extend the path, i.e. it�s source should be the last
     * known end vertex.
     */
    public void addEdge(DependencyEdge e);

    /**
     * Adds the edge first.
     *
     * @param e the e
     */
    public void addEdgeFirst(DependencyEdge e);

    public boolean containsVertex(DependencyVertex vertex);

    /**
     * If this path has a loop, it returns the vertex that is the start/end of the loop. Null other wise.
     * @return
     */
    public DependencyVertex getLoopVertex();

    /**
     * True if the path has a loop
     * @return
     */
    public boolean hasLoop();

    /**
     * Sets the loop vertex
     * @param loopVertex
     */
    public void setLoopVertex(DependencyVertex loopVertex);

    // TODO provide additional add edge methods
}
