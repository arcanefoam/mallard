/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.inter;

import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.qvtd.compiler.internal.utilities.ClassRelationships;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.DirectedNeighborIndex;

import uk.ac.york.qvtd.dependencies.inter.impl.ConsumerSupplier;
import uk.ac.york.qvtd.dependencies.util.Pair;

/**
 * A DependencyGraph represents the data dependencies between mappings in a
 * QVTc transformation.
 */
public interface DependencyGraph extends DirectedGraph<DependencyVertex, DependencyEdge> {

    /**
     * Adds the path (formed by the list of edges) between the source and the target to the cache of paths.
     */
    //void addPath(List<DependencyEdge> edgeList, DependencyVertex source, DependencyVertex target);

    /**
     * After the DependencyGraph has been loaded, call this method to find the
     * mapping derivations for all MappingVertex in the graph.
     */
    void createMappingDerivations();

    /**
     * Finds all paths in the graph, excluding those starting/ending
     * in the root.
     */
    void findAllPathsBetweenMappingActions();

    /**
     * Gets the all sub classes.
     *
     * @param type the type
     * @return the all sub classes
     */
    Set<@NonNull ClassDatum> getAllSubClassDatums(ClassDatum type);


    /**
     * Get the List of Mappings that consume the types produced by the MappingAction, i.e. direct
     * or indirect consumers.
     *
     * @param vertex the vertex
     * @param visited The visited mappings
     * @return the runtime consumers
     */
    Set<MappingAction> getConsumers(MappingAction vertex);

    /**
     * Gets the contained types produced by operation.
     *
     * @param operation the operation
     * @return the contained types produced by operation
     */
    @NonNull Set<ClassDatum> getContainedTypesProducedByAction(MappingAction operation);

    /**
     * Gets the loops from.
     *
     * @param action the action
     * @return the loops from
     */
    List<DependencyPath> getLoopsFrom(MappingAction action);

    /**
     * Gets the mapping vertices.
     *
     * @return the mapping vertices
     */
    List<MappingAction> getMappingVertices();

    /**
     * Gets the middle domain.
     *
     * @return the middle domain
     */
    TypedModel getMiddleDomain();

    /**
     * Gets the neighbour index.
     *
     * @return the neighbour index
     */
    DirectedNeighborIndex<DependencyVertex, DependencyEdge> getNeighborIndex();

    /**
     * Gets the all paths between the mappings, starting at the Pair's first and ending at the Pair's second.
     *
     * @param pair the pair
     * @return the paths from first to second
     */
    List<DependencyPath> getPathsFrom(Pair<MappingAction, MappingAction> pair);

    /**
     * Gets the predecessor actions, excluding the root mapping.
     *
     * @param vertex the vertex
     * @return the predecessor actions
     */
    Set<MappingAction> getPredecessorActions(MappingAction vertex);

    /**
     * Gets the producer-consumer relations for the given action in the form of
     * ClassDatum:MappingAction pairs.
     *
     * @param action the action
     * @return the action producer relations
     */
    Set<Pair<ClassDatum, MappingAction>> getProducerRelations(MappingAction action);

    /**
     * Get the list of mappings that produce the given ClassDatum
     * @param t
     * @return
     */
    List<MappingAction> getProducersOf(ClassDatum t);

    /**
     * Get the list of mappings that consume the given ClassDatum
     * @param t
     * @return
     */
    List<MappingAction> getConsumersOf(ClassDatum t);

    /**
     * Gets the reduced loops from a mapping action.
     *
     * @param action the action
     * @return the reduced loops from
     */
    Set<List<MappingAction>> getReducedLoopsFrom(MappingAction action);

    /**
     * Get all the maximal paths that start at the root and end at a given MappingAction. The paths only contain a list
     * of MappingActions in the path.
     *
     * Maximal paths are found by removing any path that is a proper sub-path of another
     *
     * @param target the target
     * @return the paths ending on
     */
    Set<List<MappingAction>> getReducedPathsFromRoot(MappingAction target);

    /**
     * Gets the root vertex.
     *
     * @return the root vertex
     */
    MappingAction getRootVertex();

    /**
     * Get the List of ProducerConsumers that consume the types produced by the MappingAction at runtime, i.e. direct
     * or indirect consumers. Only add indirect consumers, if any of the visited mappings produces the same types,
     * i.e. the vertex is in the list of consumers of any of the visited mappings, so we have enough elements to bind
     *
     * @param vertex the vertex
     * @param visited The visited mappings
     * @return the runtime consumers
     */
    Set<MappingAction> getRuntimeConsumers(MappingAction vertex, Set<MappingAction> visited);

    /**
     * Gets the sub-ClassDatums.
     * <p>
     * This method only returns the first level of sub-ClassDatums.
     *
     * @param classDatum the class datum
     * @return the sub class datums
     */
    List<ClassDatum> getSubClassDatums(ClassDatum classDatum);


    /**
     * Gets the successor actions.
     *
     * @param vertex the vertex
     * @return the successor actions
     */
    Set<MappingAction> getSuccessorActions(MappingAction vertex);

    /**
     * Gets the super class datums.
     * <p>
     * This method only returns the first level of super-ClassDatums.
     *
     * @param classDatum the class datum
     * @return the super class datums
     */
    List<ClassDatum> getSuperClassDatums(ClassDatum classDatum);

    /**
     * Get the List of Mappings that produce the types consumed by the MappingAction, i.e. direct
     * or indirect producers.
     *
     * @param vertex the vertex
     * @param visited The visited mappings
     * @return the runtime consumers
     */
    Set<MappingAction> getSupplier(MappingAction vertex);

    /**
     * Get all the consumer-producer relations for the given mapping, i.e. information about what
     * mappings produce the types consumed by the mapping.
     *
     * @param action the action
     * @return the consumer information
     */
    ConsumerSupplier getSupplierInformation(MappingAction action);

    /**
     * Gets the types consumed by operation.
     *
     * @param operation the operation
     * @return the types consumed by operation
     */
    List<ClassDatum> getTypesConsumedByAction(MappingAction operation);

    /**
     * Gets the types produced by operation.
     *
     * @param operation the operation
     * @return the types produced by operation
     */
    List<ClassDatum> getTypesProducedByAction(MappingAction operation);

    /**
     * Execute all the startup operations that initialise all the support structures and cached content.
     * Should be called after populating the graph with nodes and edges.
     */
    void initialize();

    /**
     * Sets the middle domain.
     *
     * @param middleDomain the new middle domain
     */
    void setMiddleDomain(TypedModel middleDomain);

    /**
     * Sets the root vertex.
     *
     * @param rootVertex the new root vertex
     */
    void setRootVertex(MappingAction rootVertex);

    /**
     * Return a representation of the graph in the DOT format.
     * @return The DOT format string.
     */
    String toDOT();


}