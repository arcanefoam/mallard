/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.inter;

import java.util.Set;

import org.eclipse.ocl.pivot.Variable;

// TODO: Auto-generated Javadoc
/**
 * A ParameterEdge connects two Mapping Actions in a producer/consumer relation.
 * <p>
 * Since a mapping action represents an invocation more than the mapping itself,
 * the parameter edge indicate that the type (Class Datum) at one of it ends
 * is produced(output)/consumed(input) by the mapping at the other end.
 */
public interface ParameterEdge extends DependencyEdge {

    /**
     * Adds the variable.
     *
     * @param var the var
     */
    void addVariable(Variable var);

    /**
     * Gets the label.
     *
     * @return the label
     */
    String getLabel();

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    Set<Variable> getVariables();

    /**
     * Checks if is distinct.
     * <p>
     * If multiple, then variables are distinct if we can prove it, i.e. we can find a predicate that states that they
     * must be distinct. If not multiple, always return false.
     * @return true, if is distinct
     */
    boolean isDistinct();

    /**
     * Whether the producer/consumer produces/consumes multiple parameters of the source/target ClassDatum. If this is
     * true, then the Set returned by {@link #getVariables()} should have more than 1 element.
     * @return True if the relation is multiple
     */
    boolean isMultiple();

    /**
     * Sets the distinct.
     *
     * @param isDistinct the new distinct
     */
    void setDistinct(boolean isDistinct);

}