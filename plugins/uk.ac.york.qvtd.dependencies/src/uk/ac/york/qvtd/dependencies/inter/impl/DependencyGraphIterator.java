package uk.ac.york.qvtd.dependencies.inter.impl;

import java.util.NoSuchElementException;

import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.util.QvtcIterator;

public class DependencyGraphIterator extends QvtcIterator<DependencyVertex, DependencyEdge> {

    private final static SentinelVertex SENTINEL = new SentinelVertex();


    public DependencyGraphIterator(DependencyGraphImpl dg, MappingAction rootVertex) {
        super(dg, SENTINEL);
        if (dg == null) {
            throw new IllegalArgumentException("graph must not be null");
        }
        vertexIterator = dg.vertexSet().iterator();
        setCrossComponentTraversal(startVertex == null);

        reusableEdgeEvent = new FlyweightEdgeEvent<DependencyEdge>(this, null);
        reusableVertexEvent = new FlyweightVertexEvent<DependencyVertex>(this, null);

        if (startVertex == null) {
            // Find the root, a vertex with no incoming edges
            try {
                this.startVertex = dg.vertexSet().stream()
                        .filter(v -> dg.inDegreeOf(v) == 0)
                        .findFirst()
                        .get();
            } catch (NoSuchElementException  e) {
                throw new IllegalArgumentException(
                        "graph must contain the a root vertex");
            }
        } else if (dg.containsVertex(startVertex)) {
            this.startVertex = startVertex;
        } else {
            throw new IllegalArgumentException(
                "graph must contain the start vertex");
        }
    }

    static class SentinelVertex implements DependencyVertex {

        @Override
        public String getLabel() {
            // TODO Auto-generated method stub
            return "S";
        }

    }

}
