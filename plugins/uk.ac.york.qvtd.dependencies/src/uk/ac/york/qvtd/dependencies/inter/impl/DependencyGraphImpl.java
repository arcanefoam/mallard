/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.inter.impl;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.qvtd.compiler.internal.utilities.ClassRelationships;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.jgrapht.EdgeFactory;
import org.jgrapht.Graphs;
import org.jgrapht.alg.DirectedNeighborIndex;
import org.jgrapht.event.ConnectedComponentTraversalEvent;
import org.jgrapht.event.EdgeTraversalEvent;
import org.jgrapht.event.TraversalListener;
import org.jgrapht.event.VertexTraversalEvent;
import org.jgrapht.ext.ComponentAttributeProvider;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.ext.StringNameProvider;
import org.jgrapht.graph.DirectedPseudograph;
import org.jgrapht.graph.ListenableDirectedGraph;

import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdgeFactory;
import uk.ac.york.qvtd.dependencies.derivations.impl.MappingDerivationsImpl;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyEdge.EdgeType;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.DependencyPath;
import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.inter.ParameterEdge;
import uk.ac.york.qvtd.dependencies.util.GraphAttributes;
import uk.ac.york.qvtd.dependencies.util.MallardDOTExporter;
import uk.ac.york.qvtd.dependencies.util.Pair;
import uk.ac.york.qvtd.dependencies.util.SolarizedHelper;
import uk.ac.york.qvtd.dependencies.util.SolarizedHelper.SolarizedPalette;


/**
 * The Class DependencyGraphImpl.
 *
 * @author Horacio Hoyos
 */
public class DependencyGraphImpl extends DirectedPseudograph<DependencyVertex, DependencyEdge> implements DependencyGraph {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3593826659918483026L;

    /** The producer-consumer relations. The key is the producer and the value the consumers */
    private Map<MappingAction, ProducerConsumer> consumersCache = new HashMap<MappingAction, ProducerConsumer>();

    /** The listener. */
    private final ListenableDirectedGraph<DependencyVertex, DependencyEdge> listener;

    /** The paths. */
    //private List<GraphPath<DependencyVertex, DependencyEdge>> paths;

    /** The middle domain. */
    private TypedModel middleDomain;

    /** The neighbour index. */
    private final DirectedNeighborIndex<DependencyVertex, DependencyEdge> neighborIndex;

    /** The paths to vertex. */
    //private Map<DependencyVertex, List<GraphPath<DependencyVertex,DependencyEdge>>> pathsToVertex;

    /** The paths to CallActionOperations. */
    //private Map<MappingAction, Set<List<MappingAction>>> pathsFromRootCache = new HashMap<>();

    /** The predecessor actions cache. */
    private Map<MappingAction, Set<MappingAction>> predecessorActionsCache;

    /** The root vertex. */
    private MappingAction rootVertex;

    // TODO routes and loop routes should probably use sets instead

    /**
     * All the paths that exist in the graph grouped by <source, target>.
     */
    private Map<Pair<DependencyVertex,DependencyVertex>, List<DependencyPath>> routes = new HashMap<>();

    /**
     * All the paths that have a loop
     */
    private Map<DependencyVertex, List<DependencyPath>> loopRoutes = new HashMap<>();

    /** The successor actions cache. */
    private Map<MappingAction, Set<MappingAction>> successorActionsCache;

    /** The consumer-producer relations. The key is the consumer and the value the suppliers */
    private Map<MappingAction, ConsumerSupplier> supplierCache = new HashMap<MappingAction, ConsumerSupplier>();

    private ClassRelationships classrel;

    /**
     * Creates a new directed graph.
     */
    public DependencyGraphImpl(ClassRelationships classrel) {
        super(DependencyEdge.class);
        listener = new ListenableDirectedGraph<>(this);
        neighborIndex = new DirectedNeighborIndex<DependencyVertex, DependencyEdge>(listener);
        listener.addGraphListener(neighborIndex);
        predecessorActionsCache = new HashMap<MappingAction, Set<MappingAction>>();
        successorActionsCache = new HashMap<MappingAction, Set<MappingAction>>();
        //paths = new ArrayList<GraphPath<DependencyVertex, DependencyEdge>>();
        //pathsToVertex = new HashMap<DependencyVertex, List<GraphPath<DependencyVertex,DependencyEdge>>>();
        this.classrel = classrel;
    }

    @Override
    public void createMappingDerivations() {
        for (DependencyVertex dv : vertexSet()) {
            if (dv instanceof MappingAction) {
                EdgeFactory<Variable, PropertyEdge> ef = ((MappingAction) dv).getMappingDependenciesEdgeFactory();
                MappingDerivationsImpl der = new MappingDerivationsImpl(ef);
                for (DependencyEdge de : incomingEdgesOf(dv)) {
                    if (de instanceof ParameterEdge) {
                        ((ParameterEdge) de).getVariables().stream()
                            .forEach(v -> der.addVertex(v, de.getSource()));
                    }
                }
                for (Variable sv : der.vertexSet()) {
                    for (Variable tv : der.vertexSet()) {
                        if (!sv.equals(tv)) {
                            PropertyEdge e = ef.createEdge(sv, tv);
                            if (e != null) {		// Dont use der.addEdge(s, t) cause it does not check if the edge is null, we could override addEdge in AbstractBaseGraph
                                der.addEdge(sv, tv, e);
                                e = ((PropertyEdgeFactory<Variable, PropertyEdge>)ef).createOppositeEdge(sv, tv);
                                if (e != null) {
                                    der.addEdge(tv, sv, e);
                                }
                            }
                        }
                    }
                }
                ((MappingAction) dv).setMappingDerivations(der);
                if (!der.vertexSet().isEmpty()) {
                    der.findMinimumSpanningTrees();
                    der.setIsFromRoot(this, classrel);
                }
            }
        }
    }


    /* (non-Javadoc)
     * @see uk.ac.york.qvtd.dependencies.inter.DependencyGraph#findAllActionPaths(uk.ac.york.qvtd.dependencies.inter.MappingAction)
     */
    @Override
    @Deprecated
    public void findAllPathsBetweenMappingActions() {
        Set<MappingAction> vxs = new HashSet<MappingAction>(this.vertexSet().stream()
                .filter(MappingAction.class::isInstance)
                .map(MappingAction.class::cast)
                .collect(Collectors.toSet()));
        List<Pair<MappingAction, MappingAction>> pairs = Pair.pairCombinations(vxs);
        for (Pair<MappingAction, MappingAction> pair : pairs) {
            findAllPaths(pair.first, pair.second);
        }
        // Do a DFS, create the paths additively: i.e. append the paths of the child to the paths of the parent
//        DependencyGraphIterator dfs = new DependencyGraphIterator(this, getRootVertex());
//        PathCreator pc = new PathCreator(this);
//        dfs.addTraversalListener(pc);
//        while (dfs.hasNext()) {
//            dfs.next();
//        }
    }


    public Set<MappingAction> getAllMappingActions() {
        return vertexSet().stream()
                .filter(MappingAction.class::isInstance)
                .map(MappingAction.class::cast)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<@NonNull ClassDatum> getAllSubClassDatums(ClassDatum classdatum) {
        @NonNull
        Set<@NonNull Class> subs = classrel.getAllSubClasses((@NonNull Class) classdatum.getType());
        return vertexSet().stream().filter(ClassDatum.class::isInstance)
                .map(ClassDatum.class::cast)
                .filter(cd -> cd.getDomain().equals(classdatum.getDomain()))	// Same domain too
                .filter(cd -> subs.contains(cd.getType()))
                .collect(Collectors.toSet());
    }


    @Override
    public Set<MappingAction> getConsumers(MappingAction vertex) {
        ProducerConsumer pc = consumersCache.get(vertex);
        assert pc != null;		// Forgot to initialise?
        Set<MappingAction> direct = pc.getDirectConsumers();
        Set<MappingAction> indirect = pc.getIndirectConsumers();
        direct.addAll(indirect);
        return direct;
    }

    @Override
    public @NonNull Set<ClassDatum> getContainedTypesProducedByAction(MappingAction action) {
        Set<ClassDatum> types = new HashSet<>(getTypesProducedByAction(action));
        return types.stream()
                .flatMap(cd -> outgoingEdgesOf(cd).stream()
                            .filter(e -> e.getType().equals(EdgeType.CONTAINER))
                            .map(ie -> ie.getTarget())
                        )
                .map(ClassDatum.class::cast)
                .collect(Collectors.toCollection(HashSet<ClassDatum>::new));
    }

    @Override
    @Deprecated
    public List<DependencyPath> getLoopsFrom(MappingAction action) {
        if (!loopRoutes.containsKey(action)) {
            return Collections.emptyList();
        }
        return loopRoutes.get(action);
    }

    @Override
    public List<MappingAction> getMappingVertices() {
        List<MappingAction> result = new ArrayList<MappingAction>();
        for (DependencyVertex dv : vertexSet()) {
            if (dv instanceof MappingAction)
                result.add((MappingAction) dv);
        }
        return result;
    }

    @Override
    public TypedModel getMiddleDomain() {
        return middleDomain;
    }


    @Override
    public DirectedNeighborIndex<DependencyVertex, DependencyEdge> getNeighborIndex() {
        return neighborIndex;
    }

    /**
     * Get the paths between the two nodes. Empty list if there are no paths.
     * @param source
     * @param target
     * @return
     */
    @Deprecated
    public List<DependencyPath> getPathsBetween(MappingAction source, MappingAction target) {

        Pair<MappingAction, MappingAction> pair = Pair.of(source, target);
        if (routes.containsKey(pair)) {
            return new ArrayList<>(routes.get(pair));
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    @Deprecated
    public List<DependencyPath> getPathsFrom(Pair<MappingAction, MappingAction> pair) {
        if (!routes.containsKey(pair)) {
            return Collections.emptyList();
        }
        return routes.get(pair);
    }

    // Cached
    // TODO Property dependencies + predicates can be used to filter the predecessors
    @Override
    public Set<MappingAction> getPredecessorActions(MappingAction vertex) {
        Set<MappingAction> result = predecessorActionsCache.get(vertex);
        if (result != null)
            return result;
        result = new HashSet<MappingAction>();
        Set<DependencyVertex> inputTypes = neighborIndex.predecessorsOf(vertex);
        for (DependencyVertex it : inputTypes) {
            for (DependencyVertex prod : neighborIndex.predecessorsOf(it)) {
                if (prod instanceof MappingAction)
                    result.add((MappingAction) prod);
            }
        }
        // the root is not really a requirement
        result.remove(getRootVertex());
        predecessorActionsCache.put(vertex, result);
        return result;
    }

    @Override
    public Set<Pair<ClassDatum, MappingAction>> getProducerRelations(MappingAction action) {
        ProducerConsumer pc = consumersCache.get(action);
        assert pc != null;		// Forgot to initialize?
        return pc.getConsumerRelations();
    }

    @Override
    public Set<MappingAction> getSupplier(MappingAction vertex) {
        ConsumerSupplier cp = supplierCache.get(vertex);
        assert cp != null;		// Forgot to initialise?
        Set<MappingAction> direct = cp.getDirectSuppliers();
        Set<MappingAction> indirect = cp.getIndirectSuppliers();
        // Only add indirect suppliers if any of the visited mappings produces the same types, i.e. the vertex is
        // in the list of suppliers of any of the visited mappings
//        Set<MappingAction> visitedConsumers = visited.stream()
//                .map(ma -> consumersCache.get(ma))
//                .filter(c -> c != null)
//                .flatMap(nc -> nc.getAllConsumers().stream())
//                .collect(Collectors.toSet());
//        indirect.retainAll(visitedConsumers);
        direct.addAll(indirect);
        return direct;
    }

    @Override
    public List<MappingAction> getProducersOf(ClassDatum cd) {

        List<MappingAction> producers = new ArrayList<MappingAction>();
        Iterator<Entry<MappingAction, ProducerConsumer>> entryIt = consumersCache.entrySet().iterator();
        while (entryIt.hasNext()) {
            Entry<MappingAction, ProducerConsumer> entry = entryIt.next();
            if (entry.getValue().getProducedTypes().contains(cd)) {
                producers.add(entry.getKey());
            }
        }
        return producers;
    }

    @Override
    public List<MappingAction> getConsumersOf(ClassDatum cd) {

        List<MappingAction> consumers = new ArrayList<MappingAction>();
        List<ClassDatum> hierarchy = getSuperClassDatums(cd);
        hierarchy.add(cd);
        Iterator<Entry<MappingAction, ConsumerSupplier>> entryIt = supplierCache.entrySet().iterator();
        while (entryIt.hasNext()) {
            Entry<MappingAction, ConsumerSupplier> entry = entryIt.next();
            // Can consume any type in the hierarchy (includes cd)
            for (ClassDatum hcd : hierarchy) {
                if (entry.getValue().getConsumedTypes().contains(hcd)) {
                    consumers.add(entry.getKey());
                }
            }
        }
        return consumers;
    }


    @Override
    public Set<List<MappingAction>> getReducedLoopsFrom(MappingAction action) {
        if (!loopRoutes.containsKey(action)) {
            return Collections.emptySet();
        }
        Set<List<MappingAction>> loopPaths = loopRoutes.get(action).stream()
                .filter(path -> path.getEdgeList().stream()
                        .anyMatch(e -> !e.getType().equals(EdgeType.CONTAINER)))
                .map(path -> Graphs.getPathVertexList(path).stream()
                        .filter(MappingAction.class::isInstance)
                        .map(MappingAction.class::cast)
                        .collect(Collectors.toList()))
                .collect(Collectors.toSet());
        Set<List<MappingAction>> reduced = new HashSet<>();
        for(List<MappingAction> path : loopPaths) {
            int count = 0;
            for (List<MappingAction> other : loopPaths) {
                if (isSubList(path, other)) {
                    count++;
                    if (count > 1) {
                        break;
                    }
                }
            }
            if (count == 1) {
                reduced.add(path);
            }
        }
        return reduced;
    }

    @Override
    public Set<List<MappingAction>> getReducedPathsFromRoot(MappingAction target) {

        List<List<MappingAction>> allPaths = getPathsBetween(rootVertex, target).stream()
                .filter(path -> path.getEdgeList().stream()
                        .anyMatch(e -> !e.getType().equals(EdgeType.CONTAINER)))
                .map(path -> Graphs.getPathVertexList(path).stream()
                        .filter(MappingAction.class::isInstance)
                        .map(MappingAction.class::cast)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());
        Set<List<MappingAction>> reduced = new HashSet<>();
        for(List<MappingAction> path : allPaths) {
            int count = 0;
            for (List<MappingAction> other : allPaths) {
                if (path != other) {
                    if (isSubList(other, path)) {
                        count++;
                        if (count > 1) {
                            break;
                        }
                    }
                }
            }
            if (count == 1) {
                reduced.add(path);
            }
        }
//        Set<List<MappingAction>> reduced2 = eliminateSubPaths(allPaths);
        return reduced;
//        return allPaths;
    }

    /**
     * This method should only be called after the graph has been completed
     * @return
     */
    @Override
    public MappingAction getRootVertex() {
        if (rootVertex == null) {
            // The root rule is the Vertex with no incoming edges
            for (DependencyVertex v : vertexSet())
                if (v instanceof MappingAction)
                    if (inDegreeOf(v) == 0) {
                        rootVertex = (MappingAction) v;
                        // Call the graph tools so we are ready
                        //findAllActionPaths(rootV);
                        break;
                    }
        }
        return rootVertex;
    }

    /**
     * Get the List of ProducerConsumers that consume the types produced by the MappingAction at runtime, i.e. direct
     * or indirect consumers
     * <p>
     * @param vertex
     * @return
     */
    @Deprecated
    @Override
    public Set<MappingAction> getRuntimeConsumers(MappingAction vertex,  Set<MappingAction> visited) {
//        ProducerConsumer pc = consumersCache.get(vertex);
//        assert pc != null;		// Forgot to initialise?
//        Set<MappingAction> direct = pc.getDirectConsumers();
//        Set<MappingAction> indirect = pc.getIndirectConsumers();
//        Set<MappingAction> visitedConsumers = visited.stream()
//                .map(ma -> consumersCache.get(ma))
//                .filter(c -> c != null)
//                .flatMap(nc -> nc.getAllConsumers().stream())
//                .collect(Collectors.toSet());
//        indirect.retainAll(visitedConsumers);
//        direct.addAll(indirect);
//        return direct;
        return null;
    }

    @Override
    public List<ClassDatum> getSubClassDatums(ClassDatum classDatum) {
        Set<DependencyVertex> pred = neighborIndex.predecessorsOf(classDatum);
        List<ClassDatum> sub = new ArrayList<ClassDatum>();
        for (DependencyVertex p : pred) {
            if (p instanceof ClassDatum)
                sub.add((ClassDatum) p);
        }
        return sub;
    }


    @Override
    @Deprecated
    public Set<MappingAction> getSuccessorActions(MappingAction vertex) {
        Set<MappingAction> result = successorActionsCache.get(vertex);
        if (result != null)
            return result;
        result = new HashSet<MappingAction>();
        Set<DependencyVertex> outputTypes = neighborIndex.successorsOf(vertex);
        for (DependencyVertex ot : outputTypes) {
            for (DependencyVertex cons : neighborIndex.successorsOf(ot)) {
                if (cons instanceof MappingAction)
                    result.add((MappingAction) cons);
            }
        }
        successorActionsCache.put(vertex, result);
        return result;
    }


    @Override
    public List<ClassDatum> getSuperClassDatums(ClassDatum classDatum) {
        Set<DependencyVertex> succ = neighborIndex.successorsOf(classDatum);
        List<ClassDatum> sup = new ArrayList<ClassDatum>();
        for (DependencyVertex p : succ) {
            if (p instanceof ClassDatum)
                sup.add((ClassDatum) p);
        }
        return sup;
    }

    @Override
    public ConsumerSupplier getSupplierInformation(MappingAction action) {
        return supplierCache.get(action);
    }

    @Override
    public List<ClassDatum> getTypesConsumedByAction(MappingAction action) {
        ConsumerSupplier pc = supplierCache.get(action);
        assert pc != null;		// Forgot to initialize?
        return pc.getAllConsumedTypes();
    }

    //******************* NEW METHODS
    @Override
    public List<ClassDatum> getTypesProducedByAction(MappingAction action) {
        ProducerConsumer pc = consumersCache.get(action);
        assert pc != null;		// Forgot to initialize?
        return pc.getAllProducedTypes();
    }

    @Override
    public void initialize() {
        //findAllPathsBetweenMappingActions();
        createAllConsumerRelations();
        createAllSupplierRelations();
    }

    @Override
    public void setMiddleDomain(TypedModel middleDomain) {
        this.middleDomain = middleDomain;
    }


    /**
     * @param rootVertex the rootVertex to set
     */
    @Override
    public void setRootVertex(MappingAction rootVertex) {
        this.rootVertex = rootVertex;
    }


    @Override
    public String toDOT() {
        MallardDOTExporter<DependencyVertex, DependencyEdge> ex = new MallardDOTExporter<DependencyVertex, DependencyEdge>(
                new GraphAttributes<DependencyVertex, DependencyEdge>(false),
                new IntegerNameProvider<DependencyVertex>(),
                new StringNameProvider<DependencyVertex>(),
                null,
                new DependencyVertexAttributes(false),
                new DependencyEdgeAttributes(false));
        StringWriter stringWriter = new StringWriter();
        ex.export(stringWriter, this);
        return stringWriter.toString();

    }

    /**
     * Adds the cycle.
     *
     * @param edgeList the edge list
     * @param source the source
     * @param target
     */
    private void addCycle(DependencyPath path, DependencyVertex loopVertex) {
        // Reduce the path to the loop
        List<DependencyEdge> edgeList = path.getEdgeList();
        DependencyEdge startEdge = edgeList.stream()
                .filter(e -> e.getSource().equals(loopVertex))
                .findFirst()
                .get();

        List<DependencyEdge> loopEdges = edgeList.subList(edgeList.indexOf(startEdge), edgeList.size());
        DependencyPath loopPath = new DependencyPathImpl(this, loopEdges);
        loopPath.setLoopVertex(loopVertex);
        List<DependencyPath> paths = loopRoutes.get(loopVertex);
        if (paths == null) {
            paths = new ArrayList<DependencyPath>();
            loopRoutes.put(loopVertex, paths);
        }
        if (!paths.stream().anyMatch(ep -> Graphs.getPathVertexList(ep).equals(Graphs.getPathVertexList(loopPath)))) {
            paths.add(loopPath);
        }
    }

    //    @Override
    private void addPath(DependencyPath path) {

        DependencyVertex source = path.getStartVertex();
        DependencyVertex target = path.getEndVertex();
        Pair<DependencyVertex,DependencyVertex> pair = Pair.of(source, target);
        List<DependencyPath> paths = routes.get(pair);
        if (paths == null) {
            paths = new ArrayList<DependencyPath>();
            routes.put(pair, paths);
        }
        if (!paths.stream().anyMatch(ep -> Graphs.getPathVertexList(ep).equals(Graphs.getPathVertexList(path)))) {
            paths.add(path);
        }
    }





    private void createAllConsumerRelations() {

        for (MappingAction ma : getAllMappingActions()) {
            ProducerConsumer pc = createConsumerRelations(ma);
            consumersCache.put(ma, pc);
        }
    }

    private void createAllSupplierRelations() {

        for (MappingAction ma : getAllMappingActions()) {
            ConsumerSupplier pc = createSupplierRelations(ma);
            supplierCache.put(ma, pc);
        }
    }

    /**
     *
     */
    private ProducerConsumer createConsumerRelations(MappingAction producer) {

        ProducerConsumer pc = new ProducerConsumer(producer);
        for (DependencyEdge oe : outgoingEdgesOf(producer)) {
            ClassDatum classDatum = (ClassDatum) oe.getTarget();
            List<Pair<MappingAction, Integer>> consumers = getAllConsumers(classDatum);
            for(Pair<MappingAction, Integer> p : consumers) {
                int variables;
                if (rootVertex.equals(producer)) {
                    variables = 1;		// The root can produce as many instances as necessary.
                }
                else {
                    variables = p.second;
                }
                pc.addConsumer(p.first, classDatum, variables);
            }
        }
        return pc;
    }


    private List<Pair<MappingAction, Integer>> getAllConsumers(ClassDatum classDatum) {

        List<Pair<MappingAction, Integer>> result = new ArrayList<Pair<MappingAction,Integer>>();
        for (DependencyEdge oe : outgoingEdgesOf(classDatum)) {
            DependencyVertex v = oe.getTarget();
            if (v instanceof MappingAction) {
                Pair<MappingAction, Integer> p = Pair.of((MappingAction)v, ((ParameterEdge) oe).getVariables().size());
                result.add(p);
            }
            else if(oe.getType().equals(EdgeType.INHERITANCE)) {
                result.addAll(getAllConsumers((ClassDatum) v));
            }
        }
        return result;
    }


    private ConsumerSupplier createSupplierRelations(MappingAction consumer) {

        ConsumerSupplier cs = new ConsumerSupplier(consumer);
        for (DependencyEdge ie : incomingEdgesOf(consumer)) {
            ClassDatum classDatum = (ClassDatum) ie.getSource();
            List<MappingAction> suppliers = getAllSuppliers(classDatum);
            for(MappingAction p : suppliers) {
                int variables;
                if (rootVertex.equals(p)) {
                    variables = 1;		// The root can produce as many instances as necessary.
                }
                else {
                    variables = ((ParameterEdge)ie).getVariables().size();
                }
                cs.addSupplier(p, classDatum, variables);
            }
        }
        return cs;

    }

    private List<MappingAction> getAllSuppliers(ClassDatum classDatum) {
        List<MappingAction> result = new ArrayList<MappingAction>();
        for (DependencyEdge ie : incomingEdgesOf(classDatum)) {
            DependencyVertex v = ie.getSource();
            if (v instanceof MappingAction) {
                result.add((MappingAction) v);
            }
            else if(ie.getType().equals(EdgeType.INHERITANCE)) {
                result.addAll(getAllSuppliers((ClassDatum) v));
            }
        }
        return result;
    }

    /**
     * Eliminate all GraphPaths that are proper subpaths of another path. This only works for subpaths that have a common
     * start and finish vertex.
     * TODO We need to think about handling loops, as loop paths are not sets!!
     * <p>
     * Modified from <a href="http://stackoverflow.com/q/14106121">StackOverflow #14106121</a>
     * Suppose you label all the input paths.
     * <p>
     * A={1, 2, 3, 4}, B={1, 4}, C={1, 2, 4}, D={1, 3, 2, 4}, E={1,3,4}
     * Now build intermediate sets, one per item/pos, containing the paths that use that item/pos
     * <p>
     * (1,1)={A,B,C,D}
     * (2,2)={A,C}
     * (2,3)={D}
     * (3,3)={A}
     * (3,2)={D}
     * (4,l)={A,B,C,D,E}
     *<p>
     * Now for each item/pos compute the intersection of all the label sets of its paths:
     *<p>
     * For A, {A,B,C,D} intersect {A,C}  intersect {A} intersect {A,B,C,D,E} = {A}   (*)
     * For B,  {A,B,C,D} intersect {A,B,C,D,E} = A,B,C,D
     *<p>
     * If the intersection contains some label other than for the set itself, then it's s a subset of that set.
     *
     * @param <T>
     *
     * @param paths
     * @return
     */
    private Set<List<MappingAction>> eliminateSubPaths(List<List<MappingAction>>  paths) {
        if (paths.size() <= 1) {
            return new HashSet<List<MappingAction>>(paths);		// Copy
        }
        Map<Pair<MappingAction,Integer>, Integer> sets_containing_element = paths.stream()
                .collect(new SubListCollector());
        return paths.stream()
                .filter(path -> isPowerOfTwo(path.stream()
                            .map(ma->sets_containing_element.get(Pair.of(ma, path.indexOf(ma))))
                            .reduce((acc, item) -> acc & item)
                            .get()))
                .collect(Collectors.toSet());
    }

//    private void addConsumedSubtypes(List<ClassDatum> operationTypes, ClassDatum t) {
//        Set<@NonNull ClassDatum> subtypes = incomingEdgesOf(t).stream()
//                .filter(se -> se.getType().equals(EdgeType.INHERITANCE))
//                .map(ie -> ie.getSource())
//                .map(ClassDatum.class::cast)
//                .collect(Collectors.toSet());
//        if (subtypes.isEmpty())
//            return;
//        Set<ClassDatum> existing = new HashSet<ClassDatum>(operationTypes);
//        for (ClassDatum st : subtypes) {
//            if (existing.add(st))
//                operationTypes.add(st);
//        }
//        subtypes.forEach(st -> addConsumedSubtypes(operationTypes, st));
//    }

    /**
     * Finds all the paths from source to target, following the directed edges
     * in forward direction.
     * Adapted from: "Simões, R., APAC: An exact algorithm for retrieving cycles
     * and paths in all kinds of graphs, Tékhne-Revista de Estudos Politécnicos,
     * Instituto Politécnico do Cávado e do Ave, 2009, 39-55",  to use edges
     * instead of vertices, paths that don't enter loops and paths that enter a
     * loop but can exit it.
     * @param source the source
     * @param target the target
     */
    private void findAllPaths(MappingAction source, MappingAction target) {
        Deque<DependencyPath> pathStack = new LinkedList<>();
        for (DependencyEdge e : outgoingEdgesOf(source)) {
            DependencyPath path = new DependencyPathImpl(this, e);
            pathStack.push(path);
        }
        DependencyPath path;
        while (!pathStack.isEmpty()) {
            path = pathStack.pop();
            DependencyVertex tailVertex = path.getEndVertex();
            // TODO add if we want self loops!
//			if (u.getTarget().equals(source)) {
//				addCycle2(path, outEdge.getTarget());
//			}
            for (DependencyEdge outEdge : outgoingEdgesOf(tailVertex)) {
                DependencyPath newPath = new DependencyPathImpl(this, path.getEdgeList());
                if (path.hasLoop()) {
                    newPath.setLoopVertex(path.getLoopVertex());
                }
                newPath.addEdge(outEdge);
                DependencyVertex nextVertex = outEdge.getTarget();
                if (!path.containsVertex(nextVertex)) {	// If the vertex is in the path, it involves a loop
                    if (target.equals(nextVertex)) {
                        addPath(newPath);
                    } else {
                        pathStack.push(newPath);
                    }
                } else  if (nextVertex instanceof MappingAction) {
                    // If the repeated vertex is a MappingAction, we have a loop
                    if (!newPath.hasLoop()) {
                        addCycle(newPath, nextVertex);
                        newPath.setLoopVertex(nextVertex);
                        // How can we add paths that enter and exit the loop? Because the !path.containsVertex(nextVertex)
                        // will start indicating loops all the time! We can mark the path as looped!?
                        pathStack.push(newPath);
                    }
                } else {
                    // More paths can continue from the ClassDatum
                    pathStack.push(newPath);
                }
            }
        }
    }


    /**
     * Get all the paths ending at the given vertex.
     * @param vertex The final vertex
     * @return A list of paths.
     */
    @Deprecated
    private List<DependencyPath> getPathsEndingAtVertex(MappingAction vertex) {
        List<DependencyPath> paths = routes.entrySet().stream()
            .filter(e -> e.getKey().second.equals(vertex))
            .flatMap(e -> e.getValue().stream())
            .collect(Collectors.toList());
        // Add paths from loops
        List<DependencyPath> loopPaths = loopRoutes.get(vertex);
        if (loopPaths != null) {
            paths.addAll(loopPaths);
        }
        return paths;
    }


    /**
     * Get all the paths starting at the given vertex, paths include both flat and looped paths
     * @param vertex The starting vertex
     * @return A list of paths.
     */
    @Deprecated
    private List<DependencyPath> getPathsStartingAtVertex(MappingAction vertex) {
        List<DependencyPath> paths = routes.entrySet().stream()
              .filter(e -> e.getKey().first.equals(vertex))
              .flatMap(e -> e.getValue().stream())
              .collect(Collectors.toList());
        // Add paths from loops
        List<DependencyPath> loopPaths = loopRoutes.get(vertex);
        if (loopPaths != null) {
            paths.addAll(loopPaths);
        }
        return paths;
    }

    /**
     * Returns True iff n is a power of two.  Assumes n > 0.
     */
    private boolean isPowerOfTwo(int n) {
        return (n & (n-1)) == 0;
    }

    /**
     * Returns true iff l1 is a sublist of l (i.e., every member of l1 is in l,
     * and for every e1 < e2 in l1, there is an e1 < e2 occurrence in l).
     * Since this is for paths, it should ignore the 1st and last items!
     */
    private <T> boolean isSubList(List<T> l1, List<? super T> l) {
      Iterator<? super T> it = l.iterator();
      for (T o1 : l1) {
        if (!it.hasNext()) {
          return false;
        }
        Object o = it.next();
        while ((o == null && !(o1 == null)) || (o != null && !o.equals(o1))) {
          if (!it.hasNext()) {
            return false;
          }
          o = it.next();
        }
      }
      return true;
    }

    private class DependencyEdgeAttributes implements ComponentAttributeProvider<DependencyEdge> {

        //private final SolarizedPalette PRODUCER_EDGE_COLOR;
        private final String CONSUMER_EDGE_STYLE = "dashed";
        private final SolarizedPalette CONTAINER_EDGE_COLOR = SolarizedPalette.YELLOW;
        private final String CONTAINER_EDGE_STYLE = "dotted";
        // TODO maybe move this to a DOT class
        private final String EDGE_COLOR_ATTR_NAME = "color";
        private final String EDGE_STYLE_ATTR_NAME = "style";
        private final SolarizedPalette INHERITANCE_EDGE_COLOR = SolarizedPalette.CYAN;
        private final String INHERITANCE_EDGE_STYLE = "dotted";
        private final SolarizedPalette MULTI_EDGE_COLOR = SolarizedPalette.RED;
        private final String PRODUCER_EDGE_STYLE = "solid";
        private final SolarizedPalette SINGLE_EDGE_COLOR = SolarizedPalette.GREEN;

        public DependencyEdgeAttributes(boolean dark) {
            super();
            //PRODUCER_EDGE_COLOR = SolarizedHelper.getPrimaryContentTone(dark);
        }


        @Override
        public Map<String, String> getComponentAttributes(DependencyEdge edge) {

            Map<String, String> attr = new HashMap<String, String>();
            switch (edge.getType()) {
            case CONSUMER:
                attr.put(EDGE_STYLE_ATTR_NAME, CONSUMER_EDGE_STYLE);
                assert edge instanceof ParameterEdge;
                if (((ParameterEdge)edge).isMultiple())
                    attr.put(EDGE_COLOR_ATTR_NAME, MULTI_EDGE_COLOR.getHexValue());
                else
                    attr.put(EDGE_COLOR_ATTR_NAME, SINGLE_EDGE_COLOR.getHexValue());
                break;
            case PRODUCER:
                attr.put(EDGE_STYLE_ATTR_NAME, PRODUCER_EDGE_STYLE);
                assert edge instanceof ParameterEdge;
                if (((ParameterEdge)edge).isMultiple())
                    attr.put(EDGE_COLOR_ATTR_NAME, MULTI_EDGE_COLOR.getHexValue());
                else
                    attr.put(EDGE_COLOR_ATTR_NAME, SINGLE_EDGE_COLOR.getHexValue());
                break;
            case CONTAINER:
                attr.put(EDGE_STYLE_ATTR_NAME, CONTAINER_EDGE_STYLE);
                attr.put(EDGE_COLOR_ATTR_NAME, CONTAINER_EDGE_COLOR.getHexValue());
                break;
            case INHERITANCE:
                attr.put(EDGE_STYLE_ATTR_NAME, INHERITANCE_EDGE_STYLE);
                attr.put(EDGE_COLOR_ATTR_NAME, INHERITANCE_EDGE_COLOR.getHexValue());
            }
            return attr;
        }

    }

    private class DependencyVertexAttributes implements ComponentAttributeProvider<DependencyVertex> {


        private static final String NODE_FONTCOLOR_ATTR_NAME = "fontcolor";
        private List<SolarizedPalette> availDomColors = new ArrayList<SolarizedPalette>(
                Arrays.asList(SolarizedPalette.BLUE,
                        SolarizedPalette.VIOLET,
                        SolarizedPalette.MAGENTA));

        private Map<TypedModel, SolarizedPalette> domainColors = new HashMap<TypedModel, SolarizedPalette>();

        private final SolarizedPalette MAPPING_ACTION_COLOR = SolarizedPalette.ORANGE;


        private final SolarizedPalette NODE_FONT_COLOR;
        // TODO maybe move this to a DOT class
        private final String NODE_SHAPE_ATTR_NAME = "shape";
        private final String CLASS_DATUM_SHAPE = "box";
        private final String NODE_COLOR_ATTR_NAME = "color";
        private final String MAPPING_ACTION_SHAPE = "hexagon";

        public DependencyVertexAttributes(boolean dark) {
            super();
            NODE_FONT_COLOR = SolarizedHelper.getPrimaryContentTone(dark);

        }

        /**
         * Colours are assigned to domains by order of request from the availDomColors list.
         */
        @Override
        public Map<String, String> getComponentAttributes(DependencyVertex component) {
            Map<String, String> attr = new HashMap<String, String>();

            if (component instanceof ClassDatum) {
                attr.put(NODE_SHAPE_ATTR_NAME, CLASS_DATUM_SHAPE);
                SolarizedPalette color = domainColors.get(((ClassDatum) component).getDomain());
                if (color == null) {
                    color = availDomColors.get(0);
                    availDomColors.remove(0);
                    domainColors.put(((ClassDatum) component).getDomain(), color);
                }
                attr.put(NODE_COLOR_ATTR_NAME, color.getHexValue());
            } else if (component instanceof MappingAction) {
                attr.put(NODE_SHAPE_ATTR_NAME, MAPPING_ACTION_SHAPE);
                attr.put(NODE_COLOR_ATTR_NAME, MAPPING_ACTION_COLOR.getHexValue());
            }
            attr.put(NODE_FONTCOLOR_ATTR_NAME, NODE_FONT_COLOR.getHexValue());
            return attr;
        }

    }

    private static class SubListCollector
        implements Collector<List<MappingAction>,
            Map<Pair<MappingAction,Integer>, Integer>,
            Map<Pair<MappingAction,Integer>, Integer>> {

        AtomicInteger index = new AtomicInteger();

        @Override
        public BiConsumer<Map<Pair<MappingAction,Integer>, Integer>, List<MappingAction>> accumulator() {
            return (acc, col) -> {
//                    System.out.println(acc);
//                    col.forEach(elem -> System.out.println(Pair.of(elem, col.indexOf(elem) == col.size() -1 ? 0 : col.indexOf(elem)+1)));
                    col.stream().map(
                            elem->acc.merge(Pair.of(elem, col.indexOf(elem) == col.size() -1 ? 0 : col.indexOf(elem)+1),
                                    1 << index.get(), (old, v) -> old | (1 << index.get())));
                    index.incrementAndGet();};
        }

        @Override
        public Set<java.util.stream.Collector.Characteristics> characteristics() {

            return Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.UNORDERED,
                    Collector.Characteristics.IDENTITY_FINISH));
        }

        @Override
        public BinaryOperator<Map<Pair<MappingAction,Integer>, Integer>> combiner() {
            return (acc1, acc2) -> {
                throw new UnsupportedOperationException();
              };
        }

        @Override
        public Function<Map<Pair<MappingAction,Integer>, Integer>, Map<Pair<MappingAction,Integer>, Integer>> finisher() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Supplier<Map<Pair<MappingAction,Integer>, Integer>> supplier() {
            return HashMap::new;
        }

    }




}