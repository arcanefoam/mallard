package uk.ac.york.qvtd.dependencies.inter.impl;

import org.eclipse.ocl.pivot.Variable;
import org.eclipse.qvtd.pivot.qvtimperative.Mapping;
import org.jgrapht.EdgeFactory;

import uk.ac.york.qvtd.dependencies.derivations.DerivationGraph;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.derivations.impl.MappingDerivationsImpl;
import uk.ac.york.qvtd.dependencies.derivations.impl.PropertyEdgeFactoryImpl;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;

public class MappingActionImpl implements MappingAction  {

        private Mapping mapping;
        private MappingDerivationsImpl derivations;

        /**
         * The root mapping action doesn't have a mapping
         */
        public MappingActionImpl() {
            super();
        }

        public MappingActionImpl(Mapping mapping) {
            super();
            this.mapping = mapping;
        }

        /* (non-Javadoc)
         * @see uk.ac.york.qvtd.dependencies.impl.MappingVertex#getLabel()
         */
        @Override
        public String getLabel() {
            return mapping.getName();
        }

        /* (non-Javadoc)
         * @see uk.ac.york.qvtd.dependencies.impl.MappingVertex#getMapping()
         */
        @Override
        public Mapping getMapping() {
            return mapping;
        }

        /* (non-Javadoc)
         * @see uk.ac.york.qvtd.dependencies.impl.MappingVertex#setMapping(org.eclipse.qvtd.pivot.qvtimperative.Mapping)
         */
        @Override
        public void setMapping(Mapping mapping) {
            this.mapping = mapping;
        }

        @Override
        public void setMappingDerivations(MappingDerivationsImpl derivations) {
            this.derivations = derivations;
            // Calculate the spanning tree
            //this.derivations.findMinimumSpanningTree();
        }

        @Override
        public MappingDerivationsImpl getMappingDerivations() {
            return derivations;
        }

        @Override
        public EdgeFactory<Variable, PropertyEdge> getMappingDependenciesEdgeFactory() {
            // TODO Auto-generated method stub
            return new PropertyEdgeFactoryImpl(mapping);
        }

        @Override
        public String toString() {
            if (mapping != null)
                return mapping.getName();
            return "root";
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((mapping== null) ? 0 : mapping.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            MappingActionImpl other = (MappingActionImpl) obj;
            if (mapping == null) {
                if (other.mapping != null)
                    return false;
            } else if (!mapping.equals(other.mapping))
                return false;
            return true;
        }

        @Override
        public int compareTo(Object o) {
            return (getLabel()).compareTo(((MappingActionImpl)o).getLabel());
        }


    }