/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.derivations.impl;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.DirectedWeightedPseudograph;
import org.jgrapht.graph.DirectedWeightedSubgraph;
import org.jgrapht.util.FibonacciHeap;
import org.jgrapht.util.FibonacciHeapNode;

import uk.ac.york.qvtd.dependencies.derivations.DirectedMinimumArborescence;

/**
 * The Class EdmondsDirectedMinimumArborescence.
 *
 * Based on the algorithm presented in Lecture Notes on "Analysis of Algorithms", Directed Minimum Spanning Trees,
 * Princeton University.
 * <a href="https://www.cs.princeton.edu/courses/archive/spring13/cos528/directed-mst-1.pdf">Consulted 05/2016 </a>
 *
 * @param <V> the value type
 * @param <E> the element type
 * @author Horacio Hoyos
 */
public class EdmondsDirectedMinimumArborescence<V, E> implements DirectedMinimumArborescence<V, E> {

    public static final int DUMMY_EDGE_COST = 1000;

    /** The trace. */
    Map<V, SuperVertex> vertexTrace = new HashMap<V, SuperVertex>();

    Map<E, FibonacciHeapNode<E>> edgeTrace = new HashMap<E, FibonacciHeapNode<E>>();

    /** The forest. */
    private EdmondsUnionFind<SuperVertex> forest;

    /** The graph. */
    private DirectedWeightedPseudograph<V, E> graph;

    /**
     * Instantiates a new Edmonds directed minimum arborescence. The contraction phase is performed
     * as part of the instantiation.
     *
     * @param graph the graph
     */
    public EdmondsDirectedMinimumArborescence(final DirectedWeightedPseudograph<V, E> graph) {
        super();
        if (graph.vertexSet().isEmpty()) {
            throw new IllegalArgumentException("The graph is empty");
        }

            this.graph = graph;
            if (!graph.vertexSet().isEmpty()) {
                // Contract
                SuperVertex v = initialize(graph);
                SuperVertex start = v;
                do {
                    SuperVertex v_set = getSet(v);
                    E minCostE = v.p.removeMin().getData();
                    V s = graph.getEdgeSource(minCostE);
                    SuperVertex u = getSuperVertex(s);
                    SuperVertex u_set = getSet(u);
                    if (!u_set.equals(v_set)) {		// The edge (u, root) is a self-loop (in the graph or a loop from parent to child), in which case it should be ignored. (See upper drawing.)
                        v_set.in = minCostE;
                        v_set.prev = u_set;
                        if (u.in == null) {		// Not visited, expand
                            v = u_set;
                            for (E e : graph.incomingEdgesOf(s)) {
                                assert edgeTrace.get(e) == null;
                                FibonacciHeapNode<E> fhn = new FibonacciHeapNode<E>(e);
                                edgeTrace.put(e, fhn);
                                u.p.insert(fhn, getCurrenteEdgeCost(e));
                            }
                        }
                        else {					// New cycle
                            SuperVertex c = new SuperVertex();
                            forest.addElement(c);
                            List<SuperVertex> loopNodes = new ArrayList<SuperVertex>();
                            loopNodes.add(v);
                            forest.union(c, v_set);
                            while(!v.equals(u)) {	// In JGraphT UnionFind parent is initialized to self
                                loopNodes.add(u_set);
                                E originalEdge = u_set.in;
                                double newValue = graph.getEdgeWeight(originalEdge)-forest.findValue(u);
                                forest.changeValue(-newValue, u_set);
                                forest.union(c, u_set);
                                SuperVertex temp_u_set = u_set;
                                u = u_set.prev;
                                u_set = getSet(u);
                                if (u_set.equals(c)) {
                                    // The u_set was contracted, we will never loop to v
                                    break;
                                }
                            }
                            for (SuperVertex ln : loopNodes) {
                                c.p = FibonacciHeap.union(c.p, ln.p);
                            }
                            v = c;
                        }
                    }
                } while (!v.p.isEmpty() && v != start);
            }

    }

    private SuperVertex getSuperVertex(V v) {

        SuperVertex superVertex = vertexTrace.get(v);
        if (superVertex == null) {
            superVertex = new SuperVertex(v);
            vertexTrace.put(v, superVertex);
            forest.addElement(superVertex);
        }
        return superVertex;
    }

    private SuperVertex getSet(SuperVertex v) {

        return forest.find(v);
    }

    private double getCurrenteEdgeCost(E e) {

        double original = graph.getEdgeWeight(e);
        V w = graph.getEdgeTarget(e);
        SuperVertex sw = getSuperVertex(w);
        return original + forest.findValue(sw);
    }

    /* (non-Javadoc)
     * @see uk.ac.york.qvtd.compiler.utilities.DirectedMinimumArborescence#getMinimumSpanningTreeEdgeSet(java.lang.Object)
     */
    @Override
    public DirectedWeightedSubgraph<V, E> getMinimumSpanningTree(V root) {
        // Cache the intial values for fixing
        Map<SuperVertex, E> restore = new HashMap<SuperVertex, E>();
        EdmondsUnionFind<SuperVertex> f = new EdmondsUnionFind<SuperVertex>(forest);
        // Expand
        SuperVertex r = getSuperVertex(root);
        Deque<SuperVertex> children = dismantle(r, f);
        while (!children.isEmpty()) {
            SuperVertex c = children.pollFirst();
            E e = c.in;
            SuperVertex v = getSuperVertex(graph.getEdgeTarget(e));
            E v_in = v.in;
            SuperVertex s = getSuperVertex(graph.getEdgeSource(e));
            SuperVertex v_in_source = getSuperVertex(graph.getEdgeSource(v_in));
            if (!s.equals(r)) {
                // The edge does not come from the root.
//                if (graph.getEdgeWeight(v_in) >= graph.getEdgeWeight(e)) { // ((graph.getEdgeWeight(e) != DUMMY_EDGE_COST)) {
//                    restore.put(v, v.in);
//                    v.in = e;
//                }
                if (f.getRankMap().get(v) <= f.getRankMap().get(r)) {
                    SuperVertex p_v = f.getParentMap().get(v);
                    if (f.getRankMap().get(p_v) <= f.getRankMap().get(r)) {		// WE don't want to point to contractions
                        restore.put(v, v.in);
                        v.in = e;
                    }
                }
            }
            else {
                SuperVertex p_v = f.getParentMap().get(v);
                if (f.getRankMap().get(p_v) <= f.getRankMap().get(v)) {		// WE don't want to point to dismantled contractions
                    restore.put(v, v.in);
                    v.in = e;
                }
            }
            children.addAll(dismantle(forest.getParentMap().get(v), f));
        }
        Set<E> subEdgeSet = graph.vertexSet().stream()
                .filter(v -> !v.equals(root))
                .map(u -> getSuperVertex(u).in)
                .collect(Collectors.toSet());
        Set<V> subVertexSet = subEdgeSet.stream()
                .flatMap(e->Stream.of(graph.getEdgeSource(e),graph.getEdgeTarget(e)))
                .collect(Collectors.toSet());
        // Restore the initial info
        for (Entry<SuperVertex, E> entry: restore.entrySet()) {
            entry.getKey().in = entry.getValue();
        }
        return new DirectedWeightedSubgraph<>(graph, subVertexSet, subEdgeSet);
    }

    /**
     * Dismantle is a destructive operation, since we want to reuse the contraction, we can duplicate the forest.
     *
     * @param root the root
     * @param f the f
     * @return the deque
     */
    private Deque<SuperVertex> dismantle(SuperVertex u, EdmondsUnionFind<SuperVertex> f) {
        Deque<SuperVertex> childern = new ArrayDeque<SuperVertex>();
        SuperVertex root = u;
        SuperVertex parent = f.find(u);
        do {
            u = parent;
            for (SuperVertex v : u.getChildren()) {
                f.removeParent(v);
                Set<EdmondsDirectedMinimumArborescence<V, E>.SuperVertex> v_child = v.getChildren();
                List<EdmondsDirectedMinimumArborescence<V, E>.SuperVertex> filter_child = v_child.stream()
                        .filter(c -> !c.getChildren().contains(root))
                        .collect(Collectors.toList());
                if (!filter_child.isEmpty()) {
                    childern.add(v);
                }
            }
            parent = f.find(u);
        } while (parent != u);
        return childern;
    }


    /**
     * Initialize. Starts by creating a SuperVertex, for every vertex u of the graph. Next,
     * inserts each edge (u, v) of the input graph into the appropriate priority queue
     *
     * @param graph the graph
     * @return the super v
     */
    private SuperVertex initialize(final WeightedGraph<V, E> graph) {
//        Set<SuperVertex> vertices = new HashSet<SuperVertex>();
//        for (V v : graph.vertexSet()) {
//            SuperVertex sv = new SuperVertex(v);
//            trace.put(v, sv);
//            vertices.add(sv);
//        }
        V s = graph.vertexSet().iterator().next();
        SuperVertex sv = new SuperVertex(s);
        vertexTrace.put(s, sv);
        for (E e : graph.edgeSet()) {
            V t = graph.getEdgeTarget(e);
            if (t.equals(s)) {
                FibonacciHeapNode<E> fhn = new FibonacciHeapNode<E>(e);
                edgeTrace.put(e, fhn);
                sv.p.insert(fhn, graph.getEdgeWeight(e));
            }
        }
        forest = new EdmondsUnionFind<SuperVertex>();
        forest.addElement(sv);
        return sv;
    }

    /**
     * The Class SuperV.
     */
    private class SuperVertex  {

        /** The vertex. Can be null for super vertices*/
        private final V vertex;
        /**
         * The selected incoming edge of u. During the contraction phase this is the cheapest edge
         * entering u. If no incoming edge was selected yet, then in[u] = null.
         */
        private E in = null;

        // This is managed by the union find and the heap
//        /**
//         * A constant that should be added to the weight of all edges entering u to obtain the
//         * adjusted cost of the edge in the contracted graph. Initially cost[u] = 0
//         */
//        private double k = 0.0;

        /**
         * The (super-)vertex preceding u in the path constructed by the algorithm. If u is not
         * yet in the path, or was just added to it, then prev[u] = null.
         */
        private SuperVertex prev = null;

        // Parent and children are implicitly maintained by the Forest
//        /**
//         * The super-vertex into which u was contracted. If u was not contracted yet, then
//         * parent[u] = null.
//         */
//        private SuperVertex parent = null;
//
//        /**
//         * A list of the (super-)vertices contacted to form u. If u is a vertex of the original
//         * graph then children[u] is empty.
//         */
//        private List<SuperVertex> children = new ArrayList<SuperVertex>();

        /**
         * A priority queue that holds all the incoming edges of u.
         */
        private FibonacciHeap<E> p = new FibonacciHeap<E>();


        /**
         * Instantiates a new super vertex.
         */
        public SuperVertex() {
            this.vertex = null;
        }

        /**
         * Instantiates a new super vertex.
         *
         * @param vertex the vertex
         */
        public SuperVertex(V vertex) {
            this.vertex = vertex;
        }

        public Set<SuperVertex> getChildren() {
            Map<SuperVertex, SuperVertex> parentMap = forest.getParentMap();
            return parentMap.entrySet().stream()
                    .filter(entry -> this.equals(entry.getValue()))
                    .map(entry -> entry.getKey())
                    .filter(v -> !v.equals(this))
                    .collect(Collectors.toSet());
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString()
        {
            if (vertex == null) {
                return "{" + getChildren().stream().map(v -> v.toString()).collect(Collectors.joining()) + "}";
            }
            else {
                return vertex.toString();
            }

        }

    }

    public String toDOT() {
        StringBuilder sb = new StringBuilder();
        sb.append("digraph {\n");
        sb.append("label = A0;\n");
        sb.append("compund=true;\n");
        if (forest != null) {
            // We need the top level sets, i.e. parent = self and vertex == null
            List<SuperVertex> topVertices = forest.getParentMap().keySet().stream()
                    .filter(sv -> (sv.vertex == null) && forest.find(sv).equals(sv))
                    .collect(Collectors.toList());
            sb.append(getClusters(topVertices));
            // Add vertices with no parent, i.e at root level
            sb.append("{ rank = same;\n");
            for (SuperVertex leaf : vertexTrace.values()) {
                if (forest.find(leaf).equals(leaf)) {
                    sb.append(toDOTString(leaf.vertex.toString()));
                }
            }
            sb.append("}\n");
            for (E edge : graph.edgeSet()) {
                String source = toDOTString(graph.getEdgeSource(edge).toString());
                String target = toDOTString(graph.getEdgeTarget(edge).toString());
                sb.append(String.format("%s -> %s;\n", source, target));

            }
        }
        sb.append("}\n");
        return sb.toString();
    }

    /**
     *
     * @param vertices
     * @return
     */
    private String getClusters(Collection<SuperVertex> vertices) {
        StringBuilder sb = new StringBuilder();
        //int id = 65;
        List<SuperVertex> leaves = vertices.stream()
                .filter(sv -> sv.getChildren().isEmpty())
                .collect(Collectors.toList());
        sb.append("{ rank = same;\n");
        for (SuperVertex leaf : leaves) {
            sb.append(toDOTString(leaf.vertex.toString()));
        }
        sb.append("}\n");
        List<SuperVertex> nodes = vertices.stream()
                .filter(sv -> !sv.getChildren().isEmpty())
                .collect(Collectors.toList());
        for (SuperVertex sv : nodes) {
            Set<SuperVertex> children = sv.getChildren();
            int level = forest.getRankMap().get(sv);
            String id = sv.toString();
            String name = String.format("%s%d",id, level);
            sb.append(String.format("subgraph \"cluster%s\" {\n",name));
            //String label = String.format("%s.%d",(char)id, level);
            sb.append(String.format("label = %s;\n",toDOTString(sv.toString())));
            sb.append(getClusters(children));
            sb.append("}\n");
            //id++;

        }
        return sb.toString();
    }

    private String toDOTString(String string) {
        return String.format("\"%s\"", string);
    }



}
