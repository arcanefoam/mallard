package uk.ac.york.qvtd.dependencies.inter;

import org.eclipse.ocl.pivot.Variable;
import org.eclipse.qvtd.pivot.qvtimperative.Mapping;
import org.jgrapht.EdgeFactory;

import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.derivations.impl.MappingDerivationsImpl;

public interface MappingAction extends DependencyVertex, Comparable<Object> {

    String getLabel();

    Mapping getMapping();

    void setMapping(Mapping m);

    MappingDerivationsImpl getMappingDerivations();

    void setMappingDerivations(MappingDerivationsImpl derivations);

    EdgeFactory<Variable, PropertyEdge> getMappingDependenciesEdgeFactory();


}