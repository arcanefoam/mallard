/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.util;

import java.util.ArrayList;
import java.util.List;

public class SolarizedHelper {

    public static SolarizedPalette getBackgroundTone(boolean dark) {
        if (dark) {
            return SolarizedPalette.BASE03;
        }
        return SolarizedPalette.BASE3;
    }

    public static SolarizedPalette getEmphasizedContentTone(boolean dark) {
        if (dark) {
            return SolarizedPalette.BASE0;
        }
        return SolarizedPalette.BASE00;
    }


    public static SolarizedPalette getHighlightsTone(boolean dark) {
        if (dark) {
            return SolarizedPalette.BASE02;
        }
        return SolarizedPalette.BASE2;
    }

    public static SolarizedPalette getPrimaryContentTone(boolean dark) {
        if (dark) {
            return SolarizedPalette.BASE0;
        }
        return SolarizedPalette.BASE00;
    }

    public static SolarizedPalette getSecondaryContentTone(boolean dark) {
        if (dark) {
            return SolarizedPalette.BASE01;
        }
        return SolarizedPalette.BASE1;
    }
    /**
     * The Solarized Colour Palette
     *
     * Provides convenient methods for using the <a href="http://ethanschoonover.com/solarized">Solarized Pallete</a>.
     *
     */
    public enum SolarizedPalette {

        BASE03("#002b36"),
        BASE02("#073642"),
        BASE01("#586e75"),
        BASE00("#657b83"),
        BASE0("#839496"),
        BASE1("#93a1a1"),
        BASE2("#eee8d5"),
        BASE3("#fdf6e3"),
        YELLOW("#b58900"),
        ORANGE("#cb4b16"),
        RED("#dc322f"),
        MAGENTA("#d33682"),
        VIOLET("#6c71c4"),
        BLUE("#268bd2"),
        CYAN("#2aa198"),
        GREEN("#859900");

        private final String hexValue;

        private SolarizedPalette(String hexValue) {
            this.hexValue = hexValue;
        }

        /**
         * @return the hexValue
         */
        public String getHexValue() {
            return hexValue;
        }

        public List<Integer> getRGBValue() {

            List<Integer> result = new ArrayList<Integer>();
            result.add((Integer.decode(hexValue) & 0xFF0000) >> 16);
            result.add((Integer.decode(hexValue) & 0xFF00) >> 8);
            result.add((Integer.decode(hexValue) & 0xFF));
            return result;
        }
    }
}