/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.Comment;
import org.eclipse.ocl.pivot.Element;
import org.eclipse.ocl.pivot.ElementExtension;
import org.eclipse.ocl.pivot.OCLExpression;
import org.eclipse.ocl.pivot.OperationCallExp;
import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.PropertyCallExp;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.ocl.pivot.VariableExp;
import org.eclipse.ocl.pivot.util.Visitor;
import org.eclipse.qvtd.pivot.qvtbase.Domain;
import org.eclipse.qvtd.pivot.qvtbase.Predicate;
import org.eclipse.qvtd.pivot.qvtbase.Rule;
import org.eclipse.qvtd.pivot.qvtbase.Transformation;
import org.eclipse.qvtd.pivot.qvtcorebase.AbstractMapping;
import org.eclipse.qvtd.pivot.qvtcorebase.Area;
import org.eclipse.qvtd.pivot.qvtcorebase.BottomPattern;
import org.eclipse.qvtd.pivot.qvtcorebase.GuardPattern;
import org.eclipse.qvtd.pivot.qvtimperative.Mapping;
import org.eclipse.qvtd.pivot.qvtimperative.MappingStatement;

/**
 * Wrapper Class to implement Helper functions till they are incorporated into
 * the QVT base code.
 *
 * @author Horacio Hoyos
 */
public class MappingImplExt implements Mapping {

    /** The delegate. */
    private Mapping delegate;

    /**
     * Instantiates a new mapping impl ext.
     *
     * @param delegate the delegate
     */
    public MappingImplExt(Mapping delegate) {
        super();
        this.delegate = delegate;
    }

    /**
     * Finds a property that relates the two variables by inspecting the mapping
     * predicates
     * @param v1
     * @param v2
     * @return The property if found, or null otherwise
     */
    public Property findRealtingProperty(Variable v1, Variable v2) {
        // All predicates in QVTi are in the mapping's guard pattern
        for (Predicate p : getGuardPattern().getPredicate()) {
            if (OCLExpressionAnalysis.oclExpIsSimpleAttributeNavigation(p.getConditionExpression())) {
                OperationCallExp ocexp = (OperationCallExp) p.getConditionExpression();
                if (((VariableExp) ocexp.getOwnedArguments().get(0)).getReferredVariable().equals(v2)) {
                    PropertyCallExp pcexp = (PropertyCallExp)ocexp.getOwnedSource();
                    if (((VariableExp) pcexp.getOwnedSource()).getReferredVariable().equals(v1))
                        return pcexp.getReferredProperty();
                }
            }
        }
        return null;
    }

    public List<Variable> getInputVariables() {
        List<Variable> ret = new ArrayList<Variable>();
        // Variables are only defined in the domains' guard pattern
        for (Domain d : delegate.getDomain()) {
            ret.addAll(((Area) d).getGuardPattern().getVariable());
        }
        return ret;
    }

    public List<Variable> getOutputVariables() {
        List<Variable> ret = new ArrayList<Variable>();
        // Variables are only defined in the domains' bottom pattern
        for (Domain d : delegate.getDomain()) {
            ret.addAll(((Area) d).getBottomPattern().getRealizedVariable());
        }
        return ret;
    }


/*****************************/
/********* QVT STUFF *********/
/*****************************/

    public EList<Adapter> eAdapters() {
        return delegate.eAdapters();
    }

    public boolean eDeliver() {
        return delegate.eDeliver();
    }

    public AbstractMapping getContext() {
        return delegate.getContext();
    }

    public void eSetDeliver(boolean deliver) {
        delegate.eSetDeliver(deliver);
    }

    public EList<? extends AbstractMapping> getRefinement() {
        return delegate.getRefinement();
    }

    public void eNotify(Notification notification) {
        delegate.eNotify(notification);
    }

    public String getName() {
        return delegate.getName();
    }

    public GuardPattern getGuardPattern() {
        return delegate.getGuardPattern();
    }

    public MappingStatement getMappingStatement() {
        return delegate.getMappingStatement();
    }

    public EList<Domain> getDomain() {
        return delegate.getDomain();
    }

    public void setName(String value) {
        delegate.setName(value);
    }

    public List<Comment> getAnnotatingComments() {
        return delegate.getAnnotatingComments();
    }

    public void setMappingStatement(MappingStatement value) {
        delegate.setMappingStatement(value);
    }

    public void setGuardPattern(GuardPattern value) {
        delegate.setGuardPattern(value);
    }

    public boolean isIsDefault() {
        return delegate.isIsDefault();
    }

    public EList<Mapping> getAllMappings() {
        return delegate.getAllMappings();
    }

    public BottomPattern getBottomPattern() {
        return delegate.getBottomPattern();
    }

    public List<ElementExtension> getOwnedExtensions() {
        return delegate.getOwnedExtensions();
    }

    public void setIsDefault(boolean value) {
        delegate.setIsDefault(value);
    }

    public EClass eClass() {
        return delegate.eClass();
    }

    public Rule getOverrides() {
        return delegate.getOverrides();
    }

    public List<Element> getOwnedAnnotations() {
        return delegate.getOwnedAnnotations();
    }

    public void setBottomPattern(BottomPattern value) {
        delegate.setBottomPattern(value);
    }

    public Resource eResource() {
        return delegate.eResource();
    }

    public EList<Variable> getAllVariables() {
        return delegate.getAllVariables();
    }

    public void setOverrides(Rule value) {
        delegate.setOverrides(value);
    }

    public List<Comment> getOwnedComments() {
        return delegate.getOwnedComments();
    }

    public EObject eContainer() {
        return delegate.eContainer();
    }

    public Transformation getTransformation() {
        return delegate.getTransformation();
    }

    public List<Element> allOwnedElements() {
        return delegate.allOwnedElements();
    }

    public EStructuralFeature eContainingFeature() {
        return delegate.eContainingFeature();
    }

    public Element getValue(Type stereotype, String propertyName) {
        return delegate.getValue(stereotype, propertyName);
    }

    public void setTransformation(Transformation value) {
        delegate.setTransformation(value);
    }

    public <R> R accept(Visitor<R> visitor) {
        return delegate.accept(visitor);
    }

    public EObject getESObject() {
        return delegate.getESObject();
    }

    @Deprecated
    public EObject getETarget() {
        return delegate.getETarget();
    }

    public EList<Rule> getOverridden() {
        return delegate.getOverridden();
    }

    public EReference eContainmentFeature() {
        return delegate.eContainmentFeature();
    }

    public EList<EObject> eContents() {
        return delegate.eContents();
    }

    public TreeIterator<EObject> eAllContents() {
        return delegate.eAllContents();
    }

    public boolean eIsProxy() {
        return delegate.eIsProxy();
    }

    public EList<EObject> eCrossReferences() {
        return delegate.eCrossReferences();
    }

    public Object eGet(EStructuralFeature feature) {
        return delegate.eGet(feature);
    }

    public Object eGet(EStructuralFeature feature, boolean resolve) {
        return delegate.eGet(feature, resolve);
    }

    public void eSet(EStructuralFeature feature, Object newValue) {
        delegate.eSet(feature, newValue);
    }

    public boolean eIsSet(EStructuralFeature feature) {
        return delegate.eIsSet(feature);
    }

    public void eUnset(EStructuralFeature feature) {
        delegate.eUnset(feature);
    }

    public Object eInvoke(EOperation operation, EList<?> arguments) throws InvocationTargetException {
        return delegate.eInvoke(operation, arguments);
    }

    @Override
    public EList<Property> getCheckedProperties() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EList<Property> getEnforcedProperties() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EList<Class> getPolledClasses() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public OCLExpression getOwnedKeyExpression() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setOwnedKeyExpression(OCLExpression value) {
        // TODO Auto-generated method stub

    }

    @Override
    public EList<? extends AbstractMapping> getSpecification() {
        // TODO Auto-generated method stub
        return null;
    }




}
