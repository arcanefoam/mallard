package uk.ac.york.qvtd.dependencies.derivations.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Wrapper so we can return the parent map.
 *
 * @author Horacio Hoyos
 */
public class EdmondsUnionFind<T> {

    private Map<T, T> parentMap;
    private Map<T, Integer> rankMap;
    private Map<T, Double> valueMap;


    public EdmondsUnionFind() {
        this(Collections.emptySet());
    }

    public EdmondsUnionFind(EdmondsUnionFind<T> forest) {
        this();
        getParentMap().putAll(forest.getParentMap());
        getRankMap().putAll(forest.getRankMap());
        valueMap.putAll(forest.valueMap);
    }

    /**
     * Creates a UnionFind instance with all of the elements of elements in
     * seperate sets.
     */
    public EdmondsUnionFind(Set<T> elements)
    {
        parentMap = new HashMap<T, T>();
        rankMap = new HashMap<T, Integer>();
        valueMap = new HashMap<T, Double>();
        for (T element : elements) {
            parentMap.put(element, element);
            rankMap.put(element, 0);
            valueMap.put(element, 0.0);
        }
    }


    /**
     * Adds a new element to the data structure in its own set.
     *
     * @param element The element to add.
     */
    public void addElement(T element)
    {
        parentMap.put(element, element);
        rankMap.put(element, 0);
        valueMap.put(element, 0.0);
    }
    /**
     * Add delta to the value of all elements in the set
     * @param delta
     * @param set
     */
    public void changeValue(double delta, T set) {
        for (Entry<T, T> entry : getParentMap().entrySet()) {
            if (entry.getValue().equals(set)) {
                T element = entry.getKey();
                double oldValue = findValue(element);
                valueMap.put(element, oldValue+delta);
            }
        }
    }


    /**
     * Returns the representative element of the set that element is in.
     *
     * @param element The element to find.
     *
     * @return The element representing the set the element is in.
     */
    public T find(T element)
    {
        if (!parentMap.containsKey(element)) {
            throw new IllegalArgumentException(
                "elements must be contained in given set");
        }

        T parent = parentMap.get(element);
        if (parent.equals(element)) {
            return element;
        }

        T newParent = find(parent);
        //parentMap.put(element, newParent);
        return newParent;
    }


    public double findValue(T element) {
        return valueMap.getOrDefault(element, 0.0);
    }

    /**
     * By reading the element we set the parent to itself, i.e. remove the parent
     * @param v
     */
    public void removeParent(T v) {
        getParentMap().put(v, v);
        getRankMap().put(v, 0);
    }

    /**
     * Merges the sets which contain element1 and element2.
     *
     * @param element1 The first element to union.
     * @param element2 The second element to union.
     */
    public void union(T element1, T element2)
    {
        if (!parentMap.containsKey(element1)
            || !parentMap.containsKey(element2))
        {
            throw new IllegalArgumentException(
                "elements must be contained in given set");
        }

        T parent1 = find(element1);
        T parent2 = find(element2);

        //check if the elements are already in the same set
        if (parent1.equals(parent2)) {
            return;
        }

        int rank1 = rankMap.get(parent1);
        int rank2 = rankMap.get(parent2);
        //if (rank1 > rank2) {
        //    parentMap.put(parent2, parent1);
        //} else if (rank1 < rank2) {
        //    parentMap.put(parent1, parent2);
        //} else {
            parentMap.put(parent2, parent1);
            rankMap.put(parent2, rank1);
            rankMap.put(parent1, rank2 + 1);
        //}
    }

    /**
     * @return map from element to parent element
     */
    protected Map<T, T> getParentMap()
    {
        return parentMap;
    }

    /**
     * @return map from element to rank
     */
    protected Map<T, Integer> getRankMap()
    {
        return rankMap;
    }

}