package uk.ac.york.qvtd.dependencies.derivations;

import org.eclipse.ocl.pivot.Variable;
import org.jgrapht.WeightedGraph;
import org.jgrapht.alg.interfaces.MinimumSpanningTree;

import uk.ac.york.qvtd.dependencies.derivations.impl.EdmondsDirectedMinimumArborescence;

public interface DerivationGraph extends WeightedGraph<Variable, PropertyEdge> {


    public static final long ONE_MULTIPLICITY_COST = 1;

    /**
     * The cost of a multiplicity property was initially considered as 10.
     * However, multiplicity properties affect the complexity and once a loop
     * exists, the cost of binding an input variable to a loop variable is the
     * same as a direct binding.
     */
    public static final long MANY_MULTIPLICITY_COST = 10;
    public static final long IMPLICIT_NAVIGATION_MULT = 5;
    public boolean pathExists(Variable sourceVariable, Variable targetVariable);

    /**
     * Uses the {@link EdmondsDirectedMinimumArborescence} to find the MDSTs of the derivation graph.
     *
     * The individual MDST will be created for mappings with multiple primary variables (i.e. the derivation
     * graph is not strongly connected or not connected).
     */
    void findMinimumSpanningTrees();


//	public final static int DIRECT_NAVIGATION_COST = 1;
//	public final static int IMPLICIT_NAVIGATION_COST = 5;
//	public final static int UNLIMITED_MULTIPLICITY_NAVIGATION_COST = 10;
//	public final static int SINGLE_NAVIGATION_MULTIPLIER = 1;
//	public final static int MULTIPLE_NAVIGATION_MULTIPLIER = 2;
//	public final static double CONTEXT_VARIABLE_BINDING_COST = 0.2;
//	public final static double CONTEXT_REALIZED_VARIABLE_BINDING_COST = 0.1; // Using ralized is preferred, this will help loops?
//	public final static int ALLINSTANCES_BINDING_COST = 100;


}
