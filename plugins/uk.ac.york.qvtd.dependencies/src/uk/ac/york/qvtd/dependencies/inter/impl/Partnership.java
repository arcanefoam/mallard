/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.dependencies.inter.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.util.Pair;

/**
 * The Class Partnership is used to keep track of the producer-consumer relations.
 * <p>
 * This information can later be used, for example, to validate that they all have been satisfied
 */
class Partnership {

    /** The individual. */
    private MappingAction individual;

    /** The partners. */
    private Map<ClassDatum, List<Relation>> partners = new HashMap<ClassDatum, List<Relation>>();

    /** The unmodifiable types set. */
    private Set<ClassDatum> unmodifiableTypesSet = null;

    /**
     * Instantiates a new partnership.
     *
     * @param individual the individual
     */
    public Partnership(MappingAction individual) {
        super();
        this.individual = individual;
    }

    /**
     * Add a new relation between the individual and the partner, through the given type.
     *
     * @param partner the partner
     * @param classDatum the class datum
     * @param instances number of instances that are part of the relation
     */
    public void addRelation(MappingAction partner, ClassDatum classDatum, int instances) {
        Relation newR = new Relation(partner, classDatum, instances);
        List<Relation> relations = partners.get(classDatum);
        if (relations == null) {
            relations = new ArrayList<>();
            partners.put(classDatum, relations);
        }
        relations.add(newR);
    }



    /**
     * Get the set of all partners.
     *
     * @return the all partners
     */
    public Set<MappingAction> getAllPartners() {
        return partners.values().stream()
            .flatMap(rs -> rs.stream())
            .map(dr -> dr.partner)
            .collect(Collectors.toSet());
    }

    /**
     * Get the set of all partners related by the specific ClassDatum.
     *
     * @param classDatum the class datum
     * @return the all partners
     */
    public Set<MappingAction> getAllPartners(ClassDatum classDatum) {
        return partners.values().stream()
            .flatMap(rs -> rs.stream())
            .filter(r ->  r.classDatum.equals(classDatum))
            .map(dr -> dr.partner)
            .collect(Collectors.toSet());
    }

    /**
     * Get the ClassDatums that define the direct relations to the given partner
     * @param partner
     * @return
     */
    public Set<ClassDatum> getDirectRelationsWith(MappingAction partner) {
        return partners.entrySet().stream()
            .filter(e -> e.getValue().stream().anyMatch(p -> p.partner.equals(partner) && p.isDirect))
            .map(e -> e.getKey())
            .collect(Collectors.toSet());
    }

    /**
     * Get the ClassDatums that define the indirect relations to the given partner
     * @param partner
     * @return
     */
    public Set<ClassDatum> getIndirectRelationsWith(MappingAction partner) {
        return partners.entrySet().stream()
            .filter(e -> e.getValue().stream().anyMatch(p -> p.partner.equals(partner) && !p.isDirect))
            .map(e -> e.getKey())
            .collect(Collectors.toSet());
    }

    /**
     * Gets the all relation types.
     *
     * @return the all relation types
     */
    public Set<ClassDatum> getRelationTypes() {
        if (unmodifiableTypesSet == null) {
            unmodifiableTypesSet = Collections.unmodifiableSet(partners.keySet());
        }
        return unmodifiableTypesSet;
    }

    /**
     * Returns a list of all the types involved in the relation, with duplicates to indicate that
     * one or more variables are part of the relation for a given type.
     * @return
     */
    public List<ClassDatum> getAllRelationTypes() {
        List<ClassDatum> result = new ArrayList<ClassDatum>();
        Iterator<Entry<ClassDatum, List<Relation>>> pIt = partners.entrySet().iterator();
        while (pIt.hasNext()) {
            Entry<ClassDatum, List<Relation>> entry = pIt.next();
            // All relations should have the same number of instances, i.e. it represents the # of vars consumed/produced
            List<Integer> instances = entry.getValue().stream().map(r -> r.instances).collect(Collectors.toList());
            // Not true for the root, it produces as many as necessary per mapping
            //assert instances.stream().distinct().collect(Collectors.toList()).size() == 1;
            int max = instances.stream().mapToInt(Integer::intValue).max().orElse(0);
            result.addAll(Collections.nCopies(max, entry.getKey()));
        }
        return result;
    }


    /**
     * Get the set of direct partners.
     *
     * @return the direct partners
     */
    public Set<MappingAction> getDirectPartners() {
        return partners.values().stream()
            .flatMap(rs -> rs.stream())
            .filter(r -> r.isDirect)
            .map(dr -> dr.partner)
            .collect(Collectors.toSet());
    }

    /**
     * Get the set of direct partners related by the specific ClassDatum.
     *
     * @param classDatum the class datum
     * @return the direct partners
     */
    public Set<MappingAction> getDirectPartners(ClassDatum classDatum) {
        return partners.values().stream()
            .flatMap(rs -> rs.stream())
            .filter(r -> r.isDirect && r.classDatum.equals(classDatum))
            .map(dr -> dr.partner)
            .collect(Collectors.toSet());
    }

    /**
     * Get the set of indirect partners.
     *
     * @return the indirect partners
     */
    public Set<MappingAction> getIndirectPartners() {
        return partners.values().stream()
            .flatMap(rs -> rs.stream())
            .filter(r -> !r.isDirect)
            .map(dr -> dr.partner)
            .collect(Collectors.toSet());
    }


    /**
     * Get the set of indirect partners related by the specific ClassDatum.
     *
     * @param classDatum the class datum
     * @return the indirect partners
     */
    public Set<MappingAction> getIndirectPartners(ClassDatum classDatum) {
        return partners.values().stream()
            .flatMap(rs -> rs.stream())
            .filter(r -> !r.isDirect && r.classDatum.equals(classDatum))
            .map(dr -> dr.partner)
            .collect(Collectors.toSet());
    }


    /**
     * Gets the partner relations.
     *
     * @return the partners
     */
    public Set<Pair<ClassDatum, MappingAction>> getPartnerRelations() {
        return partners.entrySet().stream()
            .flatMap(entry -> entry.getValue().stream()
                    .map(r -> Pair.of(entry.getKey(), r.partner))
                )
            .collect(Collectors.toSet());
    }


    /**
     * The Class Relation.
     *
     * A relation is Direct if it is a relation through a single instance of the Class Datum. If not, then either
     * another individual has to be part of the relation (provide additional instances) or other instances must exist
     * in the "world".
     */
    private class Relation {

        /** The class datum. */
        private final ClassDatum classDatum;

        /** The is direct. */
        private final boolean isDirect;

        /** The partner. */
        private final MappingAction partner;

        /** The instances. */
        private final int instances;

        /**
         * Instantiates a new relation.
         *
         * @param partner the partner
         * @param classDatum the class datum
         * @param instances the number of instances in the relation (i.e. variables of the type)
         */
        public Relation(MappingAction partner, ClassDatum classDatum, int instances) {
            super();
            this.classDatum = classDatum;
            this.partner = partner;
            this.instances = instances;
            this.isDirect = instances == 1;
        }
    }


}