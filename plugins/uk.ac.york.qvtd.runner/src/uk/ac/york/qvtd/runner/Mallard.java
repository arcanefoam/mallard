/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.runner;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpecBuilder;
import uk.ac.york.qvtd.benchmark.QVTdBenchmark_Old;
import uk.ac.york.qvtd.benchmark.QVTdBenchmark_Old.Benchmark;
import uk.ac.york.qvtd.compiler.Configuration;

/**
 * The mallard class can be used to run benchmarks on the Execution Plan exploration. It can either be used to evaluate
 * resources while doing a complete exploration (e.g. large/complex transformations) or running the benchmarks for
 * different transformations and model sizes.
 *
 * @author Horacio Hoyos
 *
 */
public class Mallard {

    /**
     * The Enum Options.
     */
    public enum Options {

        /** The test name. */
        TEST_NAME("t", "Name of the test case to run."),

        /** The number iterations. */
        NUMBER_ITERATIONS("i", "Number of iterations to run each test. Default is 10. "),

        /** The warmup time. */
        WARMUP_TIME("w", "Warmup time for the VM (in ms). Defult is 5 min. Set to 0 for no warmup."),

        /** The verbose. */
        VERBOSE("v", "Verbose"),

        /** Run only one solution */
        SINGLE("f", "Run the best soltuion"),

        /** Run the best solutions */
        BEST("b", "Run the best soltuions"),

        /** Run all the solutions */
        ALL("a", "Run all the soltuions");

        /** The option. */
        private String option;

        /** The description. */
        private String description;

        /**
         * Instantiates a new options.
         *
         * @param option the option
         * @param description the description
         */
        Options(String option, String description) {
            this.option = option;
            this.description = description;
        }

    }

    static OptionParser parser = new OptionParser();

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {

        String test = "";
        int iterations;
        int warmupTime;
        boolean warmpup;
        boolean verbose;

        parser.accepts(Options.TEST_NAME.option, Options.TEST_NAME.description).withRequiredArg();
        parser.accepts(Options.NUMBER_ITERATIONS.option, Options.NUMBER_ITERATIONS.description)
                .withRequiredArg()
                .ofType(Integer.class)
                .defaultsTo(10);
        parser.accepts(Options.WARMUP_TIME.option, Options.WARMUP_TIME.description)
                .withRequiredArg()
                .ofType(Integer.class)
                .defaultsTo(300000);
        parser.accepts(Options.VERBOSE.option, Options.VERBOSE.description);
        OptionSpecBuilder sop = parser.accepts(Options.SINGLE.option, Options.SINGLE.description);
        OptionSpecBuilder bop = parser.accepts(Options.BEST.option, Options.BEST.description);
        OptionSpecBuilder aop = parser.accepts(Options.ALL.option, Options.ALL.description);
        sop.requiredUnless(Options.BEST.option, Options.ALL.option);
        bop.requiredUnless(Options.SINGLE.option, Options.ALL.option);
        aop.requiredUnless(Options.SINGLE.option, Options.BEST.option);
        OptionSet options = parser.parse(args);
        String qvtdBenchmarkHome = System.getenv("QVTD_BM_HOME");
        if (qvtdBenchmarkHome == null) {
            System.err.println("Please set the QVTD_BM_HOME variable to the root path of your tests. "
                    + "The sourcesPath and samplesPath paths will be evaluated relative to that path. "
                    + "Use / as the path separator. ");
            return;
        }
        if (!qvtdBenchmarkHome.endsWith("/")) {
            qvtdBenchmarkHome += "/";
        }
        iterations = (int) options.valueOf(Options.NUMBER_ITERATIONS.option);
        warmupTime = (int) options.valueOf(Options.WARMUP_TIME.option);
        warmpup = warmupTime > 0 ? true : false;
        verbose = options.has(Options.VERBOSE.option);
        Benchmark type = null;
        if (options.has(Options.SINGLE.option)) {
            type = Benchmark.FIRST;
        } else if (options.has(Options.BEST.option)) {
            type = Benchmark.BEST;
        } else if (options.has(Options.ALL.option)) {
            type = Benchmark.ALL;
        }
        QVTdBenchmark_Old bm = new QVTdBenchmark_Old(iterations, warmupTime, warmpup, type, verbose);
        bm.setUp();
        if (options.has(Options.TEST_NAME.option)) {
            test = (String) options.valueOf(Options.TEST_NAME.option);
            String[] testArgs = bm.getTestArgs(test);
            // We need to change the two paths to absolute
            String sourcesPath = qvtdBenchmarkHome + testArgs[1];
            String samplesPath = qvtdBenchmarkHome + testArgs[3];
            bm.runTransformation(Configuration.Mode.ENFORCE,
                    testArgs[0],
                    sourcesPath,
                    testArgs[2],
                    samplesPath,
                    testArgs[4],
                    testArgs[5],
                    testArgs[6],
                    testArgs[7],
                    test);
            String results = qvtdBenchmarkHome + test + ".csv";
            bm.createLogByCost(results);
            String plans = qvtdBenchmarkHome + test + "Plans.dot";
            bm.storePlans(plans);

        } else {
            System.out.println("Custom test.");
            // Read the test parameters from the args
        }
        bm.tearDown();
        System.out.println("Done");

    }

}
