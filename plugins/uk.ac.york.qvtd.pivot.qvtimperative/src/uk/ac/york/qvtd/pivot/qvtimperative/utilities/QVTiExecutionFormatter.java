package uk.ac.york.qvtd.pivot.qvtimperative.utilities;

import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import org.eclipse.jdt.annotation.NonNull;

/**
 * The QVTiExecutionFormatter accepts LogRecord that include an array of object arguments.
 * The formatter accepts two messages, a pre-visit and a post-visit. The pre-visit happens
 * before visiting a visitable and includes info about the visitable. The pre-visit
 * causes an identation. The post-visit is the result of visiting the visitable. This can be
 * the computed value or an exception message.
 *
 *  Pre-vist
 * 	1 : The name of the visitable being visited
 *  2 : The value of said visitable
 *
 *  Post-visit
 *  1 : The value/name of the visit result, or the exception
 */
public class QVTiExecutionFormatter extends Formatter {

    private static final String format = "%1 %2 %3";
    private int indentationLevel = 0;
    private StringBuilder sb = new StringBuilder();

    @Override
    public String format(LogRecord record) {
        Object[] parameters = record.getParameters();
        if (parameters == null) {
            @NonNull
            Class<? extends @NonNull LogRecord> clazz = record.getClass();
            String msg = record.getMessage();
            return String.format(format, clazz, msg, "Undefined Visitable information");
        }
        if (parameters.length == 0) {
            return String.format(format, record.getClass(), record.getMessage(), "Undefined Visitable information");
        }
        sb.setLength(0);
        indent(indentationLevel);
        if (parameters[0] == LogType.PRE) {
            sb.append(parameters[1]);
            sb.append(": ");
            sb.append(parameters[2]);
            pushIndentation();
        }
        else if (parameters[0] == LogType.POST) {
            popIndentation();
            sb.append("=> ");
            sb.append(parameters[1]);
        }
        else if (parameters[0] == LogType.EXCEPTION) {
            assert parameters[1] instanceof Throwable;
            Throwable e = (Throwable) parameters[1];
            popIndentation();
            sb.append("!! ");
            sb.append(String.valueOf(e.getClass().getSimpleName()));
            sb.append(": ");
            sb.append(String.valueOf(e.getMessage()));
        }
        sb.append("\n");
        return sb.toString();
    }


    /* (non-Javadoc)
     * @see java.util.logging.Formatter#getHead(java.util.logging.Handler)
     */
    @Override
    public String getHead(Handler h) {
        sb.setLength(0);
        sb.append("*********************************\n");
        sb.append("*   QVTi Interpreted Exection   *\n");
        sb.append("*********************************\n");
        sb.append("\n");
        return sb.toString();
    }


    /* (non-Javadoc)
     * @see java.util.logging.Formatter#getTail(java.util.logging.Handler)
     */
    @Override
    public String getTail(Handler h) {
        sb.setLength(0);
        sb.append("\n");
        sb.append("\n");
        sb.append("*********************************\n");
        sb.append("*        Execution END          *\n");
        sb.append("*********************************\n");
        return sb.toString();
    }


    public void popIndentation() {
        if (indentationLevel > 0) {
            --indentationLevel;
        }
    }

    public void pushIndentation() {
        indentationLevel++;
    }

    protected void indent(int depth) {
        for (int k = 0; k < depth; k++) {
            sb.append(k%5 == 0 ? ". " : "  ");
        }
    }

    public enum LogType {
        PRE,
        POST,
        EXCEPTION;
    }

}
