/*******************************************************************************
 * Copyright (c) 2013, 2015 The University of York, Willink Transformations and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - initial API and implementation
 ******************************************************************************/
package uk.ac.york.qvtd.pivot.qvtimperative.evaluation;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Pair<F, S> {

    public final F first;
    public final S second;

    public Pair(F source, S target) {
        this.first = source;
        this.second = target;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (!(other instanceof Pair)){
            return false;
        }
        Pair<?, ?> other_ = (Pair<?, ?>) other;
        return Objects.equals(this.first, other_.first) &&
                Objects.equals(this.second, other_.second);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Objects.hashCode(first);
        result = prime * result + Objects.hashCode(second);
        return result;
    }

    @Override
    public String toString() {
        return "(" + first + "," + second + ")";
    }

    public static <F, S> Pair<F, S> of(F first, S second) {
        return new Pair<F, S>(first, second);
    }

    /**
     * Pair combinations.
     *
     * @param originalSet the original set
     * @return the list
     */
    public static <T> List<Pair<T, T>> pairCombinations(Set<T> originalSet) {
        List<T> list = new ArrayList<T>(originalSet);
        int n = list.size();
        List<Pair<T, T>> pairsList = new ArrayList<Pair<T, T>>();
        T iv;
        T jv;
        for (int i = 0; i < n; i++) {
            iv = list.get(i);
            for (int j = 0; j < n; j++) {
                if (i == j) continue;
                jv = list.get(j);
                pairsList.add(of(iv,jv));
            }
        }
        return pairsList;
    }
}