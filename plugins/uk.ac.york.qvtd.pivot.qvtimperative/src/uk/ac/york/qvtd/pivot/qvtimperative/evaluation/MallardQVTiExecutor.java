/*******************************************************************************
 * Copyright (c) 2013, 2015 The University of York, Willink Transformations and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - initial API and implementation
 ******************************************************************************/
package uk.ac.york.qvtd.pivot.qvtimperative.evaluation;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.EqualityHelper;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.ocl.pivot.CallExp;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.NamedElement;
import org.eclipse.ocl.pivot.NavigationCallExp;
import org.eclipse.ocl.pivot.OCLExpression;
import org.eclipse.ocl.pivot.OppositePropertyCallExp;
import org.eclipse.ocl.pivot.Package;
import org.eclipse.ocl.pivot.PivotFactory;
import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.ocl.pivot.VariableExp;
import org.eclipse.ocl.pivot.evaluation.EvaluationEnvironment;
import org.eclipse.ocl.pivot.evaluation.EvaluationVisitor;
import org.eclipse.ocl.pivot.ids.CollectionTypeId;
import org.eclipse.ocl.pivot.internal.complete.StandardLibraryInternal;
import org.eclipse.ocl.pivot.internal.evaluation.AbstractExecutor;
import org.eclipse.ocl.pivot.internal.messages.PivotMessagesInternal;
import org.eclipse.ocl.pivot.utilities.NameUtil;
import org.eclipse.ocl.pivot.utilities.ValueUtil;
import org.eclipse.ocl.pivot.values.CollectionValue;
import org.eclipse.ocl.pivot.values.InvalidValueException;
import org.eclipse.qvtd.compiler.internal.utilities.ClassRelationships;
import org.eclipse.qvtd.pivot.qvtbase.Domain;
import org.eclipse.qvtd.pivot.qvtbase.Predicate;
import org.eclipse.qvtd.pivot.qvtbase.Rule;
import org.eclipse.qvtd.pivot.qvtbase.Transformation;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.eclipse.qvtd.pivot.qvtbase.utilities.QVTbaseUtil;
import org.eclipse.qvtd.pivot.qvtcorebase.Area;
import org.eclipse.qvtd.pivot.qvtcorebase.Assignment;
import org.eclipse.qvtd.pivot.qvtcorebase.BottomPattern;
import org.eclipse.qvtd.pivot.qvtcorebase.CoreDomain;
import org.eclipse.qvtd.pivot.qvtcorebase.EnforcementOperation;
import org.eclipse.qvtd.pivot.qvtcorebase.GuardPattern;
import org.eclipse.qvtd.pivot.qvtcorebase.NavigationAssignment;
import org.eclipse.qvtd.pivot.qvtcorebase.PropertyAssignment;
import org.eclipse.qvtd.pivot.qvtcorebase.RealizedVariable;
import org.eclipse.qvtd.pivot.qvtcorebase.VariableAssignment;
import org.eclipse.qvtd.pivot.qvtcorebase.utilities.QVTcoreBaseUtil;
import org.eclipse.qvtd.pivot.qvtimperative.ConnectionVariable;
import org.eclipse.qvtd.pivot.qvtimperative.Mapping;
import org.eclipse.qvtd.pivot.qvtimperative.MappingCall;
import org.eclipse.qvtd.pivot.qvtimperative.MappingStatement;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.IQVTiEvaluationVisitor;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiEnvironmentFactory;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiEvaluationEnvironment;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiEvaluationVisitor;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiExecutor;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiModelManager;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiModelManager.QVTiTypedModelInstance;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiNestedEvaluationEnvironment;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiRootEvaluationEnvironment;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiTransformationAnalysis;
import org.eclipse.qvtd.runtime.evaluation.AbstractTransformer;
import org.eclipse.qvtd.runtime.evaluation.InvocationFailedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.Configuration;
import uk.ac.york.qvtd.pivot.qvtimperative.utilities.QVTimperativeUtil;


/**
 * The Class MallardQVTiExecutor.
 */
public class MallardQVTiExecutor extends AbstractExecutor implements QVTiExecutor {

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(QVTiLoggingEvaluationVisitor.class);

    /** The transformation. */
    protected final @NonNull Transformation transformation;

    /** The transformation analysis. */
    private @Nullable QVTiTransformationAnalysis transformationAnalysis = null;

    /** The model manager. */
    private @Nullable QVTiModelManager modelManager = null;

    /** The config. */
    protected final @NonNull Configuration config;

    /**  The cached option. */
    //private boolean disableTypeCache = false;

    private Map<Mapping, Set<Set<Pair<Variable, Object>>>> mappingCallHistory = new HashMap<>();

    // TODO Move me to the top
    // TODO Perhaps we need class and typed Model :O
    Map<EClass, Set<EStructuralFeature>> touchedFeatures = new HashMap<EClass, Set<EStructuralFeature>>();

    private ClassRelationships clasRels;

    /** Limit the time a transformation can take */
    private static ExecutorService executor;

    /** Execution will time-out after 20 minutes */
    private static final int EXECUTION_TIME_OUT = 20;		// TimeUnit.MINUTES

    /**
     * Instantiates a new mallard QVTi executor.
     *
     * @param environmentFactory the environment factory
     * @param transformation the transformation to execute
     * @param configuration the execution configuration
     */
    public MallardQVTiExecutor(@NonNull QVTiEnvironmentFactory environmentFactory,
            @NonNull Transformation transformation,
            @NonNull Configuration configuration) {
        super(environmentFactory);
        this.transformation = transformation;
        this.config = configuration;
        this.clasRels = new ClassRelationships(environmentFactory);
        executor = Executors.newSingleThreadExecutor();
    }

    /**
     * Adds the model to the executor
     *
     * @param typedModel the typed model
     * @param resource the resource
     */
    public void addModel(@NonNull TypedModel typedModel, @NonNull Resource resource) {
        getModelManager().addModel(typedModel, resource);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiExecutor#createModel(java.lang.String, org.eclipse.emf.common.util.URI, java.lang.String)
     */
    @Override
    public void createModel(@NonNull String name, @NonNull URI modelURI, @Nullable String contentType) {
        TypedModel typedModel = NameUtil.getNameable(transformation.getModelParameter(), name);
        if (typedModel == null) {
            throw new IllegalStateException("Unknown TypedModel '" + name + "'");
        }
        Resource resource = environmentFactory.getResourceSet().createResource(modelURI, contentType);
        if (resource != null) {
            getModelManager().addModel(typedModel, resource);
        }
    }

    /**
     * Creates the model for the given typed model.
     *
     * @param typedModel the typed model
     * @param modelURI the model uri
     * @param contentType the content type
     */
    public void createModel(@NonNull TypedModel typedModel, @NonNull URI modelURI, @Nullable String contentType) {
        Resource resource = environmentFactory.getResourceSet().createResource(modelURI, contentType);
        if (resource != null) {
            getModelManager().addModel(typedModel, resource);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ocl.pivot.internal.evaluation.AbstractExecutor#dispose()
     */
    @Override
    public void dispose() {
        executor.shutdown();
        getEvaluationEnvironment().clear();
        if (modelManager != null) {
            unloadModelsFromConfiguration();
            modelManager.dispose();
        }
        super.dispose();
    }

    /**
     * Do bottom pattern commits.
     *
     * @param mapping the mapping
     * @param undecoratedVisitor the undecorated visitor
     */
    public void doBottomPatternCommits(Mapping mapping, @NonNull EvaluationVisitor undecoratedVisitor) {
        BottomPattern middleBottomPattern = mapping.getBottomPattern();
        for (Assignment assignment : middleBottomPattern.getAssignment()) {
            //if (assignment instanceof PropertyAssignment) {
                assignment.accept(undecoratedVisitor);
            //}
        }
    }

    /**
     * Do enforcement operation commits.
     *
     * @param mapping the mapping
     * @param undecoratedVisitor the undecorated visitor
     */
    public void doEnforcementOpCommits(Mapping mapping, @NonNull EvaluationVisitor undecoratedVisitor) {
        for (Domain domain : mapping.getDomain()) {
            if (domain.isIsEnforceable()) {
                CoreDomain enforceableDomain = (CoreDomain)domain;
                BottomPattern enforceableBottomPattern = enforceableDomain.getBottomPattern();
                for (EnforcementOperation enforceOp : enforceableBottomPattern.getEnforcementOperation()) {
                    enforceOp.accept(undecoratedVisitor);
                }
            }
        }
    }

    /**
     * Do evaluate middle bottom pattern.
     *
     * @param mapping the mapping
     * @param undecoratedVisitor the undecorated visitor
     */
    public void doEvaluateMiddleBottomPattern(Mapping mapping,
            EvaluationVisitor undecoratedVisitor) {
        BottomPattern middleBottomPattern = mapping.getBottomPattern();
        assert middleBottomPattern.getEnforcementOperation().isEmpty();
        assert middleBottomPattern.getPredicate().isEmpty();
        assert middleBottomPattern.getRealizedVariable().isEmpty();
        //
        // variable declarations/initializations
        //
//		for (Variable rVar : middleBottomPattern.getVariable()) {
//			OCLExpression ownedInit = rVar.getOwnedInit();
//			if (ownedInit != null) {
//				Object initValue = ownedInit.accept(undecoratedVisitor);
////				assert initValue != null;
//				if (QVTimperativeUtil.isConnectionAccumulator(rVar)) {
//					CollectionValue.Accumulator accumulator = ValueUtil.createCollectionAccumulatorValue((CollectionTypeId) ownedInit.getTypeId());
//					if (initValue != null) {
//						for (Object value : (Iterable<?>)initValue) {
//							accumulator.add(value);
//						}
//					}
//					replace(rVar, accumulator);
//				}
//				else {
//					replace(rVar, initValue);
//				}
//			}
//		}
        //
        // variable assignments
        //
//		for (Assignment assignment : middleBottomPattern.getAssignment()) {
//			if (assignment instanceof PropertyAssignment) {
//				assignment.accept(undecoratedVisitor);
//			}
//		}
    }

    /**
     * Do local variable commits.
     *
     * @param mapping the mapping
     * @param undecoratedVisitor the undecorated visitor
     */
    public void doLocalVariableCommits(Mapping mapping, @NonNull EvaluationVisitor undecoratedVisitor) {
        // TODO Check if we need to visit the bottom pattern domains or not
        BottomPattern middleBottomPattern = mapping.getBottomPattern();
        for (Variable rVar : middleBottomPattern.getVariable()) {
            if (rVar instanceof ConnectionVariable) {
                CollectionValue.Accumulator accumulator = ValueUtil.createCollectionAccumulatorValue((CollectionTypeId) rVar.getTypeId());
                OCLExpression ownedInit = rVar.getOwnedInit();
                if (ownedInit != null) {
                    Object initValue = ownedInit.accept(undecoratedVisitor);
                    accumulator = ValueUtil.createCollectionAccumulatorValue((CollectionTypeId) ownedInit.getTypeId());
                    if (initValue != null) {
                        for (Object value : (Iterable<?>)initValue) {
                            accumulator.add(value);
                        }
                    }
                }
                else {
                    accumulator = ValueUtil.createCollectionAccumulatorValue((CollectionTypeId) rVar.getTypeId());
                }
                replace(rVar, accumulator);
            }
            else {
                OCLExpression ownedInit = rVar.getOwnedInit();
                if (ownedInit != null) {
                    Object initValue = ownedInit.accept(undecoratedVisitor);
                    replace(rVar, initValue);
                }
            }
        }
        //
        // variable assignments
        //
//        for (Assignment assignment : middleBottomPattern.getAssignment()) {
//            if (assignment instanceof VariableAssignment) {
//                assignment.accept(undecoratedVisitor);
//            }
//        }
    }

    /**
     * Do realized variable commits.
     *
     * @param mapping the mapping
     * @param undecoratedVisitor the undecorated visitor
     */
    public void doRealizedVariableCommits(Mapping mapping, @NonNull EvaluationVisitor undecoratedVisitor) {
        for (Domain domain : mapping.getDomain()) {
            if (domain.isIsEnforceable()) {
                CoreDomain enforceableDomain = (CoreDomain)domain;
                BottomPattern enforceableBottomPattern = enforceableDomain.getBottomPattern();
                for (RealizedVariable realizedVariable : enforceableBottomPattern.getRealizedVariable()) {
                    realizedVariable.accept(undecoratedVisitor);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiExecutor#execute()
     */
    @Override
    public Boolean execute() {
        initializeEvaluationEnvironment(transformation);
        getRootEvaluationEnvironment();
        StandardLibraryInternal standardLibrary = environmentFactory.getStandardLibrary();
        Variable ownedContext = QVTbaseUtil.getContextVariable(standardLibrary, transformation);
        QVTiModelManager modelManager = getModelManager();
        add(ownedContext, modelManager.getTransformationInstance(transformation));
        for (TypedModel typedModel : transformation.getModelParameter()) {
            if (typedModel != null) {
                ownedContext = QVTbaseUtil.getContextVariable(standardLibrary, typedModel);
                add(ownedContext, modelManager.getTypedModelInstance(typedModel));
            }
        }
        Future<Boolean> future = executor.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                // TODO Auto-generated method stub
                return executeInternal();
            }

        });
        Boolean result = false;
        try {
            result  =  future.get(EXECUTION_TIME_OUT, TimeUnit.MINUTES);
        }
        catch (InterruptedException ex) {
            logger.error("Execution thread interrupted.");
        }
        catch (ExecutionException ex) {
            logger.error("Execution exception.", ex);
        }
        catch (TimeoutException ex) {
            logger.error("Execution timedout.");
        }
        return result;
//        return executeInternal();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ocl.pivot.internal.evaluation.AbstractExecutor#getEnvironmentFactory()
     */
    @Override
    public @NonNull QVTiEnvironmentFactory getEnvironmentFactory() {
        return (QVTiEnvironmentFactory) super.getEnvironmentFactory();
    }

    /* (non-Javadoc)
     * @see org.eclipse.ocl.pivot.internal.evaluation.AbstractExecutor#getEvaluationEnvironment()
     */
    @Override
    public @NonNull QVTiEvaluationEnvironment getEvaluationEnvironment() {
        return (QVTiEvaluationEnvironment) super.getEvaluationEnvironment();
    }

    @Override
    public @Nullable Resource getModel(@NonNull String name) {
        TypedModel typedModel = NameUtil.getNameable(transformation.getModelParameter(), name);
        if (typedModel == null) {
            throw new IllegalStateException("Unknown TypedModel '" + name + "'");
        }
        return getModelManager().getModel(typedModel);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ocl.pivot.evaluation.Executor#getModelManager()
     */
    @Override
    public @NonNull QVTiModelManager getModelManager() {
        QVTiModelManager modelManager2 = modelManager;
        if (modelManager2 == null) {
//            if (disableTypeCache) {
//                modelManager = modelManager2 = new MallardModelManager(getTransformationAnalysis());
//            }
//            else {
                modelManager = modelManager2 = new QVTiModelManager(getTransformationAnalysis());
//            }
        }
        return modelManager2;
    }

    /**
     * Gets the output model.
     *
     * @return the output model
     */
    public Resource getOutputModel() {
        return getModelManager().getModel(config.getOutputDomain());
    }

    /**
     * Gets the transformation.
     *
     * @return the transformation
     */
    public @NonNull Transformation getTransformation() {
        return transformation;
    }

    /**
     * Gets the transformation analysis.
     *
     * @return the transformation analysis
     */
    public @NonNull QVTiTransformationAnalysis getTransformationAnalysis() {
        QVTiTransformationAnalysis transformationAnalysis2 = transformationAnalysis;
        if (transformationAnalysis2 == null) {
            transformationAnalysis = transformationAnalysis2 = createTransformationAnalysis();
            transformationAnalysis2.analyzeTransformation(transformation);
        }
        return transformationAnalysis2;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiExecutor#internalExecuteMapping(org.eclipse.qvtd.pivot.qvtimperative.Mapping, org.eclipse.ocl.pivot.evaluation.EvaluationVisitor)
     */
    @Override
    public @Nullable Object internalExecuteMapping(@NonNull Mapping mapping, @NonNull EvaluationVisitor undecoratedVisitor) {
        try {
            //
            // Validate the structure (assert syntax)
            //
            doSemanticCheck(mapping, undecoratedVisitor);
            //
            //	Check the predicates
            //
            if (!doPredicates(mapping, undecoratedVisitor)) {
                return false;
            }
        }
        catch (InvocationFailedException e) {
            throw e;
        }
        catch (Throwable e) {
            // Mapping failure are just mappings that never happened.
            AbstractTransformer.EXCEPTIONS.println("Execution failure in " + mapping.getName() + " : " + e);
            return false;
        }
        doInits(mapping, undecoratedVisitor);
        //
        //	Perform the instance model addition and property assignment only after all expressions have been evaluated
        //	possibly throwing a not-ready exception that bypasses premature commits.
        //
        doCommits(mapping, undecoratedVisitor);
        //
        //	Invoke any corollaries
        //
        MappingStatement mappingStatement = mapping.getMappingStatement();
        if (mappingStatement != null) {
                mappingStatement.accept(undecoratedVisitor);
        }
        return true;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiExecutor#internalExecuteMappingCall(org.eclipse.qvtd.pivot.qvtimperative.MappingCall, java.util.Map, org.eclipse.ocl.pivot.evaluation.EvaluationVisitor)
     */
    @Override
    public @Nullable Object internalExecuteMappingCall(@NonNull MappingCall mappingCall, @NonNull Map<Variable, Object> variable2value, @NonNull EvaluationVisitor undecoratedVisitor) {
        Mapping calledMapping = mappingCall.getReferredMapping();
        boolean result = false;
        if (calledMapping != null) {
            // Runtime loop/repeated invocation
            Set<Set<Pair<Variable, Object>>> params;
            Set<Pair<Variable, Object>> pairs = pairsFromParams(variable2value);
            if (mappingCallHistory.containsKey(calledMapping)) {
                params = mappingCallHistory.get(calledMapping);
                if (params.contains(pairs)) {
                    Object[]  logp = new Object[]{calledMapping.getName(), Integer.toHexString(pairs.hashCode())};
                    logger.debug("Already called {} with {} ", logp);
                    return "Already Transformed";
                }
            }
            else {
                params = new HashSet<Set<Pair<Variable,Object>>>();
                mappingCallHistory.put(calledMapping, params);
            }
            pushEvaluationEnvironment(calledMapping, mappingCall);
            try {
                for (Map.Entry<Variable,Object> entry : variable2value.entrySet()) {
                    @SuppressWarnings("null")@NonNull Variable variable = entry.getKey();
                    replace(variable, entry.getValue());
                }
                //System.out.println(calledMapping.getName() + ": " + Integer.toHexString(pairs.hashCode()));
                result = (boolean) calledMapping.accept(undecoratedVisitor);
                //System.out.println(calledMapping.getName() + ": " + Integer.toHexString(pairs.hashCode()));
                Object[]  logp = new Object[]{calledMapping.getName(), Integer.toHexString(pairs.hashCode()), result};
                logger.debug("Calling {} with {} was {}", logp);
                if (result) {
                    params.add(pairs);
                }
            }
            finally {
                popEvaluationEnvironment();
            }
        }
        return result;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ocl.pivot.internal.evaluation.AbstractExecutor#internalExecuteNavigationCallExp(org.eclipse.ocl.pivot.NavigationCallExp, org.eclipse.ocl.pivot.Property, java.lang.Object)
     */
    @Override
    public @Nullable Object internalExecuteNavigationCallExp(@NonNull NavigationCallExp navigationCallExp, @NonNull Property referredProperty, @Nullable Object sourceValue) {
        Object ecoreValue = null;
        if (referredProperty.isIsImplicit()) {
            QVTiModelManager modelManager = getModelManager();
            Integer cacheIndex = modelManager.getTransformationAnalysis().getCacheIndex((OppositePropertyCallExp) navigationCallExp);
            if (cacheIndex != null) {
                if (sourceValue != null) {
                    Object middleOpposite = modelManager.getUnnavigableOpposite(cacheIndex, sourceValue);
                    if (middleOpposite != null) {
                        ecoreValue = middleOpposite;
                    }
                }
                else {
                    throw new InvalidValueException(PivotMessagesInternal.FailedToEvaluate_ERROR_, referredProperty, sourceValue, navigationCallExp);
                }
            }
        }
        else {
            ecoreValue = super.internalExecuteNavigationCallExp(navigationCallExp, referredProperty, sourceValue);
        }
        return ecoreValue;
    }

//    @Override
//    public void internalExecutePropertyAssignment(@NonNull PropertyAssignment propertyAssignment, @NonNull Object slotObject, @Nullable Object ecoreValue, @Nullable Object childKey) {
//        Property targetProperty = propertyAssignment.getTargetProperty();
//        targetProperty.initValue(slotObject, ecoreValue);
//        QVTiModelManager modelManager = getModelManager();
//        Integer cacheIndex = modelManager.getTransformationAnalysis().getCacheIndex(propertyAssignment);
//        if (cacheIndex != null) {
//            modelManager.setUnnavigableOpposite(cacheIndex, slotObject, ecoreValue);
//        }
//    }

    @Override
    public void internalExecuteNavigationAssignment(@NonNull NavigationAssignment navigationAssignment,
            @NonNull Object slotObject, @Nullable Object ecoreValue, @Nullable Object childKey) {
        //Property targetProperty = propertyAssignment.getTargetProperty();
        Property targetProperty = QVTcoreBaseUtil.getTargetProperty(navigationAssignment);
        assert targetProperty != null;
        // BUG? If the Property is an implicit property the getESObject() is null, hence we need to use the oppossite
        try {
            if (targetProperty.getESObject() != null) {
                targetProperty.initValue(slotObject, ecoreValue);
            }
            else {
                targetProperty = targetProperty.getOpposite();
                EStructuralFeature eFeature = (EStructuralFeature)targetProperty.getESObject();
                if (targetProperty.isIsMany()) {
                    EList current = (EList) ((EObject)ecoreValue).eGet(eFeature);
                    current.add(slotObject);
                }
                else {
                    targetProperty.initValue(ecoreValue, slotObject);
                }
            }

        }
        catch (ClassCastException ex) {
            System.err.println("Assignment types dont match: " + navigationAssignment);
            throw ex;
        }
        catch (UnsupportedOperationException ex) {
            System.err.println("Unsupported operation: " + navigationAssignment);
            throw ex;
        }
        catch (IllegalArgumentException ex) {
            System.err.println("Read only: " + navigationAssignment);
            throw ex;
        }
        QVTiModelManager modelManager = getModelManager();
        Integer cacheIndex = modelManager.getTransformationAnalysis().getCacheIndex(navigationAssignment);
        if (cacheIndex != null) {
            modelManager.setUnnavigableOpposite(cacheIndex, slotObject, ecoreValue);
        }

    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiExecutor#internalExecuteRealizedVariable(org.eclipse.qvtd.pivot.qvtcorebase.RealizedVariable, org.eclipse.ocl.pivot.evaluation.EvaluationVisitor)
     */
    @Override
    public @Nullable Object internalExecuteRealizedVariable(@NonNull RealizedVariable realizedVariable, @NonNull EvaluationVisitor undecoratedVisitor) {
        // Realized variables are in the mapping's target bottom pattern
        // and create elements in the target model. The realized variables
        // are being visited for each binding of variable in the mapping.
        Type type = realizedVariable.getType();
        if (!(type instanceof org.eclipse.ocl.pivot.Class)) {
            return null;
        }
        Area area = ((BottomPattern)realizedVariable.eContainer()).getArea();
        TypedModel typedModel = QVTcoreBaseUtil.getTypedModel(area);
        assert typedModel != null;
        Object element = null;
        try {
            element = ((org.eclipse.ocl.pivot.Class)type).createInstance();
        }
        catch (java.lang.IllegalArgumentException ex) {
            throw new InvalidValueException(ex, "Type is abstract.", "Craete new instance", type, realizedVariable);
        }
        catch (UnsupportedOperationException ex) {
            throw new InvalidValueException(ex, PivotMessagesInternal.UnrecognizedType_ERROR_, "Craete new instance", type, realizedVariable);
        }
        // Add the realize variable binding to the environment
        replace(realizedVariable, element);
        getModelManager().addModelElement(typedModel, element);
        // Update the cache
        @NonNull
        QVTiTypedModelInstance modelInstance = (@NonNull QVTiTypedModelInstance) getModelManager().getTypedModelInstance(typedModel);
        modelInstance.getObjectsOfType((@NonNull Class) type).add(element);
        modelInstance.getObjectsOfKind((@NonNull Class) type).add(element);
        for (Class sc : clasRels.getAllSuperClasses((@NonNull Class) type)) {
            modelInstance.getObjectsOfKind(sc).add(element);
        }
        return element;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiExecutor#internalExecuteTransformation(org.eclipse.qvtd.pivot.qvtbase.Transformation, org.eclipse.ocl.pivot.evaluation.EvaluationVisitor)
     */
    @Override
    public @Nullable Object internalExecuteTransformation(@NonNull Transformation transformation, @NonNull EvaluationVisitor undecoratedVisitor) {
        Rule rule = NameUtil.getNameable(transformation.getRule(), QVTimperativeUtil.ROOT_MAPPING_NAME);
        if (rule == null) {
            throw new IllegalStateException("Transformation " + transformation.getName() + " has no root mapping");
        }
        @SuppressWarnings("null")@NonNull CallExp callExp = PivotFactory.eINSTANCE.createOperationCallExp();		// FIXME TransformationCallExp
        pushEvaluationEnvironment(rule, callExp);
        try {
            rule.accept(undecoratedVisitor);
        }
        finally {
            popEvaluationEnvironment();
        }
        return true;
    }

    /**
     * Checks if is disable type cache.
     *
     * @return the disableTypeCache
     */
//    public boolean isDisableTypeCache() {
//        return disableTypeCache;
//    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiExecutor#loadModel(java.lang.String, org.eclipse.emf.common.util.URI, java.lang.String)
     */
    @Override
    public void loadModel(@NonNull String name, @NonNull URI modelURI, @Nullable String contentType) {
        TypedModel typedModel = NameUtil.getNameable(transformation.getModelParameter(), name);
        if (typedModel == null) {
            throw new IllegalStateException("Unknown TypedModel '" + name + "'");
        }
        Resource resource;
        ResourceSet resourceSet = environmentFactory.getResourceSet();
        if (contentType == null) {
            resource = resourceSet.getResource(modelURI, true);
        }
        else {
            resource = resourceSet.createResource(modelURI, contentType);
            try {
                resource.load(null);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        if (resource != null) {
            getModelManager().addModel(typedModel, resource);
        }
    }


    /**
     * Loads the modelURI and binds it to the named TypedModel.
     *
     * @param typedModel the typed model
     * @param modelURI the model uri
     */
    public void loadModel(@NonNull TypedModel typedModel, @NonNull URI modelURI) {
        Resource resource = environmentFactory.getResourceSet().getResource(modelURI, true);
        if (resource != null) {
            getModelManager().addModel(typedModel, resource);
        }
    }


    /**
     * Load models from configuration.
     */
    public void loadModelsFromConfiguration() {

        for (Entry<TypedModel, List<URI>> entry : config.getInputModels().entrySet()) {
            registerTypedModelPacakges(entry.getKey());
            for (URI m : entry.getValue()) {
                loadModel(entry.getKey(), m);
            }
        }
        registerTypedModelPacakges(config.getMiddleDomain());
        createModel(config.getMiddleDomain(), config.getMiddleModel(), null);
        createModel(config.getOutputDomain(), config.getOutputModel(), null);
    }

    /**
     * Transform the map into Pairs
     * @param variable2value
     * @return
     */
    public Set<Pair<Variable, Object>> pairsFromParams(Map<Variable, Object> variable2value) {
        Set<Pair<Variable, Object>> pairs = new HashSet<Pair<Variable, Object>>(variable2value.size());
        Iterator<Entry<Variable, Object>> it = variable2value.entrySet().iterator();
        while (it.hasNext()) {
            Entry<Variable, Object> entry = it.next();
            Pair<Variable, Object> p = Pair.of(entry.getKey(), entry.getValue());
            pairs.add(p);
        }
        return pairs;
    }

    /**
     * Save model.
     *
     * @param name the name
     * @param modelURI the model uri
     * @param contentType the content type
     * @param savingOptions the saving options
     * @return the resource
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public Resource saveModel(@NonNull String name, @NonNull URI modelURI, String contentType, @Nullable Map<?, ?> savingOptions) throws IOException {
        TypedModel typedModel = NameUtil.getNameable(transformation.getModelParameter(), name);
        if (typedModel == null) {
            throw new IllegalStateException("Unknown TypedModel '" + name + "'");
        }
        Resource resource = getModelManager().getModel(typedModel);
        if (resource == null) {
            resource = environmentFactory.getResourceSet().createResource(modelURI, contentType);
        }
        if (resource != null) {
            resource.save(savingOptions);
        }
        return resource;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiExecutor#saveModels()
     */
    @Override
    public void saveModels() {
        getModelManager().saveModels();
    }

    /**
     * Save models.
     *
     * @param savingOptions the saving options
     */
    public void saveModels(@Nullable Map<?, ?> savingOptions) {
        getModelManager().saveModels(savingOptions);
    }

    /**
     * Save models.
     *
     * @param traceURI the trace uri
     */
    public void saveModels(@NonNull URI traceURI) {
        this.saveModels(traceURI, null);
    }

    /**
     * Save models.
     *
     * @param traceURI the trace uri
     * @param savingOptions the saving options
     */
    public void saveModels(@NonNull URI traceURI, @Nullable Map<?, ?> savingOptions) {
        this.saveModels(savingOptions);
        getModelManager().saveMiddleModel(traceURI, savingOptions);
    }

    /**
     * Save transformation.
     *
     * @param options the options
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public void saveTransformation(Map<?,?> options) throws IOException {
        XMLResource resource = (XMLResource) transformation.eResource();
//    	new AS2ID().assignIds(resource.getResourceSet());
        resource.save(options);
    }

    /**
     * Sets the disable type cache.
     *
     * @param disableTypeCache the disableTypeCache to set
     */
//    public void setDisableTypeCache(boolean disableTypeCache) {
//        this.disableTypeCache = disableTypeCache;
//    }

    /**
     * Unload models from configuration.
     */
    public void unloadModelsFromConfiguration() {

        if (modelManager != null) {
            for (@NonNull Resource r : modelManager.getAllModelResources()) {
                environmentFactory.getResourceSet().getResources().remove(r);
            }
        }
    }

    /**
     * Creates the transformation analysis.
     *
     * @return the QV ti transformation analysis
     */
    private @NonNull QVTiTransformationAnalysis createTransformationAnalysis() {
        return new MallardTransforamtionAnalysis(environmentFactory);
    }

    /**
     * Only execute the assignments that modify realized variables and then test if a matching element
     * exists in the output model. If it does, the existing Object is used to replace the newly created
     * object for the realized variable
     * @param mapping
     * @param undecoratedVisitor
     * @return true if all variables where matched
     */
    // TODO not necessary to return boolean as we will need to do the assignments again either way. The information
    // could be used for a log instead.
    @SuppressWarnings("null")
    private boolean doCheck(@NonNull Mapping mapping, @NonNull EvaluationVisitor undecoratedVisitor) {
        BottomPattern middleBottomPattern = mapping.getBottomPattern();

        for (Assignment assignment : middleBottomPattern.getAssignment()) {
            if (modifiesRealizedVariable(assignment)) {
                assignment.accept(undecoratedVisitor);
                // Save the property modified by the assignment, so we can use it in the compare!
                if (assignment instanceof PropertyAssignment) {
                    PropertyAssignment pa = (PropertyAssignment) assignment;
                    Property prop = pa.getTargetProperty();
                    EObject ef = prop.getESObject();
                    assert ef.eContainer() instanceof EClass;
                    EClass efClass = (EClass) ef.eContainer();
                    Set<EStructuralFeature> existing = touchedFeatures.get(efClass);
                    if (existing == null) {
                        existing = new HashSet<EStructuralFeature>();
                        touchedFeatures.put(efClass, existing);
                    }
                    existing.add((EStructuralFeature) ef);
                }
            }
        }
        if (mapping.getDomain().isEmpty()) {
            return true;
        }
        MallardEqualityHelper helper = new MallardEqualityHelper(touchedFeatures);
        boolean allMatched = true;
        boolean oneCheckable = false;
        // Find a matching element
        for (Domain domain : mapping.getDomain()) {
            boolean domainMatched = true;
            CoreDomain enforceableDomain = (CoreDomain)domain;
            TypedModel tm = enforceableDomain.getTypedModel();
            BottomPattern enforceableBottomPattern = enforceableDomain.getBottomPattern();
            for (RealizedVariable realizedVariable : enforceableBottomPattern.getRealizedVariable()) {
                boolean match = false;
                EObject rvEObject = (EObject) getEvaluationEnvironment().getValueOf(realizedVariable);
                for (@NonNull Object existing : getModelManager().getElementsByType(tm, realizedVariable.getType())) {
                    EObject existingEObject = (EObject) existing;
                    if (!rvEObject.equals(existingEObject)) {
                        // TODO WE need an EqualityHelper that only compares a subset of properties.
                        // The subset of properties can be obtained from the assignments. Default assignments
                        // should be not included as per spec "not play role during checking"
                        // Perhaps this can be done with the property analysis.
                        match = helper.equals(rvEObject, existingEObject);
                        if (match) {
                            // Replace the match in the environment, remove the new object
                            Object old = getEvaluationEnvironment().remove(realizedVariable);
                            // TODO implement this. Further, the existing elements must be loaded to the
                            // environment at the start of the execution
                            //getEvaluationEnvironment().removedMatchedModelElement(tm, existingEObject);
                            assert rvEObject.equals(old);
                            getEvaluationEnvironment().add(realizedVariable, existingEObject);
                            // Delete the new element
                            EcoreUtil.delete(rvEObject);
                            break;
                        }
                    }
                }
                if (!match) {
                    domainMatched &= match;
                    // Only add variables that are not matched!
                    getModelManager().addModelElement(tm, rvEObject);
                }
            }
            if (domain.isIsCheckable()) {
                oneCheckable = true;
            }
            if (domain.isIsEnforceable()) {
                allMatched &= domainMatched;
            }
        }
        if (!oneCheckable) {
            // Section 9.9 Existing elements not matched should be kept.
        }
        return !allMatched;		// If not all where matched, enforce
    }

    protected void doInits(@NonNull Mapping mapping, @NonNull EvaluationVisitor undecoratedVisitor) {
        for (Domain domain : mapping.getDomain()) {
            CoreDomain coreDomain = (CoreDomain)domain;
            BottomPattern bottomPattern = coreDomain.getBottomPattern();
            for (Variable rVar : bottomPattern.getVariable()) {
                OCLExpression ownedInit = rVar.getOwnedInit();
                if (ownedInit != null) {
                    Object initValue = ownedInit.accept(undecoratedVisitor);
                    replace(rVar, initValue);
                }
            }

        }
    }

    /**
     * We can only detect this if the slot expression ultimately reduces to a VariableExp.
     *
     * TODO What other type of OCL expressions we can analyse? At least if an slot's effective type is the same of
     * any of the realized variables we can issue a warning
     * @param assignment
     * @return
     */
    private boolean modifiesRealizedVariable(Assignment assignment) {
        if (assignment instanceof PropertyAssignment) {
            PropertyAssignment ass = (PropertyAssignment) assignment;
            OCLExpression slot = ass.getSlotExpression();
            // TODO For the time we only check var.prop slots
            if (slot instanceof VariableExp) {
                VariableExp vExp = (VariableExp) slot;
                return vExp.getReferredVariable() instanceof RealizedVariable;
            }
        } else {
            VariableAssignment ass = (VariableAssignment) assignment;
            return ass.getTargetVariable() instanceof RealizedVariable;
        }
        return false;
    }

    /**
     * Register typed model pacakges.
     *
     * @param tm the tm
     */
    private void registerTypedModelPacakges(TypedModel tm) {
        ListIterator<Package> it = tm.getUsedPackage().listIterator();
        //for (Package p : tm.getUsedPackage()) {
        while (it.hasNext()) {
            Package p = it.next();
            Object p2 = environmentFactory.getResourceSet().getPackageRegistry().get(p.getURI());		//EPackageDescriptor?
            if (p2 == null) {
                p2 = p.getEPackage();		// p is a Pivot Package
                environmentFactory.getResourceSet().getPackageRegistry().put(p.getURI(), p2);
            } else {
                if (!p2.equals(p.getEPackage())) {
                    System.out.println("Metamodel Madness!");
                    environmentFactory.getResourceSet().getPackageRegistry().put(p.getURI(), p.getEPackage());
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ocl.pivot.internal.evaluation.AbstractExecutor#createEvaluationVisitor()
     */
    @Override
    protected EvaluationVisitor.@NonNull EvaluationVisitorExtension createEvaluationVisitor() {
        IQVTiEvaluationVisitor visitor = new QVTiEvaluationVisitor(this);
        if (environmentFactory.isEvaluationTracingEnabled()) {
            // decorate the evaluation visitor with tracing support
            visitor = new QVTiLoggingEvaluationVisitor(visitor);
        }
        return visitor;
    }

    /* (non-Javadoc)
     * @see org.eclipse.ocl.pivot.internal.evaluation.AbstractExecutor#createNestedEvaluationEnvironment(org.eclipse.ocl.pivot.evaluation.EvaluationEnvironment.EvaluationEnvironmentExtension, org.eclipse.ocl.pivot.NamedElement, org.eclipse.ocl.pivot.OCLExpression)
     */
    @Override
    protected EvaluationEnvironment.@NonNull EvaluationEnvironmentExtension createNestedEvaluationEnvironment(EvaluationEnvironment.@NonNull EvaluationEnvironmentExtension evaluationEnvironment, @NonNull NamedElement executableObject, @Nullable OCLExpression callingObject) {
        if (evaluationEnvironment instanceof QVTiEvaluationEnvironment) {
            return new QVTiNestedEvaluationEnvironment((QVTiEvaluationEnvironment) evaluationEnvironment, executableObject, callingObject);
        }
        else{
            return super.createNestedEvaluationEnvironment(evaluationEnvironment, executableObject, callingObject);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.ocl.pivot.internal.evaluation.AbstractExecutor#createRootEvaluationEnvironment(org.eclipse.ocl.pivot.NamedElement)
     */
    @Override
    protected EvaluationEnvironment.@NonNull EvaluationEnvironmentExtension createRootEvaluationEnvironment(@NonNull NamedElement executableObject) {
        if (executableObject instanceof Transformation) {
            return new QVTiRootEvaluationEnvironment(this, (Transformation) executableObject);
        }
        else {
            return super.createRootEvaluationEnvironment(executableObject);
        }
    }

    /**
     * Do commits.
     *
     * @param mapping the mapping
     * @param undecoratedVisitor the undecorated visitor
     */
    protected void doCommits(@NonNull Mapping mapping, @NonNull EvaluationVisitor undecoratedVisitor) {

        doRealizedVariableCommits(mapping, undecoratedVisitor);
        //doCheck(mapping, undecoratedVisitor);
        doBottomPatternCommits(mapping, undecoratedVisitor);
        doEnforcementOpCommits(mapping, undecoratedVisitor);
        doLocalVariableCommits(mapping, undecoratedVisitor);	// Middle Bottom pattern variables ise din bindings

    }


    /**
     * Do predicates.
     *
     * @param mapping the mapping
     * @param undecoratedVisitor the undecorated visitor
     * @return true, if successful
     */
    protected boolean doPredicates(@NonNull Mapping mapping, @NonNull EvaluationVisitor undecoratedVisitor) {
        GuardPattern middleGuardPattern = mapping.getGuardPattern();
//		assert middleGuardPattern.getVariable().isEmpty();		middle guards are connection variables
        for (Predicate predicate : middleGuardPattern.getPredicate()) {
            // If the predicate is not true, the binding is not valid
            Object result = predicate.accept(undecoratedVisitor);
            if (result != Boolean.TRUE) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check the semantic constraints of QVTi.
     *
     * @param mapping the mapping
     * @param undecoratedVisitor the undecorated visitor
     */
    protected void doSemanticCheck(@NonNull Mapping mapping, @NonNull EvaluationVisitor undecoratedVisitor) {
        for (Domain domain : mapping.getDomain()) {
            if (!domain.isIsEnforceable()) {
                CoreDomain checkableDomain = (CoreDomain)domain;
                GuardPattern checkableGuardPattern = checkableDomain.getGuardPattern();
                assert checkableGuardPattern.getPredicate().isEmpty();
                BottomPattern checkableBottomPattern = checkableDomain.getBottomPattern();
                assert checkableBottomPattern.getAssignment().isEmpty();
                assert checkableBottomPattern.getEnforcementOperation().isEmpty();
                assert checkableBottomPattern.getPredicate().isEmpty();
                assert checkableBottomPattern.getRealizedVariable().isEmpty();
                assert checkableBottomPattern.getVariable().isEmpty();
            }
            else {
                CoreDomain enforceableDomain = (CoreDomain)domain;
                GuardPattern enforceableGuardPattern = enforceableDomain.getGuardPattern();
                assert enforceableGuardPattern.getPredicate().isEmpty();
                BottomPattern enforceableBottomPattern = enforceableDomain.getBottomPattern();
                assert enforceableBottomPattern.getAssignment().isEmpty();
                assert enforceableBottomPattern.getPredicate().isEmpty();
//                for (@NonNull Variable rVar : ClassUtil.nullFree(enforceableBottomPattern.getVariable())) {
//                    OCLExpression ownedInit = rVar.getOwnedInit();
//                    if (ownedInit != null) {
//                        Object initValue = ownedInit.accept(undecoratedVisitor);
//                        replace(rVar, initValue);
//                    }
//                }
//                for (@NonNull RealizedVariable rVar : ClassUtil.nullFree(enforceableBottomPattern.getRealizedVariable())) {
//                    OCLExpression ownedInit = rVar.getOwnedInit();
//                    if (ownedInit != null) {
//                        Object initValue = ownedInit.accept(undecoratedVisitor);
//                        replace(rVar, initValue);
//                    }
//                }
            }
        }
        BottomPattern middleBottomPattern = mapping.getBottomPattern();
        assert middleBottomPattern.getEnforcementOperation().isEmpty();
        assert middleBottomPattern.getPredicate().isEmpty();
        assert middleBottomPattern.getRealizedVariable().isEmpty();
    }

    /**
     * Execute internal.
     *
     * @return the boolean
     */
    protected Boolean executeInternal() {
        return (Boolean) getEvaluationVisitor().visit(transformation);
    }

    /**
     * An Equality Helper that only compares a sub set of features.
     * @author hhoyos
     *
     */
    private class MallardEqualityHelper extends EqualityHelper {

        /**
         *
         */
        private static final long serialVersionUID = -1253296106971244477L;

        private final Map<EClass, Set<EStructuralFeature>> featuresOfInterest;

        protected MallardEqualityHelper(Map<EClass, Set<EStructuralFeature>> touchedFeatures) {
            super();
            this.featuresOfInterest = touchedFeatures;
        }

        @Override
        protected boolean haveEqualFeature(EObject eObject1, EObject eObject2, EStructuralFeature feature) {
            // Only compare features of interest, else they are equal
            boolean comparable = false;
            if (featuresOfInterest.containsKey(eObject1.eClass())) {
                if (featuresOfInterest.get(eObject1.eClass()).contains(feature)) {
                    comparable = true;
                }
            } else {
                comparable = true;
            }
            if (comparable) {
                //If the set states are the same, and the values of the feature are the structurally equal, they are equal.
                final boolean isSet1 = eObject1.eIsSet(feature);
                final boolean isSet2 = eObject2.eIsSet(feature);
                if (isSet1 && isSet2) {
                    return feature instanceof EReference ?
                            haveEqualReference(eObject1, eObject2, (EReference)feature) :
                                haveEqualAttribute(eObject1, eObject2, (EAttribute)feature);
                }
                else {
                    return isSet1 == isSet2;
                }
            }
            return true;

        }
    }



}
