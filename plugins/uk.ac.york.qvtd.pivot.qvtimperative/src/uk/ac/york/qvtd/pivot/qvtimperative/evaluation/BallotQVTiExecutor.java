package uk.ac.york.qvtd.pivot.qvtimperative.evaluation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.util.EContentAdapter;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.ocl.pivot.CallExp;
import org.eclipse.ocl.pivot.PivotFactory;
import org.eclipse.ocl.pivot.evaluation.EvaluationVisitor;
import org.eclipse.ocl.pivot.utilities.NameUtil;
import org.eclipse.ocl.pivot.values.InvalidValueException;
import org.eclipse.qvtd.pivot.qvtbase.Rule;
import org.eclipse.qvtd.pivot.qvtbase.Transformation;
import org.eclipse.qvtd.pivot.qvtcorebase.NavigationAssignment;
import org.eclipse.qvtd.pivot.qvtcorebase.RealizedVariable;
import org.eclipse.qvtd.pivot.qvtimperative.Mapping;
import org.eclipse.qvtd.pivot.qvtimperative.MappingSequence;
import org.eclipse.qvtd.pivot.qvtimperative.MappingStatement;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiEnvironmentFactory;

import uk.ac.york.qvtd.compiler.Configuration;
import uk.ac.york.qvtd.pivot.qvtimperative.utilities.QVTimperativeUtil;

/**
 * The Chaotic (Naive) executor attempts to execute rules randomly. It stops if after an iteration of all the rules no
 * more change is observed in the output model.
 * @author Horacio Hoyos
 *
 */
public class BallotQVTiExecutor extends MallardQVTiExecutor {

    private boolean changeInModels;

    private class ChangeAdapter extends EContentAdapter {

        /* (non-Javadoc)
         * @see org.eclipse.emf.common.notify.impl.AdapterImpl#notifyChanged(org.eclipse.emf.common.notify.Notification)
         */
        @Override
        public void notifyChanged(Notification msg) {

            super.notifyChanged(msg);
            if (msg.getEventType() == Notification.REMOVE) {
                System.err.println("Elements shuld not been removed!");
            }
        }


    }

    public BallotQVTiExecutor(@NonNull QVTiEnvironmentFactory environmentFactory,
            @NonNull Transformation transformation, @NonNull Configuration configuration) {
        super(environmentFactory, transformation, configuration);
//        ChangeAdapter adapter = new ChangeAdapter();
//        environmentFactory.getResourceSet().eAdapters().add(adapter);//.getResource(uri, true);
    }


    @Override
    public @Nullable Object internalExecuteTransformation(@NonNull Transformation transformation, @NonNull EvaluationVisitor undecoratedVisitor) {

        Rule rule = NameUtil.getNameable(transformation.getRule(), QVTimperativeUtil.ROOT_MAPPING_NAME);
        if (rule == null) {
            throw new IllegalStateException("Transformation " + transformation.getName() + " has no root mapping");
        }
        Random r = new Random();
        @SuppressWarnings("null")@NonNull CallExp callExp = PivotFactory.eINSTANCE.createOperationCallExp();		// FIXME TransformationCallExp
        pushEvaluationEnvironment(rule, callExp);
        try {
            Mapping mapping = (Mapping) rule;
            MappingStatement rootStatement = mapping.getMappingStatement();
            if (rootStatement != null) {
                assert rootStatement instanceof MappingSequence;
                MappingSequence mappingSequence = (MappingSequence) rootStatement;
                ArrayList<MappingStatement> invocations =
                        new ArrayList<MappingStatement>(mappingSequence.getMappingStatements());
                do {
                    Collections.shuffle(invocations, new Random(r.nextLong()));
                    setChangeInModels(false);
                    executeMappings(undecoratedVisitor, mappingSequence, invocations);
                } while (isChangeInModels());
            }
        }
        catch (InvalidValueException e) {
            // The Ballot execution can ignore InvalidValueExceptions, if they are missing
            // references to middle elements. Can we verify this?
            System.err.println(e);
        }
        finally {
            popEvaluationEnvironment();
        }
        return true;

    }


    public void executeMappings(EvaluationVisitor undecoratedVisitor, MappingSequence mappingSequence,
            ArrayList<MappingStatement> invocations) {
        for (MappingStatement mappingStatement : invocations) {
            if (mappingStatement != null) {
                pushEvaluationEnvironment(mappingStatement, mappingSequence);
                try {
                    mappingStatement.accept(undecoratedVisitor);
                }
                finally {
                    popEvaluationEnvironment();
                }
            }
        }
    }

//    ArrayList<Rule> invocations = new ArrayList<Rule>(transformation.getRule());
//    do {
//        Collections.shuffle(invocations, new Random());
//




    /**
     * If a realized variable is visited, a new element is created.
     */
    @Override
    public @Nullable Object internalExecuteRealizedVariable(@NonNull RealizedVariable realizedVariable, @NonNull EvaluationVisitor undecoratedVisitor) {
        Object element = super.internalExecuteRealizedVariable(realizedVariable, undecoratedVisitor);
        setChangeInModels(true);
        return element;

    }




    /* (non-Javadoc)
     * @see uk.ac.york.qvtd.pivot.qvtimperative.evaluation.MallardQVTiExecutor#internalExecuteNavigationAssignment(org.eclipse.qvtd.pivot.qvtcorebase.NavigationAssignment, java.lang.Object, java.lang.Object, java.lang.Object)
     */
    @Override
    public void internalExecuteNavigationAssignment(@NonNull NavigationAssignment navigationAssignment,
            @NonNull Object slotObject, @Nullable Object ecoreValue, @Nullable Object childKey) {
        super.internalExecuteNavigationAssignment(navigationAssignment, slotObject, ecoreValue, childKey);
        setChangeInModels(true);
    }


    /**
     * @return the changeInModels
     */
    public boolean isChangeInModels() {
        return changeInModels;
    }


    /**
     * @param changeInModels the changeInModels to set
     */
    public void setChangeInModels(boolean changeInModels) {
        this.changeInModels = changeInModels;
    }


//    // TODO Use the dependency graph to use the derivations so the number of combinations is lower
//    private Set<@NonNull Map<Variable, Object>> getRuleParamCombinations(Mapping m) {
//
//        Map<Variable, List<Object>> baseValues = new HashMap<Variable, List<Object>>();
//        // How to get stuff from the plan? The constructor must receive the dg as we need it!
//        //TODO Do this in the Broker or something, provide a map<mapping, bindings> or something so we dont need all the
//        // jgrapht or other  dependencies in the execution level
////        MappingAction ma2 = dg.getMappingVertices().stream().filter(ma -> ma.getMapping().equals(m)).findFirst().get();
////        ma2.getMappingDerivations();
//        for (Domain d : m.getDomain()) {
//            for (Variable v : ((Area) d).getGuardPattern().getVariable()) {
//                List<Object> varObjects = getModelManager().getElementsByType(d.getTypedModel(), v.getType());
//                baseValues.put(v, varObjects);
//            }
//        }
//        return getCombinations(baseValues);
//    }


//    /**
//     * An adapter implementation for tracking resource modification.
//     */
//    private class ModificationTrackingAdapter extends AdapterImpl	{
//        @Override
//        public void notifyChanged(Notification notification) {
//            if (!notification.isTouch()) {
//                setChangeInModels(true);
//            }
//        }
//    }

//    /**
//     * Creates all combinations of variables and values. Values can be of different sizes per variable.
//     * @param lists
//     * @return
//     */
//    private <K, V> Set<@NonNull Map<K, V>> getCombinations(Map<K, List<V>> lists) {
//        if (lists.isEmpty()) {
//            return Collections.emptySet();
//        }
//        Set<@NonNull Map<K, V>> combinations = new HashSet<>();
//        Set<@NonNull Map<K, V>> newCombinations;
//        Iterator<Entry<K, List<V>>> it = lists.entrySet().iterator();
//        // Create a new list for each element of the first entry
//        Entry<K, List<V>> entry = it.next();
//        for(V i: entry.getValue()) {
//            Map<K, V> newMap = new HashMap<K, V>();
//            newMap.put(entry.getKey(), i);
//            combinations.add(newMap);
//        }
//        while(it.hasNext()) {
//            entry = it.next();
//            if (!entry.getValue().isEmpty()) {
//                newCombinations = new HashSet<>();
//                // Since lists can have different sizes, always use the new one for final size
//                for(V v : entry.getValue()) {
//                    for(Map<K, V> first: combinations) {
//                        Map<K, V> newMap = new HashMap<K, V>(first);
//                        newMap.put(entry.getKey(), v);
//                        newCombinations.add(newMap);
//                    }
//                }
//                combinations.clear();
//                combinations = newCombinations;
//            }
//        }
//        return combinations;
//    }


}
