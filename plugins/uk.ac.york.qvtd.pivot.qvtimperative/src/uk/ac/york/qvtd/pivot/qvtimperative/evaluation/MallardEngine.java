package uk.ac.york.qvtd.pivot.qvtimperative.evaluation;

import java.util.Map;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.qvtd.pivot.qvtbase.Transformation;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiEnvironmentFactory;

import uk.ac.york.qvtd.compiler.Configuration;
import uk.ac.york.qvtd.compiler.utilities.MallardMtcUtil;
import uk.ac.york.qvtd.compiler.utilities.QvtMtcExecutionException;

public class MallardEngine {

    /**
     * Execute.
     * @param qvtias
     * @param direction
     * @param mode
     * @param enviromentFactory
     * @param verbose the verbose
     * @param savingOptions
     * @param qvtiAsModel
     * @return true, if successful
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public static boolean execute(Resource qvtias, @NonNull Configuration config,
            @NonNull QVTiEnvironmentFactory enviromentFactory, boolean verbose, @Nullable Map<?, ?> savingOptions)
                    throws QvtMtcExecutionException {

        MallardQVTiExecutor qvtiExecutor = createExecutor(qvtias, config, verbose, enviromentFactory);
        //logger.info("Executing QVTi transformation on test models.");
        boolean success = qvtiExecutor.execute();
        try {
            qvtiExecutor.saveModels(savingOptions);
        }
        catch (Exception ex) {
        }
        finally {
	        try {
	            qvtiExecutor.dispose();
	        }
	        catch (Exception ex) {
	        }
        }
        qvtiExecutor = null;
        return success;
    }

    public static boolean executeBallot(Resource qvtias, @NonNull Configuration config,
            @NonNull QVTiEnvironmentFactory enviromentFactory, boolean verbose, @Nullable Map<?, ?> savingOptions)
                    throws QvtMtcExecutionException {

        BallotQVTiExecutor qvtiExecutor = createBallotExecutor(qvtias, config, verbose, enviromentFactory);
        boolean success = qvtiExecutor.execute();
        try {
            qvtiExecutor.saveModels(savingOptions);
        }
        catch (Exception ex) {
        }
        finally {
	        try {
	            qvtiExecutor.dispose();
	        }
	        catch (Exception ex) {
	        }
        }
        qvtiExecutor = null;
        return success;
    }

    public static BallotQVTiExecutor createBallotExecutor(Resource qvtias, @NonNull Configuration config, boolean tracing,
            @NonNull QVTiEnvironmentFactory environmentFactory) throws QvtMtcExecutionException {
        @NonNull Transformation transformation = MallardMtcUtil.getTransformation(qvtias);
        BallotQVTiExecutor qvtiExecutor = new BallotQVTiExecutor(environmentFactory,
                transformation, config);
        qvtiExecutor.getEnvironmentFactory().setEvaluationTracingEnabled(tracing);
        qvtiExecutor.loadModelsFromConfiguration();
        return qvtiExecutor;
    }

    /**
     * Creates the executor.
     *
     * @param qvtias the qvtias
     * @param qvtias
     * @param direction
     * @param mode
     * @param tracing the tracing
     * @param environmentFactory
     * @param config
     * @return the mallard qv ti executor
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public static MallardQVTiExecutor createExecutor(Resource qvtias, @NonNull Configuration config, boolean tracing,
            @NonNull QVTiEnvironmentFactory environmentFactory) throws QvtMtcExecutionException {

        @NonNull Transformation transformation = MallardMtcUtil.getTransformation(qvtias);
        MallardQVTiExecutor qvtiExecutor = new MallardQVTiExecutor(environmentFactory,
                transformation, config);
        qvtiExecutor.getEnvironmentFactory().setEvaluationTracingEnabled(tracing);
        qvtiExecutor.loadModelsFromConfiguration();
        return qvtiExecutor;
    }

}
