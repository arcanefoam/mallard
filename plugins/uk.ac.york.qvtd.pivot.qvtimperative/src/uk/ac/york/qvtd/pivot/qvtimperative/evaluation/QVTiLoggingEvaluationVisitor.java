package uk.ac.york.qvtd.pivot.qvtimperative.evaluation;

import java.util.regex.Pattern;

import org.eclipse.emf.common.util.Monitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.CompleteEnvironment;
import org.eclipse.ocl.pivot.OCLExpression;
import org.eclipse.ocl.pivot.StandardLibrary;
import org.eclipse.ocl.pivot.evaluation.EvaluationEnvironment;
import org.eclipse.ocl.pivot.evaluation.EvaluationLogger;
import org.eclipse.ocl.pivot.evaluation.EvaluationVisitor;
import org.eclipse.ocl.pivot.evaluation.Executor;
import org.eclipse.ocl.pivot.evaluation.ModelManager;
import org.eclipse.ocl.pivot.ids.IdResolver;
import org.eclipse.ocl.pivot.util.Visitable;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.ocl.pivot.utilities.MetamodelManager;
import org.eclipse.ocl.pivot.utilities.NameUtil;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.IQVTiEvaluationVisitor;
import org.eclipse.qvtd.pivot.qvtimperative.util.AbstractMergedQVTimperativeVisitor;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import uk.ac.york.qvtd.pivot.qvtimperative.utilities.QVTiExecutionFormatter.LogType;


public class QVTiLoggingEvaluationVisitor extends AbstractMergedQVTimperativeVisitor<Object, IQVTiEvaluationVisitor> implements IQVTiEvaluationVisitor {

    /** The logger. */
    private static Logger logger = (Logger) LoggerFactory.getLogger(QVTiLoggingEvaluationVisitor.class);

    private int indentLevel = 0;


    protected QVTiLoggingEvaluationVisitor(IQVTiEvaluationVisitor decorated) {
        super(decorated);

        //logger.setAdditive(false);// setUseParentHandlers(false);
        // A framework API should have added a specific appender named "QVT", if not we will default to the console
//        for (Handler h :) {
//            if (h instanceof FileHandler) {
//                configure = false;
//                break;
//            }
//        }
//        if (configure) {
//            for (Handler h : handlers) {
//                if (h instanceof ConsoleHandler) {
//                    logger.removeHandler(h);
//                }
//            }
//            //if (handlers.length == 0) {
//                Handler ch = new ConsoleHandler();
//                ch.setLevel(Level.INFO);
//                ch.setFormatter(new QVTiExecutionFormatter());
//                logger.addHandler(ch);
//            //}
//        }
        decorated.setUndecoratedVisitor(this);
    }

    @Override
    public Object visiting(@NonNull Visitable visitable) {
        String name;
        if (visitable instanceof EObject) {
            name = String.valueOf(((EObject)visitable).eClass().getName());
        }
        else {
            name = String.valueOf(visitable.getClass().getSimpleName());
        }
        Object[] preParamArray = {getIndent(), "|__ ", name, String.valueOf(visitable)};
        logger.debug("{}{}{} -> {}", preParamArray);
        try {
            indentLevel++;
            Object visit =  visitable.accept(context);
            Object[] postParamArray = {getIndent(), NameUtil.qualifiedNameFor(visit)};
            logger.debug("{} = {}", postParamArray);
            indentLevel--;
            postParamArray = null;
//            preParamArray = null;
            return visit;
        }
        catch (Throwable e) {
            Object[] paramArray = {LogType.EXCEPTION, e};
            logger.debug("{}: ", paramArray);
            indentLevel--;
            paramArray = null;
//            throw e;
        }
        return null;

    }

    private Object getIndent() {
        return new String(new char[indentLevel]).replace("\0", "\t");
    }

    @SuppressWarnings("deprecation")
    @Override
    public @NonNull EvaluationVisitor createNestedEvaluator() {
        return context.createNestedEvaluator();
    }

    @Override
    public @Nullable Object evaluate(@NonNull OCLExpression body) {
        return context.evaluate(body);
    }

    @Override
    public @NonNull EnvironmentFactory getEnvironmentFactory() {
        return context.getEnvironmentFactory();
    }

    @Override
    public @NonNull EvaluationEnvironment getEvaluationEnvironment() {
        return context.getEvaluationEnvironment();
    }

    /** @deprected moved to Evaluator */
    @Deprecated
    @Override
    public @NonNull EvaluationVisitor getEvaluator() {
        return context.getEvaluator();
    }

    @Override
    public @NonNull Executor getExecutor() {
        return context.getExecutor();
    }

    /** @deprecated moved to Evaluator */
    @Deprecated
    @Override
    public @NonNull MetamodelManager getMetamodelManager() {
        return context.getMetamodelManager();
    }

    @SuppressWarnings("deprecation")
    @Override
    public @NonNull ModelManager getModelManager() {
        return context.getModelManager();
    }

    @Override
    public @Nullable Monitor getMonitor() {
        return context.getMonitor();
    }

    @SuppressWarnings("deprecation")
    @Override
    public @NonNull StandardLibrary getStandardLibrary() {
        return context.getStandardLibrary();
    }

    @Override
    public boolean isCanceled() {
        return context.isCanceled();
    }

    @Override
    public void setCanceled(boolean isCanceled) {
        context.setCanceled(isCanceled);
    }

    @Override
    public void setMonitor(@Nullable Monitor monitor) {
        context.setMonitor(monitor);
    }

    @Override
    public void setUndecoratedVisitor(@NonNull EvaluationVisitor evaluationVisitor) {
        context.setUndecoratedVisitor(evaluationVisitor);
    }

    /** @deprecated moved to Executor. */
    @Deprecated
    @Override
    public void dispose() {
        context.dispose();
    }

    /** @deprecated moved to Executor. */
    @Deprecated
    @Override
    public @NonNull CompleteEnvironment getCompleteEnvironment() {
        return context.getCompleteEnvironment();
    }

    /** @deprecated moved to Executor. */
    @Deprecated
    @Override
    public int getDiagnosticSeverity(int severityPreference, @Nullable Object resultValue) {
        return context.getDiagnosticSeverity(severityPreference, resultValue);
    }

    /** @deprecated moved to Executor. */
    @Deprecated
    @Override
    public @NonNull IdResolver getIdResolver() {
        return context.getIdResolver();
    }

    /** @deprecated moved to Executor. */
    @Deprecated
    @Override
    public @Nullable EvaluationLogger getLogger() {
        return context.getLogger();
    }

    /** @deprecated moved to Executor. */
    @Deprecated
    @Override
    public @NonNull Pattern getRegexPattern(@NonNull String regex) {
        return context.getRegexPattern(regex);
    }

    /** @deprecated moved to Executor. */
    @Deprecated
    @Override
    public int getSeverity(@Nullable Object validationKey) {
        return context.getSeverity(validationKey);
    }

    /** @deprecated moved to Executor. */
    @Deprecated
    @Override
    public @NonNull Class getStaticTypeOf(@Nullable Object value) {
        return context.getStaticTypeOf(value);
    }

    /** @deprecated moved to Executor. */
    @Deprecated
    @Override
    public @NonNull Class getStaticTypeOf(@Nullable Object value, @NonNull Object... values) {
        return context.getStaticTypeOf(value, values);
    }

    /** @deprecated moved to Executor. */
    @Deprecated
    @Override
    public @NonNull Class getStaticTypeOf(@Nullable Object value, @NonNull Iterable<?> values) {
        return context.getStaticTypeOf(value, values);
    }

    /** @deprecated moved to Executor. */
    @Deprecated
    @Override
    public void setLogger(@Nullable EvaluationLogger logger) {
        context.setLogger(logger);
    }


}
