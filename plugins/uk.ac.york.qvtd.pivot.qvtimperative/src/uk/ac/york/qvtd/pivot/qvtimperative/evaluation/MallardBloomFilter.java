/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.pivot.qvtimperative.evaluation;
import java.util.*;
import java.security.*;
import java.math.*;
import java.nio.*;


/**
 * The Class MallardBloomFilter.
 */
public class MallardBloomFilter {

    /** The filter. */
    private final BitSet filter;

    /** The key size. */
    private final int keySize;

    /** The capacity. */
    private final int capacity;

    /** The current size. */
    private int currentSize;

    /** The md. */
    private final MessageDigest md;

    /** The hash algorithm. */
    private final String hashAlgorithm = "MD5";		// Use MD5, alternatives are SHA-1 and SHA-256

    /**
     * Instantiates a new mallard bloom filter.
     *
     * @param numElements the num elements
     * @param fpp the fpp
     */
    public MallardBloomFilter(int numElements, float fpp) {
        capacity = (int) Math.ceil(- numElements * Math.log(fpp) / (Math.log(2)*Math.log(2)));
        keySize = (int) Math.floor((capacity/numElements) * Math.log(2));
        filter = new BitSet(capacity);
        currentSize = 0;
        try {
            md = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException("Error : MD5 Hash not found");
        }
    }

    /**
     * Gets the current size.
     *
     * @return the current size
     */
    public int getCurrentSize() {
        return currentSize;
    }

    /**
     * Calculate the index in the filter for the given id using the configured MessageDigest.
     *
     * Id can be the hashCode.
     *
     * @param id the id
     * @return the Hash
     */
    private int getFilterIndex(int id)
    {
        md.reset();
        byte[] bytes = ByteBuffer.allocate(4).putInt(id).array();
        md.update(bytes, 0, bytes.length);
        return Math.abs(new BigInteger(1, md.digest()).intValue()) % (capacity - 1);
    }

    /**
     * Add the object to the filter.
     *
     * @param obj the obj
     * @throws BloomException the bloom exception
     */
    public void add(Object obj) throws BloomException
    {
        BitSet objectId = getObjectId(obj);
        // TODO if already exists, do something?
        filter.or(objectId);
        currentSize++;
        if (currentSize >= capacity) {
            throw new BloomException("Capacity reached, false positive probablity will increase");
        }
    }

    /**
     * Check if the object is in the filter.
     *
     * @param obj the obj
     * @return true, if successful
     */
    public boolean contains(Object obj)
    {
        BitSet objectId = getObjectId(obj);
        objectId.and(filter);
        return objectId.cardinality() != 0;
    }

    /**
     * Gets the object id.
     *
     * @param obj the obj
     * @return the object id
     */
    /* Function to get set array for an object */
    private BitSet getObjectId(Object obj)
    {
        BitSet tmpset = new BitSet(keySize);
        md.reset();
        byte[] bytes = ByteBuffer.allocate(4).putInt(obj.hashCode()).array();
        md.update(bytes);
        byte[] hash = md.digest();

//        return Math.abs(new BigInteger(1, md.digest()).intValue()) % (capacity - 1);
//
//
//        tmpset[0] = getFilterIndex(obj.hashCode());
//        for (int i = 1; i < keySize; i++){
//            tmpset[i] = (getFilterIndex(i*tmpset[0]));
//        }
        return tmpset;
    }


    /**
     * The Class BloomException.
     */
    public class BloomException extends Exception {

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = -7234601112915653409L;

        /**
         * Instantiates a new bloom exception.
         *
         * @param message the message
         */
        protected BloomException(String message) {
            super(message);
        }

    }

}