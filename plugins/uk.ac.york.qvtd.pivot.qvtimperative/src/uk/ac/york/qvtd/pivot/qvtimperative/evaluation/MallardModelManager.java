package uk.ac.york.qvtd.pivot.qvtimperative.evaluation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiModelManager;
import org.eclipse.qvtd.pivot.qvtimperative.evaluation.QVTiTransformationAnalysis;
import org.eclipse.qvtd.runtime.evaluation.TypedModelInstance;

public class MallardModelManager extends QVTiModelManager {

    public MallardModelManager(@NonNull QVTiTransformationAnalysis transformationAnalysis) {
        super(transformationAnalysis);
    }

    @Override
    public @NonNull TypedModelInstance getTypedModelInstance(@NonNull TypedModel typedModel) {
        if (typedModel2typedModelInstance == null) {
            typedModel2typedModelInstance = new HashMap<@NonNull TypedModel, @NonNull TypedModelInstance>();
        }
        TypedModelInstance typedModelInstance = typedModel2typedModelInstance.get(typedModel);
        if (typedModelInstance == null) {
            typedModelInstance = new MallardTypedModelInstance(this, typedModel);
            typedModel2typedModelInstance.put(typedModel, typedModelInstance);
        }
        return typedModelInstance;
    }

    /**
     * Adds the model element to the resource of the given TypeModel
     *
     * @param tm the TypeModel
     * @param element the element
     */
    public void addModelElement(@NonNull TypedModel model, @NonNull Object element) {

        List<@NonNull EObject> elements = modelElementsMap.get(model);
        if (elements == null) {
            Resource resource = modelResourceMap.get(model);
            if (resource != null) {
                elements = new ArrayList<@NonNull EObject>(resource.getContents());
                modelElementsMap.put(model, elements);
            }
        }
        if (elements != null) {
            elements.add((EObject) element);
        }
    }


    public static class MallardTypedModelInstance extends QVTiTypedModelInstance {

        public MallardTypedModelInstance(@NonNull QVTiModelManager modelManager, @NonNull TypedModel typedModel) {
            super(modelManager, typedModel);
            // TODO Auto-generated constructor stub
        }

//        @Override
//        public @NonNull Set<@NonNull Object> getObjectsOfKind(org.eclipse.ocl.pivot.@NonNull Class type) {
//            //if (kind2instances == null) {
//            //    kind2instances = new HashMap<@NonNull Type, @NonNull Set<@NonNull Object>>();
//            //}
//            //Set<@NonNull Object> elements = kind2instances.get(type);
//            //if (elements == null) {
//                Set<@NonNull Object> elements = new HashSet<@NonNull Object>();
//                //kind2instances.put(type, elements);
//                for (@NonNull Object o : modelManager.getElementsByType(typedModel, type)) {
//                    elements.add(o);
//                }
//            //}
//            return elements;
//        }
//
//        @Override
//        public @NonNull Set<@NonNull Object> getObjectsOfType(org.eclipse.ocl.pivot.@NonNull Class type) {
////	        if (type2instances == null) {
////	            type2instances = new HashMap<@NonNull Type, @NonNull Set<@NonNull Object>>();
////	        }
////	        Set<@NonNull Object> elements = type2instances.get(type);
////	        if (elements == null) {
//                Set<@NonNull Object> elements = new HashSet<@NonNull Object>();
////	            type2instances.put(type, elements);
//                EObject eClass = type.getESObject();
//                for (@NonNull Object eObject : getObjectsOfKind(type)) {
//                    if (modelManager.eClass(eObject) == eClass) {
//                        elements.add(eObject);
//                    }
//                }
////	        }
//            return elements;
//        }
    }


}
