package uk.ac.york.qvtd.pivot.qvtimperative.utilities;

import java.text.DecimalFormat;
import java.util.Arrays;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.utils.EqualityHelper;
import org.eclipse.emf.ecore.EObject;

import com.google.common.cache.LoadingCache;

/**
     * Equality Helper that can perform almost equal comparisons for Double values
     * @author hhoyos
     *
     */
    public final class MallardEqualityHelper extends EqualityHelper {
        private boolean almostEqual = false;
        private DecimalFormat df;

        public MallardEqualityHelper(int places, LoadingCache<EObject, URI> uriCache) {
            super(uriCache);
            this.almostEqual = true;
            char[] chars = new char[places];
            Arrays.fill(chars, '#');
            String format = "#." + new String(chars);
            df = new DecimalFormat(format);
            //df.setRoundingMode(RoundingMode.CEILING);
        }

        private MallardEqualityHelper(LoadingCache<EObject, URI> uriCache) {
            this(7, uriCache);
        }

        @Override
        public boolean matchingValues(Object object1, Object object2) {
            if (almostEqual) {
                if (object1 instanceof Double && object2 instanceof Double) {
                    String round1 = df.format(object1);
                    String round2 = df.format(object2);
                    return round1.equals(round2);
                }
            }
            return super.matchingValues(object1, object2);
        }
    }