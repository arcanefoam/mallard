package uk.ac.york.qvtd.compiler.evaluation;

import java.util.ArrayDeque;
import java.util.List;

import org.eclipse.ocl.pivot.Property;

import uk.ac.york.qvtd.compiler.Binding;
import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.bindings.DirectBinding;
import uk.ac.york.qvtd.compiler.bindings.LocalBinding;
import uk.ac.york.qvtd.compiler.bindings.NavigationBinding;
import uk.ac.york.qvtd.compiler.executionplan.AllInstLoop;
import uk.ac.york.qvtd.compiler.executionplan.CallActionInvocation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.compiler.executionplan.LoopOperation;
import uk.ac.york.qvtd.compiler.executionplan.PropNavLoop;


/**
 * Evaluate the cost of an execution plan. The cost of an execution plan is the
 * sum of the cost of all the invocation chains in the plan. An invocation
 * chain is a path that starts and ends in a Mapping Action.
 * @author hhoyos
 *
 */
public class CostEvaluator  {

    /**
     * The complexity radix can be used to tailor the relation between multi-value
     * property and all-instances loops. Using base 3, three multi-value loops
     * would be as "expensive" as an all-instances loop.
     */
    private final int PROPERTY_NAVIGATION_LOOP_COST = 10;
    private final int ALL_INSTANCES_LOOP_COST = 100;
    private final int NAVIGATION_COST = 1;
    private final int IMPLICIT_NAVIGATION_COST = 5;
    private final int ACTION_EXECUTION_COST = 0;
    private final int ALL_INS_LOOP_EXECUTION_COST = 10;		// Executiokn of a loop all action involves a model search
    private final long DIRECT_BINDING_COST = 1;				// 1 because it is recalculated for each loop


    private ExecutionPlanGraph plan;
    protected ArrayDeque<Operation> callStack = new ArrayDeque<Operation>();

    public CostEvaluator() {
        super();
    }

    public double calculateCost(ExecutionPlanGraph plan) {
        this.plan = plan;
        return getOperationCost(plan.getRoot());
    }

    /**
     * Operation cost = Cex + Cin
     * @param operation
     * @return
     */
    public double getOperationCost(Operation operation) {
        callStack.push(operation);
        double cost = getExecutionCost(operation) + getInvocationCost(operation);
        if (cost >= Long.MAX_VALUE) {
            throw new ArithmeticException("Cost to big");
        }
        callStack.pop();
        return cost;
    }

    /**
     * Cin = Complexity * (operation cost child + loop cost child) + direct
     * binding cost child.
     * TODO Cache this value!
     * @param operation
     * @return
     */
    public double getInvocationCost(Operation operation) {

        long opCx = 1;
        if (operation instanceof PropNavLoop) {
            opCx = PROPERTY_NAVIGATION_LOOP_COST;
        } else if (operation instanceof AllInstLoop) {
            opCx = ALL_INSTANCES_LOOP_COST;
        }
        long nestedOpCost = 0l;
        long directBidingCost = 0l;
        double loopBindingCost = getLoopBindingCost(operation);
        for (Invocation inv : plan.outgoingEdgesOf(operation)) {
            Operation t = inv.getTarget();
            if (!callStack.contains(t)) {
                nestedOpCost += getOperationCost(t);
                if (operation instanceof CallActionOperation)
                    directBidingCost += getDirectBindingCost(inv);
            }
        }
        // Increase load factor if overflow
        return opCx*(nestedOpCost + loopBindingCost) + directBidingCost;
    }

    /**
     * Descend till the next level of CallAction operations to get the direct
     * binding cost
     * @param inv
     * @return
     */
    private double getDirectBindingCost(Invocation inv) {
        long dbCost = 0l;
        if (inv instanceof CallActionInvocation) {
            for (Binding b : ((CallActionInvocation) inv).getBindings().values()) {
                if (b instanceof LocalBinding) {
                    LocalBinding lb = (LocalBinding) b;
                    for (Property p : ((LocalBinding) b).getDerivedVariable().getNavigation()) {
                        dbCost += p.isIsImplicit()?
                                IMPLICIT_NAVIGATION_COST : NAVIGATION_COST;
                    }
                }
                if (b instanceof DirectBinding)
                    dbCost += DIRECT_BINDING_COST;
            }
        } else {
            Operation t = inv.getTarget();
            for (Invocation ninv : plan.outgoingEdgesOf(t)) {
                dbCost += getDirectBindingCost(ninv);
            }
        }
        return dbCost;
    }

    /**
     * Loop binding comes from NavigationBindings associated to the loop
     * @param operation
     * @return
     */
    private double getLoopBindingCost(Operation operation) {
        long lbCost = 0l;
        if (operation instanceof LoopOperation) {
            lbCost += 1; // 1 for the loop variable
            List<NavigationBinding> bs =  ((LoopOperation) operation).getAssociatedLoopBindings();
            for (NavigationBinding b : bs) {
                for (Property p : b.getNavigation()) {
                    lbCost += p.isIsImplicit() ? IMPLICIT_NAVIGATION_COST : NAVIGATION_COST;
                }
            }
        }
        return lbCost;

    }

    /**
     * We assume that all execution costs can be normalised to 1 as they are
     * the same regardless of the plan. The execution cost of loops depend on the loop type. AllInstances are also 0
     * (perhaps need to be more to reflect the search of all elements) and PropNavLoop depend on the navigation
     * cost to get to the loop collection.
     * @param caop
     * @return
     */
    protected double getExecutionCost(Operation operation) {
        if (operation instanceof CallActionOperation) {
            return ACTION_EXECUTION_COST;
        }
        if (operation instanceof AllInstLoop)
            return ALL_INS_LOOP_EXECUTION_COST;
        if (operation instanceof PropNavLoop)
            return ((PropNavLoop) operation).getNavigation().stream()
                    .mapToInt(p -> p.isIsImplicit() ? IMPLICIT_NAVIGATION_COST : NAVIGATION_COST).sum();
        return 0;
    }

}
