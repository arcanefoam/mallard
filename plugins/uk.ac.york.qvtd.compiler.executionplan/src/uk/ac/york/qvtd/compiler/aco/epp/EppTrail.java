/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;

import org.jgrapht.Graph;
import org.jgrapht.Graphs;

import uk.ac.york.qvtd.compiler.aco.AbstractForagingTrail;

// TODO: Auto-generated Javadoc
/**
 * The Class EppTrail.
 */
public class EppTrail extends AbstractForagingTrail<InvocationEdge, ForagingEdge> {


    /**
     * Instantiates a new epp trail.
     *
     * @param graph the graph
     * @param startingNode
     */
    public EppTrail(Graph<InvocationEdge, ForagingEdge> graph, InvocationEdge startingNode) {
        super(graph);
        setStartVertex(startingNode);
    }

    @Override
    public double getWeight() {
        return Graphs.getPathVertexList(this).stream()
                .mapToDouble(ie -> ie.getWeight())
                .sum();
    }

}
