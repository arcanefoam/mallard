/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.bindings;

import org.eclipse.ocl.pivot.Type;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;

// TODO: Auto-generated Javadoc
/**
 * The Class ModelType.
 */
public class ModelType {

    /** The type. */
    private final Type type;

    /** The domain. */
    private final TypedModel domain;

    /**
     * Instantiates a new model type.
     *
     * @param type the type
     * @param domain the domain
     */
    public ModelType(Type type, TypedModel domain) {
        super();
        this.type = type;
        this.domain = domain;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public Type getType() {
        return type;
    }

    /**
     * Gets the domain.
     *
     * @return the domain
     */
    public TypedModel getDomain() {
        return domain;
    }

    @Override
    public String toString() {
        return domain.getName() + ":" + type.getName();

    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((domain == null) ? 0 : domain.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ModelType))
            return false;
        ModelType other = (ModelType) obj;
        if (domain == null) {
            if (other.domain != null)
                return false;
        } else if (!domain.equals(other.domain))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }




}