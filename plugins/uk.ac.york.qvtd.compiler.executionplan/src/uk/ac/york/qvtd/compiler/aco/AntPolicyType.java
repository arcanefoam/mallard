/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;
// TODO: Auto-generated Javadoc
/**
 * A type of Ant Policy. So far, only two types are supported:
 *
 * <ul>
 * <li>A node selection policy that directs the process of adding solution
 * components
 * <li>An after solution is ready policy, to perform actions after the ant has
 * finished constructing a solution.
 * </ul>
 *
 * @author Carlos G. Gavidia
 *
 */
public enum AntPolicyType {

  /** The node selection. */
  NODE_SELECTION,
 /** The after solution is ready. */
 AFTER_SOLUTION_IS_READY;
}