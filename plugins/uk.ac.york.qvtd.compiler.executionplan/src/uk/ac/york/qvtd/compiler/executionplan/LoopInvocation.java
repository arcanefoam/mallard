/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.executionplan;

import uk.ac.york.qvtd.compiler.Operation;


/**
 * The Class LoopInvocation.
 */
public class LoopInvocation extends AbstractInvocation {


	/** The target. */
	private LoopOperation target;

	/**
	 * Instantiates a new loop invocation.
	 *
	 * @param source the source
	 * @param target the target
	 */
	public LoopInvocation(Operation source, LoopOperation target) {
		super(source);
		this.target = target;

	}

	@Override
	public boolean isPolled() {
		return false;
	}

	@Override
	public boolean isLooped() {
		return false;
	}

	@Override
	public void setPolled(boolean isPolled) {
		throw new UnsupportedOperationException("Loop invocations don't support this property.");

	}

	@Override
	public void setLooped(boolean isLooped) {
		throw new UnsupportedOperationException("Loop invocations don't support this property.");

	}

	@Override
	public Operation getTarget() {
		return target;
	}


	@Override
	public String toString() {
		return source.toString() + "->" + target.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof LoopInvocation))
			return false;
		LoopInvocation other = (LoopInvocation) obj;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}





}
