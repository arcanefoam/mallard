/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.evaluation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.jgrapht.event.ConnectedComponentTraversalEvent;
import org.jgrapht.event.EdgeTraversalEvent;
import org.jgrapht.event.VertexTraversalEvent;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;

/**
 * The Class MissingCallsEvaluator.
 */
public class MissingCallsEvaluator extends AbstractExecutionPlanEvaluator {


    private CallActionOperation target;

    private List<CallActionOperation> producers;

    public MissingCallsEvaluator(ExecutionPlanGraph plan) {
        super(plan);
        // TODO Auto-generated constructor stub
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.evaluation.AbstractEvaluator#edgeTraversed(org.jgrapht.event.EdgeTraversalEvent)
     */
//    @Override
//    public void edgeTraversed(EdgeTraversalEvent<Operation, Invocation> e) {
//        super.edgeTraversed(e);
//        if (valid) {
//            Operation s = e.getEdge().getSource();
//            Operation t = e.getEdge().getTarget();
//            // This evaluator should be invoked before adding bindings
//            assert s instanceof CallActionOperation;
//            assert t instanceof CallActionOperation;
//            CallActionOperation source = (CallActionOperation) s;
//            CallActionOperation target = (CallActionOperation) t;
//            Set<MappingAction> x = plan.getDependencyGraph().getRuntimeProducers(target.getAction(), visited);
//            for (MappingAction n : x) {
//                CallActionOperation xOp = plan.getOperationForMapping(n);
//                if (!invocations.containsKey(xOp) || !invocations.get(xOp).contains(target)) {
//                    // We are missing
//                    List<CallActionOperation> ops = missing.get(xOp);
//                    if (ops == null) {
//                        ops = new ArrayList<>();
//                        missing.put(xOp, ops);
//                    }
//                    ops.add(target);
//                }
//                else {
//                    List<CallActionOperation> ops = missing.get(xOp);
//                    if (ops != null) {
//                        ops.remove(target);
//                    }
//                }
//            }
//        }
//    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.evaluation.AbstractEvaluator#vertexTraversed(org.jgrapht.event.VertexTraversalEvent)
     */
    @Override
    public void vertexTraversed(VertexTraversalEvent<Operation> e) {

        Operation vertex = e.getVertex();
        // This evaluator should be invoked before adding bindings
        assert vertex instanceof CallActionOperation;
        CallActionOperation currentOperation = (CallActionOperation) vertex;
        Set<MappingAction> x = plan.getDependencyGraph().getSupplier(currentOperation.getAction());
//        Only add indirect producers, if any of the visited mappings produces the same types,
//        * i.e. the vertex is in the list of consumers of any of the visited mappings, so we have enough elements to bind
        Set<CallActionOperation> opX = x.stream().map(a -> plan.getOperationForMapping(a)).collect(Collectors.toSet());
        Set<CallActionOperation> invocations = plan.incomingEdgesOf(currentOperation).stream()
                .map(Invocation::getSource)
                .map(CallActionOperation.class::cast)
                .collect(Collectors.toSet());
        opX.removeAll(invocations);
        if (!opX.isEmpty()) {
            target = currentOperation;
            valid = false;
            producers = new ArrayList<>(opX);
            return;
        }
    }

    /**
     * @return the missingCalls
     */
    public List<CallActionOperation> getMissingCalls() {
        return producers;
    }

    /**
     * Get the action that is missing calls
     * @return
     */
    public CallActionOperation getTargetOperation() {
        //return callStack.pop();
        return target;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.evaluation.AbstractEvaluator#connectedComponentFinished(org.jgrapht.event.ConnectedComponentTraversalEvent)
     */
//    @Override
//    public void connectedComponentFinished(ConnectedComponentTraversalEvent e) {
//
//        super.connectedComponentFinished(e);
//        Iterator<Entry<CallActionOperation, List<CallActionOperation>>> it = missing.entrySet().iterator();
//        while (it.hasNext()) {
//            Entry<CallActionOperation, List<CallActionOperation>> entry = it.next();
//            if (!entry.getValue().isEmpty()) {
//                valid = false;
//                target = entry.getKey();
//                consumers = entry.getValue();
//            }
//        }
//    }
}
