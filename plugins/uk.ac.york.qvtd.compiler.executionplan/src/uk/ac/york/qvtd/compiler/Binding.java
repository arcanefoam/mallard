/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Variable;

import uk.ac.york.qvtd.compiler.bindings.DerivedVariable;
import uk.ac.york.qvtd.compiler.executionplan.LoopOperation;

/**
 * A Binding allows assigning the value of an input variable of a mapping
 * to another variable in the context. The context is the union of the variables
 * of the invoking mapping and the variables of all the the loops of the invocation
 * chain of the invoked mapping.
 */
public interface Binding {

        /**
         * Gets the bound variable of the binding.
         *
         * The bound variable is the mapping's input variable assigned by this
         * binding.
         *
         * @return the binding variable
         */
        Variable getBoundVariable();

        /**
         * Gets the source variable of the binding.
         *
         * The source variable is the variable from which the input
         * variable is derived. It can be a loop variable ({@link LoopOperation})
         * or a derived variable ({@link DerivedVariable})
         *
         * @return the source variable
         */
        @NonNull Variable getSourceVariable();

        /**
         * Gets the cost. The cost of the binding depends on its source.
         * <ul>
         * <li> For loop bindings: 100 for all instances and 10 for property loops
         * <l1> For direct bindings 0 (direct assignment of a variable)
         * </ul>
         * This cost is multiplied by a factor that depends on whether the
         * property is implicit or not. If implicit, the factor is 5
         * @return the cost
         */
        long getCost();


    }