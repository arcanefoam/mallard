/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;

import org.jgrapht.GraphPath;


/**
 * The Interface ForagingTrail is a simple implementation of the GraphPath interface that allows the path to be built
 * incrementally. It is the responsibility of implementing classes to guarantee that the EndVertex and Weight are kept
 * updated as the trail is built.
 *
 * @param <V> the value type
 * @param <E> the element type
 */
public interface ForagingTrail<V, E> extends GraphPath<V, E> {


    /**
     * Adds the edge to the trail.
     *
     * @param edgeUsed the edge used
     * @param weight the weight
     */
    void addEdge(E edgeUsed);

    /**
     * When the ant starts, the trail has a start vertex, although the edgelist is empty
     */
    void setStartVertex(V startVertex);

}
