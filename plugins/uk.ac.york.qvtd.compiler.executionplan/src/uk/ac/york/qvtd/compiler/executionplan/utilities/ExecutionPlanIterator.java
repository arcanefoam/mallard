/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.executionplan.utilities;

import java.util.NoSuchElementException;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.dependencies.util.QvtcIterator;


public class ExecutionPlanIterator // extends CrossComponentIterator<Operation, Invocation, ExecutionPlanIterator.VisitState> {
        extends QvtcIterator<Operation, Invocation> {

    private final static SentinelOperation SENTINEL = new SentinelOperation();
    /**
     * Creates a new iterator for he specified execution plan. Iteration will
     * start at the specified start vertex. If the specified start vertex is
     * <code> null</code>, Iteration will start at the root.
     *
     * @param plan the plan
     */
    public ExecutionPlanIterator(ExecutionPlanGraph plan) {
        this(plan, plan.getRoot());
    }

    /**
     * Creates a new iterator for he specified execution plan. Iteration will
     * start at the specified start vertex. If the specified start vertex is
     * <code> null</code>, Iteration will start at the root. If the plan has
     * more than one root (i.e. more than one vertex with no incoming edges)
     * it ill pick the first "root" added to the graph.
     *
     * @param plan the graph to be iterated.
     * @param startVertex the vertex iteration to be started.
     *
     * @throws IllegalArgumentException if <code>g==null</code> or does not
     * contain <code>startVertex</code>
     */
    public ExecutionPlanIterator(ExecutionPlanGraph plan, Operation startVertex)
    {
        super(plan, SENTINEL);
        if (plan == null) {
            throw new IllegalArgumentException("graph must not be null");
        }
        vertexIterator = plan.vertexSet().iterator();
        setCrossComponentTraversal(startVertex == null);

        reusableEdgeEvent = new FlyweightEdgeEvent<Invocation>(this, null);
        reusableVertexEvent = new FlyweightVertexEvent<Operation>(this, null);

        if (startVertex == null) {
            // Find the root, a vertex with no incoming edges
            try {
                this.startVertex = plan.vertexSet().stream()
                        .filter(v -> plan.inDegreeOf(v) == 0)
                        .findFirst()
                        .get();
            } catch (NoSuchElementException  e) {
                throw new IllegalArgumentException(
                        "graph must contain the a root vertex");
            }
        } else if (plan.containsVertex(startVertex)) {
            this.startVertex = startVertex;
        } else {
            throw new IllegalArgumentException(
                "graph must contain the start vertex");
        }
    }

    /**
     * encounterVertexAgain works well when adding children, but when providing the next vertex is not so easy.
     * The problem is that provideNextVertex will pop all finished children vertices and we need to set the
     * @param vertex
     */

    /**
     * The Class SentinelOperation.
     */
    static class SentinelOperation implements Operation {

        @Override
        public Operation clone() {
            return new SentinelOperation();
        }

        @Override
        public boolean isRoot() {
            return false;
        }

        @Override
        public String toString() {
            return "S";
        }

    }

}
