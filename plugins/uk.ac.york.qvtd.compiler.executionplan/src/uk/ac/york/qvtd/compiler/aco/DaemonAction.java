/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;
// TODO: Auto-generated Javadoc

/**
 * Global actions that have impact in all the colony and its environment. As
 * such, instances have access to the Ant Colony instance and also at the
 * Environment.
 * <p>
 * <p>
 * The moment in the process where this actions take place is defined by the
 * Daemon Action type.
 *
 * @author Carlos G. Gavidia
 * @param <V> the value type
 * @param <E> Class representing the Environment.
 */
public abstract class DaemonAction<V, E> {

    /** The aco phase. */
    protected final DaemonActionType acoPhase;

    /** The ant colony. */
    protected AntColony<V, E> antColony;

    /**
     * Instantiates a new daemon action.
     *
     * @param acoPhase the aco phase
     */
    public DaemonAction(DaemonActionType acoPhase) {
        super();
        this.acoPhase = acoPhase;
    }


    /**
     * Apply daemon action.
     *
     * @param antColony the ant colony
     */
    public abstract void applyDaemonAction(
            AntColony<V, E> antColony);



    /**
     * Gets the aco phase.
     *
     * @return the acoPhase
     */
    public DaemonActionType getAcoPhase() {
        return acoPhase;
    }
}