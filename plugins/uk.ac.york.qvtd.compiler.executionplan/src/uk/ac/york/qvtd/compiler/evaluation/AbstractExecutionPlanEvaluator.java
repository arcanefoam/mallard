/*******************************************************************************
\ * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.evaluation;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;

import org.jgrapht.event.ConnectedComponentTraversalEvent;
import org.jgrapht.event.EdgeTraversalEvent;
import org.jgrapht.event.VertexTraversalEvent;

import uk.ac.york.qvtd.compiler.ExecutionListener;
import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;

/**
 * The Abstract listener is the base class for all listeners. It provides
 * the basic functionality shared by all evaluators. It provides a Call stack
 * to keep track of actions that have been invoked in the current branch and a
 * Visited set of actions to keep track of all the invoked operations. Extending
 * classes should always invoke the base methods in this class in order to allow
 * proper management of the callStack and visited set.
 *
 * @author Horacio Hoyos
 */
public abstract class AbstractExecutionPlanEvaluator implements ExecutionListener<Operation, Invocation> {

    protected boolean valid = true;
    protected ExecutionPlanGraph plan;
    /**
     * Mappings visited in current branch
     */
    protected Deque<MappingAction> callStack = new ArrayDeque<MappingAction>();
    /**
     * Mappings visited in the past in all branches
     */
    protected Set<MappingAction> visited = new HashSet<MappingAction>();

    private int connectedComponentsCounter = 0;


    public AbstractExecutionPlanEvaluator() {
        super();
    }

    public AbstractExecutionPlanEvaluator(ExecutionPlanGraph plan) {
        super();
        this.plan = plan;
    }



    public Deque<MappingAction> getCallStack() {
        return callStack;
    }

    public Set<MappingAction> getVisitedStack() {
        return visited;
    }

    @Override
    public void connectedComponentFinished(ConnectedComponentTraversalEvent e) {
    }

    /**
     * For execution plans we can not allow the plan to be disconnected
     */
    @Override
    public void connectedComponentStarted(ConnectedComponentTraversalEvent e) {
        connectedComponentsCounter++;
        if (connectedComponentsCounter > 1) {
            valid = false;
        }
    }

    @Override
    public void edgeTraversed(EdgeTraversalEvent<Invocation> e) {
    }

    @Override
    public void vertexTraversed(VertexTraversalEvent<Operation> e) {
        //System.out.println("vertexTraversed " + e.getVertex().toString());
        Operation op = e.getVertex();
        if (op instanceof CallActionOperation) {
            callStack.push(((CallActionOperation) op).getAction());
            //visited.add(((CallActionOperation) op).getAction());
        }

    }

    @Override
    public void vertexFinished(VertexTraversalEvent<Operation> e) {
        //System.out.println("vertexFinished " + e.getVertex().toString());
        // TODO, I think just pop will be safe
        Operation op = e.getVertex();
        if (op instanceof CallActionOperation) {
            callStack.removeFirstOccurrence(((CallActionOperation) op).getAction());
            visited.add(((CallActionOperation) op).getAction());
        }
    }

    @Override
    public boolean isValid() {
        return valid;
    }

}