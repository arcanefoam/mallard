/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.aco.AbstractAntColony;
import uk.ac.york.qvtd.compiler.aco.Ant;
import uk.ac.york.qvtd.compiler.aco.Environment;

/**
 * The Class EppAntColony.
 */
public class EppAntColony extends AbstractAntColony<InvocationEdge, ForagingEdge> {

        /** The environment. */
        private EppEnvironment environment;


        /**  The iteration counter. */
        private int iteration = 0;

        private Map<Double, Set<EppTrail>> plansPerCost;


        /**
         * Instantiates a new epp ant colony.
         *
         * @param environment the environment
         * @param configurationProvider the configuration provider
         * @param dg the dg
         */
        public EppAntColony(EppEnvironment environment,
                EppConfiguration configurationProvider) {
            super(configurationProvider);
            this.environment = environment;
            this.plansPerCost = new HashMap<>();
        }

        /**
         * Creates the ant.
         *
         * @return the ant
         */
        @Override
        public Ant<InvocationEdge, ForagingEdge> createAnt() {

            return new EppAnt(this);
        }

        /**
         * Gets the environment.
         *
         * @return the environment
         */
        @Override
        public Environment<InvocationEdge, ForagingEdge> getEnvironment() {
            return environment;
        }

        public double getExploitationProbability() {
            return ((EppConfiguration)getConfigurationProvider()).getExploitationProbability();
        }

        /**
         * Gets the iteration.
         *
         * @return the iteration
         */
        public int getIteration() {
            return iteration;
        }

        /**
         * @return the plansPerCost
         */
        public Map<Double, Set<EppTrail>> getPlansPerCost() {
            return plansPerCost;
        }


        /**
         * Reset all ants.
         */
        @Override
        public void resetAllAnts() {
            super.resetAllAnts();
            iteration++;
        }

        /* (non-Javadoc)
         * @see uk.ac.york.qvtd.compiler.aco.AbstractAntColony#startForaging()
         */
        @Override
        public void startForaging() {
            super.startForaging();
            // Keep track of the different plans per cost
            for (Ant<InvocationEdge, ForagingEdge> ant : getHive()) {
                Set<EppTrail> plans = plansPerCost.get(ant.getSolutionCost());
                if (plans == null) {
                    plans = new HashSet<EppTrail>();
                    plansPerCost.put(ant.getSolutionCost(), plans);
                }
                plans.add((EppTrail) ant.getTrail());
            }
        }
    }