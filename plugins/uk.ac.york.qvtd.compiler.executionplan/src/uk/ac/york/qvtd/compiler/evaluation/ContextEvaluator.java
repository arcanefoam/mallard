/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.evaluation;

import java.util.ArrayDeque;
import java.util.Deque;

import org.eclipse.ocl.pivot.Variable;
import org.jgrapht.event.EdgeTraversalEvent;
import org.jgrapht.event.VertexTraversalEvent;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.context.Context;
import uk.ac.york.qvtd.compiler.context.ContextEdgeFactory;
import uk.ac.york.qvtd.compiler.context.ContextVertex;
import uk.ac.york.qvtd.compiler.executionplan.CallActionInvocation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.compiler.executionplan.utilities.ExecutionPlanUtils;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.ParameterEdge;

/**
 * A Context Evaluator will become invalid when the first invocation
 * without binding is found. This information can be later retrieved
 * to spwan new plans that add the missing bindings.
 * The evaluator also keeps track of the context (variables, their assigned
 * properties and the relations between variables) so this information
 * can be used.
 * @author Horacio Hoyos
 *
 */
public class ContextEvaluator extends AbstractExecutionPlanEvaluator {

    private class Sentinel implements Invocation {

        @Override
        public boolean isPolled() {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void setPolled(boolean isPolled) {
            // TODO Auto-generated method stub

        }

        @Override
        public Operation getSource() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Operation getTarget() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public boolean isLooped() {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void setLooped(boolean isLooped) {
            // TODO Auto-generated method stub

        }

    }

    private Invocation lastInvocation;
    private Deque<Context> contextStack = new ArrayDeque<Context>();
    private Deque<Invocation> invocationStack = new ArrayDeque<Invocation>();


    public ContextEvaluator(ExecutionPlanGraph plan) {
        super(plan);
    }

    public Invocation getLastInvocation() {
        return lastInvocation;
    }

    public Deque<Context> getContextStack() {
        return contextStack;
    }

    @Override
    public void edgeTraversed(EdgeTraversalEvent<Invocation> e) {
        if (valid) {
            if (e.getEdge() instanceof CallActionInvocation) {
                if (((CallActionInvocation) e.getEdge()).getBindings().isEmpty()) {
                    //System.out.println("Missing bindings edge " + e.getEdge());
                    lastInvocation = e.getEdge();
                    valid = false;
                } else {
                    // TODO Not sure if thus else should be for all type of edges
                    invocationStack.addLast(e.getEdge());
                }
            }
        }
    }

    @Override
    // TODO Exploit the fact that the context is a graph!
    public void vertexTraversed(VertexTraversalEvent<Operation> e) {
        super.vertexTraversed(e);
        Operation op = e.getVertex();
        if (op instanceof CallActionOperation)  {
            Context newC = new Context(new ContextEdgeFactory());
            CallActionOperation currentOperation = (CallActionOperation) op;
            DependencyGraph dg = plan.getDependencyGraph();
            if (!dg.incomingEdgesOf(currentOperation.getAction()).isEmpty()) {
                // Only unguarded input variables can be used in the context
                for (DependencyEdge de : dg.incomingEdgesOf(currentOperation.getAction())) {
                    if (de instanceof ParameterEdge) {
                        for (Variable pv : ((ParameterEdge) de).getVariables()) {
                            if (!ExecutionPlanUtils.typeIsGuarded(currentOperation.getAction(), pv.getType())) {
                                ContextVertex cv = new ContextVertex(pv, ((ClassDatum) de.getSource()).getDomain());
                                newC.addVertex(cv);
                            }
                        }
                    }
                }
                for (DependencyEdge de : dg.outgoingEdgesOf(currentOperation.getAction())) {
                    if (de instanceof ParameterEdge) {
                        for (Variable pv : ((ParameterEdge) de).getVariables()) {
                            ContextVertex cv = new ContextVertex(pv, ((ClassDatum) de.getTarget()).getDomain());
                            newC.addVertex(cv);
                        }
                    }
                }
                // TODO use the mapping assignments to update the setProperties.

                // TODO use bindings to update the context information
                // The last invocation in the invocation stack should be the invocation
                // of this vertex
                //Invocation i = invocationStack.peek();
                //assert i.getTarget().equals(caop);
                // TODO Check how this works once loop actions are in play
                // TODO add relations between the new context and the old one, so when
                /*
                 *
                 * Una vez se tienen todas las posibilidades, se pueden refinar utilizando las intra-relations
                 * para remover F y reemplazarlo con derivations, si a2 = a1.x y b = a2.y, entonces se debe crear
                 * un nuevo plan para cada alternativa, ej (* indica la variable del loop):
                 * a1	a2		b
                 * F	F		a2*.y
                 * F	a1*.x	F
                 * F	a1*.x	a1*.x.y
                 * a1'	F		a2*.y
                 * a1'	a1'.x	F
                 * a1'	a1'.x	a1'.x.y
                 * etc..
                 */
                contextStack.push(newC);

            }
            invocationStack.pollFirst();
            // TODO, I think that with this pop, the sentinel is no longer needed.
            // Add a new sentinel to the invocation stack
            Invocation s = new Sentinel();
            invocationStack.addLast(s);
        }

    }

    @Override
    public void vertexFinished(VertexTraversalEvent<Operation> e) {
        super.vertexFinished(e);
        // TODO Check that the context is empty for a good reason
        contextStack.pollFirst();
        // TODO any variables reused from the previous context should have
        // their properties and relations updated!
        // Remove all invocations from the stack till a sentinel is found
        Invocation i;
        do {
            i = invocationStack.pollFirst();
        } while (i != null && !(i instanceof Sentinel));
    }

}
