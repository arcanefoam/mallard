package uk.ac.york.qvtd.compiler.executionplan.utilities;

import java.util.HashMap;
import java.util.Map;

import org.jgrapht.ext.VertexNameProvider;

import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;

/**
 * The Class OperationNameProvider.
 */
public class OperationNameProvider implements VertexNameProvider<Operation> {

    /** The next integer to use when generating ids. */
    private int nextID = 1;

    /** The id map. */
    private final Map<Operation, Integer> idMap = new HashMap<Operation, Integer>();

    /* (non-Javadoc)
     * @see org.jgrapht.ext.VertexNameProvider#getVertexName(java.lang.Object)
     */
    @Override
    public String getVertexName(Operation vertex) {
        if (vertex instanceof CallActionOperation) {
            return ((CallActionOperation) vertex).getAction().getLabel();
        } else { //if (vertex instanceof LoopOperation) {
            Integer id = idMap.get(vertex);
            if (id == null) {
                id = nextID++;
                idMap.put(vertex, id);
            }
            return "l"+ id.toString();
        }
    }
}