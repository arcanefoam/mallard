/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.slopy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Element;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.eclipse.qvtd.pivot.qvtcorebase.Area;
import org.eclipse.qvtd.pivot.qvtcorebase.CoreDomain;
import org.eclipse.qvtd.pivot.qvtcorebase.CorePattern;
import org.eclipse.qvtd.pivot.qvtcorebase.analysis.DomainUsage;
import org.jgrapht.graph.DirectedWeightedSubgraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.bindings.ModelType;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.compiler.executionplan.ExecutionPlan;
import uk.ac.york.qvtd.compiler.exploration.CompleteExploration;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.derivations.impl.EdmondsDirectedMinimumArborescence;
import uk.ac.york.qvtd.dependencies.derivations.impl.MappingDerivationsImpl;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.inter.ParameterEdge;

/**
 * Creates a plan that invokes all mappings sequentially as the naive algorithm would do.
 * @author Goblin
 *
 */
public class SloppyPlanner extends CompleteExploration {


    public SloppyPlanner(DependencyGraph dg, Map<Element, DomainUsage> analysis, boolean verbose, @NonNull EnvironmentFactory factory) {
        super(dg, analysis, verbose, factory);
    }

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(SloppyPlanner.class.getName());

    public void createSloppyPlan() {

        // Create initial operations
        CallActionOperation rop = new CallActionOperation(dg.getRootVertex(), true);
        //mappingToOperation.put(dg.getRootVertex(), rop);
        // The plan is very simple, all mappings invoked from the root
        logger.info("Initial structure");
        ExecutionPlanGraph ip = new ExecutionPlan(dg, rop);
        List<MappingAction> mappings = dg.getMappingVertices();
        mappings.remove(dg.getRootVertex());
        for (MappingAction m : mappings) {
            CallActionOperation op = new CallActionOperation(m, false);
            //mappingToOperation.put(m, op);
            ip.addOperationForMapping(m, op);
            ip.addEdge(rop, op);
        }
        Set<ExecutionPlanGraph> permutationPlans = new HashSet<ExecutionPlanGraph>();
        permutationPlans.add(ip);
        // Add bindings
        Set<ExecutionPlanGraph> fullBoundPlans = createBindingPlans(permutationPlans);
        Map<Double, Set<ExecutionPlanGraph>> groupCost = new HashMap<Double, Set<ExecutionPlanGraph>>();
        for (ExecutionPlanGraph p : fullBoundPlans) {
            ((ExecutionPlan) p).calculateCost();
            double cost = p.getCost();
            Set<ExecutionPlanGraph> ps = groupCost.get(cost);
            if (ps == null) {
                ps = new HashSet<ExecutionPlanGraph>();
                groupCost.put(cost, ps);
            }
            ps.add(p);
        }
        if (verbose) {
            for (Double cost : groupCost.keySet()) {
                logger.info(cost + "," + groupCost.get(cost).size());
            }
        }
        // Get the best!
        double minCost = groupCost.keySet().stream().min(Double::compare).get();
        logger.info("minCost," + minCost);
        double maxCost = groupCost.keySet().stream().max(Double::compare).get();
        logger.info("maxCost," + maxCost);

        bestSolutions = groupCost.get(minCost);
        printPlans("redbest", bestSolutions);
    }

    /**
     * For the given operation, track all input variables associated to a given ModelType.
     *
     * @param plan the plan
     * @param caop the caop
     * @return the map
     */
    @Override
    public Map<ModelType, List<Variable>> createBoundVars(ExecutionPlanGraph plan, CallActionOperation caop) {
        Map<ModelType, List<Variable>> boundVars = new HashMap<ModelType, List<Variable>>();
        List<Variable> vars;
        MappingAction ma = caop.getAction();
        MappingDerivationsImpl der = ma.getMappingDerivations();
        List<Variable> primVariables = new ArrayList<Variable>();
        for (Entry<Set<Variable>, Variable> derInfo : caop.getAction().getMappingDerivations().getPrimaryVariable().entrySet()) {
                Variable primVar = derInfo.getValue();
                Type type = primVar.getType();
                assert primVar.eContainer() instanceof CorePattern; // Input vars shuold be in a guard pattern
                Area a  = ((CorePattern) primVar.eContainer()).getArea();
                assert a instanceof CoreDomain;
                TypedModel domain = ((CoreDomain)a).getTypedModel();
                ModelType bv = new ModelType(type, domain);
                vars = boundVars.get(bv);
                if (vars == null) {
                    vars = new ArrayList<Variable>();
                    boundVars.put(bv, vars);
                }
                vars.add(primVar);
        }
//        for (DependencyEdge e : plan.getDependencyGraph().incomingEdgesOf(caop.getAction())) {
//            if (e instanceof ParameterEdge) {
//                Type type = ((ClassDatum) e.getSource()).getType();
//                TypedModel domain = ((ClassDatum) e.getSource()).getDomain();
//                ModelType bv = new ModelType(type, domain);
//                vars = boundVars.get(bv);
//                if (vars == null) {
//                    vars = new ArrayList<Variable>();
//                    boundVars.put(bv, vars);
//                }
//                vars.addAll(((ParameterEdge) e).getVariables());
//            }
//        }
        return boundVars;
    }


}
