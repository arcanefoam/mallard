/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;

import org.jgrapht.DirectedGraph;
import org.jgrapht.GraphPath;

import uk.ac.york.qvtd.compiler.aco.epp.InvocationEdge;

/**
 * The Interface Environment.
 *
 * @param <V> the problem graph Vertex type
 * @param <E> the problem graph Edge type
 */
public interface Environment<V, E> {

    /**
     * Gets the problem graph.
     *
     * @return the problem graph
     */
    public DirectedGraph<V, E> getForagingArea();


    /**
     * Adds the given value to the existing pheromone on the edge.
     * @param edgeId
     * @param value
     */
    void addPheromoneToEdge(E edge, double value);

    /**
     * Adds the pheromone to trail.
     *
     * @param trail An array of edgeIds that represent the trail
     * @param value The value to add to the trail
     */
    void addPheromoneToTrail(GraphPath<V, E> trail, double value);

    /**
     * Adds the pheromone to trail.
     *
     * @param trail An array of edgeIds that represent the trail
     * @param value The value to add to the trail
     * @param max the max allowed pheromone (for MMAS)
     */
    void addPheromoneToTrail(GraphPath<V, E> trail, double value, double max);

    /**
     * Gets the current values of pheromone for all edges in the area
     * @return A DOT graph with the pheromone value printed for each label
     */
    String getPheromoneValues();

    /**
     * Get the starting vertex for an Ant.
     * @return
     */
    InvocationEdge getStartingVertex();


    /**
     * Multiplies the pheromone of all arcs in the problem graph by the factor.
     * <p>
     *
     * @param factor Factor for multiplication.
     */
    void globalUpdateDecay(double factor);

    /**
     * Multiplies the pheromone of all arcs in the problem graph by the factor. If the new value is below the threshold
     * the value is set to the threshold.
     *
     * @param factor the factor
     * @param min the min pheromome value (for MMAS)
     */
     void globalUpdateDecay(double factor, double min);

    /**
     * Verifies if the problem graph provided is valid.
     *
     * @return True if valid, false otherwise.
     */
    boolean isProblemGraphValid();

    /**
     * Assigns the same pheromone value to all edges
     *
     * @param pheromoneValue Value to assign.
     */
    void setPheromoneValues(double pheromoneValue);


}
