/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.executionplan;

import java.util.HashMap;
import java.util.Map;

import org.jgrapht.EdgeFactory;

import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.dependencies.util.Pair;


/**
 * A factory for creating Invocation objects.
 */
public class InvocationFactory implements EdgeFactory<Operation, Invocation> {

    private boolean reuse = false;
    private Map<Pair<Operation, Operation>, Invocation> invocationPool;

    public InvocationFactory() {
    }

    public InvocationFactory(boolean reuse) {
        super();
        this.reuse = reuse;
        if (reuse) {
            invocationPool = new HashMap<Pair<Operation,Operation>, Invocation>();
        }
    }

    @Override
    public Invocation createEdge(Operation sourceVertex, Operation targetVertex) {
        if (targetVertex instanceof CallActionOperation) {
            return getInvocation(sourceVertex, targetVertex);
        } else {
            // Reuse should only be used for permutation+complete solutions so loops are not involved
            return new LoopInvocation(sourceVertex, (LoopOperation) targetVertex);
        }
    }

    private Invocation getInvocation(Operation sourceVertex, Operation targetVertex) {
        if (!reuse) {
            return new CallActionInvocation(sourceVertex, (CallActionOperation) targetVertex);
        } else {
            Pair<Operation, Operation> p = Pair.of(sourceVertex, targetVertex);
            Invocation inv = invocationPool.get(p);
            if (inv == null) {
                inv = new CallActionInvocation(sourceVertex, (CallActionOperation) targetVertex);
                invocationPool.put(p, inv);
            }
            return inv;
        }
    }

}