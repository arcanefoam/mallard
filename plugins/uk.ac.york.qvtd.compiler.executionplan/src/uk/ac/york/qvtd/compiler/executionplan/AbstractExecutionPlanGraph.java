/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.executionplan;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Collectors;

import org.jgrapht.DirectedGraph;
import org.jgrapht.EdgeFactory;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.UndirectedGraph;
import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.AbstractGraph;
import org.jgrapht.util.ArrayUnenforcedSet;
import org.jgrapht.util.TypeUtil;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.executionplan.utilities.OperationNameProvider;


/**
 * General implementation of the {@link org.jgrapht.Graph} interface, providing a {@link org.jgrapht.DirectedGraph}
 * that deterministic vertex and edge set ordering (via {@link ???} and {@link ArrayList}), outgoing edge order
 * depicts execution order form first to last.
 *
 * @author Horacio Hoyos
 */
public abstract class AbstractExecutionPlanGraph
    extends AbstractGraph<Operation, Invocation>
    implements ExecutionPlanGraph,
        Cloneable,
        Serializable
{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1263088497616142427L;
    //private final boolean allowingLoops = true;
    //private final boolean allowingMultipleEdges = false;

    /** The edge factory. */
    private EdgeFactory<Operation, Invocation> edgeFactory;

    /** The specifics. */
    private ExecutionPlanSpecifics specifics;

    /** The unmodifiable edge set. */
    private transient Set<Invocation> unmodifiableEdgeSet = null;

    /** The unmodifiable vertex set. */
    private transient Set<Operation> unmodifiableVertexSet = null;

    /** The edge list factory. */
    protected EdgeListFactory<Operation, Invocation> edgeListFactory;

    /** The operation name provider. */
    private OperationNameProvider nodeIds = new OperationNameProvider();

    /**
     * Construct a new graph. The graph can either be directed or undirected,
     * depending on the specified edge factory.
     *
     * @param ef the edge factory of the new graph.
     * @throws NullPointerException if the specified edge factory is <code>
     * null</code>.
     */
    protected AbstractExecutionPlanGraph(EdgeFactory<Operation, Invocation> ef)
    {
        if (ef == null) {
            throw new NullPointerException();
        }

        edgeFactory = ef;
        specifics = createSpecifics();
        this.edgeListFactory = new ExecutionPlanEdgeFactory();
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.executionplan.ExecutionPlanGraph#addEdge(int, org.eclipse.qvtd.compiler.Operation, org.eclipse.qvtd.compiler.Operation)
     */
    @Override
    public Invocation addEdge(int index, Operation sourceVertex, Operation targetVertex)
    {
        assertVertexExist(sourceVertex);
        assertVertexExist(targetVertex);

        if (containsEdge(sourceVertex, targetVertex))
        {
            return null;
        }
        Invocation Invocation = edgeFactory.createEdge(sourceVertex, targetVertex);
        if (containsEdge(Invocation)) { // this restriction should stay!
            return null;
        } else {
            specifics.addEdgeToTouchingVertices(index, Invocation);
            return Invocation;
        }
    }


    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.executionplan.ExecutionPlanGraph#addEdge(int, org.eclipse.qvtd.compiler.Operation, org.eclipse.qvtd.compiler.Operation, org.eclipse.qvtd.compiler.Invocation)
     */
    @Override
    public boolean addEdge(int index, Operation sourceVertex, Operation targetVertex, Invocation Invocation)
    {
        if (Invocation == null) {
            throw new NullPointerException();
        } else if (containsEdge(Invocation)) {
            return false;
        }

        assertVertexExist(sourceVertex);
        assertVertexExist(targetVertex);

        if (containsEdge(sourceVertex, targetVertex))
        {
            return false;
        }
        specifics.addEdgeToTouchingVertices(index, Invocation);
        return true;
    }



    /**
     * @see Graph#addEdge(Object, Object)
     */
    @Override public Invocation addEdge(Operation sourceVertex, Operation targetVertex)
    {
        assertVertexExist(sourceVertex);
        assertVertexExist(targetVertex);

        if (containsEdge(sourceVertex, targetVertex))
        {
            return null;
        }
        Invocation Invocation = edgeFactory.createEdge(sourceVertex, targetVertex);
        if (containsEdge(Invocation)) { // this restriction should stay!
            return null;
        } else {
            specifics.addEdgeToTouchingVertices(Invocation);
            return Invocation;
        }
    }

//    /**
//     * Returns <code>true</code> if and only if self-loops are allowed in this
//     * graph. A self loop is an edge that its source and target vertices are the
//     * same.
//     *
//     * @return <code>true</code> if and only if graph loops are allowed.
//     */
//    public boolean isAllowingLoops()
//    {
//        return true;
//    }
//
//    /**
//     * Returns <code>true</code> if and only if multiple edges are allowed in
//     * this graph. The meaning of multiple edges is that there can be many edges
//     * going from vertex v1 to vertex v2.
//     *
//     * @return <code>true</code> if and only if multiple edges are allowed.
//     */
//    public boolean isAllowingMultipleEdges()
//    {
//        return false;
//    }

    /**
     * @see Graph#addEdge(Object, Object, Object)
     */
    @Override public boolean addEdge(Operation sourceVertex, Operation targetVertex, Invocation Invocation)
    {
        if (Invocation == null) {
            throw new NullPointerException();
        } else if (containsEdge(Invocation)) {
            return false;
        }

        assertVertexExist(sourceVertex);
        assertVertexExist(targetVertex);

        if (containsEdge(sourceVertex, targetVertex))
        {
            return false;
        }
        specifics.addEdgeToTouchingVertices(Invocation);
        return true;
    }

    /**
     * @see Graph#addVertex(Object)
     */
    @Override public boolean addVertex(Operation Operation)
    {
        if (Operation == null) {
            throw new NullPointerException();
        } else if (containsVertex(Operation)) {
            return false;
        } else {
            specifics.addVertex(Operation);

            return true;
        }
    }

    /**
     * Returns a shallow copy of this graph instance. Neither edges nor vertices
     * are cloned.
     *
     * @return a shallow copy of this set.
     *
     * @throws RuntimeException
     *
     * @see java.lang.Object#clone()
     */
    @Override public Object clone()
    {
        try {
            TypeUtil<AbstractExecutionPlanGraph> typeDecl = null;

            AbstractExecutionPlanGraph newGraph =
                TypeUtil.uncheckedCast(super.clone(), typeDecl);

            //newGraph.edgeMap = new LinkedHashMap<Invocation, IntrusiveEdge>();

            newGraph.edgeFactory = this.edgeFactory;
            newGraph.unmodifiableEdgeSet = null;
            newGraph.unmodifiableVertexSet = null;

            // NOTE:  it's important for this to happen in an object
            // method so that the new inner class instance gets associated with
            // the right outer class instance
            newGraph.specifics = newGraph.createSpecifics();

            Graphs.addGraph(newGraph, this);

            return newGraph;
        } catch (CloneNotSupportedException Invocation) {
            Invocation.printStackTrace();
            throw new RuntimeException();
        }
    }

    /**
     * @see Graph#containsEdge(Object)
     */
    @Override public boolean containsEdge(Invocation Invocation)
    {
        return specifics.containsEdge(Invocation);
    }

    /**
     * @see Graph#containsVertex(Object)
     */
    @Override public boolean containsVertex(Operation Operation)
    {
        return specifics.getVertexSet().contains(Operation);
    }

    /**
     * Degree of.
     *
     * @param vertex the vertex
     * @return the int
     * @see UndirectedGraph#degreeOf(Object)
     */
    public int degreeOf(Operation vertex)
    {
        return specifics.degreeOf(vertex);
    }

    /**
     * @see Graph#edgeSet()
     */
    @Override public Set<Invocation> edgeSet()
    {
        return specifics.edgeSet();
    }

    /**
     * @see Graph#edgesOf(Object)
     */
    @Override public Set<Invocation> edgesOf(Operation vertex)
    {
        return specifics.edgesOf(vertex);
    }

    /**
     * @see Graph#getAllEdges(Object, Object)
     */
    @Override public Set<Invocation> getAllEdges(Operation sourceVertex, Operation targetVertex)
    {
        return specifics.getAllEdges(sourceVertex, targetVertex);
    }

    /**
     * @see Graph#getEdge(Object, Object)
     */
    @Override public Invocation getEdge(Operation sourceVertex, Operation targetVertex)
    {
        return specifics.getEdge(sourceVertex, targetVertex);
    }

    /**
     * @see Graph#getEdgeFactory()
     */
    @Override public EdgeFactory<Operation, Invocation> getEdgeFactory()
    {
        return edgeFactory;
    }

    /**
     * @see Graph#getEdgeSource(Object)
     */
    @Override public Operation getEdgeSource(Invocation Invocation)
    {
        return specifics.getEdgeSource(Invocation);
    }

    /**
     * @see Graph#getEdgeTarget(Object)
     */
    @Override public Operation getEdgeTarget(Invocation Invocation)
    {
        return specifics.getEdgeTarget(Invocation);
    }

    /**
     * @see Graph#getEdgeWeight(Object)
     */
    @Override public double getEdgeWeight(Invocation Invocation)
    {
//        if (Invocation instanceof DefaultWeightedEdge) {
//            return ((DefaultWeightedEdge) Invocation).getWeight();
//        } else
        if (Invocation == null) {
            throw new NullPointerException();
        } else {
            return WeightedGraph.DEFAULT_EDGE_WEIGHT;
        }
    }

    /**
     * Gets the outgoing.
     *
     * @param index the index
     * @param sourceVertex the source vertex
     * @return the outgoing
     */
    public Invocation getOutgoing(int index, Operation sourceVertex) {
        return null;
    }

    /**
     * Gets the specifics.
     *
     * @return the specifics
     */
    public ExecutionPlanSpecifics getSpecifics() {
        return specifics;
    }

    /**
     * @see DirectedGraph#incomingEdgesOf(Object)
     */
    public Set<Invocation> incomingEdgesOf(Operation vertex)
    {
        assertVertexExist(vertex);
        return specifics.incomingEdgesOf(vertex);
    }

    /**
     * @see DirectedGraph#inDegreeOf(Object)
     */
    public int inDegreeOf(Operation vertex)
    {
        assertVertexExist(vertex);
        return specifics.inDegreeOf(vertex);
    }

    @Override
    public int indexOfOutgoing(Invocation edge)
    {
        return specifics.indexOfOutgoing(edge);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.executionplan.ExecutionPlanGraph#indexOfOutgoing(org.eclipse.qvtd.compiler.Operation, org.eclipse.qvtd.compiler.Invocation)
     */
    @Override
    public int indexOfOutgoing(Operation sourceVertex, Invocation edge)
    {
        assertVertexExist(sourceVertex);
        return specifics.indexOfOutgoing(sourceVertex, edge);
    }

    /**
     * @see DirectedGraph#outDegreeOf(Object)
     */
    public int outDegreeOf(Operation vertex)
    {
        assertVertexExist(vertex);
        return specifics.outDegreeOf(vertex);
    }

    /**
     * @see DirectedGraph#outgoingEdgesOf(Object)
     */
    public Set<Invocation> outgoingEdgesOf(Operation vertex)
    {
        assertVertexExist(vertex);
        return specifics.outgoingEdgesOf(vertex);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.executionplan.ExecutionPlanGraph#outgoingListEdgesOf(org.eclipse.qvtd.compiler.Operation)
     */
    @Override
    public List<Invocation> outgoingListEdgesOf(Operation vertex)
    {
        assertVertexExist(vertex);
        return specifics.outgoingListEdgesOf(vertex);
    }

    /**
     * @see Graph#removeEdge(Object)
     */
    @Override public boolean removeEdge(Invocation Invocation)
    {
        if (containsEdge(Invocation)) {
            specifics.removeEdgeFromTouchingVertices(Invocation);

            return true;
        } else {
            return false;
        }
    }


    /**
     * @see Graph#removeEdge(Object, Object)
     */
    @Override public Invocation removeEdge(Operation sourceVertex, Operation targetVertex)
    {
        Invocation Invocation = getEdge(sourceVertex, targetVertex);

        if (Invocation != null) {
            specifics.removeEdgeFromTouchingVertices(Invocation);
        }

        return Invocation;
    }

    /**
     * @see Graph#removeVertex(Object)
     */
    @Override public boolean removeVertex(Operation Operation)
    {
        if (containsVertex(Operation)) {
            Set<Invocation> touchingEdgesList = edgesOf(Operation);

            // cannot iterate over list - will cause
            // ConcurrentModificationException
            removeAllEdges(new ArrayList<Invocation>(touchingEdgesList));

            specifics.removeVertex(Operation); // remove the vertex itself

            return true;
        } else {
            return false;
        }
    }

    /**
     * @see Graph#vertexSet()
     */
    @Override public Set<Operation> vertexSet()
    {
        if (unmodifiableVertexSet == null) {
            unmodifiableVertexSet =
                Collections.unmodifiableSet(specifics.getVertexSet());
        }

        return unmodifiableVertexSet;
    }

    /**
     * Creates the specifics.
     *
     * @return the execution plan specifics
     */
    private ExecutionPlanSpecifics createSpecifics()
    {
        return createExecutionPlanSpecifics();
    }

    @Override
    public boolean assertVertexExist(Operation v)
    {
        if (containsVertex(v)) {
            return true;
        } else if (v == null) {
            throw new NullPointerException();
        } else {
            throw new IllegalArgumentException(
                "no such vertex in graph: " + v.toString());
        }
    }

//    /**
//     * @see WeightedGraph#setEdgeWeight(Object, double)
//     */
//    public void setEdgeWeight(Invocation Invocation, double weight)
//    {
//        assert (Invocation instanceof DefaultWeightedEdge) : Invocation.getClass();
//        ((DefaultWeightedEdge) Invocation).weight = weight;
//    }

    /**
     * Creates the execution plan specifics.
     *
     * @return the execution plan specifics
     */
    protected ExecutionPlanSpecifics createExecutionPlanSpecifics()
    {
        return new ExecutionPlanSpecificsImpl();
    }

    /**
     * Operation to dot.
     *
     * @param graphId the graph id
     * @param op the op
     * @return the string
     */
    private String operationToDOT(int graphId, Operation op) {
        StringBuilder nodeSb = new StringBuilder();
        nodeSb.append("\t");
        nodeSb.append(getOperationGlobalId(op, graphId));
        //nodeSb.append(nodeIds.getVertexName(op));
        nodeSb.append("[label = \"{<h> ");
        nodeSb.append(op.toString());
        if (outDegreeOf(op) > 0) {
            nodeSb.append(" | {");
            String sep = "";
            for (int index = 0; index < outDegreeOf(op); index++) {
                nodeSb.append(sep);
                nodeSb.append("<i");
                nodeSb.append(index);
                nodeSb.append("> ");
                //nodeSb.append(rc.getCost());
                sep = "| ";
            }
            nodeSb.append(" }");
        }
        nodeSb.append(" }\"];\n");
        return nodeSb.toString();
    }

    /**
     * Invocation to dot.
     *
     * @param graphId the graph id
     * @param i the i
     * @return the string
     */
    private String invocationToDOT(int graphId, Invocation i) {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\"");
        sb.append(getOperationGlobalId(i.getSource(), graphId));
        sb.append("\":i");
        int id = 0;
        for (Invocation li : outgoingEdgesOf(i.getSource())) {
            if (li.equals(i))
                break;
            id++;
        }
        sb.append(id);
        sb.append(":s");
        sb.append(" -> ");
        sb.append("\"");
        sb.append(getOperationGlobalId(i.getTarget(), graphId));
        sb.append("\":h:n");
        if (i instanceof CallActionInvocation) {
            sb.append("[label=\"");
            sb.append(((CallActionInvocation) i).getBindingsAsString());
            sb.append("\"]");
        }

        sb.append(";\n");
        return sb.toString();
    }

    /**
     * Gets the operation global id, used for assigning it a unique id when
     * generating the DOT representation
     *
     * @param op the operation
     * @param graphId the graph id
     * @return the operation global id
     */
    private String getOperationGlobalId(Operation op, int graphId) {
        StringBuilder nodeSb = new StringBuilder();
        nodeSb.append("o");
        nodeSb.append(graphId);
        nodeSb.append("_");
        nodeSb.append(nodeIds.getVertexName(op));
        return nodeSb.toString();
    }

    /**
     * Get a DOT representation of the plan. The default graph id is 0
     *
     * @return the string
     */
    public String toDOT() {
        return toDOT(0, false);
    }


    /**
     * Get a DOT representation of the plan using the given graph id
     *
     * @param graphId the graph id
     * @return the string
     */
    public String toDOT(int graphId) {
        return toDOT(graphId, false);
    }


    /**
     * Get a DOT representation of the plan using the given graph id and
     * optionally make this graph a subgraph
     *
     * @param graphId the graph id
     * @param asSubhraph Set to True to make this graph a subgraph
     * @return the string
     */
    public String toDOT(int graphId, boolean asSubhraph) {
        String type = "digraph ";
        if (asSubhraph)
            type = "subgraph ";
        StringBuilder sb = new StringBuilder();
        sb.append(type);
        if (asSubhraph)
            sb.append("cluster");
        sb.append(graphId);
        sb.append(" {\n");
        if (asSubhraph)
            sb.append("\tlabel = "+ getCost() +  ";\n");
        sb.append("\tnode [shape = record,height=.1];\n");
        for (Operation n : vertexSet()) {
            sb.append(operationToDOT(graphId, n));
            //Add the Edges
            for (Invocation i : outgoingEdgesOf(n)) {
                sb.append(invocationToDOT(graphId, i));
            }
        }
        //Add all the Edges
        //for (Invocation i : edgeSet()) {
        //    sb.append(invocationToDOT(graphId, i));
        //}
        sb.append("}\n");
        return sb.toString();
    }

    /**
     * A factory for creating ExecutionPlanEdge objects.
     */
    public class ExecutionPlanEdgeFactory
            implements EdgeListFactory<Operation, Invocation>,
                        Serializable {

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = -6774863851783264869L;

        @Override
        public List<Invocation> createEdgeList(Operation vertex) {
            return new ArrayList<Invocation>();
        }

    }


    /**
     * A factory for creating EdgeList objects.
     *
     * @param <V> the value type
     * @param <E> the element type
     */
    private interface EdgeListFactory<V, E> {

        /**
         * Creates a new EdgeList object.
         *
         * @param vertex the vertex
         * @return the list< e>
         */
        public List<E> createEdgeList(V vertex);
    }

    /**
     * A container for vertex edges.
     *
     * <p>In this edge container we use array lists to minimize memory toll.
     * However, for high-degree vertices we replace the entire edge container
     * with a direct access subclass (to be implemented).</p>
     *
     * @author Barak Naveh
     * @param <V> the value type
     * @param <E> the element type
     */
    protected static class ExecutionPlanEdgeContainer
        implements Serializable
    {

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 7494242245729767106L;

        /** The incoming. */
        List<Invocation> incoming;

        /** The outgoing. */
        List<Invocation> outgoing;

        /**
         * Instantiates a new directed edge container.
         *
         * @param edgeListFactory the edge list factory
         * @param vertex the vertex
         */
        ExecutionPlanEdgeContainer(EdgeListFactory<Operation, Invocation> edgeListFactory,
                Operation vertex)
        {
            incoming = edgeListFactory.createEdgeList(vertex);
            outgoing = edgeListFactory.createEdgeList(vertex);
        }


        public void addIncomingEdge(Invocation e)
        {
            incoming.add(e);
        }


        public void addOutgoingEdge(int index, Invocation e)
        {
            outgoing.add(index, e);
        }


        public void addOutgoingEdge(Invocation e)
        {
            outgoing.add(e);
        }


        public int indexOfOutgoing(Invocation e) {
            return outgoing.indexOf(e);
        }


        public void removeIncomingEdge(Invocation e)
        {
            incoming.remove(e);
        }


        public void removeOutgoingEdge(Invocation e)
        {
            outgoing.remove(e);
        }
    }

    /**
     * An interface to provide partial support of the Graph API intended to allow implementation of fast access to
     * vertex adjacent edges.
     *
     * @author Barak Naveh
     * @param <Operation> the value type
     * @param <Invocation> the element type
     */
    interface ExecutionPlanSpecifics
        extends Serializable
    {

        /**
         * Adds the specified edge to the edge containers of its source and
         * target vertices.
         *
         * @param e the e
         */
        public void addEdgeToTouchingVertices(Invocation e);

        /**
         * Adds the edge to touching vertices.
         * @param index the index
         * @param e the e
         */
        public void addEdgeToTouchingVertices(int index, Invocation e);

        /**
         * Adds the vertex.
         *
         * @param v the v
         */
        public void addVertex(Operation v);

        /**
         * Contains edge.
         *
         * @param e the e
         * @return true, if successful
         */
        public boolean containsEdge(Invocation e);

        /**
         * Degree of.
         *
         * @param v the v
         * @return the int
         * @see UndirectedGraph#degreeOf(Object)
         */
        public int degreeOf(Operation v);

        /**
         * Edge set.
         *
         * @return the sets the
         */
        public Set<Invocation> edgeSet();

        /**
         * Edges of.
         *
         * @param v the v
         * @return the sets the
         * @see Graph#edgesOf(Object)
         */
        public Set<Invocation> edgesOf(Operation v);

        /**
         * Gets the all edges.
         *
         * @param sourceVertex the source vertex
         * @param targetVertex the target vertex
         * @return the all edges
         * @see Graph#getAllEdges(Object, Object)
         */
        public Set<Invocation> getAllEdges(Operation sourceVertex, Operation targetVertex);

        /**
         * Gets the edge.
         *
         * @param sourceVertex the source vertex
         * @param targetVertex the target vertex
         * @return the edge
         * @see Graph#getEdge(Object, Object)
         */
        public Invocation getEdge(Operation sourceVertex, Operation targetVertex);

        /**
         * Gets the edge source.
         *
         * @param e the e
         * @return the edge source
         */
        public Operation getEdgeSource(Invocation e);

        /**
         * Gets the edge target.
         *
         * @param e the e
         * @return the edge target
         */
        public Operation getEdgeTarget(Invocation e);

        /**
         * Gets the vertex set.
         *
         * @return the vertex set
         */
        public abstract Set<Operation> getVertexSet();

        /**
         * Incoming edges of.
         *
         * @param v the v
         * @return the sets the
         * @see DirectedGraph#incomingEdgesOf(Object)
         */
        public Set<Invocation> incomingEdgesOf(Operation v);

        /**
         * In degree of.
         *
         * @param v the v
         * @return the int
         * @see DirectedGraph#inDegreeOf(Object)
         */
        public int inDegreeOf(Operation v);

        /**
         * Index of outgoing.
         *
         * @param edge the edge
         * @return the int
         */
        public int indexOfOutgoing(Invocation edge);

        /**
         * Index of outgoing.
         *
         * @param sourceVertex the source vertex
         * @param edge the edge
         * @return the int
         */
        public int indexOfOutgoing(Operation sourceVertex, Invocation edge);

        /**
         * Out degree of.
         *
         * @param v the v
         * @return the int
         * @see DirectedGraph#outDegreeOf(Object)
         */
        public int outDegreeOf(Operation v);

        /**
         * Outgoing edges of.
         *
         * @param v the v
         * @return the sets the
         * @see DirectedGraph#outgoingEdgesOf(Object)
         */
        public Set<Invocation> outgoingEdgesOf(Operation v);

        /**
         * Outgoing list edges of.
         *
         * @param v the v
         * @return the list
         */
        public List<Invocation> outgoingListEdgesOf(Operation v);

        /**
         * Removes the specified edge from the edge containers of its source and
         * target vertices.
         *
         * @param e the e
         */
        public void removeEdgeFromTouchingVertices(Invocation e);

        /**
         * Removes the vertex.
         *
         * @param v the v
         */
        public void removeVertex(Operation v);
    }

    /**
     * The Class DirectedSpecifics.
     */
    protected class ExecutionPlanSpecificsImpl
        implements ExecutionPlanSpecifics
    {

        /** The Constant serialVersionUID. */
        private static final long serialVersionUID = 8971725103718958232L;

        /** The Constant NOT_IN_DIRECTED_GRAPH. */
        static final String NOT_IN_DIRECTED_GRAPH =
            "no such operation in a directed graph";

        /** The vertex map directed. */
        private Map<Operation, ExecutionPlanEdgeContainer> vertexMapDirected;

        /** The edge set. */
        protected Set<Invocation> edgeSet;

        /**
         * Instantiates a new directed specifics.
         */
        public ExecutionPlanSpecificsImpl()
        {
            this(new LinkedHashMap<Operation, ExecutionPlanEdgeContainer>());
        }

        /**
         * Instantiates a new directed specifics.
         *
         * @param vertexMap the vertex map
         */
        public ExecutionPlanSpecificsImpl(Map<Operation, ExecutionPlanEdgeContainer> vertexMap)
        {
            this.vertexMapDirected = vertexMap;
            edgeSet = new HashSet<Invocation>();
        }

        @Override
        public void addEdgeToTouchingVertices(int index, Invocation Invocation) {
            edgeSet.add(Invocation);
            Operation source = getEdgeSource(Invocation);
            Operation target = getEdgeTarget(Invocation);
            getEdgeContainer(source).addOutgoingEdge(index, Invocation);
            getEdgeContainer(target).addIncomingEdge(Invocation);

        }

        @Override public void addEdgeToTouchingVertices(Invocation Invocation)
        {
            edgeSet.add(Invocation);
            Operation source = getEdgeSource(Invocation);
            Operation target = getEdgeTarget(Invocation);
            getEdgeContainer(source).addOutgoingEdge(Invocation);
            getEdgeContainer(target).addIncomingEdge(Invocation);
        }

        @Override public void addVertex(Operation Operation)
        {
            // add with a lazy edge container entry
            vertexMapDirected.put(Operation, null);
        }

        @Override
        public boolean containsEdge(Invocation Invocation) {
            return edgeSet.contains(Invocation);
        }

        /**
         * @see UndirectedGraph#degreeOf(Object)
         */
        @Override public int degreeOf(Operation vertex)
        {
            throw new UnsupportedOperationException(NOT_IN_DIRECTED_GRAPH);
        }

        @Override
        public Set<Invocation> edgeSet() {
            if (unmodifiableEdgeSet == null) {
                unmodifiableEdgeSet = Collections.unmodifiableSet(edgeSet);
            }

            return unmodifiableEdgeSet;
        }

        /**
         * @see Graph#edgesOf(Object)
         */
        @Override public Set<Invocation> edgesOf(Operation vertex)
        {
            ArrayUnenforcedSet<Invocation> inAndOut =
                new ArrayUnenforcedSet<Invocation>(getEdgeContainer(vertex).incoming);
            inAndOut.addAll(getEdgeContainer(vertex).outgoing);

            Set<Invocation> loops = getAllEdges(vertex, vertex);

            for (int i = 0; i < inAndOut.size();) {
                Object Invocation = inAndOut.get(i);

                if (loops.contains(Invocation)) {
                    inAndOut.remove(i);
                    loops.remove(Invocation); // so we remove it only once
                } else {
                    i++;
                }
            }
            return Collections.unmodifiableSet(inAndOut);
        }

        /**
         * @see Graph#getAllEdges(Object, Object)
         */
        @Override public Set<Invocation> getAllEdges(Operation sourceVertex, Operation targetVertex)
        {
            Set<Invocation> edges = null;

            if (containsVertex(sourceVertex) && containsVertex(targetVertex)) {
                ExecutionPlanEdgeContainer ec = getEdgeContainer(sourceVertex);
                edges = ec.outgoing.stream()
                        .filter(e -> e.getTarget().equals(targetVertex))
                        .collect(Collectors.toCollection(LinkedHashSet::new));
            }
            return edges;
        }

        /**
         * @see Graph#getEdge(Object, Object)
         */
        @Override public Invocation getEdge(Operation sourceVertex, Operation targetVertex)
        {
            if (containsVertex(sourceVertex) && containsVertex(targetVertex)) {
                ExecutionPlanEdgeContainer ec = getEdgeContainer(sourceVertex);

                try {
                    return ec.outgoing.stream()
                            .filter(e -> e.getTarget().equals(targetVertex))
                            .findFirst()
                            .get();
                } catch (NoSuchElementException e) {
                    // No edge connecting the vertices
                }
            }
            return null;
        }

        @Override
        public Operation getEdgeSource(Invocation Invocation) {
            return Invocation.getSource();
        }

        @Override public Operation getEdgeTarget(Invocation invocation)
        {
            return invocation.getTarget();
        }

        @Override public Set<Operation> getVertexSet()
        {
            return vertexMapDirected.keySet();
        }

        /**
         * @see DirectedGraph#incomingEdgesOf(Object)
         */
        @Override public Set<Invocation> incomingEdgesOf(Operation vertex)
        {
            return new LinkedHashSet<Invocation>(getEdgeContainer(vertex).incoming);
        }

        /**
         * @see DirectedGraph#inDegreeOf(Object)
         */
        @Override public int inDegreeOf(Operation vertex)
        {
            return getEdgeContainer(vertex).incoming.size();
        }



        @Override
        public int indexOfOutgoing(Invocation edge) {
            Operation source = edge.getSource();
            ExecutionPlanEdgeContainer ec = getEdgeContainer(source);
            return ec.outgoing.indexOf(edge);
        }

        @Override
        public int indexOfOutgoing(Operation sourceVertex, Invocation edge) {
            ExecutionPlanEdgeContainer ec = getEdgeContainer(sourceVertex);
            return ec.outgoing.indexOf(edge);
        }

        /**
         * @see DirectedGraph#outDegreeOf(Object)
         */
        @Override public int outDegreeOf(Operation vertex)
        {
            return getEdgeContainer(vertex).outgoing.size();
        }

        /**
         * @see DirectedGraph#outgoingEdgesOf(Object)
         */
        @Override public Set<Invocation> outgoingEdgesOf(Operation vertex)
        {
            return new LinkedHashSet<Invocation>(getEdgeContainer(vertex).outgoing);
        }

        @Override
        public List<Invocation> outgoingListEdgesOf(Operation vertex) {

            return Collections.unmodifiableList(getEdgeContainer(vertex).outgoing);
        }

        @Override public void removeEdgeFromTouchingVertices(Invocation Invocation)
        {
            Operation source = getEdgeSource(Invocation);
            Operation target = getEdgeTarget(Invocation);

            getEdgeContainer(source).removeOutgoingEdge(Invocation);
            getEdgeContainer(target).removeIncomingEdge(Invocation);
            edgeSet.remove(Invocation);
        }

        @Override
        public void removeVertex(Operation Operation) {
            vertexMapDirected.keySet().remove(Operation);
        }

        /**
         * A lazy build of edge container for specified vertex.
         *
         * @param vertex a vertex in this graph.
         *
         * @return EdgeContainer
         */
        protected ExecutionPlanEdgeContainer getEdgeContainer(Operation vertex)
        {
            assertVertexExist(vertex);

            ExecutionPlanEdgeContainer ec = vertexMapDirected.get(vertex);

            if (ec == null) {
                ec = new ExecutionPlanEdgeContainer(edgeListFactory, vertex);
                vertexMapDirected.put(vertex, ec);
            }

            return ec;
        }

    }

}
