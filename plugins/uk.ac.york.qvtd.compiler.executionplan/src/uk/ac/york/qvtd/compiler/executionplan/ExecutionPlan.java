/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/

package uk.ac.york.qvtd.compiler.executionplan;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Variable;
import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.Graphs;
import org.jgrapht.graph.GraphPathImpl;
import org.jgrapht.traverse.BreadthFirstIterator;
import org.jgrapht.util.TypeUtil;

import uk.ac.york.qvtd.compiler.Binding;
import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.bindings.DerivedVariable;
import uk.ac.york.qvtd.compiler.bindings.DirectBinding;
import uk.ac.york.qvtd.compiler.bindings.LocalBinding;
import uk.ac.york.qvtd.compiler.bindings.LoopBinding;
import uk.ac.york.qvtd.compiler.bindings.NavigationBinding;
import uk.ac.york.qvtd.compiler.bindings.utilities.BindingsUtilities;
import uk.ac.york.qvtd.compiler.executionplan.utilities.ExecutionPlanUtils;
import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.DependencyPath;
import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.inter.impl.DependencyPathImpl;
import uk.ac.york.qvtd.dependencies.util.Pair;

/**
 * The execution plan represents one alternative of invoking the rules in a
 * transformation. The plan is represented by a set of PlanRules.
 * @author Horacio Hoyos
 *
 */
public class ExecutionPlan extends AbstractExecutionPlanGraph
    implements ExecutionPlanGraph {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6879235496193391745L;

    /** The cost of the plan. */
    private double cost = 0;

    /** The root call action operation. */
    private CallActionOperation root = null;

    /** The dependency graph. */
    private DependencyGraph dependencyGraph;

    /** Map mapping vertex to its associated call action operation. */
    private Map<MappingAction, CallActionOperation> mappingToOperation = new Hashtable<MappingAction, CallActionOperation>();


    public ExecutionPlan() {
        super(new InvocationFactory());
    }

    public ExecutionPlan(boolean reuseInvocations) {
        super(new InvocationFactory(reuseInvocations));
    }

    /**
     * Creates a new execution plan for the given dependency graph. The
     * dependency graph is considered a view of the transformation.
     * The dependency graph must have a node with no incoming edges that
     * represents to root/initial rule of the transformation.
     * A DirectedNeighborIndex is created for the graph.
     *
     * @param g the dependency graph
     */
    public ExecutionPlan(DependencyGraph g, CallActionOperation root, boolean reuse) {
        this(reuse);
        this.setDependencyGraph(g);
        //root = new CallActionOperation(g.getRootVertex(), true);
        this.root = root;
        getMappingToOperation().put(g.getRootVertex(), root);
        addVertex(root);
    }

    /**
     * Creates a new execution plan for the given dependency graph. The
     * dependency graph is considered a view of the transformation.
     * The dependency graph must have a node with no incoming edges that
     * represents to root/initial rule of the transformation.
     * A DirectedNeighborIndex is created for the graph.
     *
     * @param g the dependency graph
     */
    public ExecutionPlan(DependencyGraph g, CallActionOperation root) {
        this(g, root, true);
    }


    public ExecutionPlan(ExecutionPlan other) {
        this(other, false);
    }

    /**
     * Copy Constructor, if copy operations is true, the operations are not reused. This can be set to false until
     * starting so add bindings, in which is not a good idea to share operations, unless you know what you
     * are doing.
     */
    public ExecutionPlan(ExecutionPlan other, boolean copyOperations) {
        super(new InvocationFactory(!copyOperations));
        cost = other.cost;
        setDependencyGraph(other.getDependencyGraph());
        Map<Operation, Operation> otherToNewOp = new HashMap<Operation, Operation>();
        Map<Variable, Variable> otherToNewLocalVar = new HashMap<Variable, Variable>();
        // Copy CallActionInvocations first to have references to local variables
        List<Operation> callOps = other.vertexSet().stream().filter(o -> o instanceof CallActionOperation).collect(Collectors.toList());
        List<Operation> loopOps = other.vertexSet().stream().filter(o -> !(o instanceof CallActionOperation)).collect(Collectors.toList());
        for (Operation op : callOps) {
            CallActionOperation newOp = (CallActionOperation) op;
            if (copyOperations) {
                newOp = new CallActionOperation((CallActionOperation)op);
                // Keep variable trace for later use
                for (DerivedVariable otherlv : ((CallActionOperation) op).getLocalVariables()) {
                    for (DerivedVariable lv : newOp.getLocalVariables()) {
                        // The name holds the source variable and property "id" (name)
                        if (otherlv.getVariable().getName().equals(lv.getVariable().getName())) {
                            otherToNewLocalVar.put(otherlv.getVariable(), lv.getVariable());
                        }
                    }
                }

            }
            otherToNewOp.put(op, newOp);
            addVertex(newOp);
        }
        for (Operation op : loopOps) {
            LoopOperation newOp = (LoopOperation) op;
            if (copyOperations) {
                if (op instanceof AllInstLoop) {
                    AllInstLoop ail = (AllInstLoop) op;
                    newOp = new AllInstLoop(ail);
                    otherToNewLocalVar.put(ail.getLoopVariable(), newOp.getLoopVariable());
                } else if (op instanceof PropNavLoop) {
                    PropNavLoop pnl = (PropNavLoop) op;
                    if (otherToNewLocalVar.containsKey(pnl.getSourceVariable())) {
                        Variable sourceVar = otherToNewLocalVar.get(pnl.getSourceVariable());
                        String varName = pnl.getLoopVariable().getName();
                        int indexStart = varName.lastIndexOf("_");
                        int index = Integer.valueOf(varName.substring(indexStart+1));
                        newOp = new PropNavLoop(sourceVar,
                                pnl.getLoopVariable().getType(),
                                pnl.getNavigation(),
                                index);
                    } else {
                        newOp = new PropNavLoop(pnl);
                    }
                    otherToNewLocalVar.put(pnl.getLoopVariable(), newOp.getLoopVariable());
                }
                assert newOp != null;
            }
            otherToNewOp.put(op, newOp);
            addVertex(newOp);
        }
        root = (CallActionOperation) otherToNewOp.get(other.root);
        for (MappingAction mv : other.getMappingToOperation().keySet()) {
            Operation oClone = otherToNewOp.get(other.getMappingToOperation().get(mv));
            getMappingToOperation().put(mv, (CallActionOperation) oClone);
        }
        Map<Binding, Binding> otherToNewBind = new HashMap<Binding, Binding>();
        BreadthFirstIterator<Operation, Invocation> itr = new BreadthFirstIterator<Operation, Invocation>(other, root);
        while(itr.hasNext()) {
            Operation otherOp = itr.next();
            for (Invocation otherIn : other.outgoingEdgesOf(otherOp)) {
            Invocation newIn = addEdge(otherToNewOp.get(otherIn.getSource()), otherToNewOp.get(otherIn.getTarget()));
                if (otherIn instanceof CallActionInvocation) {
                    Iterator<Entry<Variable, Binding>> it = ((CallActionInvocation) otherIn).getBindings().entrySet().iterator();
                    while (it.hasNext()) {
                        Binding newBind = null;
                        Map.Entry<Variable, Binding> pair = it.next();
                        Binding otherBind = pair.getValue();
                        if (otherBind instanceof DirectBinding) {
                            newBind = new DirectBinding((DirectBinding) otherBind);
                        } else if (otherBind instanceof LoopBinding) {
                            newBind = BindingsUtilities.createLoopBidning((LoopBinding) otherBind, otherToNewOp);
                        } else if (otherBind instanceof LocalBinding) {
                            // Find the invoking operation
                            Operation otherSource = ExecutionPlanUtils.getInvocationChainSource(otherIn, other);
                            Operation source =  otherToNewOp.get(otherSource);
                            newBind = BindingsUtilities.createLocalBinding((LocalBinding) otherBind, (CallActionOperation) source);
                        } else if (otherBind instanceof NavigationBinding) {
                            NavigationBinding nb = (NavigationBinding) otherBind;
                            Variable sourceVar = otherToNewLocalVar.get(nb.getSourceVariable());
                            if (sourceVar == null){
                                sourceVar = nb.getSourceVariable();	// Is a navigation from a QVTi variable
                            }
//                            else {
//                                System.out.println("?");
//                            }
                            assert sourceVar != null;
                            newBind = BindingsUtilities.createNavigationBinding(nb, sourceVar);
                        }
                        assert newBind != null;
                        otherToNewBind.put(otherBind, newBind);
                        ((CallActionInvocation) newIn).getBindings().put(pair.getKey(), newBind);
                    }
                    newIn.setPolled(otherIn.isPolled());
                    newIn.setLooped(otherIn.isLooped());
                }
            }
        }
        // Now that bidings exist, fix the associated loop bindings
        for (Operation op : other.vertexSet()) {
            if (op instanceof LoopOperation)
                for (NavigationBinding b : ((LoopOperation) op).getAssociatedLoopBindings()) {
                    ((LoopOperation) otherToNewOp.get(op)).getAssociatedLoopBindings()
                        .add((NavigationBinding) otherToNewBind.get(b));
                }
        }
    }

    /**
     * For multiple bound variables for a given ModelType, all the initial
     * invocations should be added at the same time, other wise the initial
     * invocation is removed and subsequent calls to insert invocation fail.
     * @param boundVars
     * @param mt
     * @param invocation
     * @return
     */
//    @Deprecated
//    private Map<Variable, Invocation> createStartInvocations(List<Variable> boundVars, ModelType mt, Invocation invocation) {
//        Map<Variable, Invocation> result = new HashMap<Variable, Invocation>();
//        AllInstLoop la;
//        for (Variable bv : boundVars) {
//            la = new AllInstLoop(mt, 0);
//            addVertex(la);
//            addEdge(invocation.getSource(), la);
//            Invocation lastInvocation = addEdge(la, invocation.getTarget());
//            result.put(bv, lastInvocation);
//        }
//        removeEdge(invocation);
//        return result;
//    }

    /**
     * Indicates whether some other object is "equal to" this Execution plan.
     * This is a small modification of the JGrapht implementation regarding that
     * our
     * @param obj object to be compared for equality with this graph
     *
     * @return <code>true</code> if the specified object is equal to this graph
     *
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        TypeUtil<DirectedGraph<Operation, Invocation>> typeDecl = null;
        DirectedGraph<Operation, Invocation> g = TypeUtil.uncheckedCast(obj, typeDecl);

        if (!vertexSet().equals(g.vertexSet())) {
            return false;
        }
        if (edgeSet().size() != g.edgeSet().size()) {
            return false;
        }
        Iterator<Operation> thisOpIt = vertexSet().iterator();
        Iterator<Operation> thatOpIt = g.vertexSet().iterator();
        while (thisOpIt.hasNext() && thatOpIt.hasNext()) {
            Operation thisOp = thisOpIt.next();
            Operation thatOp = thatOpIt.next();
            if (outDegreeOf(thisOp) != outDegreeOf(thatOp)) {
                return false;
            }
            Iterator<Invocation> thisInvIt = outgoingEdgesOf(thisOp).iterator();
            Iterator<Invocation> thatInvIt = g.outgoingEdgesOf(thatOp).iterator();
            while (thisInvIt.hasNext() && thatInvIt.hasNext()) {
                Invocation thisE = thisInvIt.next();
                Invocation thatE = thatInvIt.next();
                Operation thisT = getEdgeTarget(thisE);
                Operation thatT = g.getEdgeTarget(thatE);
                if (!thisT.equals(thatT)) {
                    return false;
                }
                if (Math.abs(getEdgeWeight(thisE) - g.getEdgeWeight(thatE)) > 10e-7) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Gets the cost.
     *
     * @return the cost
     */
    @Override
    public double getCost() {
        return cost;
    }

    /**
     * Gets the dependency graph.
     *
     * @return the dependency graph
     */
    @Override
    public DependencyGraph getDependencyGraph() {
        return dependencyGraph;
    }

    /**
     * Gets the mapping to operation map.
     *
     * @return the mapping to operation
     */
    @Override
    public Map<MappingAction, CallActionOperation> getMappingToOperation() {
        return mappingToOperation;
    }

    /**
     * Gets the neighbour index.
     *
     * @return the neighbour index
     */
//    public DirectedNeighborIndex<Operation, Invocation> getNeighborIndex() {
//        return neighbourIndex;
//    }

    /**
     * Gets the associated operation for a given mapping.
     *
     * @param mapping the mapping
     * @return the operation for mapping
     */
    @Override
    public @Nullable CallActionOperation getOperationForMapping(MappingAction mapping) {
        return getMappingToOperation().get(mapping);
    }

    @Override
    public boolean addOperationForMapping(MappingAction mapping, CallActionOperation operation) {
        getMappingToOperation().put(mapping, operation);
        return addVertex(operation);
    }




    /**
     * Gets the paths ending on the given operation. A path is a list of MappingActions.
     *
     * Since we are interested in
     * the other CallActions visited, not the actual different paths, we filter
     * the paths so we only leave 1 path for each set of visited vertices.
     *
     * @param operation the operation where paths end
     * @return the paths ending on
     */
    @Override
    public Set<List<MappingAction>> getPathsFromRoot(
            CallActionOperation operation) {

        return getDependencyGraph().getReducedPathsFromRoot(operation.getAction());
    }

    /**
     * Gets the predecessor actions.
     *
     * @param op the operation
     * @return the predecessor actions
     */
    @Override
    public Set<MappingAction> getPredecessorActions(CallActionOperation op) {
        return getDependencyGraph().getPredecessorActions(op.getAction());
    }


    /**
     * Gets the root operation.
     *
     * @return the root
     */
    @Override
    public CallActionOperation getRoot() {

        return root;
    }


    /**
     * Returns a hash code value for this graph. The hash code of a graph is
     * defined to be the sum of the hash codes of vertices and edges in the
     * graph. It is also based on graph topology and edges weights.
     *
     * @return the hash code value this graph
     *
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        int hash = 1;
        int prime = 31;
        for (Operation o : vertexSet()) {
            int part = o.hashCode();
            for (Invocation e : outgoingEdgesOf(o)) {
                // see http://en.wikipedia.org/wiki/Pairing_function (VK);
                int x = e.hashCode();
                int y = e.getTarget().hashCode();
                int z = ((x + y + 1) * (x + y))/2 + y;
                part = (prime * part) + z;
            }
            // Adapted from AbstractList to take into consideration edge order!
            hash += part;
        }
        return hash;
    }

//    /**
//     * @param operation
//     * @param lastInvocation
//     * @return
//     */
//    // TODO Removing the edge changes the order, however, since they are removed and added
//    // in the same order, at the end the order should be preserved
//    @Deprecated
//    private Invocation insertInvocation(Operation operation, Invocation lastInvocation) {
//        addVertex(operation);
//        addEdge(lastInvocation.getSource(), operation);
//        removeEdge(lastInvocation);
//        lastInvocation = addEdge(operation, lastInvocation.getTarget());
//        return lastInvocation;
//    }


    /**
     * Return true if the operation is invoked in the plan
     * @param targetOp
     * @return
     */
    @Override
    public boolean isScheduled(CallActionOperation targetOp) {

        return !incomingEdgesOf(targetOp).isEmpty();
    }

    @Override
    public boolean isScheduled(MappingAction mapping) {
        CallActionOperation op = getMappingToOperation().get(mapping);
        if (op != null) {
            return isScheduled(op);
        }
        return false;
    }


    /**
     * Sets the cost.
     *
     * @param cost the new cost
     */
    @Override
    public void setCost(double cost) {
        this.cost = cost;
    }


    /**
     * Sets the dependency graph.
     *
     * @param dependencyGraph the new dependency graph
     */
    @Override
    public void setDependencyGraph(DependencyGraph dependencyGraph) {
        this.dependencyGraph = dependencyGraph;
    }


    /**
     * Sets the root operation.
     *
     * @param root the new root
     */
    @Override
    public void setRoot(CallActionOperation root) {

        assert root.isRoot();
        this.root = root;
    }


//    private void setMappingToOperation(Map<MappingAction, CallActionOperation> mappingToOperation) {
//        this.mappingToOperation = mappingToOperation;
//    }

    // TODO Why did we remove this?
    //@Override
    //public boolean assertVertexExist(Operation v) {
    //    return false;
    //}

    /** We can use all the paths in the ep for cost evaluation
     *
     *
     *
     *
     */

    public static final int N = 100;
    public static final int MP = 10;
    public static final int NAVIGATION_COST = 1;
    public static final double IMPLICIT_NAVIGATION_COST = 1.2;

    private static final long VARIABLE_ASSIGNMENT_COST = 1;
    private final long DIRECT_BINDING_COST = 1;				// 1 because it is recalculated for each loop

    private class ExecutionPath implements GraphPath<Operation, Invocation> {

        private ExecutionPlan plan;
        private List<Invocation> edgeList;
        private List<Invocation> unmodifiableEdgeList = null;

        private Operation startVertex;

        private Operation endVertex;

        private double weight = 0;
        private Operation loopVertex = null;

        public ExecutionPath(ExecutionPlan executionPlan, Invocation e) {
            plan = executionPlan;
            edgeList = new ArrayList<Invocation>();
            edgeList.add(e);
            startVertex = e.getSource();
            endVertex = e.getTarget();
        }

        public ExecutionPath(ExecutionPlan executionPlan, List<Invocation> edgeList) {
            plan = executionPlan;
            this.edgeList = new ArrayList<Invocation>(edgeList);
            startVertex = edgeList.get(0).getSource();
            endVertex = edgeList.get(edgeList.size()-1).getTarget();
        }

        @Override
        public Graph<Operation, Invocation> getGraph() {
            return plan;
        }

        @Override
        public Operation getStartVertex() {
            return startVertex;
        }

        @Override
        public Operation getEndVertex() {
            return endVertex;
        }

        @Override
        public List<Invocation> getEdgeList() {
            if (unmodifiableEdgeList == null) {
                unmodifiableEdgeList = Collections.unmodifiableList(edgeList);
            }
            return unmodifiableEdgeList;
        }

        @Override
        public double getWeight() {
            return weight;
        }

        public void calculateWeight() {
            // Calculate the cost of the invocation
            // each all instances loop represents a primary variable
            // Cost = (n ^ #Loops	// Called from root
            //		| n*#Loops       // else
            //		+ binding cost ()
            // All instances
            long h = edgeList.stream()
                    .filter(LoopInvocation.class::isInstance)
                    .filter(li -> li.getTarget() instanceof AllInstLoop)
                    .count();
            // Multi-value properties
            long mp = edgeList.stream()
                    .filter(LoopInvocation.class::isInstance)
                    .filter(li -> li.getTarget() instanceof PropNavLoop)
                    .count();
            double lbc = edgeList.stream()
                    .map(li -> li.getTarget())
                    .filter(PropNavLoop.class::isInstance)
                    .mapToDouble(op -> getLoopNavigationCost((PropNavLoop) op))
                    .sum();
            lbc = 0;
            for (Invocation i : edgeList) {
                if (i.getTarget() instanceof PropNavLoop) {
                    lbc +=  getLoopNavigationCost((PropNavLoop) i.getTarget());
                }
            }
            // The last invocation should be a CallActionInvocation
            double ibc = getDirectBindingCost((CallActionInvocation) edgeList.get(edgeList.size()-1));
            weight = ibc*h + Math.pow(ibc, mp)+lbc;
        }

        public void addEdge(Invocation e) {
            if (!e.getSource().equals(endVertex)) {
                throw new InvalidParameterException("The added edge must extend the path. " + e.getSource() + " does not match "
                        + endVertex);
            }
            edgeList.add(e);
            endVertex = e.getTarget();
            weight = edgeList.size();
        }

        public boolean containsVertex(Operation vertex) {
            return Graphs.getPathVertexList(this).contains(vertex);
        }

        @Override
        public String toString() {
            return edgeList.toString();
        }

        public void setLoopVertex(CallActionOperation loopVertex) {
            this.loopVertex  = loopVertex;
        }

        public boolean isLoop() {
            return loopVertex != null;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + ((edgeList == null) ? 0 : edgeList.hashCode());
            return result;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            ExecutionPath other = (ExecutionPath) obj;
            if (!getOuterType().equals(other.getOuterType()))
                return false;
            if (edgeList == null) {
                if (other.edgeList != null)
                    return false;
            } else if (!edgeList.equals(other.edgeList))
                return false;
            return true;
        }

        private ExecutionPlan getOuterType() {
            return ExecutionPlan.this;
        }



    }

    Map<Pair<CallActionOperation,CallActionOperation>, Set<ExecutionPath>> routes
            = new HashMap<Pair<CallActionOperation, CallActionOperation>, Set<ExecutionPath>>();

    public void calculateCost() {
        findAllPathsBetweenCallActionOperations();
        routes.values().stream()
                .flatMap(ps -> ps.stream())
                .forEach(ExecutionPath::calculateWeight);
        // Cost is the sum of all paths
        cost = routes.values().stream()
                .flatMap(ps -> ps.stream())
                .mapToDouble(m -> m.getWeight())
                .sum();
    }

    private void findAllPathsBetweenCallActionOperations() {
        Set<@NonNull CallActionOperation> vxs = vertexSet().stream()
                .filter(CallActionOperation.class::isInstance)
                .map(CallActionOperation.class::cast)
                .collect(Collectors.toSet());
        List<Pair<CallActionOperation, CallActionOperation>> pairs = Pair.pairCombinations(vxs);
        for (Pair<CallActionOperation, CallActionOperation> pair : pairs) {
            findAllPaths(pair);
        }
    }

    /**
     * Finds all the paths from source to target, following the directed edges
     * in forward direction. WE are only interested in tier one paths.
     * Adapted from: "Sim�es, R., APAC: An exact algorithm for retrieving cycles
     * and paths in all kinds of graphs, T�khne-Revista de Estudos Polit�cnicos,
     * Instituto Polit�cnico do C�vado e do Ave, 2009, 39-55",  to use edges
     * instead of vertices.
     *
     * @param source the source
     * @param target the target
     */
    private void findAllPaths(Pair<CallActionOperation,CallActionOperation> pair) {
        //CallActionOperation source, CallActionOperation target
        Deque<ExecutionPath> pathStack = new LinkedList<ExecutionPath>();
        for (Invocation e : outgoingEdgesOf(pair.first)) {
            ExecutionPath path = new ExecutionPath(this, e);
            pathStack.push(path);
        }
        ExecutionPath path;
        while (!pathStack.isEmpty()) {
            path = pathStack.pop();
            Operation tailVertex = path.endVertex;
            if (!(tailVertex instanceof CallActionOperation)) {		// Limit the paths to map-to-map (i.e. no maps in-beteween)
                for (Invocation outEdge : outgoingEdgesOf(tailVertex)) {
                    ExecutionPath newPath = new ExecutionPath(this, path.getEdgeList());
                    newPath.addEdge(outEdge);
                    if (!path.containsVertex(outEdge.getTarget())) {
                        if (pair.second.equals(outEdge.getTarget())) {
                            addPath(pair, newPath);
                        } else {
                            pathStack.push(newPath);
                        }
                    } else if (outEdge.getTarget() instanceof CallActionOperation) {
                        addCycle(newPath, (CallActionOperation) outEdge.getTarget());
                    }
                }
            }
        }
    }

    /**
     * TODO WE need more than the pair because calls may be repeated with different loops/args...
     * @param pair
     * @param l
     */
    private void addPath(Pair<CallActionOperation, CallActionOperation> pair, ExecutionPath l) {

        Set<ExecutionPath> paths = routes.get(pair);
        if (paths == null) {
            paths = new HashSet<ExecutionPath>();
            routes.put(pair, paths);
        }
        //GraphPath<Operation, Invocation> path = new GraphPathImpl<>(this, pair.first, pair.second, l, weight);
        paths.add(l);

    }

    /**
     * Adds a cycle in the plan
     * @param source
     * @param l
     */
    private void addCycle(ExecutionPath l, CallActionOperation loopVertex) {
        // Reduce the path to the loop
        List<Invocation> edgeList = l.getEdgeList();
        Invocation startEdge = edgeList.stream()
                .filter(e -> e.getSource().equals(loopVertex))
                .findFirst()
                .get();
        Pair<CallActionOperation, CallActionOperation> p = Pair.of(loopVertex, loopVertex);
        List<Invocation> loopEdges = edgeList.subList(edgeList.indexOf(startEdge), edgeList.size());
        ExecutionPath loopPath = new ExecutionPath(this, loopEdges);
        loopPath.setLoopVertex(loopVertex);
        addPath(p, loopPath);
    }

    /**
     * Descend till the next level of CallAction operations to get the direct
     * binding cost
     * @param inv
     * @return
     */
    private double getDirectBindingCost(CallActionInvocation inv) {
        long dbCost = 0l;
        for (Binding b : ((CallActionInvocation) inv).getBindings().values()) {
            if (b instanceof LoopBinding) {
                dbCost += VARIABLE_ASSIGNMENT_COST;
            }
            else if (b instanceof DirectBinding) {
                dbCost += DIRECT_BINDING_COST;
            }
            else if (b instanceof LocalBinding) {
                LocalBinding lb = (LocalBinding) b;
                for (Property p : lb.getDerivedVariable().getNavigation()) {
                    dbCost += p.isIsImplicit()?
                            IMPLICIT_NAVIGATION_COST : NAVIGATION_COST;
                }
            }
            else if (b instanceof NavigationBinding) {
                for (Property p : ((NavigationBinding) b).getNavigation()) {
                    dbCost += p.isIsImplicit()?
                            IMPLICIT_NAVIGATION_COST : NAVIGATION_COST;
                }
            }
        }

//        else {
//            Operation t = inv.getTarget();
//            for (Invocation ninv : outgoingEdgesOf(t)) {
//                dbCost += getDirectBindingCost(ninv);
//            }
//        }
        return dbCost;
    }

    /**
     * Loop binding comes from NavigationBindings associated to the loop
     * @param operation
     * @return
     */
    private double getLoopNavigationCost(PropNavLoop operation) {
        double lbCost = 0l;
        PropNavLoop pnl = (PropNavLoop) operation;
        for (Property p : pnl.getNavigation()) {
            lbCost += p.isIsImplicit()?
                    IMPLICIT_NAVIGATION_COST : NAVIGATION_COST;
        }
//            //lbCost += 1; // 1 for the loop variable
//            List<NavigationBinding> bs =  ((LoopOperation) operation).getAssociatedLoopBindings();
//            for (NavigationBinding b : bs) {
//                for (Property p : b.getNavigation()) {
//                    lbCost += p.isIsImplicit() ? IMPLICIT_NAVIGATION_COST : NAVIGATION_COST;
//                }
//            }

        return lbCost;

    }

    @Override
    public boolean hasMinimumInvocations() {
        List<@NonNull MappingAction> allMappings = getDependencyGraph().vertexSet().stream()
                .filter(MappingAction.class::isInstance)
                .map(MappingAction.class::cast)
                .collect(Collectors.toList());
        List<MappingAction> planMappings = vertexSet().stream()
                .map(v -> ((CallActionOperation)v).getAction())
                .collect(Collectors.toList());
        if (planMappings.containsAll(allMappings) && vertexSet().stream()
                .filter(v -> !v.isRoot())
                .allMatch(v -> inDegreeOf(v) > 0)) {
            return true;
        }
        return false;
    }

}
