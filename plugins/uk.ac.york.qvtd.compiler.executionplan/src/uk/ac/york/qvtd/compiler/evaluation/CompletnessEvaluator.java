/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.evaluation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;

import org.eclipse.ocl.pivot.Variable;
import org.eclipse.qvtd.pivot.qvtcorebase.CoreDomain;
import org.eclipse.qvtd.pivot.qvtcorebase.GuardPattern;
import org.jgrapht.event.ConnectedComponentTraversalEvent;
import org.jgrapht.event.EdgeTraversalEvent;
import org.jgrapht.event.VertexTraversalEvent;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionInvocation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.inter.impl.ClassDatumImpl;
import uk.ac.york.qvtd.dependencies.util.Pair;
import uk.ac.york.qvtd.dependencies.util.QvtcIterator;

/**
 * The Class CompletnessEvaluator allows to identify if an ExecutionPlan is missing calls.
 * An execution listener that keeps tracked of produced and consumed types to verify that the Execution Plan is
 * thorough. An Execution Plan is thorough if all the types produced are consumed for all producer-consumer relations.
 * When checking for consumed types it also validates that the plan does not starve.
 * <p>
 *
 * <p>
 * FIXME Take into consideration predicates to know if there was a complete consumption of types, or it may have been
 * partial, i.e. a guard that filters how many types are consumed
 */
public class CompletnessEvaluator extends AbstractExecutionPlanEvaluator {

    //private Deque<List<Pair<ClassDatum, MappingAction>>> expectedInvocationsStack = new ArrayDeque<List<Pair<ClassDatum, MappingAction>>>();
    private List<Pair<ClassDatum, MappingAction>> expectedInvocationsStack = new ArrayList<Pair<ClassDatum, MappingAction>>();

    private Deque<List<Pair<ClassDatum, MappingAction>>> expectedInvocations = new ArrayDeque<List<Pair<ClassDatum, MappingAction>>>();

    // We need to keep them because we need the binding info
    private Deque<Deque<CallActionInvocation>> invocations = new ArrayDeque<Deque<CallActionInvocation>>();


    public CompletnessEvaluator(ExecutionPlanGraph plan) {
        super(plan);
        valid = false;	// By definition, if the visitor doesn't get to the connectedComponentFinished the plan is not thorough
    }


    /* (non-Javadoc)
     * @see uk.ac.york.qvtd.compiler.evaluation.AbstractExecutionPlanEvaluator#connectedComponentFinished(org.jgrapht.event.ConnectedComponentTraversalEvent)
     */
    @Override
    public void connectedComponentFinished(ConnectedComponentTraversalEvent e) {
        Iterator<Pair<ClassDatum, MappingAction>> it = expectedInvocationsStack.iterator();
        while (it.hasNext()) {
            Pair<ClassDatum, MappingAction> pair = it.next();
            CallActionOperation operation = plan.getOperationForMapping(pair.second);
            // Remove operations that have not been invoked
            if ((operation == null) || (plan.inDegreeOf(operation) == 0)) {
                it.remove();
            }
        }
        valid = expectedInvocationsStack.isEmpty();
    }


    /**
     * Since edges are visited first than nodes, we keep a stack of edges being traversed.
     */
    @Override
    public void edgeTraversed(EdgeTraversalEvent<Invocation> e) {
        Invocation inv = e.getEdge();
        if (inv instanceof CallActionInvocation) {
            Deque<CallActionInvocation> caopInvs = invocations.peek();
            CallActionInvocation cai = (CallActionInvocation) inv;
            caopInvs.push(cai);

        }
    }

    public List<Pair<ClassDatum, MappingAction>> getRemainderTypes() {
        return expectedInvocationsStack;
    }

    /* (non-Javadoc)
     * @see uk.ac.york.qvtd.compiler.evaluation.AbstractExecutionPlanEvaluator#vertexFinished(org.jgrapht.event.VertexTraversalEvent)
     */
    @Override
    public void vertexFinished(VertexTraversalEvent<Operation> e) {
        // Add any missing invocations to my dad
        QvtcIterator epIt = (QvtcIterator) e.getSource();
        CallActionOperation currentOperation = (CallActionOperation) e.getVertex();
        if (!epIt.isLoop(currentOperation )) {
            List<Pair<ClassDatum, MappingAction>> thisExpectedInvocations = expectedInvocationsStack;//.pop();
            List<Pair<ClassDatum, MappingAction>> lastExpected = expectedInvocations.poll();
            if (lastExpected != null) {
                thisExpectedInvocations.addAll(lastExpected);
                //List<Pair<ClassDatum, MappingAction>> parentExpectedInvocations = expectedInvocationsStack.peek();
                //parentExpectedInvocations.addAll(thisExpectedInvocations);
            }
        }
        invocations.pop();
    }


    /**
     * When a vertex is traversed two things happen
     * <ul>
     * <li> It satisfies any producer-consumer relation in the current branch
     * <li> It adds its producer-consumer relations to the required ones.
     * </ul>
     */
    @Override
    public void vertexTraversed(VertexTraversalEvent<Operation> e) {
        if (e.getVertex() instanceof CallActionOperation) {
            CallActionOperation currentOperation = (CallActionOperation) e.getVertex();
            // Remove all expected invocations of the target, regardless of the binding
            List<Pair<ClassDatum, MappingAction>> lastExpected = expectedInvocations.peek();
            if (lastExpected != null) {
                Iterator<Pair<ClassDatum, MappingAction>> it = lastExpected.iterator();
                while (it.hasNext()) {
                    Pair<ClassDatum, MappingAction> ePair = it.next();
                    if (ePair.second.equals(currentOperation.getAction())) {
                        it.remove();
                        //System.out.println(ePair + " matched called to " + currentOperation.getAction().getLabel());
                    }
                }
            }
            // Remove any expected invocations from the visited branches, iif we use a different type for the call
            List<Pair<ClassDatum, MappingAction>> fullExpectedInvocations = expectedInvocationsStack;
            Deque<CallActionInvocation> caopInvs = invocations.peek();
            if (fullExpectedInvocations != null && caopInvs != null) {
                CallActionInvocation cai = caopInvs.pollLast();
                if (cai != null) {
                    // Remove other calls from the stack, if the invocation is from another producer-relation (i.e. using other types)
                    // Find all invocations in the stack for the same target, but different type.
                    for (Variable boundVar : cai.getBindings().keySet()) {
                        // We know all input variables are in guard patterns, and all guard patterns are in domains :)
                        assert boundVar.eContainer() instanceof GuardPattern;
                        GuardPattern gp = (GuardPattern) boundVar.eContainer();
                        ClassDatum caiCalssDatum = new ClassDatumImpl(boundVar.getType(), ((CoreDomain) gp.getArea()).getTypedModel());
                        Iterator<Pair<ClassDatum, MappingAction>> it = fullExpectedInvocations.iterator();
                        while (it.hasNext()) {
                            Pair<ClassDatum, MappingAction> pair = it.next();
                            if (pair.second.equals(currentOperation.getAction()) && !pair.first.equals(caiCalssDatum)) {
                                it.remove();
                                //System.out.println(pair + " matched collecting call to " + currentOperation.getAction().getLabel());
                            }
                        }
                    }
                    if (cai.getSource().isRoot()) {
                        // Remove all missing invocations if invoked from the root
                        Iterator<Pair<ClassDatum, MappingAction>> dIt = expectedInvocationsStack.iterator();
                        while (dIt.hasNext()) {
                            Pair<ClassDatum, MappingAction> pair = dIt.next();
                            if (pair.second.equals(currentOperation.getAction())) {
                                dIt.remove();
                            }
                        }
                    }
                }
            }
            if (currentOperation.isRoot()) {
                //expectedInvocationsStack.push(new ArrayList<Pair<ClassDatum,MappingAction>>());
            }
            else {
                QvtcIterator epIt = (QvtcIterator) e.getSource();
                if (!epIt.isLoop(currentOperation)) {
                    addProducerConsumerRelations(currentOperation);
                }
                //expectedInvocationsStack.push(new ArrayList<Pair<ClassDatum,MappingAction>>());
            }
            caopInvs = new ArrayDeque<CallActionInvocation>();
            invocations.push(caopInvs);
        }
    }


    private void addProducerConsumerRelations(CallActionOperation currentOperation) {
        DependencyGraph dg = plan.getDependencyGraph();
        List<Pair<ClassDatum, MappingAction>> lastExpected = new ArrayList<Pair<ClassDatum,MappingAction>>(dg.getProducerRelations(currentOperation.getAction()));
//        if (lastExpected.stream().anyMatch(p -> p.second.equals(currentOperation.getAction()))) {
//            lastExpectedIsLoop = true;
//            lastExpectedLoopMapping  = currentOperation.getAction();
//        }
//        else {
//            lastExpectedIsLoop = false;
//        }
        expectedInvocations.push(lastExpected);
    }



}
