/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler;

/**
 * The Interface Operation. This interface just serves as a common type for
 * vertices in the Execution Plan.
 */
public interface Operation {

	/**
	 * Checks if is the root operation
	 *
	 * @return true, if is root
	 */
	boolean isRoot();

}
