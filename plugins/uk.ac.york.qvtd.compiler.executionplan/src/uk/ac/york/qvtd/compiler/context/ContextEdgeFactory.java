/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.context;

import org.jgrapht.EdgeFactory;

public class ContextEdgeFactory implements EdgeFactory<ContextVertex, ContextEdge> {

	@Override
	public ContextEdge createEdge(ContextVertex sourceVertex, ContextVertex targetVertex) {
		// TODO Auto-generated method stub
		return new ContextEdge(sourceVertex, targetVertex);
	}

}
