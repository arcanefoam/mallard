/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.bindings;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.ocl.pivot.PivotFactory;
import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Variable;

/**
 * A Derived Variable represents a local variable in the context of an invoking
 * mapping. By adding an additional local variable to a mapping, derived
 * variables are useful for shielding the invocation of a mapping by allowing
 * null pointers during property access to be caught by the invoking mapping.
 * This facilitates things like "polled" invocation to be implemented, e.g.
 * wait for the property to be assigned to avoid the null pointer.
 */
public class DerivedVariable {

    /** The variable. */
    private final Variable variable;

    /** The navigation. */
    //private final Property navigation;
    private final List<Property> navigation;

    /** The source variable. */
    private final Variable sourceVariable;

    /**
     * Instantiates a new derived variable.
     *
     * @param sourveVariable the sourve variable
     * @param navProp the nav prop
     * @param targetName the target variable name
     * @param index the index
     */
    public DerivedVariable(Variable sourveVariable, Property navProp, int index) {
        super();
        variable = PivotFactory.eINSTANCE.createVariable();
        variable.setName(sourveVariable.getName() + "_" + navProp.getName().toLowerCase() + "_" + index);
        variable.setType(navProp.getType());
        this.navigation = new ArrayList<Property>();
        navigation.add(navProp);
        this.sourceVariable = sourveVariable;
    }

    /**
     * Copy constructor.
     */
    public DerivedVariable(DerivedVariable other) {
        super();
        variable = PivotFactory.eINSTANCE.createVariable();
        variable.setName(other.variable.getName());
        variable.setType(other.variable.getType());
        this.navigation = other.navigation;
        this.sourceVariable = other.sourceVariable;
    }

    public DerivedVariable(Variable sourceVariable, List<Property> navigation, int index) {
        super();
        variable = PivotFactory.eINSTANCE.createVariable();
        Property lastProp = navigation.get(navigation.size()-1);
        variable.setName(sourceVariable.getName() + "_" + lastProp.getName().toLowerCase() + "_" + index);
        variable.setType(lastProp.getType());
        this.navigation = navigation;
        this.sourceVariable = sourceVariable;
    }

    /**
     * Gets the navigation.
     *
     * @return the navigation
     */
    public List<Property> getNavigation() {
        return navigation;
    }


    /**
     * Gets the sourve variable.
     *
     * @return the sourve variable
     */
    public Variable getSourveVariable() {
        return sourceVariable;
    }

    /**
     * Gets the variable.
     *
     * @return the variable
     */
    public Variable getVariable() {
        return variable;
    }

    @Override
    public String toString() {
        return variable.getName() + " = " + sourceVariable.getName()
            + "." + navigation.stream().map(p -> p.getName()).collect(Collectors.joining("."));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((navigation == null) ? 0 : navigation.hashCode());
        result = prime * result + ((sourceVariable == null) ? 0 : sourceVariable.hashCode());
        result = prime * result + ((variable == null) ? 0 : variable.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof DerivedVariable))
            return false;
        DerivedVariable other = (DerivedVariable) obj;
        if (navigation == null) {
            if (other.navigation != null)
                return false;
        } else if (!navigation.equals(other.navigation))
            return false;
        if (sourceVariable == null) {
            if (other.sourceVariable != null)
                return false;
        } else if (!sourceVariable.equals(other.sourceVariable))
            return false;
        if (variable == null) {
            if (other.variable != null)
                return false;
        } else if (!variable.equals(other.variable))
            return false;
        return true;
    }

}
