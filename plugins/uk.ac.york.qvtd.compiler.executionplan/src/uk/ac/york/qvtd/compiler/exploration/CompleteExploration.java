/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.exploration;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Element;
import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.qvtd.compiler.internal.utilities.ClassRelationships;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.eclipse.qvtd.pivot.qvtcorebase.analysis.DomainUsage;
import org.jgrapht.DirectedGraph;
import org.jgrapht.GraphPath;
import org.jgrapht.Graphs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.Binding;
import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.bindings.DirectBinding;
import uk.ac.york.qvtd.compiler.bindings.LoopBinding;
import uk.ac.york.qvtd.compiler.bindings.ModelType;
import uk.ac.york.qvtd.compiler.bindings.NavigationBinding;
import uk.ac.york.qvtd.compiler.context.Context;
import uk.ac.york.qvtd.compiler.context.ContextVertex;
import uk.ac.york.qvtd.compiler.evaluation.ContextEvaluator;
import uk.ac.york.qvtd.compiler.evaluation.MissingCallsEvaluator;
import uk.ac.york.qvtd.compiler.executionplan.AllInstLoop;
import uk.ac.york.qvtd.compiler.executionplan.CallActionInvocation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.compiler.executionplan.ExecutionPlan;
import uk.ac.york.qvtd.compiler.executionplan.LoopOperation;
import uk.ac.york.qvtd.compiler.executionplan.PropNavLoop;
import uk.ac.york.qvtd.compiler.executionplan.utilities.DependencyGraphUtils;
import uk.ac.york.qvtd.compiler.executionplan.utilities.ExecutionPlanIterator;
import uk.ac.york.qvtd.compiler.executionplan.utilities.ExecutionPlanUtils;
import uk.ac.york.qvtd.dependencies.derivations.DerivationGraph;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.derivations.impl.MappingDerivationsImpl;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.inter.ParameterEdge;
import uk.ac.york.qvtd.dependencies.util.QvtcIterator;

/**
 * The Class CompleteExploration.
 */
public class CompleteExploration {

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(CompleteExploration.class.getName());

    /** The dg. */
    protected DependencyGraph dg;

    /** The analysis. */
    protected final Map<Element, DomainUsage> analysis;

    /** The verbose. */
    protected boolean verbose = true;

    /** The append. */
    private boolean append = false;

    /** The best solutions. */
    protected Set<ExecutionPlanGraph> bestSolutions;

    /** The average solutions. */
    private Set<ExecutionPlanGraph> averageSolutions;

    /** The worst solutions. */
    private Set<ExecutionPlanGraph> worstSolutions;

    /** The final plans by cost. */
    protected Map<Double, Set<ExecutionPlanGraph>> finalPlansByCost;

    /** The mapping to operation. */
    protected Map<MappingAction, CallActionOperation> mappingToOperation = new Hashtable<MappingAction, CallActionOperation>();

    /** The permutation. */
    private int permutation = 0;

    /** The log p. */
    private double logP = 1;

    /** The final plans. */
    private Set<ExecutionPlanGraph> finalPlans;

//    private ClassRelationships clasRels;


    /**
     * Instantiates a new complete exploration.
     *
     * @param dg the dg
     * @param analysis the analysis
     * @param verbose the verbose
     * @param factory
     */
    public CompleteExploration(DependencyGraph dg, Map<Element, DomainUsage> analysis, boolean verbose, @NonNull EnvironmentFactory factory) {
        super();
        this.dg = dg;
        this.analysis = analysis;
        this.verbose = verbose;
//        this.clasRels = new ClassRelationships(factory);
    }

    /**
     * Instantiates a new complete exploration.
     *
     * @param dg the dg
     * @param analysis the analysis
     * @param verbose the verbose
     * @param append the append
     */
    public CompleteExploration(DependencyGraph dg, Map<Element, DomainUsage> analysis, boolean verbose, boolean append) {
        super();
        this.dg = dg;
        this.analysis = analysis;
        this.verbose = verbose;
        this.append = append;
    }

    /**
     * Creates the all plans.
     */
    /* Creates solutions for all the possible combinations of all the permutations of
     * the graph vertices.
     * n! + n! + n*(n-1)! + : sum{x=2, x=n-1} (x)!*(n-x)!  solutions?
     */
    public void createAllPlans() {

        // Create initial operations
        CallActionOperation rop = new CallActionOperation(dg.getRootVertex(), true);
        mappingToOperation.put(dg.getRootVertex(), rop);
        createOperationsForMappings();

        // Initial plans
        Set<ExecutionPlanGraph> permutationPlans = createPermutationPlans();
        printPlans("permutations", permutationPlans);

        // TODO If mapping A consumes a type with no guards, then it should be copied to the context. In this case,
        // any child of A, say B, that consumes that type is no longer required to be called from the parent of A.
        // The reason is that since the type is not guarded, then B consumes all of them. In this case also, we would
        // need to force the type to be the head type, in order to guarantee that we are consuming the type in the
        // context (or we need to prove that the derived type is the same as the context one)
        // FALSE!!! Because the actual elements used come from the binding, what happens is that if any derived
        // parameter (type) is guarded, then we can't use the derivation and need to add a allInstances loop to
        // make sure we consume all types.

        // These should be really enforced by the bindings. When binding, we need to make sure that the derived
        // parameters are unguarded. If the derived parameters are guarded, then we need to do a global search in order
        // to guarantee that all produced elements are consumed. The assignment and predicate information can be used
        // to track a derivation, and from this now if the derived parameter is guarded or not.

        // Complete the plans
        //permutationPlans.addAll(completeCandidateSolutions(permutationPlans));
        Set<ExecutionPlanGraph> correctPlans = completeCandidateSolutions(permutationPlans);
        permutationPlans = null;
        printPlans("CorrectPlans", correctPlans);


        // Add bindings
        Set<ExecutionPlanGraph> fullBoundPlans = createBindingPlans(correctPlans);
        correctPlans = null;
        printPlans("bound", fullBoundPlans);
        // Filter out invalid plans
        removeInvalidPlans(fullBoundPlans);
        printPlans("valid", fullBoundPlans);

        // Create a copy of the plans to use in the loop reuse optimisation and then compare
        //Set<ExecutionPlanGraph> shareLoopPlans = new HashSet<ExecutionPlanGraph>();
        Map<Double, Set<ExecutionPlanGraph>> groupCost = new HashMap<Double, Set<ExecutionPlanGraph>>();
//        CostEvaluator ce = new CostEvaluator();
//        double best = Long.MAX_VALUE;
//        if (verbose) {
//            System.out.println("Costing");
//        }
//        try {
//            for (ExecutionPlanGraph p : fullBoundPlans) {
//                ExecutionPlan cp = new ExecutionPlan((ExecutionPlan)p, true);
//                shareLoopPlans.add(cp);
//                double pCost = ce.calculateCost(p);
//                p.setCost(pCost);
//                if (pCost < best) best = pCost;
//                Set<ExecutionPlanGraph> ps = groupCost.get(pCost);
//                if (ps == null) {
//                    ps = new HashSet<ExecutionPlanGraph>();
//                    groupCost.put(pCost, ps);
//                }
//                ps.add(p);
//            }
//        } catch (ArithmeticException ex){
//            ce = new LogarithmicCostEvaluator();
//            for (ExecutionPlanGraph p : fullBoundPlans) {
//                ExecutionPlan cp = new ExecutionPlan((ExecutionPlan)p, true);
//                shareLoopPlans.add(cp);
//                p.setCost(ce.calculateCost(p));
//            }
//
//        }
        for (ExecutionPlanGraph p : fullBoundPlans) {
            ((ExecutionPlan) p).calculateCost();
            double cost = p.getCost();
            Set<ExecutionPlanGraph> ps = groupCost.get(cost);
            if (ps == null) {
                ps = new HashSet<ExecutionPlanGraph>();
                groupCost.put(cost, ps);
            }
            ps.add(p);
        }
        if (verbose) {
            for (Double cost : groupCost.keySet()) {
                logger.info(cost + "," + groupCost.get(cost).size());
            }
        }
//        // Do a loop share optimization
//        reduceLoops(shareLoopPlans);
//        printPlans("reduced", shareLoopPlans);
//        // Recalculate the cost for the optimized plans
//        for (ExecutionPlanGraph p : shareLoopPlans) {
//            p.setCost(ce.calculateCost(p));
//        }
        //printPlans("reduced cost", shareLoopPlans);

//        finalPlansByCost = new HashMap<Double, Set<ExecutionPlanGraph>>();
        finalPlansByCost = groupCost;
//        //double bestRed = Long.MAX_VALUE;
//        //double bestRed = Long.MAX_VALUE;
//        for (ExecutionPlanGraph p : shareLoopPlans) {
//            double pCost = p.getCost();
//            //if (pCost < bestRed)
//            //    bestRed = pCost;
//            Set<ExecutionPlanGraph> ps = finalPlansByCost.get(pCost);
//            if (ps == null) {
//                ps = new HashSet<ExecutionPlanGraph>();
//                finalPlansByCost.put(pCost, ps);
//            }
//            ps.add(p);
//        }
//        if (verbose) {
//            for (Double cost : finalPlansByCost.keySet()) {
//                System.out.println(cost + "," + finalPlansByCost.get(cost).size());
//            }
//        }
        double minCost = finalPlansByCost.keySet().stream().min(Double::compare).get();
        logger.info("minCost," + minCost);
        double maxCost = finalPlansByCost.keySet().stream().max(Double::compare).get();
        logger.info("maxCost," + maxCost);

        bestSolutions = finalPlansByCost.get(minCost);
        printPlans("redbest", bestSolutions);
        // Since we can not execute all of them, if more than 20, get a 20 plans sample
        if (fullBoundPlans.size() > 20) {
            logger.warn("Complete exploration resulted in " + fullBoundPlans.size() + " plans. "
                    + "Taking a random sample of 20.");
            finalPlans = new LinkedHashSet<ExecutionPlanGraph>(20);
            // Pick the best + 19 random ones, with different cost
            RandomDataGenerator rdg = new RandomDataGenerator();
            finalPlans.add(getBestSolution());
            Set<Integer> added = new HashSet<Integer>();
            ExecutionPlanGraph[] plans = fullBoundPlans.toArray(new ExecutionPlanGraph[0]);
            int missing = 19;
            boolean unique = true;
            BitSet attempt = new BitSet(fullBoundPlans.size());
            do {
                int[] index = rdg.nextPermutation(fullBoundPlans.size(), missing);
                for (int i = 0; i < missing; i++) {
                    attempt.set(index[i]);
                    ExecutionPlanGraph selection = plans[index[i]];
                    Set<Double> costs = finalPlans.stream().map(p -> p.getCost()).collect(Collectors.toSet());
                    if (unique && costs.contains(selection.getCost())) {
                        continue;
                    }
                    finalPlans.add(selection);
                }
                missing = 20 - finalPlans.size();
                unique = (attempt.cardinality() != fullBoundPlans.size());
            } while (finalPlans.size() < 20);

        } else {
            finalPlans = new LinkedHashSet<ExecutionPlanGraph>(fullBoundPlans);
        }

        printPlans("All", finalPlans);
    }


    /**
     * Gets the average solutions.
     *
     * @return the average solutions
     */
    public Set<ExecutionPlanGraph> getAverageSolutions() {
        return averageSolutions;
    }

    /**
     * Return a random member of the best solutions.
     *
     * @return the best solution
     */
    public ExecutionPlanGraph getBestSolution() {
        int index = new Random().nextInt(bestSolutions.size());
        Iterator<ExecutionPlanGraph> iter = bestSolutions.iterator();
        for (int i = 0; i < index; i++) {
            iter.next();
        }
        return iter.next();
    }

    /**
     * Gets the best solutions.
     *
     * @return the best solutions
     */
    public Set<ExecutionPlanGraph> getBestSolutions() {
        return bestSolutions;
    }

    public DependencyGraph getDg() {
        return dg;
    }

    /**
     * Gets the final plans.
     *
     * @return the final plans
     */
    public Set<ExecutionPlanGraph> getFinalPlans() {
        return finalPlans;
    }

    /**
     * Gets the final plans by cost.
     *
     * @return the final plans by cost
     */
    public Map<Double, Set<ExecutionPlanGraph>> getFinalPlansByCost() {
        return finalPlansByCost;
    }

    /**
     * Gets the worst solutions.
     *
     * @return the worst solutions
     */
    public Set<ExecutionPlanGraph> getWorstSolutions() {
        return worstSolutions;
    }


    /**
     * Computes the next permutation of the list.
     * Based on the C++ std libray std::next_permutation method
     *
     * @param ls the ls
     * @return true, if successful
     */
    public boolean nextPermutation(List<MappingAction> ls) {
        if (ls.size() <= 1)
            return false;
        ListIterator<MappingAction> first = ls.listIterator(0);
        ListIterator<MappingAction> last = ls.listIterator(ls.size());
        ListIterator<MappingAction> i = ls.listIterator(ls.size());
        i.previous();
        if (verbose) {
            permutation++;
            double currlogP = Math.log10(permutation);
            if (currlogP > logP ) {
                logger.info("Permutation: " + logP);
                logP = Math.floor(currlogP)+1;
            }
            if (currlogP > 7.6) {
                logger.info(String.valueOf(permutation));
            }
        }
        for(;;) {
            ListIterator<MappingAction> ii = ls.listIterator(i.previousIndex());
            ii.next(); // ii and i are now pointing to the same position
            i.previous();
//			System.out.println("i: " + getValue(ls, i));
//			System.out.println("ii: " + getValue(ls, ii));
            if (getValue(ls, i).compareTo(getValue(ls, ii)) < 0) {
                ListIterator<MappingAction> j = ls.listIterator(ls.size());
                do {
                    j.previous();
//					System.out.println("i: " + getValue(ls, i));
//					System.out.println("j: " + getValue(ls, j));
                } while(!(getValue(ls, i).compareTo(getValue(ls, j)) < 0));
                swap(i, j);
                reverse(ii, ls.listIterator(ls.size()));
                return true;

            }
            if (i.nextIndex() == first.nextIndex()) {
                reverse(first, last);
                return false;
            }
        }
    }

    /**
     * Reverses the order of the elements in the range [first, last)
     * Based on the C++ std libray std::reverse method.
     *
     * @param first the first
     * @param last the last
     */
    public void reverse(ListIterator<MappingAction> first, ListIterator<MappingAction> last) {
        while((first.nextIndex() != last.nextIndex()) && (first.nextIndex() != last.previousIndex())) {
            last.previous();
            swap(first, last);
            first.next();
        }

    }

    /**
     * Sets the dg.
     *
     * @param dg the new dg
     */
    public void setDg(DependencyGraph dg) {
        this.dg = dg;
    }

    /**
     * Swaps the values of the elements the given iterators are pointing to.
     * Based on the C++ std libray std::swap method
     *
     * @param i the i
     * @param j the j
     */
    public void swap(ListIterator<MappingAction> i, ListIterator<MappingAction> j) {
        MappingAction temp1 = i.next();
        i.set(j.next());
        i.previous(); // Return it to its original position
        j.set(temp1);
        j.previous(); // Return it to its original position
    }

    /**
     * For each node in the graph, for all combinations of its successors, add the missing
     * successors to the list of rule calls
     * TODO Can we use the check/enfornce information to be more smart about
     * what invocations to add? E.g., A checked domain might not require an
     * invocation from the root
     *
     * @param candidatePlans the candidate plans
     * @return the sets the
     */
    private Set<ExecutionPlanGraph> completeCandidateSolutions(Set<ExecutionPlanGraph> candidatePlans)  {

        int previous = 0;
        logger.info("complete Candidate Solutions");
        Set<ExecutionPlanGraph> newPlans = new HashSet<ExecutionPlanGraph>();
        Deque<ExecutionPlanGraph> epq = new ArrayDeque<ExecutionPlanGraph>();
        for (ExecutionPlanGraph cp : candidatePlans) {
            if (ExecutionPlanUtils.isViable(cp)) {
                epq.add(cp);
            }
        }
        Set<ExecutionPlanGraph> completedPlans;
        ExecutionPlan currentPlan;
        while (!epq.isEmpty()) {
            currentPlan = (ExecutionPlan) epq.poll();
            if (verbose) {
                if ((epq.size() > previous*1.5) || (epq.size() < previous/2)) {
                    logger.info("Remaining:" + epq.size());
                    previous = epq.size();
                }
            }
            if (ExecutionPlanUtils.isCorrect(currentPlan)) {
                newPlans.add(currentPlan);
            }
            completedPlans = createPlansForMissingCalls(currentPlan);
            for (ExecutionPlanGraph np : completedPlans) {
                if (ExecutionPlanUtils.isCorrect(np)) {		// If an intermediate is correct, save it!
                    ExecutionPlan cp = new ExecutionPlan((ExecutionPlan) np);
                    newPlans.add(cp);
                }
                if (ExecutionPlanUtils.isViable(currentPlan)) {
                    epq.add(np);
                }
            }
        }
        return newPlans;
    }

    /**
     * Add the missing bindings and required loop operations to the plans.
     *
     * @param permutationPlans the permutation plans
     * @return A new set of plans with the missing information
     */
    protected Set<ExecutionPlanGraph> createBindingPlans(Set<ExecutionPlanGraph> permutationPlans) {

        int previous = 0;
        int previousF = 0;
        logger.info("Binding Plans");
        Set<ExecutionPlanGraph> fullBoundPlans = new HashSet<ExecutionPlanGraph>();
        // Add additional plans for binding alternatives
        Deque<ExecutionPlanGraph> bindingPlans = new ArrayDeque<ExecutionPlanGraph>(permutationPlans);
        ExecutionPlan p;
        QvtcIterator it;
        ContextEvaluator inspector;
        while (!bindingPlans.isEmpty()) {
            if (verbose) {
                if ((bindingPlans.size() > previous*1.5) || (bindingPlans.size() < previous/2)) {
                    logger.info("Remaining:" + bindingPlans.size());
                    previous = bindingPlans.size();
                }
            }
            p = (ExecutionPlan) bindingPlans.pollFirst();
            it = new ExecutionPlanIterator(p);
            inspector = new ContextEvaluator(p);
            it.addTraversalListener(inspector);
            while(it.hasNext() && inspector.isValid()) {
                it.next();
            }
            if (inspector.isValid()) {
                // The plan is complete
                fullBoundPlans.add(p);
                if (verbose) {
                    if ((fullBoundPlans.size() > previousF*1.5) || (fullBoundPlans.size() < previousF/2)) {
                        logger.info("Full:" + fullBoundPlans.size());
                        previousF = fullBoundPlans.size();
                    }
                }
            } else {
                // New plans need to be spawned
                for (ExecutionPlan np : spwanBindingPlansFromPlan(p, inspector)) {
                    bindingPlans.addFirst(np);
                }
            }
        }
        return fullBoundPlans;
    }



    /**
     * For the given operation, track all input variables associated to a given ModelType.
     *
     * @param plan the plan
     * @param caop the caop
     * @return the map
     */
    public Map<ModelType, List<Variable>> createBoundVars(ExecutionPlanGraph plan, CallActionOperation caop) {
        Map<ModelType, List<Variable>> boundVars = new HashMap<ModelType, List<Variable>>();
        List<Variable> vars;
        for (DependencyEdge e : plan.getDependencyGraph().incomingEdgesOf(caop.getAction())) {
            if (e instanceof ParameterEdge) {
                Type type = ((ClassDatum) e.getSource()).getType();
                TypedModel domain = ((ClassDatum) e.getSource()).getDomain();
                ModelType bv = new ModelType(type, domain);
                vars = boundVars.get(bv);
                if (vars == null) {
                    vars = new ArrayList<Variable>();
                    boundVars.put(bv, vars);
                }
                vars.addAll(((ParameterEdge) e).getVariables());
            }
        }
        return boundVars;
    }

    /**
     * Add the bindings and additional required loop actions for the given bindings.
     *
     * @param plan the plan
     * @param invocation the invocation to replace/complete
     * @param insertIndex the insert index
     * @param mt The model type
     * @param boundVars The variables that match the model type
     * @param context The context used to find sources for the model type
     * @param analysis the analysis
     */
    private void createInvocationChains(ExecutionPlanGraph plan, Invocation invocation, int insertIndex,
            ModelType mt, List<Variable> boundVars, Context context, Map<Element, DomainUsage> analysis) {

        MappingAction mv = ((CallActionOperation) invocation.getTarget()).getAction();
        MappingDerivationsImpl der = mv.getMappingDerivations();
        if (context == null) {
            // If the context is null, it's an invocation from the root and hence
            // we need an All Instances loop.
            AllInstLoop la;
            // For each unrelated bound variable, a new invocation is needed. Reduce the bound variables to remove any
            // variables that can be derived
            List<Variable> primaryVars = reduceBoundVars(boundVars, der);
            Map<Variable, AllInstLoop> initialLoops = createStartingLoops(boundVars, mt);
            for (Variable bv : primaryVars) {
                la = initialLoops.get(bv);
                //plan.addVertex(la);
                LoopBinding lb = new LoopBinding(bv, la, ExecutionPlan.ALL_INS_LOOP_BIDING_COST);
                instertInvocationChain(plan, la, invocation, insertIndex, lb, der);//, analysis);
            }
        } else {
            for(ContextVertex cv : context.getLastMatch()) {
                for (Variable bv : boundVars) {
                    // For each variable create a new invocation
                    DirectBinding db = new DirectBinding(bv, cv.getVariable(), ExecutionPlan.DIRECT_BIDING_COST);
                    instertInvocationChain(plan, invocation.getSource(), invocation, insertIndex, db, der);//, analysis);
                }
            }
        }
    }

    /**
     * Creates the operations for mappings.
     */
    private void createOperationsForMappings() {
        // Root is already mapped
        List<MappingAction> mappings = dg.getMappingVertices();
        mappings.remove(dg.getRootVertex());
        for (MappingAction m : mappings) {
            CallActionOperation op = new CallActionOperation(m, false);
            mappingToOperation.put(m, op);
        }
    }
//      CallActionOperation op = getMappingToOperation().get(mapping);
//      if (op == null) {
//          op = new CallActionOperation(mapping, false);
//          getMappingToOperation().put(mapping, op);
//          addVertex(op);
//      }
//      return op;
//  }

    /**
 * Create a set of plans of all the possible permutations of mappings in the
 * dependency graph.
 *
 * @return the sets the
 */
    private Set<ExecutionPlanGraph> createPermutationPlans() {

        logger.info("Permutation Plans");
        Set<ExecutionPlanGraph> permutationPlans = new HashSet<ExecutionPlanGraph>();
        List<MappingAction> neighbours = new ArrayList<MappingAction>(dg.getMappingVertices());
        neighbours.remove(dg.getRootVertex());
        Collections.sort(neighbours);
        Deque<MappingAction> maq;
        ExecutionPlanGraph ip = new ExecutionPlan(dg, mappingToOperation.get(dg.getRootVertex()));
        int prevSize = 0;
        do {
            if (verbose) {
                prevSize = permutationPlans.size();
            }
            maq = new ArrayDeque<MappingAction>(neighbours);
            permutationPlans.addAll(createPermutationsPlansLoop((ExecutionPlan) ip, dg, maq));
            if (verbose) {
                if (permutationPlans.size() - prevSize > 0) {
                    logger.info("Permutations for " + neighbours);
                    logger.info(permutationPlans.size() - prevSize + " new plans.");
                    logger.info(permutationPlans.size() + " total plans.");
                }
            }
        } while (nextPermutation(neighbours));
        return permutationPlans;
    }


    /**
     * Creates the permutations plans loop.
     *
     * @param ip the ip
     * @param dg the dg
     * @param targetActions the target actions
     * @return the sets the
     */
    private Set<ExecutionPlanGraph> createPermutationsPlansLoop(ExecutionPlan ip, DependencyGraph dg, Deque<MappingAction> targetActions) {

        // fb106. Test if the initial mapping has dependencies
        if (dg.getPredecessorActions(targetActions.peekFirst()).size() > 0) { // The root is removed from predecessor actions
            return Collections.emptySet();
        }
        Deque<ExecutionPlanGraph> plansL = new ArrayDeque<ExecutionPlanGraph>();
        ExecutionPlan p;
        MappingAction action;
        //List<CallActionOperation> sourceOperations;
        List<ExecutionPlanGraph> newPlans = new ArrayList<ExecutionPlanGraph>();
        plansL.addLast(new ExecutionPlan(ip));
        while (!targetActions.isEmpty()) {
            action = targetActions.pop();
            newPlans.clear();
            while (!plansL.isEmpty()) {
                p = (ExecutionPlan) plansL.pollFirst();
                // We need to create a new plan for each invocation of the existing
                // vertices to the new action
                for (Operation eop : p.vertexSet()) {
                    ExecutionPlan newP = new ExecutionPlan(p);
                    CallActionOperation targetOp = mappingToOperation.get(action);
                    newP.addOperationForMapping(action, targetOp);
                    newP.addEdge(eop, targetOp);
                    if (ExecutionPlanUtils.isViable(newP)) {
                        newPlans.add(newP);
                    }
                }
            }
            plansL.addAll(newPlans);
        }
        return new HashSet<ExecutionPlanGraph>(plansL);

    }

    /**
     * Add (insert) all possible permutations of the missing invocations for the given
     * operation at the given index.
     *
     * @param plan the plan
     * @param source the source
     * @param missingCalls the missing calls
     * @param index the index
     * @return the sets the
     */
    private ExecutionPlanGraph createPlansForInvocatio(ExecutionPlan plan, CallActionOperation source,
            CallActionOperation missingCall, int index) {

        ExecutionPlanGraph newPlan = new ExecutionPlan(plan);
        newPlan.addEdge(index, source, missingCall);
        return newPlan;
    }

    /**
     * Search for the first operation missing invocations to any of its consumers and add it. We are only interested
     * in single consumers.
     *
     * @param plan the plan
     * @return the sets the
     */
    private Set<ExecutionPlanGraph> createPlansForMissingCalls(ExecutionPlan plan) {

        Set<ExecutionPlanGraph> newPlans = new HashSet<ExecutionPlanGraph>();
        QvtcIterator it = new ExecutionPlanIterator(plan);
        MissingCallsEvaluator inspector = new MissingCallsEvaluator(plan);
        it.addTraversalListener(inspector);
        while (it.hasNext()) {
            it.next();
            if (!inspector.isValid())
                break;
        }
        if (!inspector.isValid()) {
            // Consumers of op from the evaluator
            List<CallActionOperation> consumers = inspector.getMissingCalls();
            assert !consumers.isEmpty();
            CallActionOperation target = inspector.getTargetOperation();
            // For all possible insertion points, add all the permutations of the
            // missing calls. E.g.:
            /*		if ruleCalls = [ex1, ex2,..., exn]
             *       [[missing], ex1, ex2,..., exn]
             *       [ex1,[missing], ex2,..., exn]
             * 		and so on and so forth
             * And repeat this for all combinations of [missing] for 1 to [missing].size() elements
             */
            for (CallActionOperation p : consumers) {
                // Get the missing invocations, i.e. what consumers have not been invoked
                List<Operation> existing = Graphs.successorListOf(plan, p);
                if (!existing.contains(p)) {
                    // Since we will revisit the plan, just add one missing call
                    if (existing.isEmpty()) { 	// Just add
                        newPlans.add(createPlansForInvocatio(plan, p, target, 0));
                    }
                    else {		// Add at each possible insertion point
                        for (int i = 0; i <= existing.size(); i++) {
                            newPlans.add(createPlansForInvocatio(plan, p, target, i));
                        }
                    }
                }
            }

        }
        return newPlans;
    }

    /**
     * Creates the starting loops.
     *
     * @param boundVars the bound vars
     * @param mt the mt
     * @return the map
     */
    private Map<Variable, AllInstLoop> createStartingLoops(List<Variable> boundVars, ModelType mt) {
        Map<Variable, AllInstLoop> result = new HashMap<Variable, AllInstLoop>();
        AllInstLoop la;
        for (Variable bv : boundVars) {
            la = new AllInstLoop(mt, 0);
            result.put(bv, la);
        }
        return result;
    }

    /**
     * Gets the value.
     *
     * @param l the l
     * @param i the i
     * @return the value
     */
    // Use the rule names as the value for the sublist
    private String getValue(List<MappingAction> l, ListIterator<MappingAction> i) {
        StringBuilder sb = new StringBuilder();
        if (i.hasNext()) {
            for (MappingAction v : l.subList(i.nextIndex(), l.size())) {
                sb.append(v.getLabel());
            }
        } else {
            sb.append(i.previous().getLabel());
            i.next();
        }
        return sb.toString();
    }

    /**
     * Create the required bindings and loops according to the derivation paths
     * between the bound variable and the other input variables.
     *
     * @param plan the plan
     * @param invoker the invoker
     * @param initialInvocation the initial invocation
     * @param insertIndex the insert index
     * @param inititalBinding the initital binding
     * @param derivations the derivations
     */
    // TODO Dealing with single properties after a multi value property is difficult,
    // and not to be expected (03/10/2015). If this is needed, then the last loop
    // created must be used as the source for derivations until another loop
    // is is found. To find when this is required, e.g. future use, inform the
    // user that this is not supported and discard the plan
    private void instertInvocationChain(ExecutionPlanGraph plan, Operation invoker,
            Invocation initialInvocation, int insertIndex,
            Binding inititalBinding, DerivationGraph derivations) {//, Map<Element, DomainUsage> analysis) {

        // Create the invocation chain backwards because it is easier to insert new invocations this way and the
        // initial invocation can be safely deleted at the end
        List<Variable> unBound = new ArrayList<Variable>();
        // We need to guarantee that bindings are accessed in creation order
        Map<Variable, Binding> tempBindnigs = new LinkedHashMap<Variable, Binding>();
        Invocation lastInvocation = initialInvocation;
        Operation lastOperation = invoker;
        int index = 1;
        Variable initialBindingVar;
        if (inititalBinding instanceof LoopBinding) {
            initialBindingVar = inititalBinding.getBoundVariable();
        } else {		// It can only be a direct binding
            initialBindingVar = inititalBinding.getBoundVariable();
        }
        tempBindnigs.put(initialBindingVar, inititalBinding);
        if (!initialInvocation.getSource().equals(invoker)) {
            // Invoker is a loop added by the caller, insert it
            tempBindnigs.putAll(((CallActionInvocation) initialInvocation).getBindings());
            lastInvocation = ExecutionPlanUtils.insertOperation(plan, invoker, lastInvocation, insertIndex);
            insertIndex = 0;
        }
        // Add All instances loops for unbound variables
        // TODO, should we further try to derive unbound from other unbound ones?
        // Get the derivation paths from the primary variable to the rest
        Variable primaryVariable = inititalBinding.getBoundVariable();		// The primary variable can be either a loop variable or a local variable
        List<GraphPath<Variable, PropertyEdge>> paths = DependencyGraphUtils.getDerivationPaths(
                        primaryVariable, derivations, unBound);

        for (Variable uv : unBound) {
            // Can we derive from the unbound? Yes we can, but then that means that we would probably need
            // another plan for each alternative binding form the unbound
//        	List<Variable> unBound2 = new ArrayList<Variable>();
//        	List<GraphPath<Variable, PropertyEdge>> paths2 = DependencyGraphUtils.getDerivationPaths(
//                    uv, derivations, unBound2);
//        	derivations.findMinimumSpanningTrees();
            // 1. Create the loop action
            // TODO WE could use the MT created in the abstract exploration
            // All variables are input ones, so we can get TypedModel from the domain
            //Area a = MallardMtcUtil.
            AllInstLoop la = new AllInstLoop(uv.getType(), analysis.get(uv).getTypedModel(uv), index++);
            // 3. Create a binding for the loop variable, this binding
            // can not be added to the invocation directly as further down we
            // might need to add additional operation/invocation chains
            LoopBinding allInstancesb = new LoopBinding(uv, la, ExecutionPlan.ALL_INS_LOOP_BIDING_COST);
            tempBindnigs.put(uv, allInstancesb);
            // 2. Create a new invocation for the loop
            lastInvocation = ExecutionPlanUtils.insertOperation(plan, la, lastInvocation, insertIndex);
            insertIndex = 0;
        }
        // Paths are ordered shorter to longest, but we can not just add a binding for the last property
        // because we need to make sure or derivations come from the primary variable and not some other
        // local variable
        for (GraphPath<Variable, PropertyEdge> path : paths) {
            List<PropertyEdge> edgeList = path.getEdgeList();
            PropertyEdge lastEdge = edgeList.get(edgeList.size()-1);
            // Find the last isMany property that is not the last one, if not found we derive from the primary
            Collections.reverse(edgeList);
            Iterator<PropertyEdge> it = edgeList.iterator();
            Variable sourceVariable = null;
            List<Property> navigation = new ArrayList<Property>();
            boolean useLoopVariable = false;
            long cost = 0;

            if (it.hasNext()) {
                it.next();	// Skip the last property
            }
            while(it.hasNext()) {
                PropertyEdge pe = it.next();
                if (pe.getProperty().isIsMany()) {
                    sourceVariable = derivations.getEdgeTarget(pe);
                    useLoopVariable = true;
                    int pos = edgeList.indexOf(pe);
                    for (int i = 0; i < pos; i++) {
                        navigation.add(edgeList.get(i).getProperty());
                        cost += edgeList.get(i).getCost();
                    }
                    Collections.reverse(navigation);
                    break;
                }
            }
            Variable bindingSource;
            if (!useLoopVariable) {
                Collections.reverse(edgeList);
                bindingSource = inititalBinding.getSourceVariable();
                navigation = edgeList.stream().map(e -> e.getProperty()).collect(Collectors.toList());
                cost = edgeList.stream().mapToLong(e -> e.getCost()).sum();
            }
            else {
                Binding sourceBinding = tempBindnigs.get(sourceVariable);
                bindingSource = sourceBinding.getSourceVariable();
            }
            assert bindingSource != null;
            Property property = lastEdge.getProperty();
            // Two cases, its either a multi-value property or not
            Variable targetVar = path.getEndVertex();
            if (!property.isIsMany()) {
                NavigationBinding binding = new NavigationBinding(targetVar, bindingSource, cost, navigation);
                if (lastOperation instanceof LoopOperation) {
                    ((LoopOperation) lastOperation).getAssociatedLoopBindings().add((NavigationBinding) binding);
                }
                tempBindnigs.put(targetVar, binding);
            }
            else {
                // We need a loop
                PropNavLoop pnl = new PropNavLoop(bindingSource , targetVar.getType(), navigation, index++);
                LoopBinding binding = new LoopBinding(targetVar, pnl, cost);
                tempBindnigs.put(targetVar, binding);
                // Since we may add one or more nested loops, insert it as soon as it is created
                lastOperation = pnl;
                lastInvocation =  ExecutionPlanUtils.insertOperation(plan, pnl, lastInvocation, insertIndex);
                insertIndex = 0;
            }
        }
        ((CallActionInvocation) lastInvocation).setBindings(tempBindnigs);
    }

    /**
     * Print the number of elements, and if print plans flag is set to True,
     * prints the DOT representation of the plans.
     *
     * @param name
     * @param plans
     */
    private void printPlans(String name, Set<? extends ExecutionPlanGraph> plans, boolean printPlans) {
        logger.info(name + ": " + plans.size());
        if (printPlans) {
            logger.info(ExecutionPlanUtils.generateDOT(name, plans));
        }
    }

    /**
     * Prints the plans.
     *
     * @param name the name
     * @param plans the plans
     */
    protected void printPlans(String name, Set<ExecutionPlanGraph> plans) {
        printPlans(name, plans, verbose);
    }


    /**
     * Reduce bound vars.
     *
     * @param boundVars the bound vars
     * @param der the der
     * @return the list
     */
    private List<Variable> reduceBoundVars(List<Variable> boundVars, DerivationGraph der) {

        List<Variable>  result = new ArrayList<Variable>(boundVars);
        if (boundVars.size() == 1) {
            return result;
        }
        //Set<PropertyEdge> minEdges = der.getMinimumSpanningTree().getMinimumSpanningTreeEdgeSet();
        Iterator<Variable> it1 = result.iterator();
        while (it1.hasNext()) {
            Variable var1 = it1.next();
            Iterator<Variable> it2 = result.iterator();
            while (it2.hasNext()) {
                Variable var2 = it2.next();
                // If var1 can be derived from var2, the index of var1
//                PropertyEdge e = der.getEdge(var2, var1);
//                if (minEdges.contains(e)) {
//                    // TODODo we need further verification?
//                    // var1 can be derived from var2
//                    it1.remove();
//                    break;
//                }
                if (!var1.equals(var2)) {
                    if (der.pathExists(var2, var1)) {
                        it1.remove();
                        break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Reduce loops.
     *
     * @param plans the plans
     */
    private void reduceLoops(Set<ExecutionPlanGraph> plans) {
        for (ExecutionPlanGraph p : plans) {
            ExecutionPlanUtils.optimizeLoopSharing(p);
        }
    }

    /**
     * Removes the invalid plans.
     *
     * @param plans the plans
     */
    private void removeInvalidPlans(Set<? extends ExecutionPlanGraph> plans) {
        // TODO Check the concept of "valid", probably need to validate after
        // bindings
        Iterator<? extends ExecutionPlanGraph> validIt = plans.iterator();
        while(validIt.hasNext()) {
            ExecutionPlanGraph p = validIt.next();
            if (!ExecutionPlanUtils.isCorrect(p))
                validIt.remove();
        }
    }

    /**
     * Spwan binding plans from plan.
     *
     * @param plan the plan
     * @param inspector the inspector
     * @return the list
     */
    private List<ExecutionPlan> spwanBindingPlansFromPlan(ExecutionPlan plan, ContextEvaluator inspector) {

        List<ExecutionPlan> newPlans = new ArrayList<ExecutionPlan>();
        Invocation oi = inspector.getLastInvocation();
        // The target operation can be invoked using any of its input variables as primary variable and deriving the others
        Map<ModelType, List<Variable>> boundVars = createBoundVars(plan, (CallActionOperation) oi.getTarget());
        // For each model type, found a matching context.
        // TODO We probably need to sort the set to provide determinism
        Context context = inspector.getContextStack().pollFirst();
        ExecutionPlan newPlan;
        for (ModelType mt : boundVars.keySet()) {
            if (context == null ? true : context.findMatches(mt.getType(), mt.getDomain())) {
                newPlan = new ExecutionPlan(plan, true);
                Invocation newOi = newPlan.getEdge(oi.getSource(), oi.getTarget());
                int index = newPlan.indexOfOutgoing(newOi);
                try {
                    createInvocationChains(newPlan, newOi, index, mt, boundVars.get(mt), context, analysis);
                } catch (UnsupportedOperationException ex) {
                    // The required invocations can not be added
                    //System.err.println(ex.getMessage());
                    continue;
                }
                newPlans.add(newPlan);
            }
        }
        boundVars.clear();
        boundVars = null;
        return newPlans;
    }

    /**
     * Finds the operations that are missing a call to the given action.
     *
     * @param p the p
     * @param targetOp the target op
     * @return the next source operations
     */
    protected List<CallActionOperation> getNextSourceOperations(DirectedGraph<Operation, Invocation> p, Operation targetOp) {

        List<CallActionOperation> sourceOperations = p.vertexSet().stream()
                .filter(op ->
                    ((op != targetOp && p.inDegreeOf(op) > 0) || op.isRoot())
                    && !Graphs.successorListOf(p, op).contains(targetOp))
                .map(CallActionOperation.class::cast)
                .collect(Collectors.toList());
        return sourceOperations;
    }

}
