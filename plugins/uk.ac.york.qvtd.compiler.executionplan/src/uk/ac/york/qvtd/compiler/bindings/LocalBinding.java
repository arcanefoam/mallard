/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.bindings;

import org.eclipse.ocl.pivot.Variable;

/**
 * A Local Binding represents a binding that comes from a local variable. Local
 * variables are defined by Derived Variables.
 */
public class LocalBinding extends AbstractBinding {

    /** The derived variable. */
    private final DerivedVariable derivedVariable;

    /**
     * Instantiates a new local binding.
     *
     * @param boundVariable the bound variable
     * @param derivedVariable the derived variable
     * @param cost
     */
    public LocalBinding(Variable boundVariable, DerivedVariable derivedVariable, long cost) {
        super(cost, boundVariable);
        this.derivedVariable = derivedVariable;
    }

    public LocalBinding(LocalBinding other) {
        this(other.boundVariable, other.derivedVariable, other.getCost());
    }

    @Override
    public Variable getSourceVariable() {
        return derivedVariable.getVariable();
    }

    public DerivedVariable getDerivedVariable() {
        return derivedVariable;
    }

    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder();
        buff.append(boundVariable.getName());
        buff.append(" = <");
        buff.append(derivedVariable.toString());
        buff.append(">");
        return buff.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((derivedVariable == null) ? 0 : derivedVariable.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof LocalBinding))
            return false;
        LocalBinding other = (LocalBinding) obj;
        if (derivedVariable == null) {
            if (other.derivedVariable != null)
                return false;
        } else if (!derivedVariable.equals(other.derivedVariable))
            return false;
        return true;
    }



}