package uk.ac.york.qvtd.compiler.executionplan.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.ocl.pivot.Variable;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.GraphPathImpl;

import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.dependencies.derivations.DerivationGraph;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;

public class DependencyGraphUtils {

    /**
     * Get all the paths to derive variables starting at the source var. Variables
     * that cannot be derived are returned in the unBound list.
     * @param sourceVar
     * @param der
     * @param unBound
     * @return
     */
    public static List<GraphPath<Variable, PropertyEdge>> getDerivationPaths(Variable sourceVar, DerivationGraph der, List<Variable> unBound) {
        List<GraphPath<Variable, PropertyEdge>> paths = new ArrayList<GraphPath<Variable, PropertyEdge>>();
        ArrayList<Variable> secVars = new ArrayList<Variable>(der.vertexSet());
        secVars.remove(sourceVar);
        for (Variable secVar : secVars) {
            // Get all the paths
            List<PropertyEdge> path = DijkstraShortestPath.findPathBetween(der, sourceVar, secVar);
            if (path != null) {
                paths.add(new GraphPathImpl<Variable, PropertyEdge>(der, sourceVar, secVar, path, path.size()));
            } else {
                // IF the path is null, the variables are not related, hence need to add an All instances loop
                unBound.add(secVar);
            }
        }
        Collections.sort(paths,  new Comparator<GraphPath<Variable, PropertyEdge>>() {

                @Override
                public int compare(GraphPath<Variable, PropertyEdge> o1, GraphPath<Variable, PropertyEdge> o2) {
                    return Integer.compare(o1.getEdgeList().size(), o2.getEdgeList().size());
                }
            });
        return paths;
    }

     /**
     * Gets the paths ending on the given operation. Since we are interested in
     * the other CallActions visited, not the actual different paths, we filter
     * the paths so we only leave 1 path for each set of visited vertices
     *
     * @deprecated Use {@link DependencyGraph#getReducedPathsFromRoot(MappingAction)}
     * @param caop the operation where paths end
     * @param dependencyGraph
     * @return the paths ending on
     */
    @Deprecated
    public static List<GraphPath<DependencyVertex, DependencyEdge>> getPathsFromRoot(
            DependencyGraph dependencyGraph,
            CallActionOperation caop) {


        return null;
    }

}
