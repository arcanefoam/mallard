/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.bindings;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Variable;

/**
 * A Direct Binding represents a binding to an existing variable. This type
 * of binding is used to bind realized variables in the invoking mapping
 * to input variables in the invoked mapping.
 */
public class DirectBinding extends AbstractBinding {

    /** The context variable. */
    private final @NonNull Variable existingVariable;

    /**
     * Instantiates a new context binding.
     *
     * @param boundVariable the bound variable
     * @param existingVariable the existing variable
     * @param cost the cost of the binding
     */
    public DirectBinding(Variable boundVariable, @NonNull Variable existingVariable, long cost) {
        super(cost, boundVariable);
        this.existingVariable = existingVariable;
    }

    public DirectBinding(DirectBinding other) {
        this(other.getBoundVariable(), other.getSourceVariable(), other.getCost());
    }

    @Override
    public @NonNull Variable getSourceVariable() {
        return existingVariable;
    }

    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder();
        buff.append(boundVariable.getName());
        buff.append(" = ");
        buff.append(existingVariable.getName());
        return buff.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((existingVariable == null) ? 0 : existingVariable.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof DirectBinding))
            return false;
        DirectBinding other = (DirectBinding) obj;
        if (!existingVariable.equals(other.existingVariable))
            return false;
        return true;
    }

}