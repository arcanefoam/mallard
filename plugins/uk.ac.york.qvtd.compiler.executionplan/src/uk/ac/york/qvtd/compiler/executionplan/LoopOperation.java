package uk.ac.york.qvtd.compiler.executionplan;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ocl.pivot.Variable;

import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.bindings.NavigationBinding;

public abstract class LoopOperation implements Operation {

    protected Variable loopVariable;

    /** The associated loop bindings. */
    protected List<NavigationBinding> associatedLoopBindings = new ArrayList<NavigationBinding>();

    public Variable getLoopVariable() {
        return loopVariable;
    }

    /**
     * Get the list of Bindings that are associated to this operation either by
     * loop bindings or navigation bidings.
     * @return
     */
    public List<NavigationBinding> getAssociatedLoopBindings() {
        return associatedLoopBindings;
    }

    @Override
    public String toString() {
        return "LoopAll: " + loopVariable.getType().getName();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((loopVariable == null) ? 0 : loopVariable.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LoopOperation other = (LoopOperation) obj;
        if (loopVariable == null) {
            if (other.loopVariable != null)
                return false;
        } else if (!loopVariable.equals(other.loopVariable))
            return false;
        return true;
    }

}
