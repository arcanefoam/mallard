package uk.ac.york.qvtd.compiler.evaluation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.Model;
import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Variable;
import org.jgrapht.Graph;
import org.jgrapht.event.EdgeTraversalEvent;
import org.jgrapht.event.VertexTraversalEvent;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.derivations.impl.MappingDerivationsImpl;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.inter.impl.ConsumerSupplier;
import uk.ac.york.qvtd.dependencies.util.QvtcIterator;

/**
 * An execution Listener that evaluates if a given execution plan is sound. Soundness is a stricter viability evaluation
 * as it requires all, as opposed to some, requisites to have been executed.
 *
 * A plan is sound if for all actions:
 * <ul>
 * <li> The invocation stack only contains the closure of prerequisites of the action, i.e. any invocation of a non
 * 		prerequisite is invalid because nested depend on success of parent invocation.
 * <li> All the prerequisites of the action should exist in the current invocation stack or in the visited actions.
 * <li> If the action has no prerequisites, the call stack must be empty, i.e. a mapping with no dependencies can not be
 *      invoked from another mapping because it may be restricted by guards.
 * <li> For all consumed types with more than one parameter, the same number of available elements of the type must
 * 	    exist, i.e. the producer creates two elements of the type, or a previous action has created other ones.
 * </ul>
 * @author Horacio Hoyos
 */
public class SoundnessEvaluator extends AbstractExecutionPlanEvaluator {

    public SoundnessEvaluator(ExecutionPlanGraph plan) {
        super(plan);
//        this.classrel = classrel;
    }

    /* (non-Javadoc)
     * @see uk.ac.york.qvtd.compiler.evaluation.AbstractExecutionPlanEvaluator#edgeTraversed(org.jgrapht.event.EdgeTraversalEvent)
     */
    @Override
    public void edgeTraversed(EdgeTraversalEvent<Invocation> e) {
        // If there is no containment relation between any two of the input variables, then we can not
        // guarantee that a nested invocation will provide valid bindings for all the variables.
        // For example, if rule A consumes and X and a Y, and Y is not the type of a containment property
        // of X, then if we bind Y to X.cont_prop we will not be considering all the possible cases of Y
        // Lemma. If the mapping's derivation graph is not strongly connected, then a containment property must
        // exist for each subgraph

        /* V2. Containment is not enough. I think is more related to the concept of traces. If the derivation graph
         is not strongly connected, then if in each subgraph there is a trace derivation and the L types have the
         same type of container, we need a root invocation.
         */
        if (valid) {
            super.edgeTraversed(e);
            Invocation inv = e.getEdge();
            CallActionOperation lastOp = plan.getOperationForMapping(callStack.peekFirst());
            if (!lastOp.isRoot()) {	// There can be n loop invocations before
                if (!inv.getSource().equals(inv.getTarget())) {		// Disallow loops
                    Operation target = inv.getTarget();
                    if (target instanceof CallActionOperation) {
                        MappingAction ma = ((CallActionOperation) target).getAction();
                        MappingDerivationsImpl der = ma.getMappingDerivations();
                        valid &= !der.isFromRoot();
//                        KosarajuStrongConnectivityInspector<Variable, PropertyEdge> inspector = new KosarajuStrongConnectivityInspector<>(der);
////                        if (inspector.isStronglyConnected()) {
//                            //valid &= derivationHasContainemnt(der);
////                            valid = true;
////                        } else {
//                            // TODO THIS CAN BE DONE ONCE WHEN CREATING THE DERIVATION GRAPH
//                            // FIXME WE need the config to know which is the L domain!!
//                        Error Or, we need to
//                            // Get all the M to L edges of each subgraph
//                            List<Variable> lVars = new ArrayList<Variable>();
//                            for (Set<Variable> dset : inspector.stronglyConnectedSets()) {
//                                Subgraph<Variable, PropertyEdge, MappingDerivationsImpl> sg = new Subgraph<>(der, dset);
//                                if (!sg.edgeSet().isEmpty()) {
//                                    List<PropertyEdge> mtolEdges = sg.edgeSet().stream()
//                                        .filter(pe -> pe.isFactual() && isMtoLEdge(der, pe)).collect(Collectors.toList());
//                                    if (mtolEdges.isEmpty()) {
//                                        return;		// If there are no MtoL edges in a subgraph, we can invoke
//                                    }
//                                    lVars.addAll(mtolEdges.stream().map(pe -> der.getEdgeTarget(pe)).collect(Collectors.toList()));
//                                }
//                            }
//                            // If all vars have the same containment type, we need a root invocation
//                            // We need to find the common supper type
//                            List<Type> lTypes = lVars.stream().map(v -> v.getType()).collect(Collectors.toList());
//                            if (lTypes.size() == 1) {	// There is no conflict
//                                return;
//                            }
//                            // For each type find the containing property
//                            List<Property> contProps = new ArrayList<Property>();
//                            for (Type t : lTypes) {
//                                if (t instanceof Class) {
//                                    List<Property> allProps = getAllProperties((Class) t);
//                                    contProps.addAll(allProps.stream()
//                                        .filter(prop -> (prop.getOpposite() != null) && prop.getOpposite().isIsComposite())
//                                        .collect(Collectors.toList()));
//                                }
//                            }
//                            if (contProps.size() != lTypes.size()) {
//                                // If there is not a containment property for each
//                            }
//                            // We need to find the common super type
//                            //Set<EObject> contTypes = contProps.stream().map(cp -> cp.getType()).collect(Collectors.toSet());
//                            Set<Class> contTypes = new HashSet<Class>();
//                            for (Type vType : lTypes) {
//                                Iterable<Class> containerClasses = classrel.allContainerClassesReverse((Class) vType);
//                                Class topContainer = containerClasses.iterator().next(); // It always has at least vType
//                                if (!topContainer.equals(vType)) {
//                                    contTypes.add(topContainer);
//                                }
//                            }
//                            // Group the types by package/Model
//                            Map<Object, List<EObject>> typesByPackage = contTypes.stream().
//                                    collect(Collectors.groupingBy(t -> t.eContainer()));
//                            valid &= typesByPackage.entrySet().stream()
//                                    .allMatch(en -> en.getValue().size()> 1);		// If more than 1 type, the containment is different, we can call
                            //lVars.stream().map(v -> v.getType().)
    //                		valid &= derivationHasContainemnt(sg);
    //            			if (!valid) break;
//                        }
                    }
                }
            }
        }
    }

    private EObject getTopContainer(EObject t) {
        EObject c = t.eContainer();
        if (c instanceof Model) {
            return t;
        }
        else {
            return getTopContainer(c);
        }

    }

    private List<Property> getAllProperties(Class cls) {
        return getAllSupers(cls).stream()
                .flatMap(c -> c.getOwnedProperties().stream())
                .collect(Collectors.toList());
    }

    private List<Class> getAllSupers(Class t) {

        List<Class> result = t.getSuperClasses().stream()
                .flatMap(sc -> getAllSupers(sc).stream())
                .collect(Collectors.toList());
        result.add(t);
        return result;
    }



    /**
     *
     * @param der
     * @return
     */
    private boolean derivationHasContainemnt(Graph<Variable, PropertyEdge> der) {
        for (PropertyEdge p : der.edgeSet()) {
            if (p.getProperty().isIsComposite()) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void vertexTraversed(VertexTraversalEvent<Operation> e) {
        //System.out.println("vertexTraversed " + e.getVertex().toString());
        // See if this invocation is valid
        if (valid) {
            valid = validateVertexTraversed(e);
            if (valid) {
                super.vertexTraversed(e);
            }
        }
    }

    /**
     * Remove items on list 2, that are in list 1, only once per match
     */
    private <T> void removeOneOccurrence(Collection<T> l1, Collection<T> l2) {
        Iterator<T> it = l2.iterator();
        for (T o1 : l1) {
          if (!it.hasNext()) {
            return;
          }
          Object o = it.next();
          while ((o == null && !(o1 == null)) || (o != null && !o.equals(o1))) {
            if (!it.hasNext()) {
              return;
            }
            it.remove();
            o = it.next();
          }
        }
    }

    /**
     * Used to evaluate the plan after each vertex is traversed. Checks that at least one of the dependency paths
     * is satisfied.
     * @param e
     */
    private boolean validateVertexTraversed(VertexTraversalEvent<Operation> e) {
        Operation vertex = e.getVertex();
        boolean candidate = false;
        if (vertex instanceof CallActionOperation) {
            CallActionOperation currentOperation = (CallActionOperation) vertex;
            List<MappingAction> currentCallStack = new ArrayList<MappingAction>();
            QvtcIterator it = (QvtcIterator) e.getSource();
            // We  need to know if we are in a loop. If we are in a loop, we need to validate the loop dependencies
            // too!
            //Set<List<MappingAction>> paths = plan.getPathsFromRoot(currentOperation);
            ConsumerSupplier si = plan.getDependencyGraph().getSupplierInformation(currentOperation.getAction());
//            if (!paths.isEmpty()) {
            if(!si.getAllSuppliers().isEmpty()) {
//                if (it.isLoop(currentOperation)) {
//                    // When we get here we are already inside the loop and this signals that we have encounter the loop
//                    // entry mapping again
//                    for (List<MappingAction> reqPath : paths) {
//                        // Try each reqPath with the augmented loop
//                        for (List<MappingAction> loopPath : plan.getDependencyGraph().getReducedLoopsFrom(currentOperation.getAction())) {
//                            loopPath.remove(0);		// The loop entry is duplicated
//                            reqPath.addAll(loopPath);
//                            currentCallStack.addAll(callStack);
//                            currentCallStack.add(0, currentOperation.getAction()); 		// Assume operation is invoked
//                            currentCallStack.removeAll(reqPath);
//                            candidate = currentCallStack.isEmpty(); 		// Only prerequisites on the stack
//                            if (candidate) {
//                                reqPath.removeAll(callStack); 				// Requirements missing
//                                reqPath.remove(currentOperation.getAction());			// Assume operation is invoked
//                                if (!reqPath.isEmpty()) {
//                                    reqPath.removeAll(visited);				// Requirements missing in the call stack
//                                    candidate = reqPath.isEmpty();
//                                }
//                            }
//                            if (candidate)
//                                break;
//                            currentCallStack.clear();
//                        }
//
//                    }
//                } else {
//                    for (List<MappingAction> reqPath : paths) {
//                        currentCallStack.addAll(callStack);
//                        currentCallStack.add(currentOperation.getAction()); 		// Assume operation is invoked
//                        currentCallStack.removeAll(reqPath);
//                        candidate = currentCallStack.isEmpty(); 		// Only prerequisites on the stack
//                        if (candidate) {
//                            reqPath.removeAll(callStack); 				// Requirements missing
//                            reqPath.remove(currentOperation.getAction());			// Assume operation is invoked
//                            if (!reqPath.isEmpty()) {
//                                reqPath.removeAll(visited);				// Requirements missing in the call stack
//                                candidate = reqPath.isEmpty();
//                            }
//                        }
//                        if (candidate)
//                            break;
//                        currentCallStack.clear();
//                    }
                    //1. The invocation is from a producer
                    MappingAction lastOp = callStack.peekFirst();
//                    ConsumerSupplier si = plan.getDependencyGraph().getSupplierInformation(currentOperation.getAction());
                    if (si.getIndirectSuppliers().contains(lastOp)) {
                        // Can not be invoked from an indirect relation (multi-value) unless there is another
                        // invocation that provides the same type
                        Set<ClassDatum> indirectTypes = si.getIndirectTypesBySuplier(lastOp);
                        List<MappingAction> invokers = plan.incomingEdgesOf(currentOperation).stream()
                                    .map(edge -> edge.getSource())
                                    .map(CallActionOperation.class::cast)
                                    .map(caop -> caop.getAction())
                                    .collect(Collectors.toList());
                        invokers.remove(lastOp);
                        candidate = true;
                        for (ClassDatum dt : indirectTypes) {
                            Set<MappingAction> allSuppliers = si.getAllSuppliers(dt);
                            candidate &= invokers.stream().anyMatch(i -> allSuppliers.contains(i));
                        }
                    }
                    else if(si.getDirectSuppliers().contains(lastOp)) {
                        // Can invoke if for each consumed type, at least one producer has been invoked
                        candidate = true;
                        for (ClassDatum ct : si.getConsumedTypes()) {
                            Set<MappingAction> sup = new HashSet<MappingAction>(si.getDirectSuppliers(ct));
                            if (sup.isEmpty()) {
                                continue;
                            }
                            candidate = sup.stream().anyMatch(s ->callStack.contains(s));
                            if (!candidate) {
                                candidate = sup.stream().anyMatch(s ->visited.contains(s));
                            }
                            if (!candidate) {
                                break;
                            }
                        }
//                        candidate = .stream()
//                            .allMatch(ct -> si.getDirectSuppliers(ct).stream()
//                                    .filter(ds -> ds != lastOp)
//                                    .anyMatch(s -> visited.contains(s)));
//                        HashSet<MappingAction> missingSuppliers = new HashSet<MappingAction>(si.getDirectSuppliers());
//                        missingSuppliers.remove(lastOp);
//                        candidate = visited.containsAll(missingSuppliers);
                    }
                    else {
                        // Can not be invoked from a non-suplier, unless it has to be invoked from the root and the lastOp
                    	// is the root
                    	if (lastOp == plan.getDependencyGraph().getRootVertex()) {
                    		candidate = currentOperation.getAction().getMappingDerivations().isFromRoot();
                    	}
                    	else {
                    		candidate = false;
                    	}
                    }
//                }
            }
            else {
                candidate = callStack.isEmpty(); // If no paths (i.e. no requisites, the call stack must be empty)
            }
        } else {
            // Plans with loop operations!
            candidate = true;
        }
        return candidate;
    }


}