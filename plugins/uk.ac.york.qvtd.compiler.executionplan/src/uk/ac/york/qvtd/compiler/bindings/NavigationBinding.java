package uk.ac.york.qvtd.compiler.bindings;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Variable;

public class NavigationBinding extends AbstractBinding {

    /** The navigation. */
    private final List<Property> navigation;

    /** The source variable. */
    private Variable sourceVariable;

    /**
     * @param boundVariable
     * @param sourveVariable
     * @param cost
     * @param navigation
     */
    public NavigationBinding(Variable boundVariable, Variable sourveVariable, long cost, List<Property> navigation) {
        super(cost, boundVariable);
        this.navigation = new ArrayList<Property>(navigation);
        this.sourceVariable = sourveVariable;
    }

    @Override
    public Variable getSourceVariable() {
        return sourceVariable;
    }


    public void setSourveVariable(Variable sourveVariable) {
        this.sourceVariable = sourveVariable;
    }


    /**
     * Gets the navigation.
     *
     * @return the navigation
     */
    public List<Property> getNavigation() {
        return navigation;
    }

    public String getNavigationString() {
        StringBuilder buff = new StringBuilder();
        String sep = ".";
        for (Property str : navigation) {
            buff.append(sep);
            buff.append(str.getName());
            sep = ".";
        }
        return buff.toString();
    }

    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder();
        buff.append(boundVariable.getName());
        buff.append(" = ");
        buff.append(sourceVariable.getName());
        buff.append(getNavigationString());
        return buff.toString();
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((navigation == null) ? 0 : navigation.hashCode());
        result = prime * result + ((sourceVariable == null) ? 0 : sourceVariable.hashCode());
        return result;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof NavigationBinding))
            return false;
        NavigationBinding other = (NavigationBinding) obj;
        if (navigation == null) {
            if (other.navigation != null)
                return false;
        } else if (!navigation.equals(other.navigation))
            return false;
        if (sourceVariable == null) {
            if (other.sourceVariable != null)
                return false;
        } else if (!sourceVariable.equals(other.sourceVariable))
            return false;
        return true;
    }


}
