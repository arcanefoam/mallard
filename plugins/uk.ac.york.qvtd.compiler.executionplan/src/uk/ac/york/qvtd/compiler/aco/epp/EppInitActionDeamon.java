/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.aco.AntColony;
import uk.ac.york.qvtd.compiler.aco.DaemonAction;
import uk.ac.york.qvtd.compiler.aco.DaemonActionType;

/**
 * The Class EppInitAction.
 */
public class EppInitActionDeamon extends DaemonAction<InvocationEdge, ForagingEdge> {

    private static Logger logger = LoggerFactory.getLogger(EppInitActionDeamon.class);

    /**
     * Instantiates a new epp init action.
     *
     * @param acoPhase the aco phase
     */
    public EppInitActionDeamon() {
        super(DaemonActionType.INITIAL_CONFIGURATION);
    }

    @Override
    public void applyDaemonAction(AntColony<InvocationEdge, ForagingEdge> antColony) {
        logger.trace("INITIALIZING PHEROMONE MATRIX");

        double initialPheromoneValue = getInitialPheromoneValue(antColony);
        if (initialPheromoneValue == 0.0) {
            logger.warn("An initial pheromone value of zero can affect the node selection process.");
        }

        logger.trace("Initial pheromone value: " + initialPheromoneValue);

        antColony.getEnvironment().setPheromoneValues(initialPheromoneValue);
        logger.trace("Pheromone values after initilizatation :" + antColony.getEnvironment().getPheromoneValues());
    }

    private double getInitialPheromoneValue(AntColony<InvocationEdge, ForagingEdge> antColony) {
        EppConfiguration config = (EppConfiguration) antColony.getConfigurationProvider();
        return config.getInitialPheromoneValue();
    }



}
