package uk.ac.york.qvtd.compiler.evaluation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.jgrapht.event.VertexTraversalEvent;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;

/**
 * An execution Listener that evaluates if a given execution plan is viable.
 *
 * A plan is viable if for all actions:
 * <ul>
 * <li> The invocation stack only contains the closure of prerequisites of the action, i.e. any invocation of a non
 * 		prerequisite is invalid because nested depend on success of parent invocation.
 * <li> Some of the prerequisites of the action are in the current invocation stack or the visited actions.
 * <li> If the action has no prerequisites, the call stack must be empty, i.e. a mapping with no dependencies can not be
 *      invoked from another mapping because it may be restricted by guards.
 * </ul>
 *
 * @author Horacio Hoyos
 */
public class ViabilityEvaluator extends AbstractExecutionPlanEvaluator {


    public ViabilityEvaluator(ExecutionPlanGraph plan) {
        super(plan);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void vertexTraversed(VertexTraversalEvent<Operation> e) {
        //System.out.println("vertexTraversed " + e.getVertex().toString());
        // See if this invocation is valid
        valid = validateVertexTraversed(e);
        if (valid) {
            super.vertexTraversed(e);
        }
    }

    /**
     * Used to evaluate the plan after each vertex is traversed. Checks that at least one of the dependency paths
     * is satisfied.
     * @param vertexTraversalEvent
     */
    private boolean validateVertexTraversed(VertexTraversalEvent<Operation> vertexTraversalEvent) {
        Operation vertex = vertexTraversalEvent.getVertex();
        boolean candidate = false;
        if (vertex instanceof CallActionOperation) {
            CallActionOperation caop = (CallActionOperation) vertex;
            List<MappingAction> currentCallStack = new ArrayList<MappingAction>();
            List<DependencyVertex> reqPath = new ArrayList<DependencyVertex>();
            Set<List<MappingAction>> paths = plan.getPathsFromRoot(caop);
            if (!paths.isEmpty()) {
                for (List<MappingAction> path : paths) {
                    currentCallStack.clear();
                    reqPath.clear();
                    currentCallStack.addAll(callStack);
                    reqPath.addAll(path);
                    currentCallStack.removeAll(reqPath);
                    candidate = currentCallStack.isEmpty(); // Only prerequisites on the stack
//                    if (candidate && !req.isEmpty()) {
//                        // Even if not in the current call stack, all of the prerequisites have been invoked, i.e. they
//                        // are on the visited set. The purpose of this check is that there is a possibility that there
//                        // are available objects for all inputs of the mapping.
//                        //Set<MappingAction> visitedSet = new HashSet<MappingAction>(visited);
//                        //visitedSet.removeAll(req);
//                        req.removeAll(visited);
//                        candidate &= req.isEmpty();
//                    }
                    if (candidate)
                        break;
                }
            } else {
                candidate = callStack.isEmpty(); // If no paths (i.e. no requisites, the call stack must be empty)
            }
        } else {
            // Plans with loop operations!
            candidate = true;
        }
        return candidate;
    }

}