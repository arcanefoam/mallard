/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.qvtd.pivot.qvtbase.Domain;
import org.eclipse.qvtd.pivot.qvtcorebase.Area;
import org.eclipse.qvtd.pivot.qvtcorebase.RealizedVariable;
import org.jgrapht.GraphPath;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.ext.StringEdgeNameProvider;
import org.jgrapht.ext.StringNameProvider;
import org.jgrapht.graph.DirectedPseudograph;

import uk.ac.york.qvtd.compiler.executionplan.utilities.DependencyGraphUtils;
import uk.ac.york.qvtd.compiler.executionplan.utilities.ExecutionPlanUtils;
import uk.ac.york.qvtd.dependencies.derivations.DerivationGraph;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.derivations.impl.MappingDerivationsImpl;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.DependencyEdge;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.inter.ParameterEdge;
import uk.ac.york.qvtd.dependencies.util.GraphAttributes;
import uk.ac.york.qvtd.dependencies.util.MallardDOTExporter;

/**
 * The Class InvocationGraph. Nodes represent mapping actions and edges alternative ways to invoke the consumer
 * mappings.
 */
public class InvocationGraph extends DirectedPseudograph<MappingAction, InvocationEdge> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 5710462746841695399L;

    /** The dg. */
    private final DependencyGraph dg;

    /**
     * Instantiates a new invocation graph.
     *
     * @param dg the dg
     */
    public InvocationGraph(DependencyGraph dg) {
        super(InvocationEdge.class);
        this.dg = dg;
        for (DependencyVertex mv : dg.vertexSet()) {
            if (mv instanceof MappingAction) {
                MappingAction source = (MappingAction) mv;
                addVertex(source);
                for (DependencyEdge pe : dg.outgoingEdgesOf(source)) {
                    DependencyVertex type = pe.getTarget();
                    addEdges(dg, type, source);
                }
            }
        }
    }

    /**
     * Gets the dg.
     *
     * @return the dg
     */
    public DependencyGraph getDependencyGraph() {
        return dg;
    }


    /**
     * To dot.
     *
     * @return the string
     */
    public String toDOT() {
        MallardDOTExporter<MappingAction, InvocationEdge> ex = new MallardDOTExporter<MappingAction, InvocationEdge>(
                new GraphAttributes<MappingAction, InvocationEdge>(false),
                new IntegerNameProvider<MappingAction>(),
                new StringNameProvider<MappingAction>(),
                new StringEdgeNameProvider<InvocationEdge>());
        StringWriter stringWriter = new StringWriter();
        ex.export(stringWriter, this);
        return stringWriter.toString();
    }

    /**
     * Adds the edges.
     *
     * @param dg the dg
     * @param type the type
     * @param source the source
     */
    private void addEdges(DependencyGraph dg, DependencyVertex type, MappingAction source) {

        for (DependencyEdge te : dg.outgoingEdgesOf(type)) {
            DependencyVertex target = te.getTarget();
            if (target instanceof ClassDatum) {
                addEdges(dg, target, source);
            }
            else {
                MappingAction tma = (MappingAction) target;
                addVertex(tma);
                MappingDerivationsImpl derivations = tma.getMappingDerivations();
                for (Variable primary : derivations.vertexSet()) {
                    if (((ParameterEdge) te).getVariables().contains(primary)) {
                        boolean addVertex = false;
                        // We alwyas add the vertex because the te in the dg implies that a variable must exist!
                        List<Variable> unBound = new ArrayList<Variable>();
                        List<GraphPath<Variable, PropertyEdge>> paths =
                                DependencyGraphUtils.getDerivationPaths(
                                        primary, derivations, unBound);
                        InvocationEdge inv = new InvocationEdge(dg.getRootVertex().equals(source),
                                source, tma);
                        inv.addPrimaryVariable(primary);
                     // Can we derive from the unbound? Yes we can, but then that means that we would probably need
                        // another plan for each alternative binding form the unbound
//                        	List<Variable> unBound2 = new ArrayList<Variable>();
//                        	List<GraphPath<Variable, PropertyEdge>> paths2 = DependencyGraphUtils.getDerivationPaths(
//                                    uv, derivations, unBound2);
//                        	derivations.findMinimumSpanningTrees();
                        unBound.forEach(ub -> inv.addPrimaryVariable(ub));
                        for (GraphPath<Variable, PropertyEdge> path : paths) {
                            Variable derived = path.getEndVertex();
                            inv.addDerivation(primary, derived, path);
                        }
                        inv.calcualteCost();
                        // Only add edge if it is the best one
                        InvocationEdge prevEdge = getEdge(source, tma);
                        if (prevEdge == null) {
                            addEdge(source, tma, inv);
                        }
                        else {
                            if (prevEdge.getWeight() > inv.getWeight()) {
                                removeEdge(prevEdge);
                                addEdge(source, tma, inv);
                            }
                        }
                    }
                }
            }
        }
    }
}
