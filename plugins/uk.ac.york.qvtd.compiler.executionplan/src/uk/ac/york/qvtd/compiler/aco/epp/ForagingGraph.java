/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;

import org.jgrapht.graph.SimpleDirectedGraph;

/**
 * The Class ForagingGraph.
 */
public class ForagingGraph extends SimpleDirectedGraph<InvocationEdge, ForagingEdge> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6708980292446544599L;

    /**
     * Instantiates a new foraging graph.
     */
    public ForagingGraph() {
        super(ForagingEdge.class);
    }

}
