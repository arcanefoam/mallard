/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Variable;
import org.jgrapht.GraphPath;
import org.jgrapht.graph.GraphPathImpl;

import uk.ac.york.qvtd.compiler.executionplan.ExecutionPlan;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;

/**
 * An invocation edge represents a possible invocation from one mapping to another.
 * <p>
 * Each invocation edge has one or more primary variables, one or more secondary variables
 * and a list of paths that define how the secondary variables can be derived from the primary ones.
 * @author Horacio
 *
 */
public class InvocationEdge {

    /** The primary. */
    private final List<Variable> primary = new ArrayList<Variable>();

    /** The derivations. */
//    private final Map<Pair<Variable, Variable>, GraphPath<Variable, PropertyEdge>> derivations
//            = new HashMap<Pair<Variable,Variable>, GraphPath<Variable, PropertyEdge>>();
    private final List<Derivation> derivations = new ArrayList<Derivation>();

    /** The weight. */
    private double weight = -1;

    /** The from root. */
    private final boolean fromRoot;

    /** The source. */
    private final MappingAction source;

    /** The target. */
    private final MappingAction target;

    private List<Variable> unmodifiablePrimary;

    /**
     * Instantiates a new invocation edge.
     *
     * @param fromRoot the from root
     */
    public InvocationEdge(boolean fromRoot) {
        this.fromRoot = fromRoot;
        this.source = null;
        this.target = null;
    }

    /**
     * Instantiates a new invocation edge.
     *
     * @param fromRoot the from root
     * @param source the source
     * @param target the target
     */
    public InvocationEdge(boolean fromRoot, MappingAction source, MappingAction target) {
        this.fromRoot = fromRoot;
        this.source = source;
        this.target = target;
    }


    /**
     * Adds the derivation.
     *
     * @param primary the primary
     * @param derived the derived
     * @param path the navigation
     */
    public void addDerivation(Variable primary, Variable derived, GraphPath<Variable, PropertyEdge> path) {
        // If a property is a multi-value property, we need to modify the path to start after the
        // property and find the Derivation at which this one will start
        Derivation d;
        if (path.getEdgeList().size() > 1 && path.getEdgeList().stream()
                .anyMatch(edge -> edge.getProperty().isIsMany())) {
            // There can be many multi-value property, but since paths are ordered by length, all previous multi
            // value properties should have been already used.
            d = createDerivation(derived, path);
        } else {
            // Primary variables added first and derivations in oder of length
            Derivation pd = derivations.stream().filter(der -> der.secondary.equals(primary)).findFirst().get();
            d = new Derivation(pd, derived, path);
        }
        derivations.add(d);
    }

    public void addPrimaryVariable(Variable v) {
        primary.add(v);
        Derivation d = new Derivation(null, v, null);
        derivations.add(d);
    }



    /**
     * Calculate cost.
     */
    public void calcualteCost() {

        double w;
        long mvl = 0;					// Multi-value loops
        int bc = primary.size();		// 1 per primary variable
        if (derivations.size() > 0) {
            Iterator<Derivation> it = derivations.iterator();
            // We need to do something as when doing the bindings because we need to break the mv properties into
            // "loops" and the derivation starts there (i.e. using the loop iterator)
            while (it.hasNext()) {
                Derivation der = it.next();
                if (der.primary != null) {
                    mvl += der.getPath().getEdgeList().stream()
                            .map(pe -> pe.getProperty())
                            .filter(p -> p.isIsMany())
                            .count();
                    bc += der.getPath().getEdgeList().stream()
                            .mapToDouble(pe -> getNavigationCost(pe.getProperty()))
                            .sum();
                }
            }
        }
        int ail = 0;
        if (fromRoot) {
            ail = primary.size();		// Each primary is an allInstances loop
        }
        w = bc*ail + Math.pow(bc, mvl);
        //weight = Math.log(w);
        weight = w;
    }


    /**
     * @return the derivations
     */
    public List<Derivation> getDerivations() {
        return derivations;
    }

    /**
     * Get a map of variables derived from the primary and the navigation properties, the entries are ordered
     * according to the length of the navigation
     * @param primary
     * @return
     */
    public List<Derivation> getDerivedVariables(Variable primary) {
        Derivation pd = derivations.stream().filter(der -> der.secondary.equals(primary)).findFirst().get();
        return derivations.stream()
            .filter(der -> der.primary.equals(pd))
            .sorted(new Comparator<Derivation>() {

                    @Override
                    public int compare(Derivation o1, Derivation o2) {
                        // Reverse order to get high to low
                        return Integer.compare(o2.getPath().getEdgeList().size(), o1.getPath().getEdgeList().size());
                    }
                })
            .collect(Collectors.toList());
    }

    /**
     * Gets the list of primary variables. Returns an unmodifiable view of the list. To add a
     * primary variable use {@link #addPrimaryVariable(Variable)}
     *
     * @return an unmodifiable view of the primary variables
     */
    public List<Variable> getPrimary() {
        if (unmodifiablePrimary == null) {
            unmodifiablePrimary = Collections.unmodifiableList(primary);
        }
        return unmodifiablePrimary;
    }

    /**
     * Gets the source.
     *
     * @return the source
     */
    public MappingAction getSource() {
        return source;
    }

    /**
     * Gets the target.
     *
     * @return the target
     */
    public MappingAction getTarget() {
        return target;
    }

    /**
     * Gets the weight.
     *
     * @return the cost
     */
    public double getWeight() {
        if (weight < 0) {
            calcualteCost();
        }
        return weight;
    }


    /**
     * Checks if is from root.
     *
     * @return the fromRoot
     */
    public boolean isFromRoot() {
        return fromRoot;
    }

    @Override
    public String toString() {
        return String.valueOf(weight);
    }
    /**
     * Since a multivalue represents a loop, derivations need to start again using the loop iterator!
     * @param derived
     * @param path
     * @return
     */
    private Derivation createDerivation(Variable derived, GraphPath<Variable, PropertyEdge> path) {

        assert path.getEdgeList().size() > 1;
        ArrayList<PropertyEdge> tempEdgeList = new ArrayList<PropertyEdge>(path.getEdgeList());
        Collections.reverse(tempEdgeList);
        PropertyEdge mEdge = tempEdgeList.stream().filter(edge -> edge.getProperty().isIsMany()).findFirst().get();
        int fromIndex = path.getEdgeList().indexOf(mEdge)+1;
        int toIndex = path.getEdgeList().size();
        Derivation derivation = null;
        if (fromIndex < toIndex) {
            List<PropertyEdge> newEdgeList = path.getEdgeList().subList(fromIndex, toIndex);
            Variable startVertex = path.getGraph().getEdgeSource(newEdgeList.get(0));
            Derivation pd = derivations.stream().filter(der -> der.secondary.equals(startVertex)).findFirst().get();
            GraphPathImpl<Variable, PropertyEdge> newPath = new GraphPathImpl<Variable, PropertyEdge>(path.getGraph(), startVertex, derived, newEdgeList, newEdgeList.size());
            derivation = new Derivation(pd, derived, newPath);
        }
        else {
            Variable startVertex = path.getGraph().getEdgeSource(path.getEdgeList().get(0));
            Derivation pd = derivations.stream().filter(der -> der.secondary.equals(startVertex)).findFirst().get();
            derivation = new Derivation(pd, derived, path);
        }
        return derivation;
    }

    /**
     * Gets the navigation cost.
     *
     * @param property the property
     * @return the navigation cost
     */
    private double getNavigationCost(Property property) {

        return property.isIsImplicit()? ExecutionPlan.IMPLICIT_NAVIGATION_COST : ExecutionPlan.NAVIGATION_COST;
        //return property.isIsImplicit()? 5 : 1;
    }

    public class Derivation {

        final Derivation primary;
        final Variable secondary;
        final GraphPath<Variable, PropertyEdge> path;

        protected Derivation(Derivation primary, Variable secondary, GraphPath<Variable, PropertyEdge> path) {
            super();
            this.primary = primary;
            this.secondary = secondary;
            this.path = path;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Derivation other = (Derivation) obj;
            if (!getOuterType().equals(other.getOuterType()))
                return false;
            if (path == null) {
                if (other.path != null)
                    return false;
            } else if (!path.getEdgeList().equals(other.path.getEdgeList()))
                return false;
            if (primary == null) {
                if (other.primary != null)
                    return false;
            } else if (!primary.equals(other.primary))
                return false;
            if (secondary == null) {
                if (other.secondary != null)
                    return false;
            } else if (!secondary.equals(other.secondary))
                return false;
            return true;
        }

        /**
         * @return the path
         */
        public GraphPath<Variable, PropertyEdge> getPath() {
            return path;
        }

        /**
         * @return the primary
         */
        public Derivation getPrimary() {
            return primary;
        }

        /**
         * @return the secondary
         */
        public Variable getSecondary() {
            return secondary;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + ((path == null) ? 0 : path.getEdgeList().hashCode());
            result = prime * result + ((primary == null) ? 0 : primary.hashCode());
            result = prime * result + ((secondary == null) ? 0 : secondary.hashCode());
            return result;
        }

        private InvocationEdge getOuterType() {
            return InvocationEdge.this;
        }


    }

}
