/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.aco.AbstractAntPolicy;
import uk.ac.york.qvtd.compiler.aco.Ant;
import uk.ac.york.qvtd.compiler.aco.AntPolicyType;


/**
 * The selection policy for Ant Colony System algorithms. It defines to selection rules.
 * <p>
 * <ul>
 * <li>The first is the best selection rule. It picks the component with the better heuristic and pheromone
 * 		information.
 * <li>The other one is the usual ACO policy, selecting based on the probabilities of each possible component.
 * <li>
 * </ul>
 * <p>
 * <p>
 * Using one or another is determined by a "best-choice" probability.
 *
 * @author Carlos G. Gavidia
 */
public class EppNodeSelection extends AbstractAntPolicy<InvocationEdge, ForagingEdge> {

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(EppNodeSelection.class.getName());

    /** The random data. */
    RandomDataGenerator randomData = new RandomDataGenerator();

    /**
     * Instantiates a new epp node selection.
     */
    public EppNodeSelection() {
        super(AntPolicyType.NODE_SELECTION);

    }

    @Override
    public boolean applyPolicy(Ant<InvocationEdge, ForagingEdge> ant) {

        boolean nodeWasSelected = false;
        EppAnt thisAnt = (EppAnt) ant;
        ForagingEdge usedEdge = null;

        HashMap<ForagingEdge, Double> componentsWithProbabilities = getComponentsWithProbabilities(thisAnt);
        if (componentsWithProbabilities.isEmpty()) {
            doIfNoComponentsFound(thisAnt); // Is not critical in our case
            nodeWasSelected = true;
        }
        else {
            if (useExploitation(thisAnt)) {
                logger.trace("Selecting the greedy choice");
                usedEdge = getMostConvenient(componentsWithProbabilities);
            }
            else {
                logger.trace("Selecting the probabilistic choice");
                usedEdge = getRandomEdge(componentsWithProbabilities);
            }
        }
        if (usedEdge != null) {
            nodeWasSelected = true;
            thisAnt.visitNode(usedEdge);
        }
        return nodeWasSelected;
    }

    /**
     * Do if no components found. For the Epp, this means that the ant can't add more invocations to the execution plan,
     * however it does not imply an error.
     *
     * @param ant the ant
     */
    private void doIfNoComponentsFound(EppAnt ant) {
//        logger.info("The ant can't construct the ExecutionPlan any further");
//        throw new IllegalStateException(
//        "We have no suitable components to add to the solution from current position."
//        + "\n Previous Component: "
//        + ant.getTrail().getEndVertex()
//        + "\n Environment: " + ant.getColony().getEnvironment().toString()
//        + "\nPartial solution : " + ant.getSolutionAsString());
        }

    /**
     * Gets the Tao Eta value for each edge in the ants neigboorhood.
     *
     * @param thisAnt the this ant
     * @return the components with probabilities
     */
    private HashMap<ForagingEdge, Double> getComponentsWithProbabilities(EppAnt thisAnt) {
        HashMap<ForagingEdge, Double> result = new HashMap<>();
        double sum = 0.0;
        Double totalProbability = 0.0;	// Assert test
        Set<ForagingEdge> neighbourhood = new HashSet<ForagingEdge>(thisAnt.getNeighbourhood());
        if (!neighbourhood.isEmpty()) {
            for (ForagingEdge possibleEdge : neighbourhood) {
                double taoEta = getTaoEta(thisAnt, possibleEdge);
                sum += taoEta;
                result.put(possibleEdge, taoEta);
            }
            Iterator<Entry<ForagingEdge, Double>> rIt = result.entrySet().iterator();
            while (rIt.hasNext()) {
                Entry<ForagingEdge, Double> entry = rIt.next();
                double newValue = entry.getValue();
                newValue /= sum;
                entry.setValue(newValue);
                totalProbability += newValue;
            }
            double delta = 0.001;
            if (Math.abs(totalProbability - 1.0) > delta) {
                logger.error("The sum of probabilities for the possible components is " +
                        totalProbability + ". We expect this value to be closer to 1.");
            }
        }
        return result;
    }

    /**
     * The most convenient from Dorigo.Gambardella1997 is max{tao*eta}. However, since in componentsWithProbabilities
     * all values are divided by a constant, the max will be the same
     *
     * @param componentsWithProbabilities the components with probabilities
     * @return the most convenient
     */
    private ForagingEdge getMostConvenient(HashMap<ForagingEdge, Double> componentsWithProbabilities) {

        double max = -1;
        ForagingEdge nextEdge = null;
        Iterator<Entry<ForagingEdge, Double>> pIt = componentsWithProbabilities.entrySet().iterator();
        while (pIt.hasNext()) {
            Entry<ForagingEdge, Double> entry = pIt.next();
            if (entry.getValue() > max) {
                max = entry.getValue();
                nextEdge = entry.getKey();
            }
        }
        return nextEdge;
    }

    /**
     * Gets the random edge.
     *
     * @param componentsWithProbabilities the components with probabilities
     * @return the random edge
     */
    private ForagingEdge getRandomEdge(HashMap<ForagingEdge, Double> componentsWithProbabilities) {

        double probablity = randomData.getRandomGenerator().nextDouble();
        ForagingEdge nextEdge = null;
        Iterator<Entry<ForagingEdge, Double>> pIt = componentsWithProbabilities.entrySet().iterator();
        double total = 0;
        while (pIt.hasNext()) {
            Entry<ForagingEdge, Double> entry = pIt.next();
            total += entry.getValue();
            if (total >= probablity) {
                nextEdge = entry.getKey();
                break;
            }
        }
        return nextEdge;
    }

    /**
     * Gets the tao eta.
     *
     * @param ant the ant
     * @param possibleEdge the possible edge
     * @return the tao eta
     */
    private double getTaoEta(EppAnt ant, ForagingEdge possibleEdge) {
        double pheromone = possibleEdge.getPheromone();
        // The heuristic of a ForagingEdge is the inverse of the edge weight of the InvocationGraph that
        // corresponds to its target
        double heuristic = ant.getHeuristicValue(possibleEdge);
        return Math.pow(pheromone, ant.getColony().getConfigurationProvider().getPheromoneImportance())
                * Math.pow(heuristic, ant.getColony().getConfigurationProvider().getHeuristicImportance());
    }


    /**
     * Use exploitation.
     *
     * @param ant the ant
     * @return true, if successful
     */
    private boolean useExploitation(EppAnt ant) {
        double q0 = ant.getColony().getExploitationProbability();
        double q = randomData.getRandomGenerator().nextDouble();
        return q <= q0;
    }



}
