/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.context;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ocl.pivot.Type;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;
import org.eclipse.qvtd.pivot.qvtcorebase.RealizedVariable;
import org.jgrapht.EdgeFactory;
import org.jgrapht.graph.Pseudograph;

// TODO: Auto-generated Javadoc
/**
 * A context represents the available variables and the relations between
 * them. The context also keeps track of the properties of that have been
 * assigned, guards, etc.
 * @author Goblin
 *
 */
public class Context extends Pseudograph<ContextVertex, ContextEdge>{

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3032170723495049381L;

    /** The last match. */
    private List<ContextVertex> lastMatch = new ArrayList<ContextVertex>();

    /**
     * Instantiates a new context.
     *
     * @param ef the EdgeFactory
     */
    public Context(EdgeFactory<ContextVertex, ContextEdge> ef)
    {
        super(ef);
    }

    /**
     * Find realized variables that match the given typemodel and type
     *
     * @param type the type
     * @param typedModel the typed model
     * @return true, if successful
     *
     */
    public boolean findMatches(Type type, TypedModel typedModel) {
        lastMatch.clear();
        for (ContextVertex cv : vertexSet()) {
            // We only match realized variables!
            if (cv.getVariable() instanceof RealizedVariable)
                if (cv.getType().equals(type) && cv.getDomain().equals(typedModel)) {
                    lastMatch.add(cv);
                }
        }
        return !lastMatch.isEmpty();
    }

    /**
     * Gets the last match.
     *
     * @return the last match
     */
    public List<ContextVertex> getLastMatch() {
        return lastMatch;
    }

}
