/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;

import java.util.List;

/**
 * The Interface Ant.
 *
 * @param <V> the value type
 * @param <E> the element type
 */
public interface Ant<V, E> {

    /**
     * Associates the AntPolicy to its AntPolicyType. If the ant previously contained a mapping for the AntPolicyType,
     * the old value is replaced by the specified AntPolicy.
     *
     * @param antPolicy the ant policy
     * @return the previous value associated with key, or null if there was no mapping for AntPolicyType.
     */
    AbstractAntPolicy<V, E> addPolicy(AbstractAntPolicy<V, E> antPolicy);

    /**
     * Improves the quality of the solution produced.
     */
    void doAfterSolutionIsReady();

    /**
     * Gets the ant policy.
     *
     * @param policyType the policy type
     * @return the ant policy
     */
    AbstractAntPolicy<V, E> getAntPolicy(AntPolicyType policyType);

    /**
     * Gets the colony.
     *
     * @return the colony
     */
    AntColony<V, E> getColony();

    /**
     * Gets the current index for the Ant while constructing a solution.
     *
     * @return Current index.
     */
    V getCurrentVertex();

    /**
     * Calculates the heuristic contribution of the candidate move.
     *
     * @return Heurisitic contribution.
     */
    Double getHeuristicValue(V candidate);

    /**
     * The components that are available for selection while an Ant is constructing its solution.
     *
     * @return List of available components.
     */
    List<E> getNeighbourhood();

    /**
     * Gets th solution built as a String.
     *
     * @return Solution as a String.
     */
    String getSolutionAsString();

    /**
     * Calculates the cost associated to the solution build, which is needed to determine the performance of the Ant.
     *
     * @return The cost of the solution built.
     */
    double getSolutionCost();

    /**
     * Gets the solution found by this ant.
     *
     * @return the solution
     */
    ForagingTrail<V, E> getTrail();

    /**
     * Checks if is node valid.
     *
     * @param vertex the vertex
     * @return true, if is node valid
     */
    boolean isNodeValid(V vertex);

    /**
     * Verifies if a component. is already included in the solution.
     *
     * @param vertex the vertex
     * @return True if the node is already visited. False otherwise.
     */

    boolean isNodeVisited(V vertex);

    /**
     * Returns true when the solution build by the current Ant is finished.
     *
     * @return True if the solution is finished, false otherwise.
     */
    boolean isSolutionReady();

    /**
     * Resets the ant.
     */
    void reset();

    /**
     * Selects a node and marks it as visited.
     */
    void selectNextNode();

    /**
     * Sets the trail.
     *
     * @param trail the trail
     */
    void setTrail(ForagingTrail<V, E> trail);

    /**
     * Mark a node as visited.
     *
     * @param edgeUsed the edge used to get to the visited node
     */
    void visitNode(E edgeUsed);

}