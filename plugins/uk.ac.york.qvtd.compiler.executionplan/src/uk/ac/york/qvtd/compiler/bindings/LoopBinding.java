/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.bindings;

import org.eclipse.ocl.pivot.Variable;

import uk.ac.york.qvtd.compiler.executionplan.LoopOperation;

/**
 * A Loop Binding represents a binding which source comes from a loop.
 */
public class LoopBinding extends AbstractBinding {

    /** The source loop. */
    private LoopOperation sourceLoop; // WE can know from which loop it comes

    /**
     * Instantiates a new loop binding.
     *
     * @param boundVariable the bound variable
     * @param sourceLoop the source loop
     * @param cost
     */
    public LoopBinding(Variable boundVariable, LoopOperation sourceLoop, long cost) {
        super(cost, boundVariable);
        this.sourceLoop = sourceLoop;
    }

    public LoopBinding(LoopBinding other) {
        this(other.getBoundVariable(), other.sourceLoop, other.getCost());
    }

    @Override
    public Variable getSourceVariable() {
        return sourceLoop.getLoopVariable();
    }

    public LoopOperation getSourceLoop() {
        return sourceLoop;
    }

    public void setSourceLoop(LoopOperation sourceLoop) {
        this.sourceLoop = sourceLoop;
    }

    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder();
        buff.append(boundVariable.getName());
        buff.append(" = ");
        buff.append(getSourceVariable().getName());
        return buff.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((sourceLoop == null) ? 0 : sourceLoop.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof LoopBinding))
            return false;
        LoopBinding other = (LoopBinding) obj;
        if (sourceLoop == null) {
            if (other.sourceLoop != null)
                return false;
        } else if (!sourceLoop.equals(other.sourceLoop))
            return false;
        return true;
    }



}