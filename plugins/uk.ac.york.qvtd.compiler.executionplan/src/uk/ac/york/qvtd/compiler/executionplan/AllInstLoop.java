/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.executionplan;

import org.eclipse.ocl.pivot.PivotFactory;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;

import uk.ac.york.qvtd.compiler.bindings.ModelType;

/**
 * The Class AllInstLoopOperation.
 */
public class AllInstLoop extends LoopOperation {

    private final ModelType modelType;

    /**
     * Instantiates a new all instances loop operation. A new loop variable
     * is created to represent the loop iterator.
     *
     * @param ModelType the type
     */
    public AllInstLoop(ModelType modelType, int index) {
        super();
        this.modelType = modelType;
        this.loopVariable = PivotFactory.eINSTANCE.createVariable();
        // TODO the name may need a counter to deal with multiple loops over the same type
        this.loopVariable.setName(modelType.getType().getName().toLowerCase() + "_" + "it" + "_" + index);
        this.loopVariable.setType(modelType.getType());
    }

    public AllInstLoop(Type type, TypedModel typedModel, int index) {
        this(new ModelType(type, typedModel), index);
    }

    /**
     * Copy Constructor
     */
    public AllInstLoop(AllInstLoop other) {
        super();
        modelType = other.modelType;
        loopVariable = PivotFactory.eINSTANCE.createVariable();
        // TODO the name may need a counter to deal with multiple loops over the same type
        loopVariable.setName(other.loopVariable.getName());
        loopVariable.setType(other.loopVariable.getType());
    }



    @Override
    public boolean isRoot() {
        // TODO Auto-generated method stub
        return false;
    }

    public ModelType getModelType() {
        return modelType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((modelType == null) ? 0 : modelType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof AllInstLoop))
            return false;
        AllInstLoop other = (AllInstLoop) obj;
        if (modelType == null) {
            if (other.modelType != null)
                return false;
        } else if (!modelType.equals(other.modelType))
            return false;
        return true;
    }

}
