/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import org.jgrapht.Graphs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.aco.AcoProblemSolver;
import uk.ac.york.qvtd.compiler.aco.Ant;
import uk.ac.york.qvtd.compiler.aco.AntColony;
import uk.ac.york.qvtd.compiler.aco.ConfigurationProvider;
import uk.ac.york.qvtd.compiler.aco.Environment;
import uk.ac.york.qvtd.compiler.aco.ForagingTrail;
import uk.ac.york.qvtd.compiler.executionplan.ExecutionPlan;


/**
 * The Class EppProblemSolver.
 */
public class EppProblemSolver extends AcoProblemSolver<InvocationEdge, ForagingEdge> {


    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(EppProblemSolver.class);

    private Set<ForagingTrail<InvocationEdge, ForagingEdge>> solutionSample;

    /**
     * @return the solutionSample
     */
    public Set<ForagingTrail<InvocationEdge, ForagingEdge>> getSolutionSample() {
        return solutionSample;
    }



    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.AcoProblemSolver#initialize(org.eclipse.qvtd.compiler.aco.Environment, org.eclipse.qvtd.compiler.aco.AntColony, org.eclipse.qvtd.compiler.aco.ConfigurationProvider)
     */
    @Override
    public void initialize(Environment<InvocationEdge, ForagingEdge> environment,
            AntColony<InvocationEdge, ForagingEdge> colony, ConfigurationProvider config) {

        super.initialize(environment, colony, config);
        solutionSample = new HashSet<ForagingTrail<InvocationEdge, ForagingEdge>>(getSampleSize()/10);
    }



    /* (non-Javadoc)
     * @see uk.ac.york.qvtd.compiler.aco.AcoProblemSolver#solveProblem()
     */
    @Override
    public void solveProblem() throws IllegalAccessException {
        super.solveProblem();
        solutionSample.add(getBestSolution());
        Map<Double, Set<EppTrail>> perCost = ((EppAntColony) getAntColony()).getPlansPerCost();
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        String sep = "";
        for (Entry<Double, Set<EppTrail>> e : perCost.entrySet()) {
            sb.append(sep);
            sb.append('"');
            sb.append(e.getKey());
            sb.append('"');
            sb.append(':');
            sb.append(e.getValue().size());
            sep = ", ";
        }
        sb.append('}');
        logger.info("Plan Cost Distribution: {}", sb.toString());
        logger.info("EXECUTION FINISHED");
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.AcoProblemSolver#updateBestSolution(org.eclipse.qvtd.compiler.aco.Environment)
     */
    @Override
    public void updateBestSolution(Environment<InvocationEdge, ForagingEdge> environment) {

        super.updateBestSolution(environment);
        takeSample();
    }

    // Pick a random ant and save the plan. We only want to save max sampleSize/10.. so how to keep it even?
    private void takeSample() {
        if (solutionSample.size() < getSampleSize()/10) {
            Random r = new Random();
            double nextGaussian = r.nextGaussian();
            if (nextGaussian > 0.7) {
                Random antPicker = new Random((long) (nextGaussian*Long.MAX_VALUE));
                int index;
                try {
                    index = antPicker.nextInt(getConfigurationProvider().getNumberOfAnts());
                } catch (IllegalAccessException e) {
                    return;
                }
                Ant<InvocationEdge, ForagingEdge> ant = getAntColony().getHive().get(index);
                solutionSample.add(ant.getTrail());
            }
        }
    }


    /** Create a complete plan using the given solution
     *
     * @param solution
     * @return
     */
    public ExecutionPlan createPlan(ForagingTrail<InvocationEdge, ForagingEdge> solution) {
        // Each invocation edge has the information about the source and target mappings and the bindings
        ExecutionPlan ep = ((EppEnvironment) getEnvironment()).newExecutionPlan();
        Iterator<InvocationEdge> invIt = Graphs.getPathVertexList(solution).iterator();
        if (invIt.hasNext()) invIt.next();
        while (invIt.hasNext()) {
            InvocationEdge ie = invIt.next();
            ((EppEnvironment) getEnvironment()).createInvocationChain(ep, ie);
        }
        ep.calculateCost();
        return ep;
    }


}
