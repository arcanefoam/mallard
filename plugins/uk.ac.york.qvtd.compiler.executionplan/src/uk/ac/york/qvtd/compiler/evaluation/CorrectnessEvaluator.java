/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.evaluation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.annotation.NonNull;
import org.jgrapht.event.ConnectedComponentTraversalEvent;
import org.jgrapht.event.VertexTraversalEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.Binding;
import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionInvocation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.util.QvtcIterator;

/**
 * Evaluate if an Execution Plan is correct: no starvation and thorough.
 * An execution listener that keeps tracked of produced and consumed types to verify that the Execution Plan is
 * thorough. An Execution Plan is thorough if all the types produced are consumed for all producer-consumer relations.
 * When checking for consumed types it also validates that the plan does not starve.
 * <p>
 *
 * <p>
 * TODO Take into consideration predicates to know if there was a complete consumption of types, or it may have been
 * partial, i.e. a guard that filters how many types are consumed
 *
 * @author Horacio Hoyos
 *
 */
public class CorrectnessEvaluator extends AbstractExecutionPlanEvaluator {

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(CorrectnessEvaluator.class);

    /**
     * After a vertex is finished, all its produced types are considered generated, i.e. globally available
     */
    private final Set<ClassDatum> generatedTypes = new HashSet<ClassDatum>();

    /**
     * After a vertex is finished, any types that were not consumed by its children
     */
    private final Set<ClassDatum> remainderTypes = new HashSet<ClassDatum>();

    /**
     * Types that are not consumed, but where available in the super context
     */
    private final Set<ClassDatum> allInstancesConsumed = new HashSet<ClassDatum>();

    /**
     * A stack of ClassDatums produced by the last traversed vertex
     */
    private final Deque<List<ClassDatum>> typeContext = new ArrayDeque<List<ClassDatum>>();

    /**
     * A stack of ClassDatums produced from containment relations by the last traversed vertex
     */
    private final Deque<List<ClassDatum>> typeContainmentContext = new ArrayDeque<List<ClassDatum>>();

    /**
     * The ClassDatums that have been consumed in the current context
     */
    private final Deque<List<ClassDatum>> typeContextConsumed = new ArrayDeque<List<ClassDatum>>();

    // thorough = valid
    protected boolean noStarvation = true;

    //private ProdConsScores scores = new ProdConsScores();		// TODO This is required when binding


    public CorrectnessEvaluator(ExecutionPlanGraph plan) {
        super(plan);
        valid = false;	// By definition, if the visitor doesn't get to the connectedComponentFinished the plan is not thorough
    }

    /**
     * The Execution plan has been completely traversed, we can see if was thorough by verifying that all the types
     * produced where consumed.
     */
    @Override
    public void connectedComponentFinished(ConnectedComponentTraversalEvent e) {
        for (DependencyVertex v : plan.getDependencyGraph().vertexSet()) {
            if (v instanceof ClassDatum) {
                if (plan.getDependencyGraph().outDegreeOf(v) == 0) {
                    remainderTypes.remove((ClassDatum) v);
                }
            }
        }
        if (!remainderTypes.isEmpty() && !allInstancesConsumed.isEmpty()) {
            //logger.warn("Assuming types where consumed from super context. Not all instances might have been consumed.");
            remainderTypes.removeAll(allInstancesConsumed);
        }
        valid = remainderTypes.isEmpty();
    }


    public Set<ClassDatum> getRemainderTypes() {
        return remainderTypes;
    }

    public boolean isCorrect() {
        return noStarvation && valid;
    }

    public boolean isFeasible() {
        return noStarvation;
    }

    /**
     * When a vertex finishes, it removes its produced types from the pathTypeList and it makes it produced types
     * in the visitedTypeSet many
     */
    @Override
    public void vertexFinished(VertexTraversalEvent<Operation> e) {
        if (e.getVertex() instanceof CallActionOperation) {
            CallActionOperation currentOperation = (CallActionOperation) e.getVertex();
            QvtcIterator<Operation, Invocation> it = (QvtcIterator<Operation, Invocation>) e.getSource();
            if (!it.isLoop(currentOperation)) {		// If in loop, nothing was produced and hence we can't pop anything
                List<ClassDatum> consumedTypes = typeContextConsumed.pop();
                List<ClassDatum> producedTypes = typeContext.pop();
                typeContainmentContext.pop();
                generatedTypes.addAll(producedTypes);
                producedTypes.removeAll(consumedTypes);
                remainderTypes.addAll(producedTypes);
            }
        }
    }

    /**
     * When a vertex is traversed two things happen
     * <ul>
     * <li> It consumes one type for each input parameter
     * <li> it produces one type for each output parameter. Output parameters will immediately produce any contained
     *      types.
     * </ul>
     */
    @Override
    public void vertexTraversed(VertexTraversalEvent<Operation> e) {
        if (e.getVertex() instanceof CallActionOperation) {
            CallActionOperation currentOperation = (CallActionOperation) e.getVertex();
            noStarvation &= consumeTypes(currentOperation);
            super.vertexTraversed(e);
            QvtcIterator it = (QvtcIterator) e.getSource();
            if (!it.isLoop(currentOperation)) {
                produceTypes(currentOperation);
            }
        }
    }

    /**
     * The rule we navigated into consumes input types created in the same call stack, i.e. removes types from the
     * pathTypeList. If the rule can not consume all of it types, returns false
     * <p>
     * TODO To be completely accurate, if the vertex has guards, then we could say that the rules consumes "some" or
     * all of the the objects by type by analysing the guards and testing if the created objects meet the guard
     * conditions. THIs could be a "fully completeness evaluator".
     * @param graph
     * @param currentRule
     */
    private boolean consumeTypes(CallActionOperation operation) {

        List<ClassDatum> requriedTypes = plan.getDependencyGraph().getTypesConsumedByAction(operation.getAction());
        List<ClassDatum> consumedTypes = typeContextConsumed.peek();
        List<ClassDatum> contextTypes = typeContext.peek();
        List<ClassDatum> availableTypes = new ArrayList<ClassDatum>();

        if (contextTypes != null) {
            availableTypes.addAll(contextTypes);
        }
        contextTypes = typeContainmentContext.peek();
        if (contextTypes != null) {
            availableTypes.addAll(contextTypes);
        }
        // We need to check for the type, or any of its subtypes
        for (ClassDatum t : requriedTypes) {
            Set<@NonNull ClassDatum> greedyTypes = plan.getDependencyGraph().getAllSubClassDatums(t);
            greedyTypes.add(t);
            if (!availableTypes.stream().anyMatch(at -> greedyTypes.contains(at))) {
                // No types to consume from context
                // First try higher context (types are available, but are not directly consumed)
                List<ClassDatum> superContext = typeContext.stream().skip(1).flatMap(c -> c.stream()).collect(Collectors.toList());
                if (!superContext.stream().anyMatch(sc -> greedyTypes.contains(sc))) {
                    // Consume a type
//                	boolean remove = false;
//                	for (ClassDatum gt : greedyTypes) {
//                		remove = remainderTypes.removeAll(greedyTypes);
//                		if (remove) {
//                			break;
//                		}
//                	}
                    // Then remainder, then available globally
                    if (!remainderTypes.removeAll(greedyTypes)) {
                        if (!generatedTypes.stream().anyMatch(gt -> greedyTypes.contains(gt))) {
                            return false;
                        }
                    }
                }
//                else {	// Need to consume types if it is an AllInstances Loop
//                    Iterator<MappingAction> it = callStack.iterator();
//                    Invocation inv = null;
//                    while (it.hasNext()) {
//                        MappingAction sourceV = it.next();
//                        inv = plan.getEdge(plan.getOperationForMapping(sourceV), operation);
//                        if (inv != null) {
//                            break;
//                        }
//                    }
//                    if (inv != null) {
//                        for (Binding binding : ((CallActionInvocation) inv).getBindings().values()) {
//                            if (binding.getBoundVariable().getType().equals(t.getType())) {
//                                consumedTypes.add(t);
//                            }
//                        }
//                    }
//                }
            } else {
                consumedTypes.add(t);
                availableTypes.remove(t);
                //scores.statisfyRelation(plan.getOperationForMapping(callStack.peek()), operation);
            }
        }
        return true;
    }

    /**
     * We add a ClassDatum for each production edge to both the call and visited type lists.
     *
     * We need to keep track of the intended mapping too.
     * @param graph
     * @param operation
     */
    private void produceTypes(CallActionOperation operation) {

        List<ClassDatum> produced = new ArrayList<ClassDatum>();
        List<ClassDatum> contained = new ArrayList<ClassDatum>();
        MappingAction action = operation.getAction();
        DependencyGraph dg = plan.getDependencyGraph();
        for (ClassDatum t : dg.getTypesProducedByAction(action)) {
            produced.add(t);
        }
        // Types produced by containment don't go to the path type list
        for (ClassDatum t : dg.getContainedTypesProducedByAction(action)) {
            contained.add(t);
        }
        typeContext.push(produced);
        typeContainmentContext.push(contained);
        //generatedTypes.addAll(produced);
        //generatedTypes.addAll(contained);
        typeContextConsumed.push(new ArrayList<ClassDatum>());
    }
}
