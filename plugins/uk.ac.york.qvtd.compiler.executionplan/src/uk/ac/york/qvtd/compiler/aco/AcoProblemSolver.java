/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.geometry.partitioning.utilities.AVLTree;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.aco.epp.EppConfiguration;


/**
 * The main component of the framework: Is the one in charge of making a colony
 * an ants to traverse an environment in order to generate solutions.
 * <p>
 * <p>The solveProblem() method is the one that starts the optimization process.
 * Previously, you have to properly configure your solver by assigning it a
 * Colony, an Environment and Daemon Actions (if required).
 *
 * @author Carlos G. Gavidia
 * @param <V> the value type
 * @param <E> Class representing the Environment.
 */
public class AcoProblemSolver<V, E> {

    private static final double CONFIDENCE_LEVEL = 0.95;

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(AcoProblemSolver.class);

    /** The best solution. */
    private ForagingTrail<V, E> bestSolution;

    /** The best solution cost. */
    private double bestSolutionCost =  Double.MAX_VALUE;

    private List<Double> bestSolutionSamples;

    /** The best solution as string. */
    private String bestSolutionAsString = "";

    /** The environment. */
    private Environment<V, E> environment;

    /** The ant colony. */
    private AntColony<V, E> antColony;

    /** The configuration provider. */
    // TODO(cgavidia): Maybe we should handle a list of configuration providers.
    private ConfigurationProvider configurationProvider;

    /** The daemon actions. */
    private List<DaemonAction<V, E>> daemonActions = new ArrayList<DaemonAction<V, E>>();

    private int sampleSize;

    private int actualIterations;

    private int bestSolutionFoundAt;

    private long startTime;

    private long bestSolutionTime;



    /**
     * Make sure we need T, probably do because sample size is small (few iterations).
     *
     * @param stats the stats
     * @param level the level
     * @return the double
     */
    private static double calcMeanCI(SummaryStatistics stats, double level) {
        try {
            // Create T Distribution with N-1 degrees of freedom
            TDistribution tDist = new TDistribution(stats.getN() - 1);
            // Calculate critical value
            double critVal = tDist.inverseCumulativeProbability(1.0 - (1 - level) / 2);
            // Calculate confidence interval
            return critVal * stats.getStandardDeviation() / Math.sqrt(stats.getN());
        } catch (MathIllegalArgumentException e) {
            return Double.NaN;
        }
    }

    /**
     * Adds a Daemon Action for the current solver.
     *
     * @param daemonAction Daemon action.
     */
    public void addDaemonAction(DaemonAction<V, E> daemonAction) {

        daemonActions.add(daemonAction);
    }

    /**
     * Adds a list of Daemon Actions for the current solver. A daemon action is a global procedure applied
     * while algorithm execution.
     *
     * @param daemonActions Daemon actions.
     */
    public final void addDaemonActions(List<DaemonAction<V, E>> daemonActions) {
        for (DaemonAction<V, E> daemonAction : daemonActions) {
            this.addDaemonAction(daemonAction);
        }
    }

    /**
     * @return the actualIterations
     */
    public int getActualIterations() {
        return actualIterations;
    }

    /**
     * Gets the ant colony.
     *
     * @return the ant colony
     */
    public AntColony<V, E> getAntColony() {
        return antColony;
    }

    /**
     * Gets the best solution.
     *
     * @return the best solution
     */
    public ForagingTrail<V, E> getBestSolution() {
        return bestSolution;
    }

    /**
     * Gets the best solution as string.
     *
     * @return the best solution as string
     */
    public String getBestSolutionAsString() {
        return bestSolutionAsString;
    }

    /**
     * Gets the best solution cost.
     *
     * @return the best solution cost
     */
    public double getBestSolutionCost() {
        return bestSolutionCost;
    }


    /**
     * Gets the configuration provider.
     *
     * @return the configuration provider
     * @throws IllegalAccessException the illegal access exception
     */
    public ConfigurationProvider getConfigurationProvider() throws IllegalAccessException {
        if (this.configurationProvider == null) {
            throw new IllegalAccessException(
                    "No Configuration Provider was associated with this solver");
        }

        return configurationProvider;
    }

    /**
     * Gets the environment.
     *
     * @return the environment
     */
    public Environment<V, E> getEnvironment() {
        return environment;
    }

    /**
     * Gets the stop criteria.
     *
     * @param numberOfIterations the number of iterations
     * @param iteration the iteration
     * @return False if the algorithm should stop
     */
    public boolean getStopCriteria(int numberOfIterations, int iteration) {
        if (iteration > numberOfIterations) {
            return false;
        }
        // Use the variance of the best solutions so far
//        List<Double> data = antColony.getHive().stream()
//                .map(ant -> ant.getSolutionCost())
//                .collect(Collectors.toList());
//        // Build summary statistics of the dataset "data"
//        SummaryStatistics stats = new SummaryStatistics();
//        for (double val : data ) {
//            stats.addValue(val);
//        }
        long invalidSolutions = bestSolutionSamples.stream()
                .filter(v -> v == Double.MAX_VALUE)
                .count();
        if (invalidSolutions == getSampleSize()) {
            return false;
        }
        long validSolutions = bestSolutionSamples.stream()
                .filter(v -> v != Double.MAX_VALUE)
                .count();
        if (validSolutions > getSampleSize()) {		// Don't evaluate confidence until sample is enough
            SummaryStatistics stats = new SummaryStatistics();
            for (double val : bestSolutionSamples ) {
                if (val != Double.MAX_VALUE)
                    stats.addValue(val);
            }
            // Calculate 95% confidence interval
            double ci = calcMeanCI(stats, CONFIDENCE_LEVEL);
            if (!Double.isNaN(ci)) {
                logger.trace(String.format("Mean: %f", stats.getMean()));
//                double lower = stats.getMean() - ci;
//                double upper = stats.getMean() + ci;
                double interval =  ci/stats.getMean(); // Needs to be lower than getTerminatingConfidenceIntervalWidth() to terminate
                logger.info(String.format("Confidence Interval 95%%: %f", interval));
                // We keep looking until the interval is below the getTerminatingConfidenceIntervalWidth
                return interval >= ((EppConfiguration) antColony.getConfigurationProvider()).getTerminatingConfidenceIntervalWidth();
            }
        }
        return true;
    }


    /**
     * Prepares the solver for problem resolution. This includes building the colony
     *
     * @param environment Environment instance, with problem-related information.
     * @param colony      The Ant Colony with specialized ants.
     * @param config      Algorithm configuration.
     */
    public void initialize(Environment<V, E> environment, AntColony<V, E> colony, ConfigurationProvider config) {
        this.environment = environment;
        this.antColony = colony;
        this.configurationProvider = config;
        colony.buildColony();
        // Determine the sample size
        int ss = 0;
        double p = 0.5;		// It should be the probability of picking a choice, 0.5 for determining sample size?
        double c = ((EppConfiguration) antColony.getConfigurationProvider()).getTerminatingConfidenceIntervalWidth();
        // TODO Double check this! *2 for two sided?
        double z = pToZ(1- (1-CONFIDENCE_LEVEL)/2)*2;
        ss = (int) Math.ceil(Math.pow(z, 2)*p*(1-p)/Math.pow(c,2));
        int pop = ((EppConfiguration) antColony.getConfigurationProvider()).getNumberOfIterations();
        setSampleSize((ss*pop)/(ss+pop-1));
    }

    /**
     * Solves an optimization problem using a Colony of Ants.
     *
     * @throws IllegalAccessException the illegal access exception
     */
    public void solveProblem() throws IllegalAccessException {
        logger.trace("Starting computation at: {}", new Date());
        startTime = System.nanoTime();
        applyDaemonActions(DaemonActionType.INITIAL_CONFIGURATION);
        logger.info("STARTING ITERATIONS");
        int numberOfIterations = configurationProvider.getNumberOfIterations();
        if (numberOfIterations < 1) {
            throw new IllegalAccessException(
                    "No iterations are programed for this solver. Check your Configuration Provider.");
        }
        bestSolutionSamples = new ArrayList<>(numberOfIterations);
        logger.trace("Number of iterations: {}", numberOfIterations);
        logger.trace("Sample size: {}", getSampleSize());
        actualIterations = 0;
        do {
            antColony.resetAllAnts();
            antColony.startForaging();
            // TODO(cgavidia): This should reference the Update Pheromone routine.
            // Maybe with the Policy hierarchy.
            applyDaemonActions(DaemonActionType.AFTER_ITERATION_CONSTRUCTION);
            updateBestSolution(environment);
            logger.info("Current iteration: " + actualIterations + " Best solution cost: " + bestSolutionCost);
            actualIterations++;
        } while (getStopCriteria(numberOfIterations, actualIterations));		// Since the stop criteria depends on the solutions, we need a first batch
        long endTime = System.nanoTime();
        logger.trace("Finishing computation at: {}", new Date());
        double executionTime = (endTime - startTime) / 1000000000.0;
        logger.trace("Best solution: {}", bestSolutionAsString);
        logger.trace("Completed iterations: {}", actualIterations);
        logger.info("Best solution cost: {}", bestSolutionCost);
        logger.info("Best solution found at iteration: {}", bestSolutionFoundAt);
        logger.info("Best solution found at time: {}", bestSolutionTime - startTime);
        logger.info("Total number of iterations: {}", actualIterations);
        logger.info("Duration (in seconds): " + executionTime);

    }

    /**
     * Updates the information of the best solution produced with the solutions
     * produced by the Colony.
     *
     * @param environment Environment where the solutions where produced.
     */
    public void updateBestSolution(Environment<V, E> environment) {
        logger.trace("GETTING BEST SOLUTION FOUND");

        Ant<V, E> bestAnt = antColony.getBestPerformingAnt();
        Double bestIterationCost = bestAnt.getSolutionCost();
        logger.trace("Iteration best cost: " + bestIterationCost);
        bestSolutionSamples.add(bestIterationCost);
        if (bestSolutionCost > bestIterationCost) {
            bestSolution = bestAnt.getTrail();
            bestSolutionCost = bestIterationCost;
            bestSolutionAsString = bestAnt.getSolutionAsString();
            bestSolutionFoundAt = actualIterations;
            bestSolutionTime = System.nanoTime();
            logger.trace("Best solution so far > Cost: " + bestSolutionCost
                    + ", Solution: " + bestSolutionAsString);

        }
    }


    /**
     * Applies all daemon actions of a specific type.
     *
     * @param daemonActionType Daemon action type.
     */
    private void applyDaemonActions(DaemonActionType daemonActionType) {
        for (DaemonAction<V, E> daemonAction : daemonActions) {
            if (daemonActionType.equals(daemonAction.getAcoPhase())) {
                daemonAction.applyDaemonAction(antColony);
            }
        }
    }

    double pToZ(double p) {
        NormalDistribution normal = new NormalDistribution();
        return normal.cumulativeProbability(p);
//        double z = Math.sqrt(2) * Erf.erfcInv(2*p);
//        return(z);
    }

    /**
     * @return the sampleSize
     */
    public int getSampleSize() {
        return sampleSize;
    }

    /**
     * @param sampleSize the sampleSize to set
     */
    public void setSampleSize(int sampleSize) {
        this.sampleSize = sampleSize;
    }


}