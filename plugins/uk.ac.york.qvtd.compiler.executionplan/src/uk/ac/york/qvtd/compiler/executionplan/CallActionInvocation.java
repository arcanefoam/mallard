/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.executionplan;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.ocl.pivot.Variable;

import uk.ac.york.qvtd.compiler.Binding;
import uk.ac.york.qvtd.compiler.Operation;

public class CallActionInvocation extends AbstractInvocation {

    private CallActionOperation target;
    private boolean isPolled = false;	// Where the invocation is polled

    /**
     * A binding represents the relation between an input variable and the
     * local/context/loop variable
     */
    private Map<Variable, Binding> bindings = new LinkedHashMap<Variable, Binding>();

    public CallActionInvocation(Operation source, CallActionOperation target) {
        super(source);
        this.target = target;
    }

    public CallActionInvocation(Operation source, CallActionOperation target, boolean isPolled) {
        super(source);
        this.target = target;
        this.isPolled = isPolled;
    }

    @Override
    public boolean isPolled() {
        return isPolled;
    }

    @Override
    public void setPolled(boolean isPolled) {
        this.isPolled = isPolled;
    }


    @Override
    public Operation getTarget() {
        return target;
    }

    @Override
    public boolean isLooped() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setLooped(boolean isLooped) {
        // TODO Auto-generated method stub

    }

    public Map<Variable, Binding> getBindings() {
        // TODO Auto-generated method stub
        return bindings;
    }


    public void setBindings(Map<Variable, Binding> bindings) {
        assert !bindings.values().contains(null);
        this.bindings = bindings;
    }

    @Override
    public String toString() {
        return source.toString() + "->" + target.toString();
    }

    public String getBindingsAsString() {
        StringBuilder sb = new StringBuilder();
        for (Binding b : bindings.values()) {
            sb.append(b.toString()+"\n");
        }
        return sb.toString();
    }

    /**
     * Hash code can't depend on the bindings because this are added after the
     * edge has been created and thus will mess up the internal intrusive edge
     * mechanism.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((target == null) ? 0 : target.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof CallActionInvocation))
            return false;
        CallActionInvocation other = (CallActionInvocation) obj;
        if (bindings == null) {
            if (other.bindings != null)
                return false;
        } else if (!bindings.equals(other.bindings))
            return false;
        if (target == null) {
            if (other.target != null)
                return false;
        } else if (!target.equals(other.target))
            return false;
        return true;
    }

}