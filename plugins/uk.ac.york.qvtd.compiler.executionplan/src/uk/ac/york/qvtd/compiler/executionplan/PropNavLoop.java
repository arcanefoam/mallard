package uk.ac.york.qvtd.compiler.executionplan;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ocl.pivot.PivotFactory;
import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.Variable;

public class PropNavLoop extends LoopOperation {

    /** The navigation. */
    private final List<Property> navigation;
    private final Variable sourceVariable;

    public PropNavLoop(Variable sourceVariable, Type loopVariableType, List<Property> navigation, int index) {
        super();
        this.navigation = navigation;
        this.sourceVariable = sourceVariable;
        Property property = navigation.get(navigation.size()-1);
        this.loopVariable = PivotFactory.eINSTANCE.createVariable();
        this.loopVariable.setName(property.getName().toLowerCase() + "_" + "it" + "_" + index);
        this.loopVariable.setType(loopVariableType.flattenedType());
    }
    
    public PropNavLoop(Variable sourceVariable, Type loopVariableType, Property property, int index) {
        super();
        navigation = new ArrayList<Property>();
        navigation.add(property);
        this.sourceVariable = sourceVariable;
        this.loopVariable = PivotFactory.eINSTANCE.createVariable();
        this.loopVariable.setName(property.getName().toLowerCase() + "_" + "it" + "_" + index);
        this.loopVariable.setType(loopVariableType.flattenedType());
    }

    /**
     * Copy Constructor
     */
    public PropNavLoop(PropNavLoop other) {
        sourceVariable = other.getSourceVariable();
        navigation = new ArrayList<Property>(other.navigation);
        this.loopVariable = PivotFactory.eINSTANCE.createVariable();
        // TODO the name may need a counter to deal with multiple loops over the same type
        this.loopVariable.setName(other.loopVariable.getName());
        this.loopVariable.setType(other.loopVariable.getType());
    }

    @Override
    public boolean isRoot() {
        // TODO Auto-generated method stub
        return false;
    }

    public Variable getSourceVariable() {
        return sourceVariable;
    }

    public List<Property> getNavigation() {
        return navigation;
    }

    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder();
        String sep = ".";
        for (Property prop : navigation) {
            buff.append(sep);
            buff.append(prop.getName());
        }
        return "Loop " + sourceVariable.getName() +  buff;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((navigation == null) ? 0 : navigation.hashCode());
        result = prime * result + ((sourceVariable == null) ? 0 : sourceVariable.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof PropNavLoop))
            return false;
        PropNavLoop other = (PropNavLoop) obj;
        if (navigation == null) {
            if (other.navigation != null)
                return false;
        } else if (!navigation.equals(other.navigation))
            return false;
        if (sourceVariable == null) {
            if (other.sourceVariable != null)
                return false;
        } else if (!sourceVariable.equals(other.sourceVariable))
            return false;
        return true;
    }

}
