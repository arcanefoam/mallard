/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;

// TODO: Auto-generated Javadoc
/**
 * An interface to set/modify pheromone values and such.
 * @author hhoyos
 *
 */
public interface AcoEdge {

    /**
     * Adds the given value to the pheromone.
     *
     * @param value the value
     */
    public void addPheromone(double value);

    /**
     * Decay pheromone by the given factor
     *
     * @param factor the factor
     */
    public void decayPheromone(double factor);

    /**
     * Get the current value of the pheromone on the arc.
     *
     * @return the pheromone
     */
    public double getPheromone();

    /**
     * Sets the pheromone value.
     *
     * @param value the new pheromone
     */
    public void setPheromone(double value);

}
