/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;

import java.util.HashMap;
import java.util.Map;

import org.jgrapht.Graphs;

/**
 * The little workers that build solutions: They belong to a colony. This is an
 * Abstract Type so you must extend it in order to fill the characteristics of
 * your optimization problem.
 * <p>
 * <p>
 * Some convenient methods to define are:
 * <ul>
 * <li>isSolutionReady(), to define when the Ant must stop adding components to
 * its solution.
 * <li>getSolutionCost(), to define the cost of the current solution. It
 * will help to decide the best solution built so far.
 * <li>getHeuristicValue(), to explote problem domain information while
 * constructing solutions.
 * <li>getNeighbourhood(), this returns a list of possible components to add to
 * the solution.
 * <li>getPheromoneTrailValue(), returns the pheromone trail value associated to
 * a Solution Component.
 * <li>setPheromoneTrailValue(), to assign a pheromone value to a Solution
 * Component.
 * </ul>
 *
 * @param <C> Class for components of a solution.
 * @param <E> Class representing the Environment.
 * @author Carlos G. Gavidia
 * @author Horacio Hoyos
 */
public abstract class AbstractAnt<V, E> implements Ant<V, E> {

    /**
     * One policy per supported policies
     */
    protected Map<AntPolicyType, AbstractAntPolicy<V, E>> policies = new HashMap<AntPolicyType, AbstractAntPolicy<V,E>>();

    private ForagingTrail<V, E> trail;

    private final AntColony<V, E> colony;


    public  AbstractAnt(AntColony<V, E> colony) {
        super();
        this.colony = colony;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#addPolicy(org.eclipse.qvtd.compiler.aco.AbstractAntPolicy)
     */
    @Override
    public AbstractAntPolicy<V, E> addPolicy(AbstractAntPolicy<V, E> antPolicy) {
        return policies.put(antPolicy.getPolicyType(), antPolicy);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#doAfterSolutionIsReady(org.eclipse.qvtd.compiler.aco.Environment, org.eclipse.qvtd.compiler.aco.ConfigurationProvider)
     */
    @Override
    public void doAfterSolutionIsReady() {
        AbstractAntPolicy<V, E> solutionReadyPolicity = getAntPolicy(AntPolicyType.AFTER_SOLUTION_IS_READY);

        if (solutionReadyPolicity != null) {
            solutionReadyPolicity.applyPolicy(this);
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#getAntPolicy(org.eclipse.qvtd.compiler.aco.AntPolicyType)
     */
    @Override
    public AbstractAntPolicy<V, E> getAntPolicy(AntPolicyType policyType) {
        return policies.get(policyType);
    }

    @Override
    public AntColony<V, E> getColony() {
        return colony;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#getCurrentVertex()
     */
    @Override
    public V getCurrentVertex() {
        return trail.getEndVertex();
    }


    @Override
    public Double getHeuristicValue(V candidate) {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#getSolutionAsString()
     */
    @Override
    public String getSolutionAsString() {
        // TODO Transform the solution into an execution plan so we can see it
//        String solutionString = "";
//        for (int i = 0; i < solution.length; i++) {
//            if (solution[i] != null) {
//                solutionString = solutionString + " " + solution[i].toString();
//            }
//        }
        return Graphs.getPathVertexList(trail).toString();
    }

    @Override
    public double getSolutionCost() {
        return trail.getWeight();
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#isNodeVisited(V)
     */

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#getSolution()
     */
    @Override
    public ForagingTrail<V, E> getTrail() {
        return trail;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#isNodeValid(V)
     */
    @Override
    public boolean isNodeValid(V vertex) {
        return true;
    }

    @Override
    public boolean isNodeVisited(V vertex) {
        return Graphs.getPathVertexList(trail).contains(vertex);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#selectNextNode(org.eclipse.qvtd.compiler.aco.Environment, org.eclipse.qvtd.compiler.aco.ConfigurationProvider)
     */
    @Override
    public void selectNextNode() {

        AbstractAntPolicy<V, E> selectNodePolicity = getAntPolicy(AntPolicyType.NODE_SELECTION);
        if (selectNodePolicity == null) {
            throw new UnsupportedOperationException("The and does not have a policy for node selection.");
        }
        boolean policyResult = selectNodePolicity.applyPolicy(this);
        if (!policyResult) {
            throw new UnsupportedOperationException("The node selection policy " + selectNodePolicity.getClass().getName() +
                    " wasn't able to select a node.");
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#setTrail(org.eclipse.qvtd.compiler.aco.ForagingTrail)
     */
    @Override
    public void setTrail(ForagingTrail<V, E> trail) {
        this.trail = trail;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#visitNode(E)
     */
    @Override
    public void visitNode(E edgeUsed) {
        trail.addEdge(edgeUsed);
    }

}