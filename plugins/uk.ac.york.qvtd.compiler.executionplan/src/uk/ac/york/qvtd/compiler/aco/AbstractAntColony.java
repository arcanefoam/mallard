/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A group of ants. As an abstract type, you need to define how to build
 * individual ants through the createAnt() method.
 *
 * @author Carlos G. Gavidia
 * @param <V> the value type
 * @param <E> Class representing the Environment.
 */
public abstract class AbstractAntColony<V, E> implements AntColony<V, E> {

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(AbstractAntColony.class);

    /** The number of ants. */
    private final int numberOfAnts;

    /** The hive. */
    private final List<Ant<V, E>> hive;

    /** The configuration provider. */
    private final ConfigurationProvider configurationProvider;

    /**
     * Creates a colony of ants.
     *
     * @param environment the environment
     * @param configurationProvider the configuration provider
     */
    public AbstractAntColony(ConfigurationProvider configurationProvider) {
        this.configurationProvider = configurationProvider;
        this.numberOfAnts = configurationProvider.getNumberOfAnts();
        hive = new ArrayList<Ant<V, E>>(numberOfAnts);
        logger.trace("Number of Ants in Colony: " + numberOfAnts);
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.AntColony#addAntPolicies(org.eclipse.qvtd.compiler.aco.AbstractAntPolicy)
     */
    @Override
    public final void addAntPolicies(List<AbstractAntPolicy<V, E>> antPolicies) {
        for (Ant<V, E> ant : hive) {
            for (AbstractAntPolicy<V, E> antPolicy : antPolicies) {
                ant.addPolicy(antPolicy);
            }
        }
    }

    @Override
    public void addAntPolicy(AbstractAntPolicy<V, E> antPolicy) {
        for (Ant<V, E> ant : hive) {
            ant.addPolicy(antPolicy);
        }

    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.AntColony#buildColony(org.eclipse.qvtd.compiler.aco.Environment)
     */
    @Override
    public void buildColony() {
        for (int j = 0; j < numberOfAnts; j++) {
            hive.add(createAnt());
        }
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.AntColony#getBestPerformingAnt(org.eclipse.qvtd.compiler.aco.Environment)
     */
    @Override
    public Ant<V, E> getBestPerformingAnt() {
        Ant<V, E> bestAnt = hive.stream().sorted(new Comparator<Ant<V, E>>() {

            @Override
            public int compare(Ant<V, E> ant1, Ant<V, E> ant2) {
                return Double.compare(ant1.getSolutionCost(), ant2.getSolutionCost());
            }
        }).findFirst().get();
        return bestAnt;
    }

    @Override
    public Ant<V, E> getWorstPerformingAnt() {
        Ant<V, E> worsttAnt = hive.stream(). sorted(new Comparator<Ant<V, E>>() {

            @Override
            public int compare(Ant<V, E> ant1, Ant<V, E> ant2) {
                return Double.compare(ant2.getSolutionCost(), ant1.getSolutionCost());
            }
        }).findFirst().get();
        return worsttAnt;
    }

    /**
     * @return the configurationProvider
     */
    @Override
    public ConfigurationProvider getConfigurationProvider() {
        return configurationProvider;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.AntColony#getHive()
     */
    @Override
    public List<Ant<V, E>> getHive() {
        return hive;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.AntColony#getNumberOfAnts()
     */
    @Override
    public int getNumberOfAnts() {
        return numberOfAnts;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.AntColony#clearAntSolutions()
     */
    @Override
    public void resetAllAnts() {
        logger.trace("CLEARING ANT SOLUTIONS");
        for (Ant<V, E> ant : hive) {
            ant.reset();
        }

    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.AntColony#startForaging(org.eclipse.qvtd.compiler.aco.Environment, org.eclipse.qvtd.compiler.aco.ConfigurationProvider)
     */
    @Override
    public void startForaging() {
        logger.trace("BUILDING ANT SOLUTIONS");
        int antCounter = 0;
        if (hive.size() == 0) {
            throw new UnsupportedOperationException(
                    "Your colony is empty: You have no ants to solve the problem. "
                            + "Have you called the buildColony() method?");
        }

        for (Ant<V, E> ant : hive) {
            logger.trace("Current ant: " + antCounter);
            while (!ant.isSolutionReady()) {
                ant.selectNextNode();
            }
            ant.doAfterSolutionIsReady();
            logger.trace("Solution is ready > Cost: " + ant.getSolutionCost()
                            + ", Solution: " + ant.getSolutionAsString());
            antCounter++;
        }
    }


}