/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.executionplan.utilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ocl.pivot.OCLExpression;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.ocl.pivot.VariableDeclaration;
import org.eclipse.ocl.pivot.VariableExp;
import org.eclipse.qvtd.compiler.internal.utilities.ClassRelationships;
import org.eclipse.qvtd.pivot.qvtbase.Domain;
import org.eclipse.qvtd.pivot.qvtbase.Predicate;
import org.eclipse.qvtd.pivot.qvtcorebase.Area;
import org.eclipse.qvtd.pivot.qvtcorebase.CoreDomain;
import org.eclipse.qvtd.pivot.qvtimperative.Mapping;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.traverse.BreadthFirstIterator;

import uk.ac.york.qvtd.compiler.Binding;
import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.aco.epp.InvocationEdge;
import uk.ac.york.qvtd.compiler.bindings.LoopBinding;
import uk.ac.york.qvtd.compiler.bindings.NavigationBinding;
import uk.ac.york.qvtd.compiler.evaluation.CompletnessEvaluator;
import uk.ac.york.qvtd.compiler.evaluation.CorrectnessEvaluator;
import uk.ac.york.qvtd.compiler.evaluation.LoopReduction;
import uk.ac.york.qvtd.compiler.evaluation.SoundnessEvaluator;
import uk.ac.york.qvtd.compiler.evaluation.ViabilityEvaluator;
import uk.ac.york.qvtd.compiler.executionplan.CallActionInvocation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.compiler.executionplan.ExecutionPlan;
import uk.ac.york.qvtd.compiler.executionplan.LoopInvocation;
import uk.ac.york.qvtd.compiler.executionplan.LoopOperation;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.util.Pair;

/**
 * Utility methods to analyse, evaluate and modify execution plans.
 * @author Horacio Hoyos
 *
 */
public class ExecutionPlanUtils {

    /**
     * Eliminate all lists that are proper sublist of another list.
     * TODO We need to think about handling loops, as loop paths are not sets!!
     * <p>
     * Modified from <a href="http://stackoverflow.com/q/14106121">StackOverflow #14106121</a>
     * <p>
     * Per construction all the paths start in the same element and end in the same element.
     * Each list will be labeled by
     *
     * @param <T> the generic type
     * @param paths the paths
     * @return the list
     */
    public static <T> List<Set<T>> findMaximalSubSets(List<Set<T>> paths) {
        if (paths.size() <= 1) {
            return new ArrayList<Set<T>>(paths);		// Copy
        }
        Map<T, Integer> sets_containing_element = paths.stream()
                .collect(new ContainingCollector<>());
        return paths.stream()
                .filter(path -> isPowerOfTwo(path.stream()
                        .map(ma->sets_containing_element.get(ma))
                        .reduce((acc, item) -> acc & item)
                        .get()))
                .collect(Collectors.toList());
    }

    /**
     * Generate dot file from the set of plans.
     *
     * @param name the name
     * @param plans the plans
     * @param writer the writer
     */
    public static String generateDOT(String name, Set<? extends ExecutionPlanGraph> plans) {
        Object[] sample;
        if (plans.size() > 20) {
            // Pick 20 random ones
            RandomDataGenerator rdg = new RandomDataGenerator();
            sample = rdg.nextSample(plans, 20);
        } else {
            sample = plans.toArray(new ExecutionPlanGraph[plans.size()]);
        }
        List<ExecutionPlan> sorted = new ArrayList<ExecutionPlan>();
        for (int i = 0; i < sample.length; i++) {
            sorted.add(((ExecutionPlan) sample[i]));
        }
        Collections.sort(sorted, new Comparator<ExecutionPlan>() {

            @Override
            public int compare(ExecutionPlan o1, ExecutionPlan o2) {
                return Double.compare(o1.getCost(), o2.getCost());
            }
        });
        StringBuilder sb = new StringBuilder();
        sb.setLength(0);
        sb.append("digraph " + name + " {\n");
        int i = 1;
        for (ExecutionPlan ep : sorted) {
            sb.append(ep.toDOT(i++, true));
        }
        sb.append("}\n");
        return sb.toString();
    }

    /**
     * Gets the invocation chain source.
     *
     * @param currInv the curr inv
     * @param plan the plan
     * @return the invocation chain source
     */
    public static Operation getInvocationChainSource(Invocation currInv, DirectedGraph<Operation, Invocation> plan) {
        Operation source = currInv.getSource();
        if (!(source instanceof CallActionOperation)) {
            for (Invocation prevInv : plan.incomingEdgesOf(source)) {
                source = getInvocationChainSource(prevInv, plan);
                break;
            }
        }
        return source;
    }

    /**
     * Gets the remainder types.
     *
     * @param plan the plan
     * @return the remainder types
     */
    public static Set<ClassDatum> getRemainderTypes(ExecutionPlanGraph plan) {
        ExecutionPlanIterator it = new ExecutionPlanIterator(plan);
//        CompletnessEvaluator correctEval = new CompletnessEvaluator(plan);
        CorrectnessEvaluator correctEval = new CorrectnessEvaluator(plan);
        it.addTraversalListener(correctEval);
        while(it.hasNext()) {
            it.next();
        }
        //return correctEval.getRemainderTypes().stream().map(p -> p.first).collect(Collectors.toSet());
        return correctEval.getRemainderTypes();
    }

    /**
     * A plan is candidate if:
     * <ul>
     * 	<li> The plan is sound
     * </ul>.
     *
     * @param plan the plan
     * @param classrel
     * @return true, if is valid
     */
    // TODO valid also needs to check that nested call respect guards. This means
    // that if a variable in the nesting mapping is guarded, the variable bound
    // to this one in the nested mapping should share the same guards. Probably
    // that the guards of the nesting are a subset of the guards of the nested.
    // However, this can only be done by syntax? Can we do this semantically?
    // i.e. there are many ways to express the same guard, and we would
    // need to show that they are semantically equivalent.
    public static boolean isCandidate(ExecutionPlanGraph plan) {
        ExecutionPlanIterator it = new ExecutionPlanIterator(plan);
        SoundnessEvaluator eval = new SoundnessEvaluator(plan);
        it.addTraversalListener(eval);
        while(it.hasNext()) {
            it.next();
            if (!eval.isValid())
                return false;
        }
        return eval.isValid();
    }


    /**
     * Checks if is correct. A plan is correct if it is correct and sound
     *
     * @param plan the plan
     * @param classrel
     * @return true, if is correct
     */
    public static boolean isCorrect(ExecutionPlanGraph plan) {
        ExecutionPlanIterator it = new ExecutionPlanIterator(plan);
        // Verify that all the mappings have been invoked.
        ConnectivityInspector<Operation, Invocation> ci = new ConnectivityInspector<Operation, Invocation>(plan);
        if (!ci.isGraphConnected()) {
            return false;
        }
        CorrectnessEvaluator correctEval = new CorrectnessEvaluator(plan);
        SoundnessEvaluator soundEval = new SoundnessEvaluator(plan);
        it.addTraversalListener(correctEval);
        it.addTraversalListener(soundEval);
        while(it.hasNext()) {
            Operation n = it.next();
            //it.next();
            if (!soundEval.isValid()) {
                return false;
            }
        }
        return correctEval.isCorrect();
    }

    /**
     * For the ACO, we want correctness, but during construction we don't care about remainder types.
     * @param plan the plan
     * @param classrel
     * @return true, if is feasible
     */
    public static boolean isFeasible(ExecutionPlanGraph plan) {
        return isFeasible(plan, null);
    }

    public static boolean isFeasible(ExecutionPlanGraph plan, InvocationEdge invocationEdge) {
        ExecutionPlanIterator it = new ExecutionPlanIterator(plan, null);
        //it.setFinalEdge(invocationEdge);
        CorrectnessEvaluator correctnessEvaluator = new CorrectnessEvaluator(plan);
        SoundnessEvaluator soundEval = new SoundnessEvaluator(plan);
        it.addTraversalListener(correctnessEvaluator);
        it.addTraversalListener(soundEval);
        boolean result = true;
        while(it.hasNext()) {
            it.next();
            if (!soundEval.isValid()) {
                result = false;
                break;
            }
        }
        if (result) {
            result = correctnessEvaluator.isFeasible();
        }
        return result;
    }

    public static boolean isComplete(ExecutionPlanGraph plan) {

        ExecutionPlanIterator it = new ExecutionPlanIterator(plan, null);
        CompletnessEvaluator compEval = new CompletnessEvaluator(plan);
        it.addTraversalListener(compEval);
        while(it.hasNext()) {
            it.next();
        }
        return compEval.isValid();
    }


    /**
     * Creates a single plan by merging the adjunct with the primary sections in a single
     * plan.
     *
     * @param plan the plan
     * @return true, if is correct
     */
//    public static ExecutionPlan materialize(ExecutionPlanGraph adjunct) {
//        ExecutionPlanGraph base = getBaseExecutionPlan(adjunct);
//        ExecutionPlan complete = new ExecutionPlan();
//        complete.setCost(base.getCost());	// At this point there should be no bindings so cost is meaningless
//        complete.setDependencyGraph(base.getDependencyGraph());
//        Map<Operation, Operation> otherToNewOp = new HashMap<Operation, Operation>();
//        Map<Variable, Variable> otherToNewLocalVar = new HashMap<Variable, Variable>();
//        // Copy CallActionInvocations first to have references to local variables
//        List<Operation> callOps = adjunct.vertexSet().stream()
//                .filter(o -> o instanceof CallActionOperation)
//                .collect(Collectors.toList());
//        List<Operation> loopOps = adjunct.vertexSet().stream()
//                .filter(o -> !(o instanceof CallActionOperation))
//                .collect(Collectors.toList());
//        for (Operation op : callOps) {
//            CallActionOperation newOp = new CallActionOperation((CallActionOperation)op);
//            // Keep variable trace for later use
//            for (DerivedVariable otherlv : ((CallActionOperation) op).getLocalVariables()) {
//                for (DerivedVariable lv : newOp.getLocalVariables()) {
//                    // The name holds the source variable and property "id" (name)
//                    if (otherlv.getVariable().getName().equals(lv.getVariable().getName())) {
//                        otherToNewLocalVar.put(otherlv.getVariable(), lv.getVariable());
//                    }
//                }
//            }
//            otherToNewOp.put(op, newOp);
//            complete.addVertex(newOp);
//        }
//        for (Operation op : loopOps) {
//            LoopOperation newOp = null;
//            if (op instanceof AllInstLoop) {
//                AllInstLoop ail = (AllInstLoop) op;
//                newOp = new AllInstLoop(ail);
//                otherToNewLocalVar.put(ail.getLoopVariable(), newOp.getLoopVariable());
//            } else if (op instanceof PropNavLoop) {
//                PropNavLoop pnl = (PropNavLoop) op;
//                if (otherToNewLocalVar.containsKey(pnl.getSourceVariable())) {
//                    Variable sourceVar = otherToNewLocalVar.get(pnl.getSourceVariable());
//                    String varName = pnl.getLoopVariable().getName();
//                    int indexStart = varName.lastIndexOf("_");
//                    int index = Integer.valueOf(varName.substring(indexStart+1));
//                    newOp = new PropNavLoop(sourceVar,
//                            pnl.getLoopVariable().getType(),
//                            pnl.getNavigation(),
//                            index);
//                } else {
//                    newOp = new PropNavLoop(pnl);
//                }
//                otherToNewLocalVar.put(pnl.getLoopVariable(), newOp.getLoopVariable());
//            }
//            assert newOp != null;
//            otherToNewOp.put(op, newOp);
//            complete.addVertex(newOp);
//        }
//        complete.setRoot((CallActionOperation) otherToNewOp.get(base.getRoot()));
//        for (MappingAction mv : base.getMappingToOperation().keySet()) {
//            Operation oClone = otherToNewOp.get(base.getMappingToOperation().get(mv));
//            complete.getMappingToOperation().put(mv, (CallActionOperation) oClone);
//        }
//        Map<Binding, Binding> otherToNewBind = new HashMap<Binding, Binding>();
//        BreadthFirstIterator<Operation, Invocation> itr = new BreadthFirstIterator<Operation, Invocation>(adjunct, base.getRoot());
//        while(itr.hasNext()) {
//            Operation otherOp = itr.next();
//            for (Invocation otherIn : adjunct.outgoingEdgesOf(otherOp)) {
//                Invocation newIn = complete.addEdge(otherToNewOp.get(otherIn.getSource()), otherToNewOp.get(otherIn.getTarget()));
//                if (otherIn instanceof CallActionInvocation) {
//                    Iterator<Entry<Variable, Binding>> it = ((CallActionInvocation) otherIn).getBindings().entrySet().iterator();
//                    while (it.hasNext()) {
//                        Binding newBind = null;
//                        Map.Entry<Variable, Binding> pair = it.next();
//                        Binding otherBind = pair.getValue();
//                        if (otherBind instanceof DirectBinding) {
//                            newBind = new DirectBinding((DirectBinding) otherBind);
//                        } else if (otherBind instanceof LoopBinding) {
//                            newBind = BindingsUtilities.createLoopBidning((LoopBinding) otherBind, otherToNewOp);
//                        } else if (otherBind instanceof LocalBinding) {
//                            // Find the invoking operation
//                            Operation otherSource = getInvocationChainSource(otherIn, adjunct);
//                            Operation source =  otherToNewOp.get(otherSource);
//                            newBind = BindingsUtilities.createLocalBinding((LocalBinding) otherBind, (CallActionOperation) source);
//                        } else if (otherBind instanceof NavigationBinding) {
//                            NavigationBinding nb = (NavigationBinding) otherBind;
//                            Variable sourceVar = otherToNewLocalVar.get(nb.getSourceVariable());
//                            newBind = BindingsUtilities.createNavigationBinding(nb, sourceVar);
//                        }
//                        assert newBind != null;
//                        otherToNewBind.put(otherBind, newBind);
//                        ((CallActionInvocation) newIn).getBindings().put(pair.getKey(), newBind);
//                    }
//                    newIn.setPolled(otherIn.isPolled());
//                    newIn.setLooped(otherIn.isLooped());
//                }
//            }
//        }
//        // Now that bidings exist, fix the associated loop bindings
//        for (Operation op : adjunct.vertexSet()) {
//            if (op instanceof LoopOperation)
//                for (NavigationBinding b : ((LoopOperation) op).getAssociatedLoopBindings()) {
//                    ((LoopOperation) otherToNewOp.get(op)).getAssociatedLoopBindings()
//                        .add((NavigationBinding) otherToNewBind.get(b));
//                }
//        }
//        return complete;
//    }

    /**
     * Returns true is the plan is viable. Viability is used to filter plans during construction, i.e. a plan might
     * be incomplete and thus we can not completely discard it as it can be later completed.
     * @see {@link ViabilityEvaluator}
     * @param plan
     *
     * @return true, if is viable
     */
    public static boolean isViable(ExecutionPlanGraph plan) {

        ExecutionPlanIterator it = new ExecutionPlanIterator(plan);
        ViabilityEvaluator inspector = new ViabilityEvaluator(plan);
        it.addTraversalListener(inspector);
        while (it.hasNext()) {
            it.next();
            if (!inspector.isValid())
                return false;
        }
        return true;
    }

    /**
     * Optimise the plan by reusing/reducing Loops.
     * <p>
     * Two loops can be reduced to one when:
     * <ul>
     * 	<li> All Instances Loops: Both loops are immediate siblings (no loops in
     * 		between) and the loop is over the same type:domain.
     * 	<li> Property Navigation Loops: Both loops are immediate siblings (no
     * 		loops in between) and the loops have the same source variable and
     * 		are over the same property.
     * </ul>
     * Since all instance loops reduction can make PropNavLoops become reduction
     * candidates (i.e. the only way the source variable will be the same), and
     * since a loop reduction creates new siblings, the optimisation should be
     * run multiple times until no more reduction can be done.
     *
     * @param plan the plan
     */
    public static void optimizeLoopSharing(ExecutionPlanGraph plan) {
        BreadthFirstIterator<Operation, Invocation> it;
        LoopReduction lr;
        Collection<Binding> bs;
        for(;;) {
            it = new BreadthFirstIterator<Operation, Invocation>(plan, plan.getRoot());
            lr = new LoopReduction();
            it.addTraversalListener(lr);
                while (it.hasNext() && !lr.needsMerge())
                    it.next();
            if (lr.needsMerge()) {
                // Remove the toReduceInvocation, the toReduceLoop and the outgoing edge
                // of toReduceLoop. Add a new edge from the target of the source edge
                // to the target of the toReduceLoop outgoing edge
                // We need to find all bindings that use the loop variable and update them!
                assert lr.getInvocationToReduce() instanceof LoopInvocation;
                LoopInvocation toReduceInvocation = (LoopInvocation) lr.getInvocationToReduce();
                LoopOperation toReduceLoop = (LoopOperation) toReduceInvocation.getTarget();
                LoopOperation toKeepLoop = (LoopOperation) lr.getSourceInvocation().getTarget();
                bs = getPathBindings(plan, toReduceInvocation);
                for (Binding b : bs) {
                    assert bs != null;
                    if (b.getSourceVariable().equals(toReduceLoop.getLoopVariable())) {
                        if (b instanceof LoopBinding)
                            ((LoopBinding) b).setSourceLoop(toKeepLoop);
                        if (b instanceof NavigationBinding)
                            ((NavigationBinding) b).setSourveVariable(toKeepLoop.getLoopVariable());
                    }
                }
                assert plan.outDegreeOf(toReduceLoop) == 1;
                Invocation notNeededInvocation = plan.outgoingEdgesOf(toReduceLoop).iterator().next();
                Operation targetOperation = notNeededInvocation.getTarget();
                Invocation newInvocation = plan.addEdge(toKeepLoop, targetOperation);
                if (newInvocation == null) {
                    System.out.println("WTF?");
                    newInvocation = plan.addEdge(toKeepLoop, targetOperation);
                }
                plan.removeEdge(toReduceInvocation);
                if (notNeededInvocation instanceof CallActionInvocation) {
                    CallActionInvocation nca = (CallActionInvocation) newInvocation;
                    nca.setBindings(((CallActionInvocation) notNeededInvocation).getBindings());
                    for (Binding b : nca.getBindings().values()) {
                        if (b == null)
                            System.err.println("Nullified biding");
                    }
                }
                plan.removeEdge(notNeededInvocation);
                notNeededInvocation = null;
                plan.removeVertex(toReduceLoop);
                toKeepLoop.getAssociatedLoopBindings().addAll(toReduceLoop.getAssociatedLoopBindings());
                toReduceLoop = null;
            } else {
                break;
            }
        }
    }

    /**
     * Return true if there is a variable in the mapping of the type and exists a predicate that guards the variable.
     * <p>
     * Find any predicate in which an input variable of the given type is involved. If any, then the type is guarded
     *
     * @param ma the mapping action
     * @param t the Type
     * @return true, if successful
     */
    public static boolean typeIsGuarded(MappingAction ma, Type t) {
        Mapping m= ma.getMapping();
        for (Predicate pred : getAllPredicates(m)) {
            OCLExpression condExp = pred.getConditionExpression();
            Iterable<EObject> iterable = () -> condExp.eAllContents();
            Stream<EObject> targetStream = StreamSupport.stream(iterable.spliterator(), false);
            Set<VariableDeclaration> predVars = targetStream.filter(VariableExp.class::isInstance)
                    .map(VariableExp.class::cast)
                    .map(vexp -> vexp.getReferredVariable())
                    .filter(v -> v.getType().equals(t) && getAllVariables(m).contains(v))
                    .collect(Collectors.toSet());
            if (!predVars.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the all property assignments in the Area.
     *
     * @param a the a
     * @return the all property assignments
     */
    private static List<Predicate> getAllPredicates(Area a) {
        List<Predicate> result = new ArrayList<Predicate>();
        result.addAll(a.getGuardPattern().getPredicate());
        result.addAll(a.getBottomPattern().getPredicate());
        return result;
    }

    /**
     * Get all property assignments in the Mapping.
     *
     * @param m the m
     * @return the all property assignments
     */
    private static List<Predicate> getAllPredicates(Mapping m) {

        List<Predicate> result = new ArrayList<Predicate>();
        for (Domain d : m.getDomain()) {
            result.addAll(getAllPredicates((CoreDomain) d));
        }
        result.addAll(getAllPredicates((Area) m));
        return result;
    }

    /**
     * Gets the all variables.
     *
     * @param m the m
     * @return the all variables
     */
    private static List<Variable> getAllVariables(Mapping m) {

        List<Variable> result = new ArrayList<Variable>();
        for (Domain d : m.getDomain()) {
            result.addAll(((Area) d).getGuardPattern().getVariable());
            result.addAll(((Area) d).getBottomPattern().getVariable());
        }
        result.addAll(((Area) m).getGuardPattern().getVariable());
        result.addAll(((Area) m).getBottomPattern().getVariable());
        return result;
    }

    /**
     * Gets the path bindings.
     *
     * @param plan the plan
     * @param invocation the invocation
     * @return the path bindings
     */
    private static Collection<Binding>  getPathBindings(ExecutionPlanGraph plan, LoopInvocation invocation) {
        Invocation cai = invocation;
        while (cai instanceof LoopInvocation) {
            Iterator<Invocation> it = plan.outgoingEdgesOf(cai.getTarget()).iterator();
            cai = it.next();	// There is only one invocation after each loop, at least before optimization
        }
        return ((CallActionInvocation) cai).getBindings().values();
    }



    /**
     * Returns True iff n is a power of two.  Assumes n > 0.
     *
     * @param n the n
     * @return true, if is power of two
     */
    private static boolean isPowerOfTwo(int n) {
      return (n & (n-1)) == 0;
    }

    /**
     * The Class ContainingCollector.
     *
     * @param <T> the generic type
     */
    private static class ContainingCollector<T> implements Collector<Collection<T>,Map<T, Integer>,Map<T, Integer>> {

        /** The index. */
        AtomicInteger index = new AtomicInteger();

        @Override
        public BiConsumer<Map<T, Integer>, Collection<T>> accumulator() {
            return (acc, col) -> {col.forEach(
                    elem->acc.merge(elem, 1 << index.get(), (old, v) -> old | (1 << index.get())));
                    index.incrementAndGet();};
        }

        @Override
        public Set<java.util.stream.Collector.Characteristics> characteristics() {

            return Collections.unmodifiableSet(EnumSet.of(Collector.Characteristics.UNORDERED,
                    Collector.Characteristics.IDENTITY_FINISH));
        }

        @Override
        public BinaryOperator<Map<T, Integer>> combiner() {
            return (acc1, acc2) -> {
                throw new UnsupportedOperationException();
              };
        }

        @Override
        public Function<Map<T, Integer>, Map<T, Integer>> finisher() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Supplier<Map<T, Integer>> supplier() {
            return HashMap::new;
        }

    }

    /**
     * Insert operation.
     *
     * @param plan the plan
     * @param newOperation the invoker
     * @param existingInvocation the initial invocation
     * @param insertIndex the insert index
     */
    public static Invocation insertOperation(ExecutionPlanGraph plan, Operation newOperation, Invocation existingInvocation,
                int insertIndex) {

        Operation sourceVertex = existingInvocation.getSource();
        Operation targetVertex = existingInvocation.getTarget();
        plan.removeEdge(existingInvocation);
        plan.addVertex(newOperation);
        plan.addEdge(insertIndex, sourceVertex, newOperation);
        return plan.addEdge(newOperation, targetVertex);
    }



}
