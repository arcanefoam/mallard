/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;

import java.util.List;

/**
 * The Interface AntColony.
 *
 * @param <V> the value type
 * @param <E> the element type
 */
public interface AntColony<V, E> {

    /**
     * Adds a list of policies to every Ant in the Colony. This are ant-specific behaviours, like component selection
     * while building solutions.
     *
     * @param antPolicies List of policies.
     */
    void addAntPolicies(List<AbstractAntPolicy<V, E>> antPolicies);

    /**
     * Adds the ant policy to the list of policies of every Ant in the Colony.
     *
     * @param antPolicy the ant policy
     */
    void addAntPolicy(AbstractAntPolicy<V, E> antPolicy);

    /**
     * Initialisation code for the colony. The main responsibility is Ant
     * instantiation.
     */
    void buildColony();

    /**
     * Produces a new Ant to integrate the colony.
     *
     * @return An Ant instance.
     */
    Ant<V, E> createAnt();

    /**
     * Returns the ant with the best performance so far, that is the one with the lowest cost.
     *
     * @return Best performing Ant.
     */
    Ant<V, E> getBestPerformingAnt();

    /**
     * Gets the configuration provider.
     *
     * @return the configuration provider
     */
    ConfigurationProvider getConfigurationProvider();

    /**
     * Gets the environment.
     *
     * @return the environment
     */
    Environment<V, E> getEnvironment();

    /**
     * Returns a List of all the ants in the colony.
     *
     * @return List of Ants.
     */
    List<Ant<V, E>> getHive();

    /**
     * Gets the number of ants.
     *
     * @return the number of ants
     */
    int getNumberOfAnts();

    /**
     * Gets the worst performing ant.
     *
     * @return the worst performing ant
     */
    Ant<V, E> getWorstPerformingAnt();

    /**
     * Clears solution build for every Ant in the colony.
     */
    void resetAllAnts();

    /**
     * Puts every ant in the colony to build a solution.
     */
    void startForaging();

}