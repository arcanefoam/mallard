/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.executionplan;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.ocl.pivot.Variable;

import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.bindings.DerivedVariable;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;

/**
 * A Call Action Operation is an Operation in the Execution Plan that represents
 * the invocation of a mapping in the QVTi code.
 */
public class CallActionOperation implements Operation, Comparable<CallActionOperation> {

    /** The mapping invoked. */
    private final MappingAction action;

    /** Indicate if this is the root operation. */
    private final boolean isRoot;

    /** The local variables. */
    private final List<DerivedVariable> localVariables;

    /**
     * Instantiates a new call action operation.
     *
     * @param action the action
     */
    public CallActionOperation(MappingAction action) {

        this(action, false);
    }

    /**
     * @param action
     * @param isRoot
     */
    public CallActionOperation(MappingAction action, boolean isRoot) {
        super();
        this.action = action;
        this.isRoot = isRoot;
        localVariables = new ArrayList<DerivedVariable>();
    }


    /**
     * Copy constructor
     */
    public CallActionOperation(CallActionOperation other) {
        super();
        action = other.action;
        isRoot = other.isRoot;
        // Although we could clone the list, each derived variable needs to be
        // checked individually in case the source variable needs to be cloned too
        localVariables = new ArrayList<DerivedVariable>();
        Deque<DerivedVariable> q = new ArrayDeque<DerivedVariable>(other.localVariables);
        // Easily get the cloned DV by using the original DerivedVariable variable
        Map<Variable, DerivedVariable> dvClones = new HashMap<Variable, DerivedVariable>();
        while (!q.isEmpty()) {
            DerivedVariable dv = q.pollFirst();
            DerivedVariable newdv;
            // The source of a derived variable can be either an existing mapping variable
            // or another derived variable.
            if (dv.getSourveVariable().eContainer() != null) {
                // Existing mapping variable, it is safe to clone the dv
                newdv = new DerivedVariable(dv);
                dvClones.put(dv.getVariable(), newdv);
            } else {
                DerivedVariable dvClone = dvClones.get(dv.getSourveVariable());
                if (dvClone == null) {
                    // The required DV hasn't been cloned yet, do it later
                    q.addLast(dv);
                    continue;
                } else {
                    String varName = dv.getVariable().getName();
                    int indexStart = varName.lastIndexOf("_");
                    int index = Integer.valueOf(varName.substring(indexStart+1));
                    newdv = new DerivedVariable(dvClone.getVariable(), dv.getNavigation(), index);
                    dvClones.put(dv.getVariable(), newdv);
                }
            }
            localVariables.add(newdv);
        }

    }

    /**
     * Gets the action.
     *
     * @return the action
     */
    public MappingAction getAction() {
        return action;
    }

    @Override
    public boolean isRoot() {
        return isRoot;
    }

    /**
     * Gets the local variables.
     *
     * @return the local variables
     */
    public List<DerivedVariable> getLocalVariables() {
        return localVariables;
    }

    @Override
    public int compareTo(CallActionOperation o) {
        // TODO Auto-generated method stub
        return (action.getLabel()).compareTo(o.action.getLabel());
    }

    @Override
    public String toString() {
        return action.getLabel();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result + (isRoot ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof CallActionOperation))
            return false;
        CallActionOperation other = (CallActionOperation) obj;
        if (action == null) {
            if (other.action != null)
                return false;
        } else if (!action.equals(other.action))
            return false;
        if (isRoot != other.isRoot)
            return false;
        return true;
    }

}