package uk.ac.york.qvtd.compiler.evaluation;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Operation;

public class LogarithmicCostEvaluator extends CostEvaluator {

	@Override
	public double calculateCost(ExecutionPlanGraph plan) {
		// TODO Auto-generated method stub
		return super.calculateCost(plan);
	}

	@Override
	public double getOperationCost(Operation operation) {
		callStack.push(operation);
		double exeCost = getExecutionCost(operation);
		exeCost = exeCost == 0 ? 0 : Math.log10(exeCost);
		double invCost = getInvocationCost(operation);
		invCost = invCost == 0 ? 0 : Math.log10(invCost);
        double cost = exeCost + invCost;
        callStack.pop();
        return cost;
	}

	@Override
	public double getInvocationCost(Operation operation) {
		// TODO Auto-generated method stub
		return super.getInvocationCost(operation);
	}
	
	
	
}
