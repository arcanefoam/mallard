/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;


import org.jgrapht.Graphs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.aco.AntColony;
import uk.ac.york.qvtd.compiler.aco.DaemonAction;
import uk.ac.york.qvtd.compiler.aco.DaemonActionType;
import uk.ac.york.qvtd.compiler.aco.ForagingTrail;

/**
 * The Class EppUpdateTrailDeamon.
 */
public class EppUpdateTrailDeamon extends DaemonAction<InvocationEdge, ForagingEdge> {

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(EppUpdateTrailDeamon.class);

    /** The global best counter. */
    private int globalBestCounter = 0;

    /**
     * Instantiates a new epp update trail deamon.
     */
    public EppUpdateTrailDeamon() {
        super(DaemonActionType.AFTER_ITERATION_CONSTRUCTION);
    }

    @Override
    public void applyDaemonAction(AntColony<InvocationEdge, ForagingEdge> antColony) {

        EppConfiguration config = (EppConfiguration) antColony.getConfigurationProvider();
        EppEnvironment env = (EppEnvironment) antColony.getEnvironment();
        // Update pheromone limits
        ForagingTrail<InvocationEdge, ForagingEdge> globalBestSolution = env.getGlobalBestSolution();
        EppAnt bestAnt = (EppAnt) antColony.getBestPerformingAnt();
        if (globalBestSolution != null) {
            double gbCost = getSolutionCost(globalBestSolution);
            if (bestAnt.getSolutionCost() < gbCost) {
                globalBestSolution = bestAnt.getTrail();
                env.setGlobalBestSolution(bestAnt);
                updateMaxMinPheromoneValues(config, bestAnt.getSolutionCost());
            }
        }
        else {
        	if (bestAnt.getSolutionCost() < Double.MAX_VALUE) {
	            globalBestSolution = bestAnt.getTrail();
	            env.setGlobalBestSolution(globalBestSolution);
	            updateMaxMinPheromoneValues(config, bestAnt.getSolutionCost());
        	}
        }
        logger.trace("UPDATING PHEROMONE TRAILS");
        logger.trace("Performing evaporation on all edges");
        logger.trace("Evaporation ratio: {}", config.getEvaporationRatio());
        env.globalUpdateDecay(config.getEvaporationRatio());
        if (useGlobalBestForUpdate((EppAntColony) antColony)) {
            logger.trace("Depositing pheromone on Global Best Ant trail.");
            if (globalBestSolution == null) {
            	 env.addPheromoneToTrail(bestAnt.getTrail(), 1/bestAnt.getSolutionCost());
            }
            else {
            	env.addPheromoneToTrail(globalBestSolution, 1/globalBestSolution.getWeight());
            }
            
        }
        else {
            logger.trace("Depositing pheromone on Iteration Best Ant trail.");
            env.addPheromoneToTrail(bestAnt.getTrail(), 1/bestAnt.getSolutionCost());
        }
        // MAX-MIN Limits
        env.adjustPheromeWithinLimits(config.getMinimumPheromoneValue(), config.getMaximumPheromoneValue());
    }

    /**
     * Gets the solution cost.
     *
     * @param solution the solution
     * @return the solution cost
     */
    public double getSolutionCost(ForagingTrail<InvocationEdge, ForagingEdge> solution) {
        return Graphs.getPathVertexList(solution).stream()
                .mapToDouble(ie -> ie.getWeight())
                .sum();
    }

    /**
     * From Stuetzle.Hoos2000
     * let f^gb indicate that every f^gb iterations s^gb is allowed to deposit pheromone.
     * To realize this, we apply a specific schedule to alternate the pheromone trail update between s^gb and s^ib
     * In the first 25 iterations only s^ib is used to update the pheromone trails; we set f^gb to 5 for 25 < t <= 75
     * (where t is the iteration counter), to 3 for 75 < t <= 125, to 2 for 125 < t <= 250, and to 1 for t > 250.
     *
     * @param antColony the ant colony
     * @param bestAnt the best ant
     * @return the delta pheromone value
     */
    private boolean useGlobalBestForUpdate(EppAntColony antColony) {
        boolean bestSolution = false;
        int iteration = antColony.getIteration();
        globalBestCounter++;
        if (iteration <= 25) {
            return bestSolution;
        }
        else if(iteration <= 75) {
            if (iteration == 75) globalBestCounter = 0;
            if (globalBestCounter == 5) {
                globalBestCounter = 0;
                bestSolution = true;
            } else {
                bestSolution = false;
            }
        }
        else if(iteration <= 125) {
            if (iteration == 125) globalBestCounter = 0;
            if (globalBestCounter == 3) {
                globalBestCounter = 0;
                bestSolution = true;
            } else {
                bestSolution = false;
            }
        }
        else if(iteration <= 250) {
            if (iteration == 250) globalBestCounter = 0;
            if (globalBestCounter == 2) {
                globalBestCounter = 0;
                bestSolution = true;
            } else {
                bestSolution = false;
            }
        }
        else {
            if (iteration%2 == 0) {
                bestSolution = true;
            } else {
                bestSolution = false;
            }
        }
        //return 1 / getSolutionCost(bestSolution);
        return bestSolution;
    }


    /**
     * Update max min pheromone values.
     *
     * @param config the config
     * @param bestAnt the best ant
     */
    private void updateMaxMinPheromoneValues(EppConfiguration config, double best) {
        // Stuetzle.Hoos2000 t_max is updated each time a new best solution is found
        double t_max =  (1/(1 - config.getEvaporationRatio())) * 1/best;
        config.updateMaxMinPheromoneValues(t_max);
    }



}
