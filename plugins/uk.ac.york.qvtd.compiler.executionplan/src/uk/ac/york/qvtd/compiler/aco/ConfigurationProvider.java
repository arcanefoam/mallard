/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;

// TODO: Auto-generated Javadoc
/**
 * Classes that implement this type provide configuration information to the
 * Problem Solvers. This interface contain parameters used by almost all ACO
 * algorithms.
 *
 * @author Carlos G. Gavidia
 */
public interface ConfigurationProvider {

    /**
     * Pheromone decay factor.
     *
     * @return Pheromone decay factor.
     */
    double getEvaporationRatio();

    /**
     * Heuristic coefficient, controls the amount of contribution for heuristic information  (alpha).
     *
     * @return Heuristic coefficient.
     */
    double getHeuristicImportance();

    /**
     * Initial value of every cell on the Pheromone Matrix.
     *
     * @return Initial pheromone value.
     */
    double getInitialPheromoneValue();

    /**
     * Number of ants used in the algorithm.
     *
     * @return Number of ants.
     */
    int getNumberOfAnts();

    /**
     * Maximum number of iterations.
     *
     * @return Number of iterations.
     */
    int getNumberOfIterations();

    /**
     * History coefficient, controls the amount of contribution of history expressed as
     * pheromone accumulation (beta).
     *
     * @return History coefficient.
     */
    double getPheromoneImportance();

}
