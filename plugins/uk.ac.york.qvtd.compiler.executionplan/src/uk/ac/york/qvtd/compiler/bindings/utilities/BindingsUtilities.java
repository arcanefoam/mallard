package uk.ac.york.qvtd.compiler.bindings.utilities;

import java.util.Map;

import org.eclipse.ocl.pivot.Variable;

import uk.ac.york.qvtd.compiler.Binding;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.bindings.DerivedVariable;
import uk.ac.york.qvtd.compiler.bindings.LocalBinding;
import uk.ac.york.qvtd.compiler.bindings.LoopBinding;
import uk.ac.york.qvtd.compiler.bindings.NavigationBinding;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.compiler.executionplan.LoopOperation;

public class BindingsUtilities {

    public static Binding createLocalBinding(LocalBinding oldBinding, CallActionOperation newOp) {
        DerivedVariable edv = oldBinding.getDerivedVariable();
        // Find the copied bound var
        //if (source instanceof CallActionOperation) {
            for (DerivedVariable dv : ((CallActionOperation) newOp).getLocalVariables()) {
                if (dv.getSourveVariable().getName().equals(edv.getSourveVariable().getName())
                        && dv.getNavigation().equals(edv.getNavigation())
                        && dv.getVariable().getName().equals(edv.getVariable().getName())
                        && dv.getVariable().getType().equals(edv.getVariable().getType())) {
                    return new LocalBinding(oldBinding.getBoundVariable(), dv, oldBinding.getCost());
                }
            }
        //}
        return null;
    }

    public static Binding createLoopBidning(LoopBinding otherBind, Map<Operation, Operation> otherToNewOp) {
        LoopOperation otherOp = otherBind.getSourceLoop();
        LoopOperation newOp = (LoopOperation) otherToNewOp.get(otherOp);
        return new LoopBinding(otherBind.getBoundVariable(), newOp, otherBind.getCost());
    }

    public static NavigationBinding createNavigationBinding(NavigationBinding oldBinding, Variable sourceVar) {

        return new NavigationBinding(oldBinding.getBoundVariable(), sourceVar,
                oldBinding.getCost(),
                oldBinding.getNavigation());
    }

}
