/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.bindings;

import org.eclipse.ocl.pivot.Variable;

import uk.ac.york.qvtd.compiler.Binding;

// TODO: Auto-generated Javadoc
/**
 * A base class from which particular bindings can be derived.
 *
 * Particular bindings should add support for a specific source types, e.g.
 * loops, context, etc.
 */
public abstract class AbstractBinding implements Binding {

    /** The cost. */
    private final long cost;

    /** The bound variable. */
    protected final Variable boundVariable;


    /**
     * Instantiates a new abstract binding.
     *
     * @param cost the cost
     * @param boundVariable the bound variable
     */
    public AbstractBinding(long cost, Variable boundVariable) {
        super();
        this.cost = cost;
        this.boundVariable = boundVariable;
    }

    @Override
    public Variable getBoundVariable() {
        return boundVariable;
    }

    @Override
    public long getCost() {
        return cost;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((boundVariable == null) ? 0 : boundVariable.hashCode());
        result = prime * result + (int) (cost ^ (cost >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof AbstractBinding))
            return false;
        AbstractBinding other = (AbstractBinding) obj;
        if (boundVariable == null) {
            if (other.boundVariable != null)
                return false;
        } else if (!boundVariable.equals(other.boundVariable))
            return false;
        if (cost != other.cost)
            return false;
        return true;
    }



}