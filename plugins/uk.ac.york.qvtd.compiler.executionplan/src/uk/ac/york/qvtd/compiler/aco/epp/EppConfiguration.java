/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.aco.ConfigurationProvider;


/**
 * The Class ExecutionPlanProbelmConfiguration.
 */
public class EppConfiguration implements ConfigurationProvider {

    private static Logger logger = LoggerFactory.getLogger(EppConfiguration.class);

    /** The number of tours. */
    int numberOfTours;

    /** The t_max. */
    // Limits for spanning tree from
    double t_max;		// tao max

    /** The t_min. */
    double t_min;		// tao min

    /** The n. */
    private int n;

    private double tao_p_best;

    private double tao_p_dec;

    /**
     * The maximum number of tour constructions is 2500 n and evaporation ratio is varied between 0.7 and 0.99.
     *
     * @param ig the ig
     */
    public EppConfiguration(InvocationGraph ig) {

        n = ig.vertexSet().size();
        int m = n*n;		// Number of edges
        numberOfTours = 250*n;
        tao_p_best = 0.05;
        tao_p_dec = Math.pow(tao_p_best, 1.0 / n);
        //double init_t = 1 /(1-getEvaporationRatio())*(1/(ig.edgeSet().size()*100)); 	// Assume a very high initial cost
        updateMaxMinPheromoneValues(Double.MAX_VALUE);
    }

    @Override
    public double getEvaporationRatio() {
        // Stuetzle.Hoos2000 4.4.1. Parameter values for trail persistence (they use p = 1-a), so be careful in other
        // places where it is used!
        // For a low number of tour constructions, better tours are found when using lower values, for a larger number
        // of tour constructions, however, using higher values pays off
        // TODO Make this a function of the number of tours?
        return 0.75;		// Stuetzle.Hoos2000 Experimental results
    }

    /**
     * Gets the exploitation probability, q_0 (Dorigo.Gambardella1997), with q (random uniform):
     * <p>
     * If q <= q_0, then next node is selected with exploitation (ACS, else with exploration (AC)
     *
     * @return the exploitation probability
     */
    public double getExploitationProbability() {
        return 0.75;
    }

    @Override
    public double getHeuristicImportance() {
        return 2;		// Stuetzle.Hoos2000 Experimental results
    }

    @Override
    public double getInitialPheromoneValue() {
        //return Integer.MAX_VALUE;
        return 10;
    }

    /**
     * Gets the maximum pheromone value.
     *
     * @return the maximum pheromone value
     */
    public double getMaximumPheromoneValue() {
        return t_max;
    }

    /**
     * Gets the minimum pheromone value.
     *
     * @return the minimum pheromone value
     */
    public double getMinimumPheromoneValue() {
        return t_min;
    }

    @Override
    public int getNumberOfAnts() {
        return 25;		// Stuetzle.Hoos2000 Experimental results
    }

    @Override
    public int getNumberOfIterations() {
        return numberOfTours;
    }


    @Override
    public double getPheromoneImportance() {
        return 1;		// Stuetzle.Hoos2000 Experimental results
    }

    /**
     * Update the Max and Minpheromone values.
     * @param value
     */
    public void updateMaxMinPheromoneValues(double value) {
        t_max = value;
        double avg = n/2;
        t_min = t_max*(1-tao_p_dec)/((avg-1)*tao_p_dec);
        if (t_min > t_max)
            t_min = t_max;
        logger.trace("Setting t_max to: " + t_max);
        logger.trace("Setting t_min to: " + t_min);
    }

    /**
     * Gets the terminating confidence interval width as a percent. E.g. 0.1 means that we want a with difference of
     * 10% of the mean.
     *
     * @return the terminating confidence interval width
     */
    public double getTerminatingConfidenceIntervalWidth() {
        return 0.05;
    }

}
