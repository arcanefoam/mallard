package uk.ac.york.qvtd.compiler.executionplan.utilities;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Taken from http://stackoverflow.com/questions/17192796/generate-all-combinations-from-multiple-lists
 * @author http://stackoverflow.com/users/2173619/v%C3%ADctor
 *
 */
public class Cartesian<T> {

	/**
	 * <ul>Example
	 * <li>Input  = { {a,b,c} , {1,2,3,4} }</li>
	 * <li>Output = { {a,1} , {a,2} , {a,3} , {a,4} , {b,1} , {b,2} , {b,3} , {b,4} , {c,1} , {c,2} , {c,3} , {c,4} }</li>
	 * </ul>
	 *
	 * @param collections Original list of collections which elements have to be combined.
	 * @return Resultant collection of lists with all permutations of original list.
	 */
	public static <T> Collection<List<T>> permutations(Collection<List<T>> collections) {
	  if (collections == null || collections.isEmpty()) {
	    return Collections.emptyList();
	  } else {
	    Collection<List<T>> res = new LinkedList<List<T>>();
	    Iterator<List<T>> it = collections.iterator();
	    permutationsImpl(it, res, new LinkedList<T>());
	    return res;
	  }
	}

	/** Recursive implementation for {@link #permutations(List, Collection)} */
	private static <T> void permutationsImpl(Iterator<List<T>> ori, Collection<List<T>> res, List<T> current) {
	  // if depth equals number of original collections, final reached, add and return
	  if (!ori.hasNext()) {
	    res.add(current);
	    return;
	  }

	  // iterate from current collection and copy 'current' element N times, one for each element
	  List<T> currentCollection = ori.next();
	  for (T element : currentCollection) {
	    List<T> copy = new LinkedList<T>(current);
	    copy.add(element);
	    permutationsImpl(ori, res, copy);
	  }
	}

//	/** Recursive implementation for {@link #permutations(List, Collection)} */
//	private static <T> void permutationsImpl(Collection<List<T>> ori, Collection<List<T>> res, int d, List<T> current) {
//	  // if depth equals number of original collections, final reached, add and return
//	  if (d == ori.size()) {
//	    res.add(current);
//	    return;
//	  }
//
//	  // iterate from current collection and copy 'current' element N times, one for each element
//	  currentCollection = ori.get(d);
//	  for (T element : currentCollection) {
//	    List<T> copy = new LinkedList<T>(current);
//	    copy.add(element);
//	    permutationsImpl(ori, res, d + 1, copy);
//	  }
//	}

}
