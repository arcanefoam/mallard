/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.context;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Type;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.qvtd.pivot.qvtbase.TypedModel;

import uk.ac.york.qvtd.dependencies.inter.DependencyVertex;

/**
 * A context vertex represents a variable in the context and it's relation to
 * other variables in the context can be found by traversing it's edges.
 * @author Horacio Hoyos
 *
 */
public class ContextVertex {

	private final Variable variable;
	private final TypedModel domain;
	private List<Property> setProperties;

	public ContextVertex(Variable variable, TypedModel domain) {
		super();
		this.variable = variable;
		this.domain = domain;
		setProperties = new ArrayList<Property>();
	}


	public Variable getVariable() {
		return variable;
	}


	public Type getType() {
		return variable.getType();
	}

	public TypedModel getDomain() {
		return domain;
	}


	public List<Property> getSetProperties() {
		return setProperties;
	}

	public boolean isValidFor(DependencyVertex source) {
		// TODO Auto-generated method stub
		return true;
	}

}
