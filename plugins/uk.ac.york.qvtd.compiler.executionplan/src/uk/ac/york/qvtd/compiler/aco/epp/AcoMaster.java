/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Element;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.qvtd.pivot.qvtcorebase.analysis.DomainUsage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.aco.Ant;
import uk.ac.york.qvtd.compiler.aco.AntColony;
import uk.ac.york.qvtd.compiler.aco.ForagingTrail;
import uk.ac.york.qvtd.compiler.executionplan.ExecutionPlan;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;

/**
 * The Class AcoMaster.
 */
public class AcoMaster {

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(AcoMaster.class);

    /** The ig. */
    private InvocationGraph ig;

    /** The verbose. */
    private boolean verbose;

    private final Map<Element, DomainUsage> analysis;

    private ExecutionPlan finalPlan;

    private Set<ExecutionPlan> samplePlans;

    private @NonNull EnvironmentFactory qvtEnvironmentFactory;

    /**
     * Instantiates a new aco master.
     *
     * @param dg the dg
     * @param verbose the verbose
     * @param analysis
     * @param qvtpConfig
     */
    public AcoMaster(DependencyGraph dg, boolean verbose, Map<Element, DomainUsage> analysis,
            @NonNull EnvironmentFactory qvtEnvironmentFactory) {
        super();
        this.verbose = verbose;
        this.analysis = analysis;
        this.qvtEnvironmentFactory = qvtEnvironmentFactory;
        ig = new InvocationGraph(dg);
        logger.info("Invocation graph: {}", ig.toDOT());
    }


    /**
     * Produces an Ant Colony instance for the Execution Plan Problem.
     *
     * @param environment the environment
     * @param configurationProvider Algorithm configuration.
     * @param dg the dg
     * @return Ant Colony instance.
     */
    public AntColony<InvocationEdge, ForagingEdge> getAntColony(EppEnvironment environment,
            EppConfiguration configurationProvider) {
        return new EppAntColony(environment, configurationProvider);
    }


    /**
     * @return the finalPlan
     */
    public ExecutionPlan getFinalPlan() {
        return finalPlan;
    }

    /**
     * Solve.
     *
     * @param dg the dg
     */
    public void solve() {
        solve(false);
    }

    public void solve(boolean createSamplePlans) {
        EppConfiguration configurationProvider = new EppConfiguration(ig);
        logger.info("Dependency graph: {}", ig.getDependencyGraph().toDOT());
        EppEnvironment environment = new EppEnvironment(ig, analysis, qvtEnvironmentFactory);
        AntColony<InvocationEdge, ForagingEdge> colony = getAntColony(environment, configurationProvider);
        EppProblemSolver solver = new EppProblemSolver();
        solver.initialize(environment, colony, configurationProvider);
        solver.addDaemonAction(new EppInitActionDeamon());
        solver.addDaemonAction(new EppUpdateTrailDeamon());
        colony.addAntPolicy(new EppNodeSelection());
        try {
            solver.solveProblem();
        } catch (IllegalAccessException e) {
            logger.error("Error running solver", e);
        }
        ForagingTrail<InvocationEdge, ForagingEdge> solution = solver.getBestSolution();
        finalPlan = (ExecutionPlan) solver.createPlan(solution);
        printCostStatistics(colony, solver.getActualIterations());
        printPlanStatistics(colony, solver.getActualIterations());
        logger.info("Final plan: {}", finalPlan.toDOT());
        finalPlan.calculateCost();
        logger.info("Final plan cost: {}", finalPlan.getCost());
        if (createSamplePlans) {
            samplePlans = new HashSet<ExecutionPlan>();
            for (ForagingTrail<InvocationEdge, ForagingEdge> s : solver.getSolutionSample()) {
                samplePlans.add(solver.createPlan(s));
            }
            logger.info("The plan sample size is " + samplePlans.size() + ".");
        }
    }



    // Print the cost for all the solutions of each ant
    // ant1, ant2, ant3, ..... antN
    // c1,   c1,   c1....
    // ...
    // cM
    private void printCostStatistics(AntColony<InvocationEdge, ForagingEdge> colony, int lines) {
        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (int j = 1; j <= colony.getHive().size(); j++) {
            sb.append(sep);
            sb.append("ant" + j);
            sep = ",";
        }
        sb.append("\n");
        for (int l = 0; l < lines; l++) {
            sep = "";
            Iterator<Ant<InvocationEdge, ForagingEdge>> it = colony.getHive().iterator();
            while(it.hasNext()) {
                EppAnt ant = (EppAnt) it.next();
                sb.append(sep);
                if (l < ant.getAllCosts().size()) {
                    Double cost = ant.getAllCosts().get(l);
                    if (cost == Double.MAX_VALUE) {
                        sb.append("NaN");
                    }
                    else {
                        sb.append(cost);
                    }
                }
                else {
                    sb.append("N/A");
                }
                sep = ",";
            }
            sb.append("\n");
        }
        logger.info("{}", sb);
    }

    private void printPlanStatistics(AntColony<InvocationEdge, ForagingEdge> colony, int lines) {
        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (int j = 1; j <= colony.getHive().size(); j++) {
            sb.append(sep);
            sb.append("ant" + j);
            sep = ",";
        }
        sb.append("\n");
        for (int l = 0; l < lines; l++) {
            sep = "";
            Iterator<Ant<InvocationEdge, ForagingEdge>> it = colony.getHive().iterator();
            while(it.hasNext()) {
                EppAnt ant = (EppAnt) it.next();
                sb.append(sep);
                if (l < ant.getAllCosts().size()) {
                    sb.append(ant.getAllHashs().get(l));
                }
                else {
                    sb.append("N/A");
                }
                sep = ",";
            }
            sb.append("\n");
        }
        logger.info("{}", sb);

    }


    public Set<ExecutionPlan> getSamplePlans() {
        return samplePlans;
    }




}
