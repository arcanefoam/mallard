/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler;

/**
 * The Interface Invocation.
 */
public interface Invocation {

    /**
     * Checks if is polled.
     *
     * @return true, if is polled
     */
    public boolean isPolled();

    /**
     * Checks if is looped.
     *
     * @return true, if is looped
     */
    public boolean isLooped();

    /**
     * Sets the polled.
     *
     * @param isPolled the new polled
     */
    public void setPolled(boolean isPolled);

    /**
     * Sets the looped.
     *
     * @param isPolled the new polled
     */
    public void setLooped(boolean isLooped);

    /**
     * Gets the source.
     *
     * @return the source
     */
    public Operation getSource();

    /**
     * Gets the target.
     *
     * @return the target
     */
    public Operation getTarget();

}