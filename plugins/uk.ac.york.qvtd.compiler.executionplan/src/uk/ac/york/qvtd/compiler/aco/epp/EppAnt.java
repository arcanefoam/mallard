/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.hashids.Hashids;
import org.jgrapht.Graphs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.aco.AbstractAnt;
import uk.ac.york.qvtd.compiler.aco.ForagingTrail;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.compiler.executionplan.utilities.ExecutionPlanUtils;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.util.Pair;


/**
 * The Class EppAnt.
 */
public class EppAnt extends AbstractAnt<InvocationEdge, ForagingEdge> {

    private static final long MAX_SEARCH_TIME = 300000;		// 5 mins

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(EppAnt.class);

    /** The starting node. */
    private final InvocationEdge startingNode;

    /**  The Ant builds the Execution Plan as it walks. */
    private ExecutionPlanGraph ep;

    /**  Keep the list of neighbors so we can update it as the ant walks (faster than filter each time). */
    private ArrayList<ForagingEdge> candidateEdges;

    /**  More than the colony, we query the foragingAraea. */
    //private final DirectedGraph<InvocationEdge, ForagingEdge> foragingArea;

    /** The pc pairs. */
    //Set<Pair<MappingAction, MappingAction>> pcPairs = new HashSet<Pair<MappingAction,MappingAction>>();

    private boolean done;

    private List<ForagingTrail<InvocationEdge, ForagingEdge>> allSolutions = new ArrayList<ForagingTrail<InvocationEdge,ForagingEdge>>();

    final private List<Double> allCosts = new ArrayList<Double>();
    final private List<String> allHashs = new ArrayList<String>();

    private double cost;
    private Hashids hashids;

    private long startTime;



    /**
     * Instantiates a new epp ant.
     *
     * @param colony the colony
     */
    public EppAnt(EppAntColony colony) {
        super(colony);
        EppEnvironment env = (EppEnvironment) colony.getEnvironment();
        this.startingNode = env.getStartingVertex();
        ep = env.newExecutionPlan();
        //foragingArea = env.getForagingArea();
        candidateEdges = new ArrayList<ForagingEdge>();
        hashids = new Hashids(EppAntColony.class.getName());
        // Cache the pairs of producer consumer possibilities to validate that the solution is complete.
//        for (InvocationEdge ie : env.getForagingArea().vertexSet()) {
//            if (!ie.equals(startingNode)) {
//                Pair<MappingAction, MappingAction> p = Pair.of(ie.getSource(), ie.getTarget());
//                pcPairs.add(p);
//            }
//        }
    }

    /**
     * @return the allCosts
     */
    public List<Double> getAllCosts() {
        return allCosts;
    }

    /**
     * Gets the all plan hashs. Plans with a different structure should have a different hash.
     *
     * @return the allHashs
     */
    public List<String> getAllHashs() {
        return allHashs;
    }

    /**
     * @return the allSolutions
     */
    public List<ForagingTrail<InvocationEdge, ForagingEdge>> getAllSolutions() {
        return allSolutions;
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.AbstractAnt#getColony()
     */
    @Override
    public EppAntColony getColony() {
        return (EppAntColony) super.getColony();
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.compiler.aco.Ant#getCurrentVertex()
     */
    @Override
    public InvocationEdge getCurrentVertex() {
        InvocationEdge cv = getTrail().getEndVertex();
        // The ant can be in the colony, i.e. reseted
        if (cv == null) {
            cv = getTrail().getStartVertex();
        }
        return cv;
    }

    /**
     * Gets the heuristic value.
     *
     * @param possibleEdge the possible edge
     * @return the heuristic value
     */
    public double getHeuristicValue(ForagingEdge possibleEdge) {

        return ((EppEnvironment) getColony().getEnvironment()).getEdgeHeuristic(possibleEdge);
    }

    /**
     * Since we only want to construct correct plans, the ant can not pick any node as the next node to visit. It has
     * to be a node that results in a feasible/correct plan. Since the ant gradually builds the plan there are two
     * distinct phases:
     * 1. Not all rules have been invoked. In order of preference:
     * 	a. Pick an invocation edge that is a direct consumer of the last invocation
     *  b. If the last invocation has children, pick an invocation edge that is a direct consumer of any of the children
     *  c. Pick an invocation edge from the root.
     * 2. All rules have been invoked. In order of preference
     *  a. If there are dangling types, pick an invocation that consumes them (respecting the producer/consumer relation)
     *  b. If there are dangling types, pick an invocation that consumes them from the root.
     *
     */
    @Override
    public List<ForagingEdge> getNeighbourhood() {
        List<ForagingEdge> neighbours = ((EppEnvironment) getColony().getEnvironment()).getNeighboursOf(getCurrentVertex());
        neighbours.retainAll(candidateEdges);
        // Group multiple invocations edges by source/target
        Map<Pair<MappingAction, MappingAction>, List<ForagingEdge>> targets = neighbours.stream()
                .collect(Collectors.groupingBy(e -> getSourceTargetPair(e),
                        Collectors.mapping(e -> e, Collectors.toList())));

        List<ForagingEdge> valid;
        if (ep.vertexSet().size() == 1) { // Only root
            valid = filterMinimunInvocations(targets);
        } else {
//            List<CallActionOperation> unCalled = ep.vertexSet().stream()
//                    .filter(v -> !v.equals(ep.getRoot()) && ep.inDegreeOf(v) == 0)
//                    .map(CallActionOperation.class::cast)
//                    .collect(Collectors.toList());
            Collection<CallActionOperation> allOps = ((EppEnvironment)getColony().getEnvironment()).getAllOperations();
            List<CallActionOperation> missingMappings = allOps.stream()
                    .filter(v -> !ep.vertexSet().contains(v) || (!v.equals(ep.getRoot()) && ep.inDegreeOf(v) == 0))
                    .collect(Collectors.toList());
            //missingMappings.retainAll(unCalled);
            if (missingMappings.isEmpty()) {		// All rules have been invoked
                neighbours = ((EppEnvironment) getColony().getEnvironment()).getNeighboursOf(getCurrentVertex());
                // Group multiple invocations edges by source/target
                targets = neighbours.stream()
                        .collect(Collectors.groupingBy(e -> getSourceTargetPair(e),
                                Collectors.mapping(e -> e, Collectors.toList())));
                valid = filterAdditionalInvocations(targets);
                valid.removeAll(getTrail().getEdgeList());
            }
            else {
                valid = filterMinimunInvocations(targets);
                // Calling from root is the last alternative, however we don't want to call the consumers of the root,
                // we want to call any uncalled mapping
//                if (valid.isEmpty()) {
                if (valid.size() < 2) {
                    valid.addAll(filterMissingCalls(targets, missingMappings));
                }
            }
        }
        if (valid.isEmpty()) {
            // The ant can't visit any more nodes
            done = true;
            logger.debug("The ant can't add any more edges to the path.");
        }
        return valid;
    }


    /**
     * Gets the plan.
     *
     * @return the plan
     */
    public ExecutionPlanGraph getPlan() {
        return ep;
    }

    @Override
    public double getSolutionCost() {
        return cost;
    }


    @Override
    public boolean isSolutionReady() {
        boolean isReady = false;
        if (startTime == 0) {
            startTime = System.currentTimeMillis();
        }
        else {
            long duration = System.currentTimeMillis() - startTime;
            if (duration > MAX_SEARCH_TIME) {
                logger.warn("Exploration time out");
                allSolutions.add(getTrail());
                cost = Double.MAX_VALUE;
                allCosts.add(cost);  // Discard the possible erroneous plan
                String hash = hashids.encode(getUnsignedInt(Integer.MAX_VALUE));
                allHashs.add(hash);
                return true;
            }
        }
        if (ep.hasMinimumInvocations()) {
            //isReady = ExecutionPlanUtils.isComplete(ep);
            isReady = ExecutionPlanUtils.isCorrect(ep);
        }
        if (!isReady) {
            if (done) {
                // FIXME Can plans be done without being ready? Seems so, for some cases we can not validate that
                // al types are consumed or alternatively that all producer/consumer relations have been called.
                logger.debug("Exploration done without a complete plan");
                allSolutions.add(getTrail());
                cost = Double.MAX_VALUE;
                allCosts.add(cost);  // Discard the possible erroneous plan
                String hash = hashids.encode(getUnsignedInt(Integer.MAX_VALUE));
                allHashs.add(hash);
            }
            return done;
        }
        else { 		// if (isReady)
            allSolutions.add(getTrail());
            cost = super.getSolutionCost();
            allCosts.add(cost);
            String hash = hashids.encode(getUnsignedInt(getTrail().hashCode()));
            allHashs.add(hash);
            startTime = 0;
            return true;
        }
//        if (candidateEdges.isEmpty()) {
//            allSolutions.add(getTrail());
//            allCosts.add((int) getSolutionCost());
//            return true;		// Not other edges we can visit, regardless if plan is correct
//        }
    }

    private long getUnsignedInt(int x) {
        return x & 0x00000000ffffffffL;
    }

    @Override
    public void reset() {
        startTime  = 0;
        EppEnvironment environment = (EppEnvironment) getColony().getEnvironment();
        setTrail(new EppTrail(environment, startingNode));
        candidateEdges.clear();
        candidateEdges.addAll(environment.edgeSet());
        //Clear the ExecutionPlan by removing all edges
        ArrayList<Invocation> edges = new ArrayList<Invocation>(ep.edgeSet());
        ep.removeAllEdges(edges);
        done = false;
        cost = Double.NaN;
    }


    @Override
    public String toString() {
        return Double.toString(this.getSolutionCost());
    }



    @Override
    public void visitNode(ForagingEdge edgeUsed) {

        boolean removed = candidateEdges.remove(edgeUsed);		// Walked ForagingEdge is no longer a candidate
//        if (!removed) {
//            throw new IllegalArgumentException("The visited node does not belong to the set of candidate nodes of this ant.");
//        }
        getTrail().addEdge(edgeUsed);
        edgeUsed.setTraversed(true);
        EppEnvironment environment = (EppEnvironment) getColony().getEnvironment();

        // Use the target InvocationEdge to build the plan
        InvocationEdge ie = environment.getEdgeTarget(edgeUsed);
        addEdgeToExecutionPlan(ie);
        // Color the paths
        environment.visitNode(this, ie);
        // Remove any InvocationEdge that duplicate edgeUsed, i.e. all edges with the same source and target
        // Get all other InvocationEdge with the same source and target
        MappingAction s = ie.getSource();
        MappingAction t = ie.getTarget();
        List<InvocationEdge> parallelInvocation = environment.vertexSet().stream()
                .filter(e -> !e.equals(startingNode) && e.getSource().equals(s) && e.getTarget().equals(t))
                .collect(Collectors.toList());
        // Remove all ForagingEdges that point to the parallel invocations
        for (ForagingEdge eIt : environment.edgeSet()) {
            InvocationEdge ceT = environment.getEdgeTarget(eIt);
            if (parallelInvocation.contains(ceT)) {
                candidateEdges.remove(eIt);
            }
        }
    }

    /**
     * Adds the edge to execution plan.
     *
     * @param ie the ie
     */
    private void addEdgeToExecutionPlan(InvocationEdge ie) {

        ((EppEnvironment) getColony().getEnvironment()).addEdgeToExecutionPlan(ep, ie);
    }

    /**
     * 2. All rules have been invoked. In order of preference
     *  a. If there are reminder types to be consumed add invocations that consume them
     * @param neighbours
     * @param targets
     */
    private List<ForagingEdge> filterAdditionalInvocations(Map<Pair<MappingAction, MappingAction>, List<ForagingEdge>> targets) {

        List<ForagingEdge> valid = new ArrayList<ForagingEdge>();
        Set<ClassDatum> remainder = ExecutionPlanUtils.getRemainderTypes(ep);
        for (ClassDatum t : remainder) {
            List<MappingAction> consumers = ep.getDependencyGraph().getConsumersOf(t);
            Map<Pair<MappingAction, MappingAction>, List<ForagingEdge>> tTargets = targets.entrySet().stream()
                    .filter(e -> consumers.contains(e.getKey().second))
                    .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
            for (MappingAction source : ep.getDependencyGraph().getProducersOf(t)) {
                valid.addAll(filterValidConsumers(tTargets, source));
            }
        }
        if (!remainder.isEmpty() && valid.isEmpty()) {
            valid.addAll(filterValidConsumers(targets, ep.getRoot().getAction()));
        }
        return valid;
    }

    /**
     * Not all rules have been invoked. In order of preference:
     * 	a. Pick invocation edges that are a direct consumer of any of the invoked mappings
     *  b. If the last invocation has children, pick an invocation edge that is a direct consumer of any of the children
     *  c. Pick an invocation from all the existing mappings in the path
     * @param neighbours
     * @param targets
     * @return
     */
    private List<ForagingEdge> filterMinimunInvocations(Map<Pair<MappingAction, MappingAction>, List<ForagingEdge>> targets) {

        //Direct consumers
        List<ForagingEdge> valid = new ArrayList<ForagingEdge>();
        MappingAction lastMapping = getCurrentVertex().getTarget();
        if (lastMapping == null) {	// Initial state, pick the root
            lastMapping = ep.getRoot().getAction();
            valid.addAll(filterValidConsumers(targets, lastMapping));
        }
        else {
            HashSet<Operation> existingOps = new HashSet<Operation>(ep.vertexSet());	// The EP might be modified during the loop
            // For minimum, we only want targets that call uncalled mappings
            for (Operation op : existingOps) {
                if (!op.isRoot() && ep.inDegreeOf(op) > 0) {
                //if (op.isRoot() || ep.inDegreeOf(op) > 0) {
                    MappingAction source = ((CallActionOperation)op).getAction();
                    valid.addAll(filterValidConsumers(targets, source));
                    //valid.addAll(filterValidConsumers(missingTargets, source));
                }
            }
        }
//        if (valid.isEmpty()) {
//            Operation vertex = ((EppEnvironment) getColony().getEnvironment()).getOperationForMapping(lastMapping);
//            // If the last invocation has children, pick an invocation edge that is a direct consumer of any of the children
//            for (Operation child : ep.outgoingEdgesOf(vertex).stream()
//                    .map(e -> e.getTarget())
//                    .collect(Collectors.toList())) {
//                valid.addAll(filterValidEdges(targets, ((CallActionOperation) child).getAction()));
//            }
//        }
//        if (valid.isEmpty()) {
        // Add all possible invocations
        // FIXME I think this loops works forall if I use the target of the edges in the path.
//            List<InvocationEdge> pathVertexList = Graphs.getPathVertexList(getTrail());
//            Collections.reverse(pathVertexList);
//            // Extract the set of
//            for (InvocationEdge ie : pathVertexList) {
//                if (ie.getSource() != null && !ie.getSource().equals(ep.getRoot().getAction())) {		// Ignore the initial edge (i.e. root invocation
//                    valid.addAll(filterValidConsumers(targets, ie.getSource()));
//                }
//            }
//        }
        return valid;
    }

    private Collection<? extends ForagingEdge> filterMissingCalls(
            Map<Pair<MappingAction, MappingAction>, List<ForagingEdge>> targets, List<CallActionOperation> unCalled) {

        List<ForagingEdge> valid = new ArrayList<ForagingEdge>();
        MappingAction source = ep.getRoot().getAction();
        for (CallActionOperation op : unCalled) {
            MappingAction target = op.getAction();
            Iterator<Entry<Pair<MappingAction, MappingAction>, List<ForagingEdge>>> entryIt = targets.entrySet().iterator();
            // Pick invocation edges that provide direct consumers of the last invocation
            while (entryIt.hasNext()) {
                Entry<Pair<MappingAction, MappingAction>, List<ForagingEdge>> entry = entryIt.next();
                if (entry.getKey().first.equals(source) && entry.getKey().second.equals(target)) {
                    for (ForagingEdge fe : entry.getValue()) {
                        if (testInvocation(getColony().getEnvironment().getForagingArea().getEdgeTarget(fe))) {
                            valid.addAll(entry.getValue());
                        }
                    }
                }
            }
        }
        return valid;
    }

    /**
     * Pick invocation edges that provide direct consumers of the last invocation
     * @param targets
     * @param source
     * @return
     */
    private List<ForagingEdge> filterValidConsumers(Map<Pair<MappingAction, MappingAction>, List<ForagingEdge>> targets,
            MappingAction source) {

        List<ForagingEdge> valid = new ArrayList<ForagingEdge>();
        Set<MappingAction> x = ep.getDependencyGraph().getConsumers(source);
        Iterator<Entry<Pair<MappingAction, MappingAction>, List<ForagingEdge>>> entryIt = targets.entrySet().iterator();

        while (entryIt.hasNext()) {
            Entry<Pair<MappingAction, MappingAction>, List<ForagingEdge>> entry = entryIt.next();
            if (entry.getKey().first.equals(source) && x.contains(entry.getKey().second)) {
                for (ForagingEdge fe : entry.getValue()) {
                    if (testInvocation(getColony().getEnvironment().getForagingArea().getEdgeTarget(fe))) {
                        valid.addAll(entry.getValue());
                    }
                }
            }
        }
        return valid;
    }

    /**
     * Gets the source target pair.
     *
     * @param e the e
     * @return the source target pair
     */
    private Pair<MappingAction, MappingAction> getSourceTargetPair(ForagingEdge e) {

        MappingAction source = ((EppEnvironment) getColony().getEnvironment()).getEdgeTarget(e).getSource();
        MappingAction target = ((EppEnvironment) getColony().getEnvironment()).getEdgeTarget(e).getTarget();
        return Pair.of(source, target);
    }

    /**
     * Removes the edge from execution plan.
     *
     * @param invocationEdge the ie
     */
    private void removeEdgeFromExecutionPlan(InvocationEdge invocationEdge) {
        ((EppEnvironment) getColony().getEnvironment()).removeEdgeFromExecutionPlan(ep, invocationEdge);
    }

    /**
     * Test invocation. The execution plan should be correct after adding and invocation. Since invocations can be
     * added at the beginning of the plan and the rest of the plan can still be "incomplete", the validation should be
     * done till the added invocation.
     *
     * @param invocationEdge
     * @return true, if successful
     */
    private boolean testInvocation(InvocationEdge invocationEdge) {
        addEdgeToExecutionPlan(invocationEdge);
        boolean result = true;
        if (!ExecutionPlanUtils.isFeasible(ep)) {
            result = false;
        }
        removeEdgeFromExecutionPlan(invocationEdge);
        return result;
    }



    /**
     * Test if the mapping is invoked in the trail, self invocations dont count
     *
     * @param mapping the mapping
     * @return true, if successful
     */
    private boolean trailInvokes(MappingAction mapping) {
        List<InvocationEdge> visited = Graphs.getPathVertexList(getTrail());
        //return visited.stream().anyMatch(ie -> mapping.equals(ie.getSource()) || mapping.equals(ie.getTarget()));
        return visited.stream().anyMatch(ie -> mapping.equals(ie.getTarget()) && !mapping.equals(ie.getSource()));
    }

}
