/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Class;
import org.eclipse.ocl.pivot.Element;
import org.eclipse.ocl.pivot.Property;
import org.eclipse.ocl.pivot.Variable;
import org.eclipse.ocl.pivot.utilities.EnvironmentFactory;
import org.eclipse.qvtd.compiler.internal.utilities.ClassRelationships;
import org.eclipse.qvtd.pivot.qvtbase.Domain;
import org.eclipse.qvtd.pivot.qvtcorebase.Area;
import org.eclipse.qvtd.pivot.qvtcorebase.RealizedVariable;
import org.eclipse.qvtd.pivot.qvtcorebase.analysis.DomainUsage;
import org.jgrapht.DirectedGraph;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.ext.ComponentAttributeProvider;
import org.jgrapht.ext.EdgeNameProvider;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.ext.StringEdgeNameProvider;
import org.jgrapht.ext.StringNameProvider;
import org.jgrapht.ext.VertexNameProvider;
import org.jgrapht.graph.DirectedPseudograph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.york.att.dggrappa.DGFrame;
import uk.ac.york.qvtd.compiler.Binding;
import uk.ac.york.qvtd.compiler.ExecutionPlanGraph;
import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.aco.AcoEdge;
import uk.ac.york.qvtd.compiler.aco.Environment;
import uk.ac.york.qvtd.compiler.aco.ForagingTrail;
import uk.ac.york.qvtd.compiler.aco.epp.InvocationEdge.Derivation;
import uk.ac.york.qvtd.compiler.bindings.DirectBinding;
import uk.ac.york.qvtd.compiler.bindings.LoopBinding;
import uk.ac.york.qvtd.compiler.bindings.NavigationBinding;
import uk.ac.york.qvtd.compiler.executionplan.AllInstLoop;
import uk.ac.york.qvtd.compiler.executionplan.CallActionInvocation;
import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.compiler.executionplan.ExecutionPlan;
import uk.ac.york.qvtd.compiler.executionplan.LoopOperation;
import uk.ac.york.qvtd.compiler.executionplan.PropNavLoop;
import uk.ac.york.qvtd.dependencies.derivations.DerivationGraph;
import uk.ac.york.qvtd.dependencies.derivations.PropertyEdge;
import uk.ac.york.qvtd.dependencies.derivations.impl.MappingDerivationsImpl;
import uk.ac.york.qvtd.dependencies.inter.ClassDatum;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.DependencyPath;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;
import uk.ac.york.qvtd.dependencies.util.GraphAttributes;
import uk.ac.york.qvtd.dependencies.util.MallardDOTExporter;
import uk.ac.york.qvtd.dependencies.util.Pair;

/**
 * The Class EppEnvironment.
 */
public class EppEnvironment extends ForagingGraph implements Environment<InvocationEdge, ForagingEdge>, Graph<InvocationEdge, ForagingEdge> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8464435762649452787L;

    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(EppEnvironment.class);

    /**
     * Each ant uses a distinct color
     */
    private static final String[] KELLY_COLORS = {
            "#FFB300",    // Vivid Yellow
            "#803E75",    // Strong Purple
            "#FF6800",    // Vivid Orange
            "#A6BDD7",    // Very Light Blue
            "#C10020",    // Vivid Red
            "#CEA262",    // Grayish Yellow
            "#817066",    // Medium Gray
            "#007D34",    // Vivid Green
            "#F6768E",    // Strong Purplish Pink
            "#00538A",    // Strong Blue
            "#FF7A5C",    // Strong Yellowish Pink
            "#53377A",    // Strong Violet
            "#FF8E00",    // Vivid Orange Yellow
            "#B32851",    // Strong Purplish Red
            "#F4C800",    // Vivid Greenish Yellow
            "#7F180D",    // Strong Reddish Brown
            "#93AA00",    // Vivid Yellowish Green
            "#593315",    // Deep Yellowish Brown
            "#F13A13",    // Vivid Reddish Orange
            "#232C16",    // Dark Olive Green
        };

    /** The solution explorer */
    private final DirectedPseudograph<MappingAction, ExplorationEdge> explorer;

    /** The global best solution. */
    private ForagingTrail<InvocationEdge, ForagingEdge> globalBestSolution;

    /** The start node. */
    private InvocationEdge startNode;

    /** The node name provider. */
    // Store the name providers so we always get the same ids
    IntegerNameProvider<InvocationEdge> nodeNameProvider = new MspNameProvider();

    /** The vertex id provider. */
    private IntegerNameProvider<InvocationEdge> vertexIDProvider = new IntegerNameProvider<InvocationEdge>();

    /** The mapping to operation. */
    private Map<MappingAction, CallActionOperation> mappingToOperation = new Hashtable<MappingAction, CallActionOperation>();

    /** The dg. */
    private final DependencyGraph dg;

    /** The DomainUsage analysis. */
    protected final Map<Element, DomainUsage> analysis;

    private DGFrame frame;

    private Map<EppAnt, String> antColors = new HashMap<EppAnt, String>(20);

    private ClassRelationships clasRels;

    /**
     * Instantiates a new epp environment.
     *
     * @param ig the ig
     * @param environmentFactory
     * @param analyusis TODO
     */
    public EppEnvironment(InvocationGraph ig, Map<Element, DomainUsage> analysis, @NonNull EnvironmentFactory environmentFactory) {
        super();
        this.explorer = new DirectedPseudograph<>(ExplorationEdge.class);
        this.dg = ig.getDependencyGraph();
        this.analysis = analysis;
        this.clasRels = new ClassRelationships(environmentFactory);
        generateForagingArea(ig.edgeSet());
        // We need to create ExecutionPlans
        // Create initial operations
        CallActionOperation rop = new CallActionOperation(dg.getRootVertex(), true);
        mappingToOperation.put(dg.getRootVertex(), rop);
        createOperationsForMappings();
    }

    /**
     * Add the and edge to the execution plan that connects the mappings connected by the InvocationEdge. We use the
     * information in the InvocationEdge to add a "dummy" bindning that allow us to keep track of the type used for the
     * invocation.
     * @param ep
     * @param ie
     */
    public void addEdgeToExecutionPlan(ExecutionPlanGraph ep, InvocationEdge ie) {
        CallActionOperation sOp = getOperationForMapping(ie.getSource());
        CallActionOperation tOp = getOperationForMapping(ie.getTarget());
        ep.addVertex(sOp);
        ep.addVertex(tOp);
        if (!ep.getMappingToOperation().containsKey(sOp.getAction()))
            ep.getMappingToOperation().put(sOp.getAction(), sOp);
        if (!ep.getMappingToOperation().containsKey(tOp.getAction()))
            ep.getMappingToOperation().put(tOp.getAction(), tOp);
        // Invocations might be attempted again
        Invocation e = ep.getEdge(sOp, tOp);
        if (e != null) {
            return;
        }
        e = ep.addEdge(sOp, tOp);
        //assert e != null;
        // There might be more than one primary
        for (Variable prim : ie.getPrimary()) {
            // Since it is a dummy binding, we use the same var
            DirectBinding db = new DirectBinding(prim, prim, (long) ie.getWeight());
            ((CallActionInvocation)e).getBindings().put(prim, db);
        }
    }

    /**
     * The EP in this case does not have the information
     * TODO Maybe the EP should never have this information
     * @param action
     * @return
     */
    public CallActionOperation getOperationForMapping(MappingAction action) {
        return mappingToOperation.get(action);
    }

    @Override
    public void addPheromoneToEdge(ForagingEdge edge, double value) {
        edge.addPheromone(value);
    }


    @Override
    public void addPheromoneToTrail(GraphPath<InvocationEdge, ForagingEdge> trail, double value) {
        for (AcoEdge e : trail.getEdgeList()) {
            e.addPheromone(value);
        }
    }

    /* (non-Javadoc)
     * @see uk.ac.york.qvtd.compiler.aco.Environment#addPheromoneToTrail(org.jgrapht.GraphPath, double, double)
     */
    @Override
    @Deprecated //@see adjustPheromeWithinLimits"
    public void addPheromoneToTrail(GraphPath<InvocationEdge, ForagingEdge> trail, double value, double max) {
        for (AcoEdge e : trail.getEdgeList()) {
            e.addPheromone(value);
            // @see adjustPheromeWithinLimits
//            if (e.getPheromone() > max) {
//                e.setPheromone(max);
//            }
        }
    }

    /**
     * Keep pheromone valus wihtin MAX-MIN
     * @param min
     * @param max
     */
    public void adjustPheromeWithinLimits(double min, double max) {
        for (AcoEdge e : edgeSet()) {
            if (e.getPheromone() < min) {
                e.setPheromone(min);
            }
            else if (e.getPheromone() > max) {
                e.setPheromone(max);
            }
        }
    }

    public void createInvocationChain(ExecutionPlanGraph ep, InvocationEdge ie) {
        CallActionOperation sOp = mappingToOperation.get(ie.getSource());
        CallActionOperation tOp = mappingToOperation.get(ie.getTarget());
        int index = 0;
        ep.addVertex(sOp);
        Map<Variable, Binding> tempBindnigs = new LinkedHashMap<Variable, Binding>();
        Invocation lastInvocation = null;
        Operation lastOperation = sOp;
        // Find the context variables to pair the primary variables, if one is not found, it needs an
        // all instances loop.
        for (Variable prim : ie.getPrimary()) {
            Variable sourceVar = findMatchingVar(ie.getSource(), prim);
            if (sourceVar == null) {
                // 1. Create the loop action
                // TODO WE could use the MT created in the abstract exploration
                AllInstLoop la = new AllInstLoop(prim.getType(), analysis.get(prim).getTypedModel(prim), index++);
                // 3. Create a binding for the loop variable, this binding
                // can not be added to the invocation directly as further down we
                // might need to add additional operation/invocation chains
                LoopBinding allInstancesb = new LoopBinding(prim, la, ExecutionPlan.ALL_INS_LOOP_BIDING_COST);
                tempBindnigs.put(prim, allInstancesb);
                // 2. Create a new invocation for the loop
                ep.addVertex(la);
                lastInvocation = ep.addEdge(lastOperation, la);
                lastOperation = la;
            }
            else {
                DirectBinding db = new DirectBinding(prim, sourceVar, ExecutionPlan.DIRECT_BIDING_COST);
                tempBindnigs.put(prim, db);
            }
        }
        Iterator<Derivation> derIt = ie.getDerivations().iterator();
        Variable bindingSource = null;
        Map<List<Property>, Variable> iteratorNavigations = new HashMap<List<Property>, Variable>(); // This navigation leads to this iterator
        while (derIt.hasNext()) {
            Derivation der = derIt.next();
            if (der.primary != null) {
                List<Property> navigation;
                int pathSize = der.path.getEdgeList().size();
                PropertyEdge pe = der.path.getEdgeList().get(pathSize-1);
                Long cost;
                bindingSource = tempBindnigs.get(der.primary.secondary).getSourceVariable();
                Variable targetVar = der.secondary;
                if (pe.getProperty().isIsMany()) {
                    // We need a loop
                    navigation = der.path.getEdgeList().subList(0, pathSize)
                            .stream()
                            .map(e -> e.getProperty())
                            .collect(Collectors.toList());
                    cost = der.path.getEdgeList().subList(0, pathSize)
                            .stream().mapToLong(e -> e.getCost()).sum();
                    // Test if a previous iterator should be used
                    for (Entry<List<Property>, Variable> entry : iteratorNavigations.entrySet()) {
                        if (navigation.size() != entry.getKey().size()) {		// Jan 2017 If the size is the same, no sublist
                            if (Collections.indexOfSubList(navigation, entry.getKey()) == 0) { // They should start at the same position
                                entry.getKey().forEach(p -> navigation.remove(p));		// Don't use remove all as they might be duplicate properties
                                bindingSource = entry.getValue();
                                break;
                            }
                        }
                    }
                    PropNavLoop pnl = new PropNavLoop(bindingSource, targetVar.getType(), navigation, index++);
                    iteratorNavigations.put(navigation, pnl.getLoopVariable());
                    LoopBinding binding = new LoopBinding(targetVar, pnl, cost);
                    tempBindnigs.put(targetVar, binding);
                    // 2. Create a new invocation for the loop
                    ep.addVertex(pnl);
                    lastInvocation = ep.addEdge(lastOperation, pnl);
                    lastOperation = pnl;
                }
                else {
                    navigation = der.path.getEdgeList().stream()
                            .map(e -> e.getProperty())
                            .collect(Collectors.toList());
                    cost = der.path.getEdgeList().stream().mapToLong(e -> e.getCost()).sum();
                    NavigationBinding binding = new NavigationBinding(targetVar, bindingSource, cost, navigation);
                    if (lastOperation instanceof LoopOperation) {
                        ((LoopOperation) lastOperation).getAssociatedLoopBindings().add((NavigationBinding) binding);
                    }
                    tempBindnigs.put(targetVar, binding);
                }
            }
        }
        ep.addVertex(tOp);
        lastInvocation = ep.addEdge(lastOperation, tOp);
        if (lastInvocation == null) {
        	String[] param = {lastOperation.toString(), tOp.toString()};
        	logger.warn("Adding repeated inovcation: {}-{}", param);
        }
        else {
        	((CallActionInvocation) lastInvocation).setBindings(tempBindnigs);
        }
        
    }


    public String explorerToDOT() {
        ComponentAttributeProvider<MappingAction> vertexAttributeProvider = null;
        ComponentAttributeProvider<ExplorationEdge> edgeAttributeProvider = null;

        MallardDOTExporter<MappingAction, ExplorationEdge> ex = new MallardDOTExporter<MappingAction, ExplorationEdge>(
                new GraphAttributes<MappingAction, ExplorationEdge>(false),
                new IntegerNameProvider<MappingAction>(),
                new StringNameProvider<MappingAction>(),
                new StringEdgeNameProvider<ExplorationEdge>(),
                vertexAttributeProvider,
                edgeAttributeProvider);
        StringWriter stringWriter = new StringWriter();
        ex.export(stringWriter, explorer);
        return stringWriter.toString();
    }

    public Collection<CallActionOperation> getAllOperations() {
        return mappingToOperation.values();
    }

    /**
     * @return the clasRels
     */
    public ClassRelationships getClasRels() {
        return clasRels;
    }

    /**
     * Gets the edge heuristic.
     *
     * @param edge the edge
     * @return the edge heuristic
     */
    public double getEdgeHeuristic(ForagingEdge edge) {

        return 1/getEdgeTarget(edge).getWeight();
    }

    @Override
    public DirectedGraph<InvocationEdge, ForagingEdge> getForagingArea() {
        return this;
    }


    /**
     * Gets the global best solution.
     *
     * @return the globalBestSolution
     */
    public ForagingTrail<InvocationEdge, ForagingEdge> getGlobalBestSolution() {
        return globalBestSolution;
    }

    /**
     * Gets the neighbours of.
     *
     * @param currentVertex the current vertex
     * @return the neighbours of
     */
    public List<ForagingEdge> getNeighboursOf(InvocationEdge currentVertex) {

        return new ArrayList<ForagingEdge>(outgoingEdgesOf(currentVertex));
    }

    /**
     * Gets the number of edges.
     *
     * @return the number of edges
     */
    public int getNumberOfEdges() {
        return vertexSet().size();
    }

    @Override
    public String getPheromoneValues() {

        MallardDOTExporter<InvocationEdge, ForagingEdge> ex = new MallardDOTExporter<InvocationEdge, ForagingEdge>(
                new GraphAttributes<InvocationEdge, ForagingEdge>(false),
                vertexIDProvider,
                nodeNameProvider,
                new ForagingEdgeNameProvider());
        StringWriter stringWriter = new StringWriter();
        ex.export(stringWriter, this);
        return stringWriter.toString();
    }

    @Override
    public InvocationEdge getStartingVertex() {
        return startNode;

    }

    @Override
    public void globalUpdateDecay(double factor) {
        assert factor <= 1;		// Assert is a decay
        for (AcoEdge e : edgeSet()) {
            e.decayPheromone(factor);
        }
    }

    @Override
    @Deprecated //@see adjustPheromeWithinLimits"
    public void globalUpdateDecay(double factor, double min) {
        assert factor <= 1;		// Assert is a decay
        for (AcoEdge e : edgeSet()) {
            e.decayPheromone(factor);
            // // @see adjustPheromeWithinLimits
//            if (e.getPheromone() < min) {
//                e.setPheromone(min);
//            }
        }
    }

    public Set<Invocation> incomingEdgesOf(ExecutionPlanGraph ep, MappingAction mapping) {
        CallActionOperation op = mappingToOperation.get(mapping);
        return ep.incomingEdgesOf(op);
    }

    /** Test if the mapping is being invoked in the plan */
    public boolean isInvoked(ExecutionPlanGraph ep, MappingAction mapping) {
        CallActionOperation op = mappingToOperation.get(mapping);
        return !ep.incomingEdgesOf(op).isEmpty();
    }

    @Override
    public boolean isProblemGraphValid() {
        return true;
    }

    public boolean isRoot(MappingAction mapping) {
        return dg.getRootVertex().equals(mapping);
    }

    /**
     * Test if any type in remainder is part of the producer/consumer relations between the pair
     * @param pair
     * @param remainder
     * @return
     */
    public boolean missingRelations(Pair<MappingAction, MappingAction> pair, Set<ClassDatum> remainder) {
        Set<Pair<ClassDatum, MappingAction>> rels = dg.getProducerRelations(pair.first);
        boolean found = false;
        for (ClassDatum remCd : remainder) {
            if (rels.stream().anyMatch(p -> p.first.equals(remCd) && p.second.equals(pair.second))) {
                found = true;
                break;
            }
        }
        return found;
    }

    /**
     * New execution plan.
     *
     * @return the execution plan graph
     */
    public ExecutionPlan newExecutionPlan() {

        return new ExecutionPlan(dg, mappingToOperation.get(dg.getRootVertex()),false);
    }

    /**
     * Removes the edge from execution plan.
     *
     * @param ep the ep
     * @param invocationEdge a pair with the source and target vertices
     */
    public void removeEdgeFromExecutionPlan(ExecutionPlanGraph ep, InvocationEdge invocationEdge) {
        CallActionOperation sOp = getOperationForMapping(invocationEdge.getSource());
        CallActionOperation tOp = getOperationForMapping(invocationEdge.getTarget());
        ep.removeEdge(sOp, tOp);
    }

    public void setGlobalBestSolution(EppAnt bestAnt) {
        this.globalBestSolution = bestAnt.getTrail();
    }

    /**
     * Sets the global best solution.
     *
     * @param globalBestSolution the globalBestSolution to set
     */
    public void setGlobalBestSolution(ForagingTrail<InvocationEdge, ForagingEdge> globalBestSolution) {
        this.globalBestSolution = globalBestSolution;
    }

    @Override
    public void setPheromoneValues(double pheromoneValue) {
        for (AcoEdge e : edgeSet()) {
            e.setPheromone(pheromoneValue);
        }
    }

    /**
     *
     * @return
     */
    public String toDOT() {
        EppExporter exp = new EppExporter(new GraphAttributes<InvocationEdge, ForagingEdge>(false),
                new IntegerNameProvider<InvocationEdge>(),
                new StringNameProvider<InvocationEdge>(),
                new StringEdgeNameProvider<ForagingEdge>(),
                null,
                null);

        StringWriter stringWriter = new StringWriter();
        exp.export(stringWriter, this);
        return stringWriter.toString();

    }

    public void visitNode(EppAnt eppAnt, InvocationEdge ie) {
        //FIXME We dont need colors.. was just for debugging
        String color = antColors.get(eppAnt);
        if (color == null) {
            int index = antColors.size();
            int colorIndex = (index >= 20) ? (index-20) : index;
            color = KELLY_COLORS[colorIndex];
            antColors.put(eppAnt, color);
        }
        explorer.addVertex(ie.getSource());
        explorer.addVertex(ie.getTarget());
        ExplorationEdge eEdge = explorer.getEdge(ie.getSource(), ie.getTarget());
        if (eEdge == null) {
            eEdge = new ExplorationEdge(ie);
            explorer.addEdge(ie.getSource(), ie.getTarget(), eEdge);
        }
        // We want to know the order too
        eEdge.addAnt(color);
    }

    /**
     * Creates the operations for mappings.
     */
    private void createOperationsForMappings() {
        // Root is already mapped
        List<MappingAction> mappings = dg.getMappingVertices();
        mappings.remove(dg.getRootVertex());
        for (MappingAction m : mappings) {
            CallActionOperation op = new CallActionOperation(m, false);
            mappingToOperation.put(m, op);
        }
    }

    /**
     * Find a realized variable in the source mapping that has the same type or a subtype of the variable's type. This
     * variable can be used for binding. We can not use variables (inputs) because if they are guarded, the binding
     * is not "complete".
     * @param source
     * @param variable
     * @return
     */
    private Variable findMatchingVar(MappingAction source, Variable variable) {

        Set<Class> subTypes = new HashSet<Class>(clasRels.getAllSubClasses((Class) variable.getType()));
        subTypes.add((Class) variable.getType());
        for (Domain d : source.getMapping().getDomain()) {
            for (RealizedVariable rv : ((Area) d).getBottomPattern().getRealizedVariable()) {
                if (subTypes.contains(rv.getType())) {
                    return rv;
                }
            }
        }
        return null;
    }

    /**
     * We want two iterators over the vertex set, one fast and one slow.
     * The slow one will move through the set once. For each vertex,
     * the fast iterator moves through the set, adding an edge to all
     * vertices we haven't connected to yet.
     *
     *
     * @param vertexSet the vertex set
     */
    private void generateForagingArea(Set<InvocationEdge> vertexSet) {

        //Add all the vertices to the graph
        // The start node
        startNode = new InvocationEdge(false);
        vertexSet.stream().forEach(ie -> addVertex(ie));
        Iterator<InvocationEdge> slowI = vertexSet().iterator();
        Iterator<InvocationEdge> fastI;
        int id = 0;
        ForagingEdge e;
        while (slowI.hasNext()) { //While there are more vertices in the set
            InvocationEdge latestVertex = slowI.next();
            fastI = vertexSet().iterator();
            //Jump to the first vertex *past* latestVertex
            while (fastI.next() != latestVertex) {
                ;
            }
            //And, add edges to all remaining vertices
            InvocationEdge temp;
            while (fastI.hasNext()) {
                temp = fastI.next();
                e = addEdge(latestVertex, temp);
                e.setId(Integer.toString(id++));
                e = addEdge(temp, latestVertex);
                e.setId(Integer.toString(id++));

            }
        }
        // The start node connects to all the InvocationEdge from the root
        addVertex(startNode);
        for (InvocationEdge v : vertexSet()) {
            if (!v.equals(startNode) && v.isFromRoot()) {
                e = addEdge(startNode, v);
                e.setId(Integer.toString(id++));
            }
        }
        logger.trace("Foragin Area Created: {}", toDOT());
    }

    private class EppExporter extends MallardDOTExporter<InvocationEdge, ForagingEdge> {

        public EppExporter(ComponentAttributeProvider<Graph<InvocationEdge, ForagingEdge>> graphAttributeProvider,
                VertexNameProvider<InvocationEdge> vertexIDProvider,
                VertexNameProvider<InvocationEdge> vertexLabelProvider,
                EdgeNameProvider<ForagingEdge> edgeLabelProvider,
                ComponentAttributeProvider<InvocationEdge> vertexAttributeProvider,
                ComponentAttributeProvider<ForagingEdge> edgeAttributeProvider) {
            super(graphAttributeProvider, vertexIDProvider, vertexLabelProvider, edgeLabelProvider, vertexAttributeProvider,
                    edgeAttributeProvider);
        }

        /* (non-Javadoc)
         * @see uk.ac.york.qvtd.dependencies.util.MallardDOTExporter#export(java.io.Writer, org.jgrapht.Graph)
         */
        @Override
        public void export(Writer writer, Graph<InvocationEdge, ForagingEdge> g) {
            PrintWriter out = new PrintWriter(writer);
            String indent = "  ";
            String connector;
            Map<String, String> attributes = null;
            if (g instanceof DirectedGraph<?, ?>) {
                out.println("digraph G {");
                if (getGraphAttributeProvider() != null) {
                    attributes = getGraphAttributeProvider().getComponentAttributes(g);
                }
                renderAttributes(out, null, attributes, false);
                connector = " -> ";
            } else {
                out.println("graph G {");
                connector = " -- ";
            }
            // We only print visited vertices
            for (InvocationEdge v : g.vertexSet()) {
                if (!g.edgesOf(v).isEmpty()) {
                    out.print(indent + getVertexID(v));

                    String labelName = null;
                    if (getVertexLabelProvider() != null) {
                        labelName = getVertexLabelProvider().getVertexName(v);
                    }

                    if (getVertexAttributeProvider() != null) {
                        attributes = getVertexAttributeProvider().getComponentAttributes(v);
                    }
                    renderAttributes(out, labelName, attributes);

                    out.println(";");
                }
            }
            // We only print visited edges
            for (ForagingEdge e : g.edgeSet()) {
                //if (e.isTraversed()) {
                    String source = getVertexID(g.getEdgeSource(e));
                    String target = getVertexID(g.getEdgeTarget(e));

                    out.print(indent + source + connector + target);

                    String labelName = null;
                    if (getEdgeLabelProvider() != null) {
                        labelName = getEdgeLabelProvider().getEdgeName(e);
                    }
                    if (getEdgeAttributeProvider() != null) {
                        attributes = getEdgeAttributeProvider().getComponentAttributes(e);
                    }
                    renderAttributes(out, labelName, attributes);

                    out.println(";");
                //}
            }

            out.println("}");

            out.flush();
        }

    }

    private class ExplorationEdge {

        private final InvocationEdge originalEdge;

        private final List<String> ants;		// Each ant represented by a color

        public ExplorationEdge(InvocationEdge originalEdge) {
            super();
            this.originalEdge = originalEdge;
            this.ants = new ArrayList<String>();
        }

        public void addAnt(String antColor) {
            ants.add(antColor);
        }

        public InvocationEdge getOriginalEdge() {
            return originalEdge;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            // Make 4 rows of 5 ants each
            int col = 0;
            sb.append("<<TABLE BORDER=\"0\" CELLBORDER=\"1\" CELLSPACING=\"0\" CELLPADDING=\"4\">");
            sb.append("<TR><TD COLSPAN=\"5\">").append(originalEdge.toString()).append("</TD></TR>");
            for (String color : ants) {
                if(col == 0) sb.append("<TR>");
                sb.append("<TD><FONT COLOR=\"").append(color).append("\">").append("@").append("</FONT></TD>");
                if(col == 4) sb.append("</TR>");
                col++;
                if (col > 4) col = 0;
            }
            if (col != 0) sb.append("</TR>");
            sb.append("</TABLE>>");
            return sb.toString();
        }

    }

    /**
     * Numbers the start node as 0.
     *
     * @author hhoyos
     */
    private class ForagingEdgeAttributeProvider implements ComponentAttributeProvider<ForagingEdge> {

        @Override
        public Map<String, String> getComponentAttributes(ForagingEdge component) {
            Map<String, String> attr = new HashMap<String, String>();
            // We use the pheromone for alpha
            double maxPh = edgeSet().stream().mapToDouble(e -> e.getPheromone()).max().getAsDouble();
            // If the edge belongs to the best solution, we paint it green
            long alpha = Math.round(component.getPheromone()*255/maxPh);
            if (alpha > 255) alpha = 255;
            if (globalBestSolution.getEdgeList().contains(component)) {
                attr.put("color", "#007D34" + Integer.toHexString((int) alpha));
            }
            else {
                attr.put("color", "#593315" + Integer.toHexString((int) alpha));
            }
            return attr;
        }

    }

//    private class ExplorationEdgeAttributeProvider implements ComponentAttributeProvider<ExplorationEdge> {
//
//        @Override
//        public Map<String, String> getComponentAttributes(ExplorationEdge component) {
//            Map<String, String> attr = new HashMap<String, String>();
//            String color = component.getColor();
//            if (color != null) {
//                attr.put("color", color);
//            } else {
//                attr.put("color", "#626262");
//            }
//            return attr;
//        }
//    }

    /**
     * Numbers the start node as 0.
     *
     * @author hhoyos
     */
    private class ForagingEdgeNameProvider implements EdgeNameProvider<ForagingEdge> {

        @Override
        public String getEdgeName(ForagingEdge edge) {
            return edge.getId() + "(" + edge.getPheromone() +")";
        }


    }

    /**
     * Numbers the start node as 0.
     *
     * @author hhoyos
     */
    private class MspNameProvider extends IntegerNameProvider<InvocationEdge> {

        @Override public String getVertexName(InvocationEdge vertex)
        {
            if (vertex.equals(startNode)) {
                return "0";
            }
            else {
                return super.getVertexName(vertex);
            }
        }
    }



}
