/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;

/**
 * A behavior to be performed by an Ant. This class is used to implement
 * specific behaviours required by a particular ACO algorithm.
 * <p>
 * <p>
 * Each ant policy class has a policyType, which will define in which stage of
 * the construction process is executed.
 *
 * @author Carlos G. Gavidia
 * @param <V> the value type
 * @param <E> Class representing the Environment.
 */
public abstract class AbstractAntPolicy<V, E> {

    /** The policy type. */
    // TODO(cgavidia): This should be used to program the execution of activities.
    private AntPolicyType policyType;

    /**
     * Instantiates a new abstract ant policy.
     *
     * @param antPhase the ant phase
     */
    public AbstractAntPolicy(AntPolicyType antPhase) {
        this.policyType = antPhase;
    }

    /**
     * This causes a reaction in the ant, e.g. travel to the next node
     *
     * @param ant the ant
     * @return true, if successful
     */
    public abstract boolean applyPolicy(Ant<V, E> ant);

    /**
     * Gets the policy type.
     *
     * @return the policy type
     */
    public AntPolicyType getPolicyType() {
        return policyType;
    }
}