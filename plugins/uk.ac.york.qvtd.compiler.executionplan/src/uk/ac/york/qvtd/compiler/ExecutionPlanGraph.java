/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jgrapht.DirectedGraph;

import uk.ac.york.qvtd.compiler.executionplan.CallActionOperation;
import uk.ac.york.qvtd.dependencies.inter.DependencyGraph;
import uk.ac.york.qvtd.dependencies.inter.MappingAction;

/**
 * The Interface ExecutionPlanGraph.
 */
public interface ExecutionPlanGraph extends DirectedGraph<Operation, Invocation> {

    /**
     * The cost of an all instances was initially considered as 100.
     * However, all instances loops affect the complexity and once a loop
     * exists, the cost of binding an input variable to a loop variable is the
     * same as a direct binding.
     * TODO As with multivalue properties, this value could be tweaked to
     * 1.2 and 1.1 for properties or something like that, if there was a real
     * need to differentiate this bindings from the others.
     */
    int ALL_INS_LOOP_BIDING_COST = 1;

    /** The direct biding cost. */
    int DIRECT_BIDING_COST = 1;

    /**
     * Inserts the edge at the specified position in the sourceVertex outgoing edges.
     *
     * @param index the index
     * @param sourceVertex the source vertex
     * @param targetVertex the target vertex
     * @return the invocation
     */
    Invocation addEdge(int index, Operation sourceVertex, Operation targetVertex);

    /**
     * Inserts the edge at the specified position in the sourceVertex outgoing edges.
     *
     * @param index the index
     * @param sourceVertex the source vertex
     * @param targetVertex the target vertex
     * @param invocation the Invocation
     * @return true, if successful
     */
    boolean addEdge(int index, Operation sourceVertex, Operation targetVertex, Invocation invocation);

    /**
     * Adds the operation for mapping.
     *
     * @param mapping the mapping
     * @return the call action operation
     */
    //CallActionOperation addOperationForMapping(MappingAction mapping);

    /**
     * Assert vertex exist.
     *
     * @param v the v
     * @return true, if successful
     */
    boolean assertVertexExist(Operation v);

    /**
     * Gets the cost.
     *
     * @return the cost
     */
    double getCost();

    /**
     * Gets the dependency graph.
     *
     * @return the dependency graph
     */
    DependencyGraph getDependencyGraph();

    /**
     * Gets the mapping to operation mapping that links mappings to CallActionOperations
     *
     * @return the mapping to operation
     */
    Map<MappingAction, CallActionOperation> getMappingToOperation();

    /**
     * Gets the operation for mapping.
     *
     * @param mapping the mapping
     * @return the operation for mapping
     */
    CallActionOperation getOperationForMapping(MappingAction mapping);

    /**
     * Gets the paths that start at the root and end on the given CallActionOperation
     *
     * @param caop the CallActionOperation
     * @return the paths ending on
     */
    Set<List<MappingAction>> getPathsFromRoot(CallActionOperation caop);

    /**
     * Gets the immediate predecessor actions of the given CallActionOperation
     *
     * @param op the CallActionOperation
     * @return the predecessor actions
     */
    Set<MappingAction> getPredecessorActions(CallActionOperation op);

    /**
     * Gets the root.
     *
     * @return the root
     */
    CallActionOperation getRoot();

    /**
     * Index of outgoing.
     *
     * @param edge the edge
     * @return the int
     */
    int indexOfOutgoing(Invocation edge);

    /**
     * Index of outgoing.
     *
     * @param sourceVertex the source vertex
     * @param edge the edge
     * @return the int
     */
    int indexOfOutgoing(Operation sourceVertex, Invocation edge);

    /**
     * Returns true if each mapping has been invoked the minimun number of times, usually one.
     * @return
     */
    boolean hasMinimumInvocations();


    /**
     * Checks if is scheduled.
     *
     * @param targetOp the target op
     * @return true, if is scheduled
     */
    boolean isScheduled(CallActionOperation targetOp);

    /**
     * Checks if is scheduled.
     *
     * @param mapping the mapping
     * @return true, if is scheduled
     */
    boolean isScheduled(MappingAction mapping);

    /**
     * Outgoing list edges of.
     *
     * @param vertex the vertex
     * @return the list
     */
    List<Invocation> outgoingListEdgesOf(Operation vertex);


    /**
     * Sets the cost.
     *
     * @param cost the new cost
     */
    void setCost(double cost);

    /**
     * Sets the dependency graph.
     *
     * @param dependencyGraph the new dependency graph
     */
    void setDependencyGraph(DependencyGraph dependencyGraph);

    /**
     * Sets the root.
     *
     * @param root the new root
     */
    void setRoot(CallActionOperation root);

    boolean addOperationForMapping(MappingAction mapping, CallActionOperation operation);


}