/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.context;

import org.eclipse.ocl.pivot.Property;

public class ContextEdge {

	ContextVertex source;
	ContextVertex target;

	/**
	 * This should be an OclExpression. Minimally is should be a property, but for
	 * more advance analysis the complete relation could be stored
	 */
	Property relation;

	public ContextEdge(ContextVertex source, ContextVertex target, Property relation) {
		super();
		this.source = source;
		this.target = target;
		this.relation = relation;
	}

	public ContextEdge(ContextVertex source, ContextVertex target) {
		super();
		this.source = source;
		this.target = target;
	}

}
