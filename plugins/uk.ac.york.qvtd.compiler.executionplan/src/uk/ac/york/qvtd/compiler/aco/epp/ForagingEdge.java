/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco.epp;

import uk.ac.york.qvtd.compiler.aco.AcoEdge;

/**
 * The Class ForagingEdge is the basic AcoEdge implementation.
 */
public class ForagingEdge implements AcoEdge {

    /** The pheromone. */
    private double pheromone = 0;

    private String id;

    private boolean traversed;		// Only draw traversed

    @Override
    public void addPheromone(double value) {
        pheromone += value;

    }

    @Override
    public void decayPheromone(double factor) {
        assert factor <= 1;		// We only want to decay
        pheromone *= factor;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    @Override
    public double getPheromone() {
        return pheromone;
    }

    /**
     * @return the traversed
     */
    public boolean isTraversed() {
        return traversed;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void setPheromone(double value) {
        pheromone = value;
    }

    /**
     * @param traversed the traversed to set
     */
    public void setTraversed(boolean traversed) {
        this.traversed = traversed;
    }

    @Override
    public String toString() {
        return getId() + "$" + getPheromone();
    }

}
