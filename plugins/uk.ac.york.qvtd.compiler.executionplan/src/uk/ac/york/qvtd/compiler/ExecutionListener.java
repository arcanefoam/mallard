/*******************************************************************************
 * Copyright (c) 2015 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler;

import org.jgrapht.event.TraversalListener;

public interface ExecutionListener<V, E> extends TraversalListener<V, E> {

    /**
     * Return the state of the invocation listener. Depending on the implementing
     * class, this value can have different meanings, e.g. the plan is candidate,
     * is valid, is complete, etc.
     * @return
     */
    boolean isValid();

}