package uk.ac.york.qvtd.compiler.evaluation;

import org.jgrapht.event.ConnectedComponentTraversalEvent;
import org.jgrapht.event.EdgeTraversalEvent;
import org.jgrapht.event.TraversalListener;
import org.jgrapht.event.VertexTraversalEvent;

import uk.ac.york.qvtd.compiler.Invocation;
import uk.ac.york.qvtd.compiler.Operation;
import uk.ac.york.qvtd.compiler.executionplan.AllInstLoop;

public class LoopReduction implements TraversalListener<Operation, Invocation> {

    private boolean needsMerge = false;
    private Invocation sourceInvocation;
    private Invocation mergedInvocation;


    public boolean needsMerge() {
        return needsMerge;
    }

    public Invocation getSourceInvocation() {
        return sourceInvocation;
    }



    public Invocation getInvocationToReduce() {
        return mergedInvocation;
    }



    @Override
    public void connectedComponentFinished(ConnectedComponentTraversalEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    public void connectedComponentStarted(ConnectedComponentTraversalEvent e) {
        // TODO Auto-generated method stub

    }

    @Override
    // TODO To support PropNavLoop reduction, one would need to check that the
    // variables and properties depict the same derivations in the target CAOP
    public void edgeTraversed(EdgeTraversalEvent<Invocation> e) {
        if (sourceInvocation != null) {
            Invocation current = e.getEdge();
            if (sourceInvocation.getClass().equals(current.getClass())) {
                if (sourceInvocation.getTarget() instanceof AllInstLoop
                        && current.getTarget() instanceof AllInstLoop) {
                    if (((AllInstLoop) sourceInvocation.getTarget()).getModelType()
                            .equals(((AllInstLoop) current.getTarget()).getModelType())) {
                        // We need to find all bindings that use the loop
                        // variable and update them!
                        needsMerge = true;
                        mergedInvocation = current;
                        return;
                    }
                }
            }
        }
        // Always compare against the closest sibling
        sourceInvocation = e.getEdge();
    }

    @Override
    public void vertexTraversed(VertexTraversalEvent<Operation> e) {
        // Each time we visit a new vertex, there is a new set of siblings
        sourceInvocation = null;
    }

    @Override
    public void vertexFinished(VertexTraversalEvent<Operation> e) {
        // TODO Auto-generated method stub

    }




}
