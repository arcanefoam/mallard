/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.Graph;

/**
 * The Class AbstractForagingTrail.
 *
 * @param <V> the value type
 * @param <E> the element type
 */
public class AbstractForagingTrail<V, E> implements ForagingTrail<V, E> {

    /** The graph. */
    private Graph<V, E> graph;

    /** The edge list. */
    private List<E> edgeList;

    /** The start vertex. */
    private V startVertex;

    /** The end vertex. */
    private V endVertex;

    /** The weight. */
    private double weight = 0;

    /**
     * Instantiates a new abstract foraging trail.
     *
     * @param graph the graph
     */
    public AbstractForagingTrail(Graph<V, E> graph) {
        super();
        this.graph = graph;
        this.edgeList = new ArrayList<E>();
    }

    /**
     * Instantiates a new abstract foraging trail.
     *
     * @param graph the graph
     * @param edgeList the edge list
     */
    public AbstractForagingTrail(Graph<V, E> graph, List<E> edgeList) {
        this(graph);
        this.edgeList = edgeList;
        startVertex = graph.getEdgeSource(edgeList.get(0));
        endVertex = graph.getEdgeTarget(edgeList.get(edgeList.size()-1));
    }

    /**
     * Instantiates a new abstract foraging trail.
     *
     * @param graph the graph
     * @param edgeList the edge list
     * @param weight the weight
     */
    public AbstractForagingTrail(Graph<V, E> graph, List<E> edgeList, double weight) {
        this(graph, edgeList);
        this.weight = weight;
    }


    @Override
    public void addEdge(E e) {
        if (edgeList.isEmpty()) {
            startVertex = graph.getEdgeSource(e);
        } else {
            // The ant can not skip nodes
            V source = graph.getEdgeSource(e);
            //V target = g.getEdgeTarget(e);
            assert endVertex.equals(source);
        }
        edgeList.add(e);
        endVertex = graph.getEdgeTarget(e);
    }

    @Override public List<E> getEdgeList() {
        return edgeList;
    }

    @Override public V getEndVertex() {
        return endVertex;
    }

    @Override public Graph<V, E> getGraph() {
        return graph;
    }

    @Override public V getStartVertex() {
        return startVertex;
    }

    @Override public double getWeight() {
        return weight;
    }

    @Override
    public void setStartVertex(V startVertex) {

        this.startVertex = startVertex;
    }

    @Override public String toString() {
        return edgeList.toString();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((edgeList == null) ? 0 : edgeList.hashCode());
        result = prime * result + ((graph == null) ? 0 : graph.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractForagingTrail<V, E> other = (AbstractForagingTrail) obj;
        if (edgeList == null) {
            if (other.edgeList != null)
                return false;
        } else if (!edgeList.equals(other.edgeList))
            return false;
        if (graph == null) {
            if (other.graph != null)
                return false;
        } else if (!graph.equals(other.graph))
            return false;
        return true;
    }





}
