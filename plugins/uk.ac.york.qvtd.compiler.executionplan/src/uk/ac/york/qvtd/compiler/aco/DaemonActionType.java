/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.compiler.aco;
// TODO: Auto-generated Javadoc
/**
 * Types of Daemon Actions. Currently, only two are supported:
 *
 * <ul>
 * <li>Before starting constructing solutions.
 * <li>After the end of a construction iteration. </ul>
 *
 * @author Carlos G. Gavidia
 *
 */
public enum DaemonActionType {

  /** The initial configuration. */
  INITIAL_CONFIGURATION,
 /** The after iteration construction. */
 AFTER_ITERATION_CONSTRUCTION
}
