/**********************************************************
*      This software is part of the graphviz toolset      *
*                http://www.graphviz.org/                 *
*                                                         *
*            Copyright (c) 1994-2005 AT&T Corp.           *
*                and is licensed under the                *
*            Common Public License, Version 1.0           *
*                      by AT&T Corp.                      *
*                                                         *
*        Information and Software Systems Research        *
*              AT&T Research, Florham Park NJ             *
*                                                         *
*               This version available from               *
*                   http://dynagraph.org                  *
**********************************************************/


package uk.ac.york.att.dggrappa;

import java.io.*;
import java.util.StringTokenizer;

import uk.ac.york.att.grappa.*;

import java.util.Enumeration;
import java.awt.Color;

public abstract class ReadServer implements Runnable, GrappaConstants
{
    public static final int NODE_TYPE = Grappa.NODE;
    public static final int EDGE_TYPE = Grappa.EDGE;
    public static final int GRAPH_TYPE = Grappa.SUBGRAPH;

    protected Thread mythread;
    protected class ParseError extends Exception {
	String msg;
	protected ParseError (String s) { msg = s; }
	public String getMessage() { return msg; }
    }
    protected Graph graph = null;
    //protected Animator anim = null;
    //protected Layout layout = null;
    protected boolean ignore = false;
    protected boolean created = false;
    //protected Viewer viewer = null;
    protected long lastframe = 0;
    protected long yieldtime = 20;

    public ReadServer(Graph graph//,Viewer viewer, Layout layout, Animator anim
	) {
	this.graph = graph;
	//this.viewer = viewer;
	//this.layout = layout;
	//this.anim = anim;
    }

    public abstract void run();
    public void stop() { }

    // for filled nodes without a specified fontcolor, set one
    protected void chkFontColor (Element elem)
    {
	GrappaStyle style = null;
	if(elem.getLocalAttribute(FONTCOLOR_ATTR) == null &&  elem.isNode()) {
	    Color fg = (Color)(elem.getAttributeValue(COLOR_ATTR));
            int r = fg.getRed(),g=fg.getGreen(),b=fg.getBlue();
	    float[] hsb = Color.RGBtoHSB(r,g,b,null);
            /*
            // gw: dnu
	    if(hsb[2] <= 0.6) {
		hsb[2] = 1;
	    } else {
		hsb[2] = 0;
	    }
            */
	    elem.setAttribute(FONTCOLOR_ATTR,""+hsb[0]+","+hsb[1]+","+hsb[2]);
	    //elem.setAttribute(STYLE_ATTR,"filled,lineWidth(3),lineColor("+hsb[0]+","+hsb[1]+","+hsb[2]+")");
	}
    }

    public void reconnect() { System.out.println("Reconnect not overriden!"); }
    public void disconnect() { System.out.println("Disconnect not overriden!"); }
}
