/**********************************************************
*      This software is part of the graphviz toolset      *
*                http://www.graphviz.org/                 *
*                                                         *
*            Copyright (c) 1994-2005 AT&T Corp.           *
*                and is licensed under the                *
*            Common Public License, Version 1.0           *
*                      by AT&T Corp.                      *
*                                                         *
*        Information and Software Systems Research        *
*              AT&T Research, Florham Park NJ             *
*                                                         *
*               This version available from               *
*                   http://dynagraph.org                  *
**********************************************************/


package uk.ac.york.att.dggrappa;
import java.io.*;

class ReWriter extends Writer {
    Writer o1,o2;
    ReWriter(Writer o1,Writer o2) {
        this.o1 = o1;
        this.o2 = o2;
    }
    public void write(char[] cbuf, int off, int len) throws java.io.IOException {
        o1.write(cbuf,off,len);
        o2.write(cbuf,off,len);
    }
    public void flush() throws java.io.IOException {
        o1.flush();
        o2.flush();
    }
    public void close() throws java.io.IOException {
        o1.close();
        o2.close();
    }
}
