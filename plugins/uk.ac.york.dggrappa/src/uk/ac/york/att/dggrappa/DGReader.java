/**********************************************************
*      This software is part of the graphviz toolset      *
*                http://www.graphviz.org/                 *
*                                                         *
*            Copyright (c) 1994-2005 AT&T Corp.           *
*                and is licensed under the                *
*            Common Public License, Version 1.0           *
*                      by AT&T Corp.                      *
*                                                         *
*        Information and Software Systems Research        *
*              AT&T Research, Florham Park NJ             *
*                                                         *
*               This version available from               *
*                   http://dynagraph.org                  *
**********************************************************/


package uk.ac.york.att.dggrappa;

import java.io.*;
import java.util.StringTokenizer;

import uk.ac.york.att.grappa.*;

import java.util.Enumeration;
import java.awt.Color;

public class DGReader extends ReadServer implements Runnable {
    private static final int UNKNOWN = 0,
        INSERT  = 1,
        DELETE  = 2,
        MODIFY  = 3,
        OPEN    = 4,
        CLOSE   = 5,
        LOCK    = 6,
        UNLOCK = 7,
        WIRE   = 8,
        SNIP   = 9,
        EXCEPTION = 10,
        MESSAGE = 11;
    public static final int NODE_TYPE = Grappa.NODE;
    public static final int EDGE_TYPE = Grappa.EDGE;
    public static final int GRAPH_TYPE = Grappa.SUBGRAPH;

    interface Events {
        public void inserted(Element ele);
        public void modified(Element ele);
        public void deleting(Element ele);
    }
    Events listener = null;

    Reader input = null;
    InputStream inputstream = null;
    Writer debugOut = null;
    StringBuffer buffer = null;
    int lockcount = 0;

    public boolean colorAge = true;

    private boolean disconnected = true;
    private Object disconnectMonitor;

    public DGReader(InputStream inputstream, Graph graph, Writer debugOut) {
        super(graph);
        this.inputstream = inputstream;
        this.debugOut = debugOut;
        disconnectMonitor = new Object();
        buffer = new StringBuffer(1024);
        reconnect();
    }

    public void reconnect() {
        this.input = new BufferedReader(new InputStreamReader(inputstream),30000);
        disconnected = false;
        mythread = new Thread(null, this, "DGReader");
        mythread.start();
    }

    public void disconnect() {
        if(input != null) {
            // wait until current thread completes processing current
            // line
            disconnected = true;
            try {
                synchronized(disconnectMonitor) {
                    while(mythread != null)
                        disconnectMonitor.wait();
                }
            } catch (InterruptedException e) {}
            try {
                input.close();
            }
            catch(IOException io) {
                System.err.println(io.toString());
            }
            finally {
                input = null;
            }
        }
    }

    public void run() {
        String line = null;
        int ch = 0;
        int lnbr = 0;

        //System.out.println("DGReader thread starting...");
        lastframe = System.currentTimeMillis();

        while(!disconnected) {
            ch = 0;
            buffer.setLength(0);
            while(!disconnected) {
                try {
                    ch = input.read();
                    if(ch == '\n' || ch < 0) break;
                    if(ch == '\r') {
                        ch = input.read();
                        if(ch == '\n' || ch < 0) break;
                        buffer.append('\r');
                    }
                }
                catch(IOException ex) {
                    System.err.println("Exception: " + ex.getMessage());
                    ex.printStackTrace(System.err);
                    System.exit(1);
                }
                buffer.append((char)ch);
            }
            if(ch < 0) break;
            line = buffer.toString();
            lnbr++;
            try {
                processLine(line, lnbr, graph);
            }
            catch (ParseError pe) {
                System.err.println("Error in line:");
                System.err.println(line);
                System.err.println("Parse Exception: " + pe.getMessage() + " - ignored... trace follows:");
                pe.printStackTrace(System.err);
            }
            catch (Token.TokenError te) {
                System.err.println("Error in line:");
                System.err.println(line);
                System.err.println("Token Exception: " + te.getMessage() + " - ignored...trace follows:");
                te.printStackTrace(System.err);
            }
            catch (Exception e) {
                System.err.println("Error in line:");
                System.err.println(line);
                disconnected = true;
            }
        }
        synchronized(disconnectMonitor) {
            mythread = null;
            //System.out.println("DGReader thread exiting");
            disconnectMonitor.notify();
        }
    }

    void processLine(String line, int lineNumber, Graph graph)
    throws ParseError, Token.TokenError, java.io.IOException {
        if(debugOut!=null) {
            debugOut.write("in: "+line+"\n");
            debugOut.flush();
        }
        Token st = new Token(line);

        switch (st.next()) {
            case Token.ID :
                switch (parseCmd(st.idTok())) {
                    case INSERT: doInsert(st);
                        break;
                    case DELETE: doDelete(st);
                        break;
                    case MODIFY: doModify(st);
                        break;
                    case OPEN: doOpen(st);
                        break;
                    case CLOSE:
                        break;
                    case LOCK: doLock(st);
                        break;
                    case UNLOCK: doUnlock(st);
                        break;
                    case WIRE: // doWire(st);
                        break;
                    case SNIP: // doSnip(st);
                        break;
                    case EXCEPTION:
                        String msg = line.substring(11);
                        System.err.println("dynagraph blew up: "+msg);
                        System.exit(1);
                        break;
                    case MESSAGE:
                        st.next();
                        System.err.println("dynagraph message: "+st.idTok());
                        break;
                    case UNKNOWN:
                        System.err.println("unrecognized: "+line);
                        break;
                }
                if(lockcount==0)
                    graph.repaint();
                return;
            default: throw new ParseError("expected command got "+st.idTok());
        }
    }
    private void expectV(Token st) throws Token.TokenError,ParseError {
        if(st.next()!=Token.ID)
            throw new ParseError("expected graph name got "+st.idTok());
        if(!st.idTok().equals("V"))
            throw new ParseError("graph's named V not "+st.idTok());
    }
    private void expectGraphV(Token st) throws Token.TokenError,ParseError {
        if(st.next()!=Token.ID || !st.idTok().equals("graph"))
            throw new ParseError("expected \"graph\" got "+st.idTok());
        expectV(st);
    }
    private void doOpen(Token st) throws ParseError, Token.TokenError {
        expectGraphV(st);
        graph.reset("V",true,false);
    }
    private void doLock(Token st) throws ParseError, Token.TokenError {
        expectGraphV(st);
        lockcount++;
        if(colorAge) {
            // color old things blue (new things will be black)
            float[] hsb = Color.RGBtoHSB(0,0,128,null);
            GraphEnumeration enumer = graph.elements();
            while(enumer.hasMoreElements()) {
                Element ele = (Element)enumer.nextElement();
                ele.setAttribute(COLOR_ATTR,""+hsb[0]+","+hsb[1]+","+hsb[2]);
            }
        }
    }
    private void doUnlock(Token st) throws ParseError, Token.TokenError {
        expectGraphV(st);
        if(lockcount>0)
            lockcount--;
    }
    private void doInsert(Token st) throws ParseError, Token.TokenError {
        Element elem;

        int kind = getKind(st);
        expectV(st);
        String name = getId(st);

        switch(kind) {
            case EDGE_TYPE:
                Node tail = getNode(st);
                Node head = getNode(st);
                elem = graph.findEdgeByName(name);
                if(elem == null)
                    elem = new Edge(graph,tail,head,name);
                else throw new ParseError("Edge " + name + " already exists");

                doParams(elem, st);
                break;
            case NODE_TYPE:
                elem = graph.findNodeByName(name);
                if(elem == null)
                    elem = new Node(graph,name); // set style (filled, etc.) info in chkFontColor
                else throw new ParseError("Node " + name + " already exists");

                doParams(elem, st);
                chkFontColor(elem);
                break;
            default:
                throw new ParseError("insert "+st.idTok()+" makes no sense");
        }

        created = true;

        if(listener!=null)
            listener.inserted(elem);

    }
    private void doModify(Token st) throws ParseError, Token.TokenError {
        if (created) {
            created = false;
            graph.buildShapes();
        }
        Element ele = null;
        int kind = getKind(st);
        expectV(st);
        switch(kind) {
            case NODE_TYPE:
                ele = getNode(st);
                break;
            case EDGE_TYPE:
                ele = getEdge(st);
                break;
            case GRAPH_TYPE:
                ele = graph;
                break;
            default:
                throw new ParseError("modify expected \"graph\",\"node\", or \"edge\"; got "+st.idTok());
        }
        doParams(ele,st);
        if(listener!=null)
            listener.modified(ele);
        lastframe = System.currentTimeMillis();
    }
    private void doDelete(Token st) throws ParseError, Token.TokenError {
        int kind = getKind(st);
        expectV(st);
        String name = getId(st);

        Element elem;
        switch(kind) {
            case NODE_TYPE:
                elem = graph.findNodeByName(name);
                break;
            case EDGE_TYPE:
                elem = graph.findEdgeByName(name);
                break;
            default:
                throw new ParseError("delete "+st.idTok()+" makes no sense!");
        }

        if(listener!=null)
            listener.deleting(elem);
        if(elem != null)
            elem.delete();
    }

    private void getTok(Token st, int tok) throws ParseError, Token.TokenError {
        int token = st.next();
        if (token != tok)
            throw new ParseError("Expected " + Token.toS(tok) + "; found " + Token.toS(token));
    }
    private Node getNode(Token st) throws ParseError, Token.TokenError {
        String name = getId(st);
        Node n = graph.findNodeByName(name);
        if (n == null) throw new ParseError("Could not find node '" + name + "'");
        else return n;
    }
    private Edge getEdge(Token st) throws ParseError, Token.TokenError {
        String name = getId(st);
        Edge e = graph.findEdgeByName(name);
        if (e == null) throw new ParseError("Could not find edge '" + name + "'");
        else return e;
    }
    private String getId(Token st) throws ParseError, Token.TokenError {
        int tok = st.next();
        if (tok == Token.ID) return (st.idTok());
        else throw new ParseError("Expected ID; found " + Token.toS(tok));
    }
    private int getKind(Token st) throws ParseError, Token.TokenError {
        String kind;

        kind = getId(st);
        if (kind.equals("node"))
            return NODE_TYPE;
        else if (kind.equals("edge"))
            return EDGE_TYPE;
        else if(kind.equals("graph"))
            return GRAPH_TYPE;
        else throw new ParseError("Expected graph/node/edge; found " + kind);
    }
    private void doParams(Element elem, Token st) throws ParseError, Token.TokenError {
        int tok = st.next();
        boolean more = true;

        if (tok == Token.EOS) return;
        if (tok != Token.LB)
            throw new ParseError("Parameter list does not begin with [");

        while (more) {
            String name = getId(st);
            getTok(st, Token.EQ);
            String value = getId(st);
            // gw: bad, but i don't understand attr system yet
            if(name.equals("visible"))
                elem.visible = value.equals("true");
            elem.setAttribute(name,value);
            switch (st.next()) {
                case Token.COM : break;
                case Token.RB : more = false; break;
                default : throw new ParseError("Incorrect syntax in parameter list");
            }
        }

    }
    private int parseCmd(String s) {
        switch (s.charAt(0)) {
            case 'l' : if (s.equals("lock")) return LOCK;
            break;
            case 'u' : if (s.equals("unlock")) return UNLOCK;
            break;
            case 'i' : if (s.equals("insert")) return INSERT;
            break;
            case 'd' : if (s.equals("delete")) return DELETE;
            break;
            case 'm' : if (s.equals("modify")) return MODIFY;
            if(s.equals("message")) return MESSAGE;
            break;
            case 'o' : if (s.equals("open")) return OPEN;
            break;
            case 'c' : if (s.equals("close")) return CLOSE;
            break;
            case 'w' : if (s.equals("wire")) return WIRE;
            break;
            case 's' : if (s.equals("snip")) return SNIP;
            break;
            case 'e' : if (s.equals("exception")) return EXCEPTION;
            break;
        }
        return UNKNOWN;
    }
}
