/**********************************************************
*      This software is part of the graphviz toolset      *
*                http://www.graphviz.org/                 *
*                                                         *
*            Copyright (c) 1994-2005 AT&T Corp.           *
*                and is licensed under the                *
*            Common Public License, Version 1.0           *
*                      by AT&T Corp.                      *
*                                                         *
*        Information and Software Systems Research        *
*              AT&T Research, Florham Park NJ             *
*                                                         *
*               This version available from               *
*                   http://dynagraph.org                  *
**********************************************************/


package uk.ac.york.att.dggrappa;

import java.io.*;
import java.util.StringTokenizer;

import uk.ac.york.att.grappa.*;

import java.util.Enumeration;
import java.awt.Color;

public class CopyStream implements Runnable
{
    Reader input = null;
    InputStream inputstream = null;
    PrintWriter output = null;
    StringBuffer buffer = null;
    Thread mythread;

    private boolean disconnected = true;
    private Object disconnectMonitor;

    public CopyStream(InputStream inputstream, PrintWriter output) {
        this.inputstream = inputstream;
        this.output = output;
        disconnectMonitor = new Object();
        buffer = new StringBuffer(1024);
        reconnect();
    }

    public void reconnect() {
        this.input = new BufferedReader(new InputStreamReader(inputstream),30000);
        disconnected = false;
        mythread = new Thread(null, this, "CopyStream");
        mythread.start();
    }

    public void disconnect() {
        if(input != null) {
            // wait until current thread completes processing current
            // line
            disconnected = true;
            try {
                synchronized(disconnectMonitor) {
                    while(mythread != null) {
                        disconnectMonitor.wait();
                    }
                }
            } catch (InterruptedException e) {}
            try {
                input.close();
            }
            catch(IOException io) {
                System.err.println(io.toString());
            }
            finally {
                input = null;
            }
        }
    }

    public void run() {
        String line = null;
        int ch = 0;
        int lnbr = 0;

        while(!disconnected) {
            ch = 0;
            buffer.setLength(0);
            while(!disconnected) {
                try {
                    ch = input.read();
                    if(ch == '\n' || ch < 0) break;
                    if(ch == '\r') {
                        ch = input.read();
                        if(ch == '\n' || ch < 0) break;
                        buffer.append('\r');
                    }
                }
                catch(IOException ex) {
                    System.err.println("Exception: " + ex.getMessage());
                    ex.printStackTrace(System.err);
                    System.exit(1);
                }/*
                catch(InterruptedException ix) {
                        continue;
                }*/
                buffer.append((char)ch);
            }
            if(ch < 0) break;
            line = buffer.toString();
            lnbr++;
            if(line.startsWith("open graph")) // super-lame hack: dynagraph barfs on re-open view so skip
                continue;
            try {
                output.println(line);
                output.flush();
            }
            catch (Exception e) {
                disconnected = true;
            }
        }
        synchronized(disconnectMonitor) {
            mythread = null;
            disconnectMonitor.notify();
        }
    }

}
