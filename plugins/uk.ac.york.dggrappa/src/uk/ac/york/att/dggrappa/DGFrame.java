package uk.ac.york.att.dggrappa;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;

import uk.ac.york.att.grappa.*;

import java.util.*;

/**
 * The Class DGDemoFrame.
 */
public class DGFrame extends JFrame implements ActionListener, ChangeListener, uk.ac.york.att.grappa.GrappaSelection.Events,
        uk.ac.york.att.dggrappa.DGReader.Events
{

    /** The search down. */
    boolean showPipe = false, // whether showing msgs
            bigViewer = true, // whether acting as big-graph browser
            showLeads = true, // whether to show edges leading out of what's shown
            searchUp = true, // which ways to traverse from focus
            searchDown = true;

    /** The max elements. */
    int includeDepth = 3,
            maxElements = 300; // how much to show

    /** The gp. */
    GrappaPanel gp;

    /** The curr graph. */
    Graph bigGraph = null,
            currGraph = null;

    /** The start set. */
    Vector startSet = new Vector();

    /** The adapter. */
    GrappaAdapter adapter = null;

    /** The quit. */
    JButton open = null,
            draw = null,
            layout = null,
            printer = null,
            quit = null;

    /** The left panel. */
    JPanel leftPanel = null;

    /** The selectext. */
    JComboBox selectext = null;

    /** The limit. */
    JTextField limit = null;

    /** The go. */
    JButton go = null;

    /** The depth. */
    JSlider depth = null;

    /** The trav down. */
    JCheckBox leads = null,
            travUp = null,
            travDown = null;


    // the dynagraph process receives input from the GrappaRequests sink,
    /** The dynagraph. */
    // and writes to the ReadServer
    Process dynagraph = null;

    /** The dgreader. */
    DGReader dgreader = null;

    /** The fulfill. */
    DynagraphFulfill fulfill;

    /** The copys. */
    // -i dynagraph input from stream
    CopyStream copys;

    /**
     * Instantiates a new DG demo frame.
     *
     * @param input the input
     * @param dginput the dginput
     * @param dynagraphExe the dynagraph exe
     * @param logfile the logfile
     * @param showPipe the show pipe
     * @param bigViewer the big viewer
     * @param graphattrs the graphattrs
     */
    public DGFrame(InputStream input,InputStream dginput,String dynagraphExe, String logfile,boolean showPipe, boolean bigViewer,Attribute[] graphattrs) {
        super("DGDemoFrame");
        this.showPipe = showPipe;
        this.bigViewer = bigViewer;

        currGraph = new Graph("current view");
        currGraph.setEditable(true);

        setSize(600,400);
        setLocation(100,100);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent wev) {
                Window w = wev.getWindow();
                w.setVisible(false);
                w.dispose();
                System.exit(0);
            }
        });
        try {
            String opts = " -d";
            System.out.println("executing "+dynagraphExe+opts);
            dynagraph = Runtime.getRuntime().exec(dynagraphExe+opts);
        }
        catch(IOException exc) {
            System.err.println("Dynagraph spawn failed!\n");
        }

        JScrollPane jsp = new JScrollPane();
        jsp.getViewport().setBackingStoreEnabled(true);

        /*
    DumpErr derr = new DumpErr(new BufferedReader(new InputStreamReader(dynagraph.getErrorStream())),
        new OutputStreamWriter(System.out));
    Thread errt = new Thread(derr,"DumpErrors");
    errt.start();
         */
        Writer w = new BufferedWriter(new OutputStreamWriter(dynagraph.getOutputStream()), 30000),
                sysout = new OutputStreamWriter(System.out);
        if(showPipe)
            w = new ReWriter(w,sysout);
        if(logfile!=null)
            try {
                w = new ReWriter(w,new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logfile))));
            }
        catch(IOException xep) {
            System.err.println("Couldn't open logfile!");
        }
        PrintWriter out = new PrintWriter(w);
        dgreader = new DGReader(dynagraph.getInputStream(),currGraph,showPipe?sysout:null);
        dgreader.colorAge = bigViewer;
        dgreader.listener = this;
        fulfill = new DynagraphFulfill(out,graphattrs);

        gp = new GrappaPanel(currGraph, fulfill);
        adapter = new GrappaAdapter();
        adapter.currentSelection.listener = this;
        gp.addGrappaListener(adapter);
        gp.setScaleToFit(false);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();

        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.NORTHWEST;

        leftPanel = new JPanel();
        leftPanel.setLayout(gbl);

        open = new JButton("Open");
        gbl.setConstraints(open,gbc);
        leftPanel.add(open);
        open.addActionListener(this);

        draw = new JButton("Draw");
        gbl.setConstraints(draw,gbc);
        leftPanel.add(draw);
        draw.addActionListener(this);

        /*
    layout = new JButton("Layout");
    gbl.setConstraints(layout,gbc);
    leftPanel.add(layout);
    layout.addActionListener(this);

    printer = new JButton("Print");
    gbl.setConstraints(printer,gbc);
    leftPanel.add(printer);
    printer.addActionListener(this);
         */

        quit = new JButton("Quit");
        gbl.setConstraints(quit,gbc);
        leftPanel.add(quit);
        quit.addActionListener(this);

        JPanel topPanel = new JPanel();
        GridBagLayout topLayout = new GridBagLayout();
        topPanel.setLayout(topLayout);

        GridBagConstraints gbc3 = new GridBagConstraints();
        gbc3.gridheight = GridBagConstraints.RELATIVE;
        gbc3.gridwidth = GridBagConstraints.RELATIVE;
        gbc3.weightx = 1;
        gbc3.fill = GridBagConstraints.HORIZONTAL;
        gbc3.anchor = GridBagConstraints.WEST;

        selectext = new JComboBox();
        selectext.setEditable(true);
        topLayout.setConstraints(selectext,gbc3);
        topPanel.add(selectext);
        selectext.addActionListener(this);

        GridBagConstraints gbc2 = new GridBagConstraints();
        gbc2.gridheight = GridBagConstraints.RELATIVE;
        gbc2.anchor = GridBagConstraints.WEST;
        gbc2.gridy = 0;

        go = new JButton("Go");
        go.setDefaultCapable(true);
        topLayout.setConstraints(go,gbc2);
        topPanel.add(go);
        go.addActionListener(this);

        GridBagConstraints gbc4 = new GridBagConstraints();
        gbc4.gridheight = GridBagConstraints.REMAINDER;
        gbc4.anchor = GridBagConstraints.EAST;
        gbc4.fill = GridBagConstraints.HORIZONTAL;
        gbc4.gridy = 1;

        leads = new JCheckBox("Show Leads",showLeads);
        topLayout.setConstraints(leads, gbc4);
        topPanel.add(leads);
        leads.addActionListener(this);

        travUp = new JCheckBox("Search Up",searchUp);
        topLayout.setConstraints(travUp, gbc4);
        topPanel.add(travUp);
        travUp.addActionListener(this);

        travDown = new JCheckBox("Down",searchDown);
        topLayout.setConstraints(travDown, gbc4);
        topPanel.add(travDown);
        travDown.addActionListener(this);

        JLabel sliderLabel = new JLabel("depth:");
        topLayout.setConstraints(sliderLabel, gbc4);
        topPanel.add(sliderLabel);

        GridBagConstraints gbc5 = new GridBagConstraints();
        gbc5.gridheight = GridBagConstraints.REMAINDER;
        gbc5.gridwidth = 2;
        gbc5.anchor = GridBagConstraints.EAST;
        gbc5.gridy = 1;
        gbc5.weightx = 1;
        gbc5.fill = GridBagConstraints.HORIZONTAL;

        depth = new JSlider(0,20,includeDepth);
        depth.setPaintTicks(true);
        topLayout.setConstraints(depth,gbc5);
        topPanel.add(depth);
        depth.addChangeListener(this);

        JLabel maxLabel = new JLabel("max:");
        topLayout.setConstraints(maxLabel, gbc4);
        topPanel.add(maxLabel);

        String v = ""+maxElements;
        limit = new JTextField(v);
        topLayout.setConstraints(limit,gbc4);
        topPanel.add(limit);
        limit.addActionListener(this);

        if(bigViewer)
            getContentPane().add("North", topPanel);
        getContentPane().add("Center", jsp);
        getContentPane().add("West", leftPanel);

        setVisible(true);
        jsp.setViewportView(gp);

        // load input graph first, then start running commands
        openGraph(input);
        if(dginput!=null)
            copys = new CopyStream(dginput,out);

    }

    /* (non-Javadoc)
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    // ActionListener
    public void actionPerformed(ActionEvent evt) {
        if(evt.getSource()==selectext)
            evt = new ActionEvent(go,0,"go");
        if(evt.getSource() instanceof JButton) {
            JButton tgt = (JButton)evt.getSource();
            if(tgt == draw) {
                currGraph.repaint();
            } else if(tgt == open) {
                JFileChooser chooser = new JFileChooser();
                chooser.setCurrentDirectory(new File("."));
                // Note: source for ExtensionFileFilter can be found in the SwingSet demo
                ExampleFileFilter filter = new ExampleFileFilter();
                filter.addExtension("dot");
                filter.setDescription("DOT graph files");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showOpenDialog(this);
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    FileInputStream f = null;
                    try {
                        String fname = chooser.getSelectedFile().getPath();
                        System.err.println("Opening file "+fname);
                        f = new FileInputStream(fname);
                    }
                    catch(FileNotFoundException xep) {}
                    openGraph(f);
                }
            } else if(tgt == quit) {
                System.exit(0);
            } else if(tgt == go) {
                String s = (String)selectext.getSelectedItem();
                selectext.insertItemAt(s,0);
                rereadStartSet(s);
                if(bigViewer) {
                    adapter.currentSelection.clear();
                    for(int i = 0; i<startSet.size(); ++i) {
                        Element E = (Element)startSet.elementAt(i),
                                ele = null;
                        if(E.isNode())
                            ele = findElementByNames(currGraph,E.getName(),null);
                        else if(E.isEdge())
                            ele = findElementByNames(currGraph,((Edge)E).getTail().getName(),((Edge)E).getHead().getName());
                        if(ele!=null)
                            adapter.currentSelection.add(ele);
                    }

                }
                else {
                    adapter.currentSelection.clear();
                    adapter.currentSelection.add(startSet);
                    adapter.currentSelection.yell();
                }
            } else if(tgt == printer) {
                currGraph.printGraph(System.out);
                System.out.flush();
            } else if(tgt == layout) {
                /*
        Object connector = null;
        try {
        connector = Runtime.getRuntime().exec(DGDemo.SCRIPT);
        } catch(Exception ex) {
        System.err.println("Exception while setting up Process: " + ex.getMessage() + "\nTrying URLConnection...");
        connector = null;
        }
        if(connector == null) {
        try {
            connector = (new URL("http://www.research.att.com/~john/cgi-bin/format-graph")).openConnection();
            URLConnection urlConn = (URLConnection)connector;
            urlConn.setDoInput(true);
            urlConn.setDoOutput(true);
            urlConn.setUseCaches(false);
            urlConn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
        } catch(Exception ex) {
            System.err.println("Exception while setting up URLConnection: " + ex.getMessage() + "\nLayout not performed.");
            connector = null;
        }
        }
        if(connector != null) {
        if(!GrappaSupport.filterGraph(graph,connector)) {
            System.err.println("ERROR: somewhere in filterGraph");
        }
        if(connector instanceof Process) {
            try {
            int code = ((Process)connector).waitFor();
            if(code != 0) {
                System.err.println("WARNING: proc exit code is: " + code);
            }
            } catch(InterruptedException ex) {
            System.err.println("Exception while closing down proc: " + ex.getMessage());
            ex.printStackTrace(System.err);
            }
        }
        connector = null;
        }
                 */
                currGraph.repaint();
            }
        } else if(evt.getSource() == leads) {
            boolean newv = leads.isSelected();
            if(showLeads!=newv) {
                showLeads = newv;
                sendOut();
            }
        } else if(evt.getSource() == travUp) {
            boolean newv = travUp.isSelected();
            if(searchUp!=newv) {
                searchUp = newv;
                sendOut();
            }
        } else if(evt.getSource() == travDown) {
            boolean newv = travDown.isSelected();
            if(searchDown!=newv) {
                searchDown = newv;
                sendOut();
            }
        } else if(evt.getSource() == limit) {
            try {
                int newv = Integer.parseInt(limit.getText());
                maxElements = newv;
            }
            catch(Throwable t) {
                limit.setText(String.valueOf(maxElements));
            }
        }


    }

    /* (non-Javadoc)
     * @see att.grappa.GrappaSelection.Events#added(att.grappa.Element)
     */
    // GrappaSelection.Events
    public void added(Element elem) {}

    /* (non-Javadoc)
     * @see att.grappa.GrappaSelection.Events#changed()
     */
    public void changed() {
        String s = "";
        for(int i = 0; i<adapter.currentSelection.v.size(); ++i) {
            if(i>0)
                s += ",";
            s += adapter.currentSelection.v.elementAt(i);
        }
        selectext.setSelectedItem(s);
    }

    /* (non-Javadoc)
     * @see att.dggrappa.DGReader.Events#deleting(att.grappa.Element)
     */
    public void deleting(Element ele) {}

    /* (non-Javadoc)
     * @see att.dggrappa.DGReader.Events#inserted(att.grappa.Element)
     */
    // DGReader.Events
    public void inserted(Element ele) {
        for(int i = 0; i<startSet.size(); ++i) {
            Element E = (Element)startSet.elementAt(i);
            if(ele instanceof Node) {
                if(!(E instanceof Node))
                    continue;
                if(E.getName().equals(ele.getName()))
                    adapter.currentSelection.add(ele);
            }
            else if(ele instanceof Edge) {
                if(!(E instanceof Edge))
                    continue;
                Edge e1 = (Edge)E,
                        e2 = (Edge)ele;
                if(e1.getTail().getName().equals(e2.getTail().getName()) &&
                        e1.getHead().getName().equals(e2.getHead().getName()))
                    adapter.currentSelection.add(ele);
            }
        }
    }

    /* (non-Javadoc)
     * @see att.dggrappa.DGReader.Events#modified(att.grappa.Element)
     */
    public void modified(Element ele) {}

    /* (non-Javadoc)
     * @see att.grappa.GrappaSelection.Events#removed(att.grappa.Element)
     */
    public void removed(Element elem) {}

    /* (non-Javadoc)
     * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
     */
    // ChangeListener
    public void stateChanged(ChangeEvent evt) {
        if(evt.getSource() == depth) {
            int val = depth.getValue();
            if(val != includeDepth) {
                includeDepth = val;
                sendOut();
            }
        }
    }

    /**
     * Bfs.
     *
     * @param subg the subg
     * @param start the start
     * @param cut the cut
     */
    void BFS(Subgraph subg, Node start, int cut) {
        Queue q = new Queue();
        Entry ent = new Entry(start,0);
        q.push(ent);
        while(!q.empty()) {
            Entry entry = q.pop();
            Node n = entry.n;
            int depth = entry.depth;
            // if adding this node and its leads will push us over, make it a lead
            int degree = 0;
            if(showLeads) {
                if(searchUp)
                    degree += n.inDegree();
                if(searchDown)
                    degree += n.outDegree();
            }
            if((subg.countOfElements(GrappaConstants.NODE|GrappaConstants.EDGE)+degree)>maxElements)
                depth = cut+1;
            if(subg.findNodeByName(n.getName())==null) {
                if(depth>cut) // this is a "lead"
                    n.setAttribute("visible","false");
                subg.addNode(n);
            }
            if(depth>cut)
                continue;
            n.setAttribute("visible","true"); // cut>=0 so this is not a "lead"
            if(depth==cut && !showLeads)
                continue;
            Enumeration edges;
            if(searchUp) {
                edges = n.inEdgeElements();
                while(edges.hasMoreElements()) {
                    Edge e = (Edge)edges.nextElement();
                    subg.addEdge(e);
                    Node next = (e.getTail()==n)?e.getHead():e.getTail();
                    q.push(new Entry(next,depth+1));
                }
            }
            if(searchDown) {
                edges = n.outEdgeElements();
                while(edges.hasMoreElements()) {
                    Edge e = (Edge)edges.nextElement();
                    subg.addEdge(e);
                    Node next = (e.getTail()==n)?e.getHead():e.getTail();
                    q.push(new Entry(next,depth+1));
                }
            }
        }
    }

    /**
     * Dfs.
     *
     * @param subg the subg
     * @param n the n
     * @param cut the cut
     */
    void DFS(Subgraph subg, Node n, int cut) {
        // if adding this node and its leads will push us over
        int degree = 0;
        if(showLeads) {
            if(searchUp)
                degree += n.inDegree();
            if(searchDown)
                degree += n.outDegree();
        }
        if((subg.countOfElements(GrappaConstants.NODE|GrappaConstants.EDGE)+degree)>maxElements)
            cut = -1;
        if(subg.findNodeByName(n.getName())==null) {
            if(cut<0) // this is a "lead"
                n.setAttribute("visible","false");
            subg.addNode(n);
        }
        if(cut<0 || cut==0 && !showLeads)
            return;
        n.setAttribute("visible","true"); // cut>=0 so this is not a "lead"
        Enumeration edges;
        if(searchUp) {
            edges = n.inEdgeElements();
            while(edges.hasMoreElements()) {
                Edge e = (Edge)edges.nextElement();
                subg.addEdge(e);
                Node next = (e.getTail()==n)?e.getHead():e.getTail();
                DFS(subg,next,cut-1);
            }
        }
        if(searchDown) {
            edges = n.outEdgeElements();
            while(edges.hasMoreElements()) {
                Edge e = (Edge)edges.nextElement();
                subg.addEdge(e);
                Node next = (e.getTail()==n)?e.getHead():e.getTail();
                DFS(subg,next,cut-1);
            }
        }
    }

    /**
     * Find element by names.
     *
     * @param subg the subg
     * @param t the t
     * @param h the h
     * @return the element
     */
    Element findElementByNames(Subgraph subg,String t,String h) {
        Element tail;
        if((tail = subg.findNodeByName(t))==null)
            if((tail = subg.findEdgeByName(t))==null)
                if((tail = subg.findSubgraphByName(t))==null)
                    return null;
        if(h==null) {
            if(tail!=null)
                return tail;
        }
        else {
            Enumeration enumer = ((Node)tail).outEdgeElements();
            while(enumer.hasMoreElements()) {
                Element e = (Element)enumer.nextElement();
                String n = ((Edge)e).getHead().getName();
                if(n.equals(h))
                    return e;
            }
        }
        return null;
    }

    /**
     * Open graph.
     *
     * @param input the input
     */
    void openGraph(InputStream input) {
        Graph graph = null;
        if(input != null) {
            Parser program = new Parser(input,System.err);
            try {
                //program.debug_parse(4);
                program.parse();
            } catch(Exception ex) {
                System.err.println("Exception: " + ex.getMessage());
                ex.printStackTrace(System.err);
                System.exit(1);
            }
            graph = program.getGraph();
        }
        else
            graph = new Graph("G");

        //System.err.println("The graph contains " + graph.countOfElements(Grappa.NODE|Grappa.EDGE|Grappa.SUBGRAPH) + " elements.");

        graph.setEditable(true);
        //graph.setMenuable(true);
        graph.setErrorWriter(new PrintWriter(System.err,true));
        //graph.printGraph(new PrintWriter(System.out));

        if(bigViewer)
            this.bigGraph = graph;
        else {
            currGraph.setName(graph.getName());
            segue(graph);
        }
    }

    /**
     * Reread start set.
     *
     * @param t the t
     */
    void rereadStartSet(String t) {
        SelectionParser sp = new SelectionParser(t);
        startSet.removeAllElements();
        while(true) {
            SelectionParser.Result r;
            try {
                r = sp.next();
            }
            catch(SelectionParser.Badness b) {
                break;
            }
            if(r.t==null)
                break;
            Element ele = findElementByNames((bigGraph!=null)?bigGraph:currGraph,r.t,r.h);
            if(ele!=null)
                startSet.add(ele);
        }
        sendOut();
    }

    /**
     * Search.
     *
     * @param starts the starts
     * @param depth the depth
     * @return the subgraph
     */
    Subgraph search(Vector starts,int depth) {
        Subgraph subg = new Graph("GGG");//bigGraph);
        for(int i = 0; i<starts.size(); ++i) {
            Element ele = (Element)starts.elementAt(i);
            if(ele.isNode())
                BFS(subg,(Node)ele,depth);
            else if(ele.isEdge()) {
                Edge e = (Edge)ele;
                if(searchDown)
                    BFS(subg,e.getHead(),depth-1);
                if(searchUp)
                    BFS(subg,e.getTail(),depth-1);
            }
        }
        return subg;
    }

    /**
     * Segue.
     *
     * @param subg the subg
     */
    void segue(Subgraph subg) {
        // assure labelsize attr
        Enumeration enumer = subg.nodeElements();
        while(enumer.hasMoreElements()) {
            Node n = (Node)enumer.nextElement();
            if(n.getAttribute(GrappaConstants.LABELSIZE_ATTR)==null) {
                GrappaNexus nexus = n.getGrappaNexus();
                nexus.updateText();
            }
        }
        try {
            fulfill.segue(bigGraph,subg);
            /*
        System.out.println("sent request: "+subg.countOfElements(NODE)+" nodes, "+
            subg.countOfElements(EDGE)+" edges.");
             */
        }
        catch(Throwable throwaway) {}
    }

    /**
     * Send out.
     */
    void sendOut() {
        if(!bigViewer)
            return;
        int depth = includeDepth;
        Subgraph subg;
        /*
    do {
        if(depth<0) {
            System.out.println("too darn big!");
            return;
        }
         */
        subg = search(startSet,depth);
        /*
        Vector nodev = new Vector(); // accumulate first so it doesn't recurse
        Enumeration nodes = subg.nodeElements();
        while(nodes.hasMoreElements()) {
            Node n = (Node)nodes.nextElement();
            n.setAttribute("visible","true");
            nodev.add(n);
        }
        if(showLeads) {
            // add invisible nodes for things just off the edge
            for(int i = 0; i<nodev.size(); ++i) {
                Node n = (Node)nodev.elementAt(i);
                Enumeration edges = n.edgeElements();
                while(edges.hasMoreElements()) {
                    Edge e = (Edge)edges.nextElement();
                    Node next = (e.getTail()==n)?e.getHead():e.getTail();
                    subg.addNode(next);
                    subg.addEdge(e);
                }
            }
        }
        depth--;
    } while(subg.countOfElements(NODE|EDGE)>maxElements);
         */
        segue(subg);
    }

    /**
     * The Class Entry.
     */
    public class Entry {

        /** The n. */
        Node n;

        /** The depth. */
        int depth;

        /**
         * Instantiates a new entry.
         *
         * @param n the n
         * @param depth the depth
         */
        Entry(Node n,int depth) {
            this.n = n;
            this.depth = depth;
        }
    }

    /**
     * The Class Queue.
     */
    class Queue {

        /** The v. */
        Vector v = new Vector();

        /** The front. */
        int front = 0;

        /**
         * Empty.
         *
         * @return true, if successful
         */
        public boolean empty() {
            return front>=v.size();
        }

        /**
         * Pop.
         *
         * @return the entry
         */
        public Entry pop() {
            return (Entry)v.elementAt(front++);
        }

        /**
         * Push.
         *
         * @param e the e
         */
        public void push(Entry e) {
            v.add(e);
        }
    }

    /**
     * The Class SelectionParser.
     */
    class SelectionParser {

        /** The i. */
        int i;

        /** The s. */
        String s;

        /**
         * Instantiates a new selection parser.
         *
         * @param s the s
         */
        SelectionParser(String s) {
            this.s = s;
            i = 0;
        }

        /**
         * Next.
         *
         * @return the result
         * @throws Badness the badness
         */
        public Result next() throws Badness {
            Result r = new Result();
            if(i>=s.length())
                return r;
            r.t = getName();
            skipWhitespace();
            if(i==s.length() || s.charAt(i)==',') {
                ++i;
                return r;
            }
            else if(s.charAt(i)=='-') {
                ++i;
                if(s.charAt(i)!='>')
                    throw new Badness(i);
                ++i;
                r.h = getName();
                return r;
            }
            else throw new Badness(i);
        }

        /**
         * Gets the name.
         *
         * @return the name
         */
        String getName() {
            skipWhitespace();
            if(s.charAt(i)=='"') {
                int j = i;
                while(s.charAt(++j)!='"');
                String ret = s.substring(i+1,j);
                i = j+1;
                return ret;
            }
            else {
                int j;
                for(j = i; j<s.length(); ++j) {
                    char c = s.charAt(j);
                    if(c==' ' || c=='-' || c==',')
                        break;
                }
                String ret = s.substring(i,j);
                i = j;
                return ret;
            }
        }

        /**
         * Skip whitespace.
         */
        void skipWhitespace() { // sorry
            while(i<s.length() && s.charAt(i)==' ') ++i;
        }

        /**
         * The Class Result.
         */
        public class Result {

            /** The h. */
            public String t,h; // h==null if node
        }

        /**
         * The Class Badness.
         */
        class Badness extends Throwable {

            /** The where. */
            int where;

            /**
             * Instantiates a new badness.
             *
             * @param where the where
             */
            Badness(int where) {
                this.where = where;
            }
        }
    }
}