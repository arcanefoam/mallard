/**********************************************************
*      This software is part of the graphviz toolset      *
*                http://www.graphviz.org/                 *
*                                                         *
*            Copyright (c) 1994-2005 AT&T Corp.           *
*                and is licensed under the                *
*            Common Public License, Version 1.0           *
*                      by AT&T Corp.                      *
*                                                         *
*        Information and Software Systems Research        *
*              AT&T Research, Florham Park NJ             *
*                                                         *
*               This version available from               *
*                   http://dynagraph.org                  *
**********************************************************/


package uk.ac.york.att.dggrappa;

import java.io.*;
import java.util.StringTokenizer;

import uk.ac.york.att.grappa.*;

import java.util.Enumeration;
import java.awt.Color;

public class DumpErr implements Runnable
{
    Reader input = null;
    Writer output = null;
    DumpErr(Reader input, Writer output) {
        this.input = input;
        this.output = output;
    }
    public void run() {
        StringBuffer buffer = new StringBuffer(1024);
        System.err.println("DumpErr thread starting...");
        try {
            while(true) {
                while(true) {
                    int ch;
                    if(input.ready()) {
                        ch = input.read();
                        if(ch == '\n' || ch < 0) break;
                        if(ch != '\r')
                            buffer.append(ch);
                    }
                }
                String s = buffer.toString();
                output.write("err: "+s+"\n");
            }
        }
        catch(IOException xep) {
        }
    }
}
