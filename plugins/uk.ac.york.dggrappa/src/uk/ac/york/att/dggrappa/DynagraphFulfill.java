/**********************************************************
*      This software is part of the graphviz toolset      *
*                http://www.graphviz.org/                 *
*                                                         *
*            Copyright (c) 1994-2005 AT&T Corp.           *
*                and is licensed under the                *
*            Common Public License, Version 1.0           *
*                      by AT&T Corp.                      *
*                                                         *
*        Information and Software Systems Research        *
*              AT&T Research, Florham Park NJ             *
*                                                         *
*               This version available from               *
*                   http://dynagraph.org                  *
**********************************************************/


package uk.ac.york.att.dggrappa;

import java.io.*;

import uk.ac.york.att.grappa.*;

public class DynagraphFulfill implements GrappaRequests {
    public PrintWriter writer;
    int nodeN = 0, edgeN = 0;

    void send(String s) {
        writer.println(s);
        writer.flush();
    }
    String attrString(Attribute[] attrs) {
        String ret = "[";
        for(int i = 0; i<attrs.length; ++i) {
            if(i!=0)
                ret += ",";
            ret += Element.canonString(attrs[i].getName()) + "=" + Element.canonString(attrs[i].getStringValue());
        }
        return ret+"]";
    }
    DynagraphFulfill(PrintWriter writer,Attribute[] attrs) {
        this.writer = writer;
        send("open graph V " + attrString(attrs));
    }
    public void lock(boolean whether) {
        if(whether)
            send("lock graph V");
        else
            send("unlock graph V");
    }
    public void segue(Subgraph subg, Subgraph newg) {
        writer.println("segue graph V");
        newg.printSubgraph(writer);
        writer.flush();
    }
    public void insertNode(Subgraph subg, Attribute[] attrs) {
        send("insert node V n" + nodeN++ + " "+attrString(attrs));
    }
    public void insertEdge(Subgraph subg, Node tail, Node head, Attribute[] attrs) {
        send("insert edge V e" + edgeN++ + " " + Element.canonString(tail.getName())
            + " " + Element.canonString(head.getName()) + " "+attrString(attrs));
    }
    public void modifyNode(Subgraph subg, Node node, Attribute[] attrs) {
        send("modify node V " + Element.canonString(node.getName()) + " " + attrString(attrs));
    }
    public void modifyEdge(Subgraph subg, Edge edge, Attribute[] attrs) {
        send("modify edge V " + Element.canonString(edge.getName()) + " " + attrString(attrs));
    }
    public void deleteNode(Subgraph subg, Node node) {
        send("delete node V "+Element.canonString(node.getName()));
    }
    public void deleteEdge(Subgraph subg, Edge edge) {
        send("delete edge V "+Element.canonString(edge.getName()));
    }

};
