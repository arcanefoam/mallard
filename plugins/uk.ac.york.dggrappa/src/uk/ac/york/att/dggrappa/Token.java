/**********************************************************
*      This software is part of the graphviz toolset      *
*                http://www.graphviz.org/                 *
*                                                         *
*            Copyright (c) 1994-2005 AT&T Corp.           *
*                and is licensed under the                *
*            Common Public License, Version 1.0           *
*                      by AT&T Corp.                      *
*                                                         *
*        Information and Software Systems Research        *
*              AT&T Research, Florham Park NJ             *
*                                                         *
*               This version available from               *
*                   http://dynagraph.org                  *
**********************************************************/


package uk.ac.york.att.dggrappa;

public class Token
{
  class TokenError extends Exception {
    String msg;
    public TokenError (String s) { msg = s; }
    public String getMessage () { return msg; }
  }

  public static final int EOS = 0,
                          EQ  = 1,
                          LB  = 2,
                          RB  = 3,
                          COM = 4,
                          ID  = 5;
  String buf = null;
  int    len = 0;
  int    idx = 0;
  String tok = null;

  public Token (String s) { buf = s; len = s.length(); }

  public String idTok () { return tok; }

  public static String toS (int tok)
  {
    String s = null;

    switch (tok) {
      case EOS : s = "<eos>";
        break;
      case EQ : s = "=";
        break;
      case LB : s = "[";
        break;
      case RB : s = "]";
        break;
      case COM : s = ",";
        break;
      case ID : s = "ID";
        break;
    }

    return s;
  }
    /*
    this is the definition of ID from agraph/incrface lexers
    LETTER [A-Za-z_\200-\377]
    DIGIT	[0-9]
    NAME	{LETTER}({LETTER}|{DIGIT})*
    NUMBER	[-]?(({DIGIT}+(\.{DIGIT}*)?)|(\.{DIGIT}+))
    ID		({NAME}|{NUMBER})

     absurd solution follows, adapted from dynagraph
    */
    boolean isLetteror_(char ch) {
        return Character.isLetter(ch)||ch=='_';
    }
    private boolean id() throws TokenError{
        int state = 0, start = --idx; // review character that next() didn't know
        scan:
        for(;idx<len;idx++) {
            char curr = buf.charAt(idx);
            switch(state) {
                case 0:
                    if(isLetteror_(curr))
                        state = 1;
                    else if(curr=='-')
                        state = 2;
                    else if(Character.isDigit(curr))
                        state = 3;
                    else if(curr=='.')
                        state = 4;
                    else break scan;
                    break;
                case 1:
                    if(isLetteror_(curr)||Character.isDigit(curr))
                        state = 1;
                    else break scan;
                    break;
                case 2:
                    if(Character.isDigit(curr))
                        state = 3;
                    else if(curr=='.')
                        state = 4;
                    else
                        break scan;
                    break;
                case 3:
                    if(Character.isDigit(curr))
                        state = 3;
                    else if(curr=='.')
                        state = 5;
                    else
                        break scan;
                    break;
                case 4:
                    if(Character.isDigit(curr))
                        state = 5;
                    else
                        break scan;
                    break;
                case 5:
                    if(Character.isDigit(curr))
                        state = 5;
                    else
                        break scan;
            }
        }
        // if state is zero, nothing was processed before coming to something that wasn't
        // id material.  if state is odd, the machine above landed in accepting state
        // if it's even it's "." or "-.": no good!

        if((state%2)==0)
            throw new TokenError("incomplete number token \"" + buf.substring(start) + '"');
        tok = buf.substring (start, idx);
        return true;
    }

  public int next () throws TokenError
  {
    char c;

    skipWS ();

    if (idx == len) return EOS;

    c = buf.charAt(idx);
    idx++;
    switch (c) {
      case '='  : return EQ;
      case '['  : return LB;
      case ']'  : return RB;
      case ','  : return COM;
      case '\"' : quoted ();
                  return ID;
      default   : if(id())
                    return ID;
                  else
                    throw new TokenError("illegal character (" + c + ")");

    }
  }

  private void skipWS ()
  {
    for (; idx < len; idx++)
      if (!Character.isWhitespace (buf.charAt(idx))) return;
  }

  private void quoted ()
  {
    int start = idx;
    int endq = idx;
    char c;

    for (; endq < len; endq++) {
      c = buf.charAt(endq);
      if (c == '\\') {if (endq < (len-1)) endq++; }
      else if (c == '\"') break;
    }
    if (endq < len) idx = endq+1;
    else idx = len;
    tok = buf.substring (start, endq);
  }

}
