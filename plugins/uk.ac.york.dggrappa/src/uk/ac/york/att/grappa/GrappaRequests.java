/**********************************************************
*      This software is part of the graphviz toolset      *
*                http://www.graphviz.org/                 *
*                                                         *
*            Copyright (c) 1994-2005 AT&T Corp.           *
*                and is licensed under the                *
*            Common Public License, Version 1.0           *
*                      by AT&T Corp.                      *
*                                                         *
*        Information and Software Systems Research        *
*              AT&T Research, Florham Park NJ             *
*                                                         *
*               This version available from               *
*                   http://dynagraph.org                  *
**********************************************************/


package uk.ac.york.att.grappa;
import uk.ac.york.att.grappa.*;
/**
 * An interface to describe changes that can be made to a graph.  The
 * response may be to make the change immediately (e.g. GrappaSimpleFulfill)
 * or to pass the request on to a layout server (e.g. DynagraphFulfill)
 *
 * @version 1.2, 31 Jul 2001; Copyright 1996 - 2001 by AT&T Corp.
 * @author  <a href="mailto:gordon@research.att.com">Gordon Woodhull</a>, <a href="http://www.research.att.com">Research @ AT&T Labs</a>
 */

public interface GrappaRequests {
  public void lock(boolean whether);
  public void segue(Subgraph subg,Subgraph newg);
  public void insertNode(Subgraph subg,Attribute[] attrs);
  public void insertEdge(Subgraph subg,Node tail, Node head, Attribute[] attrs);
  public void modifyNode(Subgraph subg,Node node, Attribute[] attrs);
  public void modifyEdge(Subgraph subg,Edge edge, Attribute[] attrs);
  public void deleteNode(Subgraph subg,Node node);
  public void deleteEdge(Subgraph subg,Edge edge);
};
