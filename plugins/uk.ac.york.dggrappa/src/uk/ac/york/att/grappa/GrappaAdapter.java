/**********************************************************
*      This software is part of the graphviz toolset      *
*                http://www.graphviz.org/                 *
*                                                         *
*            Copyright (c) 1994-2005 AT&T Corp.           *
*                and is licensed under the                *
*            Common Public License, Version 1.0           *
*                      by AT&T Corp.                      *
*                                                         *
*        Information and Software Systems Research        *
*              AT&T Research, Florham Park NJ             *
*                                                         *
*               This version available from               *
*                   http://dynagraph.org                  *
**********************************************************/


package uk.ac.york.att.grappa;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.event.InputEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JViewport;
import java.util.*;

/**
 * A convenience class that implements the GrappaListener interface
 * for handling mouse-related activity that occurs on a graph.
 *
 * This particular GrappaListener implementation allows the following
 * interactions with a displayed graph:
 *
 * <UL>
 * <LI>
 * display tooltips for each graph element;
 * <LI>
 * button-1 click will select an element;
 * <LI>
 * button-1 sweep will select several elements;
 * <LI>
 * shift-button 1 click will create a node;
 * <LI>
 * shift-button 1 drag, when starting in one node and ending in another, will create an edge;
 * <LI>
 * button-2 or button-3 click will raise a pop-up option menu with:
 *   <UL>
 *   <LI>
 *   zoom in, zoom out and reset zoom options;
 *   <LI>
 *   a zoom-to-sweep option, if applicable;
 *   <LI>
 *   clear selection, enclose the selected items in a new subgraph,
 *   preview deletion, cancel preview and perform deletion, if at least
 *   one item is selected;
 *   <LI>
 *   a tooltip on/off toggle option.
 *   </UL>
 * </UL>
 *
 * @version 1.2, 12 Jun 2001; Copyright 1996 - 2001 by AT&T Corp.
 * @author  <a href="mailto:john@research.att.com">John Mocenigo</a>, <a href="http://www.research.att.com">Research @ AT&T Labs</a>
 */
public class GrappaAdapter
    implements GrappaConstants, GrappaListener, ActionListener
{
    /**
     * Reference to the selection object
     */
    public GrappaSelection currentSelection = new GrappaSelection();

  /**
   * The method called when a mouse click occurs on a displayed subgraph.
   *
   * @param subg displayed subgraph where action occurred
   * @param elem subgraph element in which action occurred
   * @param pt the point where the action occurred (graph coordinates)
   * @param modifiers mouse modifiers in effect
   * @param clickCount count of mouse clicks that triggered this action
   * @param panel specific panel where the action occurred
   */
    public void grappaClicked(Subgraph subg, Element elem, GrappaPoint pt, int modifiers, int clickCount, GrappaPanel panel) {
	if((modifiers&InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK) {
	    if(clickCount == 1) {
		// looks like Java has a single click occur on the way to a
		// multiple click, so this code always executes (which is
		// not necessarily a bad thing)
		if(subg.getGraph().isSelectable()) {
		    if(modifiers == InputEvent.BUTTON1_MASK) {
		        currentSelection.clear();
		        currentSelection.add(elem);
		        currentSelection.yell();
		    }
		    else if(modifiers == (InputEvent.BUTTON1_MASK|InputEvent.CTRL_MASK)) {
		        if(elem!=null) {
		            currentSelection.toggle(elem);
		            currentSelection.yell();
		        }
            }
		    subg.getGraph().repaint();
		}
	    } else {
		// multiple clicks
		// this code executes for each click beyond the first
		//System.err.println("clickCount="+clickCount);
	    }
	}
    }

  /**
   * The method called when a mouse press occurs on a displayed subgraph.
   *
   * @param subg displayed subgraph where action occurred
   * @param elem subgraph element in which action occurred
   * @param pt the point where the action occurred (graph coordinates)
   * @param modifiers mouse modifiers in effect
   * @param panel specific panel where the action occurred
   */
    public void grappaPressed(Subgraph subg, Element elem, GrappaPoint pt, int modifiers, GrappaPanel panel) {
	if((modifiers&(InputEvent.BUTTON2_MASK|InputEvent.BUTTON3_MASK)) != 0 && (modifiers&(InputEvent.BUTTON2_MASK|InputEvent.BUTTON3_MASK)) == modifiers) {
	    // pop-up menu if button2 or button3
	    javax.swing.JPopupMenu popup = new javax.swing.JPopupMenu();
	    javax.swing.JMenuItem item = null;
	    if(panel.getToolTipText() == null) {
		popup.add(item = new javax.swing.JMenuItem("ToolTips On"));
	    } else {
		popup.add(item = new javax.swing.JMenuItem("ToolTips Off"));
	    }
	    item.addActionListener(this);
	    popup.addSeparator();
	    if(currentSelection != null) {
		popup.add(item = new javax.swing.JMenuItem("Clear Selection"));
		item.addActionListener(this);
		popup.addSeparator();
		popup.add(item = new javax.swing.JMenuItem("Enclose Selected Items in a new Subgraph"));
		item.addActionListener(this);
		popup.addSeparator();
		popup.add(item = new javax.swing.JMenuItem("Preview Deletion"));
		item.addActionListener(this);
		popup.add(item = new javax.swing.JMenuItem("Cancel Preview"));
		item.addActionListener(this);
		popup.add(item = new javax.swing.JMenuItem("Perform Deletion"));
		item.addActionListener(this);
		popup.addSeparator();
	    }
	    if(panel.hasOutline()) {
		popup.add(item = new javax.swing.JMenuItem("Zoom to Sweep"));
		item.addActionListener(this);
	    }
	    popup.add(item = new javax.swing.JMenuItem("Zoom In"));
	    item.addActionListener(this);
	    popup.add(item = new javax.swing.JMenuItem("Zoom Out"));
	    item.addActionListener(this);
	    popup.add(item = new javax.swing.JMenuItem("Reset Zoom"));
	    item.addActionListener(this);
	    if(subg.hasEmptySubgraphs()) {
		popup.addSeparator();
		popup.add(item = new javax.swing.JMenuItem("Remove Empty Subgraphs"));
		item.addActionListener(this);
	    }

	    java.awt.geom.Point2D mpt = panel.getTransform().transform(pt,null);
	    popup.show(panel,(int)mpt.getX(),(int)mpt.getY());
	}
    }

  /**
   * The method called when a mouse release occurs on a displayed subgraph.
   *
   * @param subg displayed subgraph where action occurred
   * @param elem subgraph element in which action occurred
   * @param pt the point where the action occurred (graph coordinates)
   * @param modifiers mouse modifiers in effect
   * @param pressedElem subgraph element in which the most recent mouse press occurred
   * @param pressedPt the point where the most recent mouse press occurred (graph coordinates)
   * @param pressedModifiers mouse modifiers in effect when the most recent mouse press occurred
   * @param outline enclosing box specification from the previous drag position (for XOR reset purposes)
   * @param panel specific panel where the action occurred
   */
    public void grappaReleased(Subgraph subg, Element elem, GrappaPoint pt, int modifiers, Element pressedElem, GrappaPoint pressedPt, int pressedModifiers, GrappaBox outline, GrappaPanel panel) {
	if(modifiers == InputEvent.BUTTON1_MASK && subg.getGraph().isSelectable()) {
	    if(outline != null) {
		Vector elems = GrappaSupport.findContainedElements(subg, outline);
	    currentSelection.clear();
	    currentSelection.add(elems);
	    currentSelection.yell();
		subg.getGraph().paintImmediately();
	    }
	} else if(modifiers == (InputEvent.BUTTON1_MASK|InputEvent.CTRL_MASK) && subg.getGraph().isSelectable()) {
	    if(outline != null) {
		Vector elems = GrappaSupport.findContainedElements(subg, outline);
		if(elems != null) {
		    currentSelection.toggle(elems);
		    currentSelection.yell();
		    subg.getGraph().repaint();
		} else {
		    Graphics2D g2d = (Graphics2D)(panel.getGraphics());
		    AffineTransform orig = g2d.getTransform();
		    g2d.setTransform(panel.getTransform());
		    g2d.setXORMode(Color.darkGray);
		    g2d.draw(outline);
		    g2d.setPaintMode();
		    g2d.setTransform(orig);
		}
	    }
	} else if(modifiers == (InputEvent.BUTTON1_MASK|InputEvent.SHIFT_MASK) && subg.getGraph().isEditable()) {
	    //if(elem != null && pressedElem != null) { // gw: ?
		if(pressedModifiers == modifiers) {
		    if(outline == null) {
			if(pressedElem == elem && pt.distance(pressedPt) < 5) {
			    // [should we only allow elem.isSubgraph()?]
			    // create node
			    Attribute[] attrs = null;
			    Attribute attr = subg.getNodeAttribute(LABEL_ATTR);
			    if(attr == null || attr.getValue().equals("\\N")) {
				attrs = new Attribute[] { new Attribute(Grappa.NODE, POS_ATTR, pt), new Attribute(Grappa.NODE, LABEL_ATTR, "Node" + subg.getGraph().getId(Grappa.NODE)) };
			    } else {
				attrs = new Attribute[] { new Attribute(Grappa.NODE, POS_ATTR, pt) };
			    }
                panel.requestSink.insertNode(subg,attrs);
			} else if(pressedElem != elem && pressedElem.isNode() && elem.isNode()) {
			    // create edge
			    Attribute[] attrs = new Attribute[] { new Attribute(Grappa.EDGE, POS_ATTR, new GrappaLine(new GrappaPoint[] { ((Node)pressedElem).getCenterPoint(), ((Node)elem).getCenterPoint() }, subg.getGraph().isDirected()?GrappaLine.TAIL_ARROW_EDGE:GrappaLine.NONE_ARROW_EDGE) ) };
                panel.requestSink.insertEdge(subg,(Node)pressedElem,(Node)elem,attrs);
			}
		    }
		}
	    //}
	}
    }

  /**
   * The method called when a mouse drag occurs on a displayed subgraph.
   *
   * @param subg displayed subgraph where action occurred
   * @param currentPt the current drag point
   * @param currentModifiers the current drag mouse modifiers
   * @param pressedElem subgraph element in which the most recent mouse press occurred
   * @param pressedPt the point where the most recent mouse press occurred (graph coordinates)
   * @param pressedModifiers mouse modifiers in effect when the most recent mouse press occurred
   * @param outline enclosing box specification from the previous drag position (for XOR reset purposes)
   * @param panel specific panel where the action occurred
   */
    public void grappaDragged(Subgraph subg, GrappaPoint currentPt, int currentModifiers, Element pressedElem, GrappaPoint pressedPt, int pressedModifiers, GrappaBox outline, GrappaPanel panel) {
	if((currentModifiers&InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK) {
	    if(currentModifiers == InputEvent.BUTTON1_MASK || currentModifiers == (InputEvent.BUTTON1_MASK|InputEvent.CTRL_MASK)) {
		Graphics2D g2d = (Graphics2D)(panel.getGraphics());
		AffineTransform orig = g2d.getTransform();
		g2d.setTransform(panel.getTransform());
		g2d.setXORMode(Color.darkGray);
		if(outline != null) {
		    g2d.draw(outline);
		}
		GrappaBox box = GrappaSupport.boxFromCorners(pressedPt.x, pressedPt.y, currentPt.x, currentPt.y);
		g2d.draw(box);
		g2d.setPaintMode();
		g2d.setTransform(orig);
	    }
	}
    }

  /**
   * The method called when a element tooltip is needed.
   *
   * @param subg displayed subgraph where action occurred
   * @param elem subgraph element in which action occurred
   * @param pt the point where the action occurred (graph coordinates)
   * @param modifiers mouse modifiers in effect
   * @param panel specific panel where the action occurred
   *
   * @return the tip to be displayed or null; in this implementation,
   * if the mouse is in a graph element that
   * has its <I>tip</I> attribute defined, then that text is returned.
   * If that attribute is not set, the element name is returned.
   * If the mouse is outside the graph bounds, then the text supplied
   * to the graph setToolTipText method is supplied.
   */
    public String grappaTip(Subgraph subg, Element elem, GrappaPoint pt, int modifiers, GrappaPanel panel) {
	String tip = null;

	if(elem == null) {
	    if((tip = panel.getToolTipText()) == null) {
		if((tip = subg.getGraph().getToolTipText()) == null) {
		    tip = Grappa.getToolTipText();
		}
	    }
	} else {
	    switch(elem.getType()) {
	    case SUBGRAPH:
		Subgraph sg = (Subgraph)elem;
		if((tip = (String)sg.getAttributeValue(TIP_ATTR)) == null) {
		    if(subg.getShowSubgraphLabels()) {
			tip = sg.getName();
		    } else {
			if((tip = (String)sg.getAttributeValue(LABEL_ATTR)) == null) {
			    tip = sg.getName();
			}
		    }
		    tip = "Subgraph: " + tip;
		}
		break;
	    case EDGE:
		Edge edge = (Edge)elem;
		if((tip = (String)edge.getAttributeValue(TIP_ATTR)) == null) {
		    if(subg.getShowEdgeLabels()) {
			tip = edge.toString();
		    } else {
			if((tip = (String)edge.getAttributeValue(LABEL_ATTR)) == null) {
			    tip = edge.toString();
			}
		    }
		    tip = "Edge: " + tip;
		}
		break;
	    case NODE:
		Node node = (Node)elem;
		if((tip = (String)node.getAttributeValue(TIP_ATTR)) == null) {
		    if(subg.getShowNodeLabels()) {
			tip = node.getName();
		    } else {
			if((tip = (String)node.getAttributeValue(LABEL_ATTR)) == null || tip.equals("\\N")) {
			    tip = node.getName();
			}
		    }
		    tip = "Node: " + tip;
		}
		break;
	    default:
		throw new RuntimeException("unexpected type (" + elem.getType() + ")");
	    }
	}

	return(tip);
    }

    ///////////////////////////////////////////////////////////////////////////
    //
    // ActionListener
    //
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Invoked when an action occurs.
     *
     * @param aev the action event trigger.
     */
    public void actionPerformed(ActionEvent aev) {
	Object src = aev.getSource();
	if(src instanceof javax.swing.JMenuItem) {
	    Object parent = ((javax.swing.JMenuItem)src).getParent();
	    if(parent instanceof javax.swing.JPopupMenu) {
		Object invoker = ((javax.swing.JPopupMenu)(((javax.swing.JMenuItem)src).getParent())).getInvoker();
		if(invoker instanceof GrappaPanel) {
		    GrappaPanel gp = (GrappaPanel)invoker;
		    Subgraph subg = gp.getSubgraph();
		    String text = ((javax.swing.JMenuItem)src).getText();
		    if(text.startsWith("Cancel")) {
		        currentSelection.cancelPreview();
			    subg.getGraph().repaint();
		    } else if(text.startsWith("Clear")) {
		        currentSelection.clear();
		        currentSelection.yell();
    			subg.getGraph().repaint();
		    } else if(text.startsWith("Enclose")) {
		        if((currentSelection.v.size() == 1 && currentSelection.v.firstElement()==subg))
		            return;
			    Subgraph newsubg = new Subgraph(subg);
			    Vector vec = currentSelection.v;
			    for(int i = 0; i < vec.size(); i++) {
    				if(((Element)(vec.elementAt(i))) == subg) continue;
				    ((Element)(vec.elementAt(i))).setSubgraph(newsubg);
			    }
			    currentSelection.clear();
			    currentSelection.yell();
			    subg.getGraph().repaint();
		    } else if(text.startsWith("Preview")) {
		        currentSelection.previewDelete();
    			subg.getGraph().repaint();
		    } else if(text.startsWith("Perform")) {
                        gp.requestSink.lock(true);
                        Vector vec = currentSelection.v;
			for(int i = 0; i < vec.size(); i++)
                            if(vec.elementAt(i) instanceof Node)
                                gp.requestSink.deleteNode(subg,(Node)vec.elementAt(i));
                            else if(vec.elementAt(i) instanceof Edge)
                                gp.requestSink.deleteEdge(subg,(Edge)vec.elementAt(i));
                        gp.requestSink.lock(false);
                        currentSelection.clear();
    			currentSelection.yell();
		    } else if(text.startsWith("Remove")) {
			subg.removeEmptySubgraphs();
		    } else if(text.startsWith("Reset")) {
			gp.resetZoom();
			gp.clearOutline();
		    } else if(text.startsWith("ToolTips")) {
			if(text.indexOf("Off") > 0) {
			    gp.setToolTipText(null);
			} else {
			    String tip = subg.getGraph().getToolTipText();
			    if(tip == null) {
				tip = Grappa.getToolTipText();
			    }
			    gp.setToolTipText(tip);
			}
		    } else if(text.startsWith("Zoom In")) {
			gp.multiplyScaleFactor(1.25);
			gp.clearOutline();
		    } else if(text.startsWith("Zoom Out")) {
			gp.multiplyScaleFactor(0.8);
			gp.clearOutline();
		    } else if(text.startsWith("Zoom to")) {
			gp.zoomToOutline();
			gp.clearOutline();
		    }
		}
	    }
    }
    }
    ///////////////////////////////////////////////////////////////////////////
    /*
    private void drillDown(Subgraph subg, Vector elems, int mode, int setting) {
	Object obj = null;
	for(int i = 0; i < elems.size(); i++) {
	    obj = elems.elementAt(i);
	    if(obj instanceof Vector) {
		drillDown(subg, (Vector)obj, mode, setting);
	    } else {
		GrappaSupport.setHighlight(((Element)obj), mode, setting);
		switch(setting) {
		case HIGHLIGHT_TOGGLE:
		    if((((Element)obj).highlight&mode) == mode) {
			if(currentSelection == null) {
			    currentSelection = obj;
			} else if(currentSelection instanceof Element) {
			    Object crnt = currentSelection;
			    currentSelection = new Vector();
			    ((Vector)(currentSelection)).add(crnt);
			    ((Vector)(currentSelection)).add(obj);
			} else {
			    ((Vector)(currentSelection)).add(obj);
			}
		    } else {
			if(currentSelection == obj) {
			    currentSelection = null;
			} else if(currentSelection instanceof Vector) {
			    ((Vector)(currentSelection)).remove(obj);
			}
		    }
		    break;
		case HIGHLIGHT_ON:
		    if(currentSelection == null) {
			currentSelection = obj;
		    } else if(currentSelection instanceof Element) {
			Object crnt = currentSelection;
			currentSelection = new Vector();
			((Vector)(currentSelection)).add(crnt);
			((Vector)(currentSelection)).add(obj);
		    } else {
			((Vector)(currentSelection)).add(obj);
		    }
		    break;
		case HIGHLIGHT_OFF:
		    if(currentSelection != null) {
			if(currentSelection == obj) {
			    currentSelection = null;
			} else if(currentSelection instanceof Vector) {
			    ((Vector)(currentSelection)).remove(obj);
			}
		    }
		    break;
		}
	    }
	}
    }
*/
}
