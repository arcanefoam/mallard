/**********************************************************
*      This software is part of the graphviz toolset      *
*                http://www.graphviz.org/                 *
*                                                         *
*            Copyright (c) 1994-2005 AT&T Corp.           *
*                and is licensed under the                *
*            Common Public License, Version 1.0           *
*                      by AT&T Corp.                      *
*                                                         *
*        Information and Software Systems Research        *
*              AT&T Research, Florham Park NJ             *
*                                                         *
*               This version available from               *
*                   http://dynagraph.org                  *
**********************************************************/


package uk.ac.york.att.grappa;
import uk.ac.york.att.grappa.*;

public class GrappaSimpleFulfill implements GrappaRequests {
  public void lock(boolean whether) {
  }
  public void segue(Subgraph subg, Subgraph newg) {
      subg = newg; // ? only called by DGDemo
  }
  public void insertNode(Subgraph subg, Attribute[] attrs) {
    Element el = subg.createElement(Grappa.NODE,null,attrs);
    if(el != null) {
        el.buildShape();
        subg.getGraph().repaint();
    }
    subg.getGraph().repaint();
  }
  public void insertEdge(Subgraph subg, Node tail, Node head, Attribute[] attrs) {
    Object[] info = new Object[] { head, null, tail };
    Element el = subg.createElement(Grappa.EDGE,info,attrs);
    if(el != null) {
        el.buildShape();
        subg.getGraph().repaint();
    }
  }
  public void modifyNode(Subgraph subg, Node node, Attribute[] attrs) {
    for(int i = 0; i < attrs.length; ++i)
      node.setAttribute(attrs[i]);
  }
  public void modifyEdge(Subgraph subg, Edge edge, Attribute[] attrs) {
    for(int i = 0; i < attrs.length; ++i)
      edge.setAttribute(attrs[i]);
  }
  public void deleteNode(Subgraph subg, Node node) {
    node.delete();
  }
  public void deleteEdge(Subgraph subg, Edge edge) {
    edge.delete();
  }

};
