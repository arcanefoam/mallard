/**********************************************************
*      This software is part of the graphviz toolset      *
*                http://www.graphviz.org/                 *
*                                                         *
*            Copyright (c) 1994-2005 AT&T Corp.           *
*                and is licensed under the                *
*            Common Public License, Version 1.0           *
*                      by AT&T Corp.                      *
*                                                         *
*        Information and Software Systems Research        *
*              AT&T Research, Florham Park NJ             *
*                                                         *
*               This version available from               *
*                   http://dynagraph.org                  *
**********************************************************/


package uk.ac.york.att.grappa;

import java.util.*;

/**
 * This class maintains a selection and fires almost-minimal events for
 * the changes that happen.
*/

public class GrappaSelection
    implements GrappaConstants
{
    public interface Events
    {
        // fired by GrappaSelection on particular changes
        public void added(Element ele);
        public void removed(Element ele);

        // an occasional event, fired by selection's owner (GrappaAdapter)
        // (ugly but more effective than selection firing its own)
        public void changed();
    }

    public Vector v = new Vector();
    public Events listener = null; // a more general listener list is not necessary yet

    public void add(Element elem) {
        if(elem!=null) {
            if((elem.highlight&SELECTION_MASK) != 0)
                return;
            elem.highlight |= SELECTION_MASK;
            v.add(elem);
            if(listener!=null)
                listener.added(elem);
        }
    }
    public void add(Vector v2) {
        if(v2!=null)
            for(int i = 0; i<v2.size(); ++i)
                add((Element)v2.elementAt(i));
    }
    public void remove(Element elem) {
        if(elem!=null) {
            if((elem.highlight&SELECTION_MASK) == 0)
                return;
		    elem.highlight &= ~SELECTION_MASK;
            int i = v.indexOf(elem);
            if(i>=0)
			    v.removeElementAt(i);
		    else
		        throw new InternalError("currentSelection improperly maintained");
            if(listener!=null)
                listener.removed(elem);
        }
    }
    public void remove(Vector v2) {
        for(int i = 0; i<v2.size(); ++i)
            remove((Element)v2.elementAt(i));
    }
    public void toggle(Element elem) {
        if((elem.highlight&SELECTION_MASK) != 0)
            remove(elem);
        else
            add(elem);
    }
    public void toggle(Vector v2) {
        if(v2!=null)
            for(int i = 0; i<v2.size(); ++i)
                toggle((Element)v2.elementAt(i));
    }
    public void clear() {
        while(v.size()>0)
            remove((Element)v.elementAt(0));
    }
	public void previewDelete() {
		for(int i = 0; i < v.size(); i++)
		    GrappaSupport.setHighlight((Element)v.elementAt(i), DELETION_MASK, HIGHLIGHT_ON);
	}
	public void cancelPreview() {
		for(int i = 0; i < v.size(); i++)
		    GrappaSupport.setHighlight((Element)v.elementAt(i), DELETION_MASK, HIGHLIGHT_OFF);
	}
	public void yell() {
	    if(listener!=null)
	        listener.changed();
	}
}
/*
        v.add(elem);
		    if(modifiers == InputEvent.BUTTON1_MASK) {
			// select element
			if(elem == null) {
			    if(subg.currentSelection != null) {
				if(subg.currentSelection instanceof Element) {
				    ((Element)(subg.currentSelection)).highlight &= ~HIGHLIGHT_MASK;
				} else {
				    Vector vec = ((Vector)(subg.currentSelection));
				    for(int i = 0; i < vec.size(); i++) {
					((Element)(vec.elementAt(i))).highlight &= ~HIGHLIGHT_MASK;
				    }
				}
				subg.currentSelection = null;
				subg.getGraph().repaint();
			    }
			} else {
			    if(subg.currentSelection != null) {
				if(subg.currentSelection == elem) return;
				if(subg.currentSelection instanceof Element) {
				    ((Element)(subg.currentSelection)).highlight &= ~HIGHLIGHT_MASK;
				} else {
				    Vector vec = ((Vector)(subg.currentSelection));
				    for(int i = 0; i < vec.size(); i++) {
					((Element)(vec.elementAt(i))).highlight &= ~HIGHLIGHT_MASK;
				    }
				}
				subg.currentSelection = null;
			    }
			    elem.highlight |= SELECTION_MASK;
			    subg.currentSelection = elem;
			    subg.getGraph().repaint();
			}
		    } else if(modifiers == (InputEvent.BUTTON1_MASK|InputEvent.CTRL_MASK)) {
			// adjust selection
			if(elem != null) {
			    if((elem.highlight&SELECTION_MASK) == SELECTION_MASK) {
				// unselect element
				elem.highlight &= ~SELECTION_MASK;
				if(subg.currentSelection == null) {
				// something got messed up somewhere
				    throw new InternalError("currentSelection improperly maintained");
				} else if(subg.currentSelection instanceof Element) {
				    if(((Element)(subg.currentSelection)) != elem) {
					// something got messed up somewhere
					throw new InternalError("currentSelection improperly maintained");
				    }
				    subg.currentSelection = null;
				} else {
				    Vector vec = ((Vector)(subg.currentSelection));
				    boolean problem = true;
				    for(int i = 0; i < vec.size(); i++) {
					if(((Element)(vec.elementAt(i))) == elem) {
					    vec.removeElementAt(i);
					    problem = false;
					    break;
					}
				    }
				    if(problem) {
					// something got messed up somewhere
					throw new InternalError("currentSelection improperly maintained");
				    }
				}
			    } else {
				// select element
				elem.highlight |= SELECTION_MASK;
				if(subg.currentSelection == null) {
				    subg.currentSelection = elem;
				} else if(subg.currentSelection instanceof Element) {
				    Object obj = subg.currentSelection;
				    subg.currentSelection = new Vector();
				    ((Vector)(subg.currentSelection)).add(obj);
				    ((Vector)(subg.currentSelection)).add(elem);
				} else {
				    ((Vector)(subg.currentSelection)).add(elem);
				}
			    }
			    subg.getGraph().repaint();
			}
		    }
*/