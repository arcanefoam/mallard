package uk.ac.york.qvtd.benchmark.matcher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import com.google.common.base.Function;

public class XMLIDFunction implements Function<EObject, String> {

    @Override
    public String apply(EObject input) {
        String id = null;
        // Fragile - To avoid code generation we use names!
        EClass eClass = input.eClass();
        id = getNameValueId(input, eClass);
        switch (eClass.getName()) {
        case ("Root"):
            id = "R_" + id;
            break;
        case ("Text"):
            id = "T_" + id;
            break;
        case ("Attribute"):
            id = "A_" + id;
            break;
        case ("Element"):
            id = "E_" + id;
            break;
        }
        return id;
    }

    private String getNameValueId(EObject eObject, EClass eClass) {
        EStructuralFeature nameSF = eClass.getEStructuralFeature("name");
        Object name = eObject.eGet(nameSF);
        String id = "";
        if (name != null)
            id = (String) name;
        EStructuralFeature valueSF = eClass.getEStructuralFeature("value");
        Object value = eObject.eGet(valueSF);
        if (value != null)
            id = id + ":" + (String) value;
        if (eClass.getName().equals("Element")) {
            EStructuralFeature childSF = eClass.getEStructuralFeature("children");
            EList<EObject> children = (EList<EObject>) eObject.eGet(childSF);
            String sep = "";
            List<String> childNames = new ArrayList<String>();
            for (EObject c : children) {
                childNames.add(getNameValueId(c, c.eClass()));
            }
            Collections.sort(childNames);
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            for (String cn : childNames) {
                sb.append(cn);
                sb.append(sep);
                sep = ",";
            }
            sb.append("}");
            id += sb.toString();
        }
        return id;
    }

}
