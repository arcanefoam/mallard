/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.benchmark;

import org.eclipse.emf.compare.diff.DefaultDiffEngine;
import org.eclipse.emf.compare.diff.FeatureFilter;
import org.eclipse.emf.compare.diff.IDiffProcessor;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * The Class MallardDiffEngine. Provides alternative labels for objects
 */
public class MallardDiffEngine extends DefaultDiffEngine {

    /**
     * Instantiates a new mallard diff engine.
     *
     * @param processor the processor
     */
    public MallardDiffEngine(IDiffProcessor processor) {
        super(processor);
    }

    /* (non-Javadoc)
     * @see org.eclipse.emf.compare.diff.DefaultDiffEngine#createFeatureFilter()
     */
    @Override
    protected FeatureFilter createFeatureFilter() {
        return new FeatureFilter() {

            @Override
            public boolean checkForOrderingChanges(EStructuralFeature feature) {
                return false;
            }
        };
    }

    public static String getLabel(EObject object) {
        String ret = null;
        if (object == null) {
            ret = "<null>"; //$NON-NLS-1$
        } else {
            EObject eObject = object;
            EClass eClass = eObject.eClass();
            ret = eClass.getName(); //$NON-NLS-1$

            EStructuralFeature feature = getLabelFeature(eClass);
            if (feature != null) {
                Object value = eObject.eGet(feature);
                if (value != null) {
                    ret += " " + value.toString(); //$NON-NLS-1$
                }
            }
        }
        return ret;
    }

    private static EStructuralFeature getLabelFeature(EClass eClass) {
        if (eClass == EcorePackage.Literals.ENAMED_ELEMENT) {
            return EcorePackage.Literals.ENAMED_ELEMENT__NAME;
        }

        EAttribute result = null;
        for (EAttribute eAttribute : eClass.getEAllAttributes()) {
            if (!eAttribute.isMany() && eAttribute.getEType().getInstanceClass() != FeatureMap.Entry.class) {
                if ("name".equalsIgnoreCase(eAttribute.getName())) { //$NON-NLS-1$
                    result = eAttribute;
                    break;
                } else if (result == null) {
                    result = eAttribute;
                } else if (eAttribute.getEAttributeType().getInstanceClass() == String.class
                        && result.getEAttributeType().getInstanceClass() != String.class) {
                    result = eAttribute;
                }
            }
        }
        return result;
    }

}

