package uk.ac.york.qvtd.benchmark.matcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import com.google.common.base.Function;

public class DocBookIDFunction implements Function<EObject, String> {

    private HashMap<EObject, String> idCache = new HashMap<EObject,String>();

    @Override
    public String apply(EObject input) {
        String id = idCache.getOrDefault(input, null);
        if (id == null) {
            // Fragile - To avoid code generation we use names!
            EClass eClass = input.eClass();
            switch (eClass.getName()) {
            case ("DocBook"):
                EStructuralFeature booksSF = eClass.getEStructuralFeature("books");
                id = getChildIds(input, booksSF);
                break;
            case ("Book"):
                EStructuralFeature articlesSF = eClass.getEStructuralFeature("articles");
                id = getChildIds(input, articlesSF);
                break;
            case ("Article"):
                EStructuralFeature aTitleSF = eClass.getEStructuralFeature("title");
                id = (String) input.eGet(aTitleSF);
                EStructuralFeature aSectionsSF = eClass.getEStructuralFeature("sections_1");
                id += getChildIds(input, aSectionsSF);
                break;
            case ("Sect1"):
                EStructuralFeature s1TitleSF = eClass.getEStructuralFeature("title");
                id = (String) input.eGet(s1TitleSF);
                EStructuralFeature s1parasSF = eClass.getEStructuralFeature("paras");
                id += getChildIds(input, s1parasSF);
                EStructuralFeature s1sectsSF = eClass.getEStructuralFeature("sections_2");
                id += getChildIds(input, s1sectsSF);
                break;
            case ("Sect2"):
                EStructuralFeature s2TitleSF = eClass.getEStructuralFeature("title");
                id = (String) input.eGet(s2TitleSF);
                EStructuralFeature s2parasSF = eClass.getEStructuralFeature("paras");
                id += getChildIds(input, s2parasSF);
                break;
            case ("Para"):
                EStructuralFeature contentSF = eClass.getEStructuralFeature("content");
                String info = (String) input.eGet(contentSF);
                String[] details = info.split(":");
                List<String> detailsSort = new ArrayList<String>();
                for (String d : details) {
                    if (d.contains(";")) { // Is author list
                        List<String> authors = Arrays.asList(d.split(";")).stream()
                                .filter(a -> !a.equals(" "))
                                .collect(Collectors.toList());
                        Collections.sort(authors);
                        String author = authors.stream().collect(Collectors.joining(";"));
                        detailsSort.add(author);
                    }
                    else {
                        detailsSort.add(d);
                    }
                }
                Collections.sort(detailsSort);
                id = detailsSort.stream().collect(Collectors.joining(";"));
            }
        }
        return id;
    }


    private String getChildIds(EObject input, EStructuralFeature sf) {
        EList<EObject> children = (EList<EObject>) input.eGet(sf);
        List<String> childNames = new ArrayList<String>();
        for (EObject c : children) {
            childNames.add(apply(c));
        }
        Collections.sort(childNames);
        String sep = "";
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (String cn : childNames) {
            sb.append(cn);
            sb.append(sep);
            sep = ",";
        }
        sb.append("}");
        return sb.toString();
    }
}
