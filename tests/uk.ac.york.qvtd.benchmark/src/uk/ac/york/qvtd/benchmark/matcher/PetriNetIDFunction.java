package uk.ac.york.qvtd.benchmark.matcher;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import com.google.common.base.Function;

public class PetriNetIDFunction implements Function<EObject, String> {

    @Override
    public String apply(EObject input) {
        String id = null;
        // Fragile - To avoid code generation we use names!
        EClass eClass = input.eClass();
        EStructuralFeature nameSF;
        switch (eClass.getName()) {
        case ("PetriNet"):
            nameSF = eClass.getEStructuralFeature("name");
            id = "PN_" + (String) input.eGet(nameSF);
            break;
        case ("Place"): 	// Id is name of inc + out
            EStructuralFeature incomingSF = eClass.getEStructuralFeature("incoming");
            EStructuralFeature outgoingSF = eClass.getEStructuralFeature("outgoing");
            List<EObject> arcs = new ArrayList<EObject> ();
            arcs.addAll((EList<EObject>) input.eGet(incomingSF));
            arcs.addAll((EList<EObject>) input.eGet(outgoingSF));
            ArrayList<String> names = new ArrayList<String>();
            try {
                EClass arcEClass;
                for (EObject arc : arcs) {
                    arcEClass = arc.eClass();
                    String arcId = getArcId(arc, arcEClass);
                    names.add(arcId);
                }
                id = names.stream()
                    .sorted()
                    .collect(Collectors.joining("-"));
                //id = "P_" + id;
            } catch(Exception ex) {
                // No id. Although in theory there should not unconnected elements
            }
            break;
        case ("Transition"):
            EStructuralFeature tnameSF = eClass.getEStructuralFeature("name");
            id = "T_" + (String) input.eGet(tnameSF);
            break;
        case ("PlaceToTransArc"):
        case ("TransToPlaceArc"):
            String arcId = getArcId(input, eClass);
            id = "A_" + arcId;
        }
        return id;
    }

    private String getArcId(EObject arc, EClass arcEClass) {

        String sourceName = getSourceName(arc, arcEClass);
        if (sourceName.isEmpty()) {
            return "P." + getTargetName(arc, arcEClass);
        }
        return sourceName + ".P";

    }

    private String getSourceName(EObject arc, EClass arcEClass) {
        EStructuralFeature sourceSF = arcEClass.getEStructuralFeature("source");
        EObject source = (EObject) arc.eGet(sourceSF);
        EClass sourceEClass = source.eClass();
        EStructuralFeature sourceNameSF = sourceEClass.getEStructuralFeature("name");
        return (String) source.eGet(sourceNameSF);
    }

    private String getTargetName(EObject arc, EClass arcEClass) {
        EStructuralFeature targetSF = arcEClass.getEStructuralFeature("target");
        EObject target = (EObject) arc.eGet(targetSF);
        EClass targetEClass = target.eClass();
        EStructuralFeature targetNameSF = targetEClass.getEStructuralFeature("name");
        return (String) target.eGet(targetNameSF);

    }

}
