/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.benchmark;

import uk.ac.york.qvtd.compiler.Configuration;

// TODO: Auto-generated Javadoc
/**
 * The Interface ModelValidation. Specific implementations can provide different model validations, e.g. compare,
 * constraints (OCL/EVL), patterns, etc.
 */
public interface ModelValidation {

    /**
     * Validate the models in the configuration.
     *
     * The model URIs, names and metamodles can be retreived from the configuration
     *
     * @param configuration the configuration
     * @return true, if successful
     */
    boolean validate(Configuration configuration);

    /**
     * Gets the err message.
     *
     * @return the err message
     */
    String getErrMessage();

    /**
     * Sets the err message.
     *
     * @param errMessage the new err message
     */
    void setErrMessage(String errMessage);

}
