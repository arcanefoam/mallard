package uk.ac.york.qvtd.benchmark.matcher;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import com.google.common.base.Function;

public class PathExpIDFunction implements Function<EObject, String> {

    @Override
    public String apply(EObject input) {

        String id = null;
        // Fragile - To avoid code generation we use names!
        EClass eClass = input.eClass();

        switch (eClass.getName()) {
            case ("PathExp"):
                EStructuralFeature nameSF = eClass.getEStructuralFeature("name");
                id = "PE_" + (String) input.eGet(nameSF);
                break;
            case ("State"): 	// Id is name of inc + out
                EStructuralFeature incomingSF = eClass.getEStructuralFeature("incoming");
                EStructuralFeature outgoingSF = eClass.getEStructuralFeature("outgoing");
                List<EObject> trans = new ArrayList<EObject> ();
                trans.addAll((EList<EObject>) input.eGet(incomingSF));
                trans.addAll((EList<EObject>) input.eGet(outgoingSF));
                try {
                    EClass tranEC = trans.get(0).eClass();
                    EStructuralFeature tnSF = tranEC.getEStructuralFeature("name");
                    id = trans.stream()
                        .map(t -> t.eGet(tnSF))
                        .map(t -> t.toString())
                        .sorted()
                        .collect(Collectors.joining("-"));
                    id = "S_" + id;
                } catch(Exception ex) {
                    // No id. Although in theory there should not be states without transitions
                }
                break;
            case ("Transition"):
                EStructuralFeature tnameSF = eClass.getEStructuralFeature("name");
                id = "T_" + (String) input.eGet(tnameSF);
                break;
        }
        return id;
    }

}
