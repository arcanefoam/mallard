package uk.ac.york.qvtd.benchmark;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.compare.AttributeChange;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.Diff;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.ReferenceChange;
import org.eclipse.emf.compare.diff.DiffBuilder;
import org.eclipse.emf.compare.diff.IDiffEngine;
import org.eclipse.emf.compare.diff.IDiffProcessor;
import org.eclipse.emf.compare.internal.spec.EObjectUtil;
import org.eclipse.emf.compare.match.DefaultComparisonFactory;
import org.eclipse.emf.compare.match.DefaultEqualityHelperFactory;
import org.eclipse.emf.compare.match.DefaultMatchEngine;
import org.eclipse.emf.compare.match.IComparisonFactory;
import org.eclipse.emf.compare.match.IEqualityHelperFactory;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.eobject.IEObjectMatcher;
import org.eclipse.emf.compare.match.eobject.IdentifierEObjectMatcher;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.compare.utils.EqualityHelper;
import org.eclipse.emf.compare.utils.UseIdentifiers;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.internal.StandardLibraryImpl;
import org.eclipse.ocl.pivot.internal.library.StandardLibraryContribution;
import org.eclipse.ocl.pivot.internal.resource.ProjectMap;
import org.eclipse.ocl.pivot.internal.resource.StandaloneProjectMap;
import org.eclipse.ocl.pivot.model.OCLstdlib;
import org.eclipse.ocl.pivot.utilities.PivotStandaloneSetup;
import org.eclipse.ocl.xtext.base.services.BaseLinkingService;
import org.eclipse.qvtd.pivot.qvtcore.QVTcorePivotStandaloneSetup;
import org.eclipse.qvtd.pivot.qvtimperative.QVTimperativePivotStandaloneSetup;
import org.eclipse.qvtd.pivot.qvtimperative.utilities.QVTimperative;
import org.eclipse.qvtd.xtext.qvtcore.QVTcoreStandaloneSetup;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.common.cache.LoadingCache;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.filter.ThresholdFilter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy;
import uk.ac.york.qvtd.benchmark.matcher.DocBookIDFunction;
import uk.ac.york.qvtd.benchmark.matcher.PathExpIDFunction;
import uk.ac.york.qvtd.benchmark.matcher.PetriNetIDFunction;
import uk.ac.york.qvtd.benchmark.matcher.XMLIDFunction;
import uk.ac.york.qvtd.compiler.Configuration;
import uk.ac.york.qvtd.compiler.Configuration.Mode;
import uk.ac.york.qvtd.compiler.MallardCompiler;
import uk.ac.york.qvtd.compiler.utilities.MallardMtcUtil;
import uk.ac.york.qvtd.compiler.utilities.QvtMtcExecutionException;
import uk.ac.york.qvtd.pivot.qvtimperative.evaluation.MallardEngine;

// TODO: Auto-generated Javadoc
/**
 * Validate the generated Execution Plans
 * 1. Run the Ballot plan against each of the N small models
 * 2. Run the ACO against against each of the N small models
 * 3. Compare each pair of results
 * 4. Run each of the ACO samples for the 100, 1k, 10k, and 100k models
 * @author Goblin
 *
 */
public class MallardValidation {

    private static final int NUM_ITERATIONS = 25;

    /** The logger. */
    private static Logger logger = (Logger) LoggerFactory.getLogger(MallardValidation.class);

    /** The QVTi environment. */
    private static QVTimperative myQVT;

    private static StandaloneProjectMap projectMap;

    /** The Constant QVTIAS_PARAMS. */
    public static final String QVTIAS_PARAMS = "tests\\uk.ac.york.qvtd.compiler.tests\\resources\\";

    /** The Constant GENMODEL_ROOTPATH. */
    public static final String GENMODEL_ROOTPATH = "tests\\uk.ac.york.qvtd.tests.model.generator\\model\\";

    /** The Constant OUTUPTMODEL_ROOTPATH. */
    public static final String OUTUPTMODEL_ROOTPATH = "tests\\uk.ac.york.qvtd.benchmark\\model\\";

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {

        for (Object[] data : getTestCaseParameters()) {
            String direction = (String) data[0];
            String testName = (String) data[1];
            String folderName = (String) data[2];
            String source = (String) data[3];
            String mallard_root = System.getenv("MALLARD");
            String modelsFolder = mallard_root + GENMODEL_ROOTPATH + folderName;
            String outModelsFolder = mallard_root + OUTUPTMODEL_ROOTPATH + folderName;
            String[] perfModels = {"Huge", "Large", "Normal", "Small", "Tiny"};
            Map<String, Map<String, String>> outputmodels = new HashMap<String, Map<String,String>>();
            // 1. Ballot on small models
            String ballotInfo = mallard_root + QVTIAS_PARAMS + "BALLOT\\" + testName + ".param";
            boolean verbose = false;
            runBallotPlans(direction, testName, source, ballotInfo, modelsFolder, outModelsFolder, perfModels,
                    outputmodels, verbose);
            // 2. Run each ACO against all models
            String acoInfo = mallard_root + QVTIAS_PARAMS + "ACO/" + testName + ".param";
            runAcoPlans(direction, testName, source, acoInfo, modelsFolder, outModelsFolder, perfModels, outputmodels, verbose);
            // 3. Compare the ballot models against the ACO models for each ACO plan
            Function<EObject, String> matcher = (Function<EObject, String>) data[4];
            compareBallotAco(testName, outputmodels, matcher);
            // 4. Run the Ballot and ACO plans on the increasing size models, 25 times each.
//            Map<Path, Integer> ballotTimedoutpaths = new HashMap<Path, Integer>();
//            Map<URI, Map<Path, Integer>> acoTimedoutpaths = new HashMap<URI, Map<Path, Integer>>();
//            for (int i=0; i < NUM_ITERATIONS; i++){
//                System.out.println("Iteration: " + i);
//                List<Path> paths = Files.list(Paths.get(modelsFolder))
//                        .filter(p -> stringContainsItemFromList(p, perfModels))
//                        .collect(Collectors.toList());
//                List<URI> resources = getQvtiUris(ballotInfo);
//                final URI ballotQvtiasURI = resources.get(0);
//                
//                for (Path p : paths) {
//                    if (ballotTimedoutpaths.getOrDefault(p, 0) > 1) {
//                    	System.out.println("Skipping " + p);
//                        continue;
//                    }
//                    Configuration c = createConfiguration(p, ballotQvtiasURI, direction, source, outModelsFolder);
//                    try {
//                        executeBallot(ballotQvtiasURI, c, verbose);
//                    } catch (IllegalStateException ex) {
//                        // Timed out
//                    	int times = ballotTimedoutpaths.getOrDefault(p, 0)+1;
//                    	ballotTimedoutpaths.put(p, times);
//                    	System.out.println("Time out x " + times + " " + p + "@Naive");
//                    }
//                }
//                resources = getQvtiUris(acoInfo);
//                for (URI acoQvtias : resources) {
//                	Map<Path, Integer> asTimedoutpaths;
//                	if (!acoTimedoutpaths.containsKey(acoQvtias)) {
//                		 asTimedoutpaths = new HashMap<Path, Integer>();
//                		 acoTimedoutpaths.put(acoQvtias, asTimedoutpaths);
//                		 
//                	}
//                	else {
//                		asTimedoutpaths = acoTimedoutpaths.get(acoQvtias);
//                	}
//                    paths = Files.list(Paths.get(modelsFolder))
//                            .filter(p -> stringContainsItemFromList(p, perfModels))
//                            .collect(Collectors.toList());
//                    for (Path p : paths) {
//                        if (asTimedoutpaths.getOrDefault(p, 0) > 1) {
//                        	System.out.println("Skipping " + p + "@" + acoQvtias.lastSegment());
//                        	continue;
//                        }
//                        Configuration c = createConfiguration(p, acoQvtias, direction, source, outModelsFolder);
//                        try {
//                            execute(acoQvtias, c, verbose);
//                        } catch (IllegalStateException ex) {
//                        	// Timed out
//                        	int times = asTimedoutpaths.getOrDefault(p, 0)+1;
//                        	asTimedoutpaths.put(p, times);
//                        	System.out.println("Time out x " + times + " " + p + "@" + acoQvtias);
//                        }
//                    }
//                }
//            }
        }
    }

    public static void compareBallotAco(String testName, Map<String, Map<String, String>> outputmodels,
            Function<EObject, String> idMatcher) throws IOException {
        // Need a new logger!
        configureLogger(testName + "_Diff", Level.INFO);
        myQVT = createQVT();
        for (String model_name : outputmodels.keySet()) {
            if (outputmodels.get(model_name).containsKey("ballot")) {
                String input_model = outputmodels.get(model_name).get("ballot");
                for (Entry<String, String> entry : outputmodels.get(model_name).entrySet()) {
                    if (!entry.getKey().equals("ballot")) {
                        String output_model = entry.getValue();
                        URI input_model_uri = URI.createFileURI(input_model);
                        URI output_model_uri = URI.createFileURI(output_model);
                        Resource outputModel = myQVT.getEnvironmentFactory().getResourceSet().getResource(output_model_uri, true);
                        Resource oracleModel = myQVT.getEnvironmentFactory().getResourceSet().getResource(input_model_uri, true);
                        boolean result = getDifferences(outputModel, oracleModel, idMatcher);
                        Object[] preParamArray = {entry.getKey(), model_name, result};
                        logger.info("{}: Match for model {} was {}", preParamArray);
                    }
                }
            }
        }
        myQVT.getEnvironmentFactory().dispose();
        myQVT.dispose();
    }

    /**
     * Run aco plans. Runs all the plans in the ACO sample (testname) on the models in the modelsFolder.
     *
     * @param direction the direction
     * @param testName the test name
     * @param source the source
     * @param modelsFolder the models folder
     * @param outModelsFolder the out models folder
     * @param skipModels the skip models
     * @param outputmodels the outputmodels
     * @param verbose TODO
     * @param mallard_root the mallard
     */
    public static void runAcoPlans(String direction, String testName, String source, String acoInfo,
            String modelsFolder, String outModelsFolder, String[] skipModels,
            Map<String, Map<String, String>> outputmodels, boolean verbose) {
        List<URI> resources = getQvtiUris(acoInfo);
        for (URI acoQvtias : resources) {
            try (Stream<Path> stream = Files.list(Paths.get(modelsFolder))) {
                Map<String, String> result = stream.filter(p -> !stringContainsItemFromList(p, skipModels))
                        .map(p -> createConfiguration(p, acoQvtias, direction, source, outModelsFolder))
                        .collect(Collectors.toMap(c ->getInputModelName(c), c ->execute(acoQvtias, c, verbose)));
                for (Entry<String, String> entry : result.entrySet()) {
                    Map<String, String> out = outputmodels.get(entry.getKey());
                    if (out == null) {
                        out = new HashMap<>();
                        outputmodels.put(entry.getKey(), out);
                    }
                    String qvtias_id = getQvtiasId(acoQvtias);
                    out.put(qvtias_id, entry.getValue());
                }
            } catch (IOException e) {
                logger.error("Error opening param file.", e);
            }
        }
    }

    /**
     * Run ballot plans. Runs the ballot plan on the models on the modelsFolder.
     *
     * @param direction the direction
     * @param testName the test name
     * @param source the source
     * @param modelsFolder the models folder
     * @param outModelsFolder the out models folder
     * @param bigModels the big models
     * @param outputmodels the outputmodels
     * @param verbose TODO
     * @param mallard the mallard
     */
    public static void runBallotPlans(String direction, String testName, String source, String ballotInfo,
            String modelsFolder, String outModelsFolder, String[] bigModels,
            Map<String, Map<String, String>> outputmodels, boolean verbose) {
        List<URI> resources = getQvtiUris(ballotInfo);
        final URI ballotQvtiasURI = resources.get(0);
        try (Stream<Path> stream = Files.list(Paths.get(modelsFolder))) {
                 // Collect execution results for each model
                Map<String, String> result = stream.filter(p -> !stringContainsItemFromList(p, bigModels))
                        .map(p -> createConfiguration(p, ballotQvtiasURI, direction, source, outModelsFolder))
                        .collect(Collectors.toMap(c ->getInputModelName(c), c ->executeBallot(ballotQvtiasURI, c, verbose)));
                for (Entry<String, String> entry : result.entrySet()) {
                    Map<String, String> out = outputmodels.get(entry.getKey());
                    if (out == null) {
                        out = new HashMap<>();
                        outputmodels.put(entry.getKey(), out);
                    }
                    out.put("ballot", entry.getValue()); // FIXME The ballot compiled named might change
                }
        } catch (IOException e) {
            logger.error("Error opening param file.", e);
        }
    }

    /**
     * Compare the generated model against the oracle.
     *
     * @param outputModel the output model
     * @param oracleModel the oracle model
     * @param idMatcher
     * @return the comparison
     */
    private static Comparison compare(Resource outputModel, Resource oracleModel,
            Function<EObject, String> idMatcher) {

        // Get metamodel?
        EPackage ep = outputModel.getContents().get(0).eClass().getEPackage();


        // Configure EMF Compare
        IEObjectMatcher fallBackMatcher = DefaultMatchEngine.createDefaultEObjectMatcher(UseIdentifiers.WHEN_AVAILABLE);
        IEObjectMatcher customIDMatcher =
                idMatcher == null ? fallBackMatcher : new IdentifierEObjectMatcher(fallBackMatcher, idMatcher);

        IEqualityHelperFactory helperFactory = new DefaultEqualityHelperFactory() {
            @Override
            public EqualityHelper createEqualityHelper() {
                final LoadingCache<EObject, URI> cache = EqualityHelper.createDefaultCache(getCacheBuilder());
                return new uk.ac.york.qvtd.pivot.qvtimperative.utilities.MallardEqualityHelper(4, cache);
            }
        };
        //IComparisonFactory comparisonFactory = new DefaultComparisonFactory(new DefaultEqualityHelperFactory());
        IComparisonFactory comparisonFactory = new DefaultComparisonFactory(helperFactory);
        IDiffProcessor diffProcessor = new DiffBuilder();
        IDiffEngine diffEngine = new MallardDiffEngine(diffProcessor);
        IMatchEngine.Factory matchEngineFactory = new MatchEngineFactoryImpl(customIDMatcher, comparisonFactory);
        matchEngineFactory.setRanking(20);
        IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
        matchEngineRegistry.add(matchEngineFactory);
        EMFCompare comparator = EMFCompare.builder().setDiffEngine(diffEngine)
                .setMatchEngineFactoryRegistry(matchEngineRegistry).build();
        // Compare the two models
        IComparisonScope scope = new DefaultComparisonScope(outputModel, oracleModel, null);
        return comparator.compare(scope);
    }

    /**
     * Configure the logger so each test gets a separate log file.
     *
     * @param file_name the file name
     * @param level
     * @throws SecurityException the security exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static void configureLogger(Object file_name, Level level) throws SecurityException, IOException {

        // Remove all handlers (i.e. previous executions)
        Logger rootLogger = (Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        LoggerContext loggerContext = rootLogger.getLoggerContext();
        loggerContext.reset();
        //FIXME This should be a parameter
        rootLogger.setLevel(level);
        // Log to the /MallardTests/mtc folder, use one log per test
        StringBuilder logFileBuilder = new StringBuilder();
        logFileBuilder.append(System.getenv("HOME"));
        logFileBuilder.append("/MallardTests/qvti_execution/");
        logFileBuilder.append(file_name);

        // FileAppender
        RollingFileAppender<ILoggingEvent> fileAppender = new RollingFileAppender<ILoggingEvent>();
        fileAppender.setName("FILE");
        fileAppender.setContext(loggerContext);
        String fileName = logFileBuilder.toString() + ".log";
        fileAppender.setFile(fileName);
        // Fixed window policy
        FixedWindowRollingPolicy rollPolicy = new FixedWindowRollingPolicy();
        rollPolicy.setContext(loggerContext);
        String fnp = logFileBuilder.toString() + ".%i.log";
        rollPolicy.setFileNamePattern(fnp);
        rollPolicy.setParent(fileAppender);
        rollPolicy.setMinIndex(1);
        rollPolicy.setMaxIndex(10);
        rollPolicy.start();

        SizeBasedTriggeringPolicy<ILoggingEvent> trigPolicy = new SizeBasedTriggeringPolicy<ILoggingEvent>();
        trigPolicy.setMaxFileSize("5MB");
        trigPolicy.start();

        PatternLayoutEncoder ple = new PatternLayoutEncoder();
        ple.setContext(loggerContext);
        ple.setPattern("%d{HH:mm:ss.SSS} [%thread] %-5level %logger{5} - %msg%n");
        ple.start();

        // TODO Add a filter to the file so no TRACE are added
//        ThresholdFilter filter = new ThresholdFilter();
//        filter.setLevel(level.levelStr);
//        filter.setContext(loggerContext);
//        filter.start();

        fileAppender.setEncoder(ple);
        fileAppender.setRollingPolicy(rollPolicy);
        fileAppender.setTriggeringPolicy(trigPolicy);
//        fileAppender.addFilter(filter);
        fileAppender.start();

        rootLogger.addAppender(fileAppender);

//        // Console Appender
//        PatternLayoutEncoder cple = new PatternLayoutEncoder();
//        cple.setContext(loggerContext);
//        cple.setPattern("%d{HH:mm:ss.SSS} %-5level %logger{0} - %msg%n");
//        cple.start();
//
//
//        ConsoleAppender<ILoggingEvent> consAppender = new ConsoleAppender<ILoggingEvent>();
//        consAppender.setName("STDOUT");
//        consAppender.setEncoder(cple);
//        consAppender.start();
//        rootLogger.addAppender(consAppender);
    }

    /**
     * Creates the configuration.
     *
     * @param inputModel the input model
     * @param qvtiAsUri the qvti as uri
     * @param direction the direction of execution
     * @param source the name of the input domain
     * @param outModelsFolder the output models folder
     * @return the configuration
     */
    // FIXME The source name can be found from the configuration (input typemodel)
    private static Configuration createConfiguration(Path inputModel, URI qvtiAsUri, String direction,
            String source, String outModelsFolder) {

        try {
            setUp();
        } catch (Exception e1) {
            logger.error("Error creating resource.", e1);
        }
        myQVT = createQVT();
        Resource qvtiAsModel = null;
        try {
            qvtiAsModel = MallardMtcUtil.loadASModelFromUri(qvtiAsUri, myQVT.getEnvironmentFactory());
        } catch (QvtMtcExecutionException e) {
          logger.error("Error creating resource.", e);
        }
        Path modelName = inputModel.getFileName();
        String[] tokens = modelName.toString().split("\\.(?=[^\\.]+$)");
        URI inputModelURI = URI.createFileURI(inputModel.toString());
        //Path base = inputModel.getParent();
        //URI genModelsUri = URI.createFileURI(base.toString());
        URI outModelsUri = URI.createFileURI(outModelsFolder);
        String model_folder = getQvtiasId(qvtiAsUri);
        outModelsUri = outModelsUri.appendSegment(model_folder);
        String outputSegment = tokens[0] + "_out." + tokens[1];
        String middleSegment = tokens[0] + "_trace." + tokens[1];
        URI outputModelURI = outModelsUri.appendSegment(outputSegment);
        URI middleModelURI = outModelsUri.appendSegment(middleSegment);
        Configuration config = null;
        try {
            config = MallardCompiler.createConfiguration(qvtiAsModel, Mode.ENFORCE, direction);
        } catch (QvtMtcExecutionException e) {
            logger.error("Error creating resource.", e);
        }
        finally {
            if (config != null) {
                config.addInputModel(source, inputModelURI);
                config.addMiddleModel(middleModelURI);
                config.addOutputModel(outputModelURI);
            }
        }
        return config;
    }

    /**
     * Creates the qvt.
     *
     * @return the QV timperative
     */
    private @NonNull
    static QVTimperative createQVT() {
        return QVTimperative.newInstance(getProjectMap(), null);
    }

    private static @NonNull StandaloneProjectMap getProjectMap() {
        StandaloneProjectMap projectMap2 = projectMap;
        if (projectMap2 == null) {
            projectMap = projectMap2 = EMFPlugin.IS_ECLIPSE_RUNNING ? new ProjectMap(false) : new StandaloneProjectMap(false);
        }
        return projectMap2;
    }

    /**
     * Execute the QVTi transformation for the given configuration.
     *
     * @param qvtiAsURI the qvti as model
     * @param config the config
     * @param verbose the verbose
     * @return the string
     */
    private static String execute(URI qvtiAsURI, @NonNull Configuration config, boolean verbose) {
        String inputModel = "";
        String qvtias = "";
        try {
            inputModel = getInputModelName(config);
            String qvtias_test = qvtiAsURI.segment(qvtiAsURI.segmentCount()-2);
            qvtias = getQvtiasId(qvtiAsURI);
            String model = inputModel.split("\\.")[0];
            Level level = verbose ? Level.DEBUG : Level.INFO;
            configureLogger(qvtias_test + "_" + qvtias + "_" + model, level);
        } catch (SecurityException | IOException e1) {
            logger.error("Error creating resource.", e1);
        }
        Resource qvtiAsModel = null;
        try {
            qvtiAsModel = MallardMtcUtil.loadASModelFromUri(qvtiAsURI, myQVT.getEnvironmentFactory());
        } catch (QvtMtcExecutionException e) {
          logger.error("Error creating resource.", e);
        }
        boolean success = false ;
        @NonNull Map<Object, Object> savingOptions = getDefaultSavingOptions();
        savingOptions.put(XMIResource.OPTION_SCHEMA_LOCATION, true);
        long totalTime = Integer.MAX_VALUE;
        try {
            long startTime = System.currentTimeMillis();
            success = MallardEngine.execute(qvtiAsModel, config, myQVT.getEnvironmentFactory(), verbose, savingOptions);
            totalTime = System.currentTimeMillis() - startTime;
        } catch (QvtMtcExecutionException e) {
            logger.error("Error creating resource.", e);
        }
        Object[] preParamArray = {qvtias, inputModel, success, totalTime};
        logger.info("Execution of {} for model {} was {} in {}.", preParamArray);
        String outputModelPath = config.getOutputModel().toFileString();
        config = null;
        try {
            tearDown();
        } catch (Exception e) {
            logger.error("Error on teardown.", e);
        }
        if (!success) {
            throw new IllegalStateException("Result was false");
        }
        return outputModelPath;
    }

    private static @NonNull Map<Object, Object> getDefaultSavingOptions() {
        Map<Object, Object> defaultSavingOptions = new HashMap<Object, Object>();
        defaultSavingOptions.put(XMLResource.OPTION_ENCODING, "UTF-8");
        defaultSavingOptions.put(XMLResource.OPTION_LINE_DELIMITER, "\n");
        defaultSavingOptions.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
        defaultSavingOptions.put(XMLResource.OPTION_SCHEMA_LOCATION_IMPLEMENTATION, Boolean.TRUE);
        defaultSavingOptions.put(XMLResource.OPTION_LINE_WIDTH, Integer.valueOf(132));
        return defaultSavingOptions;
    }

    /**
     * Execute the QVTi transformation in ballot mode for the given configuration.
     *
     * @param qvtiAsURI the qvti as URI
     * @param config the config
     * @param verbose the verbose
     * @return the string
     */
    private static String executeBallot(URI qvtiAsURI, @NonNull Configuration config, boolean verbose) {
        String inputModel = "";
        String qvtias = "";
        try {
            inputModel = getInputModelName(config);
            String qvtias_test = qvtiAsURI.segment(qvtiAsURI.segmentCount()-2);
            qvtias = getQvtiasId(qvtiAsURI);
            String model = inputModel.split("\\.")[0];
            Level level = verbose ? Level.DEBUG : Level.INFO;
            configureLogger(qvtias_test + "_" + qvtias + "_" + model, level);
        } catch (SecurityException | IOException e1) {
            logger.error("Error creating resource.", e1);
        }
        Resource qvtiAsModel = null;
        try {
            qvtiAsModel = MallardMtcUtil.loadASModelFromUri(qvtiAsURI, myQVT.getEnvironmentFactory());
        } catch (QvtMtcExecutionException e) {
          logger.error("Error creating resource.", e);
        }
        boolean success = false ;
        @NonNull Map<Object, Object> savingOptions = getDefaultSavingOptions();
        savingOptions.put(XMIResource.OPTION_SCHEMA_LOCATION, true);
        long totalTime = Integer.MAX_VALUE;
        try {
            long startTime = System.currentTimeMillis();
            success = MallardEngine.executeBallot(qvtiAsModel, config, myQVT.getEnvironmentFactory(), verbose, savingOptions);
            totalTime = System.currentTimeMillis() - startTime;
        } catch (QvtMtcExecutionException e) {
            logger.error("Error creating resource.", e);
        }
        Object[] preParamArray = {qvtias, inputModel, success, totalTime};
        logger.info("Execution of {} for model {} was {} in {}.", preParamArray);
        String outputModelPath = config.getOutputModel().toFileString();
        config = null;
        try {
            tearDown();
        } catch (Exception e) {
            logger.error("Error on teardown.", e);
        }
        if (!success) {
            throw new IllegalStateException("Result was false");
        }
        return outputModelPath;
    }

    /**
     * Get the execution arguments for the Abstract2Concrete tests.
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getAbstract2Concrete() {
        String direction = "umlOut";
        String testName = "Abstract2Concrete";
        String folderName = "Abstract2Concrete";
        String source = "umlIn";
        String[] ret = {direction, testName, folderName, source, null};
        return ret;
    }

    /**
     * Get the execution arguments for the BibTeXML2DocBook tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getBibTeXML2DocBook() {
        String direction = "docbook";
        String testName = "BibTeXML2DocBook";
        String folderName = "BibTeXML2DocBook";
        String source = "bibtex";
        Function<EObject, String> idMatcher = new DocBookIDFunction();
        Object[] ret = {direction, testName, folderName, source, idMatcher};
        return ret;
    }

    /**
     * Get the execution arguments for the DNF tests.
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getDNF() {
        String direction = "dnf";
        String testName = "DNF";
        String folderName = "DNF";
        String source = "bexp";
        String[] ret = {direction, testName, folderName, source, null};
        return ret;
    }

    /**
     * Get the execution arguments for the Mi2Si tests.
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getMiSi() {
        String direction = "java";
        String testName = "Mi2Si";
        String folderName = "Mi2Si";
        String source = "uml";
        String[] ret = {direction, testName, folderName, source, null};
        return ret;
    }

    /**
     * Get the execution arguments for the Railway2Control tests.
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getRailway2Control() {
        String direction = "control";
        String testName = "Railway2Control";
        String folderName = "Railway2Control";
        String source = "rail";
        String[] ret = {direction, testName, folderName, source, null};
        return ret;
    }

    /**
     * Get the execution arguments for the Mi2Si tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getPathExp2PetriNet_TextExp() {
        String direction = "path";
        String testName = "TextualPathExp2PathExp";
        String folderName = "TextualPathExp2PathExp";
        String source = "text";
        Function<EObject, String> idMatcher = new PathExpIDFunction();
        Object[] ret = {direction, testName, folderName, source, idMatcher};
        return ret;
    }

    /**
     * Get the execution arguments for the Mi2Si tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getPathExp2PetriNet_PathExp() {
        String direction = "petri";
        String testName = "PathExp2PetriNet";
        String folderName = "PathExp2PetriNet";
        String source = "path";
        Function<EObject, String> idMatcher = new PetriNetIDFunction();
        Object[] ret = {direction, testName, folderName, source, idMatcher};
        return ret;
    }

    /**
     * Get the execution arguments for the Mi2Si tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getPathExp2PetriNet_PetriNet() {
        String direction = "xml";
        String testName = "PetriNet2XML";
        String folderName = "PetriNet2XML";
        String source = "petri";
        Function<EObject, String> idMatcher = new XMLIDFunction();
        Object[] ret = {direction, testName, folderName, source, idMatcher};
        return ret;
    }

    /**
     * Register the package in the resource in the given ResourceSet.
     * @param rs The resource set
     * @param resource The resource that contains the package
     */
    private static void registerPackages(ResourceSet rs, Resource resource) {
        EObject eObject = resource.getContents().get(0);
        if (eObject instanceof EPackage) {
            EPackage p = (EPackage)eObject;
            rs.getPackageRegistry().put(p.getNsURI(), p);
        }
    }

    /**
     * Get the execution arguments for the XSLT2XQuery tests.
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getXSLT2XQuery() {
        String direction = "xq";
        String testName = "XSLT2XQuery";
        String folderName = "XSLT2XQuery";
        String source = "petri";
        String[] ret = {direction, testName, folderName, source};
        return ret;
    }



    /**
     * Gets the all gen parameters.
     *
     * @return the all gen parameters
     */
    private static Iterable<Object[]> getTestCaseParameters() {
        ArrayList<Object[]> result = new ArrayList<Object[]>();
//        result.add(getAbstract2Concrete());
//        result.add(getBibTeXML2DocBook());
//        result.add(getDNF());
//        result.add(getMiSi());
        result.add(getPathExp2PetriNet_TextExp());
        result.add(getPathExp2PetriNet_PathExp());
        result.add(getPathExp2PetriNet_PetriNet());
        result.add(getRailway2Control());
//        result.add(getXSLT2XQuery());
        return result;
    }

    /**
     * Gets the differences.
     *
     * @param outputModel the output model
     * @param oracleModel the oracle model
     * @param idMatcher
     * @return the differences
     */
    public static boolean getDifferences(Resource outputModel, Resource oracleModel,
            Function<EObject, String> idMatcher) {

        Comparison result = compare(outputModel, oracleModel, idMatcher);
        if (result.getDifferences().isEmpty()) {
            return true;
        }
        else {
            for (Diff dif : result.getDifferences()) {
                if (dif instanceof ReferenceChange) {
                    //System.out.println(((ReferenceChange) dif).getReference());
                    String feature = MallardDiffEngine.getLabel(((ReferenceChange)dif).getReference());
                    String missingValue = MallardDiffEngine.getLabel(((ReferenceChange)dif).getValue());
                    String owner = EObjectUtil.getLabel(dif.getMatch().getRight());
                    String reason = "";
                    switch (dif.getKind()) {
                        case DELETE:
                            reason = "is missing from";
                            break;
                        case ADD:
                            reason = "is additional in";
                            break;
                        case CHANGE:
                            reason = "has changed in";
                            break;
                        case MOVE:
                            reason = "has moved in";
                            break;
                    }
                    logger.error(String.format("Reference to %s in %s, in %s, %s the output model", missingValue, feature, owner, reason));
                }
                else if (dif instanceof AttributeChange) {
                    String value = (((AttributeChange)dif).getValue()).toString();
                    String attribute = MallardDiffEngine.getLabel(((AttributeChange)dif).getAttribute());
                    String owner = MallardDiffEngine.getLabel(dif.getMatch().getRight());
                    String reason = "";
                    switch (dif.getKind()) {
                        case DELETE:
                            reason = "was transformed incorrectly";
                            break;
                        case ADD:
                            reason = "has diffrent value";
                            break;
                        case CHANGE:
                            reason = "has diffrent value";
                            break;
                        case MOVE:
                            reason = "has moved in";
                            break;
                    }
                    logger.error(String.format("Attribute %s = %s, in %s, %s.", attribute, value, owner, reason));
                }
                else {
                    logger.error("We need messages for the other cases");

                }
            }
            return false;
        }
    }


    /**
     * Gets the input model name from the configuration.
     *
     * @param config the config
     * @return the input model name
     */
    private static String getInputModelName(Configuration config) {
        String inputModel;
        URI inputModelUri = null;
        for (List<URI> models : config.getInputModels().values()) {
            inputModelUri = models.get(0);
            break;	// Only one input model
        }

        inputModel = inputModelUri.lastSegment();
        return inputModel;
    }

    /**
     * Gets the qvtias id.
     *
     * @param qvtiAsURI the qvti as URI
     * @return the qvtias id
     */
    private static String getQvtiasId(URI qvtiAsURI) {
        // Need a different output model per qvtias
        int qvtiasinfo = qvtiAsURI.lastSegment().indexOf("_") + 1;
//        String model_folder = qvtiAsModel.getURI().lastSegment().substring(qvtiasinfo).split("\\.(?=[^\\.]+$)")[0];
        String qvtiasname = qvtiAsURI.lastSegment().substring(qvtiasinfo);
        String[] test = qvtiasname.split("\\.(?!.*\\.)");
        String model_folder;
        if (test.length > 0) {
            model_folder = test[0];
        }
        else {
            model_folder = qvtiasname.split("\\.")[0];
        }
        return model_folder;
    }

    /**
     * Gets the qvti resources.
     *
     * @param ballotInfo the ballot info
     * @return the qvti resources
     */
    public static List<URI> getQvtiUris(String ballotInfo) {
        List<URI> resources = null;
        try (Stream<String> stream = Files.lines(Paths.get(ballotInfo))) {
            resources = stream.map(URI::createURI).collect(Collectors.toList());
        } catch (IOException e) {
            logger.error("Error opening param file.", e);
        }
        finally {
            if (resources == null) {
                resources = Collections.emptyList();
            }
        }
        return resources;
    }

    /**
     * Sets the run environment.
     *
     * @throws Exception the exception
     */
    private static void setUp() throws Exception {

        BaseLinkingService.DEBUG_RETRY.setState(true);
        StandardLibraryContribution.REGISTRY.put(StandardLibraryImpl.DEFAULT_OCL_STDLIB_URI, new OCLstdlib.Loader());
        EcorePackage.eINSTANCE.getClass();
        PivotStandaloneSetup.doSetup();
        if (!EcorePlugin.IS_ECLIPSE_RUNNING) {
            File workspaceLoc;
            workspaceLoc = new File(System.getenv("MALLARD"));
            String strlist = System.getenv().get("FOLDERS");
            ArrayList<String> folders = new ArrayList<>(Arrays.asList(strlist.split(",")));
            File[] files = workspaceLoc.listFiles();
            for (File file : files) {
                if (file.isDirectory() && folders.contains(file.getName())) {
                    for (File prj : file.listFiles()) {
                        if (prj.isDirectory()) {
                            String name = prj.getName();
                            EcorePlugin.getPlatformResourceMap().put(name, URI.createFileURI(prj.toString() + "/"));
                        }
                    }
                }
            }
        }
        OCLstdlib.install();
        QVTcoreStandaloneSetup.doSetup();
        QVTcorePivotStandaloneSetup.doSetup();
        QVTimperativePivotStandaloneSetup.doSetup();

    }

    /**
     * Path name contains string from list.
     *
     * @param p the path
     * @param items the items
     * @return true, if successful
     */
    private static boolean stringContainsItemFromList(Path p, String[] items) {
        String name = p.getFileName().toString();
        boolean anyMatch = Arrays.stream(items).parallel().anyMatch(name::contains);
        return anyMatch;
    }

    /**
     * Tear down the run environment.
     *
     * @throws Exception the exception
     */
    private static void tearDown() throws Exception {
        myQVT.getEnvironmentFactory().dispose();
        myQVT.dispose();
        myQVT = null;
        StandardLibraryContribution.REGISTRY.remove(StandardLibraryImpl.DEFAULT_OCL_STDLIB_URI);
    }


}
