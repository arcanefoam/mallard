/*******************************************************************************
 * Copyright (c) 2016 University of York
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - Initial API and implementation
 *******************************************************************************/
package uk.ac.york.qvtd.benchmark;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.internal.StandardLibraryImpl;
import org.eclipse.ocl.pivot.internal.library.StandardLibraryContribution;
import org.eclipse.ocl.pivot.model.OCLstdlib;
import org.eclipse.ocl.pivot.utilities.PivotStandaloneSetup;
import org.eclipse.ocl.xtext.base.services.BaseLinkingService;
import org.eclipse.qvtd.pivot.qvtcore.QVTcorePivotStandaloneSetup;
import org.eclipse.qvtd.pivot.qvtimperative.QVTimperativePivotStandaloneSetup;
import org.eclipse.qvtd.pivot.qvtimperative.utilities.QVTimperative;
import org.eclipse.qvtd.xtext.qvtbase.tests.PivotTestCase;
import org.eclipse.qvtd.xtext.qvtbase.tests.XtextTestCase;
import org.eclipse.qvtd.xtext.qvtbase.tests.utilities.TestsXMLUtil;
import org.eclipse.qvtd.xtext.qvtcore.QVTcoreStandaloneSetup;

import uk.ac.york.qvtd.compiler.Configuration.Mode;
import uk.ac.york.qvtd.compiler.MallardCompiler;
import uk.ac.york.qvtd.compiler.utilities.MallardMtcUtil;
import uk.ac.york.qvtd.compiler.utilities.QvtMtcExecutionException;
import uk.ac.york.qvtd.pivot.qvtimperative.evaluation.MallardEngine;
import uk.ac.york.qvtd.pivot.qvtimperative.evaluation.MallardQVTiExecutor;

/**
 * The Class QVTdBenchmark.
 */
public class QVTdBenchmark_Old  {

    public static String[] getTestArgs(String test) {
        switch (test) {
            case "Families2Persons":
                return getFamilies2PersonsArgs();
            case "Hstm2Stm":
                return getHstm2StmArgs();
            case "HSV2HSL":
                return getHSV2HSLArgs();
            case "Rail2Control":
                return getRail2ControlArgs();
            case "Uml2Rdbms":
                return getUml2RdbmsArgs();
            case "Upper2Lower":
                return getUpper2LowerArgs();
            case "Upper2Lower1k":
                return getUpper2Lower1kArgs();
            case "Upper2Lower10k":
                return getUpper2Lower10kArgs();
            case "Upper2Lower100k":
                return getUpper2Lower100kArgs();
            default:
                throw new IllegalArgumentException("Invalid test name: " + test);
        }
    }

    /**
     * Gets the current time stamp.
     *
     * @return the current time stamp
     */
    private static String getCurrentTimeStamp() {
        return new SimpleDateFormat("HH:mm:ss.SSS").format(new Date());
    }

    /**
     * Gets the arguments for the Families2Persons test.
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getFamilies2PersonsArgs() {
        String direction = "person";
        String sourcesPath = "examples/uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/Families2Persons";
        String qvtcasSegment = "Families2Persons.qvtcas";
        String samplesPath = "tests/org.eclipse.qvtd.build.etl.tests/src/org/eclipse/qvtd/build/etl/tests/Families2Persons/samples";
        String expectedSegment = "Persons_expected.xmi";
        String inputSegment = "Families.xmi";
        String outputSegment = "Persons.xmi";
        String traceSegment = "Families2Persons_trace.xmi";
        String[] ret = {direction, sourcesPath, qvtcasSegment, samplesPath, expectedSegment, inputSegment,
                outputSegment, traceSegment};
        return ret;
    }

    /**
     * Gets the arguments for the Hstm2Stm tests
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getHstm2StmArgs() {
        String direction = "stm";
        String sourcesPath = "examples/uk.ac.york.qvtd.examples.qvtcore.modelmorf/qvtcsrc/Hstm2Stm";
        String qvtcasSegment = "hstm2stm.qvtcas";
        String samplesPath = "tests/org.eclipse.qvtd.build.etl.tests/src/org/eclipse/qvtd/build/etl/tests/Hstm2Stm/samples";
        String expectedSegment = "Samek_expected.xmi";
        String inputSegment = "HSamek.xmi";
        String outputSegment = "Samek.xmi";
        String traceSegment = "HstmToStm_trace.xmi";
        String[] ret = {direction, sourcesPath, qvtcasSegment, samplesPath, expectedSegment, inputSegment,
                outputSegment, traceSegment};
        return ret;
    }

    /**
     * Gets the arguments for the HSV2HSL tests
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getHSV2HSLArgs() {

        String direction = "hsl";
        String sourcesPath = "tests/org.eclipse.qvtd.build.etl.tests/src/org/eclipse/qvtd/build/etl/tests/HSV2HSL";
        String qvtcasSegment = "HSV2HSL.qvtcas";
        String samplesPath = "tests/org.eclipse.qvtd.build.etl.tests/src/org/eclipse/qvtd/build/etl/tests/HSV2HSL/samples";
        String expectedSegment = "SolarizedHSL_expected.xmi";
        String inputSegment = "SolarizedHSV.xmi";
        String outputSegment = "SolarizedHSL.xmi";
        String traceSegment = "HSV2HSL_trace.xmi";
        String[] ret = {direction, sourcesPath, qvtcasSegment, samplesPath, expectedSegment, inputSegment,
                outputSegment, traceSegment};
        return ret;
    }

    /**
     * Get the arguments for the Rail2Control tests.
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getRail2ControlArgs() {

        String direction = "control";
        String sourcesPath = "plugins/uk.ac.york.qvtd.compiler.tests/src/uk/ac/york/qvtd/compiler/tests/train";
        String qvtcasSegment = "railway2control.qvtcas";
        String samplesPath = "plugins/uk.ac.york.qvtd.compiler.tests/src/uk/ac/york/qvtd/compiler/tests/train/samples";
        String expectedSegment = "Control_expected.xmi";
        String inputSegment = "Norhten.xmi";
        String outputSegment = "NorhtenControl.xmi";
        String traceSegment = "Rail2Control_trace.xmi";
        String[] ret = {direction, sourcesPath, qvtcasSegment, samplesPath, expectedSegment, inputSegment,
                outputSegment, traceSegment};
        return ret;
    }

    /**
     * Get the arguments for the UmlToRdbms test.
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getUml2RdbmsArgs()  {

        String direction = "rdbms";
        String sourcesPath = "examples/uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/UML2RDBMS";
        String qvtcasSegment = "Uml2Rdbms.qvtcas";
        String samplesPath = "plugins/uk.ac.york.qvtd.benchmark/src/uk/ac/york/qvtd/benchmark/samples";
        String expectedSegment = "SimpleRDBMSPeople_expected.xmi";
        String inputSegment = "SimpleUMLPeople.xmi";
        String outputSegment = "SimpleRDBMSPeople.xmi";
        String traceSegment = "SimpleUML2RDBMS_trace.xmi";
        String[] ret = {direction, sourcesPath, qvtcasSegment, samplesPath, expectedSegment, inputSegment,
                outputSegment, traceSegment};
        return ret;
    }

    private static String[] getUpper2Lower100kArgs() {
        String direction = "lowerGraph";
        String sourcesPath = "tests/org.eclipse.qvtd.build.etl.tests/src/org/eclipse/qvtd/build/etl/tests/UpperToLower";
        String qvtcasSegment = "UpperToLower.qvtcas";
        String samplesPath = "plugins/uk.ac.york.qvtd.benchmark/src/uk/ac/york/qvtd/benchmark/samples";
        String expectedSegment = "SimpleGraphLower_expected.xmi";
        String inputSegment = "SimpleGraph100k.xmi";
        String outputSegment = "SimpleGraphLower100k.xmi";
        String traceSegment = "UpperToLower100k_trace.xmi";
        String[] ret = {direction, sourcesPath, qvtcasSegment, samplesPath, expectedSegment, inputSegment,
                outputSegment, traceSegment};
        return ret;
    }

    private static String[] getUpper2Lower10kArgs() {
        String direction = "lowerGraph";
        String sourcesPath = "tests/org.eclipse.qvtd.build.etl.tests/src/org/eclipse/qvtd/build/etl/tests/UpperToLower";
        String qvtcasSegment = "UpperToLower.qvtcas";
        String samplesPath = "plugins/uk.ac.york.qvtd.benchmark/src/uk/ac/york/qvtd/benchmark/samples";
        String expectedSegment = "SimpleGraphLower_expected.xmi";
        String inputSegment = "SimpleGraph10k.xmi";
        String outputSegment = "SimpleGraphLower10k.xmi";
        String traceSegment = "UpperToLower10k_trace.xmi";
        String[] ret = {direction, sourcesPath, qvtcasSegment, samplesPath, expectedSegment, inputSegment,
                outputSegment, traceSegment};
        return ret;
    }

    private static String[] getUpper2Lower1kArgs() {
        String direction = "lowerGraph";
        String sourcesPath = "tests/org.eclipse.qvtd.build.etl.tests/src/org/eclipse/qvtd/build/etl/tests/UpperToLower";
        String qvtcasSegment = "UpperToLower.qvtcas";
        String samplesPath = "plugins/uk.ac.york.qvtd.benchmark/src/uk/ac/york/qvtd/benchmark/samples";
        String expectedSegment = "SimpleGraphLower_expected.xmi";
        String inputSegment = "SimpleGraph1k.xmi";
        String outputSegment = "SimpleGraphLower1k.xmi";
        String traceSegment = "UpperToLower1k_trace.xmi";
        String[] ret = {direction, sourcesPath, qvtcasSegment, samplesPath, expectedSegment, inputSegment,
                outputSegment, traceSegment};
        return ret;
    }

    /**
     * Get the arguments for the UpperToLower test.
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getUpper2LowerArgs() {
        String direction = "lowerGraph";
        String sourcesPath = "tests/org.eclipse.qvtd.build.etl.tests/src/org/eclipse/qvtd/build/etl/tests/UpperToLower";
        String qvtcasSegment = "UpperToLower.qvtcas";
        String samplesPath = "tests/org.eclipse.qvtd.build.etl.tests/src/org/eclipse/qvtd/build/etl/tests/UpperToLower/samples";
        String expectedSegment = "SimpleGraphLower_expected.xmi";
        String inputSegment = "SimpleGraph.xmi";
        String outputSegment = "SimpleGraphLower.xmi";
        String traceSegment = "UpperToLower_trace.xmi";
        String[] ret = {direction, sourcesPath, qvtcasSegment, samplesPath, expectedSegment, inputSegment,
                outputSegment, traceSegment};
        return ret;
    }

    /** The Constant INPUT_URI. */
    public final String INPUT_URI = "inputURI";


    /** The Constant OUTPUT_URI. */
    public final String OUTPUT_URI = "outputURI";

    /** The Constant MIDDLE_URI. */
    public final String MIDDLE_URI = "middleURI";


    /** The num of executions. */
    private final int numOfExecutions;		// = 10;


    /** The warm up time ms. */
    private final int warmUpTime;				// = 300000; // 5 minutes


    /** The my qvt. */
    public QVTimperative myQVT;

    /** The warmup. */
    private final boolean warmup;

    /** The type. */
    private final Benchmark type;

    /** The verbose. */
    private final boolean verbose;

//    private static ModelValidation getUpper2LowerValidation() {
//        EvlValidation val = new Upper2LowerValidation();
//        return val;
//    }
//
//    private static class Upper2LowerValidation extends EvlValidation {
//
//        private static final String EVL = "uk.ac.york.qvtd.benchmark/src/uk/ac/york/qvtd/benchmark/evl/UpperToLower.evl";
//
//        protected Upper2LowerValidation() {
//            super();
//            URL r1 = this.getClass().getResource("/uk/ac/york/qvtd/benchmark/evl/UpperToLower.evl");
//            try {
//                super.setEvlScript(r1.toURI());
//            } catch (URISyntaxException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//        }
//
//        public boolean validate(Configuration configuration) {
//            EmfModelFactory factory = EmfModelFactory.getInstance();
//            EmfModel lowerModel;
//            try {
//                lowerModel = loadOutputModel(configuration, factory);
//                getEngine().addModel(lowerModel);
//                for (EmfModel upperModel : loadInputModels(configuration, factory)) {
//                    getEngine().addModel(lowerModel);
//                }
//            } catch (EolModelLoadingException e) {
//                setErrMessage(e.getMessage());
//                return false;
//            }
//
//            return super.validate(configuration);
//        }
//
//        private EmfModel loadOutputModel(Configuration configuration, EmfModelFactory factory)
//                throws EolModelLoadingException {
//            TypedModel tm = configuration.getOutputDomain();
//            String name = tm.getName();
//            URI uri = configuration.getOutputModel().getURI();
//            String path = uri.toFileString();
//            File file = new File(path);
//            Model mmModel = (Model) tm.getUsedPackage().get(0).eContainer();
//            String mmPath = mmModel.getExternalURI().substring(0, mmModel.getExternalURI().lastIndexOf('.'));
//            File mmfile = new File(URI.createURI(mmPath).toFileString());
//            EmfModel em = factory.loadEmfModel(name, file, mmfile);
//            em.setReadOnLoad(true);
//            em.setStoredOnDisposal(false);
//            em.setExpand(true);
//            return em;
//        }
//
//        private List<EmfModel> loadInputModels(Configuration configuration, EmfModelFactory factory)
//                throws EolModelLoadingException {
//
//            List<EmfModel> models = new ArrayList<EmfModel>();
//            int index = 1;
//            for(Entry<TypedModel, List<Resource>> entry : configuration.getInputModels().entrySet()) {
//                TypedModel tm = entry.getKey();
//                String name = tm.getName() + index++;
//                for (Resource m : entry.getValue()) {
//                    URI uri = m.getURI();
//                    String path = uri.toFileString();
//                    File file = new File(path);
//                    Model mmModel = (Model) tm.getUsedPackage().get(0).eContainer();
//                    String mmPath = mmModel.getExternalURI().substring(0, mmModel.getExternalURI().lastIndexOf('.'));
//                    File mmfile = new File(URI.createURI(mmPath).toFileString());
//                    EmfModel em = factory.loadEmfModel(name, file, mmfile);
//                    em.setReadOnLoad(true);
//                    em.setStoredOnDisposal(false);
//                    em.setExpand(true);
//                    em.getAliases().add(tm.getName());
//                }
//            }
//            return models;
//        }
//
//    }


    /** The results */
    private Map<Resource, List<Long>> results = new HashMap<Resource, List<Long>>();


    /** The broker */
    private MallardCompiler broker;


    /**
     * Instantiates a new QV td benchmark.
     *
     * @param numOfExecutions the num of executions
     * @param warmUpTime the warm up time
     * @param warmup the warmup
     * @param type the type
     * @param verbose the verbose
     */
    public QVTdBenchmark_Old(int numOfExecutions, int warmUpTime, boolean warmup, Benchmark type, boolean verbose) {
        super();
        this.numOfExecutions = numOfExecutions;
        this.warmUpTime = warmUpTime;
        this.warmup = warmup;
        this.type = type;
        this.verbose = verbose;
    }

    public void createLogByCost(String filePath) throws IOException {

        FileWriter writer = null;
        try {
            writer = new FileWriter(filePath);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            System.err.println("Error creating the log file. " + e.getMessage());
        }
        // One row per result, sort by cost
        List<Resource> ordered = new ArrayList<Resource>(results.keySet());
        ordered.sort(new Comparator<Resource>() {
            @Override
            public int compare(Resource o1, Resource o2) {
                if (o1 == null) {
                    Double.compare(0, broker.getCost(o2));
                }
                if (o2 == null) {
                    Double.compare(broker.getCost(o1), 0);
                }
                return Double.compare(broker.getCost(o1), broker.getCost(o2));
            }
        });
        if (writer != null) {
            /* Create a CSV string using a string builder */
            StringBuilder header = new StringBuilder();
            header.append("EP");
            for (int i = 0;i < numOfExecutions;i++) {
                header.append(",");
                header.append("R");
                header.append(i+1);
            }
            //Add another column to store the real cost
            header.append(",");
            header.append("cost");
            writer.append(header.toString());
            writer.append('\n');
            int trailingZeros = (int) Math.ceil(Math.log10(results.size())) + 1;
            String fromatString = "%0" + trailingZeros + "d";
            int planCount = 1;
            char alphabet = 'a';
            for (int i = 0; i < ordered.size(); i++) {
                Resource r = ordered.get(i);
                StringBuilder row = new StringBuilder();
                // Generate an id per plan to guarantee they are sorted by R
                String logId = null;
                // Same cost?
                if (i > 0) {
                    // TODO we can only have 26 plans per cost!
                    if (broker.getCost(ordered.get(i-1)) == broker.getCost(r)) { // Differentiate with letters
                        planCount--;
                        logId = String.format(fromatString, planCount);
                        logId += alphabet++;
                    } else {
                        alphabet = 'a';
                        logId = String.format(fromatString, planCount);
                    }
                } else {
                    logId = String.format(fromatString, planCount);
                }
                planCount++;
                row.append(logId);
                String[] segs = r.getURI().toString().split("_");
                String log_cost;
                if (segs.length > 1) {
                    log_cost = segs[1];
                    System.out.println("Map plan " + log_cost + " to " + logId);
                }
                else {
                    log_cost = "0";
                }
                for (Long t : results.get(r)) {
                    row.append(",");
                    row.append(t);
                }
                // Save the cost in the last column
                row.append(",");
                row.append(log_cost);
                writer.append(row.toString());
                writer.append('\n');
            }
            writer.flush();
            writer.close();
            System.out.println("Log saved to: " + filePath);
        }
    }

    public ModelValidation getTestValidation(String test) {
        switch (test) {
        case "Families2Persons":
            return null;
        case "Hstm2Stm":
            return null;
        case "HSV2HSL":
            return null;
        case "Rail2Control":
            return null;
        case "Uml2Rdbms":
            return null;
        case "Upper2Lower":
        case "Upper2Lower1k":
        case "Upper2Lower10k":
        case "Upper2Lower100k":
//            return getUpper2LowerValidation();

        default:
            throw new IllegalArgumentException("Invalid test name: " + test);
    }

    }

    /**
     * Run transformation.
     *
     * @param mode the mode
     * @param direction the direction
     * @param sourcesPath the sources path
     * @param qvtcasSegment the qvtcas segment
     * @param samplesPath the samples path
     * @param expectedSegment the expected segment
     * @param inputSegment the input segment
     * @param outputSegment the output segment
     * @param traceSegment the trace segment
     * @param testName
     * @param validation
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    public void runTransformation(Mode mode, String direction, String sourcesPath, String qvtcasSegment,
            String samplesPath, String expectedSegment, String inputSegment, String outputSegment, String traceSegment,
            String testName) throws QvtMtcExecutionException {

        URI sourcesUri = URI.createFileURI(sourcesPath);
        URI qvtcasUri = sourcesUri.appendSegment(qvtcasSegment);
        String genPath = MallardMtcUtil.changeTargetToBinFolder(sourcesPath);
        URI genAstUri = URI.createFileURI(genPath);
        URI modelsUri = URI.createFileURI(samplesPath);
        URI expectedOutputURI = modelsUri.appendSegment(expectedSegment);
        genPath = MallardMtcUtil.changeTargetToBinFolder(modelsUri.toString());
        URI genModelsUri = URI.createURI(genPath, true);
        @NonNull List<@NonNull URI> inputModelUris = new ArrayList<>();
        inputModelUris.add(modelsUri.appendSegment(inputSegment));
        // The generated models go to the output
        URI outputModelURI = genModelsUri.appendSegment(outputSegment);
        URI middleModelURI = genModelsUri.appendSegment(traceSegment);
        if (verbose) {
            System.out.println("Sources URI: " + sourcesUri);
            System.out.println("Samples URI: " + modelsUri);
        }
        runBenchmark(mode, direction, qvtcasUri, genAstUri, genModelsUri, inputModelUris, middleModelURI,
                outputModelURI, testName);
    }

    /**
     * Sets the up.
     *
     * @throws Exception the exception
     */
    public void setUp() throws Exception {

        BaseLinkingService.DEBUG_RETRY.setState(true);
        PivotStandaloneSetup.doSetup();
        StandardLibraryContribution.REGISTRY.put(StandardLibraryImpl.DEFAULT_OCL_STDLIB_URI, new OCLstdlib.Loader());
        XtextTestCase.configurePlatformResources();
        EcorePackage.eINSTANCE.getClass();
        OCLstdlib.install();
        QVTcoreStandaloneSetup.doSetup();
        QVTcorePivotStandaloneSetup.doSetup();
        QVTimperativePivotStandaloneSetup.doSetup();
        myQVT = createQVT();

    }

    public void storePlans(String filePath) throws IOException {

        PrintStream writer = null;
        try {
            writer = new PrintStream(filePath);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            System.err.println("Error creating the log file. " + e.getMessage());
        }
        if (writer != null) {
            //ExecutionPlanUtils.generateDOT("Sample", broker.getFinalPlans(), writer);
            //writer.flush();
            writer.close();
            System.out.println("Plans saved to: " + filePath);
        }

    }

    /**
     * Tear down.
     *
     * @throws Exception the exception
     */
    public void tearDown() throws Exception {
        myQVT.dispose();
        StandardLibraryContribution.REGISTRY.remove(StandardLibraryImpl.DEFAULT_OCL_STDLIB_URI);
    }

    /**
     * Benchmark best solution.
     *
     * @param broker the broker
     * @param testName
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private void benchmarkBestSolution(MallardCompiler broker, String testName) throws QvtMtcExecutionException {
        List<Long> runResults = executeMultipleTimes(numOfExecutions, broker, broker.getQvtiAsModel(), testName);
        results.put(broker.getQvtiAsModel(), runResults);
        if (verbose)
            System.out.println(results);
    }

    private void benchMarkChaotic(MallardCompiler broker, String testName) {
        List<Long> runResults = new ArrayList<Long>();
        Resource qvtpas = broker.getQvtpAsModel();
        boolean validate = true;
        for (int i = 0; i < numOfExecutions; i++) {
            //System.out.println("Iteration " + (i+1) + ", " +  getCurrentTimeStamp());
            Long time = -1L;
            try {
                time = executeChaotic(broker, qvtpas , testName, validate );
            } catch (QvtMtcExecutionException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            validate = false;		// We only really need one
            if (time < 0) {
                runResults.addAll(Collections.nCopies(numOfExecutions, -1L));
                break;
            }
            //System.out.println("End iteration, " +  getCurrentTimeStamp());
            runResults.add(time);
        }
        results.put(qvtpas, runResults);

    }

    /**
     * Benchmark compare multiple solutions.
     *
     * @param broker the broker
     * @param validation
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private void benchmarkCompareMultipleSolutions(MallardCompiler broker, String testName) throws QvtMtcExecutionException {
        // TODO We should run this n times with 10^1, 10^2, ..., 10^6 model elements if we want to measure scalability
        // THis is easily done by modifying the samples map!

        for (Resource iAS : broker.getQvtiAsModels()) {
            //@NonNull Transformation best = broker.getTransformation(iAS);
            System.out.println("Start Benchmark " + iAS.getURI() + ", " + getCurrentTimeStamp());
            // We want to change the output model URI to save all results to compare them too.
            List<Long> runResults = executeMultipleTimes(numOfExecutions, broker, iAS, testName);
            System.out.println("Finish Benchmark " + getCurrentTimeStamp());
            results.put(iAS, runResults);
        }
        if (verbose)
            System.out.println(results);
    }

    /**
     * Execute the QVTi AS for the given models.
     *
     * @param broker the broker
     * @param testName
     * @param transformation the transformation
     * @return the execution time (in milliseconds)
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private Long execute(MallardCompiler broker, @NonNull Resource qvtias, String testName, boolean validate) throws QvtMtcExecutionException {

//        MallardEngine engine = new MallardEngine();
//        MallardQVTiExecutor executor = engine.createExecutor(broker, verbose);
//        Long time = executeInterpreter(executor);
//        //System.out.println("End iteration, " +  getCurrentTimeStamp());
//        @NonNull
//        Map<Object, Object> savingOptions = TestsXMLUtil.defaultSavingOptions;
//        savingOptions.put(XMIResource.OPTION_SCHEMA_LOCATION, true);
//        executor.saveModels(savingOptions);
//        if (validate) {
//            ModelValidation validation = getTestValidation(testName);
//            boolean success = validation.validate(broker.getConfiguration(qvtias));
//            if (!success) {
//                // Negative time indicates failure
//                time = -1L;
//                System.err.println("Failed validation. " + validation.getErrMessage());
//            }
//        }
//        executor.dispose();
//        return time;
        return 0l;
    }

    /**
     * Execute the chaotic executor for the given models.
     *
     * @param broker the broker
     * @param testName
     * @param transformation the transformation
     * @return the execution time (in milliseconds)
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private Long executeChaotic(MallardCompiler broker, @NonNull Resource qvtpas, String testName, boolean validate) throws QvtMtcExecutionException {

//        MallardEngine engine = new MallardEngine();
//        MallardQVTiExecutor executor = engine.createChaosExecutor(broker, verbose);
//        Long time = executeInterpreter(executor);
//        //System.out.println("End iteration, " +  getCurrentTimeStamp());
//        @NonNull
//        Map<Object, Object> savingOptions = TestsXMLUtil.defaultSavingOptions;
//        savingOptions.put(XMIResource.OPTION_SCHEMA_LOCATION, true);
//        executor.saveModels(savingOptions);
//        if (validate) {
//            ModelValidation validation = getTestValidation(testName);
//            boolean success = validation.validate(broker.getConfiguration(qvtpas));
//            if (!success) {
//                // Negative time indicates failure
//                time = -1L;
//                System.err.println("Failed validation. " + validation.getErrMessage());
//            }
//        }
//        executor.dispose();
//        return time;
        return 0l;
    }

    /**
     * Execute interpreter.
     *
     * @param qvtiExecutor the qvti executor
     * @return the long
     */
    private Long executeInterpreter(MallardQVTiExecutor qvtiExecutor) {
        long startTime = System.currentTimeMillis();
        qvtiExecutor.execute();
        return System.currentTimeMillis() - startTime;
    }

    /**
     * Execute multiple times.
     *
     * @param executions the executions
     * @param broker the broker
     * @param testName
     * @param transformation the transformation
     * @return the list
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private List<Long> executeMultipleTimes(int executions, MallardCompiler broker,
            @NonNull Resource qvtias, String testName) throws QvtMtcExecutionException {

        List<Long> results = new ArrayList<Long>();
        boolean validate = true;
        for (int i = 0; i < executions; i++) {
            //System.out.println("Iteration " + (i+1) + ", " +  getCurrentTimeStamp());
            Long time = execute(broker, qvtias, testName, validate);
            validate = false;		// We only really need one
            if (time < 0) {
                // If validation fail, no point running it again
                results.addAll(Collections.nCopies(executions, -1L));
                break;
            }
            //System.out.println("End iteration, " +  getCurrentTimeStamp());
            results.add(time);
        }
        return results;
    }

    /**
     * Run benchmark.
     *
     * @param mode the mode
     * @param direction the direction
     * @param qvtcasUri the qvtcas uri
     * @param genAstUri the gen ast uri
     * @param modelsUri the models uri
     * @param inputModelUris the input model uris
     * @param middleModelURI the middle model uri
     * @param outputModelURI the output model uri
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private void runBenchmark(@NonNull Mode mode, String direction, URI qvtcasUri, URI genAstUri,
            URI modelsUri, List<@NonNull URI> inputModelUris, @NonNull URI middleModelURI,
            @NonNull URI outputModelURI, String testName) throws QvtMtcExecutionException {

        // The MallardBroker is responsible for loading the models, creating the configuration,
        // Executing the mtc and making sure everything is ready for execution
        @NonNull
        Map<Object, Object> savingOptions = TestsXMLUtil.defaultSavingOptions;
        savingOptions.put(XMIResource.OPTION_SCHEMA_LOCATION, true);
        broker = new MallardCompiler(qvtcasUri, genAstUri, myQVT.getEnvironmentFactory(),
                savingOptions);
        broker.initialConfiguration(mode, direction);
        switch (type) {
            case FIRST:
                broker.compile(verbose);
                if (warmup) {
                    warmupVM(broker);
                }
                //benchMarkChaotic(broker, testName);
                benchmarkBestSolution(broker, testName);
                break;
            case BEST:
                broker.compileBestSolutions(verbose);
                if (warmup) {
                    warmupVM(broker);
                }
                //benchMarkChaotic(broker, testName);
                benchmarkCompareMultipleSolutions(broker, testName);
                break;
            case ALL:
                broker.compileAllSolutions(verbose);
                if (warmup) {
                    warmupVM(broker);
                }
                //benchMarkChaotic(broker, testName);
                benchmarkCompareMultipleSolutions(broker, testName);
                break;
            case BEST_ACO:
                broker.compileAco(verbose);
                if (warmup) {
                    warmupVM(broker);
                }
                //benchMarkChaotic(broker, testName);
                benchmarkBestSolution(broker, testName);
                break;
            case SAMPLE_ACO:
                broker.compileAllAco(verbose);
                if (warmup) {
                    warmupVM(broker);
                }
                //benchMarkChaotic(broker, testName);
                benchmarkCompareMultipleSolutions(broker, testName);
                break;


        }
        broker.disposeModels();

    }


    /**
     * Warmup vm.
     *
     * @param broker the broker
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private void warmupVM(MallardCompiler broker) throws QvtMtcExecutionException {
        System.out.println("Start Warmup");
        long startTime = System.currentTimeMillis();
        long stopTime;
        MallardEngine engine = new MallardEngine();
//        MallardQVTiExecutor executor = engine.createExecutor(broker, verbose);
//        do {
//            engine.execute(broker, new HashMap<>(), verbose);
//            stopTime = System.currentTimeMillis();
//        } while ((stopTime - startTime) < warmUpTime);
        System.out.println("Finished Warmup");
    }

    /**
     * Creates the qvt.
     *
     * @return the QV timperative
     */
    protected @NonNull QVTimperative createQVT() {
        return QVTimperative.newInstance(PivotTestCase.getProjectMap(), null);
    }

    /**
     * The Enum Benchmark.
     */
    public enum Benchmark {

        /** The first, i.e. the best, or the first of the best (random) */
        FIRST,
        /** All the best solutions */
        BEST,
        /** A sample of 20 random solutions */
        ALL,			// All the solutions
        /** A sample of all the ACO solutions */
        SAMPLE_ACO,
        /** The best plan found by the ACO */
        BEST_ACO
    }

}
