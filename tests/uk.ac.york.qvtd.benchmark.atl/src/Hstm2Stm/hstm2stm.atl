--------------------------------------------------------------------------------
-- Copyright (c) 2016 University of York
-- All rights reserved. This program and the accompanying materials
-- are made available under the terms of the Eclipse Public License v1.0
-- which accompanies this distribution, and is available at
-- http://www.eclipse.org/legal/epl-v10.html
--
-- Contributors:
--     Horacio Hoyos - Initial API and implementation
--------------------------------------------------------------------------------

-- @path hstm=/uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/Hstm2Stm/hstmMM.ecore
-- @path stm=/uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/Hstm2Stm/stmMM.ecore

module hstm2stm;
create OUT : stm from IN : hstm;

rule LStateToState {
    from s1:hstm!State (
        s1.containsState->isEmpty())
    to s2:stm!State (
        name <- s1.name)
}

rule CStateToState {
    from s1:hstm!State (
        not s1.containsState->isEmpty())
    to s2:stm!State (
        name <- s1.name)
}

rule TransToTrans {
    from t1:hstm!Trans
    to t2:stm!Trans (
            name <- t1.name,
            fromState <- t1.fromState,
            toState <- t1.toState)
}

helper context hstm!State def : StateContainsState(mState:hstm!State) : Boolean =
    if (mState.containedInState->isEmpty()) then
        false
    else
        if (mState.containedInState = self) then
            true
        else
            self.StateContainsState(mState.containedInState)
        endif
    endif;
