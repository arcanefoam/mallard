<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="hstm2stm"/>
		<constant value="links"/>
		<constant value="NTransientLinkSet;"/>
		<constant value="col"/>
		<constant value="J"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="OclParametrizedType"/>
		<constant value="#native"/>
		<constant value="Collection"/>
		<constant value="J.setName(S):V"/>
		<constant value="OclSimpleType"/>
		<constant value="OclAny"/>
		<constant value="J.setElementType(J):V"/>
		<constant value="TransientLinkSet"/>
		<constant value="A.__matcher__():V"/>
		<constant value="A.__exec__():V"/>
		<constant value="self"/>
		<constant value="__resolve__"/>
		<constant value="1"/>
		<constant value="J.oclIsKindOf(J):B"/>
		<constant value="18"/>
		<constant value="NTransientLinkSet;.getLinkBySourceElement(S):QNTransientLink;"/>
		<constant value="J.oclIsUndefined():B"/>
		<constant value="15"/>
		<constant value="NTransientLink;.getTargetFromSource(J):J"/>
		<constant value="17"/>
		<constant value="30"/>
		<constant value="Sequence"/>
		<constant value="2"/>
		<constant value="A.__resolve__(J):J"/>
		<constant value="QJ.including(J):QJ"/>
		<constant value="QJ.flatten():QJ"/>
		<constant value="e"/>
		<constant value="value"/>
		<constant value="resolveTemp"/>
		<constant value="S"/>
		<constant value="NTransientLink;.getNamedTargetFromSource(JS):J"/>
		<constant value="name"/>
		<constant value="__matcher__"/>
		<constant value="A.__matchLStateToState():V"/>
		<constant value="A.__matchCStateToState():V"/>
		<constant value="A.__matchTransToTrans():V"/>
		<constant value="__exec__"/>
		<constant value="LStateToState"/>
		<constant value="NTransientLinkSet;.getLinksByRule(S):QNTransientLink;"/>
		<constant value="A.__applyLStateToState(NTransientLink;):V"/>
		<constant value="CStateToState"/>
		<constant value="A.__applyCStateToState(NTransientLink;):V"/>
		<constant value="TransToTrans"/>
		<constant value="A.__applyTransToTrans(NTransientLink;):V"/>
		<constant value="__matchLStateToState"/>
		<constant value="State"/>
		<constant value="hstm"/>
		<constant value="IN"/>
		<constant value="MMOF!Classifier;.allInstancesFrom(S):QJ"/>
		<constant value="containsState"/>
		<constant value="J.isEmpty():J"/>
		<constant value="B.not():B"/>
		<constant value="32"/>
		<constant value="TransientLink"/>
		<constant value="NTransientLink;.setRule(MATL!Rule;):V"/>
		<constant value="s1"/>
		<constant value="NTransientLink;.addSourceElement(SJ):V"/>
		<constant value="s2"/>
		<constant value="stm"/>
		<constant value="NTransientLink;.addTargetElement(SJ):V"/>
		<constant value="NTransientLinkSet;.addLink2(NTransientLink;B):V"/>
		<constant value="20:9-20:11"/>
		<constant value="20:9-20:25"/>
		<constant value="20:9-20:36"/>
		<constant value="21:8-22:25"/>
		<constant value="__applyLStateToState"/>
		<constant value="NTransientLink;"/>
		<constant value="NTransientLink;.getSourceElement(S):J"/>
		<constant value="NTransientLink;.getTargetElement(S):J"/>
		<constant value="3"/>
		<constant value="22:17-22:19"/>
		<constant value="22:17-22:24"/>
		<constant value="22:9-22:24"/>
		<constant value="link"/>
		<constant value="__matchCStateToState"/>
		<constant value="J.not():J"/>
		<constant value="33"/>
		<constant value="27:13-27:15"/>
		<constant value="27:13-27:29"/>
		<constant value="27:13-27:40"/>
		<constant value="27:9-27:40"/>
		<constant value="28:8-29:25"/>
		<constant value="__applyCStateToState"/>
		<constant value="29:17-29:19"/>
		<constant value="29:17-29:24"/>
		<constant value="29:9-29:24"/>
		<constant value="__matchTransToTrans"/>
		<constant value="Trans"/>
		<constant value="t1"/>
		<constant value="t2"/>
		<constant value="34:8-37:35"/>
		<constant value="__applyTransToTrans"/>
		<constant value="fromState"/>
		<constant value="toState"/>
		<constant value="35:21-35:23"/>
		<constant value="35:21-35:28"/>
		<constant value="35:13-35:28"/>
		<constant value="36:26-36:28"/>
		<constant value="36:26-36:38"/>
		<constant value="36:13-36:38"/>
		<constant value="37:24-37:26"/>
		<constant value="37:24-37:34"/>
		<constant value="37:13-37:34"/>
		<constant value="StateContainsState"/>
		<constant value="Mhstm!State;"/>
		<constant value="containedInState"/>
		<constant value="16"/>
		<constant value="0"/>
		<constant value="J.=(J):J"/>
		<constant value="14"/>
		<constant value="J.StateContainsState(J):J"/>
		<constant value="41:9-41:15"/>
		<constant value="41:9-41:32"/>
		<constant value="41:9-41:43"/>
		<constant value="44:13-44:19"/>
		<constant value="44:13-44:36"/>
		<constant value="44:39-44:43"/>
		<constant value="44:13-44:43"/>
		<constant value="47:13-47:17"/>
		<constant value="47:37-47:43"/>
		<constant value="47:37-47:60"/>
		<constant value="47:13-47:61"/>
		<constant value="45:13-45:17"/>
		<constant value="44:9-48:14"/>
		<constant value="42:9-42:14"/>
		<constant value="41:5-49:10"/>
		<constant value="mState"/>
	</cp>
	<field name="1" type="2"/>
	<field name="3" type="4"/>
	<operation name="5">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<push arg="7"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="9"/>
			<pcall arg="10"/>
			<dup/>
			<push arg="11"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="12"/>
			<pcall arg="10"/>
			<pcall arg="13"/>
			<set arg="3"/>
			<getasm/>
			<push arg="14"/>
			<push arg="8"/>
			<new/>
			<set arg="1"/>
			<getasm/>
			<pcall arg="15"/>
			<getasm/>
			<pcall arg="16"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="24"/>
		</localvariabletable>
	</operation>
	<operation name="18">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<load arg="19"/>
			<getasm/>
			<get arg="3"/>
			<call arg="20"/>
			<if arg="21"/>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<dup/>
			<call arg="23"/>
			<if arg="24"/>
			<load arg="19"/>
			<call arg="25"/>
			<goto arg="26"/>
			<pop/>
			<load arg="19"/>
			<goto arg="27"/>
			<push arg="28"/>
			<push arg="8"/>
			<new/>
			<load arg="19"/>
			<iterate/>
			<store arg="29"/>
			<getasm/>
			<load arg="29"/>
			<call arg="30"/>
			<call arg="31"/>
			<enditerate/>
			<call arg="32"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="2" name="33" begin="23" end="27"/>
			<lve slot="0" name="17" begin="0" end="29"/>
			<lve slot="1" name="34" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="35">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="4"/>
			<parameter name="29" type="36"/>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<load arg="19"/>
			<call arg="22"/>
			<load arg="19"/>
			<load arg="29"/>
			<call arg="37"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="6"/>
			<lve slot="1" name="34" begin="0" end="6"/>
			<lve slot="2" name="38" begin="0" end="6"/>
		</localvariabletable>
	</operation>
	<operation name="39">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<pcall arg="40"/>
			<getasm/>
			<pcall arg="41"/>
			<getasm/>
			<pcall arg="42"/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="5"/>
		</localvariabletable>
	</operation>
	<operation name="43">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<getasm/>
			<get arg="1"/>
			<push arg="44"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="46"/>
			<enditerate/>
			<getasm/>
			<get arg="1"/>
			<push arg="47"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="48"/>
			<enditerate/>
			<getasm/>
			<get arg="1"/>
			<push arg="49"/>
			<call arg="45"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<load arg="19"/>
			<pcall arg="50"/>
			<enditerate/>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="33" begin="5" end="8"/>
			<lve slot="1" name="33" begin="15" end="18"/>
			<lve slot="1" name="33" begin="25" end="28"/>
			<lve slot="0" name="17" begin="0" end="29"/>
		</localvariabletable>
	</operation>
	<operation name="51">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="52"/>
			<push arg="53"/>
			<findme/>
			<push arg="54"/>
			<call arg="55"/>
			<iterate/>
			<store arg="19"/>
			<load arg="19"/>
			<get arg="56"/>
			<call arg="57"/>
			<call arg="58"/>
			<if arg="59"/>
			<getasm/>
			<get arg="1"/>
			<push arg="60"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="44"/>
			<pcall arg="61"/>
			<dup/>
			<push arg="62"/>
			<load arg="19"/>
			<pcall arg="63"/>
			<dup/>
			<push arg="64"/>
			<push arg="52"/>
			<push arg="65"/>
			<new/>
			<pcall arg="66"/>
			<pusht/>
			<pcall arg="67"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="68" begin="7" end="7"/>
			<lne id="69" begin="7" end="8"/>
			<lne id="70" begin="7" end="9"/>
			<lne id="71" begin="24" end="29"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="62" begin="6" end="31"/>
			<lve slot="0" name="17" begin="0" end="32"/>
		</localvariabletable>
	</operation>
	<operation name="72">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="73"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="62"/>
			<call arg="74"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="64"/>
			<call arg="75"/>
			<store arg="76"/>
			<load arg="76"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="38"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="77" begin="11" end="11"/>
			<lne id="78" begin="11" end="12"/>
			<lne id="79" begin="9" end="14"/>
			<lne id="71" begin="8" end="15"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="64" begin="7" end="15"/>
			<lve slot="2" name="62" begin="3" end="15"/>
			<lve slot="0" name="17" begin="0" end="15"/>
			<lve slot="1" name="80" begin="0" end="15"/>
		</localvariabletable>
	</operation>
	<operation name="81">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="52"/>
			<push arg="53"/>
			<findme/>
			<push arg="54"/>
			<call arg="55"/>
			<iterate/>
			<store arg="19"/>
			<load arg="19"/>
			<get arg="56"/>
			<call arg="57"/>
			<call arg="82"/>
			<call arg="58"/>
			<if arg="83"/>
			<getasm/>
			<get arg="1"/>
			<push arg="60"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="47"/>
			<pcall arg="61"/>
			<dup/>
			<push arg="62"/>
			<load arg="19"/>
			<pcall arg="63"/>
			<dup/>
			<push arg="64"/>
			<push arg="52"/>
			<push arg="65"/>
			<new/>
			<pcall arg="66"/>
			<pusht/>
			<pcall arg="67"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="84" begin="7" end="7"/>
			<lne id="85" begin="7" end="8"/>
			<lne id="86" begin="7" end="9"/>
			<lne id="87" begin="7" end="10"/>
			<lne id="88" begin="25" end="30"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="62" begin="6" end="32"/>
			<lve slot="0" name="17" begin="0" end="33"/>
		</localvariabletable>
	</operation>
	<operation name="89">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="73"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="62"/>
			<call arg="74"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="64"/>
			<call arg="75"/>
			<store arg="76"/>
			<load arg="76"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="38"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="90" begin="11" end="11"/>
			<lne id="91" begin="11" end="12"/>
			<lne id="92" begin="9" end="14"/>
			<lne id="88" begin="8" end="15"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="64" begin="7" end="15"/>
			<lve slot="2" name="62" begin="3" end="15"/>
			<lve slot="0" name="17" begin="0" end="15"/>
			<lve slot="1" name="80" begin="0" end="15"/>
		</localvariabletable>
	</operation>
	<operation name="93">
		<context type="6"/>
		<parameters>
		</parameters>
		<code>
			<push arg="94"/>
			<push arg="53"/>
			<findme/>
			<push arg="54"/>
			<call arg="55"/>
			<iterate/>
			<store arg="19"/>
			<getasm/>
			<get arg="1"/>
			<push arg="60"/>
			<push arg="8"/>
			<new/>
			<dup/>
			<push arg="49"/>
			<pcall arg="61"/>
			<dup/>
			<push arg="95"/>
			<load arg="19"/>
			<pcall arg="63"/>
			<dup/>
			<push arg="96"/>
			<push arg="94"/>
			<push arg="65"/>
			<new/>
			<pcall arg="66"/>
			<pusht/>
			<pcall arg="67"/>
			<enditerate/>
		</code>
		<linenumbertable>
			<lne id="97" begin="19" end="24"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="1" name="95" begin="6" end="26"/>
			<lve slot="0" name="17" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="98">
		<context type="6"/>
		<parameters>
			<parameter name="19" type="73"/>
		</parameters>
		<code>
			<load arg="19"/>
			<push arg="95"/>
			<call arg="74"/>
			<store arg="29"/>
			<load arg="19"/>
			<push arg="96"/>
			<call arg="75"/>
			<store arg="76"/>
			<load arg="76"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="38"/>
			<call arg="30"/>
			<set arg="38"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="99"/>
			<call arg="30"/>
			<set arg="99"/>
			<dup/>
			<getasm/>
			<load arg="29"/>
			<get arg="100"/>
			<call arg="30"/>
			<set arg="100"/>
			<pop/>
		</code>
		<linenumbertable>
			<lne id="101" begin="11" end="11"/>
			<lne id="102" begin="11" end="12"/>
			<lne id="103" begin="9" end="14"/>
			<lne id="104" begin="17" end="17"/>
			<lne id="105" begin="17" end="18"/>
			<lne id="106" begin="15" end="20"/>
			<lne id="107" begin="23" end="23"/>
			<lne id="108" begin="23" end="24"/>
			<lne id="109" begin="21" end="26"/>
			<lne id="97" begin="8" end="27"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="3" name="96" begin="7" end="27"/>
			<lve slot="2" name="95" begin="3" end="27"/>
			<lve slot="0" name="17" begin="0" end="27"/>
			<lve slot="1" name="80" begin="0" end="27"/>
		</localvariabletable>
	</operation>
	<operation name="110">
		<context type="111"/>
		<parameters>
			<parameter name="19" type="4"/>
		</parameters>
		<code>
			<load arg="19"/>
			<get arg="112"/>
			<call arg="57"/>
			<if arg="113"/>
			<load arg="19"/>
			<get arg="112"/>
			<load arg="114"/>
			<call arg="115"/>
			<if arg="116"/>
			<load arg="114"/>
			<load arg="19"/>
			<get arg="112"/>
			<call arg="117"/>
			<goto arg="24"/>
			<pusht/>
			<goto arg="26"/>
			<pushf/>
		</code>
		<linenumbertable>
			<lne id="118" begin="0" end="0"/>
			<lne id="119" begin="0" end="1"/>
			<lne id="120" begin="0" end="2"/>
			<lne id="121" begin="4" end="4"/>
			<lne id="122" begin="4" end="5"/>
			<lne id="123" begin="6" end="6"/>
			<lne id="124" begin="4" end="7"/>
			<lne id="125" begin="9" end="9"/>
			<lne id="126" begin="10" end="10"/>
			<lne id="127" begin="10" end="11"/>
			<lne id="128" begin="9" end="12"/>
			<lne id="129" begin="14" end="14"/>
			<lne id="130" begin="4" end="14"/>
			<lne id="131" begin="16" end="16"/>
			<lne id="132" begin="0" end="16"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="17" begin="0" end="16"/>
			<lve slot="1" name="133" begin="0" end="16"/>
		</localvariabletable>
	</operation>
</asm>
