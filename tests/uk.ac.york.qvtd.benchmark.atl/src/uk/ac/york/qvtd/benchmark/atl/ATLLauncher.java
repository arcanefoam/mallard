/*******************************************************************************
 * Copyright (c) 2016 University of Alberta
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Victor Guana - Initial API and implementation
 *     Horacio Hoyos - clean up and fixes
 *******************************************************************************/
package uk.ac.york.qvtd.benchmark.atl;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.util.ExtendedMetaData;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.m2m.atl.emftvm.EmftvmFactory;
import org.eclipse.m2m.atl.emftvm.ExecEnv;
import org.eclipse.m2m.atl.emftvm.Metamodel;
import org.eclipse.m2m.atl.emftvm.Model;
import org.eclipse.m2m.atl.emftvm.Module;
import org.eclipse.m2m.atl.emftvm.impl.resource.EMFTVMResourceFactoryImpl;
import org.eclipse.m2m.atl.emftvm.util.ModuleNotFoundException;
import org.eclipse.m2m.atl.emftvm.util.ModuleResolver;
import org.eclipse.m2m.atl.emftvm.util.TimingData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An off-the-shelf launcher for ATL/EMFTVM transformations
 * @author Victor Guana - guana@ualberta.ca
 * University of Alberta - SSRG Lab.
 * Edmonton, Alberta. Canada
 * Using code examples from: https://wiki.eclipse.org/ATL/EMFTVM
 */
public class ATLLauncher {
	
	/** Execution will time-out after 20 minutes */
    private static final int EXECUTION_TIME_OUT = 20;		// TimeUnit.MINUTES
    
    /** The logger. */
    private static Logger logger = LoggerFactory.getLogger(ATLLauncher.class);
    

    private ExecEnv env = EmftvmFactory.eINSTANCE.createExecEnv();
    private Module module;
    private ResourceSet rs;
	private static ExecutorService executor;

    public void initExecEnv(String transformationLocation, String in_metamodel_path, String out_metamodel_path) {
        rs = new ResourceSetImpl();
        rs.getResourceFactoryRegistry().getExtensionToFactoryMap().putAll(Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap());
        final ExtendedMetaData extendedMetaData = new BasicExtendedMetaData(EPackage.Registry.INSTANCE);
        rs.getLoadOptions().put(XMLResource.OPTION_EXTENDED_META_DATA, extendedMetaData);

        URI transformationUri = URI.createFileURI(transformationLocation);
        Resource transformationResource = rs.getResource(transformationUri, true);
        module = (Module) transformationResource.getContents().get(0);
        // Get the module name and model information from the transformation
        String moduleName = module.getName();
        String sourcemmName = null;
        if (!module.getInputModels().isEmpty()) {
            sourcemmName = module.getInputModels().get(0).getMetaModelName();
            // Register input metamodel
            URI sourcemmUri = URI.createFileURI(in_metamodel_path);
            Metamodel sourcemm = EmftvmFactory.eINSTANCE.createMetamodel();
            sourcemm.setResource(rs.getResource(sourcemmUri, true));
            env.registerMetaModel(sourcemmName, sourcemm);
            registerPackages(rs, sourcemm.getResource());
        }
        String targetmmName = null;
        if (!module.getOutputModels().isEmpty()) {
            targetmmName = module.getOutputModels().get(0).getMetaModelName();
            // Register output metamodel
            URI targetmmUri = URI.createFileURI(out_metamodel_path);
            Metamodel targetmm = EmftvmFactory.eINSTANCE.createMetamodel();
            targetmm.setResource(rs.getResource(targetmmUri, true));
            env.registerMetaModel(targetmmName, targetmm);
            registerPackages(rs, targetmm.getResource());
        }
        //Load and run the transformation module
        MallardModuleResolver mr = new MallardModuleResolver();
        mr.addModule(module);
        env.loadModule(mr, moduleName);
        
    }

    public TimingData launch(
            String in_model_path,
            String out_model_path) throws IOException{

        // Get the module name and model information from the transformation
        String inputName = null;
        if (!module.getInputModels().isEmpty()) {
            // Create input model
            inputName = module.getInputModels().get(0).getModelName();
            URI inputUri = URI.createFileURI(in_model_path);
            Model input = EmftvmFactory.eINSTANCE.createModel();
            input.setResource(rs.getResource(inputUri, true));
            env.registerInputModel(inputName, input);
        }
        String outputName = null;
        if (!module.getOutputModels().isEmpty()) {
            // Create output model
            outputName = module.getOutputModels().get(0).getModelName();
            URI outputUri = URI.createFileURI(out_model_path);
            Model output = EmftvmFactory.eINSTANCE.createModel();
            output.setResource(rs.createResource(outputUri));
            env.registerOutputModel(outputName, output);
        }
        TimingData td = new TimingData();	// Don't time loading
        Future<Boolean> future = executor.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
            	env.run(td);
                td.finish();
                return true;
            }
        });
        Boolean result = false;
        try {
            result  =  future.get(EXECUTION_TIME_OUT, TimeUnit.MINUTES);
            // Save models
            for (Model model : env.getOutputModels().values()) {
                model.getResource().save(Collections.emptyMap());
            }
        }
        catch (InterruptedException ex) {
            logger.error("Execution thread interrupted.");
        }
        catch (ExecutionException ex) {
            logger.error("Execution exception.", ex);
        }
        catch (TimeoutException ex) {
            logger.error("Execution timedout.");
        }
        env.clearModels();
        rs.getResources().clear();
        return td;
    }

    /**
     * Create and register resource factories to read/parse .ecore, .xmi and .emftvm files,
     * we need an .xmi parser because our in/output models are .xmi and our transformations are
     * compiled using the ATL-EMFTV compiler that generates .emftvm files
     */
    public void setup() {

        EcorePackage.eINSTANCE.getClass();
        Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
        Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
        Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("emftvm", new EMFTVMResourceFactoryImpl());
        executor = Executors.newSingleThreadExecutor();
    }

    public void tearDown() {
    	executor.shutdown();
        rs.getResources().clear();
        env.clearModels();
        module = null;
        rs = null;
        env = null;
    }

    /**
     * Register the package in the resource in the given ResourceSet.
     * @param rs The resource set
     * @param resource The resource that contains the package
     */
    private static void registerPackages(ResourceSet rs, Resource resource) {
        EObject eObject = resource.getContents().get(0);
        if (eObject instanceof EPackage) {
            EPackage p = (EPackage)eObject;
            rs.getPackageRegistry().put(p.getNsURI(), p);
        }
    }

    /**
     * A ModuleResolver that stores known Modules by name.
     *
     * This class assumes that the module has been loaded previously (e.g. to get the
     * model/metamodel information)
     * @author hhoyos
     *
     */
    private class MallardModuleResolver implements ModuleResolver {

        private final Map<String, Module> known_modules = new HashMap<String, Module>();

        public void addModule(Module module) {
            String moduleName = module.getName();
            known_modules.put(moduleName, module);
        }

        @Override
        public Module resolveModule(String name) throws ModuleNotFoundException {
            Module module = known_modules.get(name);
            if (module == null) {
                throw new ModuleNotFoundException(String.format("Module %s not found", name));
            }
            return module;
        }

    }
}
