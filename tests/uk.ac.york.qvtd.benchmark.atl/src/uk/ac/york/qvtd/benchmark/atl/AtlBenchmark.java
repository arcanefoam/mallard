package uk.ac.york.qvtd.benchmark.atl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.EqualityHelper;
import org.eclipse.m2m.atl.emftvm.util.TimingData;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.google.gdata.util.common.base.Pair;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import uk.ac.york.qvtd.benchmark.MallardValidation;
import uk.ac.york.qvtd.benchmark.matcher.DocBookIDFunction;
import uk.ac.york.qvtd.benchmark.matcher.PathExpIDFunction;
import uk.ac.york.qvtd.benchmark.matcher.XMLIDFunction;

public class AtlBenchmark {

    private static Logger logger = (Logger) LoggerFactory.getLogger(AtlBenchmark.class);

    /** The Constant QVTIAS_PARAMS. */
    private static final String MM_ROOTPATH = "examples\\uk.ac.york.qvtd.examples.qvtcore\\qvtcsrc\\";

    /** The Constant OUTUPTMODEL_ROOTPATH. */
    private static final String OUTUPTMODEL_ROOTPATH = "tests\\uk.ac.york.qvtd.benchmark.atl\\model\\";

    /** The Constant OUTUPTMODEL_ROOTPATH. */
    private static final String MODULE_ROOTPATH = "tests\\uk.ac.york.qvtd.benchmark.atl\\src\\";

    /** The Constant OUTUPTMODEL_ROOTPATH. */
    private static final String QVTI_MODEL_ROOTPATH = "tests\\uk.ac.york.qvtd.benchmark\\model\\";

    private static final int NUM_ITERATIONS = 25;


    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {

        for (Object[] data : getAllGenParameters()) {
            String folderName = (String) data[0];
            String mallard_root = System.getenv("MALLARD");
            String module_folder = mallard_root + MODULE_ROOTPATH + folderName;
            String module_path =  module_folder + data[1];
            String in_metamodel_path = mallard_root + MM_ROOTPATH + folderName + data[2];
            String out_metamodel_path = mallard_root + MM_ROOTPATH + folderName + data[3];
            String modelsFolderName = (String) data[4];
            String modelsFolder = mallard_root + MallardValidation.GENMODEL_ROOTPATH + modelsFolderName;
            final Path outModelsFolder = Paths.get(mallard_root + OUTUPTMODEL_ROOTPATH + modelsFolderName);
            String[] perfModels = {"Huge", "Large", "Normal", "Small", "Tiny"};
            ATLLauncher launcher = new ATLLauncher();
            launcher.setup();
            // Run the small models to warm up the engine
            try (Stream<Path> stream = Files.list(Paths.get(modelsFolder))) {
                launcher.initExecEnv(module_path, in_metamodel_path, out_metamodel_path);
                stream.filter(p -> !stringContainsItemFromList(p, perfModels))
                        .map(p -> {
                            String name = p.getFileName().toString().split("\\.")[0];
                            String outname = name + "_out.xmi";
                            Path outpath = outModelsFolder.resolve(outname);
                            return Pair.of(p, outpath);
                        })
                        .forEach(p -> {
                            try {
                                String name = p.first.getFileName().toString().split("\\.")[0];
                                MallardValidation.configureLogger(((String)data[1]).subSequence(0, folderName.length()-1) + "_ATL" + "_" + name, Level.INFO);
                                TimingData result = launcher.launch(p.first.toString(),p.second.toString());
                                Object[] preParamArray = {data[1], name, result.getFinished() > 0, result.getFinished()};
                                logger.info("Execution of {} for model {} was {} in {}.", preParamArray);
                            } catch (IOException e) {
                                logger.error("ATl Execution failed.", e);
                            }
                        });
            }
//            String testName = modelsFolderName.substring(0, modelsFolderName.length()-1);
//            String qvtiModelsFolder =  mallard_root + QVTI_MODEL_ROOTPATH +  modelsFolderName + "Sloppy";
//            Function<EObject, String> matcher = (Function<EObject, String>) data[4];
//            compareSloppyToAtl(testName, outModelsFolder, qvtiModelsFolder , out_metamodel_path, "Ballot", matcher);
//            qvtiModelsFolder =  mallard_root + QVTI_MODEL_ROOTPATH + folderName + "ACO";
//            String testName = folderName.substring(0, folderName.length()-1);
//            // For each QVTi sample plan, run the compare
//            String acoInfo = mallard_root + MallardValidation.QVTIAS_PARAMS + "ACO/" + testName  + ".param";
//            compareAcoResults(acoInfo, data[2], outModelsFolder, qvtiModelsFolder , out_metamodel_path);
            Map<Path, Integer> atlTimedoutpaths = new HashMap<Path, Integer>();
            launcher.initExecEnv(module_path, in_metamodel_path, out_metamodel_path);
            System.out.println("Test: " + data[1]);
            for (int i=0; i < NUM_ITERATIONS; i++) {
                System.out.println("Iteration: " + i);
                List<Path> paths = Files.list(Paths.get(modelsFolder))
                      .filter(p -> stringContainsItemFromList(p, perfModels))
                      .collect(Collectors.toList());

                for (Path p : paths) {
                    if (atlTimedoutpaths.getOrDefault(p, 0) > 1) {
                        System.out.println("Skipping " + p);
                        continue;
                    }
                    try {
                        String name = p.getFileName().toString().split("\\.")[0];
                        String outname = name + "_out.xmi";
                        Path outpath = outModelsFolder.resolve(outname);
//            			Level level = verbose ? Level.DEBUG : Level.INFO;
                        MallardValidation.configureLogger(((String)data[1]).subSequence(0, folderName.length()-1) + "_ATL" + "_" + name, Level.INFO);
                        TimingData result = launcher.launch(p.toString(), outpath.toString());
                        Object[] preParamArray = {"ATL", name, result.getFinished() > 0, result.getFinished()/1000}; // Divide time by 100 so all results are in same range
                        logger.info("Execution of {} for model {} was {} in {}.", preParamArray);
                    } catch (IOException e) {
                        logger.error("ATL Execution failed.", e);
                      } catch (IllegalStateException ex) {
                        // Timed out
                          int times = atlTimedoutpaths.getOrDefault(p, 0)+1;
                          atlTimedoutpaths.put(p, times);
                          System.out.println("Time out x " + times + " " + p + "@Naive");
                      }
                }
            }
            launcher.tearDown();
        }
    }

    private static void compareAcoResults(String acoInfo, String testName, Path outModelsFolder,
            String qvtiModelsFolder,
            String out_metamodel_path) throws IOException {
        List<URI> resources = MallardValidation.getQvtiUris(acoInfo);
        for (URI acoQvtias : resources) {
            String folder = acoQvtias.lastSegment().split(".")[0];
            qvtiModelsFolder = qvtiModelsFolder + "/" + folder;
            compareSloppyToAtl(testName, outModelsFolder, qvtiModelsFolder , out_metamodel_path, folder);
        }
    }

    public static void compareSloppyToAtl(String testName, Path outModelsFolder, String qvtiModelsFolder,
            String metamodel_path,
            String type)
            throws IOException {
        // Need a new logger!
        MallardValidation.configureLogger(testName + "_" + type + "_ATL_Diff", Level.INFO);
        List<Pair<Path, Path>> matches;
        try (Stream<Path> stream = Files.list(outModelsFolder)) {
            matches = stream.map(p -> {
                try {
                    return Pair.of(p,
                                Files.list(Paths.get(qvtiModelsFolder))
                                .filter(qvtip -> qvtip.getFileName().equals(p.getFileName()))
                                .findFirst()
                                .orElseThrow(() -> new IllegalStateException(p.getFileName().toString()))
                            );
                } catch (IllegalStateException | IOException e) {
                    logger.error("Not correct folder", e);
                }
                return null;
            })
                    .collect(Collectors.toList());
        }
        EqualityHelper helper = new EcoreUtil.EqualityHelper();
        for (Pair<Path, Path> match : matches) {
            ResourceSet rs = new ResourceSetImpl();
            lazyMetamodelRegistration(metamodel_path, rs);
            String atl_model = match.getFirst().toString();
            URI atl_model_uri = URI.createFileURI(atl_model);
            String qvti_model = match.getSecond().toString();
            URI qvti_model_uri = URI.createFileURI(qvti_model);
            Resource outputModel = rs.getResource(qvti_model_uri, true);
            Resource oracleModel = rs.getResource(atl_model_uri, true);
            // 	First do EMF Equals? If no match find the differences?
            Iterator<EObject> omIt = outputModel.getContents().iterator();
            Iterator<EObject> oracleIt = oracleModel.getContents().iterator();
            boolean result = true;
            result = MallardValidation.getDifferences(outputModel, oracleModel, null);
            Object[] preParamArray = {testName, match.getFirst().getFileName().toString(), result};
            logger.info("{}: Match for model {} was {}", preParamArray);
        }
    }

    private static String lazyMetamodelRegistration(String metamodelPath, ResourceSet rs){

        Resource r = rs.getResource(URI.createFileURI(metamodelPath), true);
        EObject eObject = r.getContents().get(0);
        // A meta-model might have multiple packages we assume the main package is the first one listed
        if (eObject instanceof EPackage) {
            EPackage p = (EPackage)eObject;
            System.out.println(p.getNsURI());
            EPackage.Registry.INSTANCE.put(p.getNsURI(), p);
            return p.getNsURI();
        }
        return null;
  }

    /**
     * Path name contains string from list.
     *
     * @param p the path
     * @param items the items
     * @return true, if successful
     */
    private static boolean stringContainsItemFromList(Path p, String[] items) {
        String name = p.getFileName().toString();
        boolean anyMatch = Arrays.stream(items).parallel().anyMatch(name::contains);
        return anyMatch;
    }

    /**
     * Gets the all benchmark parameters.
     *
     * @return the all gen parameters
     */
    private static Iterable<Object[]> getAllGenParameters() {
        ArrayList<Object[]> result = new ArrayList<Object[]>();
        result.add(getAbstract2Concrete());
        result.add(getBibTeXML2DocBook());
        result.add(getMi2Si());
        result.add(getTextualPathExp2PathExp());
        result.add(getPathExp2PetriNet());
        result.add(getPetriNet2PNML());
        return result;
    }



    /**
     * Get the execution arguments for the Abstract2Concrete tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getAbstract2Concrete() {
        String atlfolder = "Abstract2Concrete\\";		// ATL needs the folder to have the slash
        String modelfolder = "Abstract2Concrete\\";
        String module = "AbstractToConcrete.emftvm";
        String in_metamodel = "ClassMM.ecore";
        String out_metamodel = "ClassMM.ecore";
        Object[] ret = {atlfolder, module, in_metamodel, out_metamodel, modelfolder, null};
        return ret;
    }

    private static Object[] getBibTeXML2DocBook() {
        String atlfolder = "BibTeXML2DocBook\\";		// ATL needs the folder to have the slash
        String modelfolder = "BibTeXML2DocBook\\";
        String module = "BibTeX2DocBook.emftvm";
        String in_metamodel = "BibTeX.ecore";
        String out_metamodel = "DocBook.ecore";
        Function<EObject, String> idMatcher = new DocBookIDFunction();
        Object[] ret = {atlfolder, module, in_metamodel, out_metamodel, modelfolder, idMatcher};
        return ret;
    }

    /**
     * Get the execution arguments for the Mi2Si tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getMi2Si() {
        String atlfolder = "Mi2Si\\";		// ATL needs the folder to have the slash
        String modelfolder = "Mi2Si\\";
        String module = "Mi2Si.emftvm";
        String in_metamodel = "umlMM.ecore";
        String out_metamodel = "javaMM.ecore";
        Object[] ret = {atlfolder, module, in_metamodel, out_metamodel, modelfolder, null};
        return ret;
    }

    /**
     * Get the execution arguments for the TextualPathExp2PathExp tests.
     *
     * @return the arguments to run the benchmark
     */
    private static String[] getTextualPathExp2PathExp() {
        String atlfolder = "PathExp2PetriNet\\";		// ATL needs the folder to have the slash
        String modelfolder = "TextualPathExp2PathExp\\";
        String module = "TextualPathExp2PathExp.emftvm";
        String in_metamodel = "TextualPathExp.ecore";
        String out_metamodel = "PathExp.ecore";
        String[] ret = {atlfolder, module, in_metamodel, out_metamodel, modelfolder};
        return ret;
    }

    /**
     * Get the execution arguments for the TextualPathExp2PathExp tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getPathExp2PetriNet() {
        String atlfolder = "PathExp2PetriNet\\";		// ATL needs the folder to have the slash
        String modelfolder = "PathExp2PetriNet\\";
        String module = "PathExp2PetriNet.emftvm";
        String in_metamodel = "PathExp.ecore";
        String out_metamodel = "PetriNet.ecore";
        Function<EObject, String> idMatcher = new PathExpIDFunction();
        Object[] ret = {atlfolder, module, in_metamodel, out_metamodel, modelfolder, idMatcher};
        return ret;
    }

    /**
     * Get the execution arguments for the TextualPathExp2PathExp tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getPetriNet2PNML() {
        String atlfolder = "PathExp2PetriNet\\";		// ATL needs the folder to have the slash
        String modelfolder = "PetriNet2XML\\";
        String module = "PetriNet2XML.emftvm";
        String in_metamodel = "PetriNet.ecore";
        String out_metamodel = "XML.ecore";
        Function<EObject, String> idMatcher = new XMLIDFunction();
        Object[] ret = {atlfolder, module, in_metamodel, out_metamodel, modelfolder};
        return ret;
    }

}
