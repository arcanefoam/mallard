package uk.ac.york.qvtd.compiler.tests;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Provide the parameters needed for the MTC tests
 * THIS FILE IS AUTO GENERATED, DO NOT EDIT IT!
 *
 * @author Horacio Hoyos
 *
 */
public class MtcTestParameterProvider {

    /** The Number of times each test is invoked. */
    private static final int NUM_TRIALS = 1;

    /**
     * Get the test parameters. For each test, the array must contain the {@link MallardMtcTests} constructor
     * parameters in order.
     * @return a list with the information for each of the tests.
     */
    public static Iterable<Object[]> getAllTestParameters() {
         ArrayList<Object[]> result = new ArrayList<Object[]>();

        // START DEVELOP EXAMPLES
//        result.add(getFamilies2PersonsBallot());
//        result.add(getHstm2StmBallot());
//        result.add(getHsv2HslBallot());
//        result.add(getUpper2LowerBallot());
//        result.add(getFamilies2PersonsACO());
//        result.add(getUml2RdbmsBallot());
//        result.add(getTinyUml2RdbmsBallot());
        // END DEVELOP EXAMPLES
        // START VALIDATION EXAMPLES
        result.add(getAbstract2ConcreteBallot());
        result.add(getBibTex2DocBookBallot());
        result.add(getDNFBallot());
        result.add(getMi2SiBallot());
        result.add(getPathExp2PetriNet_Text2PathBallot());
        result.add(getPathExp2PetriNet_Path2PetriBallot());
        result.add(getPathExp2PetriNet_Petri2PNMLBallot());
        result.add(getXSLT2XQueryBallot());
        result.add(getRailway2ControlBallot());
        // END VALIDATION EXAMPLES
        // ACO runs multiple times
        ArrayList<Object[]> tests = new ArrayList<Object[]>();
        // START DEVELOP EXAMPLES
//        tests.add(getHstm2StmACO());
//        tests.add(getHsv2HslACO());
//        tests.add(getUpper2LowerACO());
//        tests.add(getUml2RdbmsACO());
//        tests.add(getTinyUml2RdbmsACO());
        // END DEVELOP EXAMPLES
        // START VALIDATION EXAMPLES
        tests.add(getAbstract2ConcreteACO());					// OK
        tests.add(getBibTex2DocBookACO());						// OK
        tests.add(getDNFACO());									// OK
        tests.add(getMi2SiACO());								// OK
        tests.add(getPathExp2PetriNet_Text2PathACO());			// OK
        tests.add(getPathExp2PetriNet_Path2PetriACO());			// OK
        tests.add(getPathExp2PetriNet_Petri2PNMLACO());			// OK
//        tests.add(getXSLT2XQueryACO());							// No solution
        tests.add(getRailway2ControlACO());						// OK
        // END VALIDATION EXAMPLES
        for (int i = 0; i<NUM_TRIALS; i++) {
            result.addAll(tests);
        }
        return result;
    }


    /**
     * Get the execution arguments for the Families2Persons tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getFamilies2Persons() {
        String direction = "person";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/Families2Persons";
        String qvtcasSegment = "Families2Persons.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }


    /**
     * Families2Persons Ballot
     */
    public static Object[] getFamilies2PersonsBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getFamilies2Persons());
    }


    /**
     * Families2Persons ACO
     */
    public static Object[] getFamilies2PersonsACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getFamilies2Persons());
    }


    /**
     * Get the execution arguments for the Hstm2Stm tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getHstm2Stm() {
        String direction = "stm";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/Hstm2Stm";
        String qvtcasSegment = "hstm2stm.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }


    /**
     * Hstm2Stm Ballot
     */
    public static Object[] getHstm2StmBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getHstm2Stm());
    }


    /**
     * Hstm2Stm ACO
     */
    public static Object[] getHstm2StmACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getHstm2Stm());
    }


    /**
     * Get the execution arguments for the Hsv2Hsl tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getHsv2Hsl() {
        String direction = "hsl";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/HSV2HSL";
        String qvtcasSegment = "HSV2HSL.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }


    /**
     * Hsv2Hsl Ballot
     */
    public static Object[] getHsv2HslBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getHsv2Hsl());
    }


    /**
     * Hsv2Hsl ACO
     */
    public static Object[] getHsv2HslACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getHsv2Hsl());
    }


    /**
     * Get the execution arguments for the Upper2Lower tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getUpper2Lower() {
        String direction = "lowerGraph";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/Upper2Lower";
        String qvtcasSegment = "Upper2Lower.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }


    /**
     * Upper2Lower Ballot
     */
    public static Object[] getUpper2LowerBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getUpper2Lower());
    }


    /**
     * Upper2Lower ACO
     */
    public static Object[] getUpper2LowerACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getUpper2Lower());
    }


    /**
     * Get the execution arguments for the Uml2Rdbms tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getUml2Rdbms() {
        String direction = "rdbms";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/UML2RDBMS";
        String qvtcasSegment = "Uml2Rdbms.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }


    /**
     * Uml2Rdbms Ballot
     */
    public static Object[] getUml2RdbmsBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getUml2Rdbms());
    }


    /**
     * Uml2Rdbms ACO
     */
    public static Object[] getUml2RdbmsACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getUml2Rdbms());
    }


    /**
     * Get the execution arguments for the TinyUml2Rdbms tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getTinyUml2Rdbms() {
        String direction = "rdbms";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/TinyUml2Rdbms";
        String qvtcasSegment = "TinyUML2RDBMS.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }


    /**
     * TinyUml2Rdbms Ballot
     */
    public static Object[] getTinyUml2RdbmsBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getTinyUml2Rdbms());
    }


    /**
     * TinyUml2Rdbms ACO
     */
    public static Object[] getTinyUml2RdbmsACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getTinyUml2Rdbms());
    }


    /**
     * Get the execution arguments for the Abstract2Concrete tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getAbstract2Concrete() {
        String direction = "umlOut";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/Abstract2Concrete";
        String qvtcasSegment = "Abstract2Concrete.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }
    /**
     * Abstract2Concrete Ballot
     */
    public static Object[] getAbstract2ConcreteBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getAbstract2Concrete());
    }
    /**
     * Abstract2Concrete ACO
     */
    public static Object[] getAbstract2ConcreteACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getAbstract2Concrete());
    }
    /**
     * Get the execution arguments for the BibTex2DocBook tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getBibTex2DocBook() {
        String direction = "docbook";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/BibTeXML2DocBook";
        String qvtcasSegment = "BibTeXML2DocBook.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }
    /**
     * BibTex2DocBook Ballot
     */
    public static Object[] getBibTex2DocBookBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getBibTex2DocBook());
    }
    /**
     * BibTex2DocBook ACO
     */
    public static Object[] getBibTex2DocBookACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getBibTex2DocBook());
    }
    /**
     * Get the execution arguments for the DNF tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getDNF() {
        String direction = "dnf";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/DNF";
        String qvtcasSegment = "DNF.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }
    /**
     * DNF Ballot
     */
    public static Object[] getDNFBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getDNF());
    }
    /**
     * DNF ACO
     */
    public static Object[] getDNFACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getDNF());
    }
    /**
     * Get the execution arguments for the Mi2Si tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getMi2Si() {
        String direction = "java";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/Mi2Si";
        String qvtcasSegment = "Mi2Si.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }
    /**
     * Mi2Si Ballot
     */
    public static Object[] getMi2SiBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getMi2Si());
    }
    /**
     * Mi2Si ACO
     */
    public static Object[] getMi2SiACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getMi2Si());
    }
    /**
     * Get the execution arguments for the PathExp2PetriNet_Text2Path tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getPathExp2PetriNet_Text2Path() {
        String direction = "path";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/PathExp2PetriNet";
        String qvtcasSegment = "TextualPathExp2PathExp.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }
    /**
     * PathExp2PetriNet_Text2Path Ballot
     */
    public static Object[] getPathExp2PetriNet_Text2PathBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getPathExp2PetriNet_Text2Path());
    }
    /**
     * PathExp2PetriNet_Text2Path ACO
     */
    public static Object[] getPathExp2PetriNet_Text2PathACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getPathExp2PetriNet_Text2Path());
    }
    /**
     * Get the execution arguments for the PathExp2PetriNet_Path2Petri tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getPathExp2PetriNet_Path2Petri() {
        String direction = "petri";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/PathExp2PetriNet";
        String qvtcasSegment = "PathExp2PetriNet.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }
    /**
     * PathExp2PetriNet_Path2Petri Ballot
     */
    public static Object[] getPathExp2PetriNet_Path2PetriBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getPathExp2PetriNet_Path2Petri());
    }
    /**
     * PathExp2PetriNet_Path2Petri ACO
     */
    public static Object[] getPathExp2PetriNet_Path2PetriACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getPathExp2PetriNet_Path2Petri());
    }
    /**
     * Get the execution arguments for the PathExp2PetriNet_Petri2PNML tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getPathExp2PetriNet_Petri2PNML() {
        String direction = "xml";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/PathExp2PetriNet";
        String qvtcasSegment = "PetriNet2XML.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }
    /**
     * PathExp2PetriNet_Petri2PNML Ballot
     */
    public static Object[] getPathExp2PetriNet_Petri2PNMLBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getPathExp2PetriNet_Petri2PNML());
    }
    /**
     * PathExp2PetriNet_Petri2PNML ACO
     */
    public static Object[] getPathExp2PetriNet_Petri2PNMLACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getPathExp2PetriNet_Petri2PNML());
    }
    /**
     * Get the execution arguments for the XSLT2XQuery tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getXSLT2XQuery() {
        String direction = "xq";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/XSLT2XQuery";
        String qvtcasSegment = "XSLT2XQuery.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }
    /**
     * XSLT2XQuery Ballot
     */
    public static Object[] getXSLT2XQueryBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getXSLT2XQuery());
    }
    /**
     * XSLT2XQuery ACO
     */
    public static Object[] getXSLT2XQueryACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getXSLT2XQuery());
    }
    /**
     * Get the execution arguments for the Railway2Control tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] getRailway2Control() {
        String direction = "control";
        String sourcesPath = "uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/Railway2Control";
        String qvtcasSegment = "Railway2Control.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }
    /**
     * Railway2Control Ballot
     */
    public static Object[] getRailway2ControlBallot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, getRailway2Control());
    }
    /**
     * Railway2Control ACO
     */
    public static Object[] getRailway2ControlACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, getRailway2Control());
    }
    /**
     * Concatenate two arrays.
     * @param first
     * @param second
     * @return
     */
    public static <T> T[] concat(T[] first, T[] second) {
          T[] result = Arrays.copyOf(first, first.length + second.length);
          System.arraycopy(second, 0, result, first.length, second.length);
          return result;
        }

    public enum Exploration {
        COMPLETE,
        ACO,
        BALLOT,
        SKIP;
    }

}