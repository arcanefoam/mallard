/*******************************************************************************
 * Copyright (c) 2013, 2015 The University of York, Willink Transformations and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Horacio Hoyos - initial API and implementation
 ******************************************************************************/
package uk.ac.york.qvtd.compiler.tests;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assume.assumeThat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.ocl.pivot.Element;
import org.eclipse.ocl.pivot.Model;
import org.eclipse.ocl.pivot.model.OCLstdlib;
import org.eclipse.ocl.pivot.utilities.LabelUtil;
import org.eclipse.ocl.xtext.base.services.BaseLinkingService;
import org.eclipse.ocl.xtext.base.utilities.ElementUtil;
import org.eclipse.ocl.xtext.basecs.ModelElementCS;
import org.eclipse.qvtd.pivot.qvtcore.QVTcorePivotStandaloneSetup;
import org.eclipse.qvtd.pivot.qvtimperative.QVTimperativePivotStandaloneSetup;
import org.eclipse.qvtd.pivot.qvtimperative.utilities.QVTimperative;
import org.eclipse.qvtd.xtext.qvtbase.tests.LoadTestCase;
import org.eclipse.qvtd.xtext.qvtbase.tests.utilities.TestsXMLUtil;
import org.eclipse.qvtd.xtext.qvtcore.QVTcoreStandaloneSetup;
import org.eclipse.xtext.nodemodel.ICompositeNode;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy;
import uk.ac.york.qvtd.compiler.Configuration;
import uk.ac.york.qvtd.compiler.Configuration.Mode;
import uk.ac.york.qvtd.compiler.MallardCompiler;
import uk.ac.york.qvtd.compiler.tests.MtcTestParameterProvider.Exploration;
import uk.ac.york.qvtd.compiler.utilities.QvtMtcExecutionException;

/**
 * Test the MTC, i.e. that the produced intermediate ASTs are ok with respect to QVT/OCL syntax and type access.
 * @author Horacio Hoyos
 *
 */
@RunWith(Parameterized.class)
public class MallardMtcTests extends LoadTestCase {


    private static Logger logger = (Logger) LoggerFactory.getLogger(MallardMtcTests.class.getName());

    /** The QVTi Context. */
    public QVTimperative myQVT;

    /** The type. */
    private final Exploration type;

    /** The direction. */
    private final String direction;

    /** The sources path. */
    private final String sourcesPath;

    /** The qvtcas segment. */
    private final String qvtcasSegment;

    /** The as listfile. */
    private File asListfile;

    /** The writer. */
    private FileWriter writer;

    /** The string builder. */
    private StringBuilder stringBuilder = new StringBuilder();

    /** The as gen folder. */
    private File asGenFolder;

    /** The qvtcas name. */
    private String qvtcasName;

    /**
     * Instantiates a new mallard mtc test suit. Each of the input parameters is provided by the
     * {@link MtcTestParameterProvider}.
     *
     * @param type the type
     * @param direction the direction
     * @param sourcesPath the sources path
     * @param qvtcasSegment the qvtcas segment
     * @throws SecurityException the security exception
     * @throws IOException Signals that an I/O exception has occurred.
     *
     * @see {@link #data()}
     */
    public MallardMtcTests(Exploration type, String direction, String sourcesPath, String qvtcasSegment)
            throws SecurityException, IOException {

        super();
        this.type = type;
        this.direction = direction;
        this.sourcesPath = sourcesPath;
        this.qvtcasSegment = qvtcasSegment;
        this.qvtcasName = qvtcasSegment.substring(0, qvtcasSegment.lastIndexOf('.'));
        // File to save the list of qvtcas generated
        asListfile = new File("resources/" + type.name() + "/" + qvtcasName + ".param");
        // A folder to place the qvtcas so we don't delete them on build
        asGenFolder = getQvtcasFolder(qvtcasName);
    }

    /**
     * Configure the logger so each test gets a separate log file
     *
     * @param type the type
     * @param qvtcasName the qvtcas name
     * @throws SecurityException the security exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void configureLogger(Exploration type, String qvtcasName) throws SecurityException, IOException {

        // Remove all handlers (i.e. previous executions)
        //Get Root logger
        Logger rootLogger = (Logger)LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        //FIXME This should be a parameter
        rootLogger.setLevel(Level.WARN);
        LoggerContext loggerContext = rootLogger.getLoggerContext();
        loggerContext.reset();
//        RollingFileAppender<ILoggingEvent> fileAppender = (RollingFileAppender<ILoggingEvent>) root.getAppender("FILE");
//        if (fileAppender != null) {
////            RollingPolicy policy = fileAppender.getRollingPolicy();
////            policy.stop();
//            fileAppender.stop();
//            root.detachAppender("FILE");
//        }
        // Log to the /MallardTests/mtc folder, use one log per test
        StringBuilder logFileBuilder = new StringBuilder();
        logFileBuilder.append(System.getenv("HOME"));
        logFileBuilder.append("/MallardTests/mtc/");
        logFileBuilder.append(qvtcasName);
        logFileBuilder.append("_");
        logFileBuilder.append(direction);
        logFileBuilder.append("_");
        logFileBuilder.append(type.toString());

        // FileAppender
        RollingFileAppender<ILoggingEvent> fileAppender = new RollingFileAppender<ILoggingEvent>();
        fileAppender.setName("FILE");
        fileAppender.setContext(loggerContext);
        String fileName = logFileBuilder.toString() + ".log";
        fileAppender.setFile(fileName);
        // Fixed window policy
        FixedWindowRollingPolicy rollPolicy = new FixedWindowRollingPolicy();
        rollPolicy.setContext(loggerContext);
        String fnp = logFileBuilder.toString() + ".%i.log";
        rollPolicy.setFileNamePattern(fnp);
        rollPolicy.setParent(fileAppender);
        rollPolicy.setMinIndex(1);
        rollPolicy.setMaxIndex(10);
        rollPolicy.start();

        SizeBasedTriggeringPolicy<ILoggingEvent> trigPolicy = new SizeBasedTriggeringPolicy<ILoggingEvent>();
        trigPolicy.setMaxFileSize("5MB");
        trigPolicy.start();

        PatternLayoutEncoder ple = new PatternLayoutEncoder();
        ple.setContext(loggerContext);
        ple.setPattern("%d{HH:mm:ss.SSS} [%thread] %-5level %logger{5} - %msg%n");
        ple.start();

        fileAppender.setEncoder(ple);
        fileAppender.setRollingPolicy(rollPolicy);
        fileAppender.setTriggeringPolicy(trigPolicy);
        fileAppender.start();
        // TODO Add a filter to the file so no TRACE are added
        rootLogger.addAppender(fileAppender);
    }

    /**
     * Get the test parameters.
     *
     * @return the iterable
     */
    @Parameters(name = "{index}: {3}, direction={1}, using={0}")
    public static Iterable<Object[]> data() {
        return MtcTestParameterProvider.getAllTestParameters();
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.xtext.qvtbase.tests.LoadTestCase#setUp()
     */
    @Before
    public void setUp() throws Exception {

        BaseLinkingService.DEBUG_RETRY.setState(true);
        super.setUp();
        if (!EcorePlugin.IS_ECLIPSE_RUNNING) {
            File workspaceLoc;
            workspaceLoc = new File(System.getenv("MALLARD"));
            String strlist = System.getenv().get("FOLDERS");
            ArrayList<String> folders = new ArrayList<>(Arrays.asList(strlist.split(",")));
            File[] files = workspaceLoc.listFiles();
            for (File file : files) {
                if (file.isDirectory() && folders.contains(file.getName())) {
                    for (File prj : file.listFiles()) {
                        if (prj.isDirectory()) {
                            String name = prj.getName();
                            EcorePlugin.getPlatformResourceMap().put(name, URI.createFileURI(prj.toString() + "/"));
                        }
                    }
                }
            }
        }
        OCLstdlib.install();
        QVTcoreStandaloneSetup.doSetup();
        QVTcorePivotStandaloneSetup.doSetup();
        QVTimperativePivotStandaloneSetup.doSetup();
        myQVT = createQVT();
    }

    /* (non-Javadoc)
     * @see org.eclipse.qvtd.xtext.qvtbase.tests.LoadTestCase#tearDown()
     */
    @After
    public void tearDown() throws Exception {

        if (stringBuilder.length() > 0) {
            asListfile.getParentFile().mkdir();
            writer = new FileWriter(asListfile.getAbsolutePath());
            writer.append(stringBuilder);
            writer.flush();
            writer.close();
        }
        myQVT.dispose();
        super.tearDown();
    }

    /**
     * Test aco.
     *
     * @throws Exception the exception
     */
    @Test
    public void testAco() throws Exception {
        assumeThat(type, is(Exploration.ACO));
        configureLogger(type, qvtcasName);
        // Delete all files in the output folder
        for(File file: asGenFolder.listFiles())
            file.delete();
        doAcoTest(direction,sourcesPath, qvtcasSegment,  Configuration.Mode.ENFORCE);
    }


    /**
     * Test complete exploration.
     *
     * @throws Exception the exception
     */
    @Test
    public void testCompleteExploration() throws Exception {
        assumeThat(type, is(Exploration.COMPLETE));
        configureLogger(type, qvtcasName);
        // Delete all files in the output folder
        for(File file: asGenFolder.listFiles())
            file.delete();
        doExplorationTest(direction,sourcesPath, qvtcasSegment, Configuration.Mode.ENFORCE);
    }

    /**
     * Test sloppy.
     *
     * @throws Exception the exception
     */
    @Test
    public void testBallot() throws Exception {
        assumeThat(type, is(Exploration.BALLOT));
        configureLogger(type, qvtcasName);
        // Delete all files in the output folder
        for(File file: asGenFolder.listFiles())
            file.delete();
        doBallotTest(direction,sourcesPath, qvtcasSegment, Configuration.Mode.ENFORCE);
    }

    /**
     * Test that all AS models (random sample, max 10% of total samples) generated during compilation are ok.
     *
     * @param direction the direction
     * @param sourcesPath the sources path
     * @param qvtcasSegment the qvtcas segment
     * @param mode the mode
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws InterruptedException the interrupted exception
     */
    private void doAcoTest(String direction, String sourcesPath, String qvtcasSegment, Mode mode)
            throws QvtMtcExecutionException, IOException, InterruptedException {

        MallardCompiler broker = initializeBroker(direction, sourcesPath, qvtcasSegment, mode);
        broker.compileAllAco(true);
        validateAndLog(broker);
        broker.disposeModels();
    }

    /**
     * Test that all AS models (random sample, max 20) generated during compilation using complete
     * exploration are ok.
     *
     * @param direction the direction
     * @param sourcesPath the sources path
     * @param qvtcasSegment the qvtcas segment
     * @param mode the mode
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws InterruptedException the interrupted exception
     */
    private void doExplorationTest(String direction, String sourcesPath, String qvtcasSegment, Mode mode)
            throws QvtMtcExecutionException, IOException, InterruptedException {

        MallardCompiler broker = initializeBroker(direction, sourcesPath, qvtcasSegment, mode);
        broker.compileAllSolutions(true);
        validateAndLog(broker);
        broker.disposeModels();
    }

    /**
     * Test Sloppy construction.
     *
     * @param direction the direction
     * @param sourcesPath the sources path
     * @param qvtcasSegment the qvtcas segment
     * @param mode the mode
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws InterruptedException the interrupted exception
     */
    private void doBallotTest(String direction, String sourcesPath, String qvtcasSegment, Mode mode)
            throws QvtMtcExecutionException, IOException, InterruptedException {

        MallardCompiler broker = initializeBroker(direction, sourcesPath, qvtcasSegment, mode);
        broker.compileBallot(true);
        validateAndLog(broker);
        broker.disposeModels();
    }

    /**
     * Gets the qvtcas folder.
     *
     * @param qvtcasName the qvtcas name
     * @return the qvtcas folder
     */
    private File getQvtcasFolder(String qvtcasName) {

        File theDir = new File("mtc-gen/" + type.name() + "/" + qvtcasName);
        // if the directory does not exist, create it
        if (!theDir.exists()) {
            boolean result = theDir.mkdirs();
            if (!result) {
                throw new IllegalArgumentException(String.format("The folder for the %s can't be created.", qvtcasName));
            }
        }
        else if (!theDir.isDirectory()){
            throw new IllegalArgumentException(String.format("The folder for the %s can't be created because a file"
                    + " with the same name exists.", qvtcasName));
        }
        return theDir;

    }

    /**
     * Initialize broker.
     *
     * @param direction the direction
     * @param sourcesPath the sources path
     * @param qvtcasSegment the qvtcas segment
     * @param mode the mode
     * @return the mallard compiler
     * @throws QvtMtcExecutionException the qvt mtc execution exception
     */
    private MallardCompiler initializeBroker(String direction, String sourcesPath, String qvtcasSegment, Mode mode)
            throws QvtMtcExecutionException {

        // The paths include the plugin folder (e.g. tests) so they work for the benchmark too, we need to remove it
//        String[] x = sourcesPath.split("/");
//        sourcesPath = String.join("/", Arrays.copyOfRange(x, 1, x.length));
        URI sourcesUri = URI.createPlatformResourceURI(sourcesPath, true);
        URI qvtcasUri = sourcesUri.appendSegment(qvtcasSegment);
        URI genAstUri = URI.createFileURI(asGenFolder.getAbsolutePath());
        @NonNull Map<Object, Object> savingOptions = TestsXMLUtil.defaultSavingOptions;
        savingOptions.put(XMIResource.OPTION_SCHEMA_LOCATION, true);
        MallardCompiler broker = new MallardCompiler(qvtcasUri, genAstUri,
                myQVT.getEnvironmentFactory(), savingOptions);
        broker.initialConfiguration(mode, direction);
        return broker;
    }

    /**
     * Validate and log.
     *
     * @param broker the broker
     */
    private void validateAndLog(MallardCompiler broker) {

        assertNoValidationErrors("QVTu validation", broker.getQvtuAsModel());
        // Make sure we don't have references to previous models in the chain
        Map<EObject, Collection<Setting>> xrefs = EcoreUtil.ExternalCrossReferencer.find(broker.getQvtuAsModel());
        assertFalse("QVTu model has dangling references to QVTc Model",
                xrefs.keySet().stream()
                .anyMatch(eObject -> eObject.eResource().equals(broker.getQvtcAsModel())));

        assertNoValidationErrors("QVTm validation", broker.getQvtmAsModel());
        xrefs = EcoreUtil.ExternalCrossReferencer.find(broker.getQvtmAsModel());
        assertFalse("QVTm model has dangling references to QVTu Model",
                xrefs.keySet().stream()
                .anyMatch(eObject -> eObject.eResource().equals(broker.getQvtuAsModel())));

        assertNoValidationErrors("QVTp validation", broker.getQvtpAsModel());
        xrefs = EcoreUtil.ExternalCrossReferencer.find(broker.getQvtpAsModel());
        assertFalse("QVTp model has dangling references to QVTm Model",
                xrefs.keySet().stream()
                .anyMatch(eObject -> eObject.eResource().equals(broker.getQvtmAsModel())));

        for (Resource iAsModel : broker.getQvtiAsModels()) {
            assertNoValidationErrors("QVTi validation", iAsModel);
            xrefs = EcoreUtil.ExternalCrossReferencer.find(iAsModel);
            assertFalse(String.format("QVTi model %s has dangling references to QVTm Model", iAsModel),
                    xrefs.keySet().stream()
                    .anyMatch(eObject -> eObject.eResource().equals(broker.getQvtpAsModel())));
            stringBuilder.append(iAsModel.getURI().toString());
            stringBuilder.append('\n');
        }
    }

    /**
     * Creates the QVT.
     *
     * @return the QV timperative
     */
    protected @NonNull QVTimperative createQVT() {
        return QVTimperative.newInstance(getProjectMap(), null);
    }

    /**
     * Assert no validation errors.
     *
     * @param string the string
     * @param resource the resource
     */
    public static void assertNoValidationErrors(String string, Resource resource) {
        for (EObject eObject : resource.getContents()) {
            assertNoValidationErrors(string, eObject);
        }
    }

    /**
     * Assert no validation errors.
     *
     * @param string the string
     * @param eObject the e object
     */
    public static void assertNoValidationErrors(String string, EObject eObject) {
        Map<Object, Object> validationContext = LabelUtil.createDefaultContext(Diagnostician.INSTANCE);
        Diagnostic diagnostic = Diagnostician.INSTANCE.validate(eObject, validationContext);
        List<Diagnostic> children = diagnostic.getChildren();
        if (children.size() <= 0) {
            return;
        }

        // Log Warnings and below, fail on the rest
        List<Diagnostic> warnings = children.stream()
                .filter(d -> d.getSeverity() <= Diagnostic.WARNING)
                .collect(Collectors.toList());
        for(Diagnostic warn : warnings) {
            String info = "";
            if (warn.getData().size() > 0) {
                Object data = warn.getData().get(0);
                info = getDataRepresentation(data);
            }
            String uri = ((Model)eObject).getExternalURI();
            String modelName = uri.substring(uri.lastIndexOf("/")+1);
            String logString = String.format("Validation of %s: %s -> %s",
                    modelName,
                    info,
                    warn.getMessage());
            switch(warn.getSeverity()) {
            case Diagnostic.INFO:
                logger.info(logString);
                break;
            case Diagnostic.WARNING:
                logger.warn(logString);
                break;
            }
        }
        List<Diagnostic> errors = children.stream()
                .filter(d -> d.getSeverity() > Diagnostic.WARNING)
                .collect(Collectors.toList());
        if (errors.size() > 0) {
            StringBuilder s = new StringBuilder();
            s.append(children.size() + " validation errors");
            for (Diagnostic child : children){
                s.append("\n\t");
                if (child.getData().size() > 0) {
                    Object data = child.getData().get(0);
                    s.append(getDataRepresentation(data));
                }
                s.append(child.getMessage());
            }
            fail(s.toString());
        }

    }

    /**
     * Gets the data representation.
     *
     * @param data the data
     * @return the data representation
     */
    private static String getDataRepresentation(Object data) {

        StringBuilder s = new StringBuilder();
        if (data instanceof Element) {
            for (EObject eScope = (Element)data; eScope instanceof Element; eScope = eScope.eContainer()) {
                ModelElementCS csElement = ElementUtil.getCsElement((Element)eScope);
                if (csElement != null) {
                    ICompositeNode node = NodeModelUtils.getNode(csElement);
                    if (node != null) {
                        Resource eResource = csElement.eResource();
                        if (eResource != null) {
                            s.append(eResource.getURI().lastSegment() + ":");
                        }
                        int startLine = node.getStartLine();
                        s.append(startLine + ":");
                    }
                    s.append(((Element)data).eClass().getName() + ": ");
                    break;
                }
            }
        }
        return s.toString();
    }

//    public void executeChaos(String direction, String sourcesPath, String qvtcasSegment, String samplesPath,
//            String expectedSegment, String inputSegment, String outputSegment, String traceSegment, Mode mode)
//            throws QvtMtcExecutionException, IOException, InterruptedException {
//
//        // The paths from the benchmark include the plugin folder (e.g. tests) we need to remove it
//        String[] x = sourcesPath.split("/");
//        sourcesPath = String.join("/", Arrays.copyOfRange(x, 1, x.length));
//        x = samplesPath.split("/");
//        samplesPath = String.join("/", Arrays.copyOfRange(x, 1, x.length));
//
//        URI sourcesUri = URI.createPlatformResourceURI(sourcesPath, true);
//        URI qvtcasUri = sourcesUri.appendSegment(qvtcasSegment);
//        String genPath = MallardMtcUtil.changeTargetToBinFolder(sourcesPath);
//        URI genAstUri = URI.createPlatformPluginURI(genPath, true);
//        URI modelsUri = URI.createPlatformResourceURI(samplesPath, true);
//        URI expectedOutputURI = modelsUri.appendSegment(expectedSegment);
//        genPath = MallardMtcUtil.changeTargetToBinFolder(modelsUri.toString());
//        URI genModelsUri = URI.createURI(genPath, true);
//        @NonNull List<@NonNull URI> inputModelUris = new ArrayList<>();
//        inputModelUris.add(modelsUri.appendSegment(inputSegment));
//        // The generated models go to the output
//        URI outputModelURI = genModelsUri.appendSegment(outputSegment);
//        URI middleModelURI = genModelsUri.appendSegment(traceSegment);
//
//        // The MallardBroker is responsible for loading the models, creating the configuration,
//        // executing the mtc and making sure everything is ready for execution
//
//        @NonNull Map<Object, Object> savingOptions = TestsXMLUtil.defaultSavingOptions;
//        savingOptions.put(XMIResource.OPTION_SCHEMA_LOCATION, true);
//        MallardCompiler broker = new MallardCompiler(qvtcasUri, genAstUri,
//                myQVT.getEnvironmentFactory(), savingOptions);
//
//        broker.configure(mode, direction, inputModelUris, middleModelURI, outputModelURI);
//        broker.compile(verbose);
//        assertNoValidationErrors("QVTu validation", broker.getQvtuAsModel());
//        assertNoValidationErrors("QVTm validation", broker.getQvtmAsModel());
//        assertNoValidationErrors("QVTp validation", broker.getQvtpAsModel());
//        assertNoValidationErrors("QVTi validation", broker.getQvtiAsModel());
//        boolean success ;
//        MallardEngine engine = new MallardEngine();
//        success = engine.execute(broker, savingOptions, false);
//        assertTrue(success);
//        assertTrue(success);
//        Resource expected =  myQVT.getEnvironmentFactory().getResourceSet().getResource(expectedOutputURI, true);
//        Resource actual =  myQVT.getEnvironmentFactory().getResourceSet().getResource(outputModelURI, true);
//        assertSameModel(expected, actual);
//        broker.disposeModels();
//    }



}
