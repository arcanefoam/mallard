package uk.ac.york.qvtd.compiler.tests.gen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;


public class GenerateParameterProvider {

    public static void main(String[] args) {
        /* ------------------------------------------------------------------------ */
        /* You should do this ONLY ONCE in the whole application life-cycle:        */

        /* Create and adjust the configuration singleton */
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);
        try {
            cfg.setDirectoryForTemplateLoading(new File("src/uk/ac/york/qvtd/compiler/tests/gen"));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);

        /* ------------------------------------------------------------------------ */
        /* You usually do these for MULTIPLE TIMES in the application life-cycle:   */

        /* Create a data-model */
        byte[] encoded = null;
        try {
            encoded = Files.readAllBytes(Paths.get("src/uk/ac/york/qvtd/compiler/tests/gen/tests.json"));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally {
            if (encoded != null) {
                String son = new String(encoded, StandardCharsets.ISO_8859_1);
                Type mapType = new TypeToken<Map<String, Object>>(){}.getType();
                Map<String, Object> dataModel = new Gson().fromJson(son, mapType);
                /* Get the template (uses cache internally) */
                Template temp = null;
                try {
                    temp = cfg.getTemplate("MtcTestParameterProvider.ftlh");
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                /* Merge data-model with template */
                PrintWriter writer = null;
                try {
                    writer = new PrintWriter("src/uk/ac/york/qvtd/compiler/tests/MtcTestParameterProvider.java",
                            StandardCharsets.ISO_8859_1.name());
                } catch (FileNotFoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                } catch (UnsupportedEncodingException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                //Writer out = new OutputStreamWriter(System.out);
                try {
                    temp.process(dataModel, writer);
                } catch (TemplateException | IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // Note: Depending on what `out` is, you may need to call `out.close()`.
                // This is usually the case for file output, but not for servlet output.
                writer.close();

            }
        }
        System.out.println("Class Generated!");
    }
}
