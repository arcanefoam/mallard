package uk.ac.york.qvtd.compiler.tests;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Provide the parameters needed for the MTC tests
 * THIS FILE IS AUTO GENERATED, DO NOT EDIT IT!
 *
 * @author Horacio Hoyos
 *
 */
public class MtcTestParameterProvider {

    /** The Number of times each test is invoked. */
    private static final int NUM_TRIALS = ${num_trials};

    /**
     * Get the test parameters. For each test, the array must contain the {@link MallardMtcTests} constructor
     * parameters in order.
     * @return a list with the information for each of the tests.
     */
    public static Iterable<Object[]> getAllTestParameters() {
        ArrayList<Object[]> tests = new ArrayList<Object[]>();
        // START DEVELOP EXAMPLES
<#list develop as test>
<#if test.type[0]>
        tests.add(get${test.name}Complete());
</#if>
<#if test.type[1]>
        tests.add(get${test.name}Ballot());
</#if>
<#if test.type[2]>
        tests.add(get${test.name}ACO());
</#if>
</#list>
        // END DEVELOP EXAMPLES
        // START VALIDATION EXAMPLES
<#list validate as test>
<#if test.type[0]>
        tests.add(get${test.name}Complete());
</#if>
<#if test.type[1]>
        tests.add(get${test.name}Ballot());
</#if>
<#if test.type[2]>
        tests.add(get${test.name}ACO());
</#if>
</#list>
        // END VALIDATION EXAMPLES
        ArrayList<Object[]> result = new ArrayList<Object[]>();
        for (int i = 0; i<NUM_TRIALS; i++) {
            result.addAll(tests);
        }
        return result;
    }


<#list develop as test>
    /**
     * Get the execution arguments for the ${test.name} tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] get${test.name}() {
        String direction = "${test.direction}";
        String sourcesPath = "${test.path}";
        String qvtcasSegment = "${test.qvtcFile}.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }


        <#if test.type[0]>
    /**
     * ${test.name} Complete Exploration
     */
    private static Object[] get${test.name}Complete() {
        Object[] type = {Exploration.COMPLETE};
        return concat(type, get${test.name}());
    }


        </#if>
        <#if test.type[1]>
    /**
     * ${test.name} Ballot
     */
    private static Object[] get${test.name}Ballot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, get${test.name}());
    }


        </#if>
        <#if test.type[2]>
    /**
     * ${test.name} ACO
     */
    private static Object[] get${test.name}ACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, get${test.name}());
    }


        </#if>
</#list>
<#list validate as test>
    /**
     * Get the execution arguments for the ${test.name} tests.
     *
     * @return the arguments to run the benchmark
     */
    private static Object[] get${test.name}() {
        String direction = "${test.direction}";
        String sourcesPath = "${test.path}";
        String qvtcasSegment = "${test.qvtcFile}.qvtc";
        Object[] ret = {direction, sourcesPath, qvtcasSegment};
        return ret;
    }
        <#if test.type[0]>
    /**
     * ${test.name} Complete Exploration
     */
    private static Object[] get${test.name}Complete() {
        Object[] type = {Exploration.COMPLETE};
        return concat(type, get${test.name}());
    }
        </#if>
        <#if test.type[1]>
    /**
     * ${test.name} Ballot
     */
    private static Object[] get${test.name}Ballot() {
        Object[] type = {Exploration.BALLOT};
        return concat(type, get${test.name}());
    }
        </#if>
        <#if test.type[2]>
    /**
     * ${test.name} ACO
     */
    private static Object[] get${test.name}ACO() {
        Object[] type = {Exploration.ACO};
        return concat(type, get${test.name}());
    }
        </#if>
</#list>
    /**
     * Concatenate two arrays.
     * @param first
     * @param second
     * @return
     */
    private static <T> T[] concat(T[] first, T[] second) {
          T[] result = Arrays.copyOf(first, first.length + second.length);
          System.arraycopy(second, 0, result, first.length, second.length);
          return result;
        }

    public enum Exploration {
        COMPLETE,
        ACO,
        BALLOT,
        SKIP;
    }

}