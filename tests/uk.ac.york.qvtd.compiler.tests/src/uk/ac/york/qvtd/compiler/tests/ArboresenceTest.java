package uk.ac.york.qvtd.compiler.tests;

import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.jgrapht.WeightedGraph;
import org.jgrapht.alg.KosarajuStrongConnectivityInspector;
import org.jgrapht.ext.ComponentAttributeProvider;
import org.jgrapht.ext.DOTExporter;
import org.jgrapht.ext.IntegerNameProvider;
import org.jgrapht.ext.StringNameProvider;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.DirectedWeightedPseudograph;
import org.jgrapht.graph.DirectedWeightedSubgraph;
import org.junit.Test;

import uk.ac.york.qvtd.dependencies.derivations.impl.EdmondsDirectedMinimumArborescence;

public class ArboresenceTest {

    /**
     * Based on the example at https://www.google.co.uk/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0ahUKEwjWrebh7vzNAhWG1hoKHZIFAqQQFggcMAA&url=http%3A%2F%2Fwww.iitg.ernet.in%2Frinkulu%2Fcombopt%2Fslides%2Farbormincost.pdf&usg=AFQjCNFiWXgouXBSHW96_jtc8EDubXgkMQ&sig2=xSwdmFqdmKorE7bwEmzyhQ&bvm=bv.127178174,d.d2s&cad=rja
     */
    @Test
    public void test() {
        DirectedWeightedPseudograph<String, DefaultWeightedEdge> g = new DirectedWeightedPseudograph<>(DefaultWeightedEdge.class);
        g.addVertex("a");
        g.addVertex("b");
        g.addVertex("c");
        g.addVertex("d");
        g.addVertex("f");
        g.addVertex("r");
        DefaultWeightedEdge e = g.addEdge("r", "a");
        g.setEdgeWeight(e, 10);
        e = g.addEdge("r", "b");
        g.setEdgeWeight(e, 2);
        e = g.addEdge("r", "c");
        g.setEdgeWeight(e, 10);
        e = g.addEdge("a", "b");
        g.setEdgeWeight(e, 1);
        e = g.addEdge("b", "c");
        g.setEdgeWeight(e, 4);
        e = g.addEdge("d", "a");
        g.setEdgeWeight(e, 2);
        e = g.addEdge("c", "d");
        g.setEdgeWeight(e, 2);
        e = g.addEdge("c", "f");
        g.setEdgeWeight(e, 4);
        e = g.addEdge("a", "f");
        g.setEdgeWeight(e, 8);
        // Fully connect
        //e = g.addEdge("f", "r");
        //g.setEdgeWeight(e, 100);
        KosarajuStrongConnectivityInspector<String, DefaultWeightedEdge> inspect = new KosarajuStrongConnectivityInspector<String, DefaultWeightedEdge>(g);
        if (!inspect.isStronglyConnected()) {
            do {
                // Pick two sets and add an edge
                Set<String> s1 = inspect.stronglyConnectedSets().get(0);
                Set<String> s2 = inspect.stronglyConnectedSets().get(1);
                // Find an edge from an element in s1 to an element in s2, add a reverse edge
                String vs = s1.iterator().next();
                for (String vt : s2) {
                    e = g.getEdge(vs, vt);
                    if (e != null) {
                        e = g.addEdge(vt, vs);
                        g.setEdgeWeight(e, EdmondsDirectedMinimumArborescence.DUMMY_EDGE_COST);
                        break;
                    }
                }
                inspect = new KosarajuStrongConnectivityInspector<String, DefaultWeightedEdge>(g);
            } while (!inspect.isStronglyConnected());
        }

        EdmondsDirectedMinimumArborescence<String, DefaultWeightedEdge> ed = new EdmondsDirectedMinimumArborescence<>(g);
        System.out.println(ed.toDOT());
        for (String v : g.vertexSet()) {
            System.out.println("MDST from " + v);
            DirectedWeightedSubgraph<String, DefaultWeightedEdge> mdst = ed.getMinimumSpanningTree(v);
            exportGraph(mdst);
            System.out.println("Cost: " + mdst.edgeSet().stream().mapToDouble(we -> mdst.getEdgeWeight(we)).sum());
        }

    }

    static public void exportGraph(WeightedGraph<String, DefaultWeightedEdge> g){
        PrintWriter writer = new PrintWriter(System.out);
        IntegerNameProvider<String> p1=new IntegerNameProvider<String>();
        StringNameProvider<String> p2=new StringNameProvider<String>();
        ComponentAttributeProvider<DefaultWeightedEdge> p4 =
           new ComponentAttributeProvider<DefaultWeightedEdge>() {
                public Map<String, String> getComponentAttributes(DefaultWeightedEdge e) {
                    Map<String, String> map =new LinkedHashMap<String, String>();
                    map.put("weight", Double.toString(g.getEdgeWeight(e)));
                    return map;
                }
           };
        DOTExporter<String, DefaultWeightedEdge> export=new DOTExporter<String, DefaultWeightedEdge>(p1, p2, null, null, p4);
        export.export(writer, g);
    }

}
