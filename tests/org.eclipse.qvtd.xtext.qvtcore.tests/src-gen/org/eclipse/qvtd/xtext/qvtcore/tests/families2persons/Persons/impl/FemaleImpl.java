/**
 * Copyright (c) 2015, 2016 Willink Transformations and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *   E.D.Willink - Initial API and implementation
 */
package org.eclipse.qvtd.xtext.qvtcore.tests.families2persons.Persons.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.qvtd.xtext.qvtcore.tests.families2persons.Persons.Female;
import org.eclipse.qvtd.xtext.qvtcore.tests.families2persons.Persons.PersonsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Female</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FemaleImpl extends PersonImpl implements Female {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FemaleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PersonsPackage.Literals.FEMALE;
	}


} //FemaleImpl
