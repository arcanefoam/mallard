package PathExp2PetriNet;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;


public class TextPathGen {

	 private static final String IN_METAMODEL_PATH = "examples\\uk.ac.york.qvtd.examples.qvtcore\\qvtcsrc\\PathExp2PetriNet\\TextualPathExp.ecore";

	private static EFactory factory;

    private static ResourceSetImpl resourceSet;

    private static EObject ep;

	/** The generator. */
    private final RandomDataGenerator generator = new RandomDataGenerator();

	private char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

	private final Integer MAX_ALT_PATHS = 3;	// This can be a parameter too

	private EClass ptEClass;

	private EClass atEClass;


	private EClass pEClass;

	private EClass tpeEClass;

	private int size;


	/**
	 * Use a specific seed
	 */
	public TextPathGen(long seed) {
		generator.reSeed(seed);
	}


	private EObject createAlternativeTransition(int total) {
		size++;
		if (atEClass == null) {
			atEClass = getEClass("AlternativeTransition");
		}
		EObject  t = factory.create(atEClass);
		// Can have 2-3 paths
		EStructuralFeature atlPathsSF = atEClass.getEStructuralFeature("altPaths");
		int maxPaths = total > MAX_ALT_PATHS ? MAX_ALT_PATHS : total;
		int numPaths = generator.nextInt(2,  maxPaths);
		for (int i=0; i<numPaths; i++) {
			Object paths = t.eGet(atlPathsSF);
			if (paths instanceof EList) {
				((EList)paths).add(createPath());
			}
		}
		return t;
	}


	private EObject createPath() {
		if (pEClass == null) {
			pEClass = getEClass("Path");
		}
		size++;
		EObject p = factory.create(pEClass);
		return p;
	}


	private void createPaths(EObject rootPath, int total) {
		atEClass = getEClass("AlternativeTrans");
		pEClass = getEClass("Path");
		ptEClass = getEClass("PrimitiveTrans");
		tpeEClass = getEClass("TextualPathExp");
		EStructuralFeature pathSF = tpeEClass.getEStructuralFeature("path");
		EStructuralFeature transSF = pEClass.getEStructuralFeature("transitions");
		EStructuralFeature atlPathsSF = atEClass.getEStructuralFeature("altPaths");

		Deque<EObject> paths = new ArrayDeque<EObject>();
		paths.push((EObject) rootPath.eGet(pathSF));
		EObject currentPath;
		while(!paths.isEmpty()) {
			currentPath = (EObject) paths.pop();
			// Paths always end in a PrimitiveTransition
			EList trans = (EList) currentPath.eGet(transSF);
			if (trans.isEmpty()) {
				trans.add(createPrimitiveTransition(nextString(20)));
				total --;
			}
			if (total <= 0) {
				continue;
			}
			// The next transition is either Prim or Alternative
			if (total >= 3) {	// For an alternative we would like at least 2 paths, which means we need at least 2 instances left
				if (generator.getRandomGenerator().nextBoolean()) {
					EObject at = createAlternativeTransition(total);
					EList atPaths = (EList) at.eGet(atlPathsSF);
					total -= atPaths.size() + 1;
					trans.add(0, at);
					trans.add(0, createPrimitiveTransition(nextString(20)));
					for (Object p : atPaths) {
						paths.push((EObject) p);
					}
					continue;
				}
			}
			// else {
			// We can only create Primitives
			trans.add(0, createPrimitiveTransition(nextString(20), total>1));
			total--;
			paths.push(currentPath);
		}

	}

	private EObject createPrimitiveTransition(String name) {
		return createPrimitiveTransition(name, false);
	}

	private EObject createPrimitiveTransition(String name, boolean multiple) {
		if (ptEClass == null) {
			ptEClass = getEClass("PrimitiveTransition");
		}
		EObject  t = factory.create(ptEClass);
		size++;
		EStructuralFeature nameSF = ptEClass.getEStructuralFeature("name");
		t.eSet(nameSF, name);
		if (multiple) {
			EStructuralFeature multSF = ptEClass.getEStructuralFeature("isMultiple");
			t.eSet(multSF, generator.getRandomGenerator().nextBoolean());
		}
		return t;
	}


	private EObject createTextualPathExp(String name) {
		if (tpeEClass == null) {
			tpeEClass = getEClass("TextualPathExp");
		}
		EObject exp = factory.create(tpeEClass);
		size++;
		EStructuralFeature nameSF = tpeEClass.getEStructuralFeature("name");
		exp.eSet(nameSF, name);
		EObject p = createPath();
		EStructuralFeature pathSF = tpeEClass.getEStructuralFeature("path");
		exp.eSet(pathSF, p);
		return exp;
	}


	public void generate(int total, String path) {
		size = 0;
		// Load the TextualPathExpPackage, so schema location points to the Ecore
		resourceSet = new ResourceSetImpl();
		// Register input metamodel
		String mallard_root = System.getenv("MALLARD");
        URI mmUri = URI.createFileURI(mallard_root + IN_METAMODEL_PATH);
        Resource mm = resourceSet.getResource(mmUri, true);
        ep = mm.getContents().get(0);
        if (ep instanceof EPackage) {
        	factory = ((EPackage)ep).getEFactoryInstance();
        }

		// Register factory
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		URI uri = URI.createURI("file:/" + path);
		Resource model = resourceSet.createResource(uri);

		// 1. Create the TextualPathExp and randomly assign the size
		int n = total/10;
		Map<EObject, Integer> textPathExps;
		if (n > 1) {
			textPathExps = nextAddTo(n, total).stream()
					.collect(Collectors.toMap(i -> createTextualPathExp(nextString(20)), i -> i));
		}
		else {
			textPathExps = new HashMap<>(1);
			textPathExps.put(createTextualPathExp(nextString(20)), total);
		}
		// 2. Create the structure of each TextualPathExp
		for (Entry<EObject, Integer> entry : textPathExps.entrySet()) {
				model.getContents().add(entry.getKey());
				createPaths(entry.getKey(), entry.getValue());
		}

		// 3. Save
		HashMap<Object, Object> savingOptions = new HashMap<Object, Object>();
		savingOptions.put(XMLResource.OPTION_ENCODING, "UTF-8");
		savingOptions.put(XMLResource.OPTION_LINE_DELIMITER, "\n");
		savingOptions.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
		savingOptions.put(XMLResource.OPTION_SCHEMA_LOCATION_IMPLEMENTATION, Boolean.TRUE);
		savingOptions.put(XMLResource.OPTION_LINE_WIDTH, Integer.valueOf(132));
		try {
			model.save(savingOptions);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Final size: " + size);
		resourceSet.getResources().clear();
		resourceSet = null;
	}


	private EClass getEClass(String name) {
		for (EObject eo : ep.eContents()) {
			if (eo instanceof EClass) {
				EClass ec = (EClass)eo;
				if (ec.getName().equals(name)) {
					return ec;
				}
			}
		}
		return null;
	}


	private List<Integer> nextAddTo(int n, int m) {
        assert n > 1;
        int len = n-1;
        int[] index = generator.nextPermutation(m, len);
        List<Integer> values = new ArrayList<>();
        for (int i = 0; i < len; i++) {
            values.add(index[i]);
        }
        values.add(0, 0);
        values.add(m);
        Collections.sort(values);
        List<Integer> result = new ArrayList<>();
        ListIterator<Integer> it = values.listIterator(1);
        while (it.hasNext()) {
            int low = it.previous();
            it.next();
            int high = it.next();
            result.add(high-low);
        }
        return result;
    }




	private String nextString(int length) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < length; i++) {
            sb.append(chars[generator.nextInt(0, chars.length-1)]);
        }
        return sb.toString();
    }

	/**
	 *
	 */
	public static void registerEPackages() {
		// Register Ecore
		EcorePackage ecore = EcorePackage.eINSTANCE;
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());
        Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
	}

}
