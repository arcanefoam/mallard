package uk.ac.york.qvtd.tests.model.generator;

import java.net.URL;
import java.util.ArrayList;

import org.apache.commons.math3.random.RandomDataGenerator;
import org.eclipse.epsilon.emc.emf.EmfModel;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;

/**
 * The Class ModelGenerator.
 */
public class ModelGenerator {

    /**
     * Use the Serial Version ID as seed
     */
    private static final long serialVersionUID = 4449535323017681626L;

    /** We need a ENV VAR with the mallard git root. */
    public static String examplesRootPath = "examples/uk.ac.york.qvtd.examples.qvtcore/qvtcsrc/";

    /** The gen model root path. */
    public static String genModelRootPath = "tests/uk.ac.york.qvtd.tests.model.generator/model/";

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {


        // General Schema Ready, now parametrise to generate 30 small models, and the 100, 1k, 10k, and 100k ones.
        // Then move to a freemarker template to generate for the test cases
        // Need to work on the benchmark code next, to run the ballot and all ACO samples against the models, compare results and times.
        RandomDataGenerator rnd = new RandomDataGenerator();
        rnd.reSeed(serialVersionUID);
        EmgEngine engine = new EmgEngine();
        for (Object[] data : getAllGenParameters()) {
            // Get the EMG script
            String testBase = (String) data[0];
            String emgPath = "/" + testBase + data[1];
            String mallard = System.getenv("MALLARD");
            String mmpath = mallard + examplesRootPath + testBase + data[2];
            System.out.println(mmpath);
            String mbase = (String) data[3];
            if (mbase.contains("/")) {
                mbase =  mallard + genModelRootPath + data[3] + "%1$s.xmi";
            }
            else {
                mbase =  mallard + genModelRootPath + testBase + data[3] + "%1$s.xmi";
            }
            int modelsize = Integer.valueOf((String) data[4]);
            // 1. Create 30 small sized models
            for (int i=0; i<30; i++) {
                String mpath = String.format(mbase, "c"+i);
                System.out.println(mpath);
                executeGeneration(engine, emgPath, mmpath, mpath, modelsize, rnd.nextLong(0, Long.MAX_VALUE));
            }
            // 2. Create performance models
            String[] performanceModels = (String[]) data[5];
            for (String model : performanceModels) {
            	String[] modelInfo = model.split(",");
            	String mpath = String.format(mbase, modelInfo[0]);
                System.out.println(mpath);
                modelsize = Integer.valueOf(modelInfo[1]);
                executeGeneration(engine, emgPath, mmpath, mpath, modelsize, rnd.nextLong(0, Long.MAX_VALUE));
            }
        }

    }

    /**
     * Gets the all gen parameters.
     *
     * @return the all gen parameters
     */
    public static Iterable<Object[]> getAllGenParameters() {
        ArrayList<Object[]> result = new ArrayList<Object[]>();
//        result.add(getAbstractToConcrete());
//        result.add(getBibTeXML2DocBook());
//        result.add(getDNF());
//        result.add(getMi2Si());
//        result.add(getPathExp2PetriNet_PathExp());
//        result.add(getPathExp2PetriNet_PetriNet());
        result.add(getRailway2Control());

        return result;
    }

    /**
     * Execute generation.
     *
     * @param engine the engine
     * @param emgPath the emg path
     * @param mmpath the mmpath
     * @param mpath the mpath
     * @param modelsize the modelsize
     * @param seed
     */
    private static void executeGeneration(EmgEngine engine, String emgPath, String mmpath, String mpath,
            int modelsize, long seed) {
        engine.setSeed(seed);
        URL r = ModelGenerator.class.getResource(emgPath);
        engine.setSourceURI(java.net.URI.create(r.toString()));
        EmfModel genmodel = new EmfModel();
        genmodel.setName("gen");
        genmodel.setMetamodelFile(mmpath);
        genmodel.setModelFile(mpath);
        genmodel.setExpand(true);
        genmodel.setCachingEnabled(true);
        genmodel.setReadOnLoad(false);
        genmodel.setStoredOnDisposal(true);
        try {
            genmodel.loadModelFromUri();
        } catch (EolModelLoadingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        genmodel.setupContainmentChangeListeners();
        engine.addModel(genmodel);
        engine.addGlobalVariable("total", modelsize);
        try {
            engine.execute();
        } catch (EpsilonException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Final size: " + engine.getResult());
        engine.disposeModels();
        engine.reset();
    }

    /**
     * Get the execution arguments for the Abstract2Concrete tests.
     *
     * @return the arguments to generate the models
     */
    private static Object[] getAbstractToConcrete() {
    	String testBase = "Abstract2Concrete/";
    	String emgFile = "Abstract.emg";
    	String metamodel = "ClassMM.ecore";
    	String modelNameBase = "umlIn";
    	String validationModelSize = "15";
    	String tiny = "Tiny,70";
    	String small = "Small,340";
    	String normal = "Normal,1800";
    	String large = "Large,9300";
    	String huge = "Huge,49000";
    	String[] performanceModels = {tiny, small, normal, large, huge};
    	Object[] ret = {testBase, emgFile, metamodel, modelNameBase, validationModelSize, performanceModels};
        return ret;
    }

    /**
     * Get the execution arguments for the BibTeXML2DocBook tests.
     *
     * @return the arguments to generate the models
     */
    private static Object[] getBibTeXML2DocBook() {
        String testBase = "BibTeXML2DocBook/";
        String emgFile = "BibTeXML.emg";
        String metamodel = "BibTeX.ecore";
        String modelNameBase = "bibtex";
        String validationModelSize = "10";
    	String tiny = "Tiny,10";
    	String small = "Small,15";
    	String normal = "Normal,60";
    	String large = "Large,140";
    	String huge = "Huge,300";
    	String[] performanceModels = {tiny, small, normal, large, huge};
    	Object[] ret = {testBase, emgFile, metamodel, modelNameBase, validationModelSize, performanceModels};
        return ret;
    }

    /**
     * Get the execution arguments for the DNF tests.
     *
     * @return the arguments to generate the models
     */
    private static Object[] getDNF() {
        String testBase = "DNF/";
        String emgFile = "DNF.emg";
        String metamodel = "DNFMM.ecore";
        String modelNameBase = "boolexp";
        String validationModelSize = "10";
    	String tiny = "Tiny,15";
    	String small = "Small,25";
    	String normal = "Normal,100";
    	String large = "Large,180";
    	String huge = "Huge,250";
    	String[] performanceModels = {tiny, small, normal, large, huge};
    	Object[] ret = {testBase, emgFile, metamodel, modelNameBase, validationModelSize, performanceModels};
        return ret;
    }

    /**
     * Get the execution arguments for the Mi2Si tests.
     *
     * @return the arguments to generate the models
     */
    private static Object[] getMi2Si() {
        String testBase = "Mi2Si/";
        String emgFile = "Mi.emg";
        String metamodel = "umlMM.ecore";
        String modelNameBase = "uml";
        String validationModelSize = "15";
    	String tiny = "Tiny,10";
    	String small = "Small,18";
    	String normal = "Normal,40";
    	String large = "Large,92";
    	String huge = "Huge,130";
    	String[] performanceModels = {tiny, small, normal, large, huge};
    	Object[] ret = {testBase, emgFile, metamodel, modelNameBase, validationModelSize, performanceModels};
        return ret;
    }

    /**
     * Get the execution arguments for the Rail2Control tests.
     *
     * @return the arguments to generate the models
     */
    private static Object[] getRailway2Control() {
        String testBase = "Railway2Control/";
        String emgFile = "Rail.emg";
        String metamodel = "Railway.ecore";
        String modelNameBase = "rail";
        String validationModelSize = "15";
    	String tiny = "Tiny,10";
    	String small = "Small,12";
    	String normal = "Normal,23";
    	String large = "Large,28";
    	String huge = "Huge,40";
    	String[] performanceModels = {tiny, small, normal, large, huge};
    	Object[] ret = {testBase, emgFile, metamodel, modelNameBase, validationModelSize, performanceModels};
        return ret;
    }

    /**
     * Get the execution arguments for the PathExp2PetriNet_PathExp tests.
     *
     * @return the arguments to generate the models
     */
    private static Object[] getPathExp2PetriNet_PathExp() {
        String testBase = "PathExp2PetriNet/";
        String emgFile = "PathExp.emg";
        String metamodel = "PathExp.ecore";
        String modelNameBase = "path";
        String validationModelSize = "15";
    	String tiny = "Tiny,10";
    	String small = "Small,26";
    	String normal = "Normal,70";
    	String large = "Large,190";
    	String huge = "Huge,450";
    	String[] performanceModels = {tiny, small, normal, large, huge};
    	Object[] ret = {testBase, emgFile, metamodel, modelNameBase, validationModelSize, performanceModels};
        return ret;
    }

    /**
     * Get the execution arguments for the PathExp2PetriNet_PetriNet tests.
     *
     * @return the arguments to generate the models
     */
    private static Object[] getPathExp2PetriNet_PetriNet() {
        String testBase = "PathExp2PetriNet/";
        String emgFile = "PetriNet.emg";
        String metamodel = "PetriNet.ecore";
        String modelNameBase = "PetriNet2XML/petri";
        String validationModelSize = "15";
    	String tiny = "Tiny,57";
    	String small = "Small,260";
    	String normal = "Normal,1210";
    	String large = "Large,5530";
    	String huge = "Huge,25500";
    	String[] performanceModels = {tiny, small, normal, large, huge};
    	Object[] ret = {testBase, emgFile, metamodel, modelNameBase, validationModelSize, performanceModels};
        return ret;
    }



}
