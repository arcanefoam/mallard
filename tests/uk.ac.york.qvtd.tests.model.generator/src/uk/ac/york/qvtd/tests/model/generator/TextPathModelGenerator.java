package uk.ac.york.qvtd.tests.model.generator;

import java.io.Serializable;

import org.apache.commons.math3.random.RandomDataGenerator;

import PathExp2PetriNet.TextPathGen;

/**
 * We need a different generator beacuse we didnt use EMG
 * @author Goblin
 *
 */
public class TextPathModelGenerator {

	/**
	 * Use the Serial Version ID as seed
	 */
	private static final long serialVersionUID = 7385997384103005376L;


	public static void main(String[] params) {
		RandomDataGenerator rnd = new RandomDataGenerator();
		rnd.reSeed(serialVersionUID);
		// Get the EMG script
		Object[] data = getParams();
        String testBase = (String) data[0];
        String mallard = System.getenv("MALLARD");
        String mbase =  mallard + ModelGenerator.genModelRootPath + testBase + data[1] + "%1$s.xmi";
        int modelsize = Integer.valueOf((String) data[2]);
        TextPathGen.registerEPackages();
        // 1. Create 30 small sized models
        for (int i=0; i<30; i++) {
            String mpath = String.format(mbase, "c"+i);
            System.out.println(mpath);
            executeGeneration(mpath, modelsize, rnd.nextLong(0, Long.MAX_VALUE));
        }
     // 2. Create performance models
        String[] performanceModels = (String[]) data[3];
        for (String model : performanceModels) {
        	String[] modelInfo = model.split(",");
        	modelsize = Integer.valueOf(modelInfo[1]);
            String mpath = String.format(mbase, modelInfo[0]);
            System.out.println(mpath);
            executeGeneration(mpath, modelsize, rnd.nextLong(0, Long.MAX_VALUE));
        }
	}


	private static void executeGeneration(String mpath, int modelsize, long seed) {
		TextPathGen gen = new TextPathGen(seed);
		gen.generate(modelsize, mpath);
	}


	private static Object[] getParams() {
		String testBase = "TextualPathExp2PathExp/";
        String modelNameBase = "text";
        String validationModelSize = "15";
    	String tiny = "Tiny,10";
    	String small = "Small,15";
    	String normal = "Normal,25";
    	String large = "Large,50";
    	String huge = "Huge,70";
    	String[] performanceModels = {tiny, small, normal, large, huge};
    	Object[] ret = {testBase, modelNameBase, validationModelSize, performanceModels};
        return ret;
    }

}
