/*******************************************************************************
 * Copyright (c) 2016 Willink Transformations and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     E.D.Willink - initial API and implementation
 *******************************************************************************/
/**
 */
package example3.kiamacs;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node CS</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see example3.kiamacs.KiamacsPackage#getNodeCS()
 * @model abstract="true"
 * @generated
 */
public interface NodeCS extends BaseCS {
} // NodeCS
